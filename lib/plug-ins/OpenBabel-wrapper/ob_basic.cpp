

#include <openbabel/obconversion.h>
#include <openbabel/obmolecformat.h>
#include <openbabel/mol.h>
#include <openbabel/math/matrix3x3.h>
#include <openbabel/math/vector3.h>

#include <visu_tools.h>
#include <visu_basic.h>
#include <renderingMethods/renderingAtomic.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolFileFormat.h>
#include <coreTools/toolMatrix.h>
#include <visu_elements.h>
#include <visu_data.h>
#include <visu_pairs.h>
#include <visu_dump.h>

using namespace OpenBabel;

extern "C"
{
  gboolean obloaderInit();
  const char* obloaderGet_description();
  const char* obloaderGet_authors();
  const char* obloaderGet_icon();
}

#define OPENBABEL_DESCRIPTION _("<span size=\"smaller\">" \
				"This plug-in wraps the <b>OpenBabel</b>\n" \
				"library (visit the home page at URL\n" \
				"<span color=\"blue\"><u>http://www.openbabel.org</u></span>)</span>.")
#define OPENBABEL_AUTHORS     _("Caliste Damien:\n   wrapper.")

/* Local variables. */
static gchar *iconPath;

/* Local methods. */
static gboolean loadOpenBabelFile(VisuData *data, const gchar* filename,
				  ToolFileFormat *format, int nSet,
                                  GCancellable *cancel, GError **error);
static gboolean saveOpenBabelFile(ToolFileFormat *format, const char* filename,
				  int width, int height, VisuData *dataObj,
				  guchar* imageData, GError **error,
				  ToolVoidDataFunc functionWait, gpointer data);
static gpointer waitData;
static ToolVoidDataFunc waitFunc;

/* Required methods for a loadable module. */
gboolean obloaderInit()
{
  const gchar *type[] = {(char*)0};
  const gchar *typeOut[] = {(char*)"*.cif", (char*)"*.cml", (char*)"*.dmol", (char*)0};
  VisuDump *dump;

  DBG_fprintf(stderr, "OpenBabel: loading plug-in 'openbabel'...\n");

  visu_rendering_addFileFormat(visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME), 0,
                               tool_file_format_new(_("OpenBabel known formats"),
                                                    type),
                               75, loadOpenBabelFile);

  iconPath = g_build_filename(V_SIM_PIXMAPS_DIR, "openbabel.png", NULL);

  /* Declare the output using OpenBabel. */
  dump = visu_dump_new(_("OpenBabel output formats"), typeOut, saveOpenBabelFile, FALSE);
  g_object_set(G_OBJECT(dump), "ignore-type", TRUE, NULL);

  waitData = (gpointer)0;
  waitFunc = (ToolVoidDataFunc)0;

  return TRUE;
}

const char* obloaderGet_description()
{
  return OPENBABEL_DESCRIPTION;
}

const char* obloaderGet_authors()
{
  return OPENBABEL_AUTHORS;
}

const char* obloaderGet_icon()
{
  return (char*)iconPath;
}

static char* getSymbol(OpenBabel::OBAtom *atom)
{
  return (char*)etab.GetSymbol(atom->GetAtomicNum());
}

static void addNode(GList **lst, OpenBabel::OBAtom *atom)
{
  char *ele;
  GList *tmplst;
  GList *lstatom;
  gboolean found;

  ele = getSymbol(atom);

  /* Look for type ele in lst. */
  found = FALSE;
  tmplst = *lst;
  while (tmplst && !found)
    {
      lstatom = (GList*)tmplst->data;
      if ( !strcmp(getSymbol((OpenBabel::OBAtom*)lstatom->data), ele) )
	{
	  // 	  fprintf(stderr, "one node for element '%s'\n", ele);
	  lstatom = g_list_prepend(lstatom, (gpointer)atom);
	  tmplst->data = lstatom;
	  found = TRUE;
	}
      tmplst = g_list_next(tmplst);
    }
  if (!found)
    {
      //      fprintf(stderr, "one element '%s'\n", ele);
      lstatom = (GList*)0;
      lstatom = g_list_prepend(lstatom, (gpointer)atom);
      *lst = g_list_prepend(*lst, lstatom);
    }
}

static gboolean loadOpenBabelFile(VisuData *data, const gchar* filename,
				  ToolFileFormat *format, int nSet _U_,
                                  GCancellable *cancel _U_, GError **error)
{
  OpenBabel::OBMol *mol;
  OpenBabel::OBFormat *pFormat, *xyzFormat;
  bool res;
  std::ifstream fin(filename);
  std::istream* pIn = &fin;
  OpenBabel::OBConversion conv(pIn, NULL);
  GList *allNodes, *lstatom, *tmplst;
  int i, j;
  GArray *nattyp, *types;
  VisuElement *ele1, *ele2;
  guint nNodes;
  guint ntype;
  OpenBabel::OBUnitCell *uc;
  double rprimdFull[9], rprimd[3][3];
  std::vector<double> eleRGB;
  float xyz[3], xyz0[3], length, lengthMin, lengthMax;
  GList *pairs;
  gchar *infoUTF8;
  double vect[3], radius;
  gboolean newEle;
  VisuBox *box;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  /* Create a new OpenBabel object. */
  mol = new OpenBabel::OBMol;

  /* Try to guess the file format. */
  DBG_fprintf(stderr, "Open Babel: try to guess the file format of '%s'.\n", filename);
  pFormat = conv.FormatFromExt(filename);
  if (!pFormat)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("'%s' doesn't match any file format."), filename);
      fin.close();
      delete mol;
      return FALSE;
    }
  if ( pFormat->Flags() & NOTREADABLE )
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("format of '%s' is not a readable one."), filename);
      fin.close();
      delete mol;
      return FALSE;
    }
  /* Exclude the xyz file format since V_Sim handles it natively. */
  xyzFormat = conv.FindFormat("xyz");
  if (xyzFormat == pFormat)
    {
      DBG_fprintf(stderr, "OpenBabel: skip XYZ format.\n");
      fin.close();
      delete mol;
      return FALSE;
    }

  DBG_fprintf(stderr, " | set format %p.\n", (gpointer)pFormat);
  DBG_fprintf(stderr, " | format description\n%s\n", pFormat->Description());
  conv.SetInFormat(pFormat);

  /* Read the file. */
  DBG_fprintf(stderr, "Open Babel: read the file.\n");
  res = conv.Read(mol);
  fin.close();
  DBG_fprintf(stderr, " | read OK.\n");
  if (!res)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("The given file doesn't match the format '%s'."),
			   tool_file_format_getName(format));
      delete mol;
      return FALSE;
    }

  /* Store if the file is periodic or not. */
  uc = (OBUnitCell*)mol->GetData(OBGenericDataType::UnitCell);
  if (uc)
    {
      DBG_fprintf(stderr, "OpenBabel: file has periodic conditions.\n");
      uc->GetCellMatrix().GetArray(rprimdFull);
      for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	  rprimd[j][i] = rprimdFull[i * 3 + j];
      box = visu_box_new_full(rprimd, VISU_BOX_PERIODIC);
      visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box), FALSE);
      g_object_unref(box);
      uc->GetOffset().Get(vect);
    }
  else
    {
      DBG_fprintf(stderr, "OpenBabel: file has no periodic conditions.\n");
      for (i = 0; i < 3; i++)
	vect[i] = 0.;
      box = (VisuBox*)0;
    }

  DBG_fprintf(stderr, "OpenBabel: first pass to find Elements.\n");
  allNodes = (GList*)0;
  FOR_ATOMS_OF_MOL(a,mol)
    {
      addNode(&allNodes, &(*a));
    }
  DBG_fprintf(stderr, "OpenBabel: count nodes.\n");
  /* Allocate the space for the nodes. */
  ntype = g_list_length(allNodes);
  DBG_fprintf(stderr, " | %d types.\n", ntype);
  types  = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), ntype);
  nattyp = g_array_sized_new(FALSE, FALSE, sizeof(guint), ntype);
  /* Get the visuelements. */
  tmplst = allNodes;
  while (tmplst)
    {
      lstatom = (GList*)tmplst->data;
      ele1 = visu_element_retrieveFromName(getSymbol((OpenBabel::OBAtom*)lstatom->data), &newEle);
      g_array_append_val(types, ele1);
      if (newEle)
	{
	  // We use the standard OB colours.
	  eleRGB = etab.GetRGB(((OpenBabel::OBAtom*)lstatom->data)->GetAtomicNum());
	  ele1->rgb[0] = eleRGB[0];
	  ele1->rgb[1] = eleRGB[1];
	  ele1->rgb[2] = eleRGB[2];
	  tool_color_addFloatRGBA(ele1->rgb, NULL);
	  // We use standard radius for element.
	  radius =
	    etab.GetCovalentRad(((OpenBabel::OBAtom*)lstatom->data)->GetAtomicNum());
	  visu_rendering_atomic_setRadius(ele1, (float)radius);
	}
      nNodes = g_list_length(lstatom);
      g_array_append_val(nattyp, nNodes);
      g_list_free(lstatom);
      DBG_fprintf(stderr, " | '%s' : %d.\n", ele1->name, nNodes);
      tmplst = g_list_next(tmplst);
    }
  g_list_free(allNodes);

  /* Allocate space in the given visudata. */
  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  g_array_free(nattyp, TRUE);
  g_array_free(types, TRUE);

  /* Stores coordinates. */
  FOR_ATOMS_OF_MOL(a,mol)
    {
      xyz0[0] = xyz[0] = (float)a->GetX() + vect[0];
      xyz0[1] = xyz[1] = (float)a->GetY() + vect[1];
      xyz0[2] = xyz[2] = (float)a->GetZ() + vect[2];
      if (uc)
        visu_box_convertFullToCell(box, xyz, xyz0);
      visu_data_addNodeFromElementName(data, getSymbol(&(*a)), xyz, FALSE, FALSE);
    }

  if (!uc)
    box = visu_data_setTightBox(data);

  visu_box_setMargin(box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, box), TRUE);

  /* Set the bonds, if any. */
  FOR_BONDS_OF_MOL(b, mol)
    {
      ele1 = visu_element_lookup(getSymbol(b->GetBeginAtom()));
      ele2 = visu_element_lookup(getSymbol(b->GetEndAtom()));
      pairs = visu_pair_link_getAll(ele1, ele2);
      lengthMin = visu_pair_link_getDistance((VisuPairLink*)pairs->data, VISU_PAIR_DISTANCE_MIN);
      lengthMax = visu_pair_link_getDistance((VisuPairLink*)pairs->data, VISU_PAIR_DISTANCE_MAX);
      length = (float)b->GetLength();
      if (lengthMax < length)
	visu_pair_link_setDistance((VisuPairLink*)pairs->data, length, VISU_PAIR_DISTANCE_MAX);
      if (lengthMin > length || lengthMin == 0.)
	visu_pair_link_setDistance((VisuPairLink*)pairs->data, length, VISU_PAIR_DISTANCE_MIN);
    }

  /* Set the commentary. */
  if (mol->GetTitle())
    {
      infoUTF8 = g_locale_to_utf8(mol->GetTitle(), -1, NULL, NULL, NULL);
      if (infoUTF8)
	{
	  visu_data_setFileCommentary(data, infoUTF8, 0);
	  g_free(infoUTF8);
	}
      else
	{
	  g_warning("Can't convert '%s' to UTF8.\n", mol->GetTitle());
	}
    }

  delete mol;
  
  return TRUE;
}

static gboolean saveOpenBabelFile(ToolFileFormat *format _U_, const char* filename,
				  int width _U_, int height _U_, VisuData *dataObj,
				  guchar* imageData _U_, GError **error,
				  ToolVoidDataFunc functionWait, gpointer data)
{
  std::ofstream fout(filename);
  std::ostream* pOut = &fout;
  OpenBabel::OBConversion conv(NULL, pOut);
  OpenBabel::OBFormat *pFormat;
  OpenBabel::OBMol *mol;
  OpenBabel::OBAtom *atom;
  OpenBabel::OBUnitCell *cell;
  OpenBabel::vector3 a(0.,0.,0.), b(0.,0.,0.), c(0.,0.,0.);
  VisuNodeArrayIter iter;
  float coord[3];
  bool res;
  gchar *comment;
  double matrix[3][3];

  waitData = data;
  waitFunc = functionWait;

  /* Try to guess the file format. */
  DBG_fprintf(stderr, "Open Babel: try to guess the fileformat of '%s'.\n", filename);
  pFormat = conv.FormatFromExt(filename);
  if (!pFormat)
    {
      *error = g_error_new(VISU_ERROR_DUMP, DUMP_ERROR_FILE,
			   _("'%s' doesn't match any file format."), filename);
      fout.close();
      return FALSE;
    }
  if ( pFormat->Flags() & NOTWRITABLE )
    {
      *error = g_error_new(VISU_ERROR_DUMP, DUMP_ERROR_FILE,
			   _("format of '%s' is not a readable one."), filename);
      fout.close();
      return FALSE;
    }
  DBG_fprintf(stderr, " | set format %p.\n", (gpointer)pFormat);
  DBG_fprintf(stderr, " | format description\n%s\n", pFormat->Description());
  conv.SetOutFormat(pFormat);

  /* Create a new OpenBabel object. */
  mol = new OpenBabel::OBMol;

  comment = visu_data_getFileCommentary(dataObj, visu_data_getISubset(dataObj));
  if (comment)
    mol->SetTitle(comment);

  if (visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(dataObj))) != VISU_BOX_FREE)
    {
      cell = new OpenBabel::OBUnitCell;
      visu_box_getCellMatrix(visu_boxed_getBox(VISU_BOXED(dataObj)), matrix);
      a.Set(matrix[0]);
      b.Set(matrix[1]);
      c.Set(matrix[2]);
      cell->SetData(a, b, c);
      mol->SetData(cell);
    }

  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  mol->ReserveAtoms(iter.nAllStoredNodes);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(dataObj), &iter))
    {
      atom = mol->NewAtom();
      atom->SetAtomicNum(etab.GetAtomicNum(iter.element->name));
      visu_data_getNodePosition(dataObj, iter.node, coord);
      atom->SetVector((double)coord[0], (double)coord[1], (double)coord[2]);
    }

  DBG_fprintf(stderr, "Open Babel: calling write routine.\n");
  res = conv.Write(mol);

  DBG_fprintf(stderr, "Open Babel: free memory.\n");
  delete mol;
  fout.close();

  if (!res)
    {
      *error = g_error_new(VISU_ERROR_DUMP, DUMP_ERROR_FILE,
			   _("Unable to write the file."));
      return FALSE;
    }

  DBG_fprintf(stderr, "Open Babel: write succeed.\n");
  return TRUE;
}
