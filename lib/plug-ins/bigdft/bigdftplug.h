/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2011)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors: Damien CALISTE, L_Sim laboratory, (2001-2011)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at
        Documentation/licence.en.txt.
*/

#ifndef BIGDFTPLUG_H
#define BIGDFTPLUG_H

#include <visu_data.h>
#include <coreTools/toolFileFormat.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>

GtkWidget *panelBigDFT;
ToolFileFormat *fmtWvl;

#define GET_OPT_INT(f, n) g_value_get_int(tool_option_getValue(tool_file_format_getPropertyByName(f, n)))
#define GET_OPT_DBL(f, n) g_value_get_double(tool_option_getValue(tool_file_format_getPropertyByName(f, n)))
#define SET_OPT_INT(f, n, v) g_value_set_int(tool_option_getValue(tool_file_format_getPropertyByName(f, n)), v)
#define SET_OPT_DBL(f, n, v) g_value_set_double(tool_option_getValue(tool_file_format_getPropertyByName(f, n)), v)

void bigdft_show_grid(VisuData *dataObj, GArray *grid, const gchar *gG, guint id);

#endif
