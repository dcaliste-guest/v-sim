/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "xsf.h"
#include "xsf_density.h"

#include <string.h>

#include <visu_tools.h>
#include <visu_basic.h>
#include <renderingMethods/renderingAtomic.h>
#include <renderingMethods/renderingAtomic_ascii.h>
#include <renderingMethods/renderingSpin.h>
#include <coreTools/toolMatrix.h>
#include <coreTools/toolPhysic.h>


#define XSF_DESCRIPTION _("<span size=\"smaller\">" \
			  "This plug-in reads <b>XSF</b>"	\
			  " input files\n (position, forces and densities).</span>")
#define XSF_AUTHORS     "Caliste Damien"

/* Local methods */
static gboolean loadXsfIn(VisuData *data, const gchar* filename,
			  ToolFileFormat *format, int nSet,
                          GCancellable *cancel, GError **error);
static gboolean loadXsfSpin(VisuData *data, const gchar* filename,
			    ToolFileFormat *format, int nSet,
                            GCancellable *cancel, GError **error);
static int read_xsf_file(VisuData *data, GIOChannel *flux, int nSet, GError **error);
static void xsfStructuralInit(VisuRendering *method);
static void xsfSpinInit(VisuRendering *method);
/* Local variables */
static gchar *iconPath;

/* Required methods for a loadable module. */
gboolean xsfInit()
{
  DBG_fprintf(stderr, "XSF: loading plug-in 'xsf'...\n");

  DBG_fprintf(stderr, "XSF: declare a new rendering load method.\n");
  xsfStructuralInit(visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME));

  DBG_fprintf(stderr, "XSF: declare a new spin load method.\n");
  xsfSpinInit(visu_rendering_getByName(VISU_RENDERING_SPIN_NAME));

  iconPath = g_build_filename(V_SIM_PIXMAPS_DIR, "xsf.png", NULL);

  DBG_fprintf(stderr, "XSF: declare a new density load method.\n");
  xsfDensityInit();

  return TRUE;
}

const char* xsfGet_description()
{
  return XSF_DESCRIPTION;
}

const char* xsfGet_authors()
{
  return XSF_AUTHORS;
}

const char* xsfGet_icon()
{
  return iconPath;
}

static void xsfStructuralInit(VisuRendering *method)
{
  const gchar *type[] = {"*.xsf", "*.axsf", (char*)0};
  
  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("XCrysDen Structure File format"),
                                                    type),
                               90, loadXsfIn);
}

static gboolean loadXsfIn(VisuData *data, const gchar* filename,
			  ToolFileFormat *format _U_, int nSet,
                          GCancellable *cancel _U_, GError **error)
{
  int res;
  GIOChannel *readFrom;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  readFrom = g_io_channel_new_file(filename, "r", error);
  if (!readFrom)
    return FALSE;

  res = read_xsf_file(data, readFrom, nSet, error);

  if (res < 0)
    /* The file is not a XSF file. */
    return FALSE;
  else if (res > 0)
    /* The file is a XSF file but some errors occured. */
    return TRUE;
  /* Everything is OK. */
  *error = (GError*)0;
  return TRUE;
}

void xsf_reader_new(struct xsf_reader *rd)
{
  /* The storage for read line. */
  rd->line      = g_string_new("");

  /* Storage of number of elements per types. */
  rd->ntype     = 0;
  rd->elements  = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, g_free);
  rd->nodeTypes = (VisuElement**)0;
  rd->bc        = VISU_BOX_FREE;

  rd->coords    = (float*)0;
  rd->forces    = (float*)0;
  /* rd->lst       = (GList*)0; */
}

void xsf_reader_free(struct xsf_reader *rd)
{
  /* GList *tmpLst; */

  g_hash_table_destroy(rd->elements);
  if (rd->nodeTypes)
    g_free(rd->nodeTypes);
  if (rd->coords)
    g_free(rd->coords);
  if (rd->forces)
    g_free(rd->forces);
  
  /* if (rd->lst) */
  /*   { */
  /*     tmpLst = rd->lst; */
  /*     while (tmpLst) */
  /*       { */
  /*         g_free(tmpLst->data); */
  /*         tmpLst = g_list_next(tmpLst); */
  /*       } */
  /*     g_list_free(rd->lst); */
  /*   }   */

  g_string_free(rd->line, TRUE);
  if (g_io_channel_shutdown(rd->flux, FALSE, (GError**)0) != G_IO_STATUS_NORMAL)
    g_warning("XSF: can't close file.");
  g_io_channel_unref(rd->flux);
}

gboolean xsf_reader_skip_comment(struct xsf_reader *rd, GError **error)
{
  gsize term;

  /* Eat blank or commentary lines. */
  do
    {
      rd->status = g_io_channel_read_line_string(rd->flux, rd->line, &term, error);
      if (rd->status != G_IO_STATUS_NORMAL && rd->status != G_IO_STATUS_EOF)
	return FALSE;
      g_strstrip(rd->line->str);
    }
  while (rd->status != G_IO_STATUS_EOF &&
	 (rd->line->str[0] == '#' ||
	  rd->line->str[0] == '!' ||
	  rd->line->str[0] == '\0'));
  return TRUE;
}

gboolean xsf_reader_get_flag(struct xsf_reader *rd, gboolean *found, const gchar *flag,
                             int *value, gboolean  mandatory, GError **error)
{
  size_t len;

  *found = FALSE;
  len = strlen(flag);
  g_strstrip(rd->line->str);
  if (!strncmp(rd->line->str, flag, len))
    {
      if (mandatory && sscanf(rd->line->str + len, "%d", value) != 1 && *value <= 0)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong XSF format, '%s' flag has a"
				 " wrong value.\n"), flag);
	  return FALSE;
	}
      else if (!mandatory && sscanf(rd->line->str + len, "%d", value) == 1)
	{
	  if (*value <= 0)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("Wrong XSF format, '%s' flag has a"
				     " wrong value.\n"), flag);
	      return FALSE;
	    }
	}
      *found = TRUE;
    }
  if (*found)
    return xsf_reader_skip_comment(rd, error);
  else
    return TRUE;
}

static gboolean read_periodicity(struct xsf_reader *rd, GError **error)
{
  gboolean found;

  if (!xsf_reader_get_flag(rd, &found, "MOLECULE", (int*)0, FALSE, error))
    return FALSE;
  if (found)
    rd->bc = VISU_BOX_FREE;

  if (!xsf_reader_get_flag(rd, &found, "SLAB", (int*)0, FALSE, error))
    return FALSE;
  if (found)
    rd->bc = VISU_BOX_SURFACE_XY;

  if (!xsf_reader_get_flag(rd, &found, "CRYSTAL", (int*)0, FALSE, error))
    return FALSE;
  if (found)
    rd->bc = VISU_BOX_PERIODIC;

  return TRUE;
}

gboolean xsf_reader_get_box(struct xsf_reader *rd, double cart[3][3], GError **error)
{
  int i;
  
  for (i = 0; i < 3; i++)
    {
      if (sscanf(rd->line->str, "%lf %lf %lf\n",
		 cart[i], cart[i] + 1, cart[i] + 2) != 3)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong XSF format, missing float values"
				 " after tag '%s'.\n"), "PRIMVEC");
	  return FALSE;
	}
      if (!xsf_reader_skip_comment(rd, error))
	return FALSE;
    }
  DBG_fprintf(stderr, "XSF: read box %8g %8g %8g\n"
	      "              %8g %8g %8g\n"
	      "              %8g %8g %8g\n",
	      cart[0][0], cart[0][1], cart[0][2],
	      cart[1][0], cart[1][1], cart[1][2],
	      cart[2][0], cart[2][1], cart[2][2]);
  return TRUE;
}

static gboolean read_coords(struct xsf_reader *rd, GError **error)
{
  int i, zele, nb;
  float rcov, f[3];
  gchar *name, *ptChar;
  VisuElement *type;
  struct VisuMethAscii *infos;

  if (sscanf(rd->line->str, "%d", &rd->nNodes) != 1 && rd->nNodes <= 0)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("Wrong XSF format, missing or wrong integer values"
			     " after tag '%s'.\n"), "PRIMCOORD");
      return FALSE;
    }
  if (!xsf_reader_skip_comment(rd, error))
    return FALSE;
  rd->ntype     = 0;
  rd->nodeTypes = g_malloc(sizeof(VisuElement*) * rd->nNodes);
  rd->coords    = g_malloc(sizeof(float) * 3 * rd->nNodes);
  DBG_fprintf(stderr, "XSF: read coordinates.\n");
  for  (i = 0; i < rd->nNodes; i++)
    {
      name = g_strdup(rd->line->str);
      g_strstrip(name);
      ptChar = strchr(name, ' ');
      if (!ptChar)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong XSF format, can't read coordinates at line '%s'.\n"),
                               name);
	  return FALSE;
	}
      *ptChar = '\0';
      /* The first three values are coordinates.
	 Possible three others are forces. */
      nb = sscanf(ptChar + 1, "%f %f %f %f %f %f\n", rd->coords + 3 * i,
		  rd->coords + 3 * i + 1, rd->coords + 3 * i + 2, f + 0, f + 1, f + 2);
      if (nb != 3 && nb != 6)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong XSF format, can't read coordinates from '%s' (only %d read).\n"),
                               ptChar + 1, nb);
	  return FALSE;
	}
      if (nb == 6)
	{
	  /* If forces where not used, we allocate then now. */
	  if (!rd->forces)
	    rd->forces = g_malloc(sizeof(float) * 3 * rd->nNodes);
          rd->forces[3 * i + 0] = f[0];
          rd->forces[3 * i + 1] = f[1];
          rd->forces[3 * i + 2] = f[2];
	}
      /* Try to find a z number instead of a name. */
      rcov = -1.f;
      if (sscanf(name, "%d", &zele) == 1 &&
	  tool_physic_getSymbolFromZ(&ptChar, &rcov, zele))
	{
	  g_free(name);
	  name = g_strdup(ptChar);
	}
      /* adding nomloc to the hashtable */
      type = visu_element_retrieveFromName(name, (gboolean*)0);
      rd->nodeTypes[i] = type;
      infos = (struct VisuMethAscii*)g_hash_table_lookup(rd->elements,
						     (gconstpointer)type);
      if (!infos)
	{
	  infos = g_malloc(sizeof(struct VisuMethAscii));
	  infos->ele = type;
	  infos->pos = rd->ntype;
	  infos->nbNodes = 1;
	  g_hash_table_insert(rd->elements, (gpointer)type, (gpointer)infos);
	  rd->ntype += 1;
	}
      else
	infos->nbNodes += 1;
      if (!xsf_reader_skip_comment(rd, error))
	return FALSE;
    }
  DBG_fprintf(stderr, " | read OK.\n");
  return TRUE;
}


static int read_xsf_file(VisuData *data, GIOChannel *flux, int nSet, GError **error)
{
  struct xsf_reader rd;
  gboolean found;
  int valInt;
  double box[3][3];
  int i;
  int nSets, iNodes;
  GArray *nattyp, *types;
  VisuBox *boxObj;
  float xyz[3];

  /* We read every line that corresponds to this schema : "%s %f %f %f" */
  DBG_fprintf(stderr, "XSF: reading file as an xsf file.\n");

  rd.flux = flux;

  xsf_reader_new(&rd);

  /* We read the file completely to find the number of sets of points
     and we store only the one corresponding to @nSet. */
  nSets     = -1;
  if (!xsf_reader_skip_comment(&rd, error))
    {
      xsf_reader_free(&rd);
      return -1;
    }
  do
    {
      /* Test the periodicity mode. */
      if (!read_periodicity(&rd, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}

      /* If ANIMSTEPS is found, we are in animated mode. */
      if (!xsf_reader_get_flag(&rd, &found, "ANIMSTEPS", &valInt, TRUE, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}
      if (found)
	{
	  DBG_fprintf(stderr, "XSF: found the 'ANIMSTEPS' flag (%d).\n", valInt);
	  if (nSets > 0)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("Wrong XSF format, '%s' tag already"
				     " defined.\n"), "ANIMSTEPS");
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  else
	    nSets = valInt;
	}
/*       fprintf(stderr, "'%s'\n", rd.line->str); */

      /* If PRIMVEC is found, we store the box. */
      valInt = -1;
      if (!xsf_reader_get_flag(&rd, &found, "PRIMVEC", &valInt, FALSE, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}
      if (found && (valInt < 0 || (valInt - 1) == nSet))
	{
	  if (rd.bc == VISU_BOX_FREE)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("Wrong XSF format, primitive vectors found"
				     " with free boundary conditions.\n"));
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  DBG_fprintf(stderr, "XSF: found the 'PRIMVEC' flag (%d).\n", valInt);
	  /* We read the box. */
	  if (!xsf_reader_get_box(&rd, box, error))
	    {
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  /* We set nSets to 1 if not already set. */
	  if (nSets < 0)
	    nSets = 1;
	}
/*       fprintf(stderr, "'%s'\n", rd.line->str); */

      /* If PRIMCOORD is found, we store the coordinates. */
      valInt = -1;
      if (!xsf_reader_get_flag(&rd, &found, "PRIMCOORD", &valInt, FALSE, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}
      if (found && (valInt < 0 || (valInt - 1) == nSet))
	{
	  if (rd.bc == VISU_BOX_FREE)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("Wrong XSF format, primitive coordinates found"
				     " with free boundary conditions.\n"));
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  DBG_fprintf(stderr, "XSF: found the 'PRIMCOORD' flag (%d).\n", valInt);
	  /* We read the coords. */
	  if (!read_coords(&rd, error))
	    {
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  /* We set nSets to 1 if not already set. */
	  if (nSets < 0)
	    nSets = 1;
	}

      /* If ATOMS is found, we store the coordinates. */
      valInt = -1;
      if (!xsf_reader_get_flag(&rd, &found, "ATOMS", &valInt, FALSE, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}
      if (found && (valInt < 0 || (valInt - 1) == nSet))
	{
	  if (rd.bc != VISU_BOX_FREE)
	    {
	      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
				   _("Wrong XSF format, atom coordinates found"
				     " with free boundary conditions.\n"));
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  DBG_fprintf(stderr, "XSF: found the 'ATOMS' flag (%d).\n", valInt);
	  /* We read the coords. */
	  if (!read_coords(&rd, error))
	    {
	      xsf_reader_free(&rd);
	      return 1;
	    }
	  /* We set nSets to 1 if not already set. */
	  if (nSets < 0)
	    nSets = 1;
	}

      /* We go on. */
      if (!xsf_reader_skip_comment(&rd, error))
	{
	  xsf_reader_free(&rd);
	  return 1;
	}
/*       fprintf(stderr, "'%s'\n", rd.line->str); */
    }
  while(rd.status != G_IO_STATUS_EOF && rd.coords == (float*)0);
  
  DBG_fprintf(stderr, " | found %d types.\n", rd.ntype);
  if (rd.ntype == 0)
    {
      xsf_reader_free(&rd);
      return -1;
    }

  /* Allocate the space for the nodes. */
  types  = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), rd.ntype);
  nattyp = g_array_sized_new(FALSE, FALSE, sizeof(guint), rd.ntype);
  g_array_set_size(types, rd.ntype);
  g_array_set_size(nattyp, rd.ntype);
  g_hash_table_foreach(rd.elements, (GHFunc)visu_meth_asciiValToType, (gpointer)types);
  g_hash_table_foreach(rd.elements, (GHFunc)visu_meth_asciiValToNb, (gpointer)nattyp);

  DBG_fprintf(stderr, " | begin to transfer data to VisuData.\n");
  /* Begin the storage into VisuData. */
  visu_data_setNSubset(data, nSets);

  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  DBG_fprintf(stderr, "XSF: there are %d types in this file.\n", rd.ntype);
  if (DEBUG)
    for (i = 0; i < rd.ntype; i++)
      fprintf(stderr, " | %d atom(s) for type %d.\n",
              g_array_index(nattyp, guint, i), i);
  g_array_free(nattyp, TRUE);
  g_array_free(types, TRUE);
         
  /* Store the coordinates */
  boxObj = visu_box_new_full(box, rd.bc);
  visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(boxObj), FALSE);
  g_object_unref(boxObj);
  for(iNodes = 0; iNodes < rd.nNodes; iNodes++)
    {
      visu_box_convertFullToCell(boxObj, xyz, rd.coords + 3 * iNodes);
      visu_data_addNodeFromElement(data, rd.nodeTypes[iNodes],
                                   xyz, FALSE, FALSE);
    }
  visu_box_setMargin(boxObj, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, boxObj), TRUE);
  visu_box_setUnit(boxObj, TOOL_UNITS_ANGSTROEM);

  /* We store the forces as a property to be used later by the spin
     loading method. */
  if (rd.forces)
    {
      visu_rendering_atomic_setForces(data, rd.forces);
      g_object_set_data_full(G_OBJECT(data), "XSF_forces",
			     (gpointer)rd.forces, g_free);
      /* We nullify the rd.forces pointer to avoid its deletion. */
      rd.forces = (float*)0;
    }

  /* Free the local data. */
  xsf_reader_free(&rd);
  
  return 0;
}

static void freeSpin(gpointer obj, gpointer data _U_)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(float) * 3, obj);
#else
  g_free(obj);
#endif
}
static gpointer newOrCopySpin(gconstpointer obj, gpointer data _U_)
{
  float *spinData;

#if GLIB_MINOR_VERSION > 9
  spinData = g_slice_alloc(sizeof(float) * 3);
#else
  spinData = g_malloc(sizeof(float) * 3);
#endif
  if (obj)
    memcpy(spinData, obj, sizeof(float) * 3);
  else
    memset(spinData, 0, sizeof(float) * 3);
    
  return (gpointer)spinData;
}

static void xsfSpinInit(VisuRendering *method)
{
  const gchar *type[] = {"*.xsf", "*.axsf", (char*)0};

  visu_rendering_addFileFormat(method, FILE_KIND_SPIN,
                               tool_file_format_new(_("XCrysDen Structure File format"),
                                                    type),
                               40, loadXsfSpin);
}

static void initMaxModulus(VisuElement *ele _U_, GValue *val)
{
  DBG_fprintf(stderr, " | init max modulus of val %p.\n", (gpointer)val);
  g_value_init(val, G_TYPE_FLOAT);
  g_value_set_float(val, -G_MAXFLOAT);
}

static gboolean loadXsfSpin(VisuData *data, const gchar* filename,
			    ToolFileFormat *format _U_, int nSet _U_,
                            GCancellable *cancel _U_, GError **error)
{
  float *forces, *svgSpinValues;
  GValueArray *svgMaxSpinModulus;
  float sph[3], vals[3];
  VisuNodeProperty *spin;
  VisuNodeArrayIter iter;
  GValue spinValue = {0, {{0}, {0}}};
  GValue *val;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  forces = (float*)g_object_get_data(G_OBJECT(data), "XSF_forces");
  if (!forces)
    return FALSE;

  /* We check that spin and position are the same. */
  /* TODO... */

  /* Create a storage for max values of spin modulus for each element. */
  svgMaxSpinModulus = visu_node_array_setElementProperty(VISU_NODE_ARRAY(data),
                                                         VISU_RENDERING_SPIN_MAX_MODULUS_ID,
                                                         initMaxModulus);
  spin = visu_node_array_property_newPointer(VISU_NODE_ARRAY(data), VISU_RENDERING_SPIN_VALUES_ID,
                                       freeSpin, newOrCopySpin, (gpointer)0);

  g_value_init(&spinValue, G_TYPE_POINTER);
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for(visu_node_array_iterStartNumber(VISU_NODE_ARRAY(data), &iter); iter.node;
      visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(data), &iter))
    {
      visu_box_convertFullToCell(visu_boxed_getBox(VISU_BOXED(data)), vals,
                                 forces + iter.node->number * 3);
      tool_matrix_cartesianToSpherical(sph, vals);
      svgSpinValues = newOrCopySpin(sph, (gpointer)0);
      g_value_set_pointer(&spinValue, svgSpinValues);
      visu_node_property_setValue(spin, iter.node, &spinValue);
      val = g_value_array_get_nth(svgMaxSpinModulus, iter.iElement);
      g_value_set_float(val, MAX(vals[TOOL_MATRIX_SPHERICAL_MODULUS],
                                 g_value_get_float(val)));
    }

  /* We kill the temporary forces property. */
  g_free(g_object_steal_data(G_OBJECT(data), "XSF_forces"));

  /* Everything is OK. */
  *error = (GError*)0;
  return TRUE;
}
