AC_DEFUN([AC_CHECK_ABINIT],
[
  AC_MSG_CHECKING([for Abinit support])
  AC_ARG_WITH([abinit],
            [AS_HELP_STRING([--with-abinit=ARG],[ABINIT directory (with include and lib subdirs)])],
            [AB_PATH=$withval], 
            [AB_PATH="no"])

  AC_ARG_WITH([abinit_include],
            [AS_HELP_STRING([--with-abinit-include=ARG],[specific ABINIT include directory])],
            [AB_PATH_INC=$withval], 
            [AB_PATH_INC=""])

  AC_ARG_WITH([abinit_libdir],
            [AS_HELP_STRING([--with-abinit-libdir=ARG],[specific ABINIT library directory])],
            [AB_PATH_LIBDIR=$withval], 
            [AB_PATH_LIBDIR=""])
  AC_MSG_RESULT($AB_PATH)

  dnl In case ABINIT is demanded, we test Lapack first.
  if test "z$AB_PATH" != "zno" ; then
    AC_REQUIRE([ACX_LAPACK])
    if test x"$acx_lapack_ok" = xno; then
      AC_WARN([ABINIT support has been disabled since Lapack is not available.])
      AB_PATH="no"
    fi
  fi
  dnl We also test ETSF_IO
  if test "z$AB_PATH" != "zno" ; then
    
    AC_REQUIRE([AC_CHECK_ETSF_IO])
    if test x"$ac_etsf_io_ok" = xno; then
      AC_WARN([ABINIT support may not compile since ETSF_IO is not available.])
    fi
  fi
  dnl We also test LibXC
  if test "z$AB_PATH" != "zno" ; then
    
    AC_REQUIRE([AC_CHECK_LIBXC])
    if test x"$ac_libxc_ok" = xno; then
      AC_WARN([ABINIT support may not compile since LibXC is not available.])
    fi
  fi

  if test "z$AB_PATH" != "zno" ; then
    AS_IF([test "z$AB_PATH" = "zyes"],[AB_PATH="/opt/etsf/abinit"])
    AS_IF([test "z$AB_PATH_LIBDIR" = "z"],[AB_PATH_LIBDIR="$AB_PATH/lib"])
    AS_IF([test "z$AB_PATH_INC" = "z"],[AB_PATH_INC="$AB_PATH/include"])
  
    dnl First thing is to test the header files.
    CPPFLAGS_SVG=$CPPFLAGS
    CPPFLAGS="$CPPFLAGS -I$AB_PATH_INC"
    AC_CHECK_HEADER([ab6_invars.h],
                    [ac_abinit_parser_header="ab6_invars.h"],
                    [ac_abinit_parser_header="no"])
    if test $ac_abinit_parser_header = "no" ; then
      AC_CHECK_HEADER([ab7_invars.h],
                      [ac_abinit_parser_header="ab7_invars.h"],
                      [ac_abinit_parser_header="no"])
    fi
    AC_CHECK_HEADER([ab6_symmetry.h],
                    [ac_abinit_symmetry_header="ab6_symmetry.h"],
                    [ac_abinit_symmetry_header="no"])
    if test $ac_abinit_symmetry_header = "no" ; then
      AC_CHECK_HEADER([ab7_symmetry.h],
                      [ac_abinit_symmetry_header="ab7_symmetry.h"],
                      [ac_abinit_symmetry_header="no"])
    fi
    CPPFLAGS=$CPPFLAGS_SVG
    
    dnl Try to guess the lib name.
    AB_LIBRARY="abinis"
    AB_LIBRARY_VERSION=6
    if test -f $AB_PATH_LIBDIR/libabinit6.a ; then
      AB_LIBRARY="abinit6"
    fi
    if test -f $AB_PATH_LIBDIR/libabinit7.a ; then
      AB_LIBRARY="abinit7"
      AB_LIBRARY_VERSION=7
    fi

    dnl Try to find the missing dependencies in libabinis.a.
    AC_FC_WRAPPERS()
    AC_FC_FUNC([wrtout])
    AC_FC_FUNC([leave_new])
    AC_FC_FUNC([timab])
    AC_FC_FUNC([psp_from_data])
  
    AC_LANG_PUSH([C])
  
    LIBS_SVG=$LIBS
    LIBS="$LIBS_SVG -L$AB_PATH_LIBDIR -l$AB_LIBRARY $ETSF_IO_LIBS $LIBXC_LIBS $LAPACK_LIBS $BLAS_LIBS $FCLIBS"
    CFLAGS_SVG=$CFLAGS
    CFLAGS="$CFLAGS -Wno-error"

    dnl Look for the parser in libabinis.
    AC_MSG_CHECKING([for parser capabilities in lib$AB_LIBRARY.a from $AB_PATH_LIBDIR])
    AC_LINK_IFELSE([AC_LANG_SOURCE([
void $wrtout()
{
}
void $leave_new()
{
}
void $timab()
{
}
void $psp_from_data()
{
}
int main(int argc, const char **argv)
{
  ab${AB_LIBRARY_VERSION}_invars_new_from_file();
  return 0;
}
])], [ac_abinit_parser_ok=yes], [ac_abinit_parser_ok=no])
    AC_MSG_RESULT([$ac_abinit_parser_ok])

    dnl Look for specific functions in the parser routines.
    if test $ac_abinit_parser_ok = "yes" ; then
        AC_MSG_CHECKING([for invars.new_from_file_with_pseudo() routine in lib$AB_LIBRARY.a])
        AC_LINK_IFELSE([AC_LANG_SOURCE([
            void $wrtout()
            {
            }
            void $leave_new()
            {
            }
            void $timab()
            {
            }
            void $psp_from_data()
            {
            }
            int main(int argc, const char **argv)
            {
              ab${AB_LIBRARY_VERSION}_invars_new_from_file_with_pseudo();
                return 0;
            }
            ])], [ac_abinit_invars_has_new_with_pseudo=yes],
               [ac_abinit_invars_has_new_with_pseudo=no])
        AC_MSG_RESULT([$ac_abinit_invars_has_new_with_pseudo])
        AC_DEFINE([HAVE_NEW_WITH_PSEUDO], [], [If set, we can call ab_invars_new_from_file_with_pseudo()])
    fi

    dnl Look for the symmetries in libabinis.
    AC_MSG_CHECKING([for symmetry capabilities in lib$AB_LIBRARY.a from $AB_PATH_LIBDIR])
    AC_LINK_IFELSE([AC_LANG_SOURCE([
void $wrtout()
{
}
void $leave_new()
{
}
void $timab()
{
}
int main(int argc, const char **argv)
{
  ab${AB_LIBRARY_VERSION}_symmetry_new();
  return 0;
}
])], [ac_abinit_symmetry_ok=yes], [ac_abinit_symmetry_ok=no])
    AC_MSG_RESULT([$ac_abinit_symmetry_ok])
    
    dnl Look for specific functions in the symmetry routines.
    if test $ac_abinit_symmetry_ok = "yes" ; then
        AC_MSG_CHECKING([for symmetry.get_type() routine in lib$AB_LIBRARY.a])
        AC_LINK_IFELSE([AC_LANG_SOURCE([
            void $wrtout()
            {
            }
            void $leave_new()
            {
            }
            void $timab()
            {
            }
            int main(int argc, const char **argv)
            {
              ab${AB_LIBRARY_VERSION}_symmetry_get_type();
                return 0;
            }
            ])], [ac_abinit_symmetry_has_get_type=yes], [ac_abinit_symmetry_has_get_type=no])
        AC_MSG_RESULT([$ac_abinit_symmetry_has_get_type])
        AC_DEFINE([HAVE_SYM_GET_TYPE], [], [If set, we can call ab_symmetry_get_type()])
    fi

    LIBS=$LIBS_SVG
    CFLAGS=$CFLAGS_SVG
  
    AC_LANG_POP([C])

    AS_IF([test "z$ac_abinit_parser_header" != "zno" -a "z$ac_abinit_parser_ok" = "zyes"],
          [ac_abinit_parser=yes],
          [ac_abinit_parser=no])
    AS_IF([test "z$ac_abinit_symmetry_header" != "zno" -a "z$ac_abinit_symmetry_ok" = "zyes"],
          [ac_abinit_symmetry=yes],
          [ac_abinit_symmetry=no])
    AS_IF([test "z$ac_abinit_symmetry" = "zyes" -o "z$ac_abinit_parser" = "zyes"],
          [ac_abinit="yes";
           AB_LIBS="-L$AB_PATH_LIBDIR -l$AB_LIBRARY $LIBXC_LIBS $ETSF_IO_LIBS $LAPACK_LIBS $BLAS_LIBS";
           AB_CPPFLAGS="-I$AB_PATH_INC"],
          [ac_abinit=no])
  else
    ac_abinit=no
  fi
])
