AC_DEFUN([AX_CHECK_BIGDFT],
[
  AC_MSG_CHECKING([for BigDFT support])
  AC_ARG_WITH([bigdft],
            [AS_HELP_STRING([--with-bigdft=ARG],[BigDFT directory (with include and lib subdirs)])],
            [BIGDFT_PATH=$withval], 
            [BIGDFT_PATH="no"])

  AC_ARG_WITH([bigdft_include],
            [AS_HELP_STRING([--with-bigdft-include=ARG],[specific BigDFT include directory])],
            [BIGDFT_PATH_INC=$withval], 
            [BIGDFT_PATH_INC=""])

  AC_ARG_WITH([bigdft_libdir],
            [AS_HELP_STRING([--with-bigdft-libdir=ARG],[specific BigDFT library directory])],
            [BIGDFT_PATH_LIBDIR=$withval], 
            [BIGDFT_PATH_LIBDIR=""])
  AC_MSG_RESULT($BIGDFT_PATH)

  if test "z$BIGDFT_PATH" != "zno" ; then
    AS_IF([test "z$BIGDFT_PATH" = "zyes"],[BIGDFT_PATH="/opt/etsf/bigdft"])
    AS_IF([test "z$BIGDFT_PATH_LIBDIR" = "z"],[BIGDFT_PATH_LIBDIR="$BIGDFT_PATH/lib"])
    AS_IF([test "z$BIGDFT_PATH_INC" = "z"],[BIGDFT_PATH_INC="$BIGDFT_PATH/include"])
  
    PKG_CHECK_MODULES(GLIB_BIGDFT, glib-2.0 gobject-2.0 gthread-2.0 gio-2.0 >= 2.22)

    dnl First thing is to test the header file.
    CPPFLAGS_SVG=$CPPFLAGS
    CPPFLAGS="$CPPFLAGS -I$BIGDFT_PATH_INC $GLIB_BIGDFT_CFLAGS"
    AC_CHECK_HEADER([bigdft.h],
                    [ac_bigdft_header="yes"],
                    [ac_bigdft_header="no"])
    CPPFLAGS=$CPPFLAGS_SVG
    
    dnl Try to find the missing dependencies in libabinis.a.
    AC_FC_WRAPPERS()
  
    AC_LANG_PUSH([C])
  
    dnl Look for the wavefunction functions in libbigdft.a.
    AC_MSG_CHECKING([for wavefunctions in libbigdft-1.so from $BIGDFT_PATH_LIBDIR])

    CFLAGS_SVG=$CFLAGS
    CFLAGS="$CFLAGS -Wno-error"
    LIBS_SVG=$LIBS
    BIGDFT_LIBS="-L$BIGDFT_PATH_LIBDIR -lbigdft-1"
    LIBS="$LIBS_SVG $BIGDFT_LIBS"
    AC_LINK_IFELSE([AC_LANG_SOURCE([
int main(int argc, const char **argv)
{
  bigdft_read_wave_descr();
  return 0;
}
])], [ac_bigdft_ok=yes], [ac_bigdft_ok=no])
    LIBS=$LIBS_SVG
    CFLAGS=$CFLAGS_SVG

    AC_MSG_RESULT([$ac_bigdft_ok])
  
    AC_LANG_POP([C])

    AS_IF([test "z$ac_bigdft_header" = "zyes" -a "z$ac_bigdft_ok" = "zyes"],
          [ac_bigdft=yes; BIGDFT_CPPFLAGS="-I$BIGDFT_PATH_INC"],
          [ac_bigdft=no; BIGDFT_LIBS=""])
  else
    ac_bigdft=no
  fi
])
