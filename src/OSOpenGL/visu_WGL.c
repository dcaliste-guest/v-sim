/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "visu_openGL.h"

#include <windows.h>

#include <visu_tools.h>
#include <openGLFunctions/text.h>

struct _VisuPixmapContext
{
  HDC hdc;
  HDC pixmapDC;
  HBITMAP pixmap;
  int width, height;
  HGLRC context;
};

void visu_gl_setupPixelFormat(HDC hDC)
{
  /*      Pixel format index
   */
  int nPixelFormat;

  static PIXELFORMATDESCRIPTOR pfd = {
    sizeof(PIXELFORMATDESCRIPTOR),          /*size of structure*/
    1,                                      /*default version*/
    PFD_DRAW_TO_WINDOW |                    /*window drawing support*/
    PFD_SUPPORT_OPENGL |                    /*opengl support*/
    PFD_DOUBLEBUFFER,                       /*double buffering support*/
    PFD_TYPE_RGBA,                          /*RGBA color mode*/
    32,                                     /*32 bit color mode*/
    0, 0, 0, 0, 0, 0,                       /*ignore color bits*/
    0,                                      /*no alpha buffer*/
    0,                                      /*ignore shift bit*/
    0,                                      /*no accumulation buffer*/
    0, 0, 0, 0,                             /*ignore accumulation bits*/
    16,                                     /*16 bit z-buffer size*/
    0,                                      /*no stencil buffer*/
    0,                                      /*no aux buffer*/
    PFD_MAIN_PLANE,                         /*main drawing plane*/
    0,                                      /*reserved*/
    0, 0, 0 };                              /*layer masks ignored*/

  /*      Choose best matching format*/
  nPixelFormat = ChoosePixelFormat(hDC, &pfd);

  /*      Set the pixel format to the device context*/
  SetPixelFormat(hDC, nPixelFormat, &pfd);
}

void SetupPixelFormatPixmap(HDC hDC)
{
  /*      Pixel format index
   */
  int nPixelFormat;

  static PIXELFORMATDESCRIPTOR pfdPix = {
    sizeof(PIXELFORMATDESCRIPTOR),          /*size of structure*/
    1,                                      /*default version*/
    PFD_DRAW_TO_BITMAP |                    /*window drawing support*/
    PFD_SUPPORT_OPENGL,                     /*opengl support*/
    PFD_TYPE_RGBA,                          /*RGBA color mode*/
    32,                                     /*32 bit color mode*/
    0, 0, 0, 0, 0, 0,                       /*ignore color bits*/
    0,                                      /*no alpha buffer*/
    0,                                      /*ignore shift bit*/
    0,                                      /*no accumulation buffer*/
    0, 0, 0, 0,                             /*ignore accumulation bits*/
    16,                                     /*16 bit z-buffer size*/
    0,                                      /*no stencil buffer*/
    0,                                      /*no aux buffer*/
    PFD_MAIN_PLANE,                         /*main drawing plane*/
    0,                                      /*reserved*/
    0, 0, 0 };                              /*layer masks ignored*/

  /*      Choose best matching format*/
  nPixelFormat = ChoosePixelFormat(hDC, &pfdPix);

  /*      Set the pixel format to the device context*/
  SetPixelFormat(hDC, nPixelFormat, &pfdPix);
}

VisuPixmapContext* visu_pixmap_context_new(guint width, guint height)
{
  VisuPixmapContext *image;

  DBG_fprintf(stderr, "Visu WGL : creating a off-screen buffer (%dx%d).", width, height);

  image = g_malloc(sizeof(VisuPixmapContext));
  image->hdc = (HDC)0;
  image->pixmap = (HBITMAP)0;
  image->pixmapDC = (HDC)0;
  image->context = (HGLRC)0;
  image->width = width;
  image->height = height;

  image->hdc = GetDC(NULL);
  if (!image->hdc)
    {
      g_warning("Cannot get a HDC.");
      g_free(image);
      return (VisuPixmapContext*)0;
    }

  image->pixmap = CreateCompatibleBitmap(image->hdc, width, height);
  if (!image->pixmap)
    {
      g_warning("Cannot allocate a pixmap for the indirect rendering.");
      g_free(image);
      return (VisuPixmapContext*)0;
    }

  image->pixmapDC = CreateCompatibleDC(NULL);
  if (!image->pixmapDC)
    {
      g_warning("Cannot allocate a pixmapDC for the indirect rendering.");
      g_free(image);
      return (VisuPixmapContext*)0;
    }
  image->pixmap = SelectObject(image->pixmapDC, image->pixmap);

  SetupPixelFormatPixmap(image->pixmapDC);

  image->context = wglCreateContext(image->pixmapDC);
  if (!image->context)
    {
      g_warning("Cannot create indirect WGL context.");
      g_free(image);
      return (VisuPixmapContext*)0;
    }

  wglMakeCurrent(NULL, NULL);
  wglMakeCurrent(image->pixmapDC, image->context);

  /* Changing context, update fonts. */
  visu_gl_text_onNewContext();

  return image;
}

void visu_pixmap_context_free(VisuPixmapContext *dumpData)
{
  g_return_if_fail(dumpData);

  wglDeleteContext(dumpData->context);
  DeleteObject(SelectObject(dumpData->pixmapDC, dumpData->pixmap));
  DeleteDC(dumpData->pixmapDC);
  g_free(dumpData);
}

GLuint visu_gl_initFontList(guint size)
{
  GLuint BASE;
  BOOL res;
  HDC hdc;

  BASE = 0;
  
  DBG_fprintf(stderr, "Visu WGL : initialise fonts.\n");
  /* this creates display lists 1 to 256 (not checked) */
  if ( (BASE = glGenLists(256)) == 0 )
    {
      g_warning("Cannot build font display lists.\n");
      return 0;
    }
  hdc = GetDC(NULL);
  res = wglUseFontBitmaps(hdc, 0, 255, BASE);
  /*  else
      res = wglUseFontBitmaps(hdcPixmap, 0, 255, BASE);*/
  if ( !res )
    {
      g_warning("wglUseFontBitmaps returns false.\n");
      return 0;
    }
  else 
    return BASE;
}
