/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <GL/gl.h>

#include "visu_openGL.h"
#include <visu_tools.h>

/**
 * SECTION:visu_openGL
 * @short_description: Define the OS dependent routines related to
 * OpenGL.
 *
 * <para>TODO</para>
 */

/**
 * visu_pixmap_getData:
 * @width: an integer ;
 * @height: an integer ;
 * @hasAlpha: if TRUE, read also the alpha channel.
 *
 * Dump the pixels of the current GL area, assuming that its size
 * is given by @widthx@height. This method should not be used directly, see
 * visu_ui_gl_widget_getPixmapData() to dump a given OpenGL area.
 *
 * Returns: (transfer full) (element-type gint8): newly allocated dump
 * data (use g_array_unref() after use).
 */
GArray* visu_pixmap_getData(guint width, guint height, gboolean hasAlpha)
{
  GArray *image;
  guchar *row_tab;
  int row_length, m, n1, n2;

  DBG_fprintf(stderr, "Visu OpenGL: read the pixel values (%dx%d).\n", width, height);

  if (hasAlpha)
    row_length = 4 * width;
  else
    row_length = 3 * width;
  row_tab = g_malloc(sizeof(guchar) * row_length);

  image = g_array_sized_new(FALSE, FALSE, sizeof(guchar), row_length * height);

  glPixelStorei(GL_PACK_ALIGNMENT,1); /* just in case */

  /* Copy the image into our buffer */
  n2 = 0;
  for(m = height - 1; m >= 0; m--)
    {
      if (hasAlpha)
	glReadPixels(0, m, width, 1, GL_RGBA, GL_UNSIGNED_BYTE, row_tab);
      else
	glReadPixels(0, m, width, 1, GL_RGB, GL_UNSIGNED_BYTE, row_tab);
      n1 = n2;
      n2 = n1 + row_length;
      image = g_array_insert_vals(image, n1, row_tab, n2 - n1);
    }
  g_free(row_tab);

  DBG_fprintf(stderr, " | save to array %p.\n", (gpointer)image);
  return image;
}
