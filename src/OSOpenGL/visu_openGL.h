/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_GL_H
#define VISU_GL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <GL/gl.h>

#ifdef HAVE_GTKGLEXT
/**
 * GDKGLEXT_MULTIHEAD_SUPPORT: (skip)
 *
 * GtkGlExt support.
 */
  #define GDKGLEXT_MULTIHEAD_SUPPORT
  #include <gtk/gtkgl.h>
/**
 * IMPL_GTKGLEXT: (skip)
 *
 * GtkGlExt support.
 */
  #define IMPL_GTKGLEXT
#else
  #if SYSTEM_X11 == 1
  #include <X11/Xlib.h>
  #include <GL/glx.h>
/**
 * IMPL_BUILTIN_X11: (skip)
 *
 * X11 support.
 */
  #define IMPL_BUILTIN_X11
  #endif
  #if SYSTEM_WIN32 == 1
  #include <windows.h>
/**
 * IMPL_BUILTIN_WIN32: (skip)
 *
 * Win32 support.
 */
  #define IMPL_BUILTIN_WIN32
  #endif
#endif

/**
 * VisuPixmapContext:
 *
 * Short way to address #_VisuPixmapContext objects.
 */
typedef struct _VisuPixmapContext VisuPixmapContext;

GArray* visu_pixmap_getData(guint width, guint height, gboolean hasAlpha);
VisuPixmapContext* visu_pixmap_context_new(guint width, guint height);
void visu_pixmap_context_free(VisuPixmapContext *dumpData);

/**
 * visu_gl_initFontList:
 * @size: the size of the text.
 *
 * This method create a list with a default font.
 *
 * Returns: (type guint32) (transfer none): the GL id of the list storing the font.
 */
GLuint visu_gl_initFontList(guint size);

#ifdef IMPL_GTKGLEXT
/**
 * visu_gl_getGLConfig:
 * @screen: a #GdkScreen.
 *
 * Call gdk_gl_config_new_for_screen(), trying to acquire a RGBA visual with stereo 
 * capabilities. This method is used internaly and should not be used elsewhere.
 *
 * Returns: a matching #GdkGLConfig.
 */
GdkGLConfig* visu_gl_getGLConfig(GdkScreen *screen);
#endif
#ifdef IMPL_BUILTIN_X11
/**
 * visu_gl_getVisualInfo:
 * @dpy: an X display ;
 * @screenId: an X screen id.
 *
 * Call glXChooseVisual(), trying to acquire a RGBA visual with stereo 
 * capabilities. This method is used internaly and should not be used elsewhere.
 *
 * Returns: an allocated XVisualInfo.
 */
XVisualInfo* visu_gl_getVisualInfo(Display *dpy, int screenId);
#endif
#ifdef IMPL_BUILTIN_WIN32
/**
 * visu_gl_setupPixelFormat:
 * @hDC: an HDC.
 *
 * Call ChoosePixelFormat() and SetPixelFormat(), trying to acquire a RGBA visual.
 * This method is used internaly and should not be used elsewhere.
 */
void visu_gl_setupPixelFormat(HDC hDC);
#endif

#endif
