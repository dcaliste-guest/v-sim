/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOLCOLOR_H
#define TOOLCOLOR_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/**
 * TOOL_COLOR_MASK_R:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to red.
 */
#define TOOL_COLOR_MASK_R (1 << 0)
/**
 * TOOL_COLOR_MASK_G:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to green.
 */
#define TOOL_COLOR_MASK_G (1 << 1)
/**
 * TOOL_COLOR_MASK_B:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to blue.
 */
#define TOOL_COLOR_MASK_B (1 << 2)
/**
 * TOOL_COLOR_MASK_A:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value actually
 * correspond to the alpha channel.
 */
#define TOOL_COLOR_MASK_A (1 << 3)
/**
 * TOOL_COLOR_MASK_RGBA:
 *
 * This value can be used to create a mask for methods that
 * require one for reading rgb color array. This value is a
 * shortcut for #TOOL_COLOR_MASK_R | #TOOL_COLOR_MASK_G | #TOOL_COLOR_MASK_B.
 */
#define TOOL_COLOR_MASK_RGBA (15)

/******************/
/* Storing colors */
/******************/
/**
 * ToolColor:
 * @rgba: the coding of color in Red, Green, Blue, Alpha format,
 *        floating point numbers between 0 and 1 ;
 * @userData: a pointer to store some data (should be used with care).
 *
 * A structure to store colors.
 */
typedef struct _ToolColor ToolColor;
struct _ToolColor
{
  float rgba[4];
  gpointer userData;
};

#define    TOOL_TYPE_COLOR (tool_color_get_type())
GType      tool_color_get_type(void);
ToolColor* tool_color_new(float rgba[4]);
const ToolColor* tool_color_new_bright(guint id);
gboolean   tool_color_equal(ToolColor *color1, ToolColor *color2);
void       tool_color_copy(ToolColor *color, ToolColor *color_old);

void       tool_color_freeAll(void);
ToolColor* tool_color_getById(int num);
ToolColor* tool_color_getLastStored(void);
int        tool_color_getByColor(ToolColor *color);
ToolColor* tool_color_getByValues(int *pos, float red, float green, float blue, float alpha);
GList*     tool_color_getStoredColors(void);
ToolColor* tool_color_addColor(ToolColor* color);
ToolColor* tool_color_addFloatRGBA(float rgba[4], int *position);
ToolColor* tool_color_addIntRGBA(int rgba[4]);

void tool_color_convertHSVtoRGB(float* rgb, float* hsv);
void tool_color_convertHSLtoRGB(float *rgb, float *hsl);
void tool_color_convertRGBtoHSL(float *hsl, float *rgb);

G_END_DECLS

#endif
