/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOLTOOL_FILE_FORMAT_H
#define TOOLTOOL_FILE_FORMAT_H

#include <glib.h>

#include "toolOptions.h"

G_BEGIN_DECLS

/**
 * TOOL_TYPE_FILE_FORMAT:
 *
 * Return the associated #GType to the ToolFileFormat objects.
 */
#define TOOL_TYPE_FILE_FORMAT         (tool_file_format_get_type ())
/**
 * TOOL_FILE_FORMAT:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #ToolFileFormat object.
 */
#define TOOL_FILE_FORMAT(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), TOOL_TYPE_FILE_FORMAT, ToolFileFormat))
/**
 * TOOL_FILE_FORMAT_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #ToolFileFormatClass object.
 */
#define TOOL_FILE_FORMAT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TOOL_TYPE_FILE_FORMAT, ToolFileFormatClass))
/**
 * IS_TOOL_FILE_FORMAT:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #ToolFileFormat object.
 */
#define IS_TOOL_FILE_FORMAT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TOOL_TYPE_FILE_FORMAT))
/**
 * IS_TOOL_FILE_FORMAT_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #ToolFileFormatClass class.
 */
#define IS_TOOL_FILE_FORMAT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TOOL_TYPE_FILE_FORMAT))
/**
 * TOOL_FILE_FORMAT_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define TOOL_FILE_FORMAT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, TOOL_TYPE_FILE_FORMAT, ToolFileFormatClass))

typedef struct _ToolFileFormatPrivate ToolFileFormatPrivate;
/**
 * ToolFileFormat:
 *
 * An opaque structure.
 */
typedef struct _ToolFileFormat ToolFileFormat;
struct _ToolFileFormat
{
  GObject parent;

  ToolFileFormatPrivate *priv;
};
/**
 * ToolFileFormatClass:
 * @parent: the parent.
 *
 * An opaque structure.
 */
typedef struct _ToolFileFormatClass ToolFileFormatClass;
struct _ToolFileFormatClass
{
  GObjectClass parent;
};

/**
 * tool_file_format_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #ToolFileFormat objects.
 */
GType tool_file_format_get_type(void);


typedef struct _ToolFileFormatIter ToolFileFormatIter;
struct _ToolFileFormatIter
{
  GList *lst;

  gchar *name;
  gchar *label;
  GValue *val;
};

/**
 * TOOL_FILE_FORMAT_ERROR: (skip)
 *
 * Internal function for error handling.
 */
#define TOOL_FILE_FORMAT_ERROR tool_file_format_getQuark()
/**
 * tool_file_format_getQuark: (skip)
 *
 * Internal routine to get the #GQuark to handle error related to file formats.
 */
GQuark tool_file_format_getQuark(void);

/**
 * ToolFileFormatErrorFlag:
 * @TOOL_FILE_FORMAT_ERROR_METHOD: Error from the loading method.
 * @TOOL_FILE_FORMAT_ERROR_FILE: Error when opening.
 * @TOOL_FILE_FORMAT_ERROR_FORMAT: Wrongness in format.
 * @TOOL_FILE_FORMAT_ERROR_UNKNOWN_FORMAT: the file is not recognised.
 *
 * These are flags used when reading a file with a loading method associated to a file format.
 */
typedef enum
  {
    TOOL_FILE_FORMAT_ERROR_METHOD,   /* Error from the rendering method. */
    TOOL_FILE_FORMAT_ERROR_FILE,     /* Error when opening. */
    TOOL_FILE_FORMAT_ERROR_FORMAT,   /* Wrongness in format. */
    TOOL_FILE_FORMAT_ERROR_UNKNOWN_FORMAT
  } ToolFileFormatErrorFlag;

/**
 * ToolFileFormatValidate:
 * @filename: (type filename): a path.
 *
 * Function to match a given @filename for a file format. See
 * tool_file_format_setValidator().
 *
 * Since: 3.7
 *
 * Returns: TRUE if @filename is a valid name for a file format.
 */
typedef gboolean (*ToolFileFormatValidate)(const gchar *filename);

ToolFileFormat* tool_file_format_new(const gchar* descr, const gchar** patterns);
ToolFileFormat* tool_file_format_newRestricted(const gchar* descr, const gchar** patterns);
ToolFileFormat* tool_file_format_copy(const ToolFileFormat *from);
void tool_file_format_addPatterns(ToolFileFormat *fmt, const gchar **patterns);
const gchar* tool_file_format_getName(ToolFileFormat *format);
const gchar* tool_file_format_getLabel(ToolFileFormat *format);
const GList* tool_file_format_getFilePatterns(ToolFileFormat *format);
gboolean tool_file_format_canMatch(ToolFileFormat* format);
const gchar* tool_file_format_match(ToolFileFormat *format, const gchar*filename);
gboolean tool_file_format_validate(ToolFileFormat *format, const gchar *filename);
void tool_file_format_setValidator(ToolFileFormat *format, ToolFileFormatValidate validate);

void tool_file_format_addOption(ToolFileFormat *format, ToolOption *opt);
ToolOption* tool_file_format_addPropertyBoolean(ToolFileFormat *format,
                                                const gchar *name, const gchar *label,
                                                gboolean defaultVal);
ToolOption* tool_file_format_addPropertyInt(ToolFileFormat *format,
                                            const gchar *name, const gchar *label,
                                            gint defaultVal);
ToolOption* tool_file_format_addPropertyDouble(ToolFileFormat *format,
                                               const gchar *name, const gchar *label,
                                               gdouble defaultVal);
ToolOption* tool_file_format_getPropertyByName(ToolFileFormat *format,
                                               const gchar *name);
gboolean tool_file_format_iterNextProperty(ToolFileFormat *format,
                                           ToolFileFormatIter *iter);

G_END_DECLS


#endif
