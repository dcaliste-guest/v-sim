/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "toolFortran.h"
#include <visu_rendering.h>

/**
 * SECTION:toolFortran
 * @short_description: Introduces routines to read the binary Fortran data format.
 *
 * <para>In Fortran binary data are written inside tags. These tags
 * give the size to the data that are between. The routines in this
 * module can read these tags and the data inside. One must know the
 * endianness of the file and the kind of data to be read (integer,
 * float, double...). If the first tag is known, one can test the
 * endianness of the file calling tool_fortran_testEndianness().</para>
 * <para>The size of the tag (32 bits or 64 bits) is not modifiable
 * for the moment and is fixed at 32 bits. This may be changed in the
 * futur with the same kind of things that for endianness.</para>
 */

gboolean tool_fortran_readFlag(guint *nb, FILE *flux,
			      GError **error, ToolFortranEndianId endianness)
{
  char c[sizeof(int)];
  guint *val;
  register guint ks, l;

  if (fread(nb, sizeof(int), 1, flux) != 1)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("impossible to read Fortran flag, '%s' error.\n"),
			   "tool_fortran_readFlag");
      return FALSE;
    }

  if (endianness == TOOL_FORTRAN_ENDIAN_CHANGE)
    {
      ks = sizeof(int) - 1;
      for(l=0; l<sizeof(int); l++)
	c[l] = *((char*)nb + ks - l);
      val = (guint*)c;
      *nb = *val;
    }
  return TRUE;
}

gboolean tool_fortran_readCharacter(char *var, guint nb, FILE *flux,
				   GError **error, ToolFortranEndianId endianness,
				   gboolean testFlag, gboolean store)
{
  guint readNb;
  gboolean valid;

  if (testFlag)
    {
      /* Check the openning flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(char))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readCharacter");
	  return FALSE;
	}
    }

  if ((store && (readNb = fread(var, sizeof(char), nb, flux)) != nb) ||
      (!store && fseek(flux, sizeof(char) * nb, SEEK_CUR) != 0))
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("impossible to read %d characters (%d read, feof: %d, ferror: %d), '%s' error.\n"),
			   nb, readNb, feof(flux), ferror(flux), "tool_fortran_readCharacter");
      return FALSE;
    }

  if (testFlag)
    {
      /* Check the closing flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(char))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readCharacter");
	  return FALSE;
	}
    }
  return TRUE;
}
gboolean tool_fortran_readInteger(guint *var, guint nb, FILE *flux,
				 GError **error, ToolFortranEndianId endianness,
				 gboolean testFlag, gboolean store)
{
  char c[sizeof(int)];
  register guint k, ks, l;
  guint readNb, *val;
  gboolean valid;

  if (testFlag)
    {
      /* Check the openning flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(guint))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readInteger");
	  return FALSE;
	}
    }

  /* Read the data. */
  if ((store && (readNb = fread(var, sizeof(guint), nb, flux)) != nb) ||
      (!store && fseek(flux, sizeof(guint) * nb, SEEK_CUR) != 0))
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("impossible to read %d integers (%d read, feof: %d, ferror: %d), '%s' error.\n"),
			   nb, readNb, feof(flux), ferror(flux), "tool_fortran_readInteger");
      return FALSE;
    }
  
  if (testFlag)
    {
      /* Check the closing flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(guint))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readInteger");
	  return FALSE;
	}
    }

  if (store && endianness == TOOL_FORTRAN_ENDIAN_CHANGE)
    {
      for(k=0; k<nb; k++)
	{
	  ks = (k+1)*sizeof(int) - 1;
	  for(l=0; l<sizeof(int); l++)
	    c[l] = *((char*)var + ks - l);
	  val = (guint*)c;
	  var[k] = *val;
	}
    }
  return TRUE;
}
gboolean tool_fortran_readReal(float *var, guint nb, FILE *flux,
			      GError **error, ToolFortranEndianId endianness,
			      gboolean testFlag, gboolean store)
{
  char c[sizeof(float)];
  register guint k, ks, l;
  guint readNb;
  float *val;
  gboolean valid;

  if (testFlag)
    {
      /* Check the openning flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(float))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readReal");
	  return FALSE;
	}
    }

  /* Read the data. */
  if ((store && (readNb = fread(var, sizeof(float), nb, flux)) != nb) ||
      (!store && fseek(flux, sizeof(float) * nb, SEEK_CUR) != 0))
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("impossible to read %d real (%d read, feof: %d, ferror: %d), '%s' error.\n"),
			   nb, readNb, feof(flux), ferror(flux), "tool_fortran_readReal");
      return FALSE;
    }
  
  if (testFlag)
    {
      /* Check the closing flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(float))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readReal");
	  return FALSE;
	}
    }

  if (store && endianness == TOOL_FORTRAN_ENDIAN_CHANGE)
    {
      for(k=0; k<nb; k++)
	{
	  ks = (k+1)*sizeof(float) - 1;
	  for(l=0; l<sizeof(float); l++)
	    c[l] = *((char*)var + ks - l);
	  val = (float*)c;
	  var[k] = *val;
	}
    }
  return TRUE;
}
gboolean tool_fortran_readDouble(double *var, guint nb, FILE *flux,
				GError **error, ToolFortranEndianId endianness,
				gboolean testFlag, gboolean store)
{
  char c[sizeof(double)];
  register guint k, ks, l;
  guint readNb;
  double *val;
  gboolean valid;

  if (testFlag)
    {
      /* Check the openning flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(double))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readDouble");
	  return FALSE;
	}
    }

  /* Read the data. */
  if ((store && (readNb = fread(var, sizeof(double), nb, flux)) != nb) ||
      (!store && fseek(flux, sizeof(double) * nb, SEEK_CUR) != 0))
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("impossible to read %d double (%d read, feof: %d, ferror: %d), '%s' error.\n"),
			   nb, readNb, feof(flux), ferror(flux), "tool_fortran_readDouble");
      return FALSE;
    }
  
  if (testFlag)
    {
      /* Check the closing flag. */
      valid = tool_fortran_readFlag(&readNb, flux, error, endianness);
      if (!valid || readNb != nb * sizeof(double))
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("flag size unmatch, '%s' error.\n"),
			       "tool_fortran_readDouble");
	  return FALSE;
	}
    }

  if (store && endianness == TOOL_FORTRAN_ENDIAN_CHANGE)
    {
      for(k=0; k<nb; k++)
	{
	  ks = (k+1)*sizeof(double) - 1;
	  for(l=0; l<sizeof(double); l++)
	    c[l] = *((char*)var + ks - l);
	  val = (double*)c;
	  var[k] = *val;
	}
    }
  return TRUE;
}

gboolean tool_fortran_testEndianness(guint nb, FILE *flux,
				    GError **error, ToolFortranEndianId *endianness)
{
  guint readNb;
  gboolean valid;

  valid = tool_fortran_readFlag(&readNb, flux, error, TOOL_FORTRAN_ENDIAN_KEEP);
  if (valid && readNb == nb)
    {
      rewind(flux);
      *endianness = TOOL_FORTRAN_ENDIAN_KEEP;
      return TRUE;
    }

  rewind(flux);
  valid = tool_fortran_readFlag(&readNb, flux, error, TOOL_FORTRAN_ENDIAN_CHANGE);
  if (valid && readNb == nb)
    {
      rewind(flux);
      *endianness = TOOL_FORTRAN_ENDIAN_CHANGE;
      return TRUE;
    }
  
  rewind(flux);
  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
		       _("wrong fortran syntax, flag size unmatched.\n"));
  return FALSE;
}
