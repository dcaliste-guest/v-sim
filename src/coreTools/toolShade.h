/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOLSHADE_H
#define TOOLSHADE_H

#include <glib.h>
#include <glib-object.h>
#include <coreTools/toolMatrix.h>

/**
 * _ToolShade
 *
 * Opaque structure to store linear shade informations.
 */
struct _ToolShade;
/**
 * ToolShade:
 *
 * Short name to address _ToolShade objects.
 */
typedef struct _ToolShade ToolShade;

/**
 * ToolShadeColorMode:
 * @TOOL_SHADE_COLOR_MODE_RGB: variation described in the shade are applied to RGB coding colors ;
 * @TOOL_SHADE_COLOR_MODE_HSV: variation described in the shade are applied to HSV coding colors ;
 * @TOOL_SHADE_COLOR_MODE_N_VALUES: number of modes available.
 *
 * Defines color mode : Red-Green-Blue or Hue-Saturation-Value.
 */
typedef enum
  {
    TOOL_SHADE_COLOR_MODE_RGB,
    TOOL_SHADE_COLOR_MODE_HSV,
    TOOL_SHADE_COLOR_MODE_N_VALUES
  } ToolShadeColorMode;

/**
 * ToolShadeMode:
 * @TOOL_SHADE_MODE_LINEAR: all channels are defined by a linear variation
 * Ax+B ;
 * @TOOL_SHADE_MODE_ARRAY: all channels are defined by a given array of
 * values ;
 * @TOOL_SHADE_MODE_N_VALUES: the number of different shade mode.
 *
 * Defines the storage of the shade mode.
 */
typedef enum
  {
    TOOL_SHADE_MODE_LINEAR,
    TOOL_SHADE_MODE_ARRAY,
    TOOL_SHADE_MODE_N_VALUES
  } ToolShadeMode;

/**
 * ToolShadeStep:
 * @index: a value.
 * @channels: (array fixed-size=3): three values in [0;1] for RGB or
 * HSV channels.
 * 
 * Stores a step in the definition of a shade.
 *
 * Since: 3.7
 **/
typedef struct _ToolShadeStep ToolShadeStep;

struct _ToolShadeStep
{
  float index;
  float channels[3];
};

GType tool_shade_get_type(void);
#define TOOL_TYPE_SHADE (tool_shade_get_type())

ToolShade* tool_shade_new(const gchar* labelUTF8, float vectA[3], float vectB[3],
                          ToolShadeColorMode colorMode);
ToolShade* tool_shade_newFromData(const gchar* labelUTF8, guint len, float *vectCh1,
                                  float *vectCh2, float *vectCh3, ToolShadeColorMode colorMode);
ToolShade* tool_shade_newFromSteps(const gchar* labelUTF8, GList *lst,
                                   ToolShadeColorMode colorMode);
ToolShade* tool_shade_newFromString(const gchar* labelUTF8, const gchar *descr,
                                    ToolShadeColorMode colorMode, GError **error);
void tool_shade_free(ToolShade *shade);
ToolShade* tool_shade_copy(ToolShade *shade);
gboolean tool_shade_compare(ToolShade* sh1, ToolShade *sh2);
gchar* tool_shade_getLabel(ToolShade *shade);
ToolShadeColorMode tool_shade_getColorMode(ToolShade *shade);
gboolean tool_shade_setColorMode(ToolShade *shade, ToolShadeColorMode mode);
ToolShadeMode tool_shade_getMode(ToolShade *shade);
gboolean tool_shade_getLinearCoeff(ToolShade *shade, float **vectA, float **vectB);
gboolean tool_shade_setLinearCoeff(ToolShade *shade, float coeff, int channel, int order);
void tool_shade_valueToRGB(const ToolShade *shade, float rgba[4], float value);
void tool_shade_channelToRGB(const ToolShade *shade, float rgba[4], float values[3]);



GList* tool_shade_getList(void);
GList* tool_shade_appendList(ToolShade *shade, gboolean unique);

#endif
