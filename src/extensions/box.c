/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "box.h"

#include <GL/gl.h>
#include <GL/glu.h> 

#include <math.h>
#include <string.h>

#include <opengl.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/text.h>
#include <coreTools/toolMatrix.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:box legend
 * @short_description: Draw a legend describing the box.
 *
 * <para>This extension allows V_Sim to draw a small box with box
 * information like the box size.</para>
 */

/* A resource to control the position to render the box lengths. */
#define FLAG_RESOURCE_LEGEND_POSITION   "box_legend_position"
#define DESC_RESOURCE_LEGEND_POSITION   "Position of the legend area for the box ; two floating point values (0. <= v <= 1.)"
static float POSITION_DEFAULT[2] = {.5f, 0.f};

/* A resource to control if the box lengths are printed. */
#define FLAG_RESOURCE_BOX_LENGTHS   "box_show_lengths"
#define DESC_RESOURCE_BOX_LENGTHS   "Print the box lengths ; boolean (0 or 1)"
static gboolean WITH_LENGTHS_DEFAULT = FALSE;
static void exportResourcesBoxLegend(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * VisuGlExtBoxLegendClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBoxLegendClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBoxLegend:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBoxLegendPrivate:
 *
 * Private fields for #VisuGlExtBoxLegend objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBoxLegendPrivate
{
  gboolean dispose_has_run;

  /* Box definition. */
  VisuBox *box;
  gulong box_signal;
};
static VisuGlExtBoxLegend* defaultBoxLegend;

static void visu_gl_ext_box_legend_finalize(GObject* obj);
static void visu_gl_ext_box_legend_dispose(GObject* obj);
static void visu_gl_ext_box_legend_rebuild(VisuGlExt *ext);
static void visu_gl_ext_box_legend_draw(VisuGlExtFrame *frame);

/* Callbacks. */
static void onBoxSizeChanged(VisuBox *box, gfloat extens, gpointer user_data);
static void onEntryLgUsed(VisuGlExtBoxLegend *lg, gchar *key, VisuObject *obj);
static void onEntryLgPosition(VisuGlExtBoxLegend *lg, gchar *key, VisuObject *obj);

G_DEFINE_TYPE(VisuGlExtBoxLegend, visu_gl_ext_box_legend, VISU_TYPE_GL_EXT_FRAME)

static void visu_gl_ext_box_legend_class_init(VisuGlExtBoxLegendClass *klass)
{
  float rgPos[2] = {0.f, 1.f};
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Extension Box: creating the class of the object (legend).\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_BOX_LENGTHS,
                                                   DESC_RESOURCE_BOX_LENGTHS,
                                                   &WITH_LENGTHS_DEFAULT);
  visu_config_file_entry_setVersion(resourceEntry, 3.6f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_LEGEND_POSITION,
                                                      DESC_RESOURCE_LEGEND_POSITION,
                                                      2, POSITION_DEFAULT, rgPos);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesBoxLegend);

  defaultBoxLegend = (VisuGlExtBoxLegend*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_box_legend_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_box_legend_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_box_legend_rebuild;
  VISU_GL_EXT_FRAME_CLASS(klass)->draw = visu_gl_ext_box_legend_draw;
}

static void visu_gl_ext_box_legend_init(VisuGlExtBoxLegend *obj)
{
  DBG_fprintf(stderr, "Extension Box: initializing a new legend object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtBoxLegendPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->box        = (VisuBox*)0;
  obj->priv->box_signal = 0;

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BOX_LENGTHS,
                          G_CALLBACK(onEntryLgUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_LEGEND_POSITION,
                          G_CALLBACK(onEntryLgPosition), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_gl_ext_box_legend_dispose(GObject* obj)
{
  VisuGlExtBoxLegend *legend;

  DBG_fprintf(stderr, "Extension Box: dispose legend object %p.\n", (gpointer)obj);

  legend = VISU_GL_EXT_BOX_LEGEND(obj);
  if (legend->priv->dispose_has_run)
    return;
  legend->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_box_legend_setBox(legend, (VisuBox*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_box_legend_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_box_legend_finalize(GObject* obj)
{
  VisuGlExtBoxLegend *legend;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Box: finalize legend object %p.\n", (gpointer)obj);

  legend = VISU_GL_EXT_BOX_LEGEND(obj);

  /* Free privs elements. */
  if (legend->priv)
    {
      DBG_fprintf(stderr, "Extension Box: free private box.\n");
      g_free(legend->priv);
    }
  /* The free is called by g_type_free_instance... */
/*   g_free(box); */

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Box: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_box_legend_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Box: freeing ... OK.\n");
}

/**
 * visu_gl_ext_box_legend_new:
 * @name: (allow-none): the name to give to the extension.
 *
 * Creates a new #VisuGlExt to draw a legend with the box size.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBoxLegend* visu_gl_ext_box_legend_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BOX_LEGEND_ID;
  char *description = _("Draw informations related to the box.");
  VisuGlExt *extensionBox;
#define BOX_WIDTH   100
#define BOX_HEIGHT   55

  DBG_fprintf(stderr,"Extension Box: new legend object.\n");
  
  extensionBox = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_BOX_LEGEND,
                                             "name", (name)?name:name_, "label", _(name),
                                             "description", description,
                                             "nGlObj", 1, NULL));
  visu_gl_ext_setPriority(extensionBox, VISU_GL_EXT_PRIORITY_LAST);
  visu_gl_ext_setSaveState(extensionBox, TRUE);
  visu_gl_ext_frame_setTitle(VISU_GL_EXT_FRAME(extensionBox), _("Box lengths"));
  visu_gl_ext_frame_setPosition(VISU_GL_EXT_FRAME(extensionBox),
                                POSITION_DEFAULT[0], POSITION_DEFAULT[1]);
  visu_gl_ext_frame_setRequisition(VISU_GL_EXT_FRAME(extensionBox), BOX_WIDTH, BOX_HEIGHT);

  return VISU_GL_EXT_BOX_LEGEND(extensionBox);
}
/**
 * visu_gl_ext_box_legend_setBox:
 * @legend: The #VisuGlExtBoxLegend to attached to.
 * @boxObj: the box to get the size of.
 *
 * Attach an #VisuGlView to render to and setup the box to get the
 * size of also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_box_legend_setBox(VisuGlExtBoxLegend *legend, VisuBox *boxObj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX_LEGEND(legend), FALSE);

  if (legend->priv->box)
    {
      g_signal_handler_disconnect(G_OBJECT(legend->priv->box), legend->priv->box_signal);
      g_object_unref(legend->priv->box);
    }
  if (boxObj)
    {
      g_object_ref(boxObj);
      legend->priv->box_signal =
        g_signal_connect(G_OBJECT(boxObj), "SizeChanged",
                         G_CALLBACK(onBoxSizeChanged), (gpointer)legend);
    }
  else
    legend->priv->box_signal = 0;
  legend->priv->box = boxObj;

  VISU_GL_EXT_FRAME(legend)->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(legend));
}
/**
 * visu_gl_ext_box_legend_getDefault:
 *
 * V_Sim is using a default box object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtBoxLegend object used by default.
 **/
VisuGlExtBoxLegend* visu_gl_ext_box_legend_getDefault()
{
  if (!defaultBoxLegend)
    {
      defaultBoxLegend = visu_gl_ext_box_legend_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultBoxLegend), WITH_LENGTHS_DEFAULT);
    }
  return defaultBoxLegend;
}


/****************/
/* Private part */
/****************/
static void visu_gl_ext_box_legend_rebuild(VisuGlExt *ext)
{
  visu_gl_text_rebuildFontList();
  /* Force redraw. */
  VISU_GL_EXT_FRAME(ext)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(ext));
}
static void onBoxSizeChanged(VisuBox *boxObj _U_, gfloat extens _U_, gpointer user_data)
{
  DBG_fprintf(stderr, "Extension Box: caught the 'SizeChanged' signal.\n");
  VISU_GL_EXT_FRAME(user_data)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(user_data));
}
static void onEntryLgUsed(VisuGlExtBoxLegend *lg, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(lg), WITH_LENGTHS_DEFAULT);
}
static void onEntryLgPosition(VisuGlExtBoxLegend *lg, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_frame_setPosition(VISU_GL_EXT_FRAME(lg),
                                POSITION_DEFAULT[0], POSITION_DEFAULT[1]);
}
static void visu_gl_ext_box_legend_draw(VisuGlExtFrame *frame)
{
  gchar strLg[64];
  float vertices[8][3], box[3];
  VisuGlExtBoxLegend *legend;

  g_return_if_fail(VISU_IS_GL_EXT_BOX_LEGEND(frame));
  legend = VISU_GL_EXT_BOX_LEGEND(frame);

  /* Nothing to draw if no data is associated to
     the current rendering window. */
  if (!legend->priv->box)
    return;

  visu_box_getVertices(legend->priv->box, vertices, FALSE);
  box[0] = sqrt((vertices[1][0] - vertices[0][0]) *
		(vertices[1][0] - vertices[0][0]) +
		(vertices[1][1] - vertices[0][1]) *
		(vertices[1][1] - vertices[0][1]) +
		(vertices[1][2] - vertices[0][2]) *
		(vertices[1][2] - vertices[0][2]));
  box[1] = sqrt((vertices[3][0] - vertices[0][0]) *
		(vertices[3][0] - vertices[0][0]) +
		(vertices[3][1] - vertices[0][1]) *
		(vertices[3][1] - vertices[0][1]) +
		(vertices[3][2] - vertices[0][2]) *
		(vertices[3][2] - vertices[0][2]));
  box[2] = sqrt((vertices[4][0] - vertices[0][0]) *
		(vertices[4][0] - vertices[0][0]) +
		(vertices[4][1] - vertices[0][1]) *
		(vertices[4][1] - vertices[0][1]) +
		(vertices[4][2] - vertices[0][2]) *
		(vertices[4][2] - vertices[0][2]));

  glColor3fv(frame->fontRGB);

  glRasterPos2f(0.f, frame->height * 2.f / 3.f);
  sprintf(strLg, "  %s: %7.3f", "x", box[0]);
  visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 

  glRasterPos2f(0.f, frame->height / 3.f);
  sprintf(strLg, "  %s: %7.3f", "y", box[1]);
  visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 

  glRasterPos2f(0.f, 0.f);
  sprintf(strLg, "  %s: %7.3f", "z", box[2]);
  visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 
}

/* Parameters & resources*/
/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesBoxLegend(GString *data,
                                     VisuData *dataObj _U_, VisuGlView *view _U_)
{
  float xpos, ypos;

  if (!defaultBoxLegend)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_LENGTHS);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_LENGTHS, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultBoxLegend)));

  visu_gl_ext_frame_getPosition(VISU_GL_EXT_FRAME(defaultBoxLegend), &xpos, &ypos);
  visu_config_file_exportComment(data, DESC_RESOURCE_LEGEND_POSITION);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEGEND_POSITION, NULL,
                               "%4.3f %4.3f", xpos, ypos);

  visu_config_file_exportComment(data, "");
}



/**
 * SECTION:box
 * @short_description: Draw a bounding box around nodes.
 *
 * <para>This extension allows V_Sim to draw a box around the
 * nodes. The box is defined in the #VisuBox structure and can be
 * retrieved with visu_box_getGeometry(). This box is not necessary
 * orthogonal.</para>
 * <para>It has several properties, namely, its colour, its line width
 * and its line pattern. It is represented in OpenGL with simple lines
 * and is affected by the antialiasing property. Defined resources:</para>
 * <itemizedlist>
 *  <listitem>
 *   <para><emphasis>box_is_on</emphasis> (boolean): controls if a box
 *   is drawn around the rendering area (since 3.0).</para>
 *  </listitem>
 *  <listitem>
 *   <para><emphasis>box_color</emphasis> (RGB in [0;1]): defines the
 *   color of the box(since 3.0).</para>
 *  </listitem>
 *  <listitem>
 *   <para><emphasis>box_line_width</emphasis> (integer in [1;10]):
 *   defines the width of the lines of the box (since 3.0).</para>
 *  </listitem>
 *  <listitem>
 *   <para><emphasis>box_line_stipple</emphasis> (2 integers in
 *   ]0;65535]): dot scheme detail for the lines of the box. The first
 *   value is the pattern for the line of the main box and the second
 *   is the pattern for the lines of the expanded areas (since 3.4).</para>
 *  </listitem>
 * </itemizedlist>
 */

/* Parameters & resources*/
/* This is a boolean to control is the box is render or not. */
#define FLAG_RESOURCE_BOX_USED   "box_is_on"
#define DESC_RESOURCE_BOX_USED   "Control if a box is drawn around the rendering area ; boolean (0 or 1)"
static gboolean RESOURCE_BOX_USED_DEFAULT = FALSE;
/* A resource to control the color used to render the lines of the box. */
#define FLAG_RESOURCE_BOX_COLOR   "box_color"
#define DESC_RESOURCE_BOX_COLOR   "Define the color of the box ; three floating point values (0. <= v <= 1.)"
static float rgbDefault[3] = {1.0, 0.5, 0.1};
/* A resource to control the width to render the lines of the box. */
#define FLAG_RESOURCE_BOX_LINE   "box_line_width"
#define DESC_RESOURCE_BOX_LINE   "Define the width of the lines of the box ; one integer (1. <= v <= 10.)"
static float LINE_WIDTH_DEFAULT = 1.;
/* A resource to control the stipple to render the lines of the box. */
#define FLAG_RESOURCE_BOX_STIPPLE   "box_line_stipple"
#define DESC_RESOURCE_BOX_STIPPLE   "Dot scheme detail for the lines of the box (main and expanded) ; 0 < 2 integers < 2^16"
static guint16 BOX_STIPLLE_DEFAULT = 65535;
static guint16 EXPAND_STIPPLE_DEFAULT = 65280;
static gboolean readBoxLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                   VisuData *dataObj, VisuGlView *view, GError **error);

static float sideRGBDefault[4] = {0.f, 0.f, 0.f, 0.3333f};

#define RESOURCE_WITH_BASIS_DEFAULT FALSE
/* static gboolean withBasis = FALSE; */
static float basisLength = 2.5f;

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesBox(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * VisuGlExtBoxClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtBoxClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBox:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtBoxPrivate:
 *
 * Private fields for #VisuGlExtBox objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtBoxPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* Box definition. */
  VisuBox *box;
  gulong box_signal;
  /* Matrix definition (to be merge later within box. */
  float origin[3];
  float matrix[3][3];

  /* Rendenring parameters. */
  float rgb[3], sideRGB[4];
  float lineWidth;
  guint16 lineStipple[2];
};
static VisuGlExtBox* defaultBox;

static void visu_gl_ext_box_finalize(GObject* obj);
static void visu_gl_ext_box_dispose(GObject* obj);
static void visu_gl_ext_box_rebuild(VisuGlExt *ext);

/* Callbacks. */
static void onSizeChanged(VisuBox *box, gfloat extens, gpointer user_data);
static void onEntryUsed(VisuGlExtBox *box, gchar *key, VisuObject *obj);
static void onEntryColor(VisuGlExtBox *box, gchar *key, VisuObject *obj);
static void onEntryWidth(VisuGlExtBox *box, gchar *key, VisuObject *obj);
static void onEntryStipple(VisuGlExtBox *box, gchar *key, VisuObject *obj);

G_DEFINE_TYPE(VisuGlExtBox, visu_gl_ext_box, VISU_TYPE_GL_EXT)

static void visu_gl_ext_box_class_init(VisuGlExtBoxClass *klass)
{
  float rgColor[2] = {0.f, 1.f};
  float rgWidth[2] = {0.f, 10.f};
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Extension Box: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_BOX_USED,
                                                   DESC_RESOURCE_BOX_USED,
                                                   &RESOURCE_BOX_USED_DEFAULT);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_BOX_COLOR,
                                                      DESC_RESOURCE_BOX_COLOR,
                                                      3, rgbDefault, rgColor);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_BOX_LINE,
                                                      DESC_RESOURCE_BOX_LINE,
                                                      1, &LINE_WIDTH_DEFAULT, rgWidth);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                                            FLAG_RESOURCE_BOX_STIPPLE,
                                            DESC_RESOURCE_BOX_STIPPLE,
                                            1, readBoxLineStipple);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesBox);

  defaultBox = (VisuGlExtBox*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_box_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_box_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_box_rebuild;
}

static void visu_gl_ext_box_init(VisuGlExtBox *obj)
{
  DBG_fprintf(stderr, "Extension Box: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtBoxPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->isBuilt     = FALSE;
  obj->priv->origin[0]   = 0.f;
  obj->priv->origin[1]   = 0.f;
  obj->priv->origin[2]   = 0.f;
  tool_matrix_setIdentity(obj->priv->matrix);
  obj->priv->rgb[0]      = rgbDefault[0];
  obj->priv->rgb[1]      = rgbDefault[1];
  obj->priv->rgb[2]      = rgbDefault[2];
  obj->priv->sideRGB[0]  = sideRGBDefault[0];
  obj->priv->sideRGB[1]  = sideRGBDefault[1];
  obj->priv->sideRGB[2]  = sideRGBDefault[2];
  obj->priv->sideRGB[3]  = sideRGBDefault[3];
  obj->priv->lineWidth   = LINE_WIDTH_DEFAULT;
  obj->priv->lineStipple[0] = BOX_STIPLLE_DEFAULT;
  obj->priv->lineStipple[1] = EXPAND_STIPPLE_DEFAULT;
  obj->priv->box          = (VisuBox*)0;
  obj->priv->box_signal   = 0;

  /* withBasis          = RESOURCE_WITH_BASIS_DEFAULT; */
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BOX_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BOX_COLOR,
                          G_CALLBACK(onEntryColor), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BOX_LINE,
                          G_CALLBACK(onEntryWidth), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_BOX_STIPPLE,
                          G_CALLBACK(onEntryStipple), (gpointer)obj, G_CONNECT_SWAPPED);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_box_dispose(GObject* obj)
{
  VisuGlExtBox *box;

  DBG_fprintf(stderr, "Extension Box: dispose object %p.\n", (gpointer)obj);

  box = VISU_GL_EXT_BOX(obj);
  if (box->priv->dispose_has_run)
    return;
  box->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_box_setBox(box, (VisuBox*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_box_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_box_finalize(GObject* obj)
{
  VisuGlExtBox *box;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Box: finalize object %p.\n", (gpointer)obj);

  box = VISU_GL_EXT_BOX(obj);

  /* Free privs elements. */
  if (box->priv)
    {
      DBG_fprintf(stderr, "Extension Box: free private box.\n");
      g_free(box->priv);
    }
  /* The free is called by g_type_free_instance... */
/*   g_free(box); */

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Box: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_box_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Box: freeing ... OK.\n");
}

/**
 * visu_gl_ext_box_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_BOX_ID).
 *
 * Creates a new #VisuGlExt to draw a box.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtBox* visu_gl_ext_box_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_BOX_ID;
  char *description = _("Draw a box representing the limit of the area.");
  VisuGlExt *extensionBox;

  DBG_fprintf(stderr,"Extension Box: new object.\n");
  
  extensionBox = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_BOX,
                                              "name", (name)?name:name_, "label", _(name),
                                              "description", description,
                                              "nGlObj", 1, NULL));
  visu_gl_ext_setPriority(extensionBox, VISU_GL_EXT_PRIORITY_LOW);

  return VISU_GL_EXT_BOX(extensionBox);
}
/**
 * visu_gl_ext_box_setBox:
 * @box: the #VisuGlExtBox object to attach to.
 * @boxObj: the box to get the definition of.
 *
 * Attach the #VisuBox to draw the frame of.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 **/
gboolean visu_gl_ext_box_setBox(VisuGlExtBox *box, VisuBox *boxObj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);

  if (box->priv->box)
    {
      g_signal_handler_disconnect(G_OBJECT(box->priv->box), box->priv->box_signal);
      g_object_unref(box->priv->box);
    }
  if (boxObj)
    {
      g_object_ref(boxObj);
      box->priv->box_signal =
        g_signal_connect(G_OBJECT(boxObj), "SizeChanged",
                         G_CALLBACK(onSizeChanged), (gpointer)box);
    }
  else
    box->priv->box_signal = 0;
  box->priv->box = boxObj;

  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setBasis:
 * @box: the #VisuGlExtBox object to attach to.
 * @orig: (array fixed-size=3): the origin.
 * @mat: (array fixed-size=9): the basis-set.
 *
 * Define the box to draw with a simple matrix basis-set and an origin.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 **/
gboolean visu_gl_ext_box_setBasis(VisuGlExtBox *box, float orig[3], float mat[3][3])
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);

  visu_gl_ext_box_setBox(box, (VisuBox*)0);
  box->priv->origin[0] = orig[0];
  box->priv->origin[1] = orig[1];
  box->priv->origin[2] = orig[2];
  memcpy(box->priv->matrix, mat, sizeof(float) * 9);

  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}

/**
 * visu_gl_ext_box_setRGB:
 * @box: the #VisuGlExtBox to update.
 * @rgb: (array fixed-size=3): a three floats array with values (0 <=
 * values <= 1) for the red, the green and the blue color. Only values
 * specified by the mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G, #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_RGBA or a
 * combinaison to indicate what values in the rgb array must be taken
 * into account.
 *
 * Method used to change the value of the parameter box_color.
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 */
gboolean visu_gl_ext_box_setRGB(VisuGlExtBox *box, float rgb[3], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);
    
  if (mask & TOOL_COLOR_MASK_R && box->priv->rgb[0] != rgb[0])
    {
      box->priv->rgb[0] = rgb[0];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_G && box->priv->rgb[1] != rgb[1])
    {
      box->priv->rgb[1] = rgb[1];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_B && box->priv->rgb[2] != rgb[2])
    {
      box->priv->rgb[2] = rgb[2];
      diff = TRUE;
    }
  if (!diff)
    return FALSE;

  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setSideRGB:
 * @box: the #VisuGlExtBox to update.
 * @rgba: (array fixed-size=4): a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue color and the alpha channel. Only values
 * specified by the mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_A or a combinaison to indicate
 * what values in the @rgba array must be taken into account.
 *
 * Change the colour to represent the side of the super-cell. A
 * channel alpha of zero, means that the box is rendered as wire-frame
 * only. The sides are indeed drawn only if the box has expansion.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 */
gboolean visu_gl_ext_box_setSideRGB(VisuGlExtBox *box, float rgba[4], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);
    
  if (mask & TOOL_COLOR_MASK_R && box->priv->sideRGB[0] != rgba[0])
    {
      box->priv->sideRGB[0] = rgba[0];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_G && box->priv->sideRGB[1] != rgba[1])
    {
      box->priv->sideRGB[1] = rgba[1];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_B && box->priv->sideRGB[2] != rgba[2])
    {
      box->priv->sideRGB[2] = rgba[2];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_A && box->priv->sideRGB[3] != rgba[3])
    {
      box->priv->sideRGB[3] = rgba[3];
      diff = TRUE;
    }
  if (!diff)
    return FALSE;

  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setLineWidth:
 * @box: the #VisuGlExtBox to update.
 * @width: value of the desired box line width.
 *
 * Method used to change the value of the parameter box_line_width.
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 */
gboolean visu_gl_ext_box_setLineWidth(VisuGlExtBox *box, float width)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);
    
  if (width < 1. || width > 10. || width == box->priv->lineWidth)
    return FALSE;

  box->priv->lineWidth = width;
  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setLineStipple:
 * @box: the #VisuGlExtBox to update.
 * @stipple: a pattern for line stipple in OpenGL.
 *
 * Method used to change the value of the parameter box_line_stipple
 * (main part).
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 */
gboolean visu_gl_ext_box_setLineStipple(VisuGlExtBox *box, guint16 stipple)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);
    
  if (stipple == box->priv->lineStipple[0])
    return FALSE;

  box->priv->lineStipple[0] = stipple;
  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setExpandStipple:
 * @box: the #VisuGlExtBox to update.
 * @stipple: a pattern for line stipple in OpenGL.
 *
 * Method used to change the value of the parameter box_line_stipple
 * (expanded part).
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 */
gboolean visu_gl_ext_box_setExpandStipple(VisuGlExtBox *box, guint16 stipple)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);
    
  if (stipple == box->priv->lineStipple[1])
    return FALSE;

  box->priv->lineStipple[1] = stipple;
  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/**
 * visu_gl_ext_box_setOrigin:
 * @box: the #VisuGlExtBox to update.
 * @origin: (array fixed-size=3): the new origin in cartesian
 * coordinates.
 *
 * Change the origin of the drawn box. This should be used with care
 * since the OpenGL rendering area is not changed by moving the box,
 * some parts may become unrendered.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_box_draw() should be called.
 **/
gboolean visu_gl_ext_box_setOrigin(VisuGlExtBox *box, float origin[3])
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), FALSE);

  if (origin[0] == box->priv->origin[0] &&
      origin[1] == box->priv->origin[1] &&
      origin[2] == box->priv->origin[2])
    return FALSE;
      
  box->priv->origin[0] = origin[0];
  box->priv->origin[1] = origin[1];
  box->priv->origin[2] = origin[2];

  box->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(box));
}
/* Get methods. */
/**
 * visu_gl_ext_box_getRGB:
 * @box: the #VisuGlExtBox to inquire.
 *
 * Read the colour components of box (in [0;1]). 
 *
 * Returns: all the colour values of the current box line.
 */
float* visu_gl_ext_box_getRGB(VisuGlExtBox *box)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), rgbDefault);
  
  return box->priv->rgb;
}
/**
 * visu_gl_ext_box_getSideRGB:
 * @box: the #VisuGlExtBox to inquire.
 *
 * Read the colour components of the sides of the box (in [0;1]). 
 *
 * Returns: all the colour values of the current box line.
 */
float* visu_gl_ext_box_getSideRGB(VisuGlExtBox *box)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), rgbDefault);
  
  return box->priv->sideRGB;
}
/**
 * visu_gl_ext_box_getLineWidth:
 * @box: the #VisuGlExtBox to inquire.
 *
 * Read the line width used for box.
 *
 * Returns: the value of current box line width.
 */
float visu_gl_ext_box_getLineWidth(VisuGlExtBox *box)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), LINE_WIDTH_DEFAULT);
  
  return box->priv->lineWidth;
}
/**
 * visu_gl_ext_box_getLineStipple:
 * @box: the #VisuGlExtBox to inquire.
 *
 * Read the line stipple pattern used for box (main part).
 *
 * Returns: the value of current box line pattern.
 */
guint16 visu_gl_ext_box_getLineStipple(VisuGlExtBox *box)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), BOX_STIPLLE_DEFAULT);
  
  return box->priv->lineStipple[0];
}
/**
 * visu_gl_ext_box_getExpandStipple:
 * @box: the #VisuGlExtBox to inquire.
 *
 * Read the line stipple pattern used for box (expanded part).
 *
 * Returns: the value of current box line pattern.
 */
guint16 visu_gl_ext_box_getExpandStipple(VisuGlExtBox *box)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_BOX(box), EXPAND_STIPPLE_DEFAULT);
  
  return box->priv->lineStipple[1];
}
/**
 * visu_gl_ext_box_getDefault:
 *
 * V_Sim is using a default box object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtBox object used by default.
 **/
VisuGlExtBox* visu_gl_ext_box_getDefault()
{
  if (!defaultBox)
    {
      defaultBox = visu_gl_ext_box_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultBox), RESOURCE_BOX_USED_DEFAULT);
    }
  return defaultBox;
}


/****************/
/* Private part */
/****************/
static void visu_gl_ext_box_rebuild(VisuGlExt *ext)
{
  VisuGlExtBox *box = VISU_GL_EXT_BOX(ext);

  /* Force redraw. */
  box->priv->isBuilt = FALSE;
  visu_gl_ext_box_draw(box);
}
static void onSizeChanged(VisuBox *boxObj _U_, gfloat extens _U_, gpointer user_data)
{
  VisuGlExtBox *box = VISU_GL_EXT_BOX(user_data);

  DBG_fprintf(stderr, "Extension Box: caught the 'SizeChanged' signal.\n");

  box->priv->isBuilt = FALSE;
  visu_gl_ext_box_draw(box);
}
static void onEntryUsed(VisuGlExtBox *box, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(box), RESOURCE_BOX_USED_DEFAULT);
}
static void onEntryColor(VisuGlExtBox *box, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_box_setRGB(box, rgbDefault, TOOL_COLOR_MASK_RGBA);
}
static void onEntryWidth(VisuGlExtBox *box, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_box_setLineWidth(box, LINE_WIDTH_DEFAULT);
}
static void onEntryStipple(VisuGlExtBox *box, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_box_setLineStipple(box, BOX_STIPLLE_DEFAULT);
  visu_gl_ext_box_setExpandStipple(box, EXPAND_STIPPLE_DEFAULT);
}
static void drawSides(float ext[3], float v[8][3], float rgba[4])
{
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_CULL_FACE);
  glColor4fv(rgba);
  if (ext[0] > 0.)
    {
      glBegin(GL_POLYGON);
      glVertex3fv(v[0]);
      glVertex3fv(v[3]);
      glVertex3fv(v[7]);
      glVertex3fv(v[4]);
      glEnd();
      glBegin(GL_POLYGON);
      glVertex3fv(v[6]);
      glVertex3fv(v[5]);
      glVertex3fv(v[1]);
      glVertex3fv(v[2]);
      glEnd();
    }
  if (ext[1] > 0.)
    {
      glBegin(GL_POLYGON);
      glVertex3fv(v[0]);
      glVertex3fv(v[1]);
      glVertex3fv(v[5]);
      glVertex3fv(v[4]);
      glEnd();
      glBegin(GL_POLYGON);
      glVertex3fv(v[6]);
      glVertex3fv(v[7]);
      glVertex3fv(v[3]);
      glVertex3fv(v[2]);
      glEnd();
    }
  if (ext[2] > 0.)
    {
      glBegin(GL_POLYGON);
      glVertex3fv(v[0]);
      glVertex3fv(v[1]);
      glVertex3fv(v[2]);
      glVertex3fv(v[3]);
      glEnd();
      glBegin(GL_POLYGON);
      glVertex3fv(v[4]);
      glVertex3fv(v[5]);
      glVertex3fv(v[6]);
      glVertex3fv(v[7]);
      glEnd();
    }
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
}
/**
 * visu_gl_ext_box_draw:
 * @box: a #VisuBox object.
 *
 * This method create a compile list that draw a box for the given @box.
 */
void visu_gl_ext_box_draw(VisuGlExtBox *box)
{
  int i, j, k;
  float v[8][3];
  float ext[3], centre[3];
  float material[5] = {.5f, .5f, .2f, .5f, .0f};
  GLUquadricObj *obj;
  gchar strLg[64];

  /* Nothing to draw; */
  g_return_if_fail(VISU_IS_GL_EXT_BOX(box));
  if(!visu_gl_ext_getActive(VISU_GL_EXT(box)) || box->priv->isBuilt) return;

  DBG_fprintf(stderr, "Extension box: creating box for"
	      " VisuBox %p.\n", (gpointer)box->priv->box);
  if (box->priv->box)
    {
      visu_box_getVertices(box->priv->box, v, FALSE);
      visu_box_getExtension(box->priv->box, ext);
    }
  else
    {
      /* We build the vertex array. */
      v[0][0] = 0.f;
      v[0][1] = 0.f;
      v[0][2] = 0.f;
      v[1][0] = box->priv->matrix[0][0];
      v[1][1] = box->priv->matrix[1][0];
      v[1][2] = box->priv->matrix[2][0];
      v[2][0] = box->priv->matrix[0][0] + box->priv->matrix[0][1];
      v[2][1] = box->priv->matrix[1][0] + box->priv->matrix[1][1];
      v[2][2] = box->priv->matrix[2][0] + box->priv->matrix[2][1];
      v[3][0] = box->priv->matrix[0][1];
      v[3][1] = box->priv->matrix[1][1];
      v[3][2] = box->priv->matrix[2][1];
      v[4][0] = box->priv->matrix[0][2];
      v[4][1] = box->priv->matrix[1][2];
      v[4][2] = box->priv->matrix[2][2];
      v[5][0] = box->priv->matrix[0][0] + box->priv->matrix[0][2];
      v[5][1] = box->priv->matrix[1][0] + box->priv->matrix[1][2];
      v[5][2] = box->priv->matrix[2][0] + box->priv->matrix[2][2];
      v[6][0] = box->priv->matrix[0][0] + box->priv->matrix[0][1] + box->priv->matrix[0][2];
      v[6][1] = box->priv->matrix[1][0] + box->priv->matrix[1][1] + box->priv->matrix[1][2];
      v[6][2] = box->priv->matrix[2][0] + box->priv->matrix[2][1] + box->priv->matrix[2][2];
      v[7][0] = box->priv->matrix[0][1] + box->priv->matrix[0][2];
      v[7][1] = box->priv->matrix[1][1] + box->priv->matrix[1][2];
      v[7][2] = box->priv->matrix[2][1] + box->priv->matrix[2][2];
      ext[0] = 1.f;
      ext[1] = 1.f;
      ext[2] = 1.f;
    }

  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(box)), 1);
  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(box)), GL_COMPILE);

  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);

  glLineWidth(box->priv->lineWidth);

  glTranslatef(box->priv->origin[0], box->priv->origin[1], box->priv->origin[2]);

  /* We draw sides of the box, if specified. */
  if ((ext[0] > 0. || ext[1] > 0. || ext[2] > 0.) &&
      box->priv->sideRGB[3] == 1.f)
    drawSides(ext, v, box->priv->sideRGB);
  /* Draw the basic lines. */
  glColor3fv(box->priv->rgb);
  if (box->priv->lineStipple[0] != 65535)
    {
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, box->priv->lineStipple[0]);
    }
  glBegin(GL_LINES);
  glVertex3fv(v[0]);
  glVertex3fv(v[1]);
  glVertex3fv(v[1]);
  glVertex3fv(v[2]);
  glVertex3fv(v[2]);
  glVertex3fv(v[3]);
  glVertex3fv(v[3]);
  glVertex3fv(v[0]);
  glVertex3fv(v[4]);
  glVertex3fv(v[5]);
  glVertex3fv(v[5]);
  glVertex3fv(v[6]);
  glVertex3fv(v[6]);
  glVertex3fv(v[7]);
  glVertex3fv(v[7]);
  glVertex3fv(v[4]);
  glVertex3fv(v[0]);
  glVertex3fv(v[4]);
  glVertex3fv(v[1]);
  glVertex3fv(v[5]);
  glVertex3fv(v[2]);
  glVertex3fv(v[6]);
  glVertex3fv(v[3]);
  glVertex3fv(v[7]);
  glEnd();
  if (box->priv->lineStipple[0] != 65535)
    glDisable(GL_LINE_STIPPLE);
  /* Draw the extension lines. */
  if (ext[0] > 0. || ext[1] > 0. || ext[2] > 0.)
    {
      glColor3fv(box->priv->rgb);
      /* We draw then the expansion lines. */
      if (box->priv->lineStipple[1] != 65535)
	{
	  glEnable(GL_LINE_STIPPLE);
	  glLineStipple(1, box->priv->lineStipple[1]);
	}
      glBegin(GL_LINES);
      /* X coordinate. */
      for (j = -(int)ext[1]; j < 2 + (int)ext[1]; j++)
	for (k = -(int)ext[2]; k < 2 + (int)ext[2]; k++)
	  {
	    glVertex3f(-ext[0] * v[1][0] +  v[3][0] * j + v[4][0] * k,
		       -ext[0] * v[1][1] +  v[3][1] * j + v[4][1] * k,
		       -ext[0] * v[1][2] +  v[3][2] * j + v[4][2] * k);
	    if ((j == 0 || j == 1) && (k == 0 || k == 1))
	      {
		glVertex3f(v[3][0] * j + v[4][0] * k,
			   v[3][1] * j + v[4][1] * k,
			   v[3][2] * j + v[4][2] * k);
		glVertex3f(v[1][0] +  v[3][0] * j + v[4][0] * k,
			   v[1][1] +  v[3][1] * j + v[4][1] * k,
			   v[1][2] +  v[3][2] * j + v[4][2] * k);
	      }
	    glVertex3f((1. + ext[0]) * v[1][0] +  v[3][0] * j + v[4][0] * k,
		       (1. + ext[0]) * v[1][1] +  v[3][1] * j + v[4][1] * k,
		       (1. + ext[0]) * v[1][2] +  v[3][2] * j + v[4][2] * k);
	  }
      /* Y coordinate. */
      for (i = -(int)ext[0]; i < 2 + (int)ext[0]; i++)
	for (k = -(int)ext[2]; k < 2 + (int)ext[2]; k++)
	  {
	    glVertex3f(-ext[1] * v[3][0] +  v[1][0] * i + v[4][0] * k,
		       -ext[1] * v[3][1] +  v[1][1] * i + v[4][1] * k,
		       -ext[1] * v[3][2] +  v[1][2] * i + v[4][2] * k);
	    if ((i == 0 || i == 1) && (k == 0 || k == 1))
	      {
		glVertex3f(v[1][0] * i + v[4][0] * k,
			   v[1][1] * i + v[4][1] * k,
			   v[1][2] * i + v[4][2] * k);
		glVertex3f(v[3][0] +  v[1][0] * i + v[4][0] * k,
			   v[3][1] +  v[1][1] * i + v[4][1] * k,
			   v[3][2] +  v[1][2] * i + v[4][2] * k);
	      }
	    glVertex3f((1. + ext[1]) * v[3][0] +  v[1][0] * i + v[4][0] * k,
		       (1. + ext[1]) * v[3][1] +  v[1][1] * i + v[4][1] * k,
		       (1. + ext[1]) * v[3][2] +  v[1][2] * i + v[4][2] * k);
	  }
      /* Z coordinate. */
      for (i = -(int)ext[0]; i < 2 + (int)ext[0]; i++)
	for (j = -(int)ext[1]; j < 2 + (int)ext[1]; j++)
	  {
	    glVertex3f(-ext[2] * v[4][0] +  v[1][0] * i + v[3][0] * j,
		       -ext[2] * v[4][1] +  v[1][1] * i + v[3][1] * j,
		       -ext[2] * v[4][2] +  v[1][2] * i + v[3][2] * j);
	    if ((j == 0 || j == 1) && (i == 0 || i == 1))
	      {
		glVertex3f(v[1][0] * i + v[3][0] * j,
			   v[1][1] * i + v[3][1] * j,
			   v[1][2] * i + v[3][2] * j);
		glVertex3f(v[4][0] +  v[1][0] * i + v[3][0] * j,
			   v[4][1] +  v[1][1] * i + v[3][1] * j,
			   v[4][2] +  v[1][2] * i + v[3][2] * j);
	      }
	    glVertex3f((1. + ext[2]) * v[4][0] +  v[1][0] * i + v[3][0] * j,
		       (1. + ext[2]) * v[4][1] +  v[1][1] * i + v[3][1] * j,
		       (1. + ext[2]) * v[4][2] +  v[1][2] * i + v[3][2] * j);
	  }
      glEnd();
      if (box->priv->lineStipple[1] != 65535)
	glDisable(GL_LINE_STIPPLE);
    }
  /* We draw sides of the box, if specified. */
  if ((ext[0] > 0. || ext[1] > 0. || ext[2] > 0.) &&
      box->priv->sideRGB[3] > 0.f && box->priv->sideRGB[3] < 1.f)
    drawSides(ext, v, box->priv->sideRGB);

  glEnable(GL_LIGHTING);
  glEnable(GL_DITHER); /* WARNING: it is the default! */
  /* Draw the basis set if needed. */
  if (FALSE)
    {
      visu_box_getCentre(box->priv->box, centre);

      obj = gluNewQuadric();
      visu_gl_setHighlightColor(material, box->priv->rgb, 1.f);

      /* Draw the basis set. */
      glPushMatrix();
      glTranslated(0., 0., 0.);
      glRotated(90., 0, 1, 0);  
      visu_gl_drawSmoothArrow(obj, -1, VISU_GL_ARROW_BOTTOM_CENTERED,
                              basisLength - 0.3f, 0.1f, 10, FALSE,
                              0.3f, 0.2f, 10, FALSE);
      glRasterPos3f(0.0f, 0.0f, basisLength);
      sprintf(strLg, _("x: %7.3f"), centre[0]);
      visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 
      glPopMatrix();
      glPushMatrix();
      glTranslated(0., 0., 0.);
      glRotated(-90., 1, 0, 0);  
      visu_gl_drawSmoothArrow(obj, -1, VISU_GL_ARROW_BOTTOM_CENTERED,
                              basisLength - 0.3f, 0.1f, 10, FALSE,
                              0.3f, 0.2f, 10, FALSE);
      glRasterPos3f(0.0f, 0.0f, basisLength);
      sprintf(strLg, _("y: %7.3f"), centre[1]);
      visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 
      glPopMatrix();
      glPushMatrix();
      glTranslated(0., 0., 0.);
      visu_gl_drawSmoothArrow(obj, -1, VISU_GL_ARROW_BOTTOM_CENTERED,
                              basisLength - 0.3f, 0.1f, 10, FALSE,
                              0.3f, 0.2f, 10, FALSE);
      glRasterPos3f(0.0f, 0.0f, basisLength);
      sprintf(strLg, _("z: %7.3f"), centre[2]);
      visu_gl_text_drawChars(strLg, VISU_GL_TEXT_SMALL); 
      glPopMatrix();

      gluDeleteQuadric(obj);
    }
  glLineWidth(1.);
  glEndList();

  box->priv->isBuilt = TRUE;
}

/* Parameters & resources*/
/* This is a boolean to control is the box is render or not. */
static gboolean readBoxLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                   VisuData *dataObj _U_, VisuGlView *view _U_,
                                   GError **error)
{
  int stipple[2];

  g_return_val_if_fail(nbLines == 1, FALSE);
  
  if (!tool_config_file_readInteger(lines[0], position, stipple, 2, error))
    return FALSE;
  BOX_STIPLLE_DEFAULT = (guint16)stipple[0];
  EXPAND_STIPPLE_DEFAULT = (guint16)stipple[1];

  return TRUE;
}

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesBox(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  if (!defaultBox)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultBox)));

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_COLOR, NULL,
                               "%4.3f %4.3f %4.3f",
                               defaultBox->priv->rgb[0], defaultBox->priv->rgb[1],
                               defaultBox->priv->rgb[2]);

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_LINE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_LINE, NULL,
                               "%4.0f", defaultBox->priv->lineWidth);

  visu_config_file_exportComment(data, DESC_RESOURCE_BOX_STIPPLE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_BOX_STIPPLE, NULL,
                               "%d %d", defaultBox->priv->lineStipple[0],
                               defaultBox->priv->lineStipple[1]);

  visu_config_file_exportComment(data, "");
}
