/*   EXTRAITS DE LA LICENCE
     Copyright CEA, contributeurs : Luc BILLARD et Damien
     CALISTE, laboratoire L_Sim, (2001-2005)
  
     Adresse m�l :
     BILLARD, non joignable par m�l ;
     CALISTE, damien P caliste AT cea P fr.

     Ce logiciel est un programme informatique servant � visualiser des
     structures atomiques dans un rendu pseudo-3D. 

     Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
     respectant les principes de diffusion des logiciels libres. Vous pouvez
     utiliser, modifier et/ou redistribuer ce programme sous les conditions
     de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
     sur le site "http://www.cecill.info".

     Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
     pris connaissance de la licence CeCILL, et que vous en avez accept� les
     termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
     Copyright CEA, contributors : Luc BILLARD et Damien
     CALISTE, laboratoire L_Sim, (2001-2005)

     E-mail address:
     BILLARD, not reachable any more ;
     CALISTE, damien P caliste AT cea P fr.

     This software is a computer program whose purpose is to visualize atomic
     configurations in 3D.

     This software is governed by the CeCILL  license under French law and
     abiding by the rules of distribution of free software.  You can  use, 
     modify and/ or redistribute the software under the terms of the CeCILL
     license as circulated by CEA, CNRS and INRIA at the following URL
     "http://www.cecill.info". 

     The fact that you are presently reading this means that you have had
     knowledge of the CeCILL license and that you accept its terms. You can
     find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef FOG_H
#define FOG_H

#include <visu_extension.h>
#include <visu_tools.h>

#include <openGLFunctions/view.h>

void visu_gl_ext_fog_init();

gboolean visu_gl_ext_fog_setValues(float rgba[4], int mask);
gboolean visu_gl_ext_fog_setOn(gboolean value);
gboolean visu_gl_ext_fog_setUseSpecificColor(gboolean value);
/**
 * VISU_GL_EXT_FOG_MASK_START:
 *
 * Value used by the second parameter of setFogStartEndValues() to
 * specified the value that must be changed. This actually changes
 * the fog_start value.
 */
#define VISU_GL_EXT_FOG_MASK_START (1 << 0)
/**
 * VISU_GL_EXT_FOG_MASK_END:
 *
 * Value used by the second parameter of setFogStartEndValues() to
 * specified the value that must be changed. This actually changes
 * the fog_end value.
 */
#define VISU_GL_EXT_FOG_MASK_END (1 << 1)
gboolean visu_gl_ext_fog_setStartEndValues(float startEnd[2], int mask);

/**
 * visu_gl_ext_fog_getValues:
 * @rgba: a storage for four values.
 *
 * Read the RGBA value of the specific fog colour (in [0;1]).
 */
void visu_gl_ext_fog_getValues(float rgba[4]);
/**
 * visu_gl_ext_fog_getOn:
 *
 * Read if fog is used or not.
 *
 * Returns: TRUE if the fog is rendered, FALSE otherwise.
 */
gboolean visu_gl_ext_fog_getOn();
/**
 * visu_gl_ext_fog_getUseSpecificColor:
 *
 * Read if fog uses a specific colour or not.
 *
 * Returns: TRUE if the fog uses its own color or FALSE if it uses
 * the color of the background.
 */
gboolean visu_gl_ext_fog_getUseSpecificColor();
/**
 * visu_gl_ext_fog_getStart:
 *
 * Read the starting value of the fog (in [0;1]).
 *
 * Returns: the position where the fog starts.
 */
float visu_gl_ext_fog_getStart();
/**
 * visu_gl_ext_fog_getEnd:
 *
 * Read the ending value of the fog (in [0;1]).
 *
 * Returns: the position where the fog ends.
 */
float visu_gl_ext_fog_getEnd();
void visu_gl_ext_fog_create(VisuGlView *view, VisuBox *box);
/**
 * visu_gl_ext_fog_create_color:
 *
 * Call the OpenGL routines that change the colour of the fog.
 */
void visu_gl_ext_fog_create_color();

/**
 * VISU_TYPE_GL_EXT_BG:
 *
 * return the type of #VisuGlExtBg.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_BG	     (visu_gl_ext_bg_get_type ())
/**
 * VISU_GL_EXT_BG:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtBg type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_BG(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_BG, VisuGlExtBg))
/**
 * VISU_GL_EXT_BG_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtBgClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_BG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_BG, VisuGlExtBgClass))
/**
 * VISU_IS_GL_EXT_BG:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtBg object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_BG(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_BG))
/**
 * VISU_IS_GL_EXT_BG_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtBgClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_BG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_BG))
/**
 * VISU_GL_EXT_BG_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_BG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_BG, VisuGlExtBgClass))

typedef struct _VisuGlExtBg        VisuGlExtBg;
typedef struct _VisuGlExtBgPrivate VisuGlExtBgPrivate;
typedef struct _VisuGlExtBgClass   VisuGlExtBgClass;

struct _VisuGlExtBg
{
  VisuGlExt parent;

  VisuGlExtBgPrivate *priv;
};

struct _VisuGlExtBgClass
{
  VisuGlExtClass parent;
};

/**
 * VISU_GL_EXT_BG_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_BG_ID "Background"

/**
 * visu_gl_ext_bg_get_type:
 *
 * This method returns the type of #VisuGlExtBg, use
 * VISU_TYPE_GL_EXT_BG instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtBg.
 */
GType visu_gl_ext_bg_get_type(void);

VisuGlExtBg* visu_gl_ext_bg_new(const gchar *name);

int visu_gl_ext_bg_setRGBA(VisuGlExtBg *bg, float rgba[3], int mask);
void visu_gl_ext_bg_getRGBA(VisuGlExtBg *bg, float rgba[4]);
void visu_gl_ext_bg_setImage(VisuGlExtBg *bg,
                             const guchar *imageData, guint width, guint height,
                             gboolean alpha, const gchar *title, gboolean fit);
gboolean visu_gl_ext_bg_setFollowCamera(VisuGlExtBg *bg, gboolean follow, float zoomInit,
                                       float xs, float ys);
gboolean visu_gl_ext_bg_setCamera(VisuGlExtBg *bg, float zoom, float xs, float ys);

VisuGlExtBg* visu_gl_ext_bg_getDefault();

gboolean visu_gl_ext_bg_setGlView(VisuGlExtBg *bg, VisuGlView *view);
void visu_gl_ext_bg_draw(VisuGlExtBg *bg);

#endif
