/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2011-2013)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2011-2013)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "forces.h"

#include <GL/gl.h>

#include <visu_object.h>
#include <renderingMethods/renderingAtomic.h>

#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

#define FLAG_RESOURCE_FORCES_USED   "forces_are_on"
#define DESC_RESOURCE_FORCES_USED   "Control if the forces are drawn when available ; boolean (0 or 1)"
static gboolean RESOURCE_FORCES_USED_DEFAULT = FALSE;
#define FLAG_RESOURCE_FORCES_SCALE   "forces_scale"
#define DESC_RESOURCE_FORCES_SCALE   "Scaling factor (or automatic) for the force rendering ; float (-1 for auto or a positive value)"
static float RESOURCE_FORCES_SCALE_DEFAULT = -1.f;
static void exportResourceForces(GString *data, VisuData *dataObj, VisuGlView *view);


/**
 * SECTION:forces
 * @short_description: Draw arrows at each node to represent forces.
 *
 * <para>A specialised #VisuGlExtNodeVectors to represent forces on nodes.</para>
 */

/**
 * VisuGlExtForcesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtForcesClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtForces:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtForcesPrivate:
 *
 * Private fields for #VisuGlExtForces objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtForcesPrivate
{
  gboolean dispose_has_run;
};

static VisuGlExtForces *defaultForces = NULL;

static void visu_gl_ext_forces_finalize(GObject* obj);
static void visu_gl_ext_forces_dispose(GObject* obj);

/* Local callbacks. */
static gboolean onForcesHook(GSignalInvocationHint *ihint, guint nvalues,
                             const GValue *param_values, gpointer data);
static void onEntryUsed(VisuGlExtForces *forces, gchar *key, VisuObject *obj);
static void onEntryNorm(VisuGlExtForces *forces, gchar *key, VisuObject *obj);
static void onDataRendered(VisuObject *object, VisuData *dataObj,
                           VisuGlView *view, gpointer data);

G_DEFINE_TYPE(VisuGlExtForces, visu_gl_ext_forces, VISU_TYPE_GL_EXT_NODE_VECTORS)

static void visu_gl_ext_forces_class_init(VisuGlExtForcesClass *klass)
{
  float rg[2] = {-G_MAXFLOAT, G_MAXFLOAT};
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Visu GlExt Forces: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Resources for forces extensions. */
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_FORCES_USED,
                                                   DESC_RESOURCE_FORCES_USED,
                                                   &RESOURCE_FORCES_USED_DEFAULT);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCE_FORCES_SCALE,
                                                      DESC_RESOURCE_FORCES_SCALE,
                                                      1, &RESOURCE_FORCES_SCALE_DEFAULT, rg);
  visu_config_file_entry_setVersion(resourceEntry, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourceForces);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_forces_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_forces_finalize;
}

static void visu_gl_ext_forces_init(VisuGlExtForces *obj)
{
  DBG_fprintf(stderr, "Visu GlExt Forces: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtForcesPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_FORCES_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);
  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_FORCES_SCALE,
                          G_CALLBACK(onEntryNorm), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_gl_ext_forces_dispose(GObject* obj)
{
  VisuGlExtForces *vect;

  DBG_fprintf(stderr, "Visu GlExt Forces: dispose object %p.\n", (gpointer)obj);

  vect = VISU_GL_EXT_FORCES(obj);
  if (vect->priv->dispose_has_run)
    return;
  vect->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_forces_parent_class)->dispose(obj);
}
static void visu_gl_ext_forces_finalize(GObject* obj)
{
  VisuGlExtForces *vect;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu GlExt Forces: finalize object %p.\n", (gpointer)obj);

  vect = VISU_GL_EXT_FORCES(obj);

  /* Free privs elements. */
  if (vect->priv)
    {
      DBG_fprintf(stderr, "Visu GlExt Forces: free private legend.\n");
      g_free(vect->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu GlExt Forces: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_forces_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu GlExt Forces: freeing ... OK.\n");
}

/**
 * visu_gl_ext_forces_new:
 * @name: (allow-none): the name to give to the extension.
 *
 * Creates a new #VisuGlExt to draw forces.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtForces* visu_gl_ext_forces_new(const gchar *name)
{
  char *name_ = "Forces";
  char *description = _("Draw forces with vectors.");
  VisuGlExtNodeVectors *forces;

  DBG_fprintf(stderr,"Visu GlExt Forces: new object.\n");
  forces = VISU_GL_EXT_NODE_VECTORS(g_object_new(VISU_TYPE_GL_EXT_FORCES,
                                                 "name", (name)?name:name_, "label", _(name),
                                                 "description", description, "nGlObj", 1,
                                                 "propId", VISU_RENDERING_ATOMIC_FORCES,
                                                 NULL));
  visu_gl_ext_node_vectors_setTranslation(forces, 1.1f);
  visu_gl_ext_node_vectors_setRenderedSize(forces, -2.f);
  visu_gl_ext_node_vectors_setNormalisation(forces, RESOURCE_FORCES_SCALE_DEFAULT);
  
  g_signal_add_emission_hook(g_signal_lookup("ForcesChanged",
                                             VISU_TYPE_RENDERING_ATOMIC),
                             0, onForcesHook, (gpointer)forces, (GDestroyNotify)0);
  
  return VISU_GL_EXT_FORCES(forces);
}
/**
 * visu_gl_ext_forces_getDefault:
 *
 * V_Sim internally uses a default #VisuGlExtForces object to
 * represent forces.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the default #VisuGlExtForces object.
 **/
VisuGlExtForces* visu_gl_ext_forces_getDefault(void)
{
  if (!defaultForces)
    {
      defaultForces = visu_gl_ext_forces_new(NULL);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultForces), RESOURCE_FORCES_USED_DEFAULT);
      g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                       G_CALLBACK(onDataRendered), (gpointer)defaultForces);
    }
  return defaultForces;
}


/* Callbacks. */
static gboolean onForcesHook(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                             const GValue *param_values, gpointer data)
{
  VisuGlExtNodeVectors *vect = VISU_GL_EXT_NODE_VECTORS(data);

  DBG_fprintf(stderr, "Visu GlExt Forces: new forces available.\n");
  if (visu_gl_ext_node_vectors_getData(vect) ==
      VISU_DATA(g_value_get_object(param_values + 1)))
    visu_gl_ext_node_vectors_draw(vect);

  return TRUE;
}
static void onDataRendered(VisuObject *object _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data)
{
  visu_gl_ext_node_vectors_setData(VISU_GL_EXT_NODE_VECTORS(data), dataObj);
  visu_gl_ext_node_vectors_draw(VISU_GL_EXT_NODE_VECTORS(data));
}
static void onEntryUsed(VisuGlExtForces *forces, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(forces), RESOURCE_FORCES_USED_DEFAULT);
}
static void onEntryNorm(VisuGlExtForces *forces, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_node_vectors_setNormalisation(VISU_GL_EXT_NODE_VECTORS(forces),
                                            RESOURCE_FORCES_SCALE_DEFAULT);
}

/* Resources. */
static void exportResourceForces(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_FORCES_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FORCES_USED, NULL,
                               "%d", RESOURCE_FORCES_USED_DEFAULT);
  visu_config_file_exportComment(data, DESC_RESOURCE_FORCES_SCALE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FORCES_SCALE, NULL,
                               "%f", RESOURCE_FORCES_SCALE_DEFAULT);
  visu_config_file_exportComment(data, "");
}
