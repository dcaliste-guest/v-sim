/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "frame.h"

#include <string.h>

#include <GL/gl.h>

#include <coreTools/toolColor.h>
#include <openGLFunctions/text.h>

/**
 * SECTION:frame
 * @short_description: Draw a frame with the representation of a color frame.
 *
 * <para>This extension draws a frame on top of the rendering area
 * with a color frame. One can setup printed values and draw
 * additional marks inside the frame.</para>
 *
 * Since: 3.7
 */

/**
 * VisuGlExtFrameClass:
 * @parent: the parent class;
 * @draw: the draw method for the content of this frame.
 *
 * A short way to identify #_VisuGlExtFrameClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtFrame:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtFramePrivate:
 *
 * Private fields for #VisuGlExtFrame objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtFramePrivate
{
  gboolean dispose_has_run;

  /* Rendering parameters. */
  guint requisition[2];
  float scale;
  float xpos, ypos, xmargin, ymargin, xpad, ypad;
  float bgRGBA[4];
  gchar *title;

  /* Object dependencies. */
  VisuGlView *view;
  gulong widthHeight_signal;
};
static int refMask[4] = {TOOL_COLOR_MASK_R, TOOL_COLOR_MASK_G,
                         TOOL_COLOR_MASK_B, TOOL_COLOR_MASK_A};
static float bgRGBADefault[4] = {1.f, 1.f, 1.f, 0.4f};
static float fontRGBDefault[4] = {0.f, 0.f, 0.f};
static float xmarginDefault = 10.f, ymarginDefault = 10.f;
static float xpadDefault = 5.f, ypadDefault = 5.f;

static void visu_gl_ext_frame_finalize(GObject* obj);
static void visu_gl_ext_frame_dispose(GObject* obj);

static void onViewChanged(VisuGlView *view, gpointer data);

G_DEFINE_TYPE(VisuGlExtFrame, visu_gl_ext_frame, VISU_TYPE_GL_EXT)

static void visu_gl_ext_frame_class_init(VisuGlExtFrameClass *klass)
{
  DBG_fprintf(stderr, "Extension Frame: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* DBG_fprintf(stderr, "                - adding new resources ;\n"); */
  klass->draw = NULL;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_frame_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_frame_finalize;
}

static void visu_gl_ext_frame_init(VisuGlExtFrame *obj)
{
  DBG_fprintf(stderr, "Extension Frame: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->isBuilt    = FALSE;
  obj->width      = 0;
  obj->height     = 0;
  obj->fontRGB[0] = fontRGBDefault[0];
  obj->fontRGB[1] = fontRGBDefault[1];
  obj->fontRGB[2] = fontRGBDefault[2];
  
  obj->priv = g_malloc(sizeof(VisuGlExtFramePrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->requisition[0] = 0;
  obj->priv->requisition[1] = 0;
  obj->priv->scale      = 1.f;
  obj->priv->xpos       = 0.f;
  obj->priv->ypos       = 0.f;
  obj->priv->xmargin    = xmarginDefault;
  obj->priv->ymargin    = ymarginDefault;
  obj->priv->xpad       = xpadDefault;
  obj->priv->ypad       = ypadDefault;
  obj->priv->bgRGBA[0]  = bgRGBADefault[0];
  obj->priv->bgRGBA[1]  = bgRGBADefault[1];
  obj->priv->bgRGBA[2]  = bgRGBADefault[2];
  obj->priv->bgRGBA[3]  = bgRGBADefault[3];
  obj->priv->title      = g_strdup("");
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_frame_dispose(GObject* obj)
{
  VisuGlExtFrame *frame;

  DBG_fprintf(stderr, "Extension Frame: dispose object %p.\n", (gpointer)obj);

  frame = VISU_GL_EXT_FRAME(obj);
  if (frame->priv->dispose_has_run)
    return;
  frame->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_frame_setGlView(frame, (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_frame_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_frame_finalize(GObject* obj)
{
  VisuGlExtFrame *frame;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Frame: finalize object %p.\n", (gpointer)obj);

  frame = VISU_GL_EXT_FRAME(obj);
  /* Free privs elements. */
  if (frame->priv)
    {
      DBG_fprintf(stderr, "Extension Frame: free private frame.\n");
      g_free(frame->priv->title);
      g_free(frame->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Frame: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_frame_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Frame: freeing ... OK.\n");
}

/**
 * visu_gl_ext_frame_setGlView:
 * @frame: The #VisuGlExtFrame to attached to.
 * @view: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the frame.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_frame_setGlView(VisuGlExtFrame *frame, VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (frame->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(frame->priv->view), frame->priv->widthHeight_signal);
      g_object_unref(frame->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      frame->priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onViewChanged), (gpointer)frame);
    }
  else
    frame->priv->widthHeight_signal = 0;
  frame->priv->view = view;

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
/**
 * visu_gl_ext_frame_setPosition:
 * @frame: the #VisuGlExtFrame object to modify.
 * @xpos: the reduced y position (1 to the left).
 * @ypos: the reduced y position (1 to the bottom).
 *
 * Change the position of the frame representation.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_frame_setPosition(VisuGlExtFrame *frame, float xpos, float ypos)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (xpos == frame->priv->xpos && ypos == frame->priv->ypos)
    return FALSE;
  frame->priv->xpos = CLAMP(xpos, 0.f, 1.f);
  frame->priv->ypos = CLAMP(ypos, 0.f, 1.f);

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
#define SET_RGBA_I(S, I, V, M) {if (M & refMask[I] && S[I] != V[I]) \
      {S[I] = V[I]; diff = TRUE;}}
#define SET_RGBA(S, V, M) {SET_RGBA_I(S, 0, V, M);                      \
    SET_RGBA_I(S, 1, V, M); SET_RGBA_I(S, 2, V, M); SET_RGBA_I(S, 3, V, M); }
#define SET_RGB(S, V, M) {SET_RGBA_I(S, 0, V, M);                      \
    SET_RGBA_I(S, 1, V, M); SET_RGBA_I(S, 2, V, M); }
/**
 * visu_gl_ext_frame_setBgRGBA:
 * @frame: the #VisuGlExtFrame to update.
 * @rgba: (array fixed-size=4): a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue color and the alpha channel. Only values
 * specified by the @mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_A or a combinaison to indicate
 * what values in the @rgba array must be taken into account.
 *
 * Change the colour to represent the background of the frame.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called.
 */
gboolean visu_gl_ext_frame_setBgRGBA(VisuGlExtFrame *frame, float rgba[4], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);
    
  SET_RGBA(frame->priv->bgRGBA, rgba, mask);
  if (!diff)
    return FALSE;

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
/**
 * visu_gl_ext_frame_setFontRGB:
 * @frame: the #VisuGlExtFrame to update.
 * @rgb: (array fixed-size=3): a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue color. Only values specified by the @mask
 * are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B or a combinaison to indicate what values in the
 * @rgb array must be taken into account.
 *
 * Change the colour to represent the font of the frame.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called.
 */
gboolean visu_gl_ext_frame_setFontRGB(VisuGlExtFrame *frame, float rgb[3], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);
    
  SET_RGB(frame->fontRGB, rgb, mask);
  if (!diff)
    return FALSE;

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
/**
 * visu_gl_ext_frame_setScale:
 * @frame: the #VisuGlExtFrame to update.
 * @scale: a positive value.
 *
 * Change the zoom level for the rendering of the legend.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called.
 **/
gboolean visu_gl_ext_frame_setScale(VisuGlExtFrame *frame, float scale)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (frame->priv->scale == scale)
    return FALSE;

  frame->priv->scale = CLAMP(scale, 0.01f, 10.f);

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
/**
 * visu_gl_ext_frame_setTitle:
 * @frame: the #VisuGlExtFrame object to modify.
 * @title: a title.
 *
 * Change the title of the box legend.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_frame_setTitle(VisuGlExtFrame *frame, const gchar *title)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame) && title, FALSE);

  if (!strcmp(title, frame->priv->title))
    return FALSE;
  g_free(frame->priv->title);
  frame->priv->title = g_strdup(title);

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}
/**
 * visu_gl_ext_frame_setRequisition:
 * @frame: the #VisuGlExtFrame object to modify.
 * @width: the desired width.
 * @height: the desired height.
 *
 * Set the size of the frame in pixels. Use
 * visu_gl_ext_frame_setScale() to adjust the size if necessary.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_frame_setRequisition(VisuGlExtFrame *frame, guint width, guint height)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (frame->priv->requisition[0] == width &&
      frame->priv->requisition[1] == height)
    return FALSE;

  frame->priv->requisition[0] = width;
  frame->priv->requisition[1] = height;

  frame->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(frame));
}

/**
 * visu_gl_ext_frame_getScale:
 * @frame: a #VisuGlExtFrame object.
 *
 * Frames are rendered with a scaling factor of 1. by default.
 *
 * Since: 3.7
 *
 * Returns: the scaling factor used to represent frames.
 **/
float visu_gl_ext_frame_getScale(VisuGlExtFrame *frame)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), 1.f);

  return frame->priv->scale;
}
/**
 * visu_gl_ext_frame_getPosition:
 * @frame: the #VisuGlExtFrame object to inquire.
 * @xpos: (out) (allow-none): a location to store the x position.
 * @ypos: (out) (allow-none): a location to store the y position.
 *
 * Inquire the position of the representation of the frame.
 *
 * Since: 3.7
 **/
void visu_gl_ext_frame_getPosition(VisuGlExtFrame *frame, float *xpos, float *ypos)
{
  g_return_if_fail(VISU_IS_GL_EXT_FRAME(frame));

  if (xpos)
    *xpos = frame->priv->xpos;
  if (ypos)
    *ypos = frame->priv->ypos;
}

static void onViewChanged(VisuGlView *view _U_, gpointer data)
{
  VISU_GL_EXT_FRAME(data)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(data));
}

/**
 * visu_gl_ext_frame_draw:
 * @frame: a #VisuGlExtFrame object.
 *
 * Render the@frame and its child.
 *
 * Since: 3.7
 */
void visu_gl_ext_frame_draw(VisuGlExtFrame *frame)
{
  int viewport[4];
  float requisition[2];
  VisuGlExtFrameClass *klass;
  int xpos, ypos;

  g_return_if_fail(VISU_IS_GL_EXT_FRAME(frame));

  /* Nothing to draw; */
  if(!visu_gl_ext_getActive(VISU_GL_EXT(frame)) || frame->isBuilt)
    return;

  DBG_fprintf(stderr, "Visu GlExt Frame: drawing the extension '%p'.\n", (gpointer)frame);

  visu_gl_text_initFontList();

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(frame)), GL_COMPILE);

  /* We get the size of the current viewport. */
  glGetIntegerv(GL_VIEWPORT, viewport);
  DBG_fprintf(stderr, "Visu GlExt Frame: current viewport is %dx%d.\n",
              viewport[2], viewport[3]);

  /* We change the viewport. */
  DBG_fprintf(stderr, "Visu GlExt Frame: requisition is %dx%d.\n",
              frame->priv->requisition[0], frame->priv->requisition[1]);
  requisition[0] = (float)frame->priv->requisition[0];
  requisition[1] = (float)frame->priv->requisition[1] +
    ((frame->priv->title[0])?(frame->priv->ypad + 12.f):0.f);
  DBG_fprintf(stderr, "Visu GlExt Frame: requisition is %gx%g.\n",
              requisition[0], requisition[1]);
  frame->width = MIN((guint)(requisition[0] * frame->priv->scale),
                     viewport[2] - (guint)(2.f * frame->priv->xmargin));
  frame->height = MIN((guint)(requisition[1] * frame->priv->scale),
                      viewport[3] - (guint)(2.f * frame->priv->ymargin));
  xpos = frame->priv->xmargin +
    ((float)viewport[2] - 2.f * frame->priv->xmargin - frame->width) * frame->priv->xpos;
  ypos = frame->priv->ymargin +
    ((float)viewport[3] - 2.f * frame->priv->ymargin - frame->height) * frame->priv->ypos;
  glViewport(xpos, ypos, frame->width, frame->height);

  glDisable(GL_FOG);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  /* glDisable(GL_ALPHA_TEST); */
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  /* We change the projection for a 2D one. */
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0., (float)frame->width, 0., (float)frame->height, -50., +50.);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  /* We draw a big transparent square to do the back. */
  if (frame->priv->bgRGBA[3] > 0.f)
    {
      glColor4fv(frame->priv->bgRGBA);
      glRecti(0, 0, frame->width, frame->height);
    }

  /* We draw the title, if any. */
  if (frame->priv->title[0])
    {
      glColor3fv(frame->fontRGB);
      glRasterPos2f(frame->priv->xpad * frame->priv->scale,
                    frame->height - (frame->priv->ypad + 12.) * frame->priv->scale);
      visu_gl_text_drawChars(frame->priv->title, VISU_GL_TEXT_NORMAL); 
    }

  /* We change again the viewport to adjust to padding. */
  xpos += frame->priv->xpad * frame->priv->scale;
  ypos += frame->priv->ypad * frame->priv->scale;
  frame->width -= 2.f * frame->priv->xpad * frame->priv->scale;
  frame->height -= 2.f * frame->priv->ypad * frame->priv->scale;
  if (frame->priv->title[0])
    frame->height -= (frame->priv->ypad + 12.) * frame->priv->scale;
  DBG_fprintf(stderr, "Visu GlExt Frame: frame actual size is %dx%d.\n",
              frame->width, frame->height);
  glViewport(xpos, ypos, frame->width + frame->priv->scale,
             frame->height + frame->priv->scale);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0., (float)(frame->width + frame->priv->scale),
          0., (float)(frame->height + frame->priv->scale), -50., +50.);  
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  
  klass = VISU_GL_EXT_FRAME_GET_CLASS(frame);
  if (klass->draw)
    klass->draw(frame);

  /* We change the projection back. */
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  /* We set viewport back. */
  DBG_fprintf(stderr, "Visu GlExt Frame: put back viewport at %dx%d.\n",
              viewport[2], viewport[3]);
  glViewport(0, 0, viewport[2], viewport[3]);

  glEndList();

  frame->isBuilt = TRUE;
}
