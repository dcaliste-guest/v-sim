/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef FRAME_H
#define FRAME_H

#include <visu_extension.h>
#include <visu_nodes.h>
#include <openGLFunctions/view.h>

/**
 * VISU_TYPE_GL_EXT_FRAME:
 *
 * return the type of #VisuGlExtFrame.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_FRAME	     (visu_gl_ext_frame_get_type ())
/**
 * VISU_GL_EXT_FRAME:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtFrame type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_FRAME(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_FRAME, VisuGlExtFrame))
/**
 * VISU_GL_EXT_FRAME_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtFrameClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_FRAME_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_FRAME, VisuGlExtFrameClass))
/**
 * VISU_IS_GL_EXT_FRAME:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtFrame object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_FRAME(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_FRAME))
/**
 * VISU_IS_GL_EXT_FRAME_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtFrameClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_FRAME_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_FRAME))
/**
 * VISU_GL_EXT_FRAME_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_FRAME_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_FRAME, VisuGlExtFrameClass))

typedef struct _VisuGlExtFrame        VisuGlExtFrame;
typedef struct _VisuGlExtFramePrivate VisuGlExtFramePrivate;
typedef struct _VisuGlExtFrameClass   VisuGlExtFrameClass;

struct _VisuGlExtFrame
{
  VisuGlExt parent;

  gboolean isBuilt;
  guint width, height;
  float fontRGB[3];

  VisuGlExtFramePrivate *priv;
};

struct _VisuGlExtFrameClass
{
  VisuGlExtClass parent;

  void     (*draw)     (VisuGlExtFrame* frame);
};

/**
 * visu_gl_ext_frame_get_type:
 *
 * This method returns the type of #VisuGlExtFrame, use
 * VISU_TYPE_GL_EXT_FRAME instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtFrame.
 */
GType visu_gl_ext_frame_get_type(void);

gboolean visu_gl_ext_frame_setGlView(VisuGlExtFrame *frame, VisuGlView *view);
gboolean visu_gl_ext_frame_setPosition(VisuGlExtFrame *frame, float xpos, float ypos);
gboolean visu_gl_ext_frame_setBgRGBA(VisuGlExtFrame *frame, float rgba[4], int mask);
gboolean visu_gl_ext_frame_setFontRGB(VisuGlExtFrame *frame, float rgb[3], int mask);
gboolean visu_gl_ext_frame_setScale(VisuGlExtFrame *frame, float scale);
gboolean visu_gl_ext_frame_setTitle(VisuGlExtFrame *frame, const gchar *title);
gboolean visu_gl_ext_frame_setRequisition(VisuGlExtFrame *frame, guint width, guint height);

void visu_gl_ext_frame_getPosition(VisuGlExtFrame *frame, float *xpos, float *ypos);
float visu_gl_ext_frame_getScale(VisuGlExtFrame *frame);

void visu_gl_ext_frame_draw(VisuGlExtFrame *frame);

#endif
