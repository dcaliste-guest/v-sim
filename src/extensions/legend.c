/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "legend.h"

#include <visu_object.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/text.h>
#include <coreTools/toolConfigFile.h>
#include <coreTools/toolColor.h>

/**
 * SECTION:legend
 * @short_description: Draw a frame with the representation of each
 * atom species, its name and the number of elements.
 *
 * <para>This extension draws a frame on top of the rendering area with an
 * item per #VisuElement currently rendered. For each #VisuElement, a
 * small representation of its OpenGL shape is drawn, its label is
 * printed and the number of #VisuNode associated to this
 * element.</para>
 * <para>This extension defines one resource entry labeled
 * "legend_is_on" to control if the legend is printed or not.</para>
 *
 * Since: 3.5
 */

#define FLAG_RESOURCE_LEGEND_USED   "legend_is_on"
#define DESC_RESOURCE_LEGEND_USED   "Control if the legend is drawn ; boolean (0 or 1)"
static gboolean DEFAULT_LEGEND_USED = FALSE;

/**
 * VisuGlExtLegendClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtLegendClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtLegend:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtLegendPrivate:
 *
 * Private fields for #VisuGlExtLegend objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtLegendPrivate
{
  gboolean dispose_has_run;

  /* Legend definition. */
  VisuNodeArray *nodes;
  gulong popInc_signal, popDec_signal, eleSize_hook;
};
static VisuGlExtLegend* defaultLegend;

static void visu_gl_ext_legend_finalize(GObject* obj);
static void visu_gl_ext_legend_dispose(GObject* obj);
static void visu_gl_ext_legend_draw(VisuGlExtFrame *legend);
static void visu_gl_ext_legend_rebuild(VisuGlExt *ext);

/* Local callbacks. */
static void onNodePopulationChanged(VisuNodeArray *array, int *nodes, gpointer data);
static gboolean onElementSize(GSignalInvocationHint *ihint, guint nvalues,
                              const GValue *param_values, gpointer data);
static void onEntryUsed(VisuGlExtLegend *lg, gchar *key, VisuObject *obj);

/* Local routines. */
static void exportResources(GString *data, VisuData *dataObj, VisuGlView *view);

G_DEFINE_TYPE(VisuGlExtLegend, visu_gl_ext_legend, VISU_TYPE_GL_EXT_FRAME)

static void visu_gl_ext_legend_class_init(VisuGlExtLegendClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Extension Legend: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_LEGEND_USED,
                                                   DESC_RESOURCE_LEGEND_USED,
                                                   &DEFAULT_LEGEND_USED);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResources);

  defaultLegend = (VisuGlExtLegend*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_legend_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_legend_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_legend_rebuild;
  VISU_GL_EXT_FRAME_CLASS(klass)->draw = visu_gl_ext_legend_draw;
}

static void visu_gl_ext_legend_init(VisuGlExtLegend *obj)
{
  DBG_fprintf(stderr, "Extension Legend: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtLegendPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->nodes         = (VisuNodeArray*)0;
  obj->priv->popInc_signal = 0;
  obj->priv->popDec_signal = 0;

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_LEGEND_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);

  obj->priv->eleSize_hook =
    g_signal_add_emission_hook(g_signal_lookup("elementSizeChanged",
                                               VISU_TYPE_RENDERING),
                               0, onElementSize, (gpointer)obj, (GDestroyNotify)0);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_legend_dispose(GObject* obj)
{
  VisuGlExtLegend *legend;

  DBG_fprintf(stderr, "Extension Legend: dispose object %p.\n", (gpointer)obj);

  legend = VISU_GL_EXT_LEGEND(obj);
  if (legend->priv->dispose_has_run)
    return;
  legend->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_legend_setNodes(legend, (VisuNodeArray*)0);
  g_signal_remove_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                                legend->priv->eleSize_hook);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_legend_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_legend_finalize(GObject* obj)
{
  VisuGlExtLegend *legend;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Legend: finalize object %p.\n", (gpointer)obj);

  legend = VISU_GL_EXT_LEGEND(obj);

  /* Free privs elements. */
  if (legend->priv)
    {
      DBG_fprintf(stderr, "Extension Legend: free private legend.\n");
      g_free(legend->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Legend: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_legend_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Legend: freeing ... OK.\n");
}

/**
 * visu_gl_ext_legend_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_LEGEND_ID).
 *
 * Creates a new #VisuGlExt to draw a legend.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtLegend* visu_gl_ext_legend_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_LEGEND_ID;
  char *description = _("Draw the name and the shape of available elements on screen.");
  VisuGlExt *legend;
#define LEGEND_HEIGHT 30

  DBG_fprintf(stderr,"Extension Legend: new object.\n");
  
  legend = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_LEGEND,
                                       "name", (name)?name:name_, "label", _(name),
                                       "description", description,
                                       "nGlObj", 1, NULL));
  visu_gl_ext_setSaveState(legend, TRUE);
  visu_gl_ext_setPriority(legend, VISU_GL_EXT_PRIORITY_LAST);
  visu_gl_ext_frame_setPosition(VISU_GL_EXT_FRAME(legend), 0.f, 1.f);
  visu_gl_ext_frame_setRequisition(VISU_GL_EXT_FRAME(legend), G_MAXUINT / 2, LEGEND_HEIGHT);

  return VISU_GL_EXT_LEGEND(legend);
}
/**
 * visu_gl_ext_legend_setNodes:
 * @legend: The #VisuGlExtLegend to attached to.
 * @nodes: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the legend to get the
 * node population also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_frame_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_legend_setNodes(VisuGlExtLegend *legend, VisuNodeArray *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LEGEND(legend), FALSE);

  if (legend->priv->nodes)
    {
      g_signal_handler_disconnect(G_OBJECT(legend->priv->nodes), legend->priv->popInc_signal);
      g_signal_handler_disconnect(G_OBJECT(legend->priv->nodes), legend->priv->popDec_signal);
      g_object_unref(legend->priv->nodes);
    }
  if (nodes)
    {
      g_object_ref(nodes);
      legend->priv->popInc_signal =
        g_signal_connect(G_OBJECT(nodes), "PopulationIncrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)legend);
      legend->priv->popDec_signal =
        g_signal_connect(G_OBJECT(nodes), "PopulationDecrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)legend);
    }
  else
    {
      legend->priv->popInc_signal = 0;
      legend->priv->popDec_signal = 0;
    }
  legend->priv->nodes = nodes;

  VISU_GL_EXT_FRAME(legend)->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(legend));
}
/**
 * visu_gl_ext_legend_getDefault:
 *
 * V_Sim is using a default legend object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtLegend object used by default.
 **/
VisuGlExtLegend* visu_gl_ext_legend_getDefault()
{
  if (!defaultLegend)
    defaultLegend = visu_gl_ext_legend_new((gchar*)0);
  return defaultLegend;
}
static void onEntryUsed(VisuGlExtLegend *lg, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(lg), DEFAULT_LEGEND_USED);
}
static void onNodePopulationChanged(VisuNodeArray *array _U_, int *nodes _U_,
				    gpointer data)
{
  VISU_GL_EXT_FRAME(data)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(data));
}
static gboolean onElementSize(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                              const GValue *param_values _U_, gpointer data)
{
  VISU_GL_EXT_FRAME(data)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(data));

  return TRUE;
}
static void visu_gl_ext_legend_rebuild(VisuGlExt *ext)
{
  visu_gl_text_rebuildFontList();
  VISU_GL_EXT_FRAME(ext)->isBuilt = FALSE;
  visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(ext));
}

static void visu_gl_ext_legend_draw(VisuGlExtFrame *frame)
{
  guint dw;
  guint i;
  float scale;
  GString *str;
  VisuNodeArrayIter iter;
  VisuRendering *method;
  VisuGlExtLegend *legend;

  g_return_if_fail(VISU_IS_GL_EXT_LEGEND(frame));
  legend = VISU_GL_EXT_LEGEND(frame);
  if (!legend->priv->nodes)
    return;

  method = visu_object_getRendering(VISU_OBJECT_INSTANCE);
  g_return_if_fail(method);

  /* We draw the legend. */
  str = g_string_new("");
  scale = 0.5f * frame->height / visu_node_array_getMaxElementSize(legend->priv->nodes);
  visu_node_array_iterNew(legend->priv->nodes, &iter);
  dw = MAX(frame->width / visu_node_array_getNElements(legend->priv->nodes, TRUE),
           frame->height + 10 + 60);
  for (visu_node_array_iterStart(legend->priv->nodes, &iter), i = 0; iter.element;
       visu_node_array_iterNextElement(legend->priv->nodes, &iter), i+= 1)
    {
      if (!visu_element_getPhysical(iter.element))
        {
          i -= 1;
          continue;
        }

      /* The element. */
      glEnable(GL_LIGHTING);
      glCallList(visu_element_getMaterialId(iter.element));
      glPushMatrix();
      glTranslated(i * dw + 5 + frame->height / 2,
		   frame->height / 2, 0.f);
      glRotated(45., 0, 0, 1);  
      glRotated(60., 0, 1, 0); 
      glScalef(scale, scale, scale);
      glCallList(visu_rendering_getElementGlId(method, iter.element));
      glPopMatrix();
      glDisable(GL_LIGHTING);

      /* The label. */
      glColor3fv(VISU_GL_EXT_FRAME(legend)->fontRGB);
      g_string_printf(str, "%s (%d)", iter.element->name, iter.nStoredNodes);
      glRasterPos2i(i * dw + 5 + frame->height + 5, (frame->height - 20) / 2 + 5);
      visu_gl_text_drawChars(str->str, VISU_GL_TEXT_SMALL);
    }
  g_string_free(str, TRUE);
}

static void exportResources(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  if (!defaultLegend)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_LEGEND_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEGEND_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultLegend)));
  visu_config_file_exportComment(data, "");
}
