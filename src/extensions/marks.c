/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <glib.h>
#include <GL/gl.h>
#include <math.h>
#include <string.h>

#include "marks.h"
#include <visu_object.h>
#include <visu_elements.h>
#include <visu_rendering.h>
#include <visu_configFile.h>
#include <visu_extension.h>
#include <opengl.h>
#include <openGLFunctions/view.h>
#include <openGLFunctions/text.h>
#include <openGLFunctions/objectList.h>
#include <openGLFunctions/interactive.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:marks
 * @short_description: Draw features on nodes, like measurement marks
 * or highlights.
 *
 * <para>#VisuGlExtMarks is used to store a set of mark on a list of
 * nodes. A mark can be a distance measurement, an angle measurement
 * or an highlight. The measurement marks are automatically updated by
 * listening to the #VisuInteractive::node-selection signal. On the
 * contrary, highlights are set, unset or toggled using
 * visu_gl_ext_marks_setHighlightedList().</para>
 * <para>In addition, #VisuGlExtMarks can be export to or loaded from an
 * XML file thanks to visu_gl_ext_marks_exportXMLFile() and visu_gl_ext_marks_parseXMLFile().</para>
 *
 * Since: 3.6
 */

typedef enum
  {
    MARK_BIG_SQUARE,
    MARK_SMALL_SQUARE,
    MARK_HIGHLIGHT,
    MARK_DISTANCE,
    MARK_ANGLE,
    MARK_LINE
  } VisuMarkType;

struct MarkInfo_struct
{
  /* Mark type. */
  VisuMarkType type;

  /* Id used to address the VisuNode when the
     pointer node1 has changed, for example when a new VisuData
     is loaded with the same geometry and we want to apply this mark. */
  guint idNode1;
  /* Idem for a second node. */
  guint idNode2;
  /* Idem for a third node. */
  guint idNode3;

  /* List of nodes */
  GList *nodes;
  guint size;
  GLfloat *coord;
  guint *nIds;
};

struct _VisuGlExtMarks
{
  VisuGlExt parent;
  gboolean dispose_has_run;
  VisuGlExt *extNode;

  /* A reference on the #VisuData and #VisuGlView it refers to. */
  VisuData *data;
  VisuGlView *view;
  gulong cameraAngles, cameraPosition, cameraZoom, cameraPersp;
  gulong nodePosition, nodeRender, nodeMaterial, nodePopulation;

  /* Interactive object to get measurement signals. */
  VisuInteractive *inter;
  gulong nodeSelection;

  /* Some global hooks. */
  gulong elementSize;

  /* A list of MarkInfo_struct elements. */
  GList *storedMarks;
  guint hasCameraMarks;
  gboolean drawValues;

  /* Miscellaneous. */
  float infoRange;
};

/**
 * VisuGlExtMarksClass:
 *
 * An opaque structure defining the class of a #VisuGlExtMarks objects.
 *
 * Since: 3.6
 */
struct _VisuGlExtMarksClass
{
  VisuGlExtClass parent_class;
};

enum
  {
    HIGHLIGHT_CHANGE_SIGNAL,
    MEASUREMENT_CHANGE_SIGNAL,
    NB_SIGNAL
  };
static guint signals[NB_SIGNAL];

/* Local variables. */
#define MARK_BIG_SQUARE_SIZE 8
#define MARK_SMALL_SQUARE_SIZE 4
static guchar markBigSquare[MARK_BIG_SQUARE_SIZE *
			    MARK_BIG_SQUARE_SIZE * 4];
static guchar markSmallSquare[MARK_SMALL_SQUARE_SIZE *
			      MARK_SMALL_SQUARE_SIZE * 4];
static float highlightFactor = 1.25f;

/* Local methods. */
static struct MarkInfo_struct* markNew(VisuMarkType type);
static void markFree(struct MarkInfo_struct *mark);
static void markRemove(VisuGlExtMarks *marks, GList *rmList);
static void marksDraw(VisuGlExtMarks *marks, int listType);
static void removeDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type);
static void addDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type);
static gboolean toggleDistance(VisuGlExtMarks *marks, guint nodeRefId,
			       guint nodeId, gboolean set);
static gboolean toggleAngle(VisuGlExtMarks *marks, guint nodeRefId,
			    guint nodeRef2Id, guint nodeId, gboolean set);
static gboolean toggleHighlight(VisuGlExtMarks *marks, guint nodeId,
				VisuGlExtMarksStatus status, gboolean *finalStatus);
static gboolean setInformation(VisuGlExtMarks *marks, guint node);
static void putMark(VisuData *data, VisuGlView *view, guint nodeRefId,
		    guint nodeId, VisuMarkType type);
static void drawMarkDot(VisuData *data, VisuGlView *view,
                        guint nodeId, VisuMarkType type);
static void drawMarkDistance(VisuData *data, guint nodeRefId,
			     guint nodeId, VisuMarkType type);
static void drawMarkAngle(VisuData *data, guint nodeRefId, guint nodeRef2Id,
			  guint nodeId, guint id);
static void drawMarkLine(VisuData *data _U_, GLfloat *coord, guint size);

static void visu_gl_ext_marks_dispose(GObject *obj);
static void visu_gl_ext_marks_finalize(GObject *obj);
static void visu_gl_ext_marks_rebuild(VisuGlExt *ext);

/* Local callbacks. */
static void updateListOnPopulationChange(VisuData *dataObj, int *nodes,
					 gpointer data);
static void updateListOnNodeChange(VisuData *dataObj, gpointer data);
static void onPositionChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void updateListOnCameraChange(VisuGlView *view, gpointer data);
static gboolean onNodeSelection(VisuInteractive *inter, VisuInteractivePick kind,
                                VisuNode *node1, VisuNode *node2, VisuNode *node3,
                                gpointer data);
static gboolean onNodeSizeHook(GSignalInvocationHint *ihint, guint nvalues,
                               const GValue *param_values, gpointer data);

/* V_Sim resources and paramaters. */
#define FLAG_RESOURCE_FACTOR "highlight_radiusFactor"
#define DESC_RESOURCE_FACTOR "Give the factor for the highlight radius ; one float (> 1.)"
static void exportResources(GString *data, VisuData *dataObj, VisuGlView *view);

G_DEFINE_TYPE (VisuGlExtMarks, visu_gl_ext_marks, VISU_TYPE_GL_EXT)

static void visu_gl_ext_marks_class_init(VisuGlExtMarksClass *class)
{
  int i;
  float rg[2] = {1.01f, G_MAXFLOAT};
  VisuConfigFileEntry *conf;

  G_OBJECT_CLASS(class)->dispose  = visu_gl_ext_marks_dispose;
  G_OBJECT_CLASS(class)->finalize = visu_gl_ext_marks_finalize;
  VISU_GL_EXT_CLASS(class)->rebuild = visu_gl_ext_marks_rebuild;

  DBG_fprintf(stderr, "Visu Marks: installing signals.\n");
  /**
   * VisuGlExtMarks::highlightChanged:
   * @marks: the object emitting the signal.
   * @lst: a list of node ids (starting from 0).
   *
   * The list of highlighted nodes has been modified.
   *
   * Since: 3.6
   */
  signals[HIGHLIGHT_CHANGE_SIGNAL] = 
    g_signal_new("highlightChanged", G_TYPE_FROM_CLASS(class),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE, 1, G_TYPE_POINTER);
  /**
   * VisuGlExtMarks::measurementChanged:
   * @marks: the object emitting the signal.
   *
   * The list of measurements has been changed.
   *
   * Since: 3.6
   */
  signals[MEASUREMENT_CHANGE_SIGNAL] = 
    g_signal_new("measurementChanged", G_TYPE_FROM_CLASS(class),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);

  /* Create the marks. */
  for (i = 0; i < MARK_BIG_SQUARE_SIZE * MARK_BIG_SQUARE_SIZE * 4 ; i++)
    markBigSquare[i] = 0xff;
  for (i = 0; i < MARK_SMALL_SQUARE_SIZE * MARK_SMALL_SQUARE_SIZE * 4; i++)
    markSmallSquare[i] = 0xff;

  conf = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                             FLAG_RESOURCE_FACTOR,
                                             DESC_RESOURCE_FACTOR,
                                             1, &highlightFactor, rg);
  visu_config_file_entry_setVersion(conf, 3.6f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResources);
}

static void visu_gl_ext_marks_init(VisuGlExtMarks *marks)
{
  DBG_fprintf(stderr, "Visu Marks: create a new object, %p.\n",
	      (gpointer)marks);

  marks->dispose_has_run = FALSE;

  marks->data               = (VisuData*)0;
  marks->view               = (VisuGlView*)0;
  marks->storedMarks        = (GList*)0;
  marks->hasCameraMarks     = 0;
  marks->drawValues         = TRUE;
  marks->infoRange          = 1.44f;
  marks->extNode            = (VisuGlExt*)0;
  marks->inter              = (VisuInteractive*)0;
  marks->nodeSelection      = 0;

  marks->elementSize = 
    g_signal_add_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                               0, onNodeSizeHook, (gpointer)marks, (GDestroyNotify)0);
}

static void visu_gl_ext_marks_dispose(GObject *obj)
{
  VisuGlExtMarks *marks;

  DBG_fprintf(stderr, "Visu Marks: dispose object %p.\n", (gpointer)obj);

  marks = VISU_GL_EXT_MARKS(obj);

  if (marks->dispose_has_run)
    return;
  marks->dispose_has_run = TRUE;

  DBG_fprintf(stderr, " | release VisuData.\n");
  visu_gl_ext_marks_setData(marks, (VisuData*)0);
  DBG_fprintf(stderr, " | release VisuGlView.\n");
  visu_gl_ext_marks_setGlView(marks, (VisuGlView*)0);
  DBG_fprintf(stderr, " | free internal VisuGlExt.\n");
  g_object_unref(G_OBJECT(marks->extNode));

  DBG_fprintf(stderr, " | unconnect signals.\n");
  visu_gl_ext_marks_setInteractive(marks, (VisuInteractive*)0);

  g_signal_remove_emission_hook(g_signal_lookup("elementSizeChanged", VISU_TYPE_RENDERING),
                                marks->elementSize);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, " | chain up.\n");
  G_OBJECT_CLASS(visu_gl_ext_marks_parent_class)->dispose(obj);
  DBG_fprintf(stderr, " | OK.\n");
}
static void visu_gl_ext_marks_finalize(GObject *obj)
{
  g_return_if_fail(obj);
  DBG_fprintf(stderr, "Visu Marks: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_marks_parent_class)->finalize(obj);
  DBG_fprintf(stderr, " | freeing ... OK.\n");
}

/**
 * visu_gl_ext_marks_new:
 * @name: (allow-none): a possible name for the #VisuGlExt.
 *
 * Create a new #VisuGlExtMarks object. Make it listen to
 * #VisuInteractive::node-selection signal to update itself
 * automatically.
 *
 * Returns: the newly created object.
 */
VisuGlExtMarks* visu_gl_ext_marks_new(const gchar *name)
{
  char *name_ = "MarksInv";
  char *description = _("Draw some marks on element in video inverse.");
  VisuGlExtMarks *marks;

  marks = VISU_GL_EXT_MARKS(g_object_new(VISU_TYPE_GL_EXT_MARKS,
                                         "name", (name)?name:name_, "label", _(name),
                                         "description", description, "nGlObj", 1,
                                         "priority", VISU_GL_EXT_PRIORITY_LAST - 1,
                                         "saveState", TRUE, NULL));

  marks->extNode =
    VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT,
                             "name", "Marks", "label", _("Marks - classical"),
                             "description", _("Draw some marks on element."),
                             "nGlObj", 1, "priority", VISU_GL_EXT_PRIORITY_LOW, NULL));

  return marks;
}

/**
 * visu_gl_ext_marks_getInternalList:
 * @marks: a #VisuGlExtMarks object.
 *
 * Return an additional list used internaly.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExt object.
 **/
VisuGlExt* visu_gl_ext_marks_getInternalList(VisuGlExtMarks *marks)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), (VisuGlExt*)0);

  return marks->extNode;
}

static struct MarkInfo_struct* markNew(VisuMarkType type)
{
  struct MarkInfo_struct *mark;

  mark          = g_malloc(sizeof(struct MarkInfo_struct));
  mark->type    = type;
  mark->idNode1 = -1;
  mark->idNode2 = -1;
  mark->idNode3 = -1;

  mark->nodes   = (GList*)0;
  mark->size    = 0;
  mark->coord   = (GLfloat*)0;
  mark->nIds    = (guint*)0;

  return (struct MarkInfo_struct*)mark;
}
static void markFree(struct MarkInfo_struct *mark)
{
  if (mark->nodes)
    g_list_free(mark->nodes);

  if (mark->coord)
    g_free(mark->coord);
  if (mark->nIds)
    g_free(mark->nIds);

  g_free(mark);
}
static void markRemove(VisuGlExtMarks *marks, GList *rmList)
{
  struct MarkInfo_struct *mark;
  gboolean hasCameraMarks;

  mark = (struct MarkInfo_struct*)rmList->data;
  DBG_fprintf(stderr, "Visu Marks: remove mark (%d %d %d).\n",
	      mark->idNode1, mark->idNode2, mark->idNode3);
  hasCameraMarks = (marks->hasCameraMarks > 0);
  if (mark->type == MARK_LINE)
    marks->hasCameraMarks -= 1;
  markFree(mark);
  marks->storedMarks = g_list_delete_link(marks->storedMarks, rmList);
  
  if (hasCameraMarks && marks->hasCameraMarks == 0)
    {
      g_signal_handler_disconnect(G_OBJECT(marks->data), marks->cameraAngles);
      g_signal_handler_disconnect(G_OBJECT(marks->data), marks->cameraPosition);
      g_signal_handler_disconnect(G_OBJECT(marks->data), marks->cameraZoom);
      g_signal_handler_disconnect(G_OBJECT(marks->data), marks->cameraPersp);
    }
}

/**
 * visu_gl_ext_marks_setInteractive:
 * @marks: a #VisuGlExtMarks object.
 * @inter: (transfer full) (allow-none): a #VisuInteractive object.
 *
 * Listen to #VisuInteractive::node-selection signal to update @marks.
 *
 * Since: 3.7
 **/
void visu_gl_ext_marks_setInteractive(VisuGlExtMarks *marks, VisuInteractive *inter)
{
  g_return_if_fail(VISU_IS_GL_EXT_MARKS(marks));

  if (marks->inter)
    {
      g_signal_handler_disconnect(G_OBJECT(marks->inter), marks->nodeSelection);
      g_object_unref(marks->inter);
    }
  if (inter)
    {
      g_object_ref(inter);
      marks->nodeSelection = g_signal_connect(G_OBJECT(inter), "node-selection",
                                              G_CALLBACK(onNodeSelection), (gpointer)marks);
    }
  else
    marks->nodeSelection = 0;
      
  marks->inter = inter;
}
/**
 * visu_gl_ext_marks_setGlView:
 * @marks: a #VisuGlExtMarks object.
 * @view: a #VisuGlView object.
 *
 * Use the given @view to render @marks.
 *
 * Since: 3.7
 **/
void visu_gl_ext_marks_setGlView(VisuGlExtMarks *marks, VisuGlView *view)
{
  GObject *obj;

  g_return_if_fail(VISU_IS_GL_EXT_MARKS(marks));

  /* We attach a new VisuGlView object. */
  if (marks->view != view)
    {
      /* We unref the previous VisuView and disconnect signals. */
      if (marks->view)
        {
          obj = G_OBJECT(marks->view);
          if (marks->hasCameraMarks > 0)
            {
              g_signal_handler_disconnect(obj, marks->cameraAngles);
              g_signal_handler_disconnect(obj, marks->cameraPosition);
              g_signal_handler_disconnect(obj, marks->cameraZoom);
              g_signal_handler_disconnect(obj, marks->cameraPersp);
            }
          g_object_unref(obj);
        }

      marks->view = view;
      if (marks->view)
        {
          obj = G_OBJECT(marks->view);
          if (marks->hasCameraMarks > 0)
            {
              marks->cameraAngles =
                g_signal_connect(obj, "ThetaPhiOmegaChanged",
                                 G_CALLBACK(updateListOnCameraChange),
                                 (gpointer)marks);
              marks->cameraPosition =
                g_signal_connect(obj, "XsYsChanged",
                                 G_CALLBACK(updateListOnCameraChange),
                                 (gpointer)marks);
              marks->cameraZoom =
                g_signal_connect(obj, "GrossChanged",
                                 G_CALLBACK(updateListOnCameraChange),
                                 (gpointer)marks);
              marks->cameraPersp =
                g_signal_connect(obj, "PerspChanged",
                                 G_CALLBACK(updateListOnCameraChange),
                                 (gpointer)marks);  
            }
          /* We ref the new given view. */
          g_object_ref(obj);
        }
    }
}
/**
 * visu_gl_ext_marks_setData:
 * @marks: a #VisuGlExtMarks object.
 * @data: a #VisuData object.
 *
 * Attach the given @marks to @data. @marks will be updated if @data
 * is changed and internal list of marks is updated with the new nodes
 * of @data.
 */
void visu_gl_ext_marks_setData(VisuGlExtMarks *marks, VisuData *data)
{
  GList *list, *rmList;
  GObject *obj;
  struct MarkInfo_struct*mark;
  gboolean remove;

  DBG_fprintf(stderr, "Visu Marks: set new VisuData %p (%p).\n",
	      (gpointer)data, (gpointer)marks->data);

  /* We update the list of nodes with marks. */
  if (data)
    {
      /* Try to match previous marks on new VisuData. */
      list = marks->storedMarks;
      while(list)
	{
	  mark = (struct MarkInfo_struct*)list->data;
	  DBG_fprintf(stderr, " | old mark %p of type %d.\n",
		      (gpointer)mark, mark->type);
	  remove = FALSE;
	  rmList = list;
	  /* We remove mark if one of the node can't be matched. */
	  switch (mark->type)
	    {
	    case MARK_BIG_SQUARE:
	    case MARK_SMALL_SQUARE:
	    case MARK_HIGHLIGHT:
	      remove = remove || !visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode1);
	      break;
	    case MARK_DISTANCE:
	      remove = remove ||
		!visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode1) ||
		!visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode2);
	      break;
	    case MARK_ANGLE:
	      remove = remove ||
		!visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode1) ||
		!visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode2) ||
		!visu_node_array_getFromId(VISU_NODE_ARRAY(data), mark->idNode3);
	      break;
	    default:
	      g_warning("TODO implementation required.");
	    }
	  list = g_list_next(list);
	  if (remove)
	    {
	      DBG_fprintf(stderr, " | delete old mark %p of type %d.\n",
			  (gpointer)mark, mark->type);
	      markRemove(marks, rmList);
	    }
	}
    }
  else
    {
      for (list = marks->storedMarks; list; list = g_list_next(list))
	markFree((struct MarkInfo_struct*)list->data);
      g_list_free(marks->storedMarks);
      marks->storedMarks   = (GList*)0;
    }
  DBG_fprintf(stderr, " | New mark list is pointing to %p.\n",
	      (gpointer)marks->storedMarks);

  /* We attach a new VisuData object. */
  if (marks->data != data)
    {
      /* We unref the previous VisuData and disconnect signals. */
      if (marks->data)
        {
          obj = G_OBJECT(marks->data);
          g_signal_handler_disconnect(obj, marks->nodePosition);
          g_signal_handler_disconnect(obj, marks->nodeRender);
          g_signal_handler_disconnect(obj, marks->nodeMaterial);
          g_signal_handler_disconnect(obj, marks->nodePopulation);
          g_object_unref(obj);
        }

      marks->data = data;
      if (marks->data)
        {
          obj = G_OBJECT(marks->data);

          /* We ref the new given data. */
          g_object_ref(obj);

          /* Connect a signal on file change to remove everything. */
          marks->nodePopulation =
            g_signal_connect(obj, "PopulationDecrease",
                             G_CALLBACK(updateListOnPopulationChange),
                             (gpointer)marks);
          marks->nodePosition =
            g_signal_connect(obj, "PositionChanged",
                             G_CALLBACK(onPositionChanged),
                             (gpointer)marks);
          marks->nodeRender =
            g_signal_connect(obj, "VisibilityChanged",
                             G_CALLBACK(updateListOnNodeChange),
                             (gpointer)marks);
          marks->nodeMaterial =
            g_signal_connect(obj, "MaterialChanged",
                             G_CALLBACK(updateListOnNodeChange),
                             (gpointer)marks);
        }
    }

  g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, NULL);

  marksDraw(marks, -1);
}

static void updateListOnPopulationChange(VisuData *dataObj, int *nodes,
					 gpointer data)
{
  int i;
  GList *list, *rmList;
  struct MarkInfo_struct*mark;
  gboolean remove;
  VisuGlExtMarks *marks;

  marks = (VisuGlExtMarks*)data;
  g_return_if_fail(marks);

  if (dataObj != marks->data)
    return;

  DBG_fprintf(stderr, "Visu Marks: caught 'PopulationDecrease'"
	      " signal (%p).\n", data);
  
  /* Run through the mark list to get all nodeId and look into nodes
     to find if mark must be removed or not. */
  DBG_fprintf(stderr, " | list contains %d elements\n",
	      g_list_length(marks->storedMarks));
  list = marks->storedMarks;
  while(list)
    {
      mark = (struct MarkInfo_struct*)list->data;
      DBG_fprintf(stderr, " | mark %p of type %d.\n",
		  (gpointer)mark, mark->type);
      /* We remove mark if one of the node is in the list. */
      remove = FALSE;
      rmList = list;
      for (i = 0; !remove && nodes[i] >= 0; i++)
	remove = remove ||
	  ((guint)nodes[i] == mark->idNode1) ||
	  ((guint)nodes[i] == mark->idNode2) ||
	  ((guint)nodes[i] == mark->idNode3);
      list = g_list_next(list);
      if (remove)
	{
	  DBG_fprintf(stderr, " | delete mark %p of type %d.\n",
		      (gpointer)mark, mark->type);
	  markRemove(marks, rmList);
	}
    }

  marksDraw(marks, -1);
}
static void updateListOnNodeChange(VisuData *dataObj, gpointer data)
{
  VisuGlExtMarks *marks;

  DBG_fprintf(stderr, "Visu Marks: caught a node rendering"
	      " changing signal (%p).\n", data);

  marks = (VisuGlExtMarks*)data;
  g_return_if_fail(data);

  if (dataObj != marks->data)
    return;

  marksDraw(marks, -1);
}
static void onPositionChanged(VisuData *dataObj, VisuElement *ele _U_, gpointer data)
{
  VisuGlExtMarks *marks;

  DBG_fprintf(stderr, "Visu Marks: caught a node position"
	      " changing signal (%p).\n", data);

  marks = (VisuGlExtMarks*)data;
  g_return_if_fail(data);

  if (dataObj != marks->data)
    return;

  marksDraw(marks, -1);
}
static void updateListOnCameraChange(VisuGlView *view _U_, gpointer data _U_)
{
/*   GList *tmpLst; */
/*   struct MarkInfo_struct* mark; */
  /* VisuGlExtMarks *marks; */

  /* marks = (VisuGlExtMarks*)data; */
  /* g_return_if_fail(data); */

/*   for (tmpLst = mesureData->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst)) */
/*     { */
/*       mark = (struct MarkInfo_struct*)(tmpLst->data); */
/*       if (mark->type == PICK_MESURE_MARK_LINE) */
/* 	visu_interactive_class_getNodes2DCoordinates(mesureData->data, mark->nIds, g_list_length(mark->nodes), mark->coord, &mark->size); */
/*     } */
/*   marksDraw(marks, 0); */
}

static gboolean onNodeSizeHook(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                               const GValue *param_values, gpointer data)
{
  VisuRendering *render;
  VisuGlExtMarks *marks;

  DBG_fprintf(stderr, "Visu Marks: get a 'elementSizeChanged' signals at %f.\n",
              g_value_get_float(param_values + 1));
  render = VISU_RENDERING(g_value_get_object(param_values));
  if (render != visu_object_getRendering(VISU_OBJECT_INSTANCE))
    return TRUE;

  marks = (VisuGlExtMarks*)data;
  marksDraw(marks, 1);

  return TRUE;
}
static gboolean onNodeSelection(VisuInteractive *inter _U_, VisuInteractivePick pick,
                                VisuNode *node0, VisuNode *node1, VisuNode *node2,
                                gpointer data)
{
  gint nodeId;
  VisuMarkType type;
  gboolean set, status;
  GList *list;
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(data);

  DBG_fprintf(stderr, "Visu Marks: get a 'node-selection' signals -> %d.\n", pick);
  
  switch (pick)
    {
    case PICK_DISTANCE:
      if (marks->drawValues)
	{
	  toggleDistance(marks, node1->number, node0->number, FALSE);
	  putMark(marks->data, marks->view,
                  node1->number, node0->number, MARK_DISTANCE);
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, NULL);
	  marksDraw(marks, 0);
	}
      return TRUE;
    case PICK_ANGLE:
      if (marks->drawValues)
	{
	  toggleAngle(marks, node1->number, node2->number, node0->number, FALSE);
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, NULL);
	  marksDraw(marks, 0);
	  VISU_REDRAW_FORCE;
	}
      return TRUE;
    case PICK_UNREFERENCE_1:
    case PICK_UNREFERENCE_2:
    case PICK_REFERENCE_1:
    case PICK_REFERENCE_2:
      type = (pick == PICK_REFERENCE_1 || pick == PICK_UNREFERENCE_1)?
	MARK_BIG_SQUARE:MARK_SMALL_SQUARE;
      nodeId = -1;
      for (list = marks->storedMarks; list; list = g_list_next(list))
	if (((struct MarkInfo_struct*)list->data)->type == type)
	  nodeId = ((struct MarkInfo_struct*)list->data)->idNode1;
      /* Erase previous one. */
      if (nodeId >= 0)
	{
	  removeDot(marks, nodeId, type);
	  putMark(marks->data, marks->view, -1, nodeId, type);
	}
      /* Add new one. */
      if (pick == PICK_REFERENCE_1 || pick == PICK_REFERENCE_2)
	{
	  addDot(marks, node0->number, type);
	  putMark(marks->data, marks->view, -1, node0->number, type);
	}
      marksDraw(marks, 0);
      return TRUE;
    case PICK_HIGHLIGHT:
      set = toggleHighlight(marks, node0->number, MARKS_STATUS_TOGGLE, &status);
      if (set)
	{
          list = visu_gl_ext_marks_getHighlightedList(marks);
          g_signal_emit(G_OBJECT(marks), signals[HIGHLIGHT_CHANGE_SIGNAL], 0, list, NULL);
          g_list_free(list);
	  marksDraw(marks, 1);
	  VISU_REDRAW_FORCE;
	}
      return TRUE;
    case PICK_INFORMATION:
      set = setInformation(marks, node0->number);
      if (set)
	{
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, NULL);
	  marksDraw(marks, 0);
	  VISU_REDRAW_FORCE;
	}
      return TRUE;
    case PICK_SELECTED:
    default:
      return TRUE;
    }
}

/* Method that add/remove  mark to the list of drawn marks. */
static void removeDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_if_fail(marks);

  /* Look for the mark. */
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == type && mark->idNode1 == nodeId)
	{
	  DBG_fprintf(stderr, "Visu Marks: found a dot mark.\n");
	  markRemove(marks, tmpLst);
	  return;
	}
    }
}
static void addDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type)
{
  struct MarkInfo_struct *mark;

  g_return_if_fail((type == MARK_BIG_SQUARE ||
		    type == MARK_SMALL_SQUARE ||
		    type == MARK_HIGHLIGHT));

  DBG_fprintf(stderr, "Visu Marks: add a new mark of type %d.\n", type);
  mark          = markNew(type);
  mark->idNode1 = nodeId;
  marks->storedMarks = g_list_prepend(marks->storedMarks, (gpointer)mark);
  return;
}
static gboolean toggleDistance(VisuGlExtMarks *marks, guint nodeRefId,
			       guint nodeId, gboolean set)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(marks, FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_DISTANCE &&
	  ((mark->idNode1 == nodeRefId && mark->idNode2 == nodeId) ||
	   (mark->idNode2 == nodeRefId && mark->idNode1 == nodeId)))
	{
	  DBG_fprintf(stderr, "Visu Marks: found a distance mark.\n");
	  if (!set)
	    markRemove(marks, tmpLst);
	  return set;
	}
    }
  /* Found none, create a new one. */
  mark          = markNew(MARK_DISTANCE);
  mark->idNode1 = nodeRefId;
  mark->idNode2 = nodeId;
  DBG_fprintf(stderr, "Visu Marks: add a distance mark.\n");
  marks->storedMarks = g_list_prepend(marks->storedMarks, (gpointer)mark);
  return TRUE;
}
static gboolean toggleAngle(VisuGlExtMarks *marks, guint nodeRefId,
			    guint nodeRef2Id, guint nodeId, gboolean set)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(marks, FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_ANGLE &&
	  mark->idNode1 == nodeRefId &&
	  ((mark->idNode2 == nodeRef2Id && mark->idNode3 == nodeId) ||
	   (mark->idNode3 == nodeRef2Id && mark->idNode2 == nodeId)))
	{
	  DBG_fprintf(stderr, "Visu Marks: found an angle mark.\n");
	  if (!set)
	    markRemove(marks, tmpLst);
	  return set;
	}
    }
  /* Found none, create a new one. */
  mark          = markNew(MARK_ANGLE);
  mark->idNode1 = nodeRefId;
  mark->idNode2 = nodeRef2Id;
  mark->idNode3 = nodeId;
  DBG_fprintf(stderr, "Visu Marks: add an angle mark.\n");
  marks->storedMarks = g_list_prepend(marks->storedMarks, (gpointer)mark);
  return TRUE;
}
static gboolean toggleHighlight(VisuGlExtMarks *marks, guint nodeId,
				VisuGlExtMarksStatus status, gboolean *finalStatus)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(marks, FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_HIGHLIGHT &&
	  mark->idNode1 == nodeId)
	{
	  DBG_fprintf(stderr, "Visu Marks: found a highlight mark (%d).\n", nodeId);
	  if (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET)
	    markRemove(marks, tmpLst);
	  if (finalStatus)
	    *finalStatus = (status != MARKS_STATUS_TOGGLE && status != MARKS_STATUS_UNSET);
	  /* Return if it has been changed. */
	  return (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET);
	}
    }
  if (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET)
    /* Found none, create a new one. */
    addDot(marks, nodeId, MARK_HIGHLIGHT);
  if (finalStatus)
    *finalStatus = (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET);
  /* Return if it has been changed. */
  return (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET);
}
static gboolean setInformation(VisuGlExtMarks *marks, guint node)
{
  VisuNodeInfo *infos;
  float min;
  GList *lst, *tmpLst, *tmpLst2;
  int i, n;
  gboolean redraw;
  float *dists;
  guint *ids;
  float xyz1[3], xyz2[3];

  g_return_val_if_fail(marks, FALSE);

  DBG_fprintf(stderr, "Visu Marks: compute and print distances"
	      " and angles for %d.\n", node);
  infos = visu_data_getDistanceList(marks->data, node, &min);

  lst = (GList*)0;
  redraw = FALSE;
  for (i = 0; infos[i].id != node; i+= 1)
    if (infos[i].dist < marks->infoRange * min)
      {
	toggleDistance(marks, node, infos[i].id, TRUE);
	lst = g_list_prepend(lst, GINT_TO_POINTER(infos[i].id));
	redraw = TRUE;
      }
  g_free(infos);

  n = g_list_length(lst);
  if (n > 1)
    {
      n = n * (n - 1 ) / 2;
      DBG_fprintf(stderr, "Visu Marks: there are %d angles at max.\n", n);
      ids = g_malloc(sizeof(guint) * n * 2);
      dists = g_malloc(sizeof(float) * n);
      i = 0;
      min = G_MAXFLOAT;
      for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
	for (tmpLst2 = tmpLst->next; tmpLst2; tmpLst2 = g_list_next(tmpLst2))
	  {
	    ids[i * 2 + 0] = (guint)GPOINTER_TO_INT(tmpLst->data);
	    ids[i * 2 + 1] = (guint)GPOINTER_TO_INT(tmpLst2->data);
	    visu_data_getNodePosition(marks->data,
				     visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data),
								ids[i * 2 + 0]),
				     xyz1);
	    visu_data_getNodePosition(marks->data,
				     visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data),
								ids[i * 2 + 1]),
				     xyz2);
	    dists[i] =
	      (xyz1[0] - xyz2[0]) * (xyz1[0] - xyz2[0]) +
	      (xyz1[1] - xyz2[1]) * (xyz1[1] - xyz2[1]) +
	      (xyz1[2] - xyz2[2]) * (xyz1[2] - xyz2[2]);
	    DBG_fprintf(stderr, " | %d (%d %d) -> %g\n", i, ids[i * 2 + 0],
			ids[i * 2 + 1], dists[i]);
	    min = MIN(min, dists[i]);
	    i += 1;
	  }
  
      for (i = 0; i < n; i++)
	if (dists[i] < (2.75f * min))
	  {
	    DBG_fprintf(stderr, " | %d (%d %d) -> %g OK\n", i, ids[i * 2 + 0],
			ids[i * 2 + 1], dists[i]);
	    toggleAngle(marks, node, ids[i * 2 + 0],
			ids[i * 2 + 1], TRUE);
	  }
      g_free(ids);
      g_free(dists);
    }
  g_list_free(lst);

  return redraw;
}
/**
 * visu_gl_ext_marks_getActive:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 *
 * Retrieve if @nodeId is implied any measurement marks stored in @mark.
 *
 * Returns: TRUE if @nodeId participate to any mark (distance,
 * angle...).
 */
gboolean visu_gl_ext_marks_getActive(VisuGlExtMarks *marks, guint nodeId)
{
  GList *list;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks, FALSE);

  for (list = marks->storedMarks; list; list = g_list_next(list))
    {
      mark = (struct MarkInfo_struct*)list->data;
      if ((mark->type == MARK_DISTANCE &&
	   mark->idNode1 == nodeId) ||
	  (mark->type == MARK_ANGLE &&
	   mark->idNode1 == nodeId))
	return TRUE;
    }
  return FALSE;
}
/**
 * visu_gl_ext_marks_getHighlightedList:
 * @marks: a #VisuGlExtMarks object ;
 *
 * @marks has a list of mark for some nodes. These marks are only
 * highlight marks.
 *
 * Returns: (element-type guint32) (transfer container): list of
 * highlighted nodes (starting from 0), should freed with g_list_free().
 *
 * Since: 3.6
 */
GList* visu_gl_ext_marks_getHighlightedList(VisuGlExtMarks *marks)
{
  GList *lst, *tmpLst;
  struct MarkInfo_struct* mark;

  g_return_val_if_fail(marks, (GList*)0);

  lst = (GList*)0;
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)(tmpLst->data);
      if (mark->type == MARK_HIGHLIGHT)
	lst = g_list_prepend(lst, GINT_TO_POINTER(mark->idNode1));
    }
  return lst;
}
/**
 * visu_gl_ext_marks_getHighlightStatus:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id (ranging from 0).
 *
 * Nodes can be highlighted.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @nodeId has an highlight.
 **/
gboolean visu_gl_ext_marks_getHighlightStatus(VisuGlExtMarks *marks, guint nodeId)
{
  GList *tmpLst;
  struct MarkInfo_struct* mark;

  g_return_val_if_fail(marks, FALSE);

  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)(tmpLst->data);
      if (mark->type == MARK_HIGHLIGHT && mark->idNode1 == nodeId)
        return TRUE;
    }
  return FALSE;
}
/**
 * visu_gl_ext_marks_setHighlightedList:
 * @marks: a #VisuGlExtMarks object ;
 * @lst: (element-type guint32): a set of node ids (0 started) ;
 * @status: changing command.
 *
 * @marks has a list of mark for some nodes. These marks can be
 * highlight (or distance, angles...). Depending on @status values,
 * the mark may be switch on or off.
 *
 * Returns: TRUE if redraw needed.
 *
 * Since: 3.6
 */
gboolean visu_gl_ext_marks_setHighlightedList(VisuGlExtMarks *marks, GList *lst,
				      VisuGlExtMarksStatus status)
{
  gboolean redraw, res;
  GList *tmpLst;

  DBG_fprintf(stderr, "Visu Marks: set highlight status of list to %d.\n",
	      status);
  redraw = FALSE;
  for (; lst; lst = g_list_next(lst))
    {
      res = toggleHighlight(marks, (guint)GPOINTER_TO_INT(lst->data),
			    status, (gboolean*)0);
      DBG_fprintf(stderr, " | %d -> %d\n", GPOINTER_TO_INT(lst->data), res);
      redraw = redraw || res;
    }
  DBG_fprintf(stderr, "Visu Marks: resulting redraw %d.\n",
	      redraw);
  if (redraw)
    {
      marksDraw(marks, 1);
      tmpLst = visu_gl_ext_marks_getHighlightedList(marks);
      g_signal_emit(G_OBJECT(marks), signals[HIGHLIGHT_CHANGE_SIGNAL], 0, tmpLst, NULL);
      g_list_free(tmpLst);
    }
  return redraw;
}
/**
 * visu_gl_ext_marks_setDrawValues:
 * @marks: a #VisuGlExtMarks object.
 * @status: a boolean.
 *
 * Change if the measurements are printed or not (distance length, or
 * angles...).
 *
 * Returns: TRUE if @marks is modified.
 */
gboolean visu_gl_ext_marks_setDrawValues(VisuGlExtMarks *marks, gboolean status)
{
  g_return_val_if_fail(marks, FALSE);

  if (marks->drawValues == status)
    return FALSE;

  marks->drawValues = status;
  return TRUE;
}
/**
 * visu_gl_ext_marks_removeMeasures:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 *
 * This routine scans the @mark to remove all marks of distance or
 * angle where @nodeId is implied in.
 *
 * Returns: TRUE is @mark is changed.
 */
gboolean visu_gl_ext_marks_removeMeasures(VisuGlExtMarks *marks, gint nodeId)
{
  gboolean redraw;
  GList *list, *tmpLst;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks, FALSE);

  DBG_fprintf(stderr, "Visu Marks: remove measures (%d).\n", nodeId);
  redraw = FALSE;
  for (list = marks->storedMarks; list; list = tmpLst)
    {
      tmpLst = g_list_next(list);
      mark = (struct MarkInfo_struct*)list->data;
      if ((mark->type == MARK_DISTANCE ||
	   mark->type == MARK_ANGLE) &&
	  (nodeId < 0 || mark->idNode1 == (guint)nodeId))
	{
	  markRemove(marks, list);
	  redraw = TRUE;
	}
    }
  if (!redraw)
    return FALSE;

  marksDraw(marks, 0);
  return TRUE;
}
/**
 * visu_gl_ext_marks_setInfos:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 * @status: a boolean.
 *
 * Depending on @status, it removes all measurements from @nodeId or
 * it calculate all first neighbour relations of @nodeId.
 *
 * Return: TRUE if @marks is changed.
 */
gboolean visu_gl_ext_marks_setInfos(VisuGlExtMarks *marks, guint nodeId, gboolean status)
{
  DBG_fprintf(stderr, "Visu Marks: set info for node %d.\n", nodeId);

  if (status)
    {
      if (setInformation(marks, nodeId))
	{
	  marksDraw(marks, 0);
	  return TRUE;
	}
    }
  else
    return visu_gl_ext_marks_removeMeasures(marks, nodeId);
    
  return FALSE;
}


/****************************/
/* OpenGL drawing routines. */
/****************************/
static void marksDraw(VisuGlExtMarks *marks, int listType)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;
  guint id;

  g_return_if_fail(marks);

  DBG_fprintf(stderr, "Visu Marks: update the list %p"
	      " of all marks.\n", (gpointer)marks->storedMarks);
  if (listType == 0 || listType < 0)
    glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(marks)), 1);
  if (listType == 1 || listType < 0)
    glDeleteLists(visu_gl_ext_getGlList(marks->extNode), 1);
  if (!marks->storedMarks)
    return;

  if (listType == 0 || listType < 0)
    {
      visu_gl_text_initFontList();

      id = 0;
      /* Draw the colour inverse list. */
      glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(marks)), GL_COMPILE);
      glEnable(GL_DEPTH_TEST);
      glClear(GL_DEPTH_BUFFER_BIT);
/*       glDisable(GL_DEPTH_TEST); */
      glDisable(GL_LIGHTING);
      glDisable(GL_FOG);
      glDisable(GL_CULL_FACE);
      /*   glDisable(GL_DITHER); */
      glEnable(GL_BLEND);
      glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR);
      for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
	{
	  mark = (struct MarkInfo_struct*)tmpLst->data;
	  DBG_fprintf(stderr, " | draw mark of type %d.\n", mark->type);
	  switch (mark->type)
	    {
	    case MARK_BIG_SQUARE:
	    case MARK_SMALL_SQUARE:
	      drawMarkDot(marks->data, marks->view, mark->idNode1, mark->type);
	      break;
	    case MARK_DISTANCE:
	      drawMarkDistance(marks->data, mark->idNode1, mark->idNode2, mark->type);
	      break;
	    case MARK_LINE:
	      drawMarkLine(marks->data, mark->coord, mark->size);
	      break;
	    case MARK_ANGLE:
	      drawMarkAngle(marks->data, mark->idNode1, mark->idNode2,
			    mark->idNode3, id);
	      id += 1;
	      break;
	    default:
	      break;
	    }
	}
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEndList();
    }
  if (listType == 1 || listType < 0)
    {
      /* Draw the classical list. */
      glNewList(visu_gl_ext_getGlList(marks->extNode), GL_COMPILE);
      glEnable(GL_LIGHTING);
      for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
	{
	  mark = (struct MarkInfo_struct*)tmpLst->data;
	  if (mark->type == MARK_HIGHLIGHT)
	    {
	      DBG_fprintf(stderr, " | draw mark of type %d.\n", mark->type);
	      drawMarkDot(marks->data, marks->view, mark->idNode1, mark->type);
	    }
	}
      glEndList();
    }
}
static void drawMarkDot(VisuData *data, VisuGlView *view, guint nodeId, VisuMarkType type)
{
  VisuNode *node;
  float xyz[3], material[5] = {1.f, 1.f, 1.f, 0.f, 0.f};
  VisuElement *ele;
  int nlat;
  float eleSize;
  GLUquadricObj *obj;

  node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_if_fail(node);

  /* If the node is masked, then we don't draw the dot. */
  if (!node->rendered)
    return;
  /* If the element is masked, then we don't draw the dot. */
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), node);
  if (!ele->rendered)
    return;

  DBG_fprintf(stderr, "Visu Mark: draw a mark (%d) on node %d.\n",
	      (int)type, node->number);

  visu_data_getNodePosition(data, node, xyz);
  switch (type)
    {
    case MARK_BIG_SQUARE:
      glRasterPos3f(xyz[0], xyz[1], xyz[2]);
      glDrawPixels(MARK_BIG_SQUARE_SIZE,
		   MARK_BIG_SQUARE_SIZE,
		   GL_RGBA, GL_UNSIGNED_BYTE, markBigSquare);
      break;
    case MARK_SMALL_SQUARE:
      glRasterPos3f(xyz[0], xyz[1], xyz[2]);
      glDrawPixels(MARK_SMALL_SQUARE_SIZE,
		   MARK_SMALL_SQUARE_SIZE,
		   GL_RGBA, GL_UNSIGNED_BYTE, markSmallSquare);
      break;
    case MARK_HIGHLIGHT:
      g_return_if_fail(view);

      obj = gluNewQuadric();
      eleSize = visu_rendering_getSizeOfElement(visu_object_getRendering(VISU_OBJECT_INSTANCE), ele);
      visu_gl_setHighlightColor(material, ele->rgb, 0.5f);
      nlat = visu_gl_view_getDetailLevel(view, eleSize * highlightFactor);
      glPushMatrix();
      glTranslated(xyz[0], xyz[1], xyz[2]);
      gluSphere(obj, (double)eleSize * highlightFactor, 2 * nlat, 2 * nlat);
      glPopMatrix();
      gluDeleteQuadric(obj);
      break;
    default:
      break;
    }
}
static void drawMarkDistance(VisuData *data, guint nodeRefId,
			     guint nodeId, VisuMarkType type _U_)
{
  VisuNode *nodeRef;
  VisuNode *node;
  float xyzRef[3], xyz[3];
  VisuElement *ele;

  nodeRef = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeRefId);
  node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_if_fail(node && nodeRef);

  /* If one of the node is masked, then we don't draw the distance. */
  if (!nodeRef->rendered || !node->rendered)
    return;

  /* If one of the element is masked, then we don't draw the distance. */
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), nodeRef);
  if (!ele->rendered)
    return;
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), node);
  if (!ele->rendered)
    return;

  visu_data_getNodePosition(data, nodeRef, xyzRef);
  visu_data_getNodePosition(data, node, xyz);

  visu_gl_drawDistance(xyzRef, xyz, TRUE);
}
static void drawMarkAngle(VisuData *data, guint nodeRefId, guint nodeRef2Id,
			  guint nodeId, guint id)
{
  VisuNode *nodeRef, *nodeRef2, *node;
  float xyzRef[3], xyzRef2[3], xyz[3];
  VisuElement *ele;

  nodeRef = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeRefId);
  nodeRef2 = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeRef2Id);
  node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_if_fail(node && nodeRef && nodeRef2);

  /* If one of the node is masked, then we don't draw the angle. */
  if (!nodeRef->rendered || !nodeRef2->rendered || !node->rendered)
    return;

  /* If one of the element is masked, then we don't draw the angle. */
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), nodeRef);
  if (!ele->rendered)
    return;
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), nodeRef2);
  if (!ele->rendered)
    return;
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(data), node);
  if (!ele->rendered)
    return;

  visu_data_getNodePosition(data, nodeRef, xyzRef);
  visu_data_getNodePosition(data, nodeRef2, xyzRef2);
  visu_data_getNodePosition(data, node, xyz);

  DBG_fprintf(stderr, "Visu PickMesure: draw an angle mark on nodes %d/%d/%d.\n",
	      nodeRef->number, nodeRef2->number, node->number);
  visu_gl_drawAngle(xyzRef, xyzRef2, xyz, id, TRUE);
}
/* Calculate the node vertices coordinates (contained in the
array nodevertices), from node coordinates (xn,yn) */
static void defineNodeVertices(int nVert, double radius, double xn, double yn, double *nodeVertices)
{
  int i;
  for (i = 0; i < nVert; i++) {
    nodeVertices[2 * i] = (xn + radius * (cos (((2 * G_PI * i) / nVert))));
    nodeVertices[2 * i + 1] = (yn + radius * (sin (((2 * G_PI * i) / nVert))));
  }
}

/* Add node vertices to vertices array, from position i+1 */
static void addVerticesToGlobalArray(int nVert, double *nodeVertices, double *vertices, int i)
{
  int j;
  int k = 0;
  for (j = (i * 2) * nVert; j < (i * 2) * nVert + (nVert * 2); j += 2) {
    vertices[j] = nodeVertices[k];
    vertices[j + 1] = nodeVertices[k + 1];
    k += 2;
  }
}
static void drawMarkLine(VisuData *data _U_, GLfloat *coord, guint size)
{
  #define NEWLINE 1
  #define NEWNODE 2
  int i = 0, j;
  int elem = 0;
  int nNodes, nTotalVert;
  /***********************************/
  /* values must be selected by user */
  int nVert = 30;
  double radius = 30;
  /***********************************/
  double *vertices, *nodeVertices, *verticesKept;
  double link[2] = {0};
  double distance;
  gboolean visible = TRUE;
  gboolean li = FALSE;

  nNodes = size / 2;
  nTotalVert = nNodes * nVert;
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0.0, 600, 0., 600);
   
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  vertices     = g_malloc(sizeof(double) * (nTotalVert * 2));
  nodeVertices = g_malloc(sizeof(double) * (nVert * 2));
  verticesKept = g_malloc(sizeof(double) * (nTotalVert * 2) + sizeof(int) * nTotalVert * 2);

  /* For each node, compute and add its vertices coordinates (nodeVertices) 
     to the vertices global array (vertices) */
  for (i = 0; i < nNodes; i++){
    defineNodeVertices(nVert, radius, coord[2 * i], coord[2 * i + 1], nodeVertices);
    addVerticesToGlobalArray(nVert, nodeVertices, vertices, i);
  }
  g_free(nodeVertices);
  verticesKept[elem] = NEWNODE;
  elem++;

  /* For each vertex */
  for (i = 0; i < nTotalVert; i++){
    /* We notify with a marker each new node */
    if (i / nVert != (i - 1) / nVert){
      verticesKept[elem] = NEWNODE;
      elem++;
    }
    /* For each node */
    for (j = 0; j < nNodes; j++){
      /* If vertex i doesn't belong to node j */
      if (j != i / nVert){
        /* Distance between vertex i and node j */
        distance = sqrt( (pow ((coord[2 * j + 1] - vertices[2 * i + 1]), 2)) +
                         (pow ((coord[2 * j]     - vertices[2 * i]),     2)) );
        if (distance < radius)
          visible = FALSE;
      }
    }
    if (visible){
      verticesKept[elem] = vertices[2 * i];
      verticesKept[elem + 1] = vertices[2 * i + 1];
      elem += 2;
    }
    else {
      if (verticesKept[elem - 1] != NEWLINE){
        verticesKept[elem] = NEWLINE;
        elem++;
      }
      visible = TRUE;
    }
  }

  glLineWidth(4.0);
  glColor3f(1.0, 1.0, 1.0);
  i = 0;

  while (i < elem){
    if (verticesKept[i] == NEWLINE)
      i++;
    else if (verticesKept[i] == NEWNODE){
      i++;
      if (verticesKept[i] != NEWNODE && verticesKept[i] != NEWLINE){
        link[0] = verticesKept[i];
        link[1] = verticesKept[i + 1];
        li = TRUE;
      }
      else {
        li = FALSE;
        while (verticesKept[i] == NEWNODE || verticesKept[i] == NEWLINE)
          i++;
      }
    }
    else {
      glBegin(GL_LINES);
      while (verticesKept[i + 2] != NEWNODE && verticesKept[i + 2] != NEWLINE && i + 2 < elem){
        glVertex2f(verticesKept[i], verticesKept[i + 1]);
        glVertex2f(verticesKept[i + 2], verticesKept[i + 3]);
        i += 2;
      }
      if (i + 2 >= elem){
        if (li){
          glVertex2f(verticesKept[i], verticesKept[i + 1]);
          glVertex2f(link[0], link[1]);
        }
        i = elem;
      }
      else if (verticesKept[i + 2] == NEWLINE)
        i += 3;
      else if (verticesKept[i + 2] == NEWNODE){
        if (li){
          glVertex2f(verticesKept[i], verticesKept[i + 1]);
          glVertex2f(link[0], link[1]);
        }
        i += 2;
      }
      glEnd();
    }
  }

  g_free(verticesKept);
  g_free(vertices);
  
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}
static void putMark(VisuData *data, VisuGlView *view, guint nodeRefId,
		    guint nodeId, VisuMarkType type)
{
  float centre[3];

  visu_box_getCentre(visu_boxed_getBox(VISU_BOXED(data)), centre);

  DBG_fprintf(stderr, "Visu Marks: draw directly a distance on front buffer.\n");

  glDrawBuffer(GL_FRONT);
  glPushAttrib(GL_ENABLE_BIT);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR);
  glPushMatrix();
  glTranslated(-centre[0], -centre[1], -centre[2]);
  switch (type)
    {
    case MARK_BIG_SQUARE:
    case MARK_SMALL_SQUARE:
      drawMarkDot(data, view, nodeId, type);
      break;
    case MARK_DISTANCE:
      drawMarkDistance(data, nodeRefId, nodeId, type);
      break;
    default:
      g_warning("No direct drawing available for this type.");
    }
  glPopMatrix();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPopAttrib();
  glDrawBuffer(GL_BACK);

  glFlush();
}


/*************************/
/* Resources management. */
/*************************/
static void exportResources(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_FACTOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FACTOR, NULL,
                               "%f", highlightFactor);
  visu_config_file_exportComment(data, "");
}


/****************************/
/* List drawing management. */
/****************************/
static void visu_gl_ext_marks_rebuild(VisuGlExt *ext)
{
  visu_gl_text_rebuildFontList();
  marksDraw(VISU_GL_EXT_MARKS(ext), -1);
}

/******************************************/
/* XML files for marks and other exports. */
/******************************************/

/**
 * visu_gl_ext_marks_getMeasurementLabels:
 * @marks: a #VisuGlExtMarks object.
 *
 * Exports as a string the ids of nodes for measurement marks.
 *
 * Since: 3.6
 *
 * Returns: a newly allocated string.
 */
gchar* visu_gl_ext_marks_getMeasurementLabels(VisuGlExtMarks *marks)
{
  GString *str;
  GList *tmpLst;
  struct MarkInfo_struct *mark;
  guint i;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), (gchar*)0);

  str = g_string_new("#");

  /* Look for the mark. */
  for (tmpLst = marks->storedMarks, i = 0; tmpLst && i < 6;
       tmpLst = g_list_next(tmpLst), i += 1)
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_DISTANCE)
        g_string_append_printf(str, "      %4d-%4d", mark->idNode1 + 1, mark->idNode2 + 1);
      else if (mark->type == MARK_ANGLE)
        g_string_append_printf(str, " %4d-%4d-%4d",
                               mark->idNode3 + 1, mark->idNode1 + 1, mark->idNode2 + 1);
    }
  if (!tmpLst)
    g_string_append(str, "\n");
  else
    g_string_append(str, " (truncated list)\n");

  return g_string_free(str, FALSE);
}
/**
 * visu_gl_ext_marks_getMeasurementStrings:
 * @marks: a #VisuGlExtMarks object.
 * @dataObj: the #VisuData to apply the measurements to.
 *
 * Exports as a string all measurements stored in @marks.
 *
 * Since: 3.6
 *
 * Returns: a newly allocated string.
 */
gchar* visu_gl_ext_marks_getMeasurementStrings(VisuGlExtMarks *marks, VisuData *dataObj)
{
  GString *str;
  GList *tmpLst;
  struct MarkInfo_struct *mark;
  float posSelect[3], posRef1[3], posRef2[3], dx, dy, dz, dr;
  float dx1, dy1, dz1, dx2, dy2, dz2, dr1, dr2, ang;
  VisuNode *nodes[3];
  gchar *lbl;
  gboolean export;
  guint i;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), (gchar*)0);

  str = g_string_new(" ");
  export = FALSE;
  /* Look for the mark. */
  for (tmpLst = marks->storedMarks, i = 0; tmpLst && i < 6;
       tmpLst = g_list_next(tmpLst), i += 1)
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      nodes[0] = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), mark->idNode1);
      nodes[1] = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), mark->idNode2);
      nodes[2] = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), mark->idNode3);
      if (mark->type == MARK_DISTANCE)
	{
	  DBG_fprintf(stderr, "Visu Marks: export this distance.\n");
          visu_data_getNodePosition(dataObj, nodes[0], posRef1);
          visu_data_getNodePosition(dataObj, nodes[1], posSelect);
          dx = posSelect[0] - posRef1[0];
          dy = posSelect[1] - posRef1[1];
          dz = posSelect[2] - posRef1[2];
          dr = sqrt(dx*dx + dy*dy + dz*dz);
          g_string_append_printf(str, "   %12.6g", dr);
          export = TRUE;
	}
      else if (mark->type == MARK_ANGLE)
        {
	  DBG_fprintf(stderr, "Visu Marks: export this angle.\n");
          visu_data_getNodePosition(dataObj, nodes[0], posRef1);
          visu_data_getNodePosition(dataObj, nodes[1], posRef2);
          visu_data_getNodePosition(dataObj, nodes[2], posSelect);
          dx1 = posSelect[0] - posRef1[0];
          dy1 = posSelect[1] - posRef1[1];
          dz1 = posSelect[2] - posRef1[2];
          dx2 = posRef2[0] - posRef1[0];
          dy2 = posRef2[1] - posRef1[1];
          dz2 = posRef2[2] - posRef1[2];
          dr1 = sqrt(dx1*dx1 + dy1*dy1 + dz1*dz1);
          dr2 = sqrt(dx2*dx2 + dy2*dy2 + dz2*dz2);
          ang = acos((dx2*dx1+dy2*dy1+dz2*dz1)/(dr2*dr1))/TOOL_PI180;
          g_string_append_printf(str, "   %12.6g", ang);
          export = TRUE;
        }
    }
  if (!export)
    {
      g_string_free(str, TRUE);
      return (gchar*)0;
    }
  lbl = visu_data_getFilesAsLabel(dataObj);
  if (lbl)
    {
      g_string_append_printf(str, " # %s\n", lbl);
      g_free(lbl);
    }

  return g_string_free(str, FALSE);
}

/*
   <pick data-mode="selected" data-info="">
     <node id="23" />
     <node id="16" />
     <distance ref="45" id="23" />
   </pick>
*/

/* Known elements. */
#define PICK_PARSER_ELEMENT_PICK  "pick"
#define PICK_PARSER_ELEMENT_NODE  "node"
#define PICK_PARSER_ELEMENT_DIST  "distance"
#define PICK_PARSER_ELEMENT_ANGL  "angle"
/* Known attributes. */
#define PICK_PARSER_ATTRIBUTES_MODE "info-mode"
#define PICK_PARSER_ATTRIBUTES_INFO "info-data"
#define PICK_PARSER_ATTRIBUTES_ID   "id"
#define PICK_PARSER_ATTRIBUTES_REF  "ref"
#define PICK_PARSER_ATTRIBUTES_REF2 "ref2"
#define PICK_PARSER_ATTRIBUTES_HLT  "highlight"

static gboolean startPick;
static VisuGlExtInfosDrawId mode;
static guint info;

/* This method is called for every element that is parsed.
   The user_data must be a GList of _pick_xml. When a 'surface'
   element, a new struct instance is created and prepend in the list.
   When 'hidden-by-planes' or other qualificative elements are
   found, the first surface of the list is modified accordingly. */
static void pickXML_element(GMarkupParseContext *context _U_,
			    const gchar         *element_name,
			    const gchar        **attribute_names,
			    const gchar        **attribute_values,
			    gpointer             user_data,
			    GError             **error)
{
  GList **pickList;
  int i, n;
  guint val, val2, val3;
  gboolean highlight;

  g_return_if_fail(user_data);
  pickList = (GList **)user_data;

  DBG_fprintf(stderr, "Pick parser: found '%s' element.\n", element_name);
  if (!strcmp(element_name, PICK_PARSER_ELEMENT_PICK))
    {
      /* Initialise the pickList. */
      if (*pickList)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
		      _("DTD error: element '%s' should appear only once."),
		      PICK_PARSER_ELEMENT_PICK);
	  return;
	}
      *pickList = (GList*)0;
      startPick = TRUE;
      /* Should have 2 mandatory attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_MODE))
	    {
	      if (!strcmp(attribute_values[i], "never"))
		mode = DRAW_NEVER;
	      else if (!strcmp(attribute_values[i], "selected"))
		mode = DRAW_SELECTED;
	      else if (!strcmp(attribute_values[i], "always"))
		mode = DRAW_ALWAYS;
	      else
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_MODE, attribute_values[i]);
		  return;
		}
	    }
	  else if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_INFO))
	    {
	      n = sscanf(attribute_values[i], "%u", &info);
	      if (n != 1 || info < 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_INFO, attribute_values[i]);
		  return;
		}
	    }
	}
    }
  else if (!strcmp(element_name, PICK_PARSER_ELEMENT_NODE))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_NODE);
	  return;
	}
      highlight = FALSE;
      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_HLT))
	    highlight = (!strcmp(attribute_values[i], "yes") ||
			 !strcmp(attribute_values[i], "Yes"));
	}
      if (highlight)
	*pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_HIGHLIGHT));
      else
	*pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_SELECTED));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (!strcmp(element_name, PICK_PARSER_ELEMENT_DIST))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_DIST);
	  return;
	}

      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF))
	    {
	      n = sscanf(attribute_values[i], "%u", &val2);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF, attribute_values[i]);
		  return;
		}
	    }
	}
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_DISTANCE));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val2));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (!strcmp(element_name, PICK_PARSER_ELEMENT_ANGL))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_ANGL);
	  return;
	}

      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF))
	    {
	      n = sscanf(attribute_values[i], "%u", &val2);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF, attribute_values[i]);
		  return;
		}
	    }
	  else if (!strcmp(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF2))
	    {
	      n = sscanf(attribute_values[i], "%u", &val3);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF2, attribute_values[i]);
		  return;
		}
	    }
	}
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_ANGLE));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val3));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val2));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (startPick)
    {
      /* We silently ignore the element if pickList is unset, but
	 raise an error if pickList has been set. */
      g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
		  _("Unexpected element '%s'."), element_name);
    }
}

/* Check when a element is closed that everything required has been set. */
static void pickXML_end(GMarkupParseContext *context _U_,
			const gchar         *element_name,
			gpointer             user_data _U_,
			GError             **error _U_)
{
  if (!strcmp(element_name, PICK_PARSER_ELEMENT_PICK))
    startPick = FALSE;
}

/* What to do when an error is raised. */
static void pickXML_error(GMarkupParseContext *context _U_,
			  GError              *error,
			  gpointer             user_data)
{
  DBG_fprintf(stderr, "Pick parser: error raised '%s'.\n", error->message);
  g_return_if_fail(user_data);

  /* We free the current list of pick. */
  g_list_free(*(GList**)user_data);
}

/**
 * visu_gl_ext_marks_parseXMLFile:
 * @marks: a #VisuGlExtMarks object.
 * @filename: a location to save to.
 * @infos: (element-type guint32) (out): a location to a #GList.
 * @drawingMode: a location to a flag.
 * @drawingInfos: a location to a flag.
 * @error: a location to store an error.
 *
 * This routines read from an XML file the description of selected
 * nodes, @mark is updated accordingly.
 *
 * Since: 3.5
 *
 * Returns: TRUE if no error.
 */
gboolean visu_gl_ext_marks_parseXMLFile(VisuGlExtMarks *marks, const gchar* filename,
				GList **infos, VisuGlExtInfosDrawId *drawingMode,
				guint *drawingInfos, GError **error)
{
  GMarkupParseContext* xmlContext;
  GMarkupParser parser;
  gboolean status;
  gchar *buffer;
  gsize size;
  GList *tmpLst;
  guint id1, id2, id3;

  g_return_val_if_fail(filename, FALSE);
  g_return_val_if_fail(infos && drawingMode && drawingInfos, FALSE);

  buffer = (gchar*)0;
  if (!g_file_get_contents(filename, &buffer, &size, error))
    return FALSE;

  /* Create context. */
  *infos = (GList*)0;
  parser.start_element = pickXML_element;
  parser.end_element   = pickXML_end;
  parser.text          = NULL;
  parser.passthrough   = NULL;
  parser.error         = pickXML_error;
  xmlContext = g_markup_parse_context_new(&parser, 0, infos, NULL);

  /* Parse data. */
  startPick = FALSE;
  status = g_markup_parse_context_parse(xmlContext, buffer, size, error);

  /* Free buffers. */
  g_markup_parse_context_free(xmlContext);
  g_free(buffer);

  if (!status)
    return FALSE;

  if (!*infos)
    {
      *error = g_error_new(G_MARKUP_ERROR, G_MARKUP_ERROR_EMPTY,
			  _("No picked node found."));
      return FALSE;
    }

  /* Need to reverse the list since elements have been prepended. */
  *infos        = g_list_reverse(*infos);
  *drawingMode  = mode;
  *drawingInfos = info;

  /* Update the current marks. */
  if (marks)
    {
      tmpLst = *infos;
      while(tmpLst)
	{
	  if (GPOINTER_TO_INT(tmpLst->data) == PICK_SELECTED)
	    tmpLst = g_list_next(tmpLst);
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_HIGHLIGHT)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id1))
		toggleHighlight(marks, id1, MARKS_STATUS_SET, (gboolean*)0);
	    }
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_DISTANCE)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id2 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id1) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id2))
		toggleDistance(marks, id1, id2, TRUE);
	    }
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_ANGLE)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id2 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id3 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id1) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id2) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(marks->data), id3))
		toggleAngle(marks, id2, id1, id3, TRUE);
	    }
	  else
	    {
	      g_error("Should not be here!");
	    }
      
	  tmpLst = g_list_next(tmpLst);
	}
      marksDraw(marks, 0);
      marksDraw(marks, 1);
    }

  tmpLst = visu_gl_ext_marks_getHighlightedList(marks);
  g_signal_emit(G_OBJECT(marks), signals[HIGHLIGHT_CHANGE_SIGNAL], 0, tmpLst, NULL);
  g_list_free(tmpLst);
  g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, NULL);

  return TRUE;
}

/**
 * visu_gl_ext_marks_exportXMLFile:
 * @marks: a #VisuGlExtMarks object.
 * @filename: a location to save to.
 * @nodes: an array of nodes, -1 terminated (can be NULL).
 * @drawingMode: a flag.
 * @drawingInfos: a flag.
 * @error: a location to store an error.
 *
 * This routines export to an XML file a description of selected
 * @nodes. If @nodes is NULL, the nodes stored in the @mark will be
 * used instead.
 *
 * Since: 3.5
 *
 * Returns: TRUE if no error.
 */
gboolean visu_gl_ext_marks_exportXMLFile(VisuGlExtMarks *marks, const gchar* filename,
				 int *nodes, VisuGlExtInfosDrawId drawingMode,
				 guint drawingInfos, GError **error)
{
  gboolean valid, set;
  GString *output;
  int i;
  char *modes[] = {"never", "selected", "always"};
  GList *tmpLst;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks && filename, FALSE);

  /*
   <pick data-mode="selected" data-info="0">
     <node id="23" />
     <node id="16" highlight="yes" />
     <distance ref="45" id="23" />
     <angle ref="45" ref2="65" id="23" />
   </pick>
  */

  output = g_string_new("  <pick");
  g_string_append_printf(output, " data-mode=\"%s\" data-infos=\"%d\">\n",
			 modes[drawingMode], drawingInfos);
  if (nodes)
    for (i = 0; nodes[i] >= 0; i++)
      {
	set = FALSE;
	/* We print them only if they are not part of marks. */
	for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
	  {
	    mark = (struct MarkInfo_struct*)(tmpLst->data);
	    set = set || ( (mark->type == MARK_DISTANCE &&
			    (guint)nodes[i] == mark->idNode2) ||
			   (mark->type == MARK_HIGHLIGHT &&
			    (guint)nodes[i] == mark->idNode1) );
	  }
	if (!set)
	  g_string_append_printf(output, "    <node id=\"%d\" />\n", nodes[i] + 1);
      }
  for (tmpLst = marks->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)(tmpLst->data);
      if (mark->type == MARK_DISTANCE)
	g_string_append_printf(output, "    <distance ref=\"%d\" id=\"%d\" />\n",
			       mark->idNode1 + 1, mark->idNode2 + 1);
      else if (mark->type == MARK_ANGLE)
	g_string_append_printf(output,
			       "    <angle ref=\"%d\" ref2=\"%d\" id=\"%d\" />\n",
			       mark->idNode1 + 1, mark->idNode2 + 1,
			       mark->idNode3 + 1);
      else if (mark->type == MARK_HIGHLIGHT)
	g_string_append_printf(output, "    <node id=\"%d\" highlight=\"yes\" />\n",
			       mark->idNode1 + 1);
    }
  g_string_append(output, "  </pick>");

  valid = tool_XML_substitute(output, filename, "pick", error);
  if (!valid)
    {
      g_string_free(output, TRUE);
      return FALSE;
    }
  
  valid = g_file_set_contents(filename, output->str, -1, error);
  g_string_free(output, TRUE);
  return valid;
}
