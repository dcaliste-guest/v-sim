/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2013-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2013-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "nodes.h"

#include <visu_object.h>
#include <visu_nodes.h>
#include <visu_data.h>
#include <opengl.h>
#include <coreTools/toolColor.h>
#include <openGLFunctions/objectList.h>

/**
 * SECTION:nodes
 * @short_description: Defines methods to draw a set of nodes.
 *
 * <para></para>
 */

typedef struct _GlIds
{
  GLuint material, element, nodes;
} GlIds;

/**
 * VisuGlExtNodesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtNodesClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtNodes:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtNodesPrivate:
 *
 * Private fields for #VisuGlExtNodes objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtNodesPrivate
{
  gboolean dispose_has_run;

  VisuGlExtNodesEffects effect;

  VisuRendering *method;
  guint nEle;
  GArray *glIds;

  VisuGlView *view;
  VisuData *dataObj;
  gulong popId, posId, popIncId, popDecId, visId, renId, eleId, precId;
};

#define VISU_GL_EXT_NODES_ID "Nodes"

/* Local routines. */
static void visu_gl_ext_nodes_dispose (GObject* obj);
static void visu_gl_ext_nodes_finalize(GObject* obj);
static void visu_gl_ext_nodes_rebuild (VisuGlExt *ext);

static void compileElementMaterial(VisuGlExtNodes *ext,
                                   GLuint displayList, VisuElement *ele);
static void compileElement(VisuGlExtNodes *ext, GLuint displayList, VisuElement *ele);
static void callNode(VisuGlExtNodes *ext, VisuElement *ele, VisuNode *node, GLuint eleId);

static void createGlLists(VisuGlExtNodes *ext, guint nEle);
static void createAllElements(VisuGlExtNodes *ext);
static void createNodes(VisuGlExtNodes *ext, VisuElement *ele);
static void createAllNodes(VisuGlExtNodes *ext);

/* Signal callbacks. */
static void onPopulationDefined(VisuNodeArray *array, guint nEle, gpointer data);
static void onPopulationChanged(VisuData *dataObj, gint *ids, gpointer data);
static void onPositionChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void onRenderingChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void onElementRenderingChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void onVisibilityChanged(VisuData *dataObj, gpointer data);
static void onPrecisionChanged(VisuGlView *view, gpointer data);
static void onRenderingMethodChanged(VisuGlExtNodes *nodes,
                                     VisuRendering *method, VisuObject *obj);

G_DEFINE_TYPE(VisuGlExtNodes, visu_gl_ext_nodes, VISU_TYPE_GL_EXT)

static void visu_gl_ext_nodes_class_init(VisuGlExtNodesClass *klass)
{
  DBG_fprintf(stderr, "Extension Nodes: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose      = visu_gl_ext_nodes_dispose;
  G_OBJECT_CLASS(klass)->finalize     = visu_gl_ext_nodes_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_nodes_rebuild;

  g_type_class_add_private(klass, sizeof(VisuGlExtNodesPrivate));
}

static void visu_gl_ext_nodes_init(VisuGlExtNodes *obj)
{
  VisuGlExt *ext;

  DBG_fprintf(stderr, "Extension Nodes: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_GL_EXT_NODES,
                                          VisuGlExtNodesPrivate);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->effect = VISU_GL_EXT_NODES_NO_EFFECT;
  obj->priv->glIds = g_array_new(FALSE, FALSE, sizeof(GlIds));
  obj->priv->nEle = 0;
  obj->priv->method = (VisuRendering*)0;
  obj->priv->view = (VisuGlView*)0;
  obj->priv->dataObj = (VisuData*)0;
  obj->priv->popId = 0;
  obj->priv->posId = 0;
  obj->priv->popIncId = 0;
  obj->priv->popDecId = 0;
  obj->priv->visId = 0;
  obj->priv->renId = 0;
  obj->priv->eleId = 0;
  obj->priv->precId = 0;

  /* Parent data. */
  ext = VISU_GL_EXT(obj);
  visu_gl_ext_setPriority(ext, VISU_GL_EXT_PRIORITY_NODES);
  visu_gl_ext_setSensitiveToRenderingMode(ext, TRUE);

  visu_gl_ext_nodes_setRendering(obj, visu_object_getRendering(VISU_OBJECT_INSTANCE));
  g_signal_connect_object(VISU_OBJECT_INSTANCE, "renderingChanged",
                          G_CALLBACK(onRenderingMethodChanged),
                          (gpointer)obj, G_CONNECT_SWAPPED | G_CONNECT_AFTER);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_nodes_dispose(GObject* obj)
{
  VisuGlExtNodes *nodes;

  DBG_fprintf(stderr, "Extension Nodes: dispose object %p.\n", (gpointer)obj);

  nodes = VISU_GL_EXT_NODES(obj);
  if (nodes->priv->dispose_has_run)
    return;
  nodes->priv->dispose_has_run = TRUE;

  visu_gl_ext_nodes_setData(nodes, (VisuGlView*)0, (VisuData*)0);
  visu_gl_ext_nodes_setRendering(nodes, (VisuRendering*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_nodes_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_nodes_finalize(GObject* obj)
{
  VisuGlExtNodesPrivate *nodes;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Nodes: finalize object %p.\n", (gpointer)obj);

  nodes = VISU_GL_EXT_NODES(obj)->priv;

  /* Free privs elements. */
  DBG_fprintf(stderr, "Extension Nodes: free private nodes.\n");
  g_array_free(nodes->glIds, TRUE);

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Nodes: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_nodes_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Nodes: freeing ... OK.\n");
}

/**
 * visu_gl_ext_nodes_new:
 *
 * Creates a new #VisuGlExt to draw a set of nodes. It can be used
 * also for picking, see visu_gl_ext_nodes_getSelection().
 *
 * Since: 3.7
 *
 * Returns: a pointer to the VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtNodes* visu_gl_ext_nodes_new()
{
  char *name = VISU_GL_EXT_NODES_ID;
  char *description = _("Draw all the nodes.");
  VisuGlExtNodes *extensionNodes;

  DBG_fprintf(stderr,"Extension Nodes: new object.\n");
  
  extensionNodes = VISU_GL_EXT_NODES(g_object_new(VISU_TYPE_GL_EXT_NODES,
                                                  "name", name, "label", _(name),
                                                  "description", description,
                                                  "nGlObj", 1, NULL));

  return extensionNodes;
}

static void visu_gl_ext_nodes_rebuild(VisuGlExt *ext)
{
  VisuGlExtNodes *extNodes;
  VisuNodeArray *nodes;
  VisuNodeArrayIter iter;

  extNodes = VISU_GL_EXT_NODES(ext);
  DBG_fprintf(stderr, "Extension Nodes: rebuilding object list for visuData %p.\n",
              (gpointer)extNodes->priv->dataObj);

  if (!extNodes->priv->dataObj || !extNodes->priv->method)
    glDeleteLists(visu_gl_ext_getGlList(ext), 1);
  else
    {
      nodes = VISU_NODE_ARRAY(extNodes->priv->dataObj);
      visu_node_array_iterNew(nodes, &iter);
      for (visu_node_array_iterStart(nodes, &iter); iter.element;
           visu_node_array_iterNextElement(nodes, &iter))
        visu_element_createMaterial(iter.element);
      createGlLists(extNodes, visu_node_array_getNElements(nodes, FALSE));
      createAllElements(extNodes);
      createAllNodes(extNodes);
    }
}

/**
 * createAllElements:
 * @data: a #VisuData object.
 * @view: a #VisuGlView object.
 *
 * This method will call the visu_rendering_createElement() method of the
 * current #RenderingMethod on all the elements of the given #VisuData.
 */
static void createAllElements(VisuGlExtNodes *ext)
{
  VisuNodeArray *array;
  VisuNodeArrayIter iter;
  GlIds *ids;

  g_return_if_fail(VISU_IS_GL_EXT_NODES(ext));

  array = VISU_NODE_ARRAY(ext->priv->dataObj);
  DBG_fprintf(stderr, "Extension Nodes: create OpenGl elements for"
	      " all VisuElement used in given VisuNodeArray %p.\n", (gpointer)array);
  visu_node_array_iterNew(array, &iter);
  g_return_if_fail(ext->priv->glIds->len >= iter.nElements);
  for (visu_node_array_iterStart(array, &iter); iter.element;
       visu_node_array_iterNextElement(array, &iter))
    {
      ids = ((GlIds*)ext->priv->glIds->data) + iter.iElement;
      compileElementMaterial(ext, ids->material, iter.element);
      compileElement(ext, ids->element, iter.element);
    }
  DBG_fprintf(stderr, "Extension Nodes: creation done for all elements.\n");
}

/**
 * createAllNodes: 
 * @data: a #VisuData object.
 *
 * This create the glObjectList registered that contains all the
 * nodes. This glObjectList is made of all nodes of all element whose
 * attribute rendered is TRUE and translated to their own positions.
 */
static void createAllNodes(VisuGlExtNodes *ext)
{
  VisuNodeArrayIter iter;
  guint i;
#if DEBUG == 1
  GTimer *timer;
  gulong fractionTimer;
#endif

  g_return_if_fail(VISU_IS_GL_EXT_NODES(ext));

  DBG_fprintf(stderr, "##### All node creation #####\n");
  DBG_fprintf(stderr, "Extension Nodes: 'createAllNodes' called.\n");

#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif

  visu_node_array_iterNew(VISU_NODE_ARRAY(ext->priv->dataObj), &iter);
  DBG_fprintf(stderr, "Extension Nodes: loop on elements.\n");
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(ext->priv->dataObj), &iter); iter.element;
       visu_node_array_iterNextElement(VISU_NODE_ARRAY(ext->priv->dataObj), &iter))
    createNodes(ext, iter.element);
  DBG_fprintf(stderr, "Extension Nodes: OK.\n");

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(ext)), GL_COMPILE);
  glLineWidth(1);
  DBG_fprintf(stderr, "Extension Nodes: create nodes for %d elements.\n", ext->priv->nEle);
  for (i = 0; i < ext->priv->nEle; i++)
    glCallList(g_array_index(ext->priv->glIds, GlIds, i).nodes);
  glEndList();

#if DEBUG == 1
  g_timer_stop(timer);
  fprintf(stderr, "Extension Nodes: lists built in %g micro-s.\n",
          g_timer_elapsed(timer, &fractionTimer)/1e-6);
  g_timer_destroy(timer);
#endif
}

static void compileElementMaterial(VisuGlExtNodes *ext, GLuint displayList, VisuElement *ele)
{
  GLuint id;
  float rgba[4], hsl[3], mat[VISU_GL_LIGHT_MATERIAL_N_VALUES];

  id = visu_element_getMaterialId(ele);

  glNewList(displayList, GL_COMPILE);
  if (ext->priv->effect == VISU_GL_EXT_NODES_FLATTEN ||
      ext->priv->effect == VISU_GL_EXT_NODES_FLATTEN_DARK ||
      ext->priv->effect == VISU_GL_EXT_NODES_FLATTEN_LIGHT)
    {
      switch (ext->priv->effect)
        {
        case (VISU_GL_EXT_NODES_FLATTEN_DARK):  mat[VISU_GL_LIGHT_MATERIAL_AMB] = .2f; break;
        case (VISU_GL_EXT_NODES_FLATTEN):       mat[VISU_GL_LIGHT_MATERIAL_AMB] = .75f; break;
        case (VISU_GL_EXT_NODES_FLATTEN_LIGHT): mat[VISU_GL_LIGHT_MATERIAL_AMB] = 1.f; break;
        default: break;
        }
      mat[VISU_GL_LIGHT_MATERIAL_DIF] = 0.f;
      mat[VISU_GL_LIGHT_MATERIAL_SHI] = 0.f;
      mat[VISU_GL_LIGHT_MATERIAL_SPE] = 0.f;
      mat[VISU_GL_LIGHT_MATERIAL_EMI] = 0.f;
      visu_gl_setColor(mat, ele->rgb);
    }
  else if (ext->priv->effect != VISU_GL_EXT_NODES_NO_EFFECT)
    {
      tool_color_convertRGBtoHSL(hsl, ele->rgb);
      switch (ext->priv->effect)
        {
        case (VISU_GL_EXT_NODES_DESATURATE): hsl[1] = 0.f; break;
        case (VISU_GL_EXT_NODES_SATURATE):   hsl[1] = 1.f; break;
        case (VISU_GL_EXT_NODES_DARKEN):     hsl[2] -= 0.2f; break;
        case (VISU_GL_EXT_NODES_LIGHTEN):    hsl[2] += 0.2f; break;
        default: break;
        }
      tool_color_convertHSLtoRGB(rgba, hsl);
      rgba[3] = ele->rgb[3];
      visu_gl_setColor(ele->material, rgba);
    }
  else
    glCallList(id);
  glEndList();
}
static void compileElement(VisuGlExtNodes *ext, GLuint displayList, VisuElement *ele)
{
  GLuint id;

  DBG_fprintf(stderr, "Extension Nodes: create element '%s' %p.\n",
              ele->name, (gpointer)ele);
  id = visu_rendering_createElement(ext->priv->method, ele, ext->priv->view);

  glNewList(displayList, GL_COMPILE);
  if (ext->priv->effect != VISU_GL_EXT_NODES_NO_EFFECT)
    glScalef(0.75f, 0.75f, 0.75f);
  glCallList(id);
  glEndList();
}
static void callNode(VisuGlExtNodes *ext, VisuElement *ele, VisuNode *node, GLuint eleId)
{
  visu_rendering_createNode(ext->priv->method, ext->priv->dataObj,
                            node, ele, eleId);
}
	
/**
 * createNodes: 
 * @data: a #VisuData object ;
 * @ele: a #VisuElement object.
 *
 * This creates the glObjectList that contains all the nodes of the given #VisuElement.
 */
static void createNodes(VisuGlExtNodes *ext, VisuElement *ele)
{
  VisuNodeArrayIter iter;
  guint iElement;
  GlIds *ids;

  g_return_if_fail(VISU_IS_GL_EXT_NODES(ext) && VISU_IS_ELEMENT_TYPE(ele));

  if (!ele->materialIsUpToDate)
    visu_element_createMaterial(ele);

  iElement = visu_node_array_getElementId(VISU_NODE_ARRAY(ext->priv->dataObj), ele);
  g_return_if_fail(iElement < ext->priv->glIds->len);
  DBG_fprintf(stderr, "Extension Nodes: create list of element '%s' (%d).\n",
              ele->name, iElement);
  ids = &g_array_index(ext->priv->glIds, GlIds, iElement);
  DBG_fprintf(stderr, " | list of nodes %d.\n", ids->nodes);
  glNewList(ids->nodes, GL_COMPILE);
  if (ele->rendered)
    {
      glCallList(ids->material);
      DBG_fprintf(stderr, "Extension Nodes: creating glObjectList of nodes for '%s' - %d.\n",
		  ele->name, ids->material);

      visu_node_array_iterNew(VISU_NODE_ARRAY(ext->priv->dataObj), &iter);
      iter.element = ele;
      for(visu_node_array_iterRestartNode(VISU_NODE_ARRAY(ext->priv->dataObj), &iter);
          iter.node;
          visu_node_array_iterNextNode(VISU_NODE_ARRAY(ext->priv->dataObj), &iter))
        if (iter.node->rendered)
          {
            glLoadName((GLuint)iter.node->number);
            callNode(ext, ele, iter.node, ids->element);
          }
    }
  else
    DBG_fprintf(stderr, "Extension Nodes: skipping glObjectList of nodes for '%s' - %d.\n",
		ele->name, ids->nodes);
  glEndList();
}

static void createGlLists(VisuGlExtNodes *ext, guint nEle)
{
  guint len;

  ext->priv->nEle = nEle;
  DBG_fprintf(stderr, "Extension Nodes: setup population at %d.\n", ext->priv->nEle);

  if (ext->priv->nEle > ext->priv->glIds->len)
    {
      DBG_fprintf(stderr, " | increase nodes gl ids (%d).\n", ext->priv->glIds->len);
      len = ext->priv->glIds->len;
      /* Change glIds size according to nEle. */
      ext->priv->glIds = g_array_set_size(ext->priv->glIds, ext->priv->nEle);
      for (; len < ext->priv->nEle; len++)
        {
          ((GlIds*)ext->priv->glIds->data)[len].material = visu_gl_objectlist_new(1);
          ((GlIds*)ext->priv->glIds->data)[len].element = visu_gl_objectlist_new(1);
          ((GlIds*)ext->priv->glIds->data)[len].nodes = visu_gl_objectlist_new(1);
        }
    }
}

/**
 * visu_gl_ext_nodes_setRendering:
 * @nodes: a #VisuGlExtNodes object.
 * @method: (transfer full) (allow-none): a #VisuRendering method.
 *
 * Provide rendering method for nodes and elements.
 *
 * Since: 3.7
 **/
void visu_gl_ext_nodes_setRendering(VisuGlExtNodes *nodes, VisuRendering *method)
{
  g_return_if_fail(VISU_IS_GL_EXT_NODES(nodes));
  
  if (nodes->priv->method)
    g_object_unref(nodes->priv->method);
  nodes->priv->method = method;
  if (method)
    g_object_ref(method);
}
/**
 * visu_gl_ext_nodes_setData:
 * @nodes: the #VisuGlExtNodes object that will render @dataObj nodes.
 * @view: the #VisuGlView to render to.
 * @dataObj: the nodes.
 *
 * This setup @nodes to render nodes of @dataObj on @view. Any changes
 * on @dataObj nodes will be automatically updated on the GL lists
 * representing the nodes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_nodes_setData(VisuGlExtNodes *nodes, VisuGlView *view, VisuData *dataObj)
{
  g_return_if_fail(VISU_IS_GL_EXT_NODES(nodes));

  if (nodes->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->view), nodes->priv->precId);
      g_object_unref(nodes->priv->view);
    }
  if (nodes->priv->dataObj)
    {
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->eleId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->popId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->posId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->popIncId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->popDecId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->visId);
      g_signal_handler_disconnect(G_OBJECT(nodes->priv->dataObj), nodes->priv->renId);
      g_object_unref(nodes->priv->dataObj);
    }
  if (dataObj && view)
    {
      g_object_ref(view);
      g_object_ref(dataObj);
      nodes->priv->view = view;
      nodes->priv->dataObj = dataObj;
      nodes->priv->eleId =
        g_signal_connect_after(G_OBJECT(dataObj), "ElementRenderingChanged",
                               G_CALLBACK(onElementRenderingChanged), (gpointer)nodes);
      nodes->priv->popId =
        g_signal_connect(G_OBJECT(dataObj), "PopulationDefined",
                         G_CALLBACK(onPopulationDefined), (gpointer)nodes);
      nodes->priv->posId =
        g_signal_connect_after(G_OBJECT(dataObj), "PositionChanged",
                               G_CALLBACK(onPositionChanged), (gpointer)nodes);
      nodes->priv->popIncId =
        g_signal_connect_after(G_OBJECT(dataObj), "PopulationIncrease",
                               G_CALLBACK(onPopulationChanged), (gpointer)nodes);
      nodes->priv->popDecId =
        g_signal_connect_after(G_OBJECT(dataObj), "PopulationDecrease",
                               G_CALLBACK(onPopulationChanged), (gpointer)nodes);
      nodes->priv->visId =
        g_signal_connect_after(G_OBJECT(dataObj), "VisibilityChanged",
                               G_CALLBACK(onVisibilityChanged), (gpointer)nodes);
      nodes->priv->renId =
        g_signal_connect_after(G_OBJECT(dataObj), "RenderingChanged",
                               G_CALLBACK(onRenderingChanged), (gpointer)nodes);
      nodes->priv->precId =
	g_signal_connect(G_OBJECT(view), "DetailLevelChanged",
			 G_CALLBACK(onPrecisionChanged), (gpointer)nodes);

      createGlLists(nodes,
                    visu_node_array_getNElements(VISU_NODE_ARRAY(dataObj), FALSE));
    }
  else
    {
      nodes->priv->eleId = 0;
      nodes->priv->popId = 0;
      nodes->priv->posId = 0;
      nodes->priv->popIncId = 0;
      nodes->priv->popDecId = 0;
      nodes->priv->visId = 0;
      nodes->priv->renId = 0;
      nodes->priv->precId = 0;
      nodes->priv->view = (VisuGlView*)0;
      nodes->priv->dataObj = (VisuData*)0;
    }
}
/**
 * visu_gl_ext_nodes_setMaterialEffect:
 * @nodes: a #VisuGlExtNodes object.
 * @effect: a #VisuGlExtNodesEffects id.
 *
 * Changes the effect applied on the color used to render #VisuElement.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the effect has been changed.
 **/
gboolean visu_gl_ext_nodes_setMaterialEffect(VisuGlExtNodes *nodes,
                                             VisuGlExtNodesEffects effect)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODES(nodes), FALSE);

  if (nodes->priv->effect == effect)
    return FALSE;

  nodes->priv->effect = effect;
  createAllElements(nodes);
  return TRUE;
}
/**
 * visu_gl_ext_nodes_getData:
 * @nodes: a #VisuGlExtNodes object.
 *
 * Retrieves the associated #VisuData of @nodes.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuData object.
 **/
VisuData* visu_gl_ext_nodes_getData(VisuGlExtNodes *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_NODES(nodes), (VisuData*)0);

  return nodes->priv->dataObj;
}
/**
 * visu_gl_ext_nodes_draw:
 * @nodes: a #VisuGlExtNodes object.
 *
 * Create the OpenGL list that store nodes.
 *
 * Since: 3.7
 **/
void visu_gl_ext_nodes_draw(VisuGlExtNodes *nodes)
{
  g_return_if_fail(VISU_IS_GL_EXT_NODES(nodes));

  /* Nothing to draw; */
  if(!nodes->priv->dataObj || !nodes->priv->method ||
     !visu_gl_ext_getActive(VISU_GL_EXT(nodes)))
    {
      glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(nodes)), 1);
      return;
    }
  
  createAllElements(nodes);
  createAllNodes(nodes);
}

/********************/
/* Signal handlers. */
/********************/
static void onRenderingMethodChanged(VisuGlExtNodes *nodes,
                                     VisuRendering *method, VisuObject *obj _U_)
{
  visu_gl_ext_nodes_setRendering(nodes, method);
  visu_gl_ext_nodes_draw(nodes);
}
static void onPopulationDefined(VisuNodeArray *array _U_, guint nEle, gpointer data)
{
  createGlLists(VISU_GL_EXT_NODES(data), nEle);
}
static void onPopulationChanged(VisuData *dataObj _U_, gint *ids _U_, gpointer data)
{
  createAllNodes(VISU_GL_EXT_NODES(data));
}
static void onPositionChanged(VisuData *dataObj _U_, VisuElement *ele, gpointer data)
{
  VisuGlExtNodes *ext = VISU_GL_EXT_NODES(data);

  if (ele)
    createNodes(ext, ele);
  else
    createAllNodes(ext);
}
static void onVisibilityChanged(VisuData *dataObj _U_, gpointer data)
{
  createAllNodes(VISU_GL_EXT_NODES(data));
}
static void onRenderingChanged(VisuData *dataObj _U_, VisuElement *ele, gpointer data)
{
  VisuGlExtNodes *ext = VISU_GL_EXT_NODES(data);

  if (ele)
    createNodes(ext, ele);
  else
    createAllNodes(ext);
}
static void onElementRenderingChanged(VisuData *dataObj _U_, VisuElement *ele, gpointer data)
{
  VisuGlExtNodes *ext = VISU_GL_EXT_NODES(data);
  guint iElement;

  if (ele)
    {
      iElement = visu_node_array_getElementId(VISU_NODE_ARRAY(ext->priv->dataObj), ele);
      compileElementMaterial(ext, ((GlIds*)ext->priv->glIds->data)[iElement].material, ele);
      compileElement(ext, ((GlIds*)ext->priv->glIds->data)[iElement].element, ele);
    }
  else
    createAllElements(ext);
}
static void onPrecisionChanged(VisuGlView *view _U_, gpointer data)
{
  createAllElements(VISU_GL_EXT_NODES(data));
}

/****************/
/* OpenGL pick. */
/****************/
static int _getSelection(VisuGlExtNodes *ext, VisuGlView *view,
                         GLfloat x0, GLfloat y0, GLfloat w, GLfloat h,
                         GLuint *select_buf, GLsizei bufsize)
{
  float centre[3];
  GLint viewport[4] = {0, 0, 0, 0};
  int hits;
 
  g_return_val_if_fail(VISU_IS_GL_EXT_NODES(ext), 0);
   
  DBG_fprintf(stderr, "Extension Nodes: get nodes in region %gx%g - %gx%g.\n",
              x0, x0, w, h);

  if ((w == 0) || (h == 0))
    return 0;

  visu_box_getCentre(visu_boxed_getBox(VISU_BOXED(view)), centre);
  
  glSelectBuffer(bufsize, select_buf);
  hits = glRenderMode(GL_SELECT);
  glInitNames();
  glPushName(-1);
   
  viewport[2] = view->window->width;
  viewport[3] = view->window->height;

  glNewList(10, GL_COMPILE);
  gluPickMatrix(x0 , y0, w, h, viewport);
  glEndList();

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glCallList(10);
  glFrustum(view->window->left, view->window->right, view->window->bottom,
            view->window->top, view->window->near, view->window->far);
  glMatrixMode(GL_MODELVIEW); 
  glPushMatrix();
  glTranslated(-centre[0], -centre[1], -centre[2]);
  glCallList(visu_gl_ext_getGlList(VISU_GL_EXT(ext)));
  glFlush();

  hits = glRenderMode(GL_RENDER);
  DBG_fprintf(stderr, "%d elements are on the z buffer %gx%g - %gx%g.\n", hits,
              x0, y0, w, h);

  /* return the buffer to normal */
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);

  return hits;
}

/**
 * visu_gl_ext_nodes_getSelectionByRegion:
 * @ext: a #VisuGlExtNodes object;
 * @view: a #VisuGlView object;
 * @x1: a window coordinate;
 * @y1: a window coordinate;
 * @x2: a window coordinate;
 * @y2: a window coordinate.
 *
 * Get the #VisuNode ids in the picked region defined by (x1, y1) -
 * (x2, y2).
 * 
 * Since: 3.7
 *
 * Returns: (transfer full) (element-type guint): an empty list if no
 * node found, or a newly created list of ids if any.
 **/
GList* visu_gl_ext_nodes_getSelectionByRegion(VisuGlExtNodes *ext, VisuGlView *view,
                                              int x1, int y1, int x2, int y2)
{
  GList *lst;
  int i, hits, ptr, names;
  GLuint *select_buf;
  GLsizei bufsize;
  
  g_return_val_if_fail(VISU_IS_GL_EXT_NODES(ext) && ext->priv->dataObj, (GList*)0);

  bufsize = visu_node_array_getNNodes(VISU_NODE_ARRAY(ext->priv->dataObj)) * 4;
  select_buf = g_malloc(sizeof(GLuint) * bufsize);
  hits = _getSelection(ext, view, 0.5f * (x1 + x2),
                       (float)view->window->height - 0.5f * (y1 + y2),
                       (float)ABS(x2 - x1), (float)ABS(y2 - y1),
                       select_buf, bufsize);

  lst = (GList*)0;
  ptr = 0;
  for(i = 0; i < hits; i++)
    {
      names = select_buf[ptr];
      if (names != 1)
        {
          g_warning("OpenGL picking is not working???\n");
          return (GList*)0;
        }
      ptr += 3;
      lst = g_list_prepend(lst, GINT_TO_POINTER((int)select_buf[ptr]));
      ptr += 1;
    }
  g_free(select_buf);

  return lst;
}

/**
 * visu_gl_ext_nodes_getSelection:
 * @ext: a #VisuGlExtNodes object;
 * @view: a #VisuGlView object;
 * @x: a window coordinate;
 * @y: a window coordinate.
 *
 * Get the id of a #VisuNode on top of the z-buffer.
 *
 * Since: 3.7
 *
 * Returns: -1 if no node found, or its id.
 **/
int visu_gl_ext_nodes_getSelection(VisuGlExtNodes *ext, VisuGlView *view, int x, int y)
{
  int i, hits, ptr, names, number;
  GLuint *select_buf;
  GLsizei bufsize;
  unsigned int z1;
  unsigned int z1_sauve = UINT_MAX;

  g_return_val_if_fail(VISU_IS_GL_EXT_NODES(ext) && ext->priv->dataObj, -1);

  bufsize = visu_node_array_getNNodes(VISU_NODE_ARRAY(ext->priv->dataObj)) * 4;
  select_buf = g_malloc(sizeof(GLuint) * bufsize);
  hits = _getSelection(ext, view, x, (float)view->window->height - y, 2.f, 2.f,
                       select_buf, bufsize);

  ptr = 0;
  number = -1;
  for(i = 0; i < hits; i++)
    {
      names = select_buf[ptr];
      if (names != 1)
        {
          g_warning("OpenGL picking is not working???\n");
          return -1;
        }
      ptr = ptr + 1;
      z1 = select_buf[ptr];
      DBG_fprintf(stderr, " | z position %f for %d\n", (float)z1/0x7fffffff,
                  (int)select_buf[ptr + 2]);
      ptr = ptr + 2;
      if (z1 < z1_sauve) {
        z1_sauve = z1;
        number = (int)select_buf[ptr];
      }
      ptr = ptr + 1;
    }
  return number;
}
