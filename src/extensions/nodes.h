/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2013-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2013-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef NODES_H
#define NODES_H

#include <visu_extension.h>
#include <openGLFunctions/view.h>
#include <visu_data.h>
#include <visu_rendering.h>

G_BEGIN_DECLS

/**
 * VisuGlExtNodesEffects:
 * @VISU_GL_EXT_NODES_NO_EFFECT: no effect (apply pristine element
 * color and material).
 * @VISU_GL_EXT_NODES_DESATURATE: desaturate colour.
 * @VISU_GL_EXT_NODES_SATURATE: saturate colour.
 * @VISU_GL_EXT_NODES_DARKEN: darken colour.
 * @VISU_GL_EXT_NODES_LIGHTEN: lighten colour.
 * @VISU_GL_EXT_NODES_FLATTEN_DARK: render darker without light efect.
 * @VISU_GL_EXT_NODES_FLATTEN: render without light efect.
 * @VISU_GL_EXT_NODES_FLATTEN_LIGHT: render lighter without light efect.
 *
 * The rendering done by #VisuGlExtNodes can alter the color and
 * material of rendered nodes.
 *
 * Since: 3.7
 */
typedef enum
  {
    VISU_GL_EXT_NODES_NO_EFFECT,
    VISU_GL_EXT_NODES_DESATURATE,
    VISU_GL_EXT_NODES_SATURATE,
    VISU_GL_EXT_NODES_DARKEN,
    VISU_GL_EXT_NODES_LIGHTEN,
    VISU_GL_EXT_NODES_FLATTEN_DARK,
    VISU_GL_EXT_NODES_FLATTEN,
    VISU_GL_EXT_NODES_FLATTEN_LIGHT
  } VisuGlExtNodesEffects;

/**
 * VISU_TYPE_GL_EXT_NODES:
 *
 * return the type of #VisuGlExtNodes.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_NODES	     (visu_gl_ext_nodes_get_type ())
/**
 * VISU_GL_EXT_NODES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtNodes type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_NODES(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_NODES, VisuGlExtNodes))
/**
 * VISU_GL_EXT_NODES_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtNodesClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_NODES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_NODES, VisuGlExtNodesClass))
/**
 * VISU_IS_GL_EXT_NODES:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtNodes object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_NODES(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_NODES))
/**
 * VISU_IS_GL_EXT_NODES_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtNodesClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_NODES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_NODES))
/**
 * VISU_GL_EXT_NODES_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_NODES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_NODES, VisuGlExtNodesClass))

typedef struct _VisuGlExtNodes        VisuGlExtNodes;
typedef struct _VisuGlExtNodesPrivate VisuGlExtNodesPrivate;
typedef struct _VisuGlExtNodesClass   VisuGlExtNodesClass;

struct _VisuGlExtNodes
{
  VisuGlExt parent;

  VisuGlExtNodesPrivate *priv;
};

struct _VisuGlExtNodesClass
{
  VisuGlExtClass parent;
};

/**
 * visu_gl_ext_nodes_get_type:
 *
 * This method returns the type of #VisuGlExtNodes, use
 * VISU_TYPE_GL_EXT_NODES instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtNodes.
 */
GType visu_gl_ext_nodes_get_type(void);

VisuGlExtNodes* visu_gl_ext_nodes_new();
void visu_gl_ext_nodes_setData(VisuGlExtNodes *nodes, VisuGlView *view, VisuData *dataObj);
void visu_gl_ext_nodes_setRendering(VisuGlExtNodes *nodes, VisuRendering *method);
gboolean visu_gl_ext_nodes_setMaterialEffect(VisuGlExtNodes *nodes,
                                             VisuGlExtNodesEffects effect);

void visu_gl_ext_nodes_draw(VisuGlExtNodes *nodes);

VisuData* visu_gl_ext_nodes_getData(VisuGlExtNodes *nodes);
GList* visu_gl_ext_nodes_getSelectionByRegion(VisuGlExtNodes *ext, VisuGlView *view,
                                              int x1, int y1, int x2, int y2);
int visu_gl_ext_nodes_getSelection(VisuGlExtNodes *ext, VisuGlView *view, int x, int y);

G_END_DECLS

#endif
