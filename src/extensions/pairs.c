/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "pairs.h"

#include <math.h>
#include <GL/gl.h>

#include <visu_object.h>
#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>
#include <pairsModeling/wire.h>
#include <pairsModeling/cylinder.h>
#include <openGLFunctions/text.h>
#include <extraFunctions/dataNode.h>

/**
 * SECTION:pairs
 * @short_description: Draw links between nodes.
 *
 * <para>This extension draws links between nodes, depending on
 * #VisuPairExtension drawing capabilities.</para>
 *
 * Since: 3.7
 */

/**
 * VisuGlExtPairsClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtPairsClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPairs:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPairsPrivate:
 *
 * Private fields for #VisuGlExtPairs objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtPairsPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* Pairs definition. */
  VisuData *data;
  gulong popInc_signal, popDec_signal, visChg_signal, posChg_signal;
  VisuGlView *view;
  gulong detail_signal;
  gulong link_signal;

  /* Additional storage. */
  GHashTable *pairData;
};

typedef struct _PairDataProperties
{
  VisuPairExtension *ext;
  GArray *drawnPairs;
} PairDataProperties;

/**
 * VisuPairExtension:
 *
 * An Opaque structure.
 */
struct _VisuPairExtension
{
  /* Some variable to describe this OpenGL extension.
     The attribute name is mandatory since it is
     used to identify the method. */
  char* name;
  char* printName;
  char* description;

  gboolean sensitiveToFacette;

  VisuPairDrawFuncs draw;

  /* Ref counter. */
  guint refCount;
};

static GList *availableVisuPairExtensions;
static VisuPairExtension *defaultPairMethod;
static guint sensitiveToFacette;
static VisuGlExtPairs *defaultPairs = NULL;

#define BONDNUMBER_ID "bondNumber_data"
static VisuDataNode *dataNode;

#define FLAG_RESOURCES_PAIRS    "pairs_are_on"
#define DESC_RESOURCES_PAIRS    "Ask the opengl engine to draw pairs between elements ; boolean 0 or 1"
static gboolean pairsAreOn = FALSE;
#define FLAG_RESOURCES_FAV_PAIRS  "pairs_favoriteMethod"
#define DESC_RESOURCES_FAV_PAIRS  "Favorite method used to render files ; chain"
static gboolean readFavPairsMethod(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				   VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCES_PAIRS_DATA "pair_data"
#define DESC_RESOURCES_PAIRS_DATA "Draw pairs between [ele1] [ele2] [0. <= dmin] [0. <= dmax] [0. <= RGB <= 1.]x3"
static gboolean readPairsData(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			      VisuData *dataObj, VisuGlView *view, GError **error);
#define FLAG_RESOURCES_PAIR_LINK "pair_link"
#define DESC1_RESOURCES_PAIR_LINK "Draw a link between [ele1] [ele2] [0. <= dmin] [0. <= dmax]"
#define DESC2_RESOURCES_PAIR_LINK "                    [0. <= RGB <= 1.]x3 [bool: drawn] [bool: printLength] [string: method]"
static gboolean readPairLink(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj, VisuGlView *view, GError **error);
static void exportResourcesPairs(GString *data, VisuData *dataObj, VisuGlView *view);

static void visu_gl_ext_pairs_finalize(GObject* obj);
static void visu_gl_ext_pairs_dispose(GObject* obj);
static void visu_gl_ext_pairs_rebuild(VisuGlExt *ext);

/* Local callbacks. */
static void onPairsParametersChange(VisuGlView *view, gpointer data);
static void onNodePopulationChanged(VisuNodeArray *array, int *nodes, gpointer data);
static void onNodeVisibilityChanged(VisuNodeArray *array, gpointer data);
static void onNodePositionChanged(VisuNodeArray *array, VisuElement *ele, gpointer data);
static void onActiveChanged(VisuGlExtPairs *pairs, GParamSpec *pspec, gpointer data);
static gboolean onPairDataHook(GSignalInvocationHint *ihint, guint nvalues,
                               const GValue *param_values, gpointer data);
static void onEntryUsed(VisuGlExtPairs *pairs, gchar *key, VisuObject *obj);

/* Local routines. */
static PairDataProperties* _newPairDataProperties(VisuGlExtPairs *pairs, VisuPairLink *data);
static void _freePairProperties(gpointer data);
static void freeData(gpointer obj, gpointer data);
static gpointer newOrCopyData(gconstpointer orig, gpointer user_data);

G_DEFINE_TYPE(VisuGlExtPairs, visu_gl_ext_pairs, VISU_TYPE_GL_EXT)

static void visu_gl_ext_pairs_class_init(VisuGlExtPairsClass *klass)
{
  VisuConfigFileEntry *resourceEntry, *oldEntry;

  DBG_fprintf(stderr, "Extension Pairs: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Register a new NodeData. */
  dataNode = VISU_DATA_NODE(visu_data_node_new(BONDNUMBER_ID, G_TYPE_INT));
  visu_data_node_setLabel(dataNode, _("Bonds"));

  /* Initialise the pairs extensions. */
  availableVisuPairExtensions = (GList*)0;
  visu_gl_pairs_wire_init();
  visu_gl_pairs_cylinder_init();
  defaultPairMethod = (VisuPairExtension*)availableVisuPairExtensions->data;
  sensitiveToFacette = (defaultPairMethod->sensitiveToFacette)?1:0;

  /* Add resources. */
  visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                   FLAG_RESOURCES_PAIRS,
                                   DESC_RESOURCES_PAIRS,
                                   &pairsAreOn);
  visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
                            FLAG_RESOURCES_FAV_PAIRS,
                            DESC_RESOURCES_FAV_PAIRS,
                            1, readFavPairsMethod);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     FLAG_RESOURCES_PAIRS_DATA,
				     DESC_RESOURCES_PAIRS_DATA,
				     1, readPairsData);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_PAIR_LINK,
					  DESC1_RESOURCES_PAIR_LINK,
					  2, readPairLink);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesPairs);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_pairs_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_pairs_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_pairs_rebuild;

  g_type_class_ref(VISU_TYPE_PAIR_LINK);
}

static void visu_gl_ext_pairs_init(VisuGlExtPairs *obj)
{
  DBG_fprintf(stderr, "Extension Pairs: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtPairsPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->isBuilt    = FALSE;
  obj->priv->data          = (VisuData*)0;
  obj->priv->popInc_signal = 0;
  obj->priv->popDec_signal = 0;
  obj->priv->visChg_signal = 0;
  obj->priv->posChg_signal = 0;
  obj->priv->view          = (VisuGlView*)0;
  obj->priv->detail_signal = 0;
  obj->priv->pairData      = g_hash_table_new_full(g_direct_hash, g_direct_equal,
                                                   NULL, _freePairProperties);

  g_signal_connect(G_OBJECT(obj), "notify::active",
                   G_CALLBACK(onActiveChanged), (gpointer)0);

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCES_PAIRS,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);

  /* We listen for any VisuPairLink rendering changes. */
  obj->priv->link_signal =
    g_signal_add_emission_hook(g_signal_lookup("ParameterChanged", VISU_TYPE_PAIR_LINK),
                               0, onPairDataHook, (gpointer)obj, (GDestroyNotify)0);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_pairs_dispose(GObject* obj)
{
  VisuGlExtPairs *pairs;

  DBG_fprintf(stderr, "Extension Pairs: dispose object %p.\n", (gpointer)obj);

  pairs = VISU_GL_EXT_PAIRS(obj);
  if (pairs->priv->dispose_has_run)
    return;
  pairs->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_pairs_setData(pairs, (VisuGlView*)0, (VisuData*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_pairs_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_pairs_finalize(GObject* obj)
{
  VisuGlExtPairs *pairs;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Pairs: finalize object %p.\n", (gpointer)obj);

  pairs = VISU_GL_EXT_PAIRS(obj);

  /* Free privs elements. */
  if (pairs->priv)
    {
      DBG_fprintf(stderr, "Extension Pairs: free private pairs.\n");
      g_hash_table_destroy(pairs->priv->pairData);
      g_signal_remove_emission_hook(g_signal_lookup("ParameterChanged", VISU_TYPE_PAIR_LINK),
                                    pairs->priv->link_signal);
      g_free(pairs->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Pairs: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_pairs_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Pairs: freeing ... OK.\n");
}

/**
 * visu_gl_ext_pairs_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_PAIRS_ID).
 *
 * Creates a new #VisuGlExt to draw a pairs.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtPairs* visu_gl_ext_pairs_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_PAIRS_ID;
  char *description = _("Draw pairs between elements with a criterion of distance.");
  VisuGlExtPairs *pairs;

  DBG_fprintf(stderr,"Extension Pairs: new object.\n");
  
  pairs = VISU_GL_EXT_PAIRS(g_object_new(VISU_TYPE_GL_EXT_PAIRS,
                                         "name", (name)?name:name_, "label", _(name),
                                         "description", description,
                                         "nGlObj", 1, NULL));
  visu_gl_ext_setPriority(VISU_GL_EXT(pairs), VISU_GL_EXT_PRIORITY_NORMAL);
  visu_gl_ext_setSensitiveToRenderingMode(VISU_GL_EXT(pairs), TRUE);

  return pairs;
}
/**
 * visu_gl_ext_pairs_setData:
 * @pairs: The #VisuGlExtPairs to attached to.
 * @view: the view where to draw the pairs.
 * @data: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the pairs to get the
 * node population also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_pairs_draw() should be called and
 * then 'OpenGLAskForReDraw' signal be emitted.
 **/
gboolean visu_gl_ext_pairs_setData(VisuGlExtPairs *pairs, VisuGlView *view, VisuData *data)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_PAIRS(pairs), FALSE);

  if (pairs->priv->data)
    {
      g_signal_handler_disconnect(G_OBJECT(pairs->priv->data), pairs->priv->popInc_signal);
      g_signal_handler_disconnect(G_OBJECT(pairs->priv->data), pairs->priv->popDec_signal);
      g_signal_handler_disconnect(G_OBJECT(pairs->priv->data), pairs->priv->visChg_signal);
      g_signal_handler_disconnect(G_OBJECT(pairs->priv->data), pairs->priv->posChg_signal);
      g_object_unref(pairs->priv->data);
    }
  if (data)
    {
      g_object_ref(data);
      pairs->priv->popInc_signal =
        g_signal_connect(G_OBJECT(data), "PopulationIncrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)pairs);
      pairs->priv->popDec_signal =
        g_signal_connect(G_OBJECT(data), "PopulationDecrease",
                         G_CALLBACK(onNodePopulationChanged), (gpointer)pairs);
      pairs->priv->visChg_signal =
        g_signal_connect(G_OBJECT(data), "VisibilityChanged",
                         G_CALLBACK(onNodeVisibilityChanged), (gpointer)pairs);
      pairs->priv->posChg_signal =
        g_signal_connect(G_OBJECT(data), "PositionChanged",
                         G_CALLBACK(onNodePositionChanged), (gpointer)pairs);

      visu_data_node_setUsed(dataNode, data,
                             (visu_gl_ext_getActive(VISU_GL_EXT(pairs)))?1:0);
    }
  else
    {
      pairs->priv->popInc_signal = 0;
      pairs->priv->popDec_signal = 0;
      pairs->priv->visChg_signal = 0;
      pairs->priv->posChg_signal = 0;
    }
  pairs->priv->data = data;

  if (pairs->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(pairs->priv->view), pairs->priv->detail_signal);
      g_object_unref(pairs->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      pairs->priv->detail_signal =
        g_signal_connect(G_OBJECT(view), "DetailLevelChanged",
                         G_CALLBACK(onPairsParametersChange), (gpointer)pairs);
    }
  else
    pairs->priv->detail_signal = 0;
  pairs->priv->view = view;

  pairs->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(pairs));
}
/**
 * visu_gl_ext_pairs_setDrawMethod:
 * @pairs: the rendering #VisuGlExtPairs object.
 * @data: a #VisuPairLink object.
 * @ext: a #VisuPairExtension object.
 * 
 * Set the drawing method of a pair.
 *
 * Since: 3.6
 *
 * Returns: TRUE if drawing method is changed.
 */
gboolean visu_gl_ext_pairs_setDrawMethod(VisuGlExtPairs *pairs,
                                         VisuPairLink *data, VisuPairExtension *ext)
{
  PairDataProperties *prop;

  g_return_val_if_fail(VISU_IS_GL_EXT_PAIRS(pairs) && data, FALSE);

  DBG_fprintf(stderr, "Visu Pairs: set method %p for %p.\n",
	      (gpointer)ext, (gpointer)data);
  prop = _newPairDataProperties(pairs, data);
  if (prop->ext == ext)
    return FALSE;

  if (prop->ext && prop->ext->sensitiveToFacette)
    sensitiveToFacette -= 1;
  prop->ext = ext;
  if (prop->ext && prop->ext->sensitiveToFacette)
    sensitiveToFacette += 1;
  pairs->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(pairs));
}
/**
 * visu_gl_ext_pairs_getNDrawn:
 * @pairs: the rendering #VisuGlExtPairs object.
 * @data: a #VisuPairLink object.
 *
 * This returns the number of pairs with characteristics of @data,
 * that have been drawn.
 *
 * Returns: the number of drawn pairs.
 *
 * Since: 3.7
 */
guint visu_gl_ext_pairs_getNDrawn(VisuGlExtPairs *pairs, VisuPairLink *data)
{
  PairDataProperties *prop;

  g_return_val_if_fail(VISU_IS_GL_EXT_PAIRS(pairs), FALSE);

  prop = (PairDataProperties*)g_hash_table_lookup(pairs->priv->pairData, (gpointer)data);
  if (!prop)
    return 0;
  else
    return prop->drawnPairs->len;
}
/**
 * visu_gl_ext_pairs_getDrawnPair:
 * @pairs: the rendering #VisuGlExtPairs object.
 * @data: a #VisuPairLink object ;
 * @node1: (out): a location to store a node id ;
 * @node2: (out): a location to store a node id ;
 * @id: an index value.
 *
 * After pairs have been drawn, the ids of nodes in between links have
 * been done can be retrieved with this routine. @id range in between
 * 0 and the value returned by visu_gl_ext_pairs_getNDrawn().
 *
 * Returns: TRUE if @id is a valid number.
 *
 * Since: 3.7
 */
gboolean visu_gl_ext_pairs_getDrawnPair(VisuGlExtPairs *pairs, VisuPairLink *data,
                                        guint *node1, guint *node2, guint id)
{
  PairDataProperties *prop;
  guint *nodeIds;

  g_return_val_if_fail(VISU_IS_GL_EXT_PAIRS(pairs) && node1 && node2, FALSE);

  prop = (PairDataProperties*)g_hash_table_lookup(pairs->priv->pairData, (gpointer)data);
  if (!prop || id >= prop->drawnPairs->len)
    return FALSE;
  else
    {
      nodeIds = &g_array_index(prop->drawnPairs, guint, id);
      *node1 = nodeIds[0];
      *node2 = nodeIds[1];
      return TRUE;
    }
}
/**
 * visu_gl_ext_pairs_getDrawMethod:
 * @pairs: the rendering #VisuGlExtPairs object.
 * @data: a #VisuPairLink object.
 * 
 * Get the drawing method of a pair.
 *
 * Since: 3.6
 *
 * Returns: a drawing method.
 */
VisuPairExtension* visu_gl_ext_pairs_getDrawMethod(VisuGlExtPairs *pairs, VisuPairLink *data)
{
  PairDataProperties *prop;

  g_return_val_if_fail(VISU_IS_GL_EXT_PAIRS(pairs) && data, defaultPairMethod);

  prop = (PairDataProperties*)g_hash_table_lookup(pairs->priv->pairData, (gpointer)data);
  return (prop && prop->ext)?prop->ext:defaultPairMethod;
}
/**
 * visu_gl_ext_pairs_getDefault:
 *
 * V_Sim is using a default pair object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtPairs object used by default.
 **/
VisuGlExtPairs* visu_gl_ext_pairs_getDefault()
{
  if (!defaultPairs)
    {
      defaultPairs = visu_gl_ext_pairs_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultPairs), pairsAreOn);
    }
  return defaultPairs;
}

/************/
/* Signals. */
/************/
static void onPairsParametersChange(VisuGlView *view _U_, gpointer data)
{
  VisuGlExtPairs *pairs = VISU_GL_EXT_PAIRS(data);

  if (FALSE)
    {
      pairs->priv->isBuilt = FALSE;
      visu_gl_ext_pairs_draw(pairs);
    }
}
static gboolean idle_draw(gpointer data);
static void onNodePopulationChanged(VisuNodeArray *array _U_, int *nodes _U_,
				    gpointer data)
{
  VisuGlExtPairs *pairs = VISU_GL_EXT_PAIRS(data);

  pairs->priv->isBuilt = FALSE;
  g_idle_add(idle_draw, (gpointer)pairs);
}
static void onNodeVisibilityChanged(VisuNodeArray *array _U_, gpointer data)
{
  VisuGlExtPairs *pairs = VISU_GL_EXT_PAIRS(data);

  pairs->priv->isBuilt = FALSE;
  visu_gl_ext_pairs_draw(pairs);
}
static void onNodePositionChanged(VisuNodeArray *array _U_, VisuElement *ele _U_,
                                  gpointer data)
{
  VisuGlExtPairs *pairs = VISU_GL_EXT_PAIRS(data);

  pairs->priv->isBuilt = FALSE;
  visu_gl_ext_pairs_draw(pairs);
}
static void onActiveChanged(VisuGlExtPairs *pairs, GParamSpec *pspec _U_, gpointer data _U_)
{
  if (!pairs->priv->data || !dataNode)
    return;

  visu_data_node_setUsed(dataNode, pairs->priv->data,
                         (visu_gl_ext_getActive(VISU_GL_EXT(pairs)))?1:0);
}
static void onEntryUsed(VisuGlExtPairs *pairs, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(pairs), pairsAreOn);
}
static gboolean onPairDataHook(GSignalInvocationHint *ihint _U_, guint nvalues _U_,
                               const GValue *param_values, gpointer user_data)
{
  VisuGlExtPairs *pairs;
  VisuElement *ele1, *ele2;

  pairs = VISU_GL_EXT_PAIRS(user_data);
  if (!pairs->priv->data)
    return TRUE;

  visu_pair_getElements
    (visu_pair_link_getPair(VISU_PAIR_LINK(g_value_get_object(param_values))), &ele1, &ele2);
  pairs->priv->isBuilt = pairs->priv->isBuilt &&
    (visu_node_array_getElementId(VISU_NODE_ARRAY(pairs->priv->data), ele1) < 0 ||
     visu_node_array_getElementId(VISU_NODE_ARRAY(pairs->priv->data), ele2) < 0);
  DBG_fprintf(stderr, "Visu GlExt Pairs: some pair has changed (%d).\n",
              pairs->priv->isBuilt);
  return TRUE;
}

static void visu_gl_ext_pairs_rebuild(VisuGlExt *ext)
{
  VisuGlExtPairs *pairs = VISU_GL_EXT_PAIRS(ext);

  visu_gl_text_rebuildFontList();
  pairs->priv->isBuilt = FALSE;
  visu_gl_ext_pairs_draw(pairs);
}

static PairDataProperties* _newPairDataProperties(VisuGlExtPairs *pairs, VisuPairLink *data)
{
  PairDataProperties *prop;

  prop = (PairDataProperties*)g_hash_table_lookup(pairs->priv->pairData, (gpointer)data);
  if (!prop)
    {
#if GLIB_MINOR_VERSION > 9
      prop = g_slice_alloc(sizeof(PairDataProperties));
#else
      prop = g_malloc(sizeof(PairDataProperties));
#endif
      prop->drawnPairs = g_array_new(FALSE, FALSE, sizeof(guint) * 2);
      prop->ext = (VisuPairExtension*)0;
      g_hash_table_insert(pairs->priv->pairData, (gpointer)data, (gpointer)prop);
    }
  return prop;
}
static void _freePairProperties(gpointer data)
{
  PairDataProperties *prop = (PairDataProperties*)data;

  g_array_free(prop->drawnPairs, TRUE);
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(PairDataProperties), data);
#else
  g_free(data);
#endif
}
static void freeData(gpointer obj, gpointer data _U_)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(int), obj);
#else
  g_free(obj);
#endif
}
static gpointer newOrCopyData(gconstpointer orig, gpointer user_data _U_)
{
  int* data;

#if GLIB_MINOR_VERSION > 9
  data = g_slice_alloc(sizeof(int));
#else
  data = g_malloc(sizeof(int));
#endif
  if (orig)
    *data = *(int*)orig;
  else
    *data = 0;
    
  return (gpointer)data;
}
static int* getBond(VisuNodeArray *nodes, VisuNode *node)
{
  int *bond;
  GValue val = {0, {{0}, {0}}};

  g_value_init(&val, G_TYPE_POINTER);
  visu_node_array_getPropertyValue(nodes, node, BONDNUMBER_ID, &val);
  bond = (int*)g_value_get_pointer(&val);

  if (!bond)
    {
      bond = (int*)newOrCopyData((gconstpointer)0, (gpointer)0);
      g_value_set_pointer(&val, (gpointer)bond);
      visu_node_setpropertyValue(nodes, node, BONDNUMBER_ID, &val);
    }
  return bond;
}
/**
 * visu_gl_ext_pairs_draw:
 * @pairs: a #VisuGlExtPairs object.
 *
 * Draw the @pairs, see visu_gl_ext_pairs_setData() to attach
 * a #VisuData object to @pairs.
 *
 * Since: 3.7
 */
void visu_gl_ext_pairs_draw(VisuGlExtPairs *pairs)
{
  VisuNodeArray *nodes;
  VisuPairExtension *ext;
  VisuNodeArrayIter iter1, iter2;
  VisuPairLink *data;
  GList *tmpLst;
  gboolean useBond;
  int *bond1, *bond2;
  float mM[2];
  PairDataProperties *prop;
  float d2, d2min, d2max, d2min_buffered, d2max_buffered, l;
  float xyz1[3], xyz2[3], alpha;
  guint nodeIds[2];
  char distStr[8];

  g_return_if_fail(VISU_IS_GL_EXT_PAIRS(pairs));

  /* Nothing to draw if no data is associated to
     the current rendering window. */
  if (!pairs->priv->data || !pairs->priv->view)
    return;
  /* Nothing to draw; */
  if(!visu_gl_ext_getActive(VISU_GL_EXT(pairs)) || pairs->priv->isBuilt) return;
  nodes = VISU_NODE_ARRAY(pairs->priv->data);

  /* Cleans. */
  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(pairs)), 1);

  visu_gl_text_initFontList();

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(pairs)), GL_COMPILE);

  /* We get the counting array for bonds. */
  if (!visu_node_array_getProperty(nodes, (const gchar*)BONDNUMBER_ID))
    {
      /* Create the Node property to count the number of bonds. */
      DBG_fprintf(stderr, "Visu GlExt Pairs: create the bond node property.\n");
      visu_node_array_property_newPointer(nodes, BONDNUMBER_ID, freeData,
                                          newOrCopyData, (gpointer)0);
    }
  useBond = TRUE;

  DBG_fprintf(stderr, "Visu GlExt Pairs: creating pairs between elements.\n");
  visu_node_array_iterNew(nodes, &iter1);
  visu_node_array_iterNew(nodes, &iter2);
  for(visu_node_array_iterStart(nodes, &iter1); iter1.element;
      visu_node_array_iterNextElement(nodes, &iter1))
    {
      if (!visu_element_getRendered(iter1.element))
        continue;

      /* Initialise the bond count. */
      if (useBond)
        for(visu_node_array_iterRestartNode(nodes, &iter1); iter1.node;
            visu_node_array_iterNextNode(nodes, &iter1))
          {
            bond1 = getBond(nodes, iter1.node);
            *bond1 = 0;
          }

      for(visu_node_array_iterStart(nodes, &iter2);
          iter2.element && iter2.iElement <= iter1.iElement ;
          visu_node_array_iterNextElement(nodes, &iter2))
        {
          if (!visu_element_getRendered(iter2.element))
            continue;
	  
          DBG_fprintf(stderr, "Visu GlExt Pairs: draw pairs between '%s' and '%s'.\n",
        	      iter1.element->name, iter2.element->name);
          for (tmpLst = visu_pair_link_getAll(iter1.element, iter2.element);
               tmpLst; tmpLst = g_list_next(tmpLst))
            {
              data = (VisuPairLink*)tmpLst->data;
              if (!visu_pair_link_isDrawn(data))
                continue;

              mM[0] = visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN);
              mM[1] = visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX);
	      d2min = mM[0] * mM[0];
	      d2max = mM[1] * mM[1];
	      l = mM[1] - mM[0];
	      d2min_buffered = (mM[0] - 0.15 * l);
	      d2min_buffered *= d2min_buffered;
	      d2max_buffered = (mM[1] + 0.15 * l);
	      d2max_buffered *= d2max_buffered;

              prop = _newPairDataProperties(pairs, data);
              DBG_fprintf(stderr, " | having properties %p.\n", (gpointer)prop);

              ext = (prop && prop->ext)?prop->ext:defaultPairMethod;
              DBG_fprintf(stderr, " | using %p '%s' method.\n", (gpointer)ext, ext->name);
              if (ext->draw.start)
        	ext->draw.start(iter1.element, iter2.element, data, pairs->priv->view);

              g_array_set_size(prop->drawnPairs, 0);
              for(visu_node_array_iterRestartNode(nodes, &iter1); iter1.node;
        	  visu_node_array_iterNextNode(nodes, &iter1))
        	{
        	  if (!iter1.node->rendered)
        	    continue;

        	  bond1 = (useBond)?getBond(nodes, iter1.node):(int*)0;
        	  for(visu_node_array_iterRestartNode(nodes, &iter2); iter2.node;
        	      visu_node_array_iterNextNode(nodes, &iter2))
        	    {
        	      if (!iter2.node->rendered)
        		continue;
        	      /* Don't draw the inter element pairs two times. */
        	      if (iter1.element == iter2.element &&
        		  iter2.node >= iter1.node)
        		break;

        	      visu_data_getNodePosition(pairs->priv->data, iter1.node, xyz1);
        	      visu_data_getNodePosition(pairs->priv->data, iter2.node, xyz2);
        	      d2 = (xyz1[0] - xyz2[0]) * (xyz1[0] - xyz2[0]) +
        		(xyz1[1] - xyz2[1]) * (xyz1[1] - xyz2[1]) +
        		(xyz1[2] - xyz2[2]) * (xyz1[2] - xyz2[2]);
        	      if(d2 <= 0. || d2 < d2min_buffered || d2 > d2max_buffered)
        		continue;

        	      if (d2 < d2min)
        		alpha = (d2 - d2min_buffered) /
        		  (d2min - d2min_buffered);
        	      else if (d2 > d2max)
        		alpha = (d2max_buffered - d2) /
        		  (d2max_buffered - d2max);
        	      else
        		{
        		  alpha = 1.f;
        		  /* Update bond count. */
        		  if (bond1)
        		    (*bond1) += 1;
        		  bond2 = (useBond)?getBond(nodes, iter2.node):(int*)0;
        		  if (bond2)
        		    (*bond2) += 1;
        		}

        	      ext->draw.main(iter1.element, iter2.element, data, pairs->priv->view,
                                xyz1[0], xyz1[1], xyz1[2], xyz2[0], xyz2[1], xyz2[2],
                                d2, alpha);
        	      if (visu_pair_link_getPrintLength(data))
        		{
        		  glRasterPos3f((xyz1[0] + xyz2[0]) / 2.,
        				(xyz1[1] + xyz2[1]) / 2.,
        				(xyz1[2] + xyz2[2]) / 2.);
        		  sprintf(distStr, "%7.3f", sqrt(d2));
        		  visu_gl_text_drawChars(distStr, VISU_GL_TEXT_NORMAL);
        		}
                      nodeIds[0] = iter1.node->number;
                      nodeIds[1] = iter2.node->number;
                      g_array_append_vals(prop->drawnPairs, nodeIds, 1);
        	    }
        	}
              if (ext->draw.stop)
        	ext->draw.stop(iter1.element, iter2.element, data, pairs->priv->view);

              DBG_fprintf(stderr, "Visu GlExt Pairs: have drawn %d pairs.\n",
                          prop->drawnPairs->len);
            }
        }
    }

  glEndList();

  /* Emit the valueChanged signal of dataNode. */
  visu_data_node_emitValueChanged(dataNode, pairs->priv->data);

  pairs->priv->isBuilt = TRUE;
}
static gboolean idle_draw(gpointer data)
{
  visu_gl_ext_pairs_draw(VISU_GL_EXT_PAIRS(data));
  return FALSE;
}

/**************/
/* Resources. */
/**************/
static gboolean readFavPairsMethod(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				   VisuData *dataObj _U_, VisuGlView *view _U_,
                                   GError **error)
{
  VisuPairExtension *method;

  g_return_val_if_fail(nbLines == 1, FALSE);

  lines[0] = g_strstrip(lines[0]);

  if (!lines[0][0])
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d, 1 string value must appear"
			     " after the %s markup.\n"), position,
			   FLAG_RESOURCES_FAV_PAIRS);
      return FALSE;
    }
  else
    {
      method = visu_pair_extension_getByName(lines[0]);
      if (!method)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			       _("Parse error at line %d, the method"
				 " '%s' is unknown.\n"), position, lines[0]);
	  return FALSE;
	}
      visu_pair_extension_setDefault(method);
    }

  return TRUE;
}
gboolean visu_pair_readLinkFromTokens(gchar **tokens, int *index, VisuPairLink **data,
				     int lineId, GError **error)
{
  VisuElement *ele[2];
  float minMax[2];
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(data, FALSE);
  g_return_val_if_fail(tokens && index, FALSE);
  
  /* Read the two elements. */
  res = tool_config_file_readElementFromTokens(tokens, index, ele, 2, lineId, error);
  if (!res)
    return FALSE;
  /* Read the two distances. */
  res = tool_config_file_readFloatFromTokens(tokens, index, minMax, 2, lineId, error);
  if (!res)
    return FALSE;
  
  /* Check the values. */
  res = FALSE;
  res = res || tool_config_file_clampFloat(&minMax[0], minMax[0], 0., -1.);
  res = res || tool_config_file_clampFloat(&minMax[1], minMax[1], 0., -1.);
  if (res)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d, given distance"
			     " are out of bounds (should be positive).\n"), lineId);
      return FALSE;
    }

  /* Set the values. */
  *data = visu_pair_link_new(ele[0], ele[1], minMax);
  g_return_val_if_fail(*data, FALSE);

  return TRUE;
}
static gboolean readPairLink(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			     VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gchar **tokens;
  int id;
  gboolean flags[2], res;
  VisuPairLink *data;
  float rgb[4];
  ToolColor *color;
  gchar *methodName;
  VisuPairExtension *method;

  g_return_val_if_fail(nbLines == 2, FALSE);

  tokens = g_strsplit_set(lines[0], " \t\n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  if (!visu_pair_readLinkFromTokens(tokens, &id, &data, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  /* Read second line with basic data. */
  tokens = g_strsplit_set(lines[1], " \t\n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  /* Read the color. */
  if (!tool_config_file_readFloatFromTokens(tokens, &id, rgb, 3, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the flags drawn and printLength. */
  if (!tool_config_file_readBooleanFromTokens(tokens, &id, flags, 2, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the specific method, if any. */
  methodName = g_strjoinv(" ", tokens + id);
  if (methodName && methodName[0])
    {
      method = visu_pair_extension_getByName(methodName);
      if (!method)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			       _("Parse error at line %d, the method"
				 " '%s' is unknown.\n"), position + 1, methodName);
          g_free(methodName);
          g_strfreev(tokens);
	  return FALSE;
	}
    }
  else
    method = (VisuPairExtension*)0;
  if (methodName)
    g_free(methodName);
  /* Global free. */
  g_strfreev(tokens);

  /* Check the values. */
  res = tool_config_file_clampFloat(&rgb[0], rgb[0], 0., 1.) ||
    tool_config_file_clampFloat(&rgb[1], rgb[1], 0., 1.) ||
    tool_config_file_clampFloat(&rgb[2], rgb[2], 0., 1.);
  if (res)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d, 3 floating points"
			     "(0 <= v <= 1) must appear after the %s markup.\n"),
			   position, FLAG_RESOURCES_PAIRS_DATA);
      return FALSE;
    }


  /* Set the values. */
  rgb[3] = 1.f;
  color = tool_color_getByValues((int*)0, rgb[0], rgb[1], rgb[2], rgb[3]);
  if (!color)
    color = tool_color_addFloatRGBA(rgb, (int*)0);
  visu_pair_link_setColor(data, color);
  visu_pair_link_setDrawn(data, flags[0]);
  visu_pair_link_setPrintLength(data, flags[1]);
  visu_gl_ext_pairs_setDrawMethod(visu_gl_ext_pairs_getDefault(), data, method);

  return TRUE;
}
static void exportLevel1(VisuElement *ele1, VisuElement *ele2,
                         VisuPairLink *data, gpointer user_data)
{
  PairDataProperties *prop;
  struct _VisuConfigFileForeachFuncExport *str;
  ToolColor *color;
  gchar *buf;
  
  str = (struct _VisuConfigFileForeachFuncExport*)user_data;
  
  /* We export only if the two elements of the pair are used in the given dataObj. */
  if (str->dataObj &&
      (!visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele1) >= 0 ||
       !visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele2) >= 0))
    return;

  color = visu_pair_link_getColor(data);
  buf = g_strdup_printf("%s %s  %4.3f %4.3f", visu_element_getName(ele1), 
                        visu_element_getName(ele2),
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));
  prop = (PairDataProperties*)g_hash_table_lookup(visu_gl_ext_pairs_getDefault()->priv->pairData, (gpointer)data);
  visu_config_file_exportEntry(str->data, FLAG_RESOURCES_PAIR_LINK, buf,
                               "%4.3f %4.3f %4.3f  %d  %d  %s",
                               color->rgba[0], color->rgba[1], color->rgba[2],
                               visu_pair_link_getDrawn(data),
                               visu_pair_link_getPrintLength(data),
                               (prop && prop->ext)?prop->ext->name:
                               defaultPairMethod->name);
  g_free(buf);
}
static void exportResourcesPairs(GString *data, VisuData *dataObj, VisuGlView *view _U_)
{
  struct _VisuConfigFileForeachFuncExport str;
  GList *pairsMeth;
  GString *buf;

  visu_config_file_exportComment(data, DESC_RESOURCES_PAIRS);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_PAIRS, NULL,
                               "%i", (defaultPairs)?visu_gl_ext_getActive(VISU_GL_EXT(defaultPairs)):pairsAreOn);

  if (defaultPairMethod)
    {
      buf = g_string_new("");
      g_string_append_printf(buf, "%s (", DESC_RESOURCES_FAV_PAIRS);
      for (pairsMeth = availableVisuPairExtensions; pairsMeth;
           pairsMeth = g_list_next(pairsMeth))
	{
	  g_string_append_printf(buf, "'%s'", ((VisuPairExtension*)pairsMeth->data)->name);
	  if (pairsMeth->next)
	    g_string_append_printf(buf, ", ");	  
	}
      g_string_append_printf(buf, ")");
      visu_config_file_exportComment(data, buf->str);
      g_string_free(buf, TRUE);
      visu_config_file_exportEntry(data, FLAG_RESOURCES_FAV_PAIRS, NULL,
                                   "%s", defaultPairMethod->name);
    }

  visu_config_file_exportComment(data, DESC1_RESOURCES_PAIR_LINK);
  visu_config_file_exportComment(data, DESC2_RESOURCES_PAIR_LINK);
  str.data = data;
  str.dataObj = dataObj;
  visu_pair_foreach(exportLevel1, (gpointer)&str);

  visu_config_file_exportComment(data, "");
}


/*****************************************/
/* Methods to organize pairs extensions. */
/*****************************************/
/**
 * visu_pair_extension_get_type:
 *
 * Create and retrieve a #GType for a #VisuPairExtension object.
 *
 * Since: 3.7
 *
 * Returns: a new type for #VisuPairExtension structures.
 */
GType visu_pair_extension_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("VisuPairExtension", 
                                   (GBoxedCopyFunc)visu_pair_extension_ref,
                                   (GBoxedFreeFunc)visu_pair_extension_unref);
  return g_define_type_id;
}
/**
 * visu_pair_extension_new:
 * @name: name of the extension (must be non null) ;
 * @printName: a string to label the method that can be safely translated ;
 * @description: a brief description of the extension (can be null) ;
 * @sensitive: a boolean 0 or 1 ;
 * @meth: a set of drawing methods.
 *
 * This creates a new pairs extension. Such an extension describes how to draw
 * links (called pairs) between elements. The @sensitive argument is to inform if
 * pairs must be redrawn when the OpenGL engine sends the OpenGLFacetteChanged signal.
 *
 * Returns: the new VisuPairExtension or null if something wrong happens.
 */
VisuPairExtension* visu_pair_extension_new(const char* name, const char* printName,
                                           const char* description, gboolean sensitive,
                                           const VisuPairDrawFuncs *meth)
{
  VisuPairExtension *extension;

  g_return_val_if_fail(meth->main && name, (VisuPairExtension*)0);

  DBG_fprintf(stderr, "Visu Pairs: registering a new pairs extension ... ");
  extension = g_malloc(sizeof(VisuPairExtension));
  extension->refCount    = 1;

  extension->name        = g_strdup(name);
  extension->printName   = g_strdup(printName);
  extension->description = g_strdup(description);
  
  extension->sensitiveToFacette = sensitive;

  extension->draw        = *meth;

  availableVisuPairExtensions = g_list_append(availableVisuPairExtensions,
                                              (gpointer)extension);
  DBG_fprintf(stderr, "'%s' (%p).\n", extension->name, (gpointer)extension);

  return extension;
}
/**
 * visu_pair_extension_ref:
 * @ext: a #VisuPairExtension object.
 *
 * Increase the ref counter.
 *
 * Since: 3.7
 *
 * Returns: itself.
 **/
VisuPairExtension* visu_pair_extension_ref(VisuPairExtension *ext)
{
  ext->refCount += 1;
  return ext;
}
/**
 * visu_pair_extension_unref:
 * @ext: a #VisuPairExtension object.
 *
 * Decrease the ref counter, free all memory if counter reachs zero.
 *
 * Since: 3.7
 **/
void visu_pair_extension_unref(VisuPairExtension *ext)
{
  ext->refCount -= 1;
  if (!ext->refCount)
    visu_pair_extension_free(ext);
}
/**
 * visu_pair_extension_free:
 * @extension: the extension to delete.
 *
 * Free all the allocated attributes of the specified method.
 */
void visu_pair_extension_free(VisuPairExtension* extension)
{
  if (!extension)
    return;
  if (extension->name)
    g_free(extension->name);
  if (extension->printName)
    g_free(extension->printName);
  if (extension->description)
    g_free(extension->description);
  availableVisuPairExtensions = g_list_remove(availableVisuPairExtensions,
                                              (gpointer)extension);
  g_free(extension);
}
/**
 * visu_pair_extension_setDefault:
 * @extension: a #VisuPairExtension object.
 *
 * Choose the method used to draw pairs.
 *
 * Returns: TRUE if pairs should be rebuilt.
 */
gboolean visu_pair_extension_setDefault(VisuPairExtension *extension)
{
  DBG_fprintf(stderr, "Visu Pairs: set pairs drawing method to '%s'"
	      " (previous '%s').\n", extension->name, defaultPairMethod->name);

  if (extension == defaultPairMethod)
    return FALSE;
  
  if (defaultPairMethod &&
      defaultPairMethod->sensitiveToFacette)
    sensitiveToFacette -= 1;
  defaultPairMethod = extension;
  if (defaultPairMethod &&
      defaultPairMethod->sensitiveToFacette)
    sensitiveToFacette += 1;

  return TRUE;
}
/**
 * visu_pair_extension_getDefault:
 *
 * If some process need to know the current #VisuPairExtension. Such extension has
 * been set with setPairsMethod().
 *
 * Returns: the current #VisuPairExtension, NULL if none has been set.
 */
VisuPairExtension* visu_pair_extension_getDefault()
{
  return defaultPairMethod;
}
/**
 * visu_pair_extension_getAllMethods:
 * 
 * Useful to know all #VisuPairExtension.
 *
 * Returns: (element-type VisuPairExtension*) (transfer none): a list
 * of all the known #VisuPairExtension. This list should be considered
 * read-only.
 */
GList* visu_pair_extension_getAllMethods()
{
  return availableVisuPairExtensions;
}
/**
 * visu_pair_extension_getByName:
 * @name: a method name (untranslated).
 * 
 * Useful to know a #VisuPairExtension when knowing its name.
 *
 * Returns: a #VisuPairExtension, or NULL if not any matchs.
 *
 * Since: 3.6
 */
VisuPairExtension* visu_pair_extension_getByName(const gchar *name)
{
  GList *pairsMethods;
  gchar *name_;

  name_ = g_strstrip(g_strdup(name));
  pairsMethods = availableVisuPairExtensions;
  while (pairsMethods &&
         g_ascii_strcasecmp(((VisuPairExtension*)pairsMethods->data)->name, name_))
    pairsMethods = g_list_next(pairsMethods);
  g_free(name_);
  return (pairsMethods)?(VisuPairExtension*)pairsMethods->data:(VisuPairExtension*)0;
}
/**
 * visu_pair_extension_getName:
 * @extension: a #VisuPairExtension object ;
 * @UTF8: if name is provided for UI or not.
 *
 * A #VisuPairExtension has an internal name and a translated name for UI.
 *
 * Since: 3.7
 *
 * Returns: a private label identifying this @extension.
 **/
const gchar* visu_pair_extension_getName(VisuPairExtension *extension, gboolean UTF8)
{
  g_return_val_if_fail(extension, (const gchar*)0);

  if (UTF8)
    return extension->printName;
  else
    return extension->name;
}



/* OBSOLETE function, kept for backward compatibility. */
static gboolean readPairsData(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			      VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  VisuElement* ele[2];
  int res, token;
  float rgb[4];
  float minMax[2];
  VisuPairLink *data;
  gchar **tokens;
  ToolColor *color;

  /* Tokenize the line of values. */
  tokens = g_strsplit_set(g_strchug(lines[0]), " \n", TOOL_MAX_LINE_LENGTH);
  token = 0;

  /* Get the two elements. */
  if (!tool_config_file_readElementFromTokens(tokens, &token, ele, 2, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }

  /* Read 5 floats. */
  if (!tool_config_file_readFloatFromTokens(tokens, &token, minMax, 2, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  if (!tool_config_file_readFloatFromTokens(tokens, &token, rgb, 3, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  res = tool_config_file_clampFloat(&rgb[0], rgb[0], 0., 1.) ||
    tool_config_file_clampFloat(&rgb[1], rgb[1], 0., 1.) ||
    tool_config_file_clampFloat(&rgb[2], rgb[2], 0., 1.) ||
    tool_config_file_clampFloat(&minMax[0], minMax[0], 0., -1.) ||
    tool_config_file_clampFloat(&minMax[1], minMax[1], 0., -1.);
  if (res)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d, 5 floating points"
			     " must appear after the %s markup.\n"),
			   position, FLAG_RESOURCES_PAIRS_DATA);
      return FALSE;
    }
  data = visu_pair_link_new(ele[0], ele[1], minMax);
  g_return_val_if_fail(data, FALSE);

  rgb[3] = 1.f;
  color = tool_color_getByValues((int*)0, rgb[0], rgb[1], rgb[2], rgb[3]);
  if (!color)
    color = tool_color_addFloatRGBA(rgb, (int*)0);
  visu_pair_link_setColor(data, color);
  
  return TRUE;
}
