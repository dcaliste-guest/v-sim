/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef PAIRS_H
#define PAIRS_H

#include <visu_extension.h>
#include <visu_elements.h>
#include <visu_nodes.h>
#include <visu_data.h>
#include <visu_pairs.h>
#include <openGLFunctions/view.h>

G_BEGIN_DECLS

typedef struct _VisuPairExtension VisuPairExtension;

/**
 * VISU_TYPE_GL_EXT_PAIRS:
 *
 * return the type of #VisuGlExtPairs.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_PAIRS	     (visu_gl_ext_pairs_get_type ())
/**
 * VISU_GL_EXT_PAIRS:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtPairs type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairs))
/**
 * VISU_GL_EXT_PAIRS_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtPairsClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairsClass))
/**
 * VISU_IS_GL_EXT_PAIRS:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtPairs object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PAIRS(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_PAIRS))
/**
 * VISU_IS_GL_EXT_PAIRS_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtPairsClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PAIRS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_PAIRS))
/**
 * VISU_GL_EXT_PAIRS_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PAIRS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_PAIRS, VisuGlExtPairsClass))

typedef struct _VisuGlExtPairs        VisuGlExtPairs;
typedef struct _VisuGlExtPairsPrivate VisuGlExtPairsPrivate;
typedef struct _VisuGlExtPairsClass   VisuGlExtPairsClass;

struct _VisuGlExtPairs
{
  VisuGlExt parent;

  VisuGlExtPairsPrivate *priv;
};

struct _VisuGlExtPairsClass
{
  VisuGlExtClass parent;
};

/**
 * visu_gl_ext_pairs_get_type:
 *
 * This method returns the type of #VisuGlExtPairs, use
 * VISU_TYPE_GL_EXT_PAIRS instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtPairs.
 */
GType visu_gl_ext_pairs_get_type(void);

/**
 * VISU_GL_EXT_PAIRS_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_PAIRS_ID "Pairs"

VisuGlExtPairs* visu_gl_ext_pairs_new(const gchar *name);
VisuGlExtPairs* visu_gl_ext_pairs_getDefault();

gboolean visu_gl_ext_pairs_setData(VisuGlExtPairs *pairs,
                                   VisuGlView *view, VisuData *data);
gboolean visu_gl_ext_pairs_setDrawMethod(VisuGlExtPairs *pairs,
                                         VisuPairLink *data, VisuPairExtension *ext);

guint visu_gl_ext_pairs_getNDrawn(VisuGlExtPairs *pairs, VisuPairLink *data);
gboolean visu_gl_ext_pairs_getDrawnPair(VisuGlExtPairs *pairs, VisuPairLink *data,
                                        guint *node1, guint *node2, guint id);
VisuPairExtension* visu_gl_ext_pairs_getDrawMethod(VisuGlExtPairs *pairs,
                                                   VisuPairLink *data);

void visu_gl_ext_pairs_draw(VisuGlExtPairs *pairs);

/**
 * VisuPairDrawFuncs:
 * @start: a method called before drawing a family of #VisuPairLink.
 * @stop: a method called after drawing a family of #VisuPairLink.
 * @main: a method used to draw each link of a #VisuPairLink.
 *
 * Drawing method for a #VisuPairLink.
 */
typedef struct _VisuPairDrawFuncs VisuPairDrawFuncs;
struct _VisuPairDrawFuncs
{
  /**
   * start:
   * @ele1: a #VisuElement object ;
   * @ele2: a #VisuElement object ;
   * @data: a #VisuPairLink object.
   * @view: a #VisuGlView object, giving some constants describing
   *        the OpenGL scene ;
   *
   * Prototype of functions called at the beginning and
   * the end of drawing of each pairs types. @ele1 and @ele2
   * arguments are the two elements between the pair defined by @data is drawn.
   * This is useful to set some OpenGL definition specific to each pair, such
   * as the color for example.
   */
  void (*start)(VisuElement *ele1, VisuElement *ele2, VisuPairLink *data, VisuGlView *view);

  /**
   * stop:
   * @ele1: a #VisuElement object ;
   * @ele2: a #VisuElement object ;
   * @data: a #VisuPairLink object.
   * @view: a #VisuGlView object, giving some constants describing
   *        the OpenGL scene ;
   *
   * Prototype of functions called at the beginning and
   * the end of drawing of each pairs types. @ele1 and @ele2
   * arguments are the two elements between the pair defined by @data is drawn.
   * This is useful to set some OpenGL definition specific to each pair, such
   * as the color for example.
   */
  void (*stop)(VisuElement *ele1, VisuElement *ele2, VisuPairLink *data, VisuGlView *view);

  /**
   * main:
   * @ele1: a #VisuElement object ;
   * @ele2: a #VisuElement object ;
   * @data: a #VisuPairLink object ;
   * @view: a #VisuGlView object, giving some constants describing
   *        the OpenGL scene ;
   * @x1: a floating point value ;
   * @y1: a floating point value ;
   * @z1: a floating point value ;
   * @x2: a floating point value ;
   * @y2: a floating point value ;
   * @z2: a floating point value ;
   * @d2: a floating point value ;
   * @alpha: a floating point value.
   *
   * Prototype of function to draw a pair. Such function are called each time a pair
   * is drawn between the two points (@x1, @y1, @z1) and (@x2, @y2, @z2). The @d2 argument
   * is the square distance between the two points. The @alpha argument
   * is a proposed alpha colour from the main program, its value is in [0;1].
   */
  void (*main)(VisuElement *ele1, VisuElement *ele2,
               VisuPairLink *data, VisuGlView *view,
               double x1, double y1, double z1,
               double x2, double y2, double z2,
               float d2, float alpha);
};

#define VISU_TYPE_PAIR_EXTENSION (visu_pair_extension_get_type())
GType visu_pair_extension_get_type(void);
VisuPairExtension* visu_pair_extension_new(const char* name, const char* printName,
                                           const char* description, gboolean sensitive,
                                           const VisuPairDrawFuncs *meth);
VisuPairExtension* visu_pair_extension_ref(VisuPairExtension *ext);
VisuPairExtension* visu_pair_extension_getByName(const gchar *name);
void visu_pair_extension_unref(VisuPairExtension *ext);
void visu_pair_extension_free(VisuPairExtension* extension);
gboolean visu_pair_extension_setDefault(VisuPairExtension *extension);
GList* visu_pair_extension_getAllMethods();
const gchar* visu_pair_extension_getName(VisuPairExtension *extension, gboolean UTF8);
VisuPairExtension* visu_pair_extension_getDefault();

G_END_DECLS

#endif
