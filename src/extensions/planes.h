/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef PLANES_H
#define PLANES_H

#include "frame.h"

#include <visu_extension.h>
#include <visu_box.h>
#include <extraFunctions/plane.h>

/**
 * VISU_TYPE_GL_EXT_PLANES:
 *
 * return the type of #VisuGlExtPlanes.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_PLANES	     (visu_gl_ext_planes_get_type ())
/**
 * VISU_GL_EXT_PLANES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtPlanes type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PLANES(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_PLANES, VisuGlExtPlanes))
/**
 * VISU_GL_EXT_PLANES_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtPlanesClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PLANES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_PLANES, VisuGlExtPlanesClass))
/**
 * VISU_IS_GL_EXT_PLANES:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtPlanes object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PLANES(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_PLANES))
/**
 * VISU_IS_GL_EXT_PLANES_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtPlanesClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PLANES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_PLANES))
/**
 * VISU_GL_EXT_PLANES_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PLANES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_PLANES, VisuGlExtPlanesClass))

typedef struct _VisuGlExtPlanes        VisuGlExtPlanes;
typedef struct _VisuGlExtPlanesPrivate VisuGlExtPlanesPrivate;
typedef struct _VisuGlExtPlanesClass   VisuGlExtPlanesClass;

struct _VisuGlExtPlanes
{
  VisuGlExtFrame parent;

  VisuGlExtPlanesPrivate *priv;
};

struct _VisuGlExtPlanesClass
{
  VisuGlExtFrameClass parent;
};

/**
 * visu_gl_ext_planes_get_type:
 *
 * This method returns the type of #VisuGlExtPlanes, use
 * VISU_TYPE_GL_EXT_PLANES instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtPlanes.
 */
GType visu_gl_ext_planes_get_type(void);

/**
 * VISU_GL_EXT_PLANES_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_PLANES_ID "Planes"

VisuGlExtPlanes* visu_gl_ext_planes_new(const gchar *name);

gboolean visu_gl_ext_planes_add(VisuGlExtPlanes *planes, VisuPlane *plane);
gboolean visu_gl_ext_planes_remove(VisuGlExtPlanes *planes, VisuPlane *plane);

void visu_gl_ext_planes_draw(VisuGlExtPlanes *planes);

VisuGlExtPlanes* visu_gl_ext_planes_getDefault();

#endif
