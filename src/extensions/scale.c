/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "scale.h"

#include <string.h>

#include <GL/gl.h>
#include <GL/glu.h> 

#include <opengl.h>
#include <openGLFunctions/text.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolMatrix.h>
#include <coreTools/toolConfigFile.h>
#include <math.h>

/**
 * SECTION:scale
 * @short_description: Draw an arrow with a label.
 *
 * This little extension is used to draw an arrow at a given position
 * displaying a given length.
 *
 * Since: 3.3
 */

/**
 * VisuGlExtScaleClass:
 * @parent: parent structure.
 *
 * An opaque structure.
 *
 * Since: 3.3
 */
/**
 * VisuGlExtScale:
 *
 * All fields are private, use the access routines.
 *
 * Since: 3.3
 */
/**
 * VisuGlExtScalePrivate:
 *
 * All fields are private, use the access routines.
 *
 * Since: 3.3
 */

/* Parameters & resources*/
/* This is a boolean to control is the axes is render or not. */
#define FLAG_RESOURCE_SCALE_USED   "scales_are_on"
#define DESC_RESOURCE_SCALE_USED   "Control if scales are drawn ; boolean (0 or 1)"
#define SCALE_USED_DEFAULT 0
static gboolean readScaleIsOn(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                              VisuData *dataObj, VisuGlView *view, GError **error);

/* A resource to control the color used to render the lines of the Scale. */
#define FLAG_RESOURCE_SCALE_COLOR   "scales_color"
#define DESC_RESOURCE_SCALE_COLOR   "Define the color RGBA of all scales ; four floating point values (0. <= v <= 1.)"
static gboolean readScaleColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                               VisuData *dataObj, VisuGlView *view, GError **error);

/* A resource to control the width to render the lines of the Scale. */
#define FLAG_RESOURCE_SCALE_LINE   "scales_line_width"
#define DESC_RESOURCE_SCALE_LINE   "Define the width of the lines of all scales ; one floating point value (1. <= v <= 10.)"
static gboolean readScaleLineWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                   VisuData *dataObj, VisuGlView *view, GError **error);

/* A resource to control the width to render the lines of the Scale. */
#define FLAG_RESOURCE_SCALE_STIPPLE   "scales_line_stipple"
#define DESC_RESOURCE_SCALE_STIPPLE   "Define the stipple pattern of the lines of all scales ; one integer value (0 <= v <= 65535)"
static gboolean readScaleLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                     VisuData *dataObj, VisuGlView *view, GError **error);

/* A resource to control the elements of a scale (origin, direction, length... */
#define FLAG_RESOURCE_SCALE_DEFINITION "scale_definition"
#define DESC_RESOURCE_SCALE_DEFINITION "Define the position, the direction, the length and the legend of a scale ; position[3] direction[3] length legend"
#define SCALE_LEGEND_DEFAULT _("Length: %6.2f")
static gboolean readScaleDefinition(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                    VisuData *dataObj, VisuGlView *view, GError **error);

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesScale(GString *data, VisuData *dataObj, VisuGlView *view);

#define SCALE_AUTO_LEGEND "[auto]"

typedef struct _Arrow
{
  /* Definition of the scale. */
  float origin[3];
  float direction[3];
  float length;

  /* Characteristics. */
  gboolean drawn;
  gchar *legendPattern;
  GString *legend;
} Arrow;

struct _VisuGlExtScalePrivate
{
  /* Internal object gestion. */
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* A list of arrows. */
  GList *arrows;

  /* Related objects. */
  VisuGlView *view;
  gulong widthHeight_signal, nearFar_signal, angles_signal;
};

static VisuGlExtScale * defaultScale = NULL;

/* Some global modifiers. */
static float _width = 1.f;
static float _rgba[4] = {0.f, 0.f, 0.f, 1.f};
static guint16 _stipple = 65535;

/* Object gestion methods. */
static void scale_dispose (GObject* obj);
static void scale_finalize(GObject* obj);
static void scale_rebuild (VisuGlExt *ext);

/* Local methods. */
static void _freeArrow(Arrow *arr);
static void _drawArrow(Arrow *arr, guint nlat);

/* Local callbacks */
static void onScaleParametersChange(VisuGlView *view, gpointer data);

G_DEFINE_TYPE(VisuGlExtScale, visu_gl_ext_scale, VISU_TYPE_GL_EXT)

static void visu_gl_ext_scale_class_init(VisuGlExtScaleClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Scale: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = scale_dispose;
  G_OBJECT_CLASS(klass)->finalize = scale_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = scale_rebuild;

  /* Create the entries in config files. */
  DBG_fprintf(stderr," - create entries for config file.\n");
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_SCALE_USED,
					  DESC_RESOURCE_SCALE_USED,
					  1, readScaleIsOn);
  visu_config_file_entry_setVersion(resourceEntry, 3.3f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_SCALE_COLOR,
					  DESC_RESOURCE_SCALE_COLOR,
					  1, readScaleColor);
  visu_config_file_entry_setVersion(resourceEntry, 3.3f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_SCALE_LINE,
					  DESC_RESOURCE_SCALE_LINE,
					  1, readScaleLineWidth);
  visu_config_file_entry_setVersion(resourceEntry, 3.3f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_SCALE_DEFINITION,
					  DESC_RESOURCE_SCALE_DEFINITION,
					  1, readScaleDefinition);
  visu_config_file_entry_setVersion(resourceEntry, 3.3f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_SCALE_STIPPLE,
					  DESC_RESOURCE_SCALE_STIPPLE,
					  1, readScaleLineStipple);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResourcesScale);

  g_type_class_add_private(klass, sizeof(VisuGlExtScalePrivate));
}

static void visu_gl_ext_scale_init(VisuGlExtScale *obj)
{
  DBG_fprintf(stderr, "Scale: creating a new scale (%p).\n", (gpointer)obj);
  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_GL_EXT_SCALE,
                                          VisuGlExtScalePrivate);
  obj->priv->dispose_has_run = FALSE;
  obj->priv->isBuilt         = FALSE;
  obj->priv->arrows          = (GList*)0;
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->nearFar_signal     = 0;
  obj->priv->angles_signal      = 0;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void scale_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Scale: dispose object %p.\n", (gpointer)obj);

  if (VISU_GL_EXT_SCALE(obj)->priv->dispose_has_run)
    return;

  VISU_GL_EXT_SCALE(obj)->priv->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_scale_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void scale_finalize(GObject* obj)
{
  VisuGlExtScale *scale = VISU_GL_EXT_SCALE(obj);
  GList *lst;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Scale: finalize object %p.\n", (gpointer)obj);

  /* Free my memory. */
  for (lst = scale->priv->arrows; lst; lst = g_list_next(lst))
    _freeArrow((Arrow*)lst->data);
  g_list_free(scale->priv->arrows);
  visu_gl_ext_scale_setGlView(scale, (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_scale_parent_class)->finalize(obj);
}

/**
 * visu_gl_ext_scale_new:
 * @name: (allow-none): a name for the #VisuGlExt.
 *
 * Create a new arrow set without any elements. Add arrows with visu_gl_ext_scale_add().
 *
 * Returns: a newly created #VisuGlExtScale object.
 *
 * Since: 3.3
 */
VisuGlExtScale* visu_gl_ext_scale_new(const gchar *name)
{
  char *name_ = "Scale";
  char *description = _("Draw scales in the rendering area.");
  VisuGlExt *scale;

  scale = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_SCALE,
                                      "name", (name)?name:name_, "label", _(name),
                                      "description", description,
                                      "nGlObj", 1, "saveState", TRUE, NULL));

  return VISU_GL_EXT_SCALE(scale);
}
/**
 * visu_gl_ext_scale_setGlView:
 * @scale: a #VisuGlExtScale object.
 * @view: (transfer full): a #VisuGlView object.
 *
 * Attach @view to @scale, so rendering is updated when @view is
 * modified.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @view is changed.
 **/
gboolean visu_gl_ext_scale_setGlView(VisuGlExtScale *scale, VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), FALSE);

  if (view == scale->priv->view)
    return FALSE;

  if (scale->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(scale->priv->view), scale->priv->widthHeight_signal);
      g_signal_handler_disconnect(G_OBJECT(scale->priv->view), scale->priv->nearFar_signal);
      g_signal_handler_disconnect(G_OBJECT(scale->priv->view), scale->priv->angles_signal);
      g_object_unref(scale->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      scale->priv->nearFar_signal =
        g_signal_connect(G_OBJECT(view), "NearFarChanged",
                         G_CALLBACK(onScaleParametersChange), (gpointer)scale);
      scale->priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onScaleParametersChange), (gpointer)scale);
      scale->priv->angles_signal =
        g_signal_connect(G_OBJECT(view), "ThetaPhiOmegaChanged",
                         G_CALLBACK(onScaleParametersChange), (gpointer)scale);
    }
  else
    {
      scale->priv->nearFar_signal = 0;
      scale->priv->widthHeight_signal = 0;
      scale->priv->angles_signal = 0;
    }
  scale->priv->view = view;

  scale->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(scale));
}
/**
 * visu_gl_ext_scale_add:
 * @scale: the #VisuGlExtScale object to add to.
 * @origin: (array fixed-size=3): the origin ;
 * @orientation: (array fixed-size=3): the orientation in cartesian coordinates ;
 * @length: the length of the arrow ;
 * @legend: (allow-none): the text going with the arrow (can be NULL).
 *
 * Create a new arrow pointing somewhere in the box with a label.
 * If @legend is NULL, then the label will be the value of the length.
 *
 * Since: 3.7
 *
 * Returns: the id of the newly added arrow.
 */
guint visu_gl_ext_scale_add(VisuGlExtScale *scale, float origin[3], float orientation[3],
                            float length, const gchar *legend)
{
  Arrow *arr;

  g_return_val_if_fail(length > 0.f && VISU_IS_GL_EXT_SCALE(scale), 0);

  arr = g_malloc(sizeof(Arrow));
  arr->drawn     = TRUE;
  arr->origin[0] = origin[0];
  arr->origin[1] = origin[1];
  arr->origin[2] = origin[2];
  arr->direction[0] = orientation[0];
  arr->direction[1] = orientation[1];
  arr->direction[2] = orientation[2];
  arr->length = length;
  if (legend)
    arr->legendPattern = g_strdup(legend);
  else
    arr->legendPattern = (gchar*)0;
  arr->legend = g_string_new("");
  if (legend)
    g_string_assign(arr->legend, legend);
  else
    g_string_printf(arr->legend, SCALE_LEGEND_DEFAULT, arr->length);
  scale->priv->arrows = g_list_append(scale->priv->arrows, arr);

  return g_list_length(scale->priv->arrows) - 1;
}

static void _freeArrow(Arrow *arr)
{
  if (arr->legendPattern)
    g_free(arr->legendPattern);
  g_string_free(arr->legend, TRUE);
  g_free(arr);
}

/**
 * visu_gl_ext_scale_setDefaultRGB:
 * @rgba: a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue and the alpha color. Only values specified by the mask
 * are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G, #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_RGBA or a
 * combinaison to indicate what values in the rgb array must be taken
 * into account.
 *
 * Method used to change the value of the private parameter
 * scales_color. This affects all the drawn scales.
 *
 * Since: 3.3
 *
 * Returns: TRUE if scaleDraw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_scale_setDefaultRGB(float rgba[4], int mask)
{
  gboolean diff;
  
  diff = FALSE;
  if (mask & TOOL_COLOR_MASK_R && _rgba[0] != rgba[0])
    {
      _rgba[0] = rgba[0];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_G && _rgba[1] != rgba[1])
    {
      _rgba[1] = rgba[1];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_B && _rgba[2] != rgba[2])
    {
      _rgba[2] = rgba[2];
      diff = TRUE;
    }
  if (mask & TOOL_COLOR_MASK_A && _rgba[3] != rgba[3])
    {
      _rgba[3] = rgba[3];
      diff = TRUE;
    }
  if (defaultScale)
    defaultScale->priv->isBuilt = FALSE;
  return diff;
}
/**
 * visu_gl_ext_scale_setDefaultLineWidth:
 * @width: value of the desired width.
 *
 * Method used to change the value of the parameter
 * scale_line_width. This affects all the drawn scales.
 *
 * Since: 3.3
 *
 * Returns: TRUE if scaleDraw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 */
gboolean visu_gl_ext_scale_setDefaultLineWidth(float width)
{
  if (width == _width)
    return FALSE;

  _width = CLAMP(width, 0.f, 10.f);
  if (defaultScale)
    defaultScale->priv->isBuilt = FALSE;
  return TRUE;
}
/**
 * visu_gl_ext_scale_setDefaultStipple:
 * @stipple: a pattern for line.
 *
 * The scales share a line pattern for the stick of the arrow.
 *
 * Since: 3.3
 *
 * Returns: TRUE if scale are drawn, FALSE otherwise.
 */
gboolean visu_gl_ext_scale_setDefaultStipple(guint16 stipple)
{
  if (stipple == _stipple)
    return FALSE;

  _stipple = stipple;
  if (defaultScale)
    defaultScale->priv->isBuilt = FALSE;
  return TRUE;
}

/* Get methods. */
/**
 * visu_gl_ext_scale_getDefaultRGB:
 *
 * All the scales shared a common colour.
 *
 * Since: 3.3
 *
 * Returns: (array fixed-size=4) (transfer none): a four component array.
 */
float* visu_gl_ext_scale_getDefaultRGB()
{
  return _rgba;
}
/**
 * visu_gl_ext_scale_getDefaultLineWidth:
 *
 * The scales share a line width for the stick of the arrow.
 *
 * Since: 3.3
 *
 * Returns: the value of current width.
 */
float visu_gl_ext_scale_getDefaultLineWidth()
{
  return _width;
}
/**
 * visu_gl_ext_scale_getDefaultStipple:
 *
 * The scales share a line pattern for the stick of the arrow.
 *
 * Since: 3.3
 *
 * Returns: the value of current stipple pattern.
 */
guint16 visu_gl_ext_scale_getDefaultStipple()
{
  return _stipple;
}


/**
 * visu_gl_ext_scale_getNArrows:
 * @scale: the #VisuGlExtScale to poll.
 *
 * A #VisuGlExtScale is characterised by a set of arrows.
 *
 * Since: 3.7
 *
 * Returns: the number of stored arrows.
 */
guint visu_gl_ext_scale_getNArrows(VisuGlExtScale *scale)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), 0);

  return g_list_length(scale->priv->arrows);
}
/**
 * visu_gl_ext_scale_getLength:
 * @scale: the #VisuGlExtScale to poll.
 * @i: the ith arrow.
 *
 * A #VisuGlExtScale is characterised by its length.
 *
 * Since: 3.3
 *
 * Returns: a positive floating point value or a negative value if @i
 * is not in the arrow list.
 */
float visu_gl_ext_scale_getLength(VisuGlExtScale *scale, guint i)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), -1.f);

  lst = g_list_nth(scale->priv->arrows, i);
  if (lst)
    return ((Arrow*)lst->data)->length;
  else
    return -1.f;
}
/**
 * visu_gl_ext_scale_getOrigin:
 * @scale: the #VisuGlExtScale to poll.
 * @i: the ith arrow.
 *
 * A #VisuGlExtScale is characterised by its origin in cartesian coordinates.
 *
 * Since: 3.3
 *
 * Returns: (array fixed-size=3) (transfer none) (allow-none): three
 * floating point values.
 */
float* visu_gl_ext_scale_getOrigin(VisuGlExtScale *scale, guint i)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), (float*)0);

  lst = g_list_nth(scale->priv->arrows, i);
  if (lst)
    return ((Arrow*)lst->data)->origin;
  else
    return (float*)0;
}
/**
 * visu_gl_ext_scale_getOrientation:
 * @scale: the #VisuGlExtScale to poll.
 * @i: the ith arrow.
 *
 * A #VisuGlExtScale is characterised by its orientation in cartesian coordinates.
 *
 * Since: 3.3
 *
 * Returns: (array fixed-size=3) (transfer none) (allow-none): three
 * floating point values.
 */
float* visu_gl_ext_scale_getOrientation(VisuGlExtScale *scale, guint i)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), (float*)0);

  lst = g_list_nth(scale->priv->arrows, i);
  if (lst)
    return ((Arrow*)lst->data)->direction;
  else
    return (float*)0;
}
/**
 * visu_gl_ext_scale_getLegend:
 * @scale: the #VisuGlExtScale to poll.
 * @i: the ith arrow.
 *
 * A #VisuGlExtScale can have a legend. This is not actualy the string printed
 * on screen but the one used to generate it.
 *
 * Since: 3.3
 *
 * Returns: (allow-none): a string (private, do not free it).
 */
const gchar* visu_gl_ext_scale_getLegend(VisuGlExtScale *scale, guint i)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), (const gchar*)0);

  lst = g_list_nth(scale->priv->arrows, i);
  if (lst)
    return ((Arrow*)lst->data)->legendPattern;
  else
    return (const gchar*)0;
}

/**
 * visu_gl_ext_scale_setOrigin:
 * @scale: the #VisuGlExtScale to modify ;
 * @i: the ith arrow ;
 * @xyz: (array fixed-size=3): a vector in cartesian coordinates ;
 * @mask: relevant values in @xyz, see #TOOL_XYZ_MASK_X...
 *
 * Routine that changes the origin of the scale.
 *
 * Since: 3.3
 *
 * Returns: TRUE if visu_gl_ext_scale_draw() should be called.
 */
gboolean visu_gl_ext_scale_setOrigin(VisuGlExtScale *scale, guint i, float xyz[3], int mask)
{
  GList *lst;
  Arrow *arr;
  gboolean difference;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), FALSE);

  lst = g_list_nth(scale->priv->arrows, i);
  if (!lst)
    return FALSE;

  arr = (Arrow*)lst->data;
  difference = FALSE;
  if (mask & TOOL_XYZ_MASK_X && arr->origin[0] != xyz[0])
    {
      arr->origin[0] = xyz[0];
      difference = TRUE;
    }
  if (mask & TOOL_XYZ_MASK_Y && arr->origin[1] != xyz[1])
    {
      arr->origin[1] = xyz[1];
      difference = TRUE;
    }
  if (mask & TOOL_XYZ_MASK_Z && arr->origin[2] != xyz[2])
    {
      arr->origin[2] = xyz[2];
      difference = TRUE;
    }
  if (!difference)
    return FALSE;

  scale->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(scale));
}

/**
 * visu_gl_ext_scale_setOrientation:
 * @scale: the #VisuGlExtScale to modify ;
 * @i: the ith arrow ;
 * @xyz: (array fixed-size=3): a vector in cartesian coordinates ;
 * @mask: relevant values in @xyz, see #TOOL_XYZ_MASK_X...
 *
 * Routine that changes the direction of the scale.
 *
 * Since: 3.3
 *
 * Returns: TRUE if visu_gl_ext_scale_draw() should be called.
 */
gboolean visu_gl_ext_scale_setOrientation(VisuGlExtScale *scale, guint i, float xyz[3], int mask)
{
  GList *lst;
  Arrow *arr;
  gboolean difference;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), FALSE);

  lst = g_list_nth(scale->priv->arrows, i);
  if (!lst)
    return FALSE;

  arr = (Arrow*)lst->data;
  difference = FALSE;
  if (mask & TOOL_XYZ_MASK_X && arr->direction[0] != xyz[0])
    {
      arr->direction[0] = xyz[0];
      difference = TRUE;
    }
  if (mask & TOOL_XYZ_MASK_Y && arr->direction[1] != xyz[1])
    {
      arr->direction[1] = xyz[1];
      difference = TRUE;
    }
  if (mask & TOOL_XYZ_MASK_Z && arr->direction[2] != xyz[2])
    {
      arr->direction[2] = xyz[2];
      difference = TRUE;
    }
  if (!difference)
    return FALSE;

  scale->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(scale));
}
/**
 * visu_gl_ext_scale_setLength:
 * @scale: the #VisuGlExtScale to modify ;
 * @i: the ith arrow ;
 * @lg: a positive length.
 *
 * Routine that changes the length of the scale.
 *
 * Since: 3.3
 *
 * Returns: TRUE if visu_gl_ext_scale_draw() should be called.
 */
gboolean visu_gl_ext_scale_setLength(VisuGlExtScale *scale, guint i, float lg)
{
  GList *lst;
  Arrow *arr;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), FALSE);

  lst = g_list_nth(scale->priv->arrows, i);
  if (!lst)
    return FALSE;

  arr = (Arrow*)lst->data;
  if (lg == arr->length)
    return FALSE;

  arr->length = lg;
  scale->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(scale));
}
/**
 * visu_gl_ext_scale_setLegend:
 * @scale: the #VisuGlExtScale to modify ;
 * @i: the ith arrow ;
 * @value: (allow-none): a string (can be NULL).
 *
 * Routine that changes the legend of the scale. If @value is NULL
 * then the length of the scale is printed.
 *
 * Since: 3.3
 *
 * Returns: TRUE if visu_gl_ext_scale_draw() should be called.
 */
gboolean visu_gl_ext_scale_setLegend(VisuGlExtScale *scale, guint i, const gchar *value)
{
  GList *lst;
  Arrow *arr;

  g_return_val_if_fail(VISU_IS_GL_EXT_SCALE(scale), FALSE);

  lst = g_list_nth(scale->priv->arrows, i);
  if (!lst)
    return FALSE;

  arr = (Arrow*)lst->data;
  
  g_free(arr->legendPattern);

  if (value && *g_strstrip((gchar*)value))
    arr->legendPattern = g_strdup(value);
  else
    arr->legendPattern = (gchar*)0;
  if (arr->legendPattern)
    g_string_assign(arr->legend, arr->legendPattern);
  else
    g_string_printf(arr->legend, SCALE_LEGEND_DEFAULT, arr->length);

  scale->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(scale));
}

/**
 * visu_gl_ext_scale_getDefault:
 *
 * The default #VisuGlExtScale object used by V_Sim.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtScale object.
 */
VisuGlExtScale* visu_gl_ext_scale_getDefault()
{
  if (!defaultScale)
    {
      defaultScale = visu_gl_ext_scale_new(NULL);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultScale), SCALE_USED_DEFAULT);
    }
  return defaultScale;
}

/****************/
/* Private part */
/****************/

static void scale_rebuild(VisuGlExt *ext)
{
  visu_gl_text_rebuildFontList();
  VISU_GL_EXT_SCALE(ext)->priv->isBuilt = FALSE;
  visu_gl_ext_scale_draw(VISU_GL_EXT_SCALE(ext));
}
static void onScaleParametersChange(VisuGlView *view _U_, gpointer data)
{
  VISU_GL_EXT_SCALE(data)->priv->isBuilt = FALSE;
  visu_gl_ext_scale_draw(VISU_GL_EXT_SCALE(data));
}

/**
 * visu_gl_ext_scale_draw:
 * @scale: the #VisuGlExtScale object to draw.
 *
 * This method creates a compile list that draw all arrow of a scale.
 *
 * Since: 3.3
 */
void visu_gl_ext_scale_draw(VisuGlExtScale *scale)
{
  GList *tmpLst;
  int nlat;
  float radius = 0.3;

  g_return_if_fail(VISU_IS_GL_EXT_SCALE(scale));

  /* Nothing to draw; */
  if(!scale->priv->view ||
     !visu_gl_ext_getActive(VISU_GL_EXT(scale)) ||
     scale->priv->isBuilt) return;

  DBG_fprintf(stderr, "Extension Scale: creating scales.\n");

  visu_gl_text_initFontList();
  nlat = visu_gl_view_getDetailLevel(scale->priv->view, radius);
  
  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(scale)), 1);
  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(scale)), GL_COMPILE);

  /* Deactivate light and fog. */
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);

  glLineWidth(_width);
  glColor4fv(_rgba);
  if (_stipple != 65535)
    {
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, _stipple);
    }

  for (tmpLst = scale->priv->arrows; tmpLst; tmpLst = g_list_next(tmpLst))
    _drawArrow((Arrow*)tmpLst->data, nlat);

  glEndList();

  scale->priv->isBuilt = TRUE;
}
/* fonction qui dessine la legende*/
static void _drawArrow(Arrow *arr, guint nlat)
{
  float x2,y2,z2, norm;
  float angles[3]; /*declaration des tableaux angles et coordonnées a 3 cellules*/
  float coord[3], u, v, s;
  float radius = 0.3 * (0.25f * log(_width) + 1.f);
  float tl = 1.f;
  GLUquadricObj *obj;
  float modelView[16];

  obj = gluNewQuadric();

  norm = arr->length / sqrt(arr->direction[0] * arr->direction[0] +
                            arr->direction[1] * arr->direction[1] +
                            arr->direction[2] * arr->direction[2]) - tl;
  x2 = arr->origin[0] + arr->direction[0] * norm;
  y2 = arr->origin[1] + arr->direction[1] * norm;
  z2 = arr->origin[2] + arr->direction[2] * norm;

  coord[0] = x2 - arr->origin[0];
  coord[1] = y2 - arr->origin[1];
  coord[2] = z2 - arr->origin[2];

  tool_matrix_cartesianToSpherical(angles, coord);

  glPushMatrix();
  glTranslated(arr->origin[0], arr->origin[1], arr->origin[2]);
  glRotated(angles[2], 0., 0., 1.);
  glRotated(angles[1], 0., 1., 0.);
  glTranslated(0., 0., angles[0]);
  gluCylinder(obj, radius, 0., tl, nlat, 1);
  glRotated(180., 1., 0., 0.);
  gluDisk(obj, 0, radius, nlat, 1);
  glPopMatrix();

  glBegin (GL_LINES);
  glVertex3fv(arr->origin);
  glVertex3f(x2, y2, z2);
  glEnd();

  glGetFloatv(GL_MODELVIEW_MATRIX, modelView);
  v = -(coord[0] * modelView[0] + coord[1] * modelView[4] + coord[2] * modelView[8]);
  u = +(coord[0] * modelView[1] + coord[1] * modelView[5] + coord[2] * modelView[9]);
  s = (v < 0.f)?0.25f:0.75f;
  if (v > 0.f)
    {
      u *= -1.f;
      v *= -1.f;
    }
  coord[0] = arr->origin[0] + s * arr->direction[0] * norm;
  coord[1] = arr->origin[1] + s * arr->direction[1] * norm;
  coord[2] = arr->origin[2] + s * arr->direction[2] * norm;
  norm = 1.f / sqrt(u * u + v * v) * radius * 2.f;
  coord[0] += (u * modelView[0] + v * modelView[1]) * norm;
  coord[1] += (u * modelView[4] + v * modelView[5]) * norm;
  coord[2] += (u * modelView[8] + v * modelView[9]) * norm;
  glRasterPos3fv(coord);
  visu_gl_text_drawChars(arr->legend->str, VISU_GL_TEXT_NORMAL);

  gluDeleteQuadric(obj);
}

/* Parameters & resources*/

/* This is a boolean to control is the Scale is render or not. */
static gboolean readScaleIsOn(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
			      int position, VisuData *dataObj _U_,
                              VisuGlView *view _U_, GError **error)
{
  gboolean val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readBoolean(lines[0], position, &val, 1, error))
    return FALSE;
  visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_scale_getDefault()), val);
  return TRUE;
}
/* A resource to control the color used to render the lines of the Scale. */
static gboolean readScaleColor(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
			       int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float rgb[4];
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, rgb, 4, error))
    {
      if (*error)
	g_error_free(*error);
      *error = (GError*)0;
      if (!tool_config_file_readFloat(lines[0], position, rgb, 3, error))
	return FALSE;
      rgb[3] = 1.f;
    }

  if (tool_config_file_clampFloat(&rgb[0], rgb[0], 0., 1.) ||
      tool_config_file_clampFloat(&rgb[1], rgb[1], 0., 1.) ||
      tool_config_file_clampFloat(&rgb[2], rgb[2], 0., 1.) ||
      tool_config_file_clampFloat(&rgb[3], rgb[3], 0., 1.))
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: 4 floating points"
			     "(0 <= v <= 1) must appear after the %s markup.\n"),
			   position, FLAG_RESOURCE_SCALE_COLOR);
      return FALSE;
    }
  visu_gl_ext_scale_setDefaultRGB(rgb, TOOL_COLOR_MASK_RGBA);

  return TRUE;
}
/* A resource to control the width to render the lines of the Scale. */
static gboolean readScaleLineWidth(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				   int position, VisuData *dataObj _U_,
                                   VisuGlView *view _U_, GError **error)
{
  float width;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &width, 1, error))
    return FALSE;
  if (tool_config_file_clampFloat(&width, width, 1., 10.))
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: 1 floating point"
			     "(1 <= v <= 10) must appear after the %s markup.\n"),
			   position, FLAG_RESOURCE_SCALE_LINE);
      return FALSE;
    }
  visu_gl_ext_scale_setDefaultLineWidth(width);

  return TRUE;
}
static gboolean readScaleLineStipple(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				     int position, VisuData *dataObj _U_,
                                     VisuGlView *view _U_, GError **error)
{
  int stipple;

  g_return_val_if_fail(nbLines == 1, FALSE);
  
  if (!tool_config_file_readInteger(lines[0], position, &stipple, 1, error))
    return FALSE;
  visu_gl_ext_scale_setDefaultStipple((guint16)stipple);

  return TRUE;
}
static gboolean readScaleDefinition(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				    int position, VisuData *dataObj _U_,
                                    VisuGlView *view _U_, GError **error)
{
  float xyz[3], orientation[3], len;
  gchar **tokens, *remains, *legend;
  int pos;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit(g_strchug(lines[0]), " ", 0);
  pos = 0;
  if (!tool_config_file_readFloatFromTokens(tokens, &pos, xyz, 3, position, error))
    {
      g_strfreev(tokens);
      return FALSE;      
    }
  if (!tool_config_file_readFloatFromTokens(tokens, &pos, orientation, 3, position, error))
    {
      g_strfreev(tokens);
      return FALSE;      
    }
  if (!tool_config_file_readFloatFromTokens(tokens, &pos, &len, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;      
    }
  if (tokens[pos])
    remains = g_strjoinv(" ", &tokens[pos]);
  else
    remains = g_strdup(SCALE_LEGEND_DEFAULT);
  g_strfreev(tokens);

  legend = g_strstrip(remains);
  if (legend && !strcmp(legend, SCALE_AUTO_LEGEND))
    legend = (gchar*)0;
  visu_gl_ext_scale_add(visu_gl_ext_scale_getDefault(), xyz, orientation, len, legend);
  g_free(remains);

  return TRUE;
}

/* Export function that is called by visu_module to write the
   values of resources to a file. */
static void exportResourcesScale(GString *data,
                                 VisuData *dataObj _U_, VisuGlView *view _U_)
{
  GList *tmpLst;
  Arrow *arr;
  gchar *legend;

  if (!defaultScale)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_SCALE_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_SCALE_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultScale)));

  visu_config_file_exportComment(data, DESC_RESOURCE_SCALE_COLOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_SCALE_COLOR, NULL,
                               "%4.3f %4.3f %4.3f %4.3f",
                               _rgba[0], _rgba[1], _rgba[2], _rgba[3]);

  visu_config_file_exportComment(data, DESC_RESOURCE_SCALE_LINE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_SCALE_LINE, NULL,
                               "%4.0f", _width);

  visu_config_file_exportComment(data, DESC_RESOURCE_SCALE_STIPPLE);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_SCALE_STIPPLE, NULL,
                               "%d", _stipple);

  visu_config_file_exportComment(data, DESC_RESOURCE_SCALE_DEFINITION);
  for (tmpLst = defaultScale->priv->arrows; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      arr = (Arrow*)tmpLst->data;
      if (arr->legendPattern)
	legend = arr->legendPattern;
      else
	legend = SCALE_AUTO_LEGEND;
      visu_config_file_exportEntry(data, FLAG_RESOURCE_SCALE_DEFINITION, NULL,
                                   "%g %g %g  %g %g %g  %g  %s",
                                   arr->origin[0], arr->origin[1], arr->origin[2],
                                   arr->direction[0], arr->direction[1],
                                   arr->direction[2], arr->length, legend);
    }
  visu_config_file_exportComment(data, "");
}
