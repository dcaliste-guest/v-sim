/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef SHADE_H
#define SHADE_H

#include "frame.h"

#include <visu_extension.h>
#include <coreTools/toolShade.h>
#include <openGLFunctions/view.h>

/**
 * VISU_TYPE_GL_EXT_SHADE:
 *
 * return the type of #VisuGlExtShade.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_SHADE	     (visu_gl_ext_shade_get_type ())
/**
 * VISU_GL_EXT_SHADE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtShade type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_SHADE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_SHADE, VisuGlExtShade))
/**
 * VISU_GL_EXT_SHADE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtShadeClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_SHADE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_SHADE, VisuGlExtShadeClass))
/**
 * VISU_IS_GL_EXT_SHADE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtShade object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_SHADE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_SHADE))
/**
 * VISU_IS_GL_EXT_SHADE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtShadeClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_SHADE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_SHADE))
/**
 * VISU_GL_EXT_SHADE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_SHADE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_SHADE, VisuGlExtShadeClass))

typedef struct _VisuGlExtShade        VisuGlExtShade;
typedef struct _VisuGlExtShadePrivate VisuGlExtShadePrivate;
typedef struct _VisuGlExtShadeClass   VisuGlExtShadeClass;

struct _VisuGlExtShade
{
  VisuGlExtFrame parent;

  VisuGlExtShadePrivate *priv;
};

struct _VisuGlExtShadeClass
{
  VisuGlExtFrameClass parent;
};

/**
 * visu_gl_ext_shade_get_type:
 *
 * This method returns the type of #VisuGlExtShade, use
 * VISU_TYPE_GL_EXT_SHADE instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtShade.
 */
GType visu_gl_ext_shade_get_type(void);

/**
 * VISU_GL_EXT_SHADE_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_SHADE_ID "Shade"

VisuGlExtShade* visu_gl_ext_shade_new(const gchar *name);

gboolean visu_gl_ext_shade_setShade(VisuGlExtShade *ext, ToolShade *shade);
gboolean visu_gl_ext_shade_setMinMax(VisuGlExtShade *shade, float minV, float maxV);
gboolean visu_gl_ext_shade_setScaling(VisuGlExtShade *shade, ToolMatrixScalingFlag scaling);
gboolean visu_gl_ext_shade_setMarks(VisuGlExtShade *shade, float *marks, guint n);

#endif
