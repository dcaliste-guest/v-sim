/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "surfs.h"

#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <opengl.h>
#include <visu_object.h>
#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:surfs
 * @short_description: Defines methods to draw surfaces.
 *
 * <para>#VisuSurfaces object can be drawn with this class. Simply
 * create a new #VisuGlExtSurfaces object and add surfaces with
 * visu_gl_ext_surfaces_add(). It is better to add several surfaces to
 * a single #VisuGlExtSurfaces object since all vertices are reordered
 * when necessary to ensure proper transparency.</para>
 */

#define DESC_RESOURCE_INTRA "Choose if the interior is drawn in color inverse ;"\
  " a boolean (0 or 1)"
#define FLAG_RESOURCE_INTRA "isosurfaces_drawIntra"
static gboolean INTRA_DEFAULT = FALSE;

typedef struct _VisuSurfacesOrder VisuSurfacesOrder;
/**
 * VisuSurfacesOrder:
 *
 * Short name to adress #_VisuSurfacesOrder objects.
 */
struct _VisuSurfacesOrder
{
  /* The size of the allocated arrays. */
  guint allocatedSize;

  /* Any_pointer[i][0:1] gives the id for surfaces and id for poly i
     in the z sorted from back to front. */
  int **any_pointer;
  /* Store the z value.
     The array is recomputed each time, but stored here to
     avoid constant malloc. */
  double *any_z;
  /* Give for all poly the id for surfaces object and the id for poly
     in this object. any_pointer elements point to that array. */
  int *polygon_number;
};

VisuSurfacesOrder* visu_surfaces_order_new(void);
void visu_surfaces_order_free(VisuSurfacesOrder *order);
VisuSurfacesOrder* visu_surfaces_order_copy(VisuSurfacesOrder *order);
void visu_surfaces_order_polygons(VisuSurfacesOrder *order, VisuSurfaces *surf[]);

static double z_eye(float mat[16], float points[3])
{
  return
    (mat[ 2]*points[0]+
     mat[ 6]*points[1]+
     mat[10]*points[2]+
     mat[14]*1.)/
    (mat[ 3]*points[0]+
     mat[ 7]*points[1]+
     mat[11]*points[2]+
     mat[15]*1.);
}

static void sort_by_z(int *pointer[], double *zs, int begin, int end) {
   int i;
   int middle;
   int *temp;

   if( begin >= end ) return;
   temp = pointer[begin];
   pointer[begin] = pointer[(end+begin)/2];
   pointer[(end+begin)/2] = temp;
   middle = begin;
   for(i = begin +1; i <= end; i++) {
      if ( zs[*pointer[i]] < zs[*pointer[begin]] ) {
         temp = pointer[i];
         pointer[i] = pointer[++middle];
         pointer[middle] = temp;
      }
   }
   temp = pointer[begin];
   pointer[begin] = pointer[middle];
   pointer[middle] = temp;
   sort_by_z(pointer, zs, begin, middle-1);
   sort_by_z(pointer, zs, middle+1, end);
}

/**
 * visu_surfaces_order_new:
 *
 * Create an object to hold the order in which the surfaces must be
 * drawn. See visu_surfaces_order_polygons() to set this object.
 *
 * Returns: a newly created #VisuSurfacesOrder object without any values.
 */
VisuSurfacesOrder* visu_surfaces_order_new(void)
{
  VisuSurfacesOrder *order;

  order = g_malloc(sizeof(VisuSurfacesOrder));
  order->allocatedSize = 0;
  order->any_pointer = (int**)0;
  order->any_z = (double*)0;
  order->polygon_number = (int*)0;

  return order;
}
/**
 * visu_surfaces_order_free:
 * @order: the object to be freed.
 *
 * Free memory used by a #VisuSurfacesOrder object.
 */
void visu_surfaces_order_free(VisuSurfacesOrder *order)
{
  g_return_if_fail(order);

  if (order->any_pointer)
    g_free(order->any_pointer);
  if (order->any_z)
    g_free(order->any_z);
  if (order->polygon_number)
    g_free(order->polygon_number);

  g_free(order);
}
/**
 * visu_surfaces_order_copy:
 * @order: a #VisuSurfacesOrder object.
 *
 * Do a deep copy of @order.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): a new #VisuSurfacesOrder object, copied
 * from @order.
 **/
VisuSurfacesOrder* visu_surfaces_order_copy(VisuSurfacesOrder *order)
{
  VisuSurfacesOrder *out;

  g_return_val_if_fail(order, (VisuSurfacesOrder*)0);

  out = visu_surfaces_order_new();
  out->allocatedSize = order->allocatedSize;
  if (order->any_pointer)
    {
      out->any_pointer = g_malloc(sizeof(int*) * out->allocatedSize);
      memcpy(out->any_pointer, order->any_pointer, sizeof(int*) * out->allocatedSize);
    }
  if (order->any_z)
    {
      out->any_z = g_malloc(sizeof(double) * out->allocatedSize);
      memcpy(out->any_z, order->any_z, sizeof(double) * out->allocatedSize);
    }
  if (order->polygon_number)
    {
      out->polygon_number = g_malloc(sizeof(int) * out->allocatedSize);
      memcpy(out->polygon_number, order->polygon_number, sizeof(int) * out->allocatedSize);
    }

  return out;
}
/**
 * visu_surfaces_order_polygons:
 * @order: the description of the polygons order ;
 * @surf: an array of #VisuSurfaces object, must be NULL terminated.
 *
 * Re-orders the polygons in back to front order.
 * This function should be called everytime a redraw is needed.
 */
void visu_surfaces_order_polygons(VisuSurfacesOrder *order, VisuSurfaces *surf[])
{
  guint i,j, k, idSurf, nb;
  float mat[16];
  VisuSurfacesPoints *points;

  g_return_if_fail(surf && order);

  DBG_fprintf(stderr, "Isosurfaces: re-ordering polygons in back to front order.\n");
  
  /* We compute the number of visible polygons. */
  nb = 0;
  for( idSurf = 0; surf[idSurf]; idSurf++)
    for ( i = 0; i < surf[idSurf]->nsurf; i++)
      {
        DBG_fprintf(stderr, " |  - obj %d surface %d -> %d %d (%d).\n",
                    idSurf, i, surf[idSurf]->basePoints.num_polys_surf[i],
                    surf[idSurf]->volatilePlanes.num_polys_surf[i],
                    surf[idSurf]->resources[i]->rendered);
	if (surf[idSurf]->resources[i]->rendered)
	  {
	    nb += surf[idSurf]->basePoints.num_polys_surf[i];
	    nb += surf[idSurf]->volatilePlanes.num_polys_surf[i];
	  }
      }
  DBG_fprintf(stderr, "Isosurfaces: found %d visible polygons.\n", nb);
  /* If the given order object need to be reallocated, we do it. */
  if (nb != order->allocatedSize)
    {
      DBG_fprintf(stderr, "Isosurfaces: need to reallocate the order object(%d %d).\n",
		  order->allocatedSize, nb);
      order->any_z = g_realloc(order->any_z, sizeof(double) * nb);
      order->any_pointer = g_realloc(order->any_pointer, sizeof(int*) * nb);
      order->polygon_number = g_realloc(order->polygon_number, sizeof(int) * nb * 4);
      order->allocatedSize = nb;
    }

  glGetFloatv(GL_MODELVIEW_MATRIX, mat);
  
  /* For all polygons, we compute the z position of the isobarycentre. */
  DBG_fprintf(stderr, "Isosurfaces: compute z values.\n");
  nb = 0;
  for( idSurf = 0; surf[idSurf]; idSurf++)
    {
      for (k = 0; k < 2; k++)
	{
	  points = (k == 0)?&surf[idSurf]->basePoints:&surf[idSurf]->volatilePlanes;
	  for( i = 0; i < points->num_polys; i++)
	    {
              g_return_if_fail(points->poly_surf_index[i]);
              g_return_if_fail(surf[idSurf]);
              g_return_if_fail(points->poly_surf_index[i] < 0 ||
                               surf[idSurf]->resources[points->poly_surf_index[i] - 1]);
	      if (points->poly_surf_index[i] > 0 &&
		  surf[idSurf]->resources[points->poly_surf_index[i] - 1]->rendered)
		{
		  /* Ok, we handle polygon nb. */
		  order->polygon_number[4 * nb] = nb;
		  /* We store the surfaces id of this polygon. */
		  order->polygon_number[4 * nb + 1] = idSurf;
		  /* We store the id of this polygon. */
		  order->polygon_number[4 * nb + 2] = i;
		  /* We store k to say that this polygon is from base or volatile. */
		  order->polygon_number[4 * nb + 3] = k;
		  order->any_pointer[nb] = &(order->polygon_number[4 * nb]);
		  order->any_z[nb] = 0.0;
		  for(j = 0; j < points->poly_num_vertices[i]; j++)
		    order->any_z[nb] +=
		      z_eye(mat, points->poly_points_data[points->poly_vertices[i][j]]);
		  order->any_z[nb] /= points->poly_num_vertices[i];
		  nb += 1;
		}
	    }
	}
    }
  if (nb != order->allocatedSize)
    {
      g_error("Incorrect checksum in ordering (%d | %d).", nb, order->allocatedSize);
    }
   
  DBG_fprintf(stderr, "Isosurfaces: sorting...");
  sort_by_z(order->any_pointer, order->any_z, 0, nb - 1);
  DBG_fprintf(stderr, "OK\n");
}

/**
 * VisuGlExtSurfacesClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtSurfacesClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtSurfaces:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtSurfacesPrivate:
 *
 * Private fields for #VisuGlExtSurfaces objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtSurfacesPrivate
{
  gboolean dispose_has_run;
  gboolean isBuilt;

  /* Polygon ordering for alpha drawing. */
  VisuSurfacesOrder *order;
  gboolean reorderingNeeded;

  /* Rendering characteristics. */
  gboolean drawIntra;

  /* Signals for the attached objects. */
  GList *surfs;
  VisuGlView *view;
  gulong angles_signal;
  VisuInteractive *inter;
  gulong observe_signal;
};

typedef struct _SurfaceHandleStruct
{
  VisuSurfaces *surface;
  gulong added_signal, removed_signal, masked_signal, rendering_signal, box_signal;
} _SurfaceHandle;

static VisuGlExtSurfaces* defaultSurfaces;

static void visu_gl_ext_surfaces_finalize(GObject* obj);
static void visu_gl_ext_surfaces_dispose(GObject* obj);
static void visu_gl_ext_surfaces_rebuild(VisuGlExt *ext);

/* Callbacks. */
static void onSurfaceAdded(VisuSurfaces *surfaces, gpointer data);
static void onSurfaceMasked(VisuSurfaces *surfaces, gpointer data);
static void onSurfaceBoxChanged(VisuSurfaces *surfaces, gpointer data);
static void onSurfaceRemoved(VisuSurfaces *surfaces, guint idSurf, gpointer data);
static void onSurfaceRendering(VisuSurfaces *surfaces, guint idSurf, gpointer data);
static void onCameraChange(VisuGlView *view, gpointer data);
static void onObserve(VisuInteractive *inter, gboolean start, gpointer data);
static void onEntryIntra(VisuGlExtSurfaces *surfs, gchar *key, VisuObject *obj);

/* Local methods. */
static void isosurfaces_export_resources(GString *data,
                                         VisuData *dataObj, VisuGlView *view);

/* Local routines. */
static void _freeSurfaceHandle(gpointer obj)
{
  _SurfaceHandle *shd;

  shd = (_SurfaceHandle*)obj;
  g_signal_handler_disconnect(G_OBJECT(shd->surface), shd->added_signal);
  g_signal_handler_disconnect(G_OBJECT(shd->surface), shd->removed_signal);
  g_signal_handler_disconnect(G_OBJECT(shd->surface), shd->rendering_signal);
  g_signal_handler_disconnect(G_OBJECT(shd->surface), shd->masked_signal);
  g_signal_handler_disconnect(G_OBJECT(shd->surface), shd->box_signal);
  g_object_unref(shd->surface);
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(_SurfaceHandle), obj);
#else
  g_free(obj);
#endif
}
static gpointer _newSurfaceHandle(VisuGlExtSurfaces *surfaces, VisuSurfaces *surface)
{
  _SurfaceHandle *shd;

  g_object_ref(surface);
#if GLIB_MINOR_VERSION > 9
  shd = g_slice_alloc(sizeof(_SurfaceHandle));
#else
  shd = g_malloc(sizeof(_SurfaceHandle));
#endif
  DBG_fprintf(stderr, "Extension Surfaces: add listeners on surface %p.\n", (gpointer)surface);
  shd->surface = surface;
  shd->added_signal = g_signal_connect(G_OBJECT(surface), "added",
                                       G_CALLBACK(onSurfaceAdded), (gpointer)surfaces);
  shd->removed_signal = g_signal_connect(G_OBJECT(surface), "removed",
                                         G_CALLBACK(onSurfaceRemoved), (gpointer)surfaces);
  shd->masked_signal = g_signal_connect(G_OBJECT(surface), "masked",
                                        G_CALLBACK(onSurfaceMasked), (gpointer)surfaces);
  shd->rendering_signal = g_signal_connect(G_OBJECT(surface), "rendering",
                                           G_CALLBACK(onSurfaceRendering), (gpointer)surfaces);
  shd->box_signal = g_signal_connect(G_OBJECT(surface), "setBox",
                                     G_CALLBACK(onSurfaceBoxChanged), (gpointer)surfaces);
  return (gpointer)shd;
}
static gint _cmpSurfaceHandle(gconstpointer a, gconstpointer b)
{
  _SurfaceHandle *shd_a = (_SurfaceHandle*)a;
  
  if (shd_a->surface == b)
    return 0;
  return 1;
}
#define _getSurface(H) ((_SurfaceHandle*)H)->surface

G_DEFINE_TYPE(VisuGlExtSurfaces, visu_gl_ext_surfaces, VISU_TYPE_GL_EXT)

static void visu_gl_ext_surfaces_class_init(VisuGlExtSurfacesClass *klass)
{
  VisuConfigFileEntry *entry;

  DBG_fprintf(stderr, "Extension Surfaces: creating the class of the object.\n");
  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  DBG_fprintf(stderr, "                - adding new resources ;\n");
  entry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                           FLAG_RESOURCE_INTRA,
                                           DESC_RESOURCE_INTRA,
                                           &INTRA_DEFAULT);
  visu_config_file_entry_setVersion(entry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     isosurfaces_export_resources);

  defaultSurfaces = (VisuGlExtSurfaces*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_surfaces_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_surfaces_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_surfaces_rebuild;
}
static void visu_gl_ext_surfaces_init(VisuGlExtSurfaces *obj)
{
  DBG_fprintf(stderr, "Extension Surfaces: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuGlExtSurfacesPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->isBuilt     = FALSE;
  obj->priv->drawIntra   = INTRA_DEFAULT;
  obj->priv->surfs       = (GList*)0;
  obj->priv->view           = (VisuGlView*)0;
  obj->priv->angles_signal  = 0;
  obj->priv->inter          = (VisuInteractive*)0;
  obj->priv->observe_signal = 0;
  obj->priv->reorderingNeeded = FALSE;
  obj->priv->order            = visu_surfaces_order_new();

  g_signal_connect_object(VISU_OBJECT_INSTANCE,
                          "entryParsed::" FLAG_RESOURCE_INTRA,
                          G_CALLBACK(onEntryIntra), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_gl_ext_surfaces_dispose(GObject* obj)
{
  VisuGlExtSurfaces *surfaces;
  GList *lst;

  DBG_fprintf(stderr, "Extension Surfaces: dispose object %p.\n", (gpointer)obj);

  surfaces = VISU_GL_EXT_SURFACES(obj);
  if (surfaces->priv->dispose_has_run)
    return;
  surfaces->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  for (lst = surfaces->priv->surfs; lst; lst = g_list_next(lst))
    _freeSurfaceHandle((_SurfaceHandle*)lst->data);
  visu_gl_ext_surfaces_setOnTheFlyOrdering(surfaces, (VisuGlView*)0);
  visu_gl_ext_surfaces_setOnObserveOrdering(surfaces, (VisuInteractive*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_surfaces_parent_class)->dispose(obj);
}
static void visu_gl_ext_surfaces_finalize(GObject* obj)
{
  VisuGlExtSurfaces *surfaces;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Extension Surfaces: finalize object %p.\n", (gpointer)obj);

  surfaces = VISU_GL_EXT_SURFACES(obj);
  /* Free privs elements. */
  if (surfaces->priv)
    {
      DBG_fprintf(stderr, "Extension Surfaces: free private surfaces.\n");
      visu_surfaces_order_free(surfaces->priv->order);
      g_list_free(surfaces->priv->surfs);
      g_free(surfaces->priv);
    }

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Extension Surfaces: chain to parent.\n");
  G_OBJECT_CLASS(visu_gl_ext_surfaces_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Extension Surfaces: freeing ... OK.\n");
}

/**
 * visu_gl_ext_surfaces_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_SURFACES_ID).
 *
 * Creates a new #VisuGlExt to draw surfaces.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtSurfaces* visu_gl_ext_surfaces_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_SURFACES_ID;
  char *description = _("Drawing iso-surfaces");
  VisuGlExt *extensionSurfaces;

  DBG_fprintf(stderr,"Extension Surfaces: new object.\n");
  
  extensionSurfaces = VISU_GL_EXT(g_object_new(VISU_TYPE_GL_EXT_SURFACES,
                                                  "name", (name)?name:name_,
                                                  "label", _(name),
                                                  "description", description, "nGlObj", 2,
                                                  "priority", VISU_GL_EXT_PRIORITY_NORMAL + 2,
                                                  "saveState", TRUE, NULL));
  visu_gl_ext_setSensitiveToRenderingMode(extensionSurfaces, TRUE);

  return VISU_GL_EXT_SURFACES(extensionSurfaces);
}

/**
 * visu_gl_ext_surfaces_add:
 * @surfaces: a #VisuGlExtSurfaces object.
 * @surf: (transfer full): a #VisuSurfaces object.
 *
 * Add a new surface to the list of drawn surfaces.
 *
 * Since: 3.7
 *
 * Returns: FALSE if @surf was already registered.
 **/
gboolean visu_gl_ext_surfaces_add(VisuGlExtSurfaces *surfaces, VisuSurfaces *surf)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfaces), FALSE);

  lst = g_list_find_custom(surfaces->priv->surfs, surf, _cmpSurfaceHandle);
  if (lst)
    return FALSE;

  surfaces->priv->surfs = g_list_prepend(surfaces->priv->surfs,
                                         _newSurfaceHandle(surfaces, surf));

  surfaces->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  surfaces->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(surfaces));
}
/**
 * visu_gl_ext_surfaces_remove:
 * @surfaces: a #VisuGlExtSurfaces object.
 * @surf: a #VisuSurfaces object.
 *
 * Removes @surf from the list of drawn surfaces.
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_surfaces_draw() should be called.
 **/
gboolean visu_gl_ext_surfaces_remove(VisuGlExtSurfaces *surfaces, VisuSurfaces *surf)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfaces), FALSE);
  
  lst = g_list_find_custom(surfaces->priv->surfs, surf, _cmpSurfaceHandle);
  if (!lst)
    return FALSE;

  _freeSurfaceHandle(lst->data);
  surfaces->priv->surfs = g_list_delete_link(surfaces->priv->surfs, lst);

  surfaces->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  surfaces->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(surfaces));
}
/**
 * visu_gl_ext_surfaces_setOnTheFlyOrdering:
 * @surfaces: the #VisuGlExtSurfaces object to attached to rendering view.
 * @view: (transfer full) (allow-none): a #VisuGlView object.
 *
 * Attach @surfaces to @view, so it can be rendered there. See visu_gl_ext_surfaces_draw().
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_surfaces_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_surfaces_setOnTheFlyOrdering(VisuGlExtSurfaces *surfaces,
                                                  VisuGlView *view)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfaces), FALSE);

  /* No change to be done. */
  if (view == surfaces->priv->view)
    return FALSE;

  if (surfaces->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(surfaces->priv->view),
                                  surfaces->priv->angles_signal);
      g_object_unref(surfaces->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      surfaces->priv->angles_signal =
        g_signal_connect(G_OBJECT(view), "ThetaPhiOmegaChanged",
                         G_CALLBACK(onCameraChange), (gpointer)surfaces);
    }
  else
    {
      surfaces->priv->angles_signal = 0;
    }
  surfaces->priv->view = view;

  return TRUE;
}
/**
 * visu_gl_ext_surfaces_setOnObserveOrdering:
 * @surfaces: the #VisuGlExtSurfaces object to attached to rendering inter.
 * @inter: (transfer full) (allow-none): a #VisuInteractive object.
 *
 * Attach @surfaces to @inter, so it can be rendered there. See visu_gl_ext_surfaces_draw().
 *
 * Since: 3.7
 *
 * Returns: TRUE if visu_gl_ext_surfaces_draw() should be called and then 'OpenGLAskForReDraw'
 *          signal be emitted.
 **/
gboolean visu_gl_ext_surfaces_setOnObserveOrdering(VisuGlExtSurfaces *surfaces,
                                                   VisuInteractive *inter)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfaces), FALSE);

  /* No change to be done. */
  if (inter == surfaces->priv->inter)
    return FALSE;

  if (surfaces->priv->inter)
    {
      g_signal_handler_disconnect(G_OBJECT(surfaces->priv->inter),
                                  surfaces->priv->observe_signal);
      g_object_unref(surfaces->priv->inter);
      DBG_fprintf(stderr, "Extension Surfaces: inter %p has %d ref counts.\n",
                  (gpointer)surfaces->priv->inter,
                  G_OBJECT(surfaces->priv->inter)->ref_count);
    }
  if (inter)
    {
      g_object_ref(inter);
      surfaces->priv->observe_signal =
        g_signal_connect(G_OBJECT(inter), "observe",
                         G_CALLBACK(onObserve), (gpointer)surfaces);
      DBG_fprintf(stderr, "Extension Surfaces: inter %p has %d ref counts.\n",
                  (gpointer)inter, G_OBJECT(inter)->ref_count);
    }
  else
    {
      surfaces->priv->observe_signal = 0;
    }
  surfaces->priv->inter = inter;

  return TRUE;
}

/**
 * visu_gl_ext_surfaces_getDrawIntra:
 * @surfs: a #VisuGlExtSurfaces object.
 *
 * Retrieve if the interiors of surfaces are drawn with a colour inverse or not.
 *
 * Returns: TRUE if the interior is painted in colour inverse.
 */
gboolean visu_gl_ext_surfaces_getDrawIntra(VisuGlExtSurfaces *surfs)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfs), FALSE);

  return surfs->priv->drawIntra;
}
/**
 * visu_gl_ext_surfaces_setDrawIntra:
 * @surfs: a #VisuGlExtSurfaces object.
 * @status: a boolean.
 *
 * Set if the interiors of surfaces are drawn with a colour inverse or not.
 *
 * Returns: TRUE if calling routine should redraw the surfaces with
 *          visu_gl_ext_surfaces_draw().
 */
gboolean visu_gl_ext_surfaces_setDrawIntra(VisuGlExtSurfaces *surfs, gboolean status)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SURFACES(surfs), FALSE);

  if (surfs->priv->drawIntra == status)
    return FALSE;

  surfs->priv->drawIntra = status;
  surfs->priv->isBuilt = FALSE;
  return visu_gl_ext_getActive(VISU_GL_EXT(surfs));
}

/**
 * visu_gl_ext_surfaces_getDefault:
 *
 * V_Sim is using a default surfaces object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExtSurfaces object used by default.
 **/
VisuGlExtSurfaces* visu_gl_ext_surfaces_getDefault()
{
  if (!defaultSurfaces)
    {
      defaultSurfaces = visu_gl_ext_surfaces_new((gchar*)0);
      visu_gl_ext_setActive(VISU_GL_EXT(defaultSurfaces), FALSE);
    }
  return defaultSurfaces;
}

static void sort_block_by_z(int *order, float *z, int begin, int end) {
   int i;
   int middle;
   int temp;

   if( begin >= end ) return;

   /* We make sure end + begin / 2 has found its place. */
   temp = order[begin];
   order[begin] = order[(end + begin) / 2];
   order[(end + begin) / 2] = temp;

   middle = begin;
   for(i = begin + 1; i <= end; i++) {
      if ( z[order[i]] < z[order[begin]] ) {
         temp = order[i];
	 middle += 1;
         order[i] = order[middle];
         order[middle] = temp;
      }
   }
   temp = order[begin];
   order[begin] = order[middle];
   order[middle] = temp;
   sort_block_by_z(order, z, begin, middle-1);
   sort_block_by_z(order, z, middle+1, end);
}
/**
 * visu_surfaces_duplicate:
 * @totalList: an OpenGL identifier for the global list to create ;
 * @simpleBlockList: an OpenGL identifier for the list with the
 * surfaces in the primitive cell ;
 * @box: the definition of the #VisuBox for extension ;
 * @reorder: if TRUE the blocks are drawn from back to front.
 *
 * Duplicate the list @simpleBlockList using the extension of the
 * given @box.
 */
static void _duplicate(int totalList, int simpleBlockList,
                       VisuBox *box, gboolean reorder)
{
  float ext[3], *xyzTrans, boxTrans[3], *z;
  int i, j, k, n, *order;
  float mat[16];

  DBG_fprintf(stderr, "Isosurfaces: duplicate the primitive block.\n");
  g_return_if_fail(VISU_IS_BOX(box));

  /* Duplicate the surface according to the box extension. */
  visu_box_getExtension(box, ext);
  
  if (reorder)
    glGetFloatv(GL_MODELVIEW_MATRIX, mat);

  n = (1 + 2 * (int)ext[0]) * (1 + 2 * (int)ext[1]) * (1 + 2 * (int)ext[2]);
  xyzTrans = g_malloc(sizeof(int) * 3 * n);
  z = (float*)0;
  if (reorder)
    z = g_malloc(sizeof(float) * n);
  order = g_malloc(sizeof(int) * n);
  n = 0;
  for (i = -(int)ext[0]; i < 1 + (int)ext[0]; i++)
    for (j = -(int)ext[1]; j < 1 + (int)ext[1]; j++)
      for (k = -(int)ext[2]; k < 1 + (int)ext[2]; k++)
	{
	  boxTrans[0] = (float)i;
	  boxTrans[1] = (float)j;
	  boxTrans[2] = (float)k;
          visu_box_convertBoxCoordinatestoXYZ(box, xyzTrans + 3 * n, boxTrans);
	  if (reorder)
	    z[n] = (mat[ 2] * xyzTrans[3 * n + 0] +
		    mat[ 6] * xyzTrans[3 * n + 1] +
		    mat[10] * xyzTrans[3 * n + 2] +
		    mat[14] * 1.) /
	      (mat[ 3] * xyzTrans[3 * n + 0] +
	       mat[ 7] * xyzTrans[3 * n + 1] +
	       mat[11] * xyzTrans[3 * n + 2] +
	       mat[15] * 1.);
	  order[n] = n;
	  n += 1;
	}

  if (reorder)
    /* we sort xyzTrans following z values. */
    sort_block_by_z(order, z, 0, n - 1);

  glNewList(totalList, GL_COMPILE);  
  for (i = 0; i < n; i++)
    {
/*       DBG_fprintf(stderr, "Isosurfaces: translate surfaces to box %d.\n", */
/* 		  order[i]); */
      glPushMatrix();
      glTranslated(xyzTrans[3 * order[i] + 0],
		   xyzTrans[3 * order[i] + 1],
		   xyzTrans[3 * order[i] + 2]);
      glCallList(simpleBlockList);
      glPopMatrix();
    }
  glEndList();

  g_free(order);
  g_free(xyzTrans);
  if (reorder)
    g_free(z);
}
/**
 * visu_gl_ext_surfaces_draw:
 * @surfs: a #VisuGlExtSurfaces object.
 *
 * Rebuild each visible surface's list. The order in which to draw the surfaces
 * is given in the @order argument. If the resource 'isosurfaces_drawIntra' is TRUE
 * then, the interior of the surfaces is drawn as color inverse.
 *
 * Since: 3.7
 */
void visu_gl_ext_surfaces_draw(VisuGlExtSurfaces *surfs)
{
  GList *lst;
  guint i, ip;
  VisuSurfaces **surf;
  VisuSurfacesPoints *points;
  int j, itp, idSurf;
  VisuSurfacesResources *res, *res_old;
  float rgba[4];
  VisuBox *box;
  
  g_return_if_fail(VISU_IS_GL_EXT_SURFACES(surfs));

  /* Nothing to draw; */
  if(!visu_gl_ext_getActive(VISU_GL_EXT(surfs)) ||
     surfs->priv->isBuilt) return;
   
  DBG_fprintf(stderr, "Isosurfaces: rebuilding surfaces list\n");
  surfs->priv->isBuilt = TRUE;

  glDeleteLists(visu_gl_ext_getGlList(VISU_GL_EXT(surfs)), 1);
  if (!surfs->priv->surfs)
    return;

  glNewList(visu_gl_ext_getGlList(VISU_GL_EXT(surfs)) + 1, GL_COMPILE);  
  if (surfs->priv->drawIntra)
    glEnable(GL_CULL_FACE);
  else
    glDisable(GL_CULL_FACE);
   
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);   

  /* We create an array of VisuSurfaces for the list. */
  surf = g_malloc(sizeof(VisuSurfaces*) * (g_list_length(surfs->priv->surfs) + 1));
  for( lst = surfs->priv->surfs, idSurf = 0; lst; lst = g_list_next(lst), idSurf++)
    {
      surf[idSurf] = _getSurface((_SurfaceHandle*)lst->data);
      DBG_fprintf(stderr, " | %p\n", (gpointer)surf[idSurf]);
    }
  DBG_fprintf(stderr, "Extension Surfaces: %d stored surface objects.\n", idSurf);
  surf[idSurf] = (VisuSurfaces*)0;

  /* We compute the number of visible polygons. */
  /* nb = 0; */
  /* for(idSurf = 0; surf[idSurf] ; idSurf++) */
  /*   for ( i = 0; i < visu_surfaces_getN(surf[idSurf]); i++) */
  /*     { */
  /*       DBG_fprintf(stderr, " |  - obj %d surface %d -> %d %d.\n", */
  /*                   idSurf, i, surf[idSurf]->basePoints.num_polys_surf[i], */
  /*                   surf[idSurf]->volatilePlanes.num_polys_surf[i]); */
  /*       if (visu_surfaces_resources_getRendered(visu_surfaces_getResource(surf[idSurf], i))) */
  /*         { */
  /*           nb += surf[idSurf]->basePoints.num_polys_surf[i]; */
  /*           nb += surf[idSurf]->volatilePlanes.num_polys_surf[i]; */
  /*         } */
  /*     } */
  /* DBG_fprintf(stderr, " | found %d visible polygons.\n", nb); */

  /* If order is out of date, we update. */
  if (surfs->priv->reorderingNeeded || surfs->priv->order->allocatedSize == 0)
    {
      visu_surfaces_order_polygons(surfs->priv->order, surf);
      surfs->priv->reorderingNeeded = FALSE;
    }

  DBG_fprintf(stderr, " | draw polygons.\n");
  res = res_old = (VisuSurfacesResources*)0;
  for(i = 0; i < surfs->priv->order->allocatedSize; i++)
    {
      idSurf = *(surfs->priv->order->any_pointer[i] + 1);
      /* ip is index of polygon in surf[idSurf]. */
      ip = *(surfs->priv->order->any_pointer[i] + 2);
      points = (*(surfs->priv->order->any_pointer[i] + 3) == 0)?
        &surf[idSurf]->basePoints:&surf[idSurf]->volatilePlanes;
      /* itp is index of surface of polygon ip. */
      itp = points->poly_surf_index[ip] - 1;
      /*      if(surf_draw_flag[itp] == 0) continue; */
      res = surf[idSurf]->resources[itp];
      if (res != res_old)
        {
          /* 	if(res->rendered == FALSE) */
          /* 	  continue; */
          if (surfs->priv->drawIntra)
            {
              rgba[0] = 1. - res->color->rgba[0];
              rgba[1] = 1. - res->color->rgba[1];
              rgba[2] = 1. - res->color->rgba[2];
              rgba[3] = res->color->rgba[3];
            }
          else
            visu_gl_setColor(res->material, res->color->rgba);

          /*          glCallList(lists_location + itp); */
          res_old = res;
        }
      /* This is where to find the points and the normals. */
      glBegin(GL_POLYGON);
      if (surfs->priv->drawIntra)
        visu_gl_setColor(res->material, res->color->rgba);
      for(j = 0; j < (int)points->poly_num_vertices[ip]; j++ ) {
        glNormal3fv(points->poly_points_data[points->poly_vertices[ip][j]] +
                    VISU_SURFACES_POINTS_OFFSET_NORMAL);
        glVertex3fv(points->poly_points_data[points->poly_vertices[ip][j]]);
      }
      glEnd();
    
      if (surfs->priv->drawIntra)
        {
          glBegin(GL_POLYGON);
          visu_gl_setColor(res->material, rgba);
          for(j = points->poly_num_vertices[ip] - 1; j >= 0; j-- )
            {
              glNormal3f(-points->poly_points_data[points->poly_vertices[ip][j]][VISU_SURFACES_POINTS_OFFSET_NORMAL + 0],
                         -points->poly_points_data[points->poly_vertices[ip][j]][VISU_SURFACES_POINTS_OFFSET_NORMAL + 1],
                         -points->poly_points_data[points->poly_vertices[ip][j]][VISU_SURFACES_POINTS_OFFSET_NORMAL + 2]);
              glVertex3fv(points->poly_points_data[points->poly_vertices[ip][j]]);
            }
          glEnd();
        }
    }
   
  glEndList();

  box = visu_boxed_getBox(VISU_BOXED(surf[0]));

  g_free(surf);

  /* Duplicate here. */
  _duplicate(visu_gl_ext_getGlList(VISU_GL_EXT(surfs)),
             visu_gl_ext_getGlList(VISU_GL_EXT(surfs)) + 1,
             box, TRUE);
}

/**************/
/* Callbacks. */
/**************/
static void visu_gl_ext_surfaces_rebuild(VisuGlExt *ext)
{
  VISU_GL_EXT_SURFACES(ext)->priv->isBuilt = FALSE;
  VISU_GL_EXT_SURFACES(ext)->priv->reorderingNeeded = TRUE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(ext));
}
static void onSurfaceAdded(VisuSurfaces *surfaces _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
}
static void onSurfaceBoxChanged(VisuSurfaces *surfaces _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
}
static void onSurfaceRemoved(VisuSurfaces *surfaces _U_, guint idSurf _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
}
static void onSurfaceMasked(VisuSurfaces *surfaces _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
}
static void onSurfaceRendering(VisuSurfaces *surfaces _U_, guint idSurf _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
}
static void onCameraChange(VisuGlView *view _U_, gpointer data)
{
  VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
  VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
  visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
}
static void onObserve(VisuInteractive *inter _U_, gboolean start, gpointer data)
{
  if (!VISU_GL_EXT_SURFACES(data)->priv->view && !start)
    {
      VISU_GL_EXT_SURFACES(data)->priv->reorderingNeeded = !visu_gl_getTrueTransparency();
      VISU_GL_EXT_SURFACES(data)->priv->isBuilt = FALSE;
      visu_gl_ext_surfaces_draw(VISU_GL_EXT_SURFACES(data));
    }
}
static void onEntryIntra(VisuGlExtSurfaces *surfs, gchar *key _U_, VisuObject *obj _U_)
{
  visu_gl_ext_surfaces_setDrawIntra(surfs, INTRA_DEFAULT);
}

/***************/
/* Properties. */
/***************/
static void isosurfaces_export_resources(GString *data,
                                         VisuData *dataObj _U_, VisuGlView *view _U_)
{
  if (!defaultSurfaces)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_INTRA);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_INTRA, NULL,
                               "%d", defaultSurfaces->priv->drawIntra);
  visu_config_file_exportComment(data, "");
}
