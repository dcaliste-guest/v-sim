/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATAFILE_H
#define VISU_DATAFILE_H

#include <glib.h>
#include <visu_data.h>
#include <coreTools/toolShade.h>

/**
 * VisuColorizationInputScaleId:
 * @VISU_COLORIZATION_NORMALIZE: input data are converted into [0;1] using input min/max values.
 * @VISU_COLORIZATION_MINMAX: input data are converted into [0;1] using user defined min/max values.
 *
 * Control how input data are converted into [0;1], after conversion,
 * values are clamped if needed.
 */
typedef enum
  {
    VISU_COLORIZATION_NORMALIZE,
    VISU_COLORIZATION_MINMAX
  } VisuColorizationInputScaleId;

/**
 * VISU_ERROR_COLORIZATION: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_ERROR_COLORIZATION visu_colorization_getErrorQuark()
GQuark visu_colorization_getErrorQuark();
/**
 * VisuColorizationErrorFlag:
 * @VISU_COLORIZATION_ERROR_NO_COLUMN: no column can be found in the file ;
 * @VISU_COLORIZATION_ERROR_MISSING_DATA: some data are missing to match the
 * number of nodes.
 *
 * Possible errors when reading a file with column data.
 */
typedef enum
  {
    VISU_COLORIZATION_ERROR_NO_COLUMN,
    VISU_COLORIZATION_ERROR_MISSING_DATA
  } VisuColorizationErrorFlag;

#define VISU_TYPE_COLORIZATION (visu_colorization_get_type())
GType visu_colorization_get_type(void);

typedef struct _VisuColorization VisuColorization;

VisuColorization* visu_colorization_new();
VisuColorization* visu_colorization_new_fromData(VisuData *dataObj,
                                          guint nbColumns, GArray *data, gboolean *new);
VisuColorization* visu_colorization_new_fromFile(VisuData *data, const char* filename,
                                          gboolean *new, GError **error);
VisuColorization* visu_colorization_get(VisuData *data,
                                             gboolean create, gboolean *new);


VisuColorization* visu_colorization_ref(VisuColorization *dt);
void visu_colorization_unref(VisuColorization *dt);

gboolean visu_colorization_setScaleType(VisuColorization *dt, VisuColorizationInputScaleId scale);
VisuColorizationInputScaleId visu_colorization_getScaleType(const VisuColorization *dt);

gboolean visu_colorization_setMin(VisuColorization *dt, float min, int column);
gboolean visu_colorization_setMax(VisuColorization *dt, float max, int column);
float visu_colorization_getMin(const VisuColorization *dt, int column);
float visu_colorization_getMax(const VisuColorization *dt, int column);

gboolean visu_colorization_setColUsed(VisuColorization *dt, int val, int pos);
const int* visu_colorization_getColUsed(const VisuColorization *dt);

int visu_colorization_getNColumns(const VisuColorization *dt);
gboolean visu_colorization_getSingleColumnId(const VisuColorization *dt, gint *id);

gboolean visu_colorization_setShade(VisuColorization *dt, ToolShade *shade);
ToolShade* visu_colorization_getShade(const VisuColorization *dt);

gboolean visu_colorization_setScalingUsed(VisuColorization *dt, int val);
int visu_colorization_getScalingUsed(const VisuColorization *dt);

gboolean visu_colorization_setRestrictInRange(VisuColorization *dt, gboolean status);
gboolean visu_colorization_getRestrictInRange(const VisuColorization *dt);

gboolean visu_colorization_getColumnMinMax(const VisuColorization *dt,
                                        float minMax[2], guint column);

gboolean visu_colorization_getFileSet(const VisuColorization *dt);
gchar* visu_colorization_getFile(const VisuColorization *dt);

gboolean visu_colorization_getUsed(const VisuColorization *dt);

typedef struct _VisuColorizationNodeData VisuColorizationNodeData;
/**
 * VisuColorizationNodeData:
 * @dataObj: a #VisuData object.
 * @node: a #VisuNode object.
 * @data: (element-type float): the column values for @node.
 *
 * This structure holds the column value for a given node. It is
 * mainly intended for bindings...
 */
struct _VisuColorizationNodeData
{
  const VisuData *dataObj;
  const VisuNode *node;
  GArray *data;
};
/**
 * VisuColorizationHidingFunc:
 * @dt: a #VisuColorization object ;
 * @values: the column values.
 * @data: (closure): some data.
 *
 * Function to decide to hide a node or not.
 * 
 * Returns: TRUE to hide a node depending on @values. 
 *
 * Since: 3.7
 */
typedef gboolean (*VisuColorizationHidingFunc)(VisuColorization *dt,
                                               const VisuColorizationNodeData *values,
                                               gpointer data);
void visu_colorization_setHidingFunc(VisuColorization *dt, VisuColorizationHidingFunc func,
                                     gpointer data, GDestroyNotify destroy);
gboolean visu_colorization_applyHide(VisuColorization *dt, VisuData *dataObj);



int visu_colorization_init();

gboolean visu_colorization_setUsed(VisuData *data, int val);

#endif
