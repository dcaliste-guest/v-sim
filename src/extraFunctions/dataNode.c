/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "dataNode.h"

#include <visu_tools.h>
#include <string.h>

/**
 * SECTION:dataNode
 * @short_description: Extends capabilities of node properties.
 *
 * <para>This module is a wrapper around node properties from
 * #VisuData. It adds some capabilities, such as a translatable name,
 * a way to go from data to strings and reverse, callbacks when the
 * properties is changed...</para>
 * <para>When creating a node property calling
 * visu_node_array_property_newPointer() for instance, a #VisuDataNode can be created using
 * visu_data_node_new(). A #VisuDataNode may not be attached to a specific
 * #VisuData and is a global property found on each #VisuData (or
 * potentialy found). The advantage of doing this is to keep track of
 * available properties for #VisuData objects. To get all public
 * properties, call visu_data_node_class_getAll().</para>
 */

struct _VisuDataNodeClass
{
  GObjectClass parent;
};


struct internalUsedVisuData_struct
{
  /* The #VisuData using this VisuDataNode. */
  VisuData *dataObj;
  /* Its associated dimension. */
  gint dimension;
  /* A handler on the signal emitted when the VisuData is freed. */
  gulong freeingSignal;
};

struct _VisuDataNode
{
  GObject parent;

  /* Internal object gestion. */
  gboolean dispose_has_run;

  /* The key used to store the node property. */
  gchar *id;

  /* The type of stored data. */
  GType type;

  /* These are the routines called when a string is get from values or
     values set from a string. */
  VisuDataNodeToStringFunc getAsString;
  VisuDataNodeFromStringFunc setAsString;

  /* The label. */
  const gchar *label;

  /* The dimension (must be set to use visu_data_node_setValueAsString()
     or visu_data_node_getValueAsString()), can vary. */
  GList *lstVisuData;

  /* If TRUE the data are editable and signal may be emitted. */
  gboolean editable;

  /* To be removed. */
  /* Callbacks method and data associated. */
  VisuDataNodeCallbackMethod callback;
  gpointer data;
};


enum
  {
    VISU_DATA_NODE_USED_SIGNAL,
    VISU_DATA_NODE_UNUSED_SIGNAL,
    VISU_DATA_NODE_EDITED_SIGNAL,
    VISU_DATA_NODE_VALUE_CHANGED_SIGNAL,
    VISU_DATA_NODE_NB_SIGNAL
  };

/* Internal variables. */
static GList *storedData = (GList*)0;
static GObjectClass *parent_class = NULL;
static guint dataNode_signals[VISU_DATA_NODE_NB_SIGNAL] = { 0 };


/* Object gestion methods. */
static void data_node_dispose   (GObject* obj);
static void data_node_finalize  (GObject* obj);

/* Local routines. */
static gchar* valueAsString(VisuDataNode *data, VisuData *dataObj,
			    VisuNode *node);
static gboolean valueFromString(VisuDataNode *data, VisuData *dataObj,
				VisuNode *node, gchar *labelIn,
				gchar **labelOut, gboolean *modify);

/* Local callbacks. */
static void onVisuDataFreed(VisuData *dataObj, gpointer data);

G_DEFINE_TYPE(VisuDataNode, visu_data_node, G_TYPE_OBJECT)

static void visu_data_node_class_init(VisuDataNodeClass *klass)
{
  GType paramObj[1] = {G_TYPE_OBJECT};

  DBG_fprintf(stderr, "Data Node: creating the class of the object.\n");
  parent_class = g_type_class_peek_parent(klass);

  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuDataNode::propertyUsed:
   * @data: the #VisuDataNode emitting the signal.
   * @dataObj: the #VisuData its working on.
   *
   * The given property @data gets used by @dataObj.
   *
   * Since: 3.3
   */
  dataNode_signals[VISU_DATA_NODE_USED_SIGNAL] =
    g_signal_newv("propertyUsed", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, paramObj);
  /**
   * VisuDataNode::propertyUnused:
   * @data: the #VisuDataNode emitting the signal.
   * @dataObj: the #VisuData its working on.
   *
   * The given property @data gets unused by @dataObj.
   *
   * Since: 3.3
   */
  dataNode_signals[VISU_DATA_NODE_UNUSED_SIGNAL] =
    g_signal_newv("propertyUnused", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, paramObj);
  /**
   * VisuDataNode::valueChanged:
   * @data: the #VisuDataNode emitting the signal.
   * @dataObj: the #VisuData its working on.
   *
   * The given property @data used by @dataObj has its value changed.
   *
   * Since: 3.3
   */
  dataNode_signals[VISU_DATA_NODE_VALUE_CHANGED_SIGNAL] =
    g_signal_newv("valueChanged", G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, paramObj);

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = data_node_dispose;
  G_OBJECT_CLASS(klass)->finalize = data_node_finalize;
}

static void visu_data_node_init(VisuDataNode *obj)
{
#if DEBUG == 1
  GList *tmpLst;
#endif

  obj->dispose_has_run = FALSE;

  obj->id = (gchar*)0;

  obj->type = (GType)0;
  obj->setAsString = (VisuDataNodeFromStringFunc)0;
  obj->getAsString = (VisuDataNodeToStringFunc)0;

  obj->label = (gchar*)0;

  obj->lstVisuData = (GList*)0;

  obj->editable = FALSE;
  obj->callback = (VisuDataNodeCallbackMethod)0;
  obj->data = (gpointer)0;

  /* Add object from allObjects list. */
  storedData = g_list_prepend(storedData, (gpointer)obj);
  DBG_fprintf(stderr, "Visu DataNode: add %p to the list of dataNode %p.\n",
              (gpointer)obj, (gpointer)storedData);
#if DEBUG == 1
  for (tmpLst = storedData; tmpLst; tmpLst = g_list_next(tmpLst))
    g_return_if_fail(VISU_IS_DATA_NODE_TYPE(tmpLst->data));
#endif
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void data_node_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Data Node: dispose object %p.\n", (gpointer)obj);

  if (VISU_DATA_NODE(obj)->dispose_has_run)
    return;

  VISU_DATA_NODE(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}
/* This method is called once only. */
static void data_node_finalize(GObject* obj)
{
  GList *lst;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Data Node: finalize object %p.\n", (gpointer)obj);

  g_free(VISU_DATA_NODE(obj)->id);
  for (lst = VISU_DATA_NODE(obj)->lstVisuData; lst; lst = g_list_next(lst))
    g_free(lst->data);
  g_list_free(VISU_DATA_NODE(obj)->lstVisuData);

  /* Remove object from allObjects list. */
  storedData = g_list_remove(storedData, (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


/**
 * visu_data_node_new:
 * @name: a string used to store the data as a node property ;
 * @type: the type of data stored (array or not).
 *
 * This method creates a new #VisuDataNode and registers it. A #VisuDataNode is
 * characterised by a string used as a key to store it as a node property
 * (see visu_node_array_property_newPointer()). The stored data can be arrays or not.
 * Notice that @name is copied.
 *
 * Returns: (transfer full): a newly created #VisuDataNode.
 */
GObject* visu_data_node_new(const gchar *name, GType type)
{
  VisuDataNode *data;

  g_return_val_if_fail(name, (GObject*)0);
  g_return_val_if_fail(type == G_TYPE_INT || type == G_TYPE_FLOAT ||
		       type == G_TYPE_BOOLEAN || type == G_TYPE_STRING,
		       (GObject*)0);

  /* Create the object with defaulty values. */
  data = VISU_DATA_NODE(g_object_new(VISU_DATA_NODE_TYPE, NULL));
  g_return_val_if_fail(data, (GObject*)0);

  /* Set specific values. */
  data->id   = g_strdup(name);
  data->type = type;
  data->setAsString = valueFromString;
  data->getAsString = valueAsString;

  return G_OBJECT(data);
}
/**
 * visu_data_node_newWithCallbacks:
 * @name: a string used to store the data as a node property ;
 * @setAsString: (scope call): a custom routine to change node values from a string ;
 * @getAsString: (scope call): a custom routine to create a string from node values.
 *
 * As visu_data_node_new(), but with custom @setAsString and @getAsString routines. This
 * is used to allow to treat every node related values, not just node properties.
 *
 * Returns: (transfer full): a newly created #VisuDataNode.
 */
GObject* visu_data_node_newWithCallbacks(const gchar *name,
                                         VisuDataNodeFromStringFunc setAsString,
                                         VisuDataNodeToStringFunc getAsString)
{
  VisuDataNode *data;

  g_return_val_if_fail(name, (GObject*)0);
  g_return_val_if_fail(setAsString && getAsString, (GObject*)0);

  /* Create the object with defaulty values. */
  data = VISU_DATA_NODE(g_object_new(VISU_DATA_NODE_TYPE, NULL));
  g_return_val_if_fail(data, (GObject*)0);

  /* Set specific values. */
  data->id   = g_strdup(name);
  data->type = (GType)0;
  data->setAsString = setAsString;
  data->getAsString = getAsString;

  return G_OBJECT(data);
}
/**
 * visu_data_node_class_getAll:
 *
 * All registered #VisuDataNode are stored in an intern list. This method is
 * used to retrieve it.
 *
 * Returns: (transfer none) (element-type VisuDataNode*): a #GList of
 * all registereed #VisuDataNode (this list is owned by V_Sim).
 */
GList* visu_data_node_class_getAll(void)
{
  DBG_fprintf(stderr, "Visu DataNode: get list of DataNode properties (%p %d).\n",
              (gpointer)storedData, g_list_length(storedData));
  return storedData;
}
/**
 * visu_data_node_setLabel:
 * @data: a #VisuDataNode ;
 * @label: an UTF8 string (may be translated).
 *
 * A #VisuDataNode can have a label, if not its name is used.
 * Notice that @label is not copied.
 */
void visu_data_node_setLabel(VisuDataNode *data, const gchar *label)
{
  g_return_if_fail(VISU_IS_DATA_NODE_TYPE(data));

  data->label = label;
}
/**
 * visu_data_node_getLabel:
 * @data: a #VisuDataNode ;
 *
 * A #VisuDataNode can have a label, if not its name is used.
 *
 * Returns: the label of the node (in UTF8), this string is owned by V_Sim.
 */
const gchar* visu_data_node_getLabel(VisuDataNode *data)
{
  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data), (const gchar*)0);

  if (data->label)
    return data->label;
  else
    return data->id;
}

static struct internalUsedVisuData_struct *getInternalStorage(VisuDataNode *data, VisuData *dataObj)
{
  struct internalUsedVisuData_struct *storage;
  GList *tmpLst;

  storage = (struct internalUsedVisuData_struct*)0;
  tmpLst = data->lstVisuData;
  while (!storage && tmpLst)
    {
      if (((struct internalUsedVisuData_struct*)tmpLst->data)->dataObj == dataObj)
	storage = (struct internalUsedVisuData_struct*)tmpLst->data;
      tmpLst = g_list_next(tmpLst);
    }
  return storage;
}
/**
 * visu_data_node_setUsed:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object ;
 * @nb: a positive or null integer.
 *
 * A #VisuDataNode stores data for each node of a given #VisuData. The number of data stored for
 * each node is set with this method. When some part wants to set a new node property
 * and wants to make it public, this method can be called. It also can be called when
 * the node property is removed (using a null @nb argument).
 * If @nb is changed for a positive value, the "propertyUsed"
 * signal is emitted with @dataObj as argument, instead if the value is changed
 * for null, the "propertyUnused" signal is triggered also with @dataObj as argument.
 */
void visu_data_node_setUsed(VisuDataNode *data, VisuData *dataObj, gint nb)
{
  struct internalUsedVisuData_struct *storage;

  g_return_if_fail(VISU_IS_DATA_NODE_TYPE(data) && VISU_IS_DATA(dataObj));

  storage = getInternalStorage(data, dataObj);
  if (nb > 0)
    {
      DBG_fprintf(stderr, "Data Node: associating %p to the node property '%s' (%d).\n",
		  (gpointer)dataObj, data->id,
                  g_list_length(data->lstVisuData));

      /* Add a new associated #VisuData or change an already
	 associated one. */
      if (!storage)
	{
	  storage = g_malloc(sizeof(struct internalUsedVisuData_struct));
	  storage->dataObj = dataObj;
	  storage->dimension = nb;
	  storage->freeingSignal =  g_signal_connect(G_OBJECT(dataObj), "objectFreed",
						     G_CALLBACK(onVisuDataFreed),
						     (gpointer)data);
	  data->lstVisuData = g_list_prepend(data->lstVisuData,
                                                   (gpointer)storage);

	  DBG_fprintf(stderr, "Data Node: %p emit the 'propertyUsed'.\n",
		      (gpointer)data);
	  g_signal_emit(data, dataNode_signals[VISU_DATA_NODE_USED_SIGNAL],
			0 , dataObj, NULL);
	}
      else
	storage->dimension = nb;
    }
  else
    {
      if (!storage)
	return;

      g_signal_handler_disconnect(storage->dataObj, storage->freeingSignal);
      data->lstVisuData = g_list_remove(data->lstVisuData, storage);
      g_free(storage);

      DBG_fprintf(stderr, "Data Node: removing association with %p of the"
		  " node property '%s' (%d).\n",
		  (gpointer)dataObj, data->id,
                  g_list_length(data->lstVisuData));

      DBG_fprintf(stderr, "Data Node: %p emit the 'propertyUnused'.\n", (gpointer)data);
      g_signal_emit(data, dataNode_signals[VISU_DATA_NODE_UNUSED_SIGNAL],
		    0 , dataObj, NULL);
    }
}
/**
 * visu_data_node_getUsed:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object.
 *
 * Access method to know if the given @dataObj has a public node property of the type
 * @data.
 *
 * Returns: TRUE if @dataObj has a node property of the given type.
 */
gboolean visu_data_node_getUsed(VisuDataNode *data, VisuData *dataObj)
{
  struct internalUsedVisuData_struct *storage;

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data) && VISU_IS_DATA(dataObj), FALSE);

  storage = getInternalStorage(data, dataObj);
  return (storage != (struct internalUsedVisuData_struct *)0);
}
/**
 * visu_data_node_emitValueChanged:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object.
 *
 * Emit the 'valueChanged' signal. This method should be called after
 * all changes have been done to a node property set of nodes. Then routines
 * using this property can update itself.
 */
void visu_data_node_emitValueChanged(VisuDataNode *data, VisuData *dataObj)
{
  DBG_fprintf(stderr, "Data Node: %p emit the 'valueChanged'.\n", (gpointer)data);
  g_signal_emit(data, dataNode_signals[VISU_DATA_NODE_VALUE_CHANGED_SIGNAL],
		0 , dataObj, NULL);
}
/**
 * visu_data_node_setValueAsString:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object ;
 * @node: a #VisuNode ;
 * @labelIn: a formatted string ;
 * @labelOut: a pointer to store a string.
 *
 * If the string is correctly formatted (that means it has the same format as
 * the string returned by visu_data_node_getValueAsString()) the stored values are modified.
 * The resulting string is created and put into @labelOut. If the input string @labelIn
 * is unparsable, @labelOut will contain current values. Notice that this method is
 * callable only if the @data is editable (see visu_data_node_getEditable()).
 *
 * Returns: TRUE if the string was correctly parsed.
 */
gboolean visu_data_node_setValueAsString(VisuDataNode *data, VisuData *dataObj,
				   VisuNode *node, gchar *labelIn, gchar **labelOut)
{
  gboolean valid, modify;

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data) &&
		       VISU_IS_DATA(dataObj) && node, FALSE);
  g_return_val_if_fail(labelIn && labelOut, FALSE);
  g_return_val_if_fail(data->setAsString, FALSE);

  DBG_fprintf(stderr, "Node Data: read value from '%s' for data '%s'.\n",
	      labelIn, data->id);
  valid = data->setAsString(data, dataObj, node, labelIn, labelOut, &modify);
  DBG_fprintf(stderr, "Node Data: read %d to '%s' (%d).\n",
	      valid, *labelOut, modify);

  /* Call the callback if values have been modified. */
  if (valid && modify)
    {
      /* To be removed. */
      if (data->callback)
	data->callback(dataObj, node, data->data);

      /* Emit the modified signal instead. */
    }

  return valid;
}
/**
 * visu_data_node_getValueAsString:
 * @data: a #VisuDataNode ;
 * @dataObj: a #VisuData object ;
 * @node: a #VisuNode.
 *
 * For the given node, the values stored are printed into a string.
 *
 * Returns: a newly created string.
 */
gchar* visu_data_node_getValueAsString(VisuDataNode *data, VisuData *dataObj,
				 VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data) && dataObj && node, (gchar*)0);
  g_return_val_if_fail(data->getAsString, (gchar*)0);
  
  return data->getAsString(data, dataObj, node);
}

/**
 * visu_data_node_setCallback:
 * @data: a #VisuDataNode ;
 * @callback: (scope call): a #VisuDataNodeCallbackMethod method ;
 * @user_data: a pointer to a location used to store some user data.
 *
 * When visu_data_node_setValueAsString(), values may be modified. If true, the given
 * callback method is called with @user_data as argument.
 */
void visu_data_node_setCallback(VisuDataNode *data, VisuDataNodeCallbackMethod callback,
                                gpointer user_data)
{
  g_return_if_fail(VISU_IS_DATA_NODE_TYPE(data));

  data->editable = TRUE;
  data->callback = callback;
  data->data = user_data;
}
/**
 * visu_data_node_getEditable:
 * @data: a #VisuDataNode.
 *
 * If some callback method has been given with visu_data_node_setCallback(), then
 * the values are considered editable.
 *
 * Returns: TRUE if values are editable.
 */
gboolean visu_data_node_getEditable(VisuDataNode *data)
{
  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data), FALSE);

  return data->editable;
}
/**
 * visu_data_node_setEditable:
 * @data: a #VisuDataNode ;
 * @status: a boolean value.
 *
 * Set if the values are modifiable.
 */
void visu_data_node_setEditable(VisuDataNode *data, gboolean status)
{
  g_return_if_fail(VISU_IS_DATA_NODE_TYPE(data));

  data->editable = status;
}

static void onVisuDataFreed(VisuData *dataObj, gpointer data)
{
  /* The given visuData is to be freed, so we remove it from the list
     of associated VisuData. */
  visu_data_node_setUsed(VISU_DATA_NODE(data), dataObj, 0);
}


/*********************/
/* Private routines. */
/*********************/

/* Internal fallback routines for the set and get as string when the
   types are integer, float or boolean node properties. */
static gchar* valueAsString(VisuDataNode *data, VisuData *dataObj,
			    VisuNode *node)
{
  GString *str;
  int i;
  gchar *value;
  gpointer dataValues;
  struct internalUsedVisuData_struct *storage;
  GValue val = {0, {{0}, {0}}};

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data) && dataObj && node, (gchar*)0);

  /* Check if the given property has an association with the given VisuData. */
  DBG_fprintf(stderr, "Data Node: get label for node property '%s'.\n", data->id);
  storage = getInternalStorage(data, dataObj);
  if (!storage || storage->dimension <= 0)
    {
      DBG_fprintf(stderr, "Data Node: no associated data.\n");
      return (gchar*)0;
    }

  g_value_init(&val, G_TYPE_POINTER);
  visu_node_array_getPropertyValue(VISU_NODE_ARRAY(dataObj), node,
                                   data->id, &val);
  dataValues = g_value_get_pointer(&val);
  if (!dataValues)
    {
      if (data->type != G_TYPE_STRING)
	return (gchar*)0;
      else
	return g_strdup("");
    }

  /* Set the format attribute. */
  str = g_string_new("");
  if (storage->dimension > 1)
    g_string_append(str, "( ");
  for (i = 0; i < storage->dimension; i++)
    {
      switch (data->type)
	{
	case G_TYPE_INT:
	  g_string_append_printf(str, "%d", ((int*)dataValues)[i]);
	  break;
	case G_TYPE_FLOAT:
	  g_string_append_printf(str, "%.3g", ((float*)dataValues)[i]);
	  break;
	case G_TYPE_BOOLEAN:
	  if (((gboolean*)dataValues)[i])
	    /* T for True. */
	    g_string_append(str, _("T"));
	  else
	    /* F for False. */
	    g_string_append(str, _("F"));
	  break;
	case G_TYPE_STRING:
	  g_string_append(str, (gchar*)dataValues);
	  break;
	default:
	  g_warning("Unsupported type for VisuDataNode");
	}
      if (i < storage->dimension - 1)
	g_string_append(str, " ; ");
    }
  if (storage->dimension > 1)
    g_string_append(str, " )");
  value = str->str;
  g_string_free(str, FALSE);

  DBG_fprintf(stderr, "Data Node: get values '%s'.\n", value);

  return value;
}
static gboolean valueFromString(VisuDataNode *data, VisuData *dataObj,
				VisuNode *node, gchar *labelIn,
				gchar **labelOut, gboolean *modify)
{
  int res, ln, i, valueInt;
  gchar **datas, *str;
  gpointer dataValues;
  gboolean error;
  struct internalUsedVisuData_struct *storage;
  float valueFloat;
  GValue val = {0, {{0}, {0}}};

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(data) &&
		       VISU_IS_DATA(dataObj) && node, FALSE);
  g_return_val_if_fail(labelIn && labelOut && modify, FALSE);

  if (!data->editable)
    {
      *labelOut = g_strdup(_("No data"));
      g_warning("Can't call 'visu_data_node_setValueAsString' since the property"
		" '%s' is not editable.", data->id);
      return FALSE;
    }

  /* Check if the given property has an association with the given VisuData. */
  storage = getInternalStorage(data, dataObj);
  if (!storage)
    {
      *labelOut = g_strdup(_("No data"));
      g_warning("Can't call 'visu_data_node_setValueAsString' since the property"
		" '%s' has not been associated with the given VisuData.", data->id);
      return FALSE;
    }
  
  /* Grep the property from the low level storage in the VisuData. */
  g_value_init(&val, G_TYPE_POINTER);
  visu_node_array_getPropertyValue(VISU_NODE_ARRAY(dataObj), node,
                                   data->id, &val);
  dataValues = g_value_get_pointer(&val);
  if (data->type != G_TYPE_STRING && !dataValues)
    {
      *labelOut = g_strdup(_("No data"));
      g_warning("Can't call 'visu_data_node_setValueAsString' since %p has no"
		" node property labelled '%s'", (gpointer)dataObj, data->id);
      return FALSE;
    }

  /* Parse the given labelIn.
     remove first and last parenthesis. */
  if (labelIn[0] == '(')
    labelIn += 1;
  ln = strlen(labelIn);
  if (labelIn[ln - 1] == ')')
    labelIn[ln - 1] = '\0';
  datas = g_strsplit(labelIn, ";", storage->dimension);
  *modify = FALSE;
  for (i = 0; datas[i]; i++)
    {
      DBG_fprintf(stderr, "Data Node: reading value '%s'.\n", datas[i]);
      error = FALSE;
      switch (data->type)
	{
	case G_TYPE_INT:
	  res = sscanf(datas[i], "%d", &valueInt);
	  if (res != 1)
	    error = TRUE;
	  else
	    if (((int*)dataValues)[i] != valueInt)
	      {
		((int*)dataValues)[i] = valueInt;
		*modify = TRUE;
	      }
	  break;
	case G_TYPE_FLOAT:
	  res = sscanf(datas[i], "%f", &valueFloat);
	  if (res != 1)
	    error = TRUE;
	  else
	    if (((float*)dataValues)[i] != valueFloat)
	      {
		((float*)dataValues)[i] = valueFloat;
		*modify = TRUE;
	      }
	  break;
	case G_TYPE_BOOLEAN:
	  /* T for True. */
	  if (!strcmp(datas[i], _("T")))
	    {
	      if (!((gboolean*)dataValues)[i])
		{
		  ((gboolean*)dataValues)[i] = TRUE;
		  *modify = TRUE;
		}
	    }
	  /* F for False. */
	  else if (!strcmp(datas[i], _("F")))
	    {
	      if (((gboolean*)dataValues)[i])
		{
		  ((gboolean*)dataValues)[i] = FALSE;
		  *modify = TRUE;
		}
	    }
	  else
	    error = TRUE;
	  break;
	case G_TYPE_STRING:
	  str = g_strdup(datas[i]);
	  g_strstrip(str);
	  g_value_set_pointer(&val, (gpointer)str);
	  visu_node_setpropertyValue(VISU_NODE_ARRAY(dataObj), node,
                                     data->id, &val);
	  break;
	default:
	  g_warning("Unsupported type for VisuDataNode");
	  error = TRUE;
	}
      if (error)
	{
	  *labelOut = valueAsString(data, dataObj, node);
	  g_strfreev(datas);
	  return FALSE;
	}
    }
  g_strfreev(datas);
  /* Special case of empty string. */
  if (data->type == G_TYPE_STRING && i == 0)
    {
      g_value_set_pointer(&val, (gpointer)0);
      visu_node_setpropertyValue(VISU_NODE_ARRAY(dataObj), node,
                                 data->id, &val);
      i = 1;
    }
  if (i != storage->dimension)
    error = TRUE;
  else
    error = FALSE;

  *labelOut = valueAsString(data, dataObj, node);

  return !error;
}
