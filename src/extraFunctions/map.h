/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef MAP_H
#define MAP_H

#include <glib-object.h>

#include "scalarFields.h"
#include "plane.h"
#include <coreTools/toolShade.h>
#include <coreTools/toolMatrix.h>

/**
 * VisuMapExportFormat:
 * @VISU_MAP_EXPORT_SVG: SVG export ;
 * @VISU_MAP_EXPORT_PDF: PDF export.
 *
 * Possible export for the map, see visu_map_export().
 *
 * Since: 3.6
 */
typedef enum
  {
    VISU_MAP_EXPORT_SVG,
    VISU_MAP_EXPORT_PDF
  } VisuMapExportFormat;

/**
 * VisuMap:
 *
 * All fields are private.
 *
 * Since: 3.6
 */
typedef struct _VisuMap VisuMap;

#define VISU_TYPE_MAP (visu_map_get_type())
GType    visu_map_get_type(void);

VisuMap* visu_map_new();
VisuMap* visu_map_newFromPlane(VisuPlane *plane);
VisuMap* visu_map_ref(VisuMap *map);
void     visu_map_unref(VisuMap *map);
void     visu_map_free(VisuMap *map);

gboolean visu_map_setPlane(VisuMap *map, VisuPlane *plane);
gboolean visu_map_setField(VisuMap *map, VisuScalarField *field,
                           ToolMatrixScalingFlag scale, float *inputMinMax);
gboolean visu_map_setLevel(VisuMap *map, float glPrec, float gross, float refLength);
gboolean visu_map_setLines(VisuMap *map, guint nIsoLines, float minmax[2]);

VisuPlane* visu_map_getPlane(VisuMap *map);
float* visu_map_getScaledMinMax(VisuMap *map);
float* visu_map_getFieldMinMax(VisuMap *map);

float visu_map_getLegendScale();
float visu_map_getLegendPosition(ToolXyzDir dir);

void visu_map_compute(VisuMap *map);
void visu_map_draw(VisuMap *map, float prec, ToolShade *shade, float *rgb,
                   gboolean alpha);
gboolean visu_map_export(VisuMap *map, ToolShade *shade, float *rgb, float precision,
                         gchar *filename, VisuMapExportFormat format, GError **error);

void visu_map_init();

#endif
