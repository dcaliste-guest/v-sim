/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PLANE_H
#define VISU_PLANE_H

#include <visu_box.h>
#include <visu_data.h>
#include <coreTools/toolColor.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_PLANE:
 *
 * return the type of #VisuPlane.
 */
#define VISU_TYPE_PLANE	     (visu_plane_get_type ())
/**
 * VISU_PLANE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPlane type.
 */
#define VISU_PLANE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PLANE, VisuPlane))
/**
 * VISU_PLANE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPlaneClass.
 */
#define VISU_PLANE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PLANE, VisuPlaneClass))
/**
 * VISU_IS_PLANE_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPlane object.
 */
#define VISU_IS_PLANE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PLANE))
/**
 * VISU_IS_PLANE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPlaneClass class.
 */
#define VISU_IS_PLANE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PLANE))
/**
 * VISU_PLANE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_PLANE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PLANE, VisuPlaneClass))

/**
 * VisuPlaneClass:
 *
 * An opaque structure.
 */
typedef struct _VisuPlaneClass VisuPlaneClass;

/**
 * VisuPlane:
 *
 * All fields are private, use the access routines.
 */
typedef struct _VisuPlane VisuPlane;

/**
 * visu_plane_get_type:
 *
 * This method returns the type of #VisuPlane, use VISU_TYPE_PLANE instead.
 *
 * Returns: the type of #VisuPlane.
 */
GType visu_plane_get_type(void);


/**
 * VisuPlaneHidingMode:
 * @VISU_PLANE_HIDE_UNION: element are masked if one plane at least mask it ;
 * @VISU_PLANE_HIDE_INTER: element are masked if all planes mask it ;
 * @VISU_PLANE_HIDE_N_VALUES: number of masking possibilities.
 *
 * Enum used to address different hiding modes. See visu_plane_class_setHiddingMode() for
 * further details.
 */
typedef enum
  {
    VISU_PLANE_HIDE_UNION,
    VISU_PLANE_HIDE_INTER,
    VISU_PLANE_HIDE_N_VALUES
  } VisuPlaneHidingMode;

/**
 * VISU_PLANE_SIDE_PLUS
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, the side is the one pointed by the normal vector.
 */
#define VISU_PLANE_SIDE_PLUS +1
/**
 * VISU_PLANE_SIDE_MINUS
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, the side is the one at the opposite of the one pointed
 * by the normal vector.
 */
#define VISU_PLANE_SIDE_MINUS -1
/**
 * VISU_PLANE_SIDE_NONE
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, no node is hidden.
 */
#define VISU_PLANE_SIDE_NONE 0
VisuPlane* visu_plane_new(VisuBox *box, float vect[3], float dist, ToolColor *color);
VisuPlane* visu_plane_newUndefined(void);
/**
 * visu_plane_setNormalVector:
 * @plane: a #VisuPlane object ;
 * @vect: three values defining the normal vector (unitary or not).
 *
 * Change the normal vector defining the orientation of the plane.
 *
 * Returns: 1 if the intersections should be recalculated by
 *          a call to planeComputeInter(), 0 if not. Or -1 if there is
 *          an error.
 */
gboolean visu_plane_setNormalVector(VisuPlane *plane, float vect[3]);
/**
 * visu_plane_setDistanceFromOrigin:
 * @plane: a #VisuPlane object ;
 * @dist: the distance between origin and intersection of the plane and the
 * line made by origin and normal vector.
 *
 * Change the position of the plane.
 *
 * Returns: 1 if the intersections should be recalculated by
 *          a call to planeComputeInter(), 0 if not. Or -1 if there is
 *          an error.
 */
gboolean visu_plane_setDistanceFromOrigin(VisuPlane *plane, float dist);
/**
 * visu_plane_setColor:
 * @plane: a #VisuPlane object ;
 * @color: a #ToolColor.
 *
 * Change the color of the plane.
 *
 * Returns: 0 if everything went right.
 */
gboolean visu_plane_setColor(VisuPlane *plane, ToolColor *color);

void visu_plane_getBasis(VisuPlane *plane, float xyz[2][3], float center[3]);
GList* visu_plane_getIntersection(VisuPlane *plane);
float* visu_plane_getReducedIntersection(VisuPlane *plane, guint *nVals);
/**
 * visu_plane_getNVectUser:
 * @plane: a #VisuPlane.
 * @vect: an already alloacted (size 3) float array.
 *
 * Stores the coordinates of the normal vector in @vec of the plane. It returns the values
 * given by the user, not the normalized vaues.
 */
void visu_plane_getNVectUser(VisuPlane *plane, float *vect);
/**
 * visu_plane_getNVect:
 * @plane: a #VisuPlane.
 * @vect: an already alloacted (size 3) float array.
 *
 * Stores the coordinates of the normal vector in @vect of the plane.
 * It returns the normalized values.
 */
void visu_plane_getNVect(VisuPlane *plane, float *vect);
gfloat visu_plane_getDistanceFromOrigin(VisuPlane *plane);
gboolean visu_plane_getLineIntersection(const VisuPlane *plane, const float A[3],
                                        const float B[3], float *lambda);
gboolean visu_plane_getPlaneIntersection(const VisuPlane *plane1, const VisuPlane *plane2,
                                         float A[3], float B[3]);
ToolColor* visu_plane_getColor(VisuPlane *plane);
/**
 * visu_plane_setHiddenState:
 * @plane: a #VisuPlane ;
 * @side: a key, #VISU_PLANE_SIDE_NONE, #VISU_PLANE_SIDE_PLUS or #VISU_PLANE_SIDE_MINUS.
 *
 * The plane can hide the nodes on one of its side.
 * The @side argument can be VISU_PLANE_SIDE_PLUS or VISU_PLANE_SIDE_MINUS or
 * VISU_PLANE_SIDE_NONE. It codes the side of the plane which hides the nodes.
 * If VISU_PLANE_SIDE_NONE is selected all nodes are rendered.
 * 
 * Returns: 1 if visu_plane_class_showHideAll() should be called.
 */
int visu_plane_setHiddenState(VisuPlane *plane, int side);
/**
 * visu_plane_getHiddenState:
 * @plane: a #VisuPlane.
 *
 * The plane can hide the nodes on one of its side. this
 * method get the status for the given @plane.
 * 
 * Returns: the state, defined by VISU_PLANE_SIDE_PLUS or VISU_PLANE_SIDE_MINUS or
 * VISU_PLANE_SIDE_NONE.
 */
int visu_plane_getHiddenState(VisuPlane *plane);
gboolean visu_plane_setRendered(VisuPlane *plane, gboolean rendered);
gboolean visu_plane_getRendered(VisuPlane *plane);


/* No object method. */
gboolean visu_plane_class_showHideAll(VisuPlane **listOfVisuPlanes, VisuData *visuData);
gboolean visu_plane_class_getVisibility(VisuPlane **listOfVisuPlanes, float point[3]);
gboolean visu_plane_class_getIntersection(VisuPlane **listOfVisuPlanes, float pointA[3],
                                          float pointB[3], float inter[3], gboolean inside);
/**
 * visu_plane_class_getOrderedIntersections:
 * @nVisuPlanes: the number of planes (must be consistent with the number of
 *           planes in listOfVisuPlanes!)
 * @listOfVisuPlanes: an array of #VisuPlane, NULL terminated ;
 * @pointA: three cartesian coordinates.
 * @pointB: three cartesian coordinates.
 * @inter: a pointer to the location to store the intersection points.
 *         Supposing you know the number of intersection points !
 * @index: a pointer to the location to store the indices of ordering
 *         of the planes.
 *
 * Compute the location of the intersection points of segment AB with list of
 * planes @listOfVisuPlanes. If there are several intersections, they are ordered
 * by the proximity to point A.
 *
 * Returns: TRUE if the intersections are found.
 */
gboolean visu_plane_class_getOrderedIntersections(int nVisuPlanes, VisuPlane **listOfVisuPlanes,
                                                  float pointA[3], float pointB[3], float *inter, int *index);
/**
 * visu_plane_class_setHiddingMode:
 * @mode: a value related to the hiding mode (look at the enum #VisuPlaneHidingMode).
 *
 * This method is used to set the hiding mode flag. In union mode, elements
 * are not drwn if they are hidden by one plane at least. In intersection mode,
 * elements are only hidden if masked by all planes.
 *
 * Returns: 1 if visu_plane_class_showHideAll() should be called.
 */
int visu_plane_class_setHiddingMode(VisuPlaneHidingMode mode);

gboolean visu_plane_class_parseXMLFile(gchar* filename, VisuPlane ***planes, GError **error);
/**
 * visu_plane_class_exportXMLFile:
 * @filename: the file to export to ;
 * @list: an array (NULL terminated) to export ;
 * @error: a pointer to store the error (can be NULL).
 *
 * Export in XML format the given list of planes to the given file.
 *
 * Returns: TRUE if everything goes right, if not and @error (if not NULL)
 *          is set and contains the message of the error.
 */
gboolean visu_plane_class_exportXMLFile(const gchar* filename,
                                        VisuPlane **list, GError **error);

G_END_DECLS


#endif
