/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mél :
	BILLARD, non joignable par mél ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "scalarFields.h"

#include <string.h>

#include <visu_tools.h>
#include <iface_boxed.h>
#include <coreTools/toolFileFormat.h>
#include <coreTools/toolMatrix.h>
#include <openGLFunctions/text.h>

/**
 * SECTION:scalarFields
 * @short_description:Gives capabilities to load a scalar field.
 *
 * <para>A scalar field is represented by the given of datas on a
 * regular grid meshing the bounding box. Scalar field can be read
 * from several kind of files by adding load methods using
 * scalarFieldAdd_loadMethod(). The basic implementation gives access
 * to ASCII encoded files following a simple format.</para>
 *
 * When the scalar field is periodic, the values on the border x = 1,
 * y = 1 or z = 1 can be obtained reading those of border x = 0, y = 0
 * or z = 0. So if there are n values in one direction, the nth is at
 * position 1 - 1/n in box coordinates in that direction. On the
 * contrary, for non-periodic scalar field, the nth value is at
 * coordinate 1 in box system and can be different from value 0.
 *
 * <para>In coordination with #VisuPlane and #ToolShade, scalar field can be
 * represented as coloured map calling scalarFieldDraw_map(). The
 * current implementation of interpolation is very limited since basic
 * linear approximation is used.</para>
 *
 * <para>If a structure file also contains a scalar field, when
 * loaded, it should add a #VisuData property called
 * VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE using
 * g_object_set_data(). Then V_Sim will be able to handle the
 * structure file as a density file also.</para>
 */

#define MESH_FLAG             "meshType"
#define MESH_FLAG_UNIFORM     "uniform"
#define MESH_FLAG_NON_UNIFORM "nonuniform"

/**
 * VisuScalarField:
 *
 * An opaque structure for the scalar field.
 */
/**
 * VisuScalarFieldClass:
 * @parent: the parent class.
 *
 * An opaque structure for the class.
 */

/*
 * _VisuScalarField:
 * @filename: the path to the file from which the scalar field
 *            has been read ;
 * @commentary: a commentary read from the file (must be in UTF-8) ;
 * @box: description of the associated bounding box ;
 * @nElements: number of points in each direction ;
 * @data: the values ;
 * @min: the minimum value ;
 * @max: the maximum value ;
 * @options: a GList of #Option values.
 *
 * The structure used to store a scalar field.
 */
struct _VisuScalarFieldPrivate
{
  gboolean dispose_has_run;

  /* The name of the file, where the data were read from. */
  gchar *filename;

  /* An associated commentary. */
  gchar *commentary;

  /* Description of the box. */
  VisuBox *box;
  float shift[3]; 

  /* Number of elements in each directions [x, y, z]. */
  guint nElements[3];

  /* Mesh. */
  VisuScalarFieldMeshFlags mesh_type;
  double *meshx;
  double *meshy;
  double *meshz;

  /* Datas. */
  double ***data;
  GArray *arr;

  /* Minimum and maximum values. */
  double min, max;

  /* A GList to store some options (key, values) associated
     to the data. */
  GList *options;
};

/* Local variables. */
static GList *loadMethods;

struct _VisuScalarFieldMethod
{
  ToolFileFormat parent;

  VisuScalarFieldMethodLoadFunc load;
  int priority;

  gboolean dispose_has_run;
};
struct _VisuScalarFieldMethodClass
{
  ToolFileFormatClass parent;
};

/* Object gestion methods. */
static void visu_scalar_field_dispose (GObject* obj);
static void visu_scalar_field_finalize(GObject* obj);
static void visu_boxed_interface_init (VisuBoxedInterface *iface);

/* Local methods. */
static VisuBox* visu_scalar_field_getBox(VisuBoxed *boxed);
static gboolean visu_scalar_field_setBox(VisuBoxed *self, VisuBox *box, gboolean update);
static gboolean scalarFieldLoad_fromAscii(VisuScalarFieldMethod *meth,
                                          const gchar *filename,
                                          GList **fieldList, GError **error);
static gint compareLoadPriority(gconstpointer a, gconstpointer b);

G_DEFINE_TYPE_WITH_CODE(VisuScalarField, visu_scalar_field, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_BOXED,
                                              visu_boxed_interface_init))

static void visu_scalar_field_class_init(VisuScalarFieldClass *klass)
{
  const gchar *type[] = {"*.pot", "*.dat", (char*)0};
  const gchar *descr = _("Potential/density files");

  DBG_fprintf(stderr, "VisuScalarField: creating the class of the object.\n");

  /* DBG_fprintf(stderr, "                - adding new signals ;\n"); */

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose  = visu_scalar_field_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_scalar_field_finalize;

  g_type_class_add_private(klass, sizeof(VisuScalarFieldPrivate));

  visu_scalar_field_method_new(descr, type, scalarFieldLoad_fromAscii, G_PRIORITY_LOW);
}
static void visu_boxed_interface_init(VisuBoxedInterface *iface)
{
  iface->get_box = visu_scalar_field_getBox;
  iface->set_box = visu_scalar_field_setBox;
}

static void visu_scalar_field_init(VisuScalarField *obj)
{
  DBG_fprintf(stderr, "VisuScalarField: creating a new scalar_field (%p).\n", (gpointer)obj);

  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_SCALAR_FIELD,
                                          VisuScalarFieldPrivate);

  obj->priv->dispose_has_run = FALSE;

  obj->priv->nElements[0] = 0;
  obj->priv->nElements[1] = 0;
  obj->priv->nElements[2] = 0;
  obj->priv->filename     = (gchar*)0;
  obj->priv->commentary   = (gchar*)0;
  obj->priv->meshx        = (double*)0;
  obj->priv->meshy        = (double*)0;
  obj->priv->meshz        = (double*)0;
  obj->priv->box          = (VisuBox*)0;
  obj->priv->data         = (double***)0;
  obj->priv->arr          = (GArray*)0;
  obj->priv->min          = G_MAXFLOAT;
  obj->priv->max          = -G_MAXFLOAT;
  obj->priv->mesh_type    = VISU_SCALAR_FIELD_MESH_UNIFORM;
  obj->priv->shift[0]     = 0.f;
  obj->priv->shift[1]     = 0.f;
  obj->priv->shift[2]     = 0.f;
  obj->priv->options      = (GList*)0;
}
static void visu_scalar_field_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "VisuScalarField: dispose object %p.\n", (gpointer)obj);

  if (VISU_SCALAR_FIELD(obj)->priv->dispose_has_run)
    return;
  VISU_SCALAR_FIELD(obj)->priv->dispose_has_run = TRUE;

  visu_scalar_field_setBox(VISU_BOXED(obj), (VisuBox*)0, FALSE);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_scalar_field_parent_class)->dispose(obj);
}
static void visu_scalar_field_finalize(GObject* obj)
{
  VisuScalarFieldPrivate *field = VISU_SCALAR_FIELD(obj)->priv;
  guint i;
  GList *tmplst;

  DBG_fprintf(stderr, "VisuScalarField: finalize object %p.\n", (gpointer)obj);

  g_free(field->filename);
  g_free(field->commentary);

  g_free(field->meshx);
  g_free(field->meshy);
  g_free(field->meshz);

  if (field->data)
    {
      for (i = 0; i < field->nElements[0]; i++)
        g_free(field->data[i]);
      g_free(field->data);
    }
  if (field->arr)
    g_array_unref(field->arr);

  if (field->options)
    {
      for (tmplst = field->options; tmplst; tmplst = g_list_next(tmplst))
        tool_option_free(tmplst->data);
      g_list_free(field->options);
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_scalar_field_parent_class)->finalize(obj);
}

/**
 * visu_scalar_field_new:
 * @filename: (type filename): the path to the filename the field should be read from.
 *
 * Create a new #VisuScalarField object that is empty (all internal pointers
 * are set to NULL and no memory is allocated except for the object itself.
 * The @filename argument is copied.
 *
 * Returns: (transfer full): a newly created #VisuScalarField
 * object.
 */
VisuScalarField* visu_scalar_field_new(const gchar *filename)
{
  VisuScalarField *field;

  g_return_val_if_fail(filename && filename[0], (VisuScalarField*)0);

  field = VISU_SCALAR_FIELD(g_object_new(VISU_TYPE_SCALAR_FIELD, NULL));
  field->priv->filename = g_strdup(filename);

  return field;
}

static void setToolFileFormatOption(gpointer key, gpointer value, gpointer data)
{
  DBG_fprintf(stderr, "Visu VisuScalarField: transfer option '%s' to file format.\n",
              (gchar*)key);
  tool_file_format_addOption(TOOL_FILE_FORMAT(data), tool_option_copy((ToolOption*)value));
}

/**
 * visu_scalar_field_new_fromFile:
 * @filename: (type filename): the path to the file to be loaded ;
 * @fieldList: (transfer full) (out) (element-type VisuScalarField*):
 * a #GList to store read field(s) ;
 * @table: (allow-none): a set of different options (can be NULL).
 * @error: a location on a error pointer ;
 *
 * Read the given file and try to load it as a scalar field file. If succeed,
 * all read fields are appended to the @fieldList argument. If an error
 * occurs, it is stored into @error. When entering the routine, *@error must be NULL.
 * If @table is given, it means that the caller routine gives some options to the loader
 * routine. These options are a set of names and values.
 *
 * If the file contains several fields, they must be loaded and added to @fieldList.
 *
 * Returns: (skip): TRUE if everything goes with no error.
 */
gboolean visu_scalar_field_new_fromFile(const gchar *filename, GList **fieldList,
                                        GHashTable *table, GError **error)
{
  gboolean validFormat;
  GList *tmpLst;

  g_return_val_if_fail(filename, FALSE);
  g_return_val_if_fail(*fieldList == (GList*)0, FALSE);
  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);

  DBG_fprintf(stderr, "Visu VisuScalarField: try all known formats.\n");

  /* Try all supported format. */
  validFormat = FALSE;
  for (tmpLst = loadMethods; tmpLst && !validFormat; tmpLst = g_list_next(tmpLst))
    {
      /* Transfer option from table to ToolFileFormat. */
      if (table)
        g_hash_table_foreach(table, setToolFileFormatOption, tmpLst->data);
      DBG_fprintf(stderr, " | try format '%s'.\n",
                  tool_file_format_getName(TOOL_FILE_FORMAT(tmpLst->data)));
      validFormat =
        visu_scalar_field_method_load(VISU_SCALAR_FIELD_METHOD(tmpLst->data),
                                      filename, fieldList, error);
    }
  
  if (!validFormat)
    g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_UNKNOWN_FORMAT, 
		_("unknown density/potential format.\n")); 
  DBG_fprintf(stderr, "Visu VisuScalarField: found %d field(s).\n",
              g_list_length(*fieldList));
  return validFormat;
}

static gboolean scalarFieldLoad_fromAscii(VisuScalarFieldMethod *meth _U_,
                                          const gchar *filename,
                                          GList **fieldList, GError **error)
{
  FILE *in;
  char rep[TOOL_MAX_LINE_LENGTH], flag[TOOL_MAX_LINE_LENGTH];
  char format[TOOL_MAX_LINE_LENGTH], period[TOOL_MAX_LINE_LENGTH];
  char *feed;
  gchar *comment;
  int res;
  guint i, j, k;
  guint size[3];
  double box[6];
  VisuScalarField *field;
  VisuBox *boxObj;
  gboolean periodic;
  VisuScalarFieldMeshFlags meshtype;

  g_return_val_if_fail(filename, FALSE);
  g_return_val_if_fail(*fieldList == (GList*)0, FALSE);
  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);

  DBG_fprintf(stderr, "VisuScalarField : try to read '%s' as a ASCII scalar"
	      " field data file.\n", filename);

  in = fopen(filename, "r");
  if (!in)
    {
      g_set_error(error, G_FILE_ERROR, G_FILE_ERROR_ACCES,
		  _("impossible to open the file.\n"));
      return FALSE;
    }

  /* 1st line (comment) */
  if (!fgets(rep, TOOL_MAX_LINE_LENGTH, in))
    {
      fclose(in);
      return FALSE;
    }
  rep[strlen(rep)-1] = 0; /* skipping \n */
  comment = g_locale_to_utf8(rep, -1, NULL, NULL, NULL);
  if (!comment)
    comment = g_strdup("");

  if (!fgets(rep, TOOL_MAX_LINE_LENGTH, in) ||
      sscanf(rep, "%u %u %u", size, size + 1, size + 2) != 3)
    {
      /* Not a valid ASCII format. */
      g_free(comment);
      fclose(in);
      return FALSE;
    }

  if (!fgets(rep, TOOL_MAX_LINE_LENGTH, in) ||
      sscanf(rep, "%lf %lf %lf", box, box + 1, box + 2) != 3)
    {
      /* Not a valid ASCII format. */
      g_free(comment);
      fclose(in);
      return FALSE;
    }
  if (!fgets(rep, TOOL_MAX_LINE_LENGTH, in) ||
      sscanf(rep, "%lf %lf %lf", box + 3, box + 4, box + 5) != 3)
    {
      /* Not a valid ASCII format. */
      g_free(comment);
      fclose(in);
      return FALSE;
    }

  if (!fgets(rep, TOOL_MAX_LINE_LENGTH, in) ||
      sscanf(rep, "%s %s", format, period) < 1 ||
      (strcmp(format, "xyz") && strcmp(format, "zyx")))
    {
      /* Not a valid ASCII format. */
      g_free(comment);
      fclose(in);
      return FALSE;
    }
  periodic = !strcmp(period, "periodic");

  /* OK, from now on, the format is supposed to be ASCII. */
  field = visu_scalar_field_new(filename);
  if (!field)
    {
      g_warning("impossible to create a VisuScalarField object.");
      g_free(comment);
      fclose(in);
      return FALSE;
    }

  /* by default the meshtype is set to uniform to keep working previous version.*/
  meshtype = VISU_SCALAR_FIELD_MESH_UNIFORM;
  feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in);
  while (feed && rep[0] == '#')
    {
      if (strncmp(rep + 2, MESH_FLAG, strlen(MESH_FLAG)) == 0)
	{
	  DBG_fprintf(stderr, "VisuScalarField: found flag '%s'.\n", MESH_FLAG);
	  res = sscanf(rep + 2 + strlen(MESH_FLAG) + 1, "%s", flag);
	  if (res == 1 && strcmp(flag, MESH_FLAG_UNIFORM) == 0)
	    meshtype = VISU_SCALAR_FIELD_MESH_UNIFORM;
	  else if (res == 1 && strcmp(flag, MESH_FLAG_NON_UNIFORM) == 0)
	    meshtype = VISU_SCALAR_FIELD_MESH_NON_UNIFORM;
	  else if (res == 1)
	    {
              g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("wrong '%s' value for flag '%s'.\n"), flag, MESH_FLAG);
              fclose(in);
              return TRUE;
	    }
	}
      feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in);
    }

  visu_scalar_field_setCommentary(field, comment);
  visu_scalar_field_setMeshtype(field, meshtype);
  visu_scalar_field_setGridSize(field, size);
  boxObj = visu_box_new(box, (periodic)?VISU_BOX_PERIODIC:VISU_BOX_FREE);
  visu_box_setMargin(boxObj, 0.f, FALSE);
  visu_boxed_setBox(VISU_BOXED(field), VISU_BOXED(boxObj), FALSE);
  g_object_unref(boxObj);
  *fieldList = g_list_append(*fieldList, (gpointer)field);

  if (meshtype == VISU_SCALAR_FIELD_MESH_NON_UNIFORM)
    {
      DBG_fprintf(stderr, "VisuScalarField : Start to read meshx.\n");

      for ( i = 0; i < size[0]; i++ )
        {
	  if (!feed)
	    {
	      g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("not enough meshx values.\n"));
	      fclose(in);
	      return TRUE;
	    }
          res = sscanf(rep, "%lf", &field->priv->meshx[i]);
	  do feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in); while (rep[0] == '#' && feed);
          if (res != 1)
            {
              g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("impossible to read meshx values.\n"));
              fclose(in);
              return TRUE;
            }
        }

      DBG_fprintf(stderr, "VisuScalarField : Start to read meshy.\n");

      for ( j = 0; j < size[1]; j++ )
        {
	  if (!feed)
	    {
	      g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("not enough meshy values.\n"));
	      fclose(in);
	      return TRUE;
	    }
          res = sscanf(rep, "%lf", &field->priv->meshy[j]);
	  do feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in); while (rep[0] == '#' && feed);
          if (res != 1)
            {
              g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("impossible to read meshy values.\n"));
              fclose(in);
              return TRUE;
            }
        }

      DBG_fprintf(stderr, "VisuScalarField : Start to read meshz.\n");

      for ( k = 0; k < size[2]; k++ )
        {
	  if (!feed)
	    {
	      g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("not enough meshz values.\n"));
	      fclose(in);
	      return TRUE;
	    }
          res = sscanf(rep, "%lf", &field->priv->meshz[k]);
	  do feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in); while (rep[0] == '#' && feed);
          if (res != 1)
            {
              g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			  _("impossible to read meshz values.\n"));
              fclose(in);
              return TRUE;
            }
        }
    }

  DBG_fprintf(stderr, "VisuScalarField : Start to read data.\n");

  field->priv->min = G_MAXFLOAT;
  field->priv->max = G_MINFLOAT;
  if(!strcmp(format, "xyz"))
    {
      for ( k = 0; k < size[2]; k++ ) 
	for ( j = 0; j < size[1]; j++ ) 
	  for ( i = 0; i < size[0]; i++ ) 
	    {
	      if (!feed)
		{
		  g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			      _("not enough density values.\n"));
		  fclose(in);
		  return TRUE;
		}

	      res = sscanf(rep, "%lf", &field->priv->data[i][j][k]);

	      do feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in); while (rep[0] == '#' && feed);
	      if (res != 1)
		{
		  g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			      _("impossible to read density values.\n"));
		  fclose(in);
		  return TRUE;
		}
	      field->priv->min = MIN(field->priv->data[i][j][k], field->priv->min);
	      field->priv->max = MAX(field->priv->data[i][j][k], field->priv->max);
	    }
    }
  else
    {
      for ( i = 0; i < size[0]; i++ ) 
	for ( j = 0; j < size[1]; j++ ) 
	  for ( k = 0; k < size[2]; k++ ) 
	    {
	      if (!feed)
		{
		  g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			      _("not enough density values.\n"));
		  fclose(in);
		  return TRUE;
		}
	      res = sscanf(rep, "%lf", &field->priv->data[i][j][k]);
	      do feed = fgets(rep, TOOL_MAX_LINE_LENGTH, in); while (rep[0] == '#' && feed);
	      if (res != 1)
		{
		  g_set_error(error, TOOL_FILE_FORMAT_ERROR, TOOL_FILE_FORMAT_ERROR_FORMAT,
			      _("impossible to read density values.\n"));
		  fclose(in);
		  return TRUE;
		}
	      field->priv->min = MIN(field->priv->data[i][j][k], field->priv->min);
	      field->priv->max = MAX(field->priv->data[i][j][k], field->priv->max);
	    }
    }
  DBG_fprintf(stderr, " | done.\n");
  
  fclose(in);
  return TRUE;
}
/**
 * visu_scalar_field_getCommentary:
 * @field: a #VisuScalarField object.
 *
 * If the file format support a commentary, this is a good method to get it.
 *
 * Returns: a pointer on the commentary (it should not be freed), can be NULL.
 */
const gchar* visu_scalar_field_getCommentary(VisuScalarField *field)
{
  g_return_val_if_fail(field, (gchar*)0);
  return field->priv->commentary;
}
/**
 * visu_scalar_field_setCommentary:
 * @field: a #VisuScalarField object ;
 * @comment: an UTF-8 string to store as a commentary.
 *
 * A commentary can be associated to a #VisuScalarField, use this method to set it.
 * The value of @comment is NOT copied.
 */
void visu_scalar_field_setCommentary(VisuScalarField *field, const gchar* comment)
{
  g_return_if_fail(field);
  
  field->priv->commentary = g_strdup(comment);
}
/**
 * visu_scalar_field_getMeshtype:
 * @field: a #VisuScalarField object ;
 * to be added
 *
 * The vertex may be distributed linearly along the different
 * directions or customily distributed.
 * 
 * Returns: a #VisuScalarFieldMeshFlags (uniform or nonuniform).
 */
VisuScalarFieldMeshFlags visu_scalar_field_getMeshtype(VisuScalarField *field)
{
  g_return_val_if_fail(field, VISU_SCALAR_FIELD_MESH_UNIFORM);
  return field->priv->mesh_type;
}
/**
 * visu_scalar_field_setMeshtype:
 * @field: a #VisuScalarField object ;
 * @meshtype: a #VisuScalarFieldMeshFlags object.
 *
 * Change the distribution of the vertex of the scalarfield between
 * regular or custom.
 */
void visu_scalar_field_setMeshtype(VisuScalarField *field, VisuScalarFieldMeshFlags meshtype)
{
  g_return_if_fail(field);

  field->priv->mesh_type = meshtype;
}
/**
 * visu_scalar_field_getFilename:
 * @field: a #VisuScalarField object.
 *
 * The data are read from a file.
 *
 * Returns: a pointer on the filename (it should not be freed).
 */
const gchar* visu_scalar_field_getFilename(VisuScalarField *field)
{
  g_return_val_if_fail(field, (gchar*)0);
  return field->priv->filename;
}
/**
 * visu_scalar_field_setGridSize:
 * @field: a #VisuScalarField object ;
 * @grid: (array fixed-size=3): 3 integers.
 *
 * This method is used to set the division in x, y, and z directions.
 * If the size of internal array for data is changed, it is reallocated and
 * previous data are erased. Use visu_scalar_field_getData() to get a pointer on this
 * data array.
 */
void visu_scalar_field_setGridSize(VisuScalarField *field, const guint grid[3])
{
  guint i, j;
  
  g_return_if_fail(field);
  
  if (field->priv->nElements[0] == grid[0] &&
      field->priv->nElements[1] == grid[1] &&
      field->priv->nElements[2] == grid[2])
    return;
  
  DBG_fprintf(stderr, "VisuScalarField: changing size from (%d ; %d ; %d)"
                      " to (%d ; %d ; %d).\n", field->priv->nElements[0], field->priv->nElements[1],
                      field->priv->nElements[2], grid[0], grid[1], grid[2]);

  if (field->priv->mesh_type == VISU_SCALAR_FIELD_MESH_NON_UNIFORM)
    {
      DBG_fprintf(stderr, " |�free the previous mesh allocation.\n");
      /* If mesh was already allocated, we free it. */
      if (field->priv->meshx)
        g_free(field->priv->meshx);

      if (field->priv->meshy)
        g_free(field->priv->meshy);

      if (field->priv->meshz)
        g_free(field->priv->meshz);
    }

  /* If data was already allocated, we free it. */
  if (field->priv->data)
    {
      DBG_fprintf(stderr, " |�free the previous data allocation.\n");
      for (i = 0; i < field->priv->nElements[0]; i++)
        g_free(field->priv->data[i]);
      g_free(field->priv->data);
    }
  if (field->priv->arr)
    g_array_unref(field->priv->arr);

  /* We change the size and reallocate mesh and data. */
  field->priv->nElements[0] = grid[0];
  field->priv->nElements[1] = grid[1];
  field->priv->nElements[2] = grid[2];

  if (field->priv->mesh_type == VISU_SCALAR_FIELD_MESH_NON_UNIFORM)
    {
      DBG_fprintf(stderr, "VisuScalarField: allocating meshx array.\n");
      field->priv->meshx = g_malloc(sizeof(double) * grid[0]);

      DBG_fprintf(stderr, "VisuScalarField: allocating meshy array.\n");
      field->priv->meshy = g_malloc(sizeof(double) * grid[1]);

      DBG_fprintf(stderr, "VisuScalarField: allocating meshz array.\n");
      field->priv->meshz = g_malloc(sizeof(double) * grid[2]);
    }

  DBG_fprintf(stderr, "VisuScalarField: allocating data array.\n");
  field->priv->arr  = g_array_sized_new(FALSE, FALSE, sizeof(double),
                                  grid[0] * grid[1] * grid[2]);
  g_array_set_size(field->priv->arr, grid[0] * grid[1] * grid[2]);
  field->priv->data = g_malloc(sizeof(double **) * grid[0]);
  for(i = 0; i < grid[0]; i++)
    {
      field->priv->data[i] = g_malloc(sizeof(double *) * grid[1]);
      for(j = 0; j < grid[1]; j++)
        field->priv->data[i][j] = &g_array_index(field->priv->arr, double,
                                           ((i * grid[1] + j) * grid[2]));
    }
  DBG_fprintf(stderr, " | allocation done.\n");
}
/**
 * visu_scalar_field_setOriginShift:
 * @field: a #VisuScalarField object.
 * @shift: (array fixed-size=3): a shift to apply to.
 *
 * The scalar field can be shifted with respect to the origin of the
 * #VisuData box. Use this routine to set this @shift.
 *
 * Since: 3.7
 **/
void visu_scalar_field_setOriginShift(VisuScalarField *field, const float shift[3])
{
  g_return_if_fail(field);
  
  DBG_fprintf(stderr, "VisuScalarField: set origin shift to %g,%g,%g.\n",
              shift[0], shift[1], shift[2]);
  field->priv->shift[0] = shift[0];
  field->priv->shift[1] = shift[1];
  field->priv->shift[2] = shift[2];
}
/**
 * visu_scalar_field_getOriginShift:
 * @field: a #VisuScalarField object.
 * @shift: (out caller-allocates) (type ToolVector): a location to store the shift
 * of @field.
 *
 * The scalar field can be shifted with respect to the origin of the
 * #VisuData box. Use this routine to get this @shift.
 *
 * Since: 3.7
 **/
void visu_scalar_field_getOriginShift(VisuScalarField *field, float shift[3])
{
  g_return_if_fail(field);
  
  shift[0] = field->priv->shift[0];
  shift[1] = field->priv->shift[1];
  shift[2] = field->priv->shift[2];
  DBG_fprintf(stderr, "VisuScalarField: get origin shift %g,%g,%g.\n",
              shift[0], shift[1], shift[2]);
}
/**
 * visu_scalar_field_getMinMax:
 * @field: a #VisuScalarField object ;
 * @minmax: (array fixed-size=2): two double values.
 *
 * Get the minimum and the maximum values of the given @field.
 */
void visu_scalar_field_getMinMax(VisuScalarField *field, double minmax[2])
{
  g_return_if_fail(field);
  
  minmax[0] = field->priv->min;
  minmax[1] = field->priv->max;
}
/**
 * visu_scalar_field_setData:
 * @field: a #VisuScalarField object ;
 * @data: (element-type double): an array containing data to be copied ;
 * @xyzOrder: a boolean.
 *
 * Set the data of the given @field. The array @data should be stored in z direction
 * first, followed by y and x if @xyzOrder is FALSE, or in the other
 * order when TRUE. The number of elements in the x, y and z directions
 * are read from field->priv->nElements. Then use visu_scalar_field_setGridSize()
 * before using this method.
 */
void visu_scalar_field_setData(VisuScalarField *field, GArray *data, gboolean xyzOrder)
{
  guint i, j, k, ii;
  
  g_return_if_fail(field && data);
  g_return_if_fail(data->len == field->priv->nElements[0] *
                   field->priv->nElements[1] * field->priv->nElements[2]);
  
  DBG_fprintf(stderr, "VisuScalarField: set data from array %p (%d ele.).\n",
              (gpointer)data, data->len);
  field->priv->min = G_MAXFLOAT;
  field->priv->max = -G_MAXFLOAT;
  ii = 0;
  if (xyzOrder)
    for (k = 0 ; k < field->priv->nElements[2] ; k++)
      for (j = 0 ; j < field->priv->nElements[1] ; j++)
	for (i = 0 ; i < field->priv->nElements[0] ; i++)
	  {
	    field->priv->data[i][j][k] = ((double*)data->data)[ii];
	    field->priv->min = MIN(field->priv->data[i][j][k], field->priv->min);
	    field->priv->max = MAX(field->priv->data[i][j][k], field->priv->max);
	    ii += 1;
	  }
  else
    for (i = 0 ; i < field->priv->nElements[0] ; i++)
      for (j = 0 ; j < field->priv->nElements[1] ; j++)
	for (k = 0 ; k < field->priv->nElements[2] ; k++)
	  {
	    field->priv->data[i][j][k] = ((double*)data->data)[ii];
	    field->priv->min = MIN(field->priv->data[i][j][k], field->priv->min);
	    field->priv->max = MAX(field->priv->data[i][j][k], field->priv->max);
	    ii += 1;
	  }
  DBG_fprintf(stderr, " | done.\n");
}

/**
 * visu_scalar_field_getValue:
 * @field: a #VisuScalarField object ;
 * @xyz: (array fixed-size=3): a point coordinate (in real space) ;
 * @value: (out caller-allocates): a location to store the value ;
 * @extension: (array fixed-size=3): a possible extension in box coordinates.
 *
 * Knowing the point coordinates, it interpolate a value from the
 * scalar field. If the scalar field is periodic, then it allow the
 * coordinates to extend inside the given @extension.
 *
 * Returns: TRUE if the value can be interpolate, FALSE otherwise, for instance,
 *          when the point @xyz is out of bounds.
 */
gboolean visu_scalar_field_getValue(VisuScalarField *field, float xyz[3],
                                    double *value, float extension[3])
{
  float redXyz[3], factor[3], pos;
  int l, m, n, ijk[3], dijk[3], nMax;
  int nval1, nval2;
  double *mesh;
  float xyz_[3];
  gboolean periodic[3];
  VisuScalarFieldMeshFlags meshtype;

  /* Taken from ABINIT */
  float x1,x2,x3;

  g_return_val_if_fail(field, FALSE);

  meshtype = visu_scalar_field_getMeshtype(field);

  /* First, we transform the coordinates into reduced coordinates. */
  xyz_[0] = xyz[0] + field->priv->shift[0];
  xyz_[1] = xyz[1] + field->priv->shift[1];
  xyz_[2] = xyz[2] + field->priv->shift[2];
  visu_box_convertXYZtoBoxCoordinates(field->priv->box, redXyz, xyz_);
  visu_box_getPeriodicity(field->priv->box, periodic);

  /* We compute i, j, k. */
  for (l = 0; l < 3; l++)
    {
      /* If we are periodic and inside the extension, we put back in
	 the box. */
      if (periodic[l] && redXyz[l] > -extension[l] && redXyz[l] < 1. + extension[l])
	redXyz[l] = tool_modulo_float(redXyz[l], 1);
      if (periodic[l])
	nMax = field->priv->nElements[l];
      else
	nMax = field->priv->nElements[l] - 1;

      switch (meshtype)
        {
        case VISU_SCALAR_FIELD_MESH_UNIFORM:
          pos = (float)nMax * redXyz[l];
          ijk[l] = (int)pos;
          factor[l] = pos - (float)ijk[l];
          break;
        case VISU_SCALAR_FIELD_MESH_NON_UNIFORM:
	  mesh = (double*)0;
	  switch (l)
	    {
	    case 0:
	      mesh = visu_scalar_field_getMeshx(field);
	      break;
	    case 1:
	      mesh = visu_scalar_field_getMeshy(field);
	      break;
	    case 2:
	      mesh = visu_scalar_field_getMeshz(field);
	      break;
	    }
          nval1 = 0;
          nval2 = nMax-1;
	  n = 0;
          for (m = 0; m < nMax/2; m++)
            {
              n = (int)((nval2-nval1)/2);
              if (n == 0) {
                n = nval1;
                break;    }
              else
                n = nval1+n;
              if (redXyz[l] > mesh[n])
                nval1 = n;
              else
                nval2 = n;
            }
          ijk[l] = n;
          factor[l] = (redXyz[l]-mesh[n])/(mesh[n+1]-mesh[n]);
          break;
        default:
          g_warning("Wrong value for 'meshtype'.");
          return FALSE;
        }
	
      if (ijk[l] < 0 || redXyz[l] < 0.)
	return FALSE;
      if (ijk[l] >= nMax)
	return FALSE;
    }

  /* lower left is ijk. */
  /* upper right is dijk. */
  dijk[0] = (ijk[0] + 1) % field->priv->nElements[0];
  dijk[1] = (ijk[1] + 1) % field->priv->nElements[1];
  dijk[2] = (ijk[2] + 1) % field->priv->nElements[2];
  
  /* weight is factor. */
  x1 = factor[0];
  x2 = factor[1];
  x3 = factor[2];

  /* calculation of the density value */
  *value  = 0.f;
  *value += field->priv->data[ ijk[0]][ ijk[1]][ ijk[2]] *
    (1.f - x1) * (1.f - x2) * (1.f - x3);
  *value += field->priv->data[dijk[0]][ ijk[1]][ ijk[2]] * x1 * (1.f - x2) * (1.f - x3);
  *value += field->priv->data[ ijk[0]][dijk[1]][ ijk[2]] * (1.f - x1) * x2 * (1.f - x3);
  *value += field->priv->data[ ijk[0]][ ijk[1]][dijk[2]] * (1.f - x1) * (1.f - x2) * x3;
  *value += field->priv->data[dijk[0]][dijk[1]][ ijk[2]] * x1 * x2 * (1.f - x3);
  *value += field->priv->data[ ijk[0]][dijk[1]][dijk[2]] * (1.f  -x1) * x2 * x3;
  *value += field->priv->data[dijk[0]][ ijk[1]][dijk[2]] * x1 * (1.f - x2) * x3;
  *value += field->priv->data[dijk[0]][dijk[1]][dijk[2]] * x1 * x2 * x3;

  return TRUE;
}
static gboolean visu_scalar_field_setBox(VisuBoxed *self, VisuBox *box, gboolean update _U_)
{
  VisuScalarField *field;

  g_return_val_if_fail(VISU_IS_SCALAR_FIELD_TYPE(self), FALSE);
  field = VISU_SCALAR_FIELD(self);

  DBG_fprintf(stderr, "VisuScalarField: set the bounding box to %p.\n", (gpointer)box);
  if (field->priv->box == box)
    return FALSE;

  if (field->priv->box)
    g_object_unref(field->priv->box);
  if (box)
    g_object_ref(box);
  field->priv->box = box;

  return TRUE;
}
static VisuBox* visu_scalar_field_getBox(VisuBoxed *boxed)
{
  g_return_val_if_fail(VISU_IS_SCALAR_FIELD_TYPE(boxed), (VisuBox*)0);

  return VISU_SCALAR_FIELD(boxed)->priv->box;
}
/**
 * visu_scalar_field_getGridSize:
 * @field: a #VisuScalarField object ;
 * @grid: (out caller-allocates) (type ToolGridSize): 3 integer locations.
 *
 * This method is used to get the division in x, y, and z directions.
 */
void visu_scalar_field_getGridSize(VisuScalarField *field, guint grid[3])
{
  int i;

  g_return_if_fail(field);

  DBG_fprintf(stderr, "Visu ScalarField: get grid size.\n");
  for (i = 0; i < 3; i++)
    grid[i] = field->priv->nElements[i];
  DBG_fprintf(stderr, " | done.\n");
}
/**
 * visu_scalar_field_getMeshx:
 * @field: a #VisuScalarField object.
 *
 * The mesh along x is stored as an array in x increasing.
 *
 * Returns: a pointer on the allocated meshx array (it should not be freed).
 */
double* visu_scalar_field_getMeshx(VisuScalarField *field)
{
  g_return_val_if_fail(field, (double*)0);
  return field->priv->meshx;
}
/**
 * visu_scalar_field_getMeshy:
 * @field: a #VisuScalarField object.
 *
 * The mesh along y is stored as an array in y increasing.
 *
 * Returns: a pointer on the allocated meshy array (it should not be freed).
 */
double* visu_scalar_field_getMeshy(VisuScalarField *field)
{
  g_return_val_if_fail(field, (double*)0);
  return field->priv->meshy;
}
/**
 * visu_scalar_field_getMeshz:
 * @field: a #VisuScalarField object.
 *
 * The mesh along z is stored as an array in z increasing.
 *
 * Returns: a pointer on the allocated meshz array (it should not be freed).
 */
double* visu_scalar_field_getMeshz(VisuScalarField *field)
{
  g_return_val_if_fail(field, (double*)0);
  return field->priv->meshz;
}
/**
 * visu_scalar_field_getData:
 * @field: a #VisuScalarField object.
 *
 * The data are stored as a 3 indexes array in x, y and z increasing.
 *
 * Returns: a pointer on the allocated data array (it should not be freed).
 */
double*** visu_scalar_field_getData(VisuScalarField *field)
{
  g_return_val_if_fail(field, (double***)0);

  return field->priv->data;
}
/**
 * visu_scalar_field_getDataArray:
 * @field: a #VisuScalarField object.
 *
 * The data are stored z first in a flatten array.
 *
 * Since: 3.7
 *
 * Returns: (transfer none) (element-type double): a pointer on the allocated data array.
 */
const GArray* visu_scalar_field_getDataArray(const VisuScalarField *field)
{
  g_return_val_if_fail(field, (const GArray*)0);

  return field->priv->arr;
}
/**
 * visu_scalar_field_getAllOptions:
 * @field: a #VisuScalarField object.
 *
 * Some #Option can be stored in association to the values of the scalar field.
 * These options are usually values associated to the read data, such as
 * a spin direction when dealing with density of spin...
 *
 * Returns: (transfer container) (element-type ToolOption*): a newly
 * created GList that should be freed after use with
 * g_list_free(). But data of the list are owned by V_Sim and should
 * not be modified or freed.
 */
GList* visu_scalar_field_getAllOptions(VisuScalarField *field)
{
  g_return_val_if_fail(field, (GList*)0);
  
  return g_list_copy(field->priv->options);
}
/**
 * visu_scalar_field_addOption:
 * @field: a #VisuScalarField object ;
 * @option: a newly allocated option.
 *
 * This method adds an option to the list of #Option associated to the data. The given
 * @option will not be duplicated and should not be used elsewhere because it will be freed
 * when the @field will be freed.
 */
void visu_scalar_field_addOption(VisuScalarField *field, ToolOption *option)
{
  g_return_if_fail(field && option);
  
  field->priv->options = g_list_append(field->priv->options, (gpointer)option);
}


/* Load method handling. */
static void visu_scalar_field_method_dispose(GObject* obj);
static void visu_scalar_field_method_finalize(GObject* obj);

G_DEFINE_TYPE(VisuScalarFieldMethod, visu_scalar_field_method, TOOL_TYPE_FILE_FORMAT)

static void visu_scalar_field_method_class_init(VisuScalarFieldMethodClass *klass)
{
  DBG_fprintf(stderr, "Visu VisuScalarField: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_scalar_field_method_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_scalar_field_method_finalize;

  loadMethods = (GList*)0;
}
static void visu_scalar_field_method_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu VisuScalarField: dispose object %p.\n", (gpointer)obj);

  if (VISU_SCALAR_FIELD_METHOD(obj)->dispose_has_run)
    return;

  VISU_SCALAR_FIELD_METHOD(obj)->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_scalar_field_method_parent_class)->dispose(obj);
}
static void visu_scalar_field_method_finalize(GObject* obj)
{
  DBG_fprintf(stderr, "Visu VisuScalarField: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_scalar_field_method_parent_class)->finalize(obj);
}

static void visu_scalar_field_method_init(VisuScalarFieldMethod *obj)
{
  DBG_fprintf(stderr, "Visu VisuScalarField: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->dispose_has_run = FALSE;
  obj->load = (VisuScalarFieldMethodLoadFunc)0;
  obj->priority = G_PRIORITY_LOW;

  loadMethods = g_list_prepend(loadMethods, obj);
}

/**
 * visu_scalar_field_method_new:
 * @descr: the name of the method ;
 * @patterns: (array zero-terminated=1): a NULL terminated list of strings ;
 * @method: (scope call): a #VisuScalarFieldMethodLoadFunc method ;
 * @priority: a priority value (the lower value, the higher priority).
 *
 * This routine is used to add a new method to load scalar field. The priority uses
 * the scale of the GLib (G_PRIORITY_DEFAULT is 0, G_PRIORITY_LOW is
 * 300 for instance).
 *
 * Returns: (transfer full): a newly create method to load scalar fields.
 */
VisuScalarFieldMethod* visu_scalar_field_method_new(const gchar* descr,
                                                    const gchar** patterns,
                                                    VisuScalarFieldMethodLoadFunc method,
                                                    int priority)
{
  VisuScalarFieldMethod *meth;

  g_return_val_if_fail(descr && method && patterns, (VisuScalarFieldMethod*)0);

  meth = VISU_SCALAR_FIELD_METHOD(g_object_new(VISU_TYPE_SCALAR_FIELD_METHOD,
                                               "name", descr, "ignore-type", FALSE, NULL));
  tool_file_format_addPatterns(TOOL_FILE_FORMAT(meth), patterns);
  meth->load     = method;
  meth->priority = priority;

  loadMethods = g_list_sort(loadMethods, compareLoadPriority);

  return meth;
}
/**
 * visu_scalar_field_method_load:
 * @fmt: a #VisuScalarFieldMethod object ;
 * @filename: (type filename): a path ;
 * @fieldList: (out) (element-type VisuScalarField*): a location to
 * store a list ;
 * @error: a location to an error.
 *
 * Call the load routine of the given scalar field file format @fmt.
 *
 * Since: 3.7
 *
 * Returns: TRUE on success.
 */
gboolean visu_scalar_field_method_load(VisuScalarFieldMethod *fmt,
                                       const gchar *filename, GList **fieldList,
                                       GError **error)
{
  gboolean res;

  g_return_val_if_fail(filename, FALSE);
  g_return_val_if_fail(VISU_IS_SCALAR_FIELD_METHOD(fmt), FALSE);
  g_return_val_if_fail(*fieldList == (GList*)0, FALSE);
  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);

  DBG_fprintf(stderr, "Scalar Fields: try to open the file as a '%s'.\n",
              tool_file_format_getName(TOOL_FILE_FORMAT(fmt)));
  res = fmt->load(fmt, filename, fieldList, error);
  if (!res && *error)
    {
      g_error_free(*error);
      *error = (GError*)0;
    }

  return res;
}

static gint compareLoadPriority(gconstpointer a, gconstpointer b)
{
  if (VISU_SCALAR_FIELD_METHOD(a)->priority <
      VISU_SCALAR_FIELD_METHOD(b)->priority)
    return (gint)-1;
  else if (VISU_SCALAR_FIELD_METHOD(a)->priority >
	   VISU_SCALAR_FIELD_METHOD(b)->priority)
    return (gint)+1;
  else
    return (gint)0;
}
/**
 * visu_scalar_field_method_getAll:
 *
 * This routine gives access to all the registered load method for scamlar fields.
 *
 * Returns: (transfer none) (element-type VisuScalarFieldMethod*):
 * returns a list of V_Sim owned #VisuScalarFieldMethod objects.
 */
GList* visu_scalar_field_method_getAll(void)
{
  return loadMethods;
}
