/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SCALARFIELDS_H
#define SCALARFIELDS_H

#include <glib.h>

#include <visu_box.h>
#include <coreTools/toolOptions.h>
#include <coreTools/toolMatrix.h>

/**
 * VisuScalarFieldMeshFlags:
 * @VISU_SCALAR_FIELD_MESH_UNIFORM: the mesh has constant divisions along x, y and z axis ;
 * @VISU_SCALAR_FIELD_MESH_NON_UNIFORM: the mesh has non linear divisions along x, y or z axis.
 *
 * flag (comment) standing at the begining of a Scalar field file,
 * that gives informations concerning the mesh.
 */
typedef enum
  {
    VISU_SCALAR_FIELD_MESH_UNIFORM,
    VISU_SCALAR_FIELD_MESH_NON_UNIFORM
  } VisuScalarFieldMeshFlags;

/**
 * VISU_TYPE_SCALAR_FIELD:
 *
 * return the type of #VisuScalarField.
 */
#define VISU_TYPE_SCALAR_FIELD	     (visu_scalar_field_get_type ())
/**
 * SCALAR_FIELD:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuScalarField type.
 */
#define VISU_SCALAR_FIELD(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SCALAR_FIELD, VisuScalarField))
/**
 * VISU_SCALAR_FIELD_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuScalarFieldClass.
 */
#define VISU_SCALAR_FIELD_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SCALAR_FIELD, VisuScalarFieldClass))
/**
 * VISU_IS_SCALAR_FIELD_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuScalarField object.
 */
#define VISU_IS_SCALAR_FIELD_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SCALAR_FIELD))
/**
 * VISU_IS_SCALAR_FIELD_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuScalarFieldClass class.
 */
#define VISU_IS_SCALAR_FIELD_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SCALAR_FIELD))
/**
 * VISU_SCALAR_FIELD_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SCALAR_FIELD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SCALAR_FIELD, VisuScalarFieldClass))

typedef struct _VisuScalarField        VisuScalarField;
typedef struct _VisuScalarFieldPrivate VisuScalarFieldPrivate;
typedef struct _VisuScalarFieldClass   VisuScalarFieldClass;

struct _VisuScalarField
{
  GObject parent;

  VisuScalarFieldPrivate *priv;
};

struct _VisuScalarFieldClass
{
  GObjectClass parent;
};

/**
 * visu_scalar_field_get_type:
 *
 * This method returns the type of #VisuScalarField, use VISU_TYPE_SCALAR_FIELD instead.
 *
 * Returns: the type of #VisuScalarField.
 */
GType visu_scalar_field_get_type(void);

/**
 * VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE:
 *
 * Flag used to registered a gboolean property in a #VisuData object.
 * If this flag is TRUE, the file used to read the structure can be used to
 * read a density or a potential.
 */
#define VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE "fileFormat_hasPotentialOrDensity"

VisuScalarField* visu_scalar_field_new(const gchar *filename);
gboolean visu_scalar_field_new_fromFile(const gchar *filename, GList **fieldList,
				  GHashTable *table, GError **error);

void visu_scalar_field_setCommentary(VisuScalarField *field, const gchar* comment);
void visu_scalar_field_setData(VisuScalarField *field, GArray *data, gboolean xyzOrder);
void visu_scalar_field_getGridSize(VisuScalarField *field, guint grid[3]);
void visu_scalar_field_setGridSize(VisuScalarField *field, const guint grid[3]);
void visu_scalar_field_setOriginShift(VisuScalarField *field, const float shift[3]);
void visu_scalar_field_getOriginShift(VisuScalarField *field, float shift[3]);
void visu_scalar_field_setMeshtype(VisuScalarField *field, VisuScalarFieldMeshFlags meshtype);
void visu_scalar_field_addOption(VisuScalarField *field, ToolOption *option);

void visu_scalar_field_getMinMax(VisuScalarField *field, double minmax[2]);
VisuScalarFieldMeshFlags visu_scalar_field_getMeshtype(VisuScalarField *field);
double* visu_scalar_field_getMeshx(VisuScalarField *field);
double* visu_scalar_field_getMeshy(VisuScalarField *field);
double* visu_scalar_field_getMeshz(VisuScalarField *field);
double*** visu_scalar_field_getData(VisuScalarField *field);
const GArray* visu_scalar_field_getDataArray(const VisuScalarField *field);
const gchar* visu_scalar_field_getFilename(VisuScalarField *field);
const gchar* visu_scalar_field_getCommentary(VisuScalarField *field);
GList* visu_scalar_field_getAllOptions(VisuScalarField *field);

gboolean visu_scalar_field_getValue(VisuScalarField *field, float xyz[3],
                                    double *value, float extension[3]);


/* Handling load methos. */
/**
 * VISU_TYPE_SCALAR_FIELD_METHOD:
 *
 * Return the associated #GType to the VisuScalarFieldMethod objects.
 */
#define VISU_TYPE_SCALAR_FIELD_METHOD         (visu_scalar_field_method_get_type ())
/**
 * VISU_SCALAR_FIELD_METHOD:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuScalarFieldMethod object.
 */
#define VISU_SCALAR_FIELD_METHOD(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_SCALAR_FIELD_METHOD, VisuScalarFieldMethod))
/**
 * VISU_SCALAR_FIELD_METHOD_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuScalarFieldMethodClass object.
 */
#define VISU_SCALAR_FIELD_METHOD_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_SCALAR_FIELD_METHOD, VisuScalarFieldMethodClass))
/**
 * VISU_IS_SCALAR_FIELD_METHOD:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuScalarFieldMethod object.
 */
#define VISU_IS_SCALAR_FIELD_METHOD(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_SCALAR_FIELD_METHOD))
/**
 * VISU_IS_SCALAR_FIELD_METHOD_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuScalarFieldMethodClass class.
 */
#define VISU_IS_SCALAR_FIELD_METHOD_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_SCALAR_FIELD_METHOD))
/**
 * VISU_SCALAR_FIELD_METHOD_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_SCALAR_FIELD_METHOD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SCALAR_FIELD_METHOD, VisuScalarFieldMethodClass))

/**
 * VisuScalarFieldMethod:
 *
 * An opaque structure.
 */
typedef struct _VisuScalarFieldMethod VisuScalarFieldMethod;
/**
 * VisuScalarFieldMethodClass:
 *
 * An opaque structure.
 */
typedef struct _VisuScalarFieldMethodClass VisuScalarFieldMethodClass;

/**
 * visu_scalar_field_method_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuScalarFieldMethod objects.
 */
GType visu_scalar_field_method_get_type(void);



/**
 * VisuScalarFieldMethodLoadFunc:
 * @meth: a #VisuScalarFieldMethod object ;
 * @filename: (type filename): the filename (path) the field should be loaded from ;
 * @fieldList: (out) (element-type VisuScalarField): a #GList to store read field(s) ;
 * @error: a location on a error pointer.
 *
 * Read the given file try to load it as a scalar field file.
 * If succeed (i.e. with none fatal errors) the method should return TRUE, but if not
 * @fieldList must be unchanged and the method should return TRUE. If an error
 * occurs, it is stored into @error. When entering the routine, *@error must be NULL.
 * If @table is given, it means that the caller routine gives some options to the loader
 * routine. These options are a set of names and values.
 *
 * If the file contains several fields, they must be loaded and added to @fieldList.
 * 
 *
 * Returns: TRUE if the read file is in a valid format (even with minor
 *          errors), FALSE otherwise.
 */
typedef gboolean (*VisuScalarFieldMethodLoadFunc)(VisuScalarFieldMethod *meth,
                                                  const gchar *filename,
                                                  GList **fieldList,
                                                  GError **error);

VisuScalarFieldMethod* visu_scalar_field_method_new(const gchar* descr,
                                                    const gchar** patterns,
                                                    VisuScalarFieldMethodLoadFunc method,
                                                    int priority);

gboolean visu_scalar_field_method_load(VisuScalarFieldMethod *fmt,
                                       const gchar *filename, GList **fieldList,
                                       GError **error);

GList* visu_scalar_field_method_getAll(void);

#endif
