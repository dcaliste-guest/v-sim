/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "surfaces_points.h"

#include <string.h>

#include <visu_tools.h>
#include <coreTools/toolMatrix.h>

/**
 * SECTION:surfaces_points
 * @short_description: Define a structure to store a set of triangles
 * defining a surface.
 *
 * <para>This structure is used to store and draw polyedges as a set
 * of XYZ points and a set of link to them.</para>
 */

/**
 * VisuSurfacesPoints:
 * @nsurf: number of surfaces encoded in this structure ;
 * @bufferSize: number of stored float in addition to coordinates and
 * normals ;
 * @num_polys: number of polygoins stored in this structure ;
 * @num_points: number of vertices stored in this structure ;
 * @num_polys_surf: number of visible polygons stored in this structure per surface ;
 * @poly_surf_index: gives the id of the surface for each polygon,
 *                   this value ranges from - nsurf to + nsurf. abs(id - 1) gives
 *                   the index of the surface the polygon is attached to. If values
 *                   are negative, then the polygon is currently not used ;
 * @poly_num_vertices: gives the number of vertices used by each polygons ;
 * @poly_vertices: returns the id j of the vertices of polygon i ;
 * @poly_points_data: vectors giving additional data of vertex i.
 *
 * This structure stores geometric description of surfaces.
 * Several surfaces are stored in a single structure for improved performances.
 */

/**
 * visu_surfaces_points_check:
 * @points: a set of points.
 *
 * A debug routines to check that all pointers and size are
 * relevant. It should not be used outside a debug area because it can
 * be slow.
 */
void visu_surfaces_points_check(VisuSurfacesPoints *points)
{
  guint i, j;
  guint *nbPolys;

  /* Check the surface index. */
  DBG_fprintf(stderr, " | check surface index for each polygons.\n");
  for (i = 0; i < points->num_polys; i++)
    g_return_if_fail(ABS(points->poly_surf_index[i]) > 0 &&
		     ABS(points->poly_surf_index[i]) <= points->nsurf);

  /* Check vertice index. */
  DBG_fprintf(stderr, " | check vertice index for each polygons.\n");
  for (i = 0; i < points->num_polys; i++)
    for (j = 0; j < points->poly_num_vertices[i]; j++)
      g_return_if_fail(points->poly_vertices[i][j] < points->num_points);

  /* Check the number of polygons. */
  nbPolys = g_malloc(sizeof(int) * points->nsurf);
  memset(nbPolys, 0, sizeof(int) * points->nsurf);
  for (i = 0; i < points->num_polys; i++)
    if (points->poly_surf_index[i] > 0)
      nbPolys[points->poly_surf_index[i] - 1] += 1;
  for (i = 0; i < points->nsurf; i++)
    {
      DBG_fprintf(stderr, " | %d counted %7d : stored %7d\n", i, 
		  nbPolys[i], points->num_polys_surf[i]);
      g_return_if_fail(nbPolys[i] == points->num_polys_surf[i]);
    }
  g_free(nbPolys);
}
/**
 * visu_surfaces_points_translate:
 * @points: a set of points.
 * @xyz: a given translation in cartesian coordinates.
 *
 * In devel...
 */
void visu_surfaces_points_translate(VisuSurfacesPoints *points, float xyz[3])
{
  gboolean *verticeStatus, visibility, boundary;
  guint i, j;
  int *boundaryPolys, nBounadryPolys;

  g_return_if_fail(points);

  DBG_fprintf(stderr, "IsosurfacesPoints: translate points %p of %gx%gx%g.\n",
	      (gpointer)points, xyz[0], xyz[1], xyz[2]);

  /* As for surface hide: translate and reput in the box
     except if the triangle has some points that have been
     reput in the box. */
  verticeStatus = g_malloc(sizeof(gboolean) * points->num_points);
  /* Apply the translation and compute a translation
     flag for each vertice. */
  for (i = 0; i < points->num_points; i++)
    {
      /* Translations are given in cartesian coordinates. */
/*       points->poly_points_data[j] += xyz[j]; */
      
/*       verticeStatus[i] = visu_plane_class_getVisibility(planes, surf->basePoints.poly_points_data[i]); */
    }
  /* We store the id of boundary polygons. */
  boundaryPolys = g_malloc(sizeof(int) * points->num_polys);
  nBounadryPolys = 0;
  /* Hide polygons. */
  for (i = 0; i < points->num_polys; i++)
    {
      visibility = TRUE;
      boundary = FALSE;
/*       if (surf->resources[ABS(points->poly_surf_index[i]) - 1]->sensitiveToPlanes) */
	{
	  for (j = 0; j < points->poly_num_vertices[i]; j++)
	    {
	      visibility = visibility && verticeStatus[points->poly_vertices[i][j]];
	      boundary = boundary || verticeStatus[points->poly_vertices[i][j]];
	    }
	  boundary = !visibility && boundary;
	}
      if (!visibility && points->poly_surf_index[i] > 0)
	{
	  /* Hide this polygon. */
	  points->num_polys_surf[points->poly_surf_index[i] - 1] -= 1;
	  points->poly_surf_index[i] = -points->poly_surf_index[i];
	}
      else if (visibility && points->poly_surf_index[i] < 0)
	{
	  /* Show this polygon. */
	  points->poly_surf_index[i] = -points->poly_surf_index[i];
	  points->num_polys_surf[points->poly_surf_index[i] - 1] += 1;
	}
      if (boundary)
	boundaryPolys[nBounadryPolys++] = i;
    }
/*   if (DEBUG) */
/*     for (i = 0; i < surf->nsurf; i++) */
/*       fprintf(stderr, , " | surface %2d -> %7d polygons\n", i, points->num_polys_surf[i]); */
  /* We count the number of boundaries per surface and allocate
     accordingly the volatile. */
}
/**
 * visu_surfaces_points_transform:
 * @points: a set of points.
 * @trans: a matrix.
 *
 * Apply @trans matrix to all vertices coordinates stored by @points.
 *
 * Since: 3.7
 **/
void visu_surfaces_points_transform(VisuSurfacesPoints *points, float trans[3][3])
{
  guint i;
  float old_poly_points[6];

  g_return_if_fail(points);

  DBG_fprintf(stderr, " | apply change to %d points.\n",
              points->num_points);
  for (i = 0; i < points->num_points; i++)
    {
      old_poly_points[0] = points->poly_points_data[i][0];
      old_poly_points[1] = points->poly_points_data[i][1];
      old_poly_points[2] = points->poly_points_data[i][2];
      old_poly_points[3] = points->poly_points_data[i][VISU_SURFACES_POINTS_OFFSET_NORMAL + 0];
      old_poly_points[4] = points->poly_points_data[i][VISU_SURFACES_POINTS_OFFSET_NORMAL + 1];
      old_poly_points[5] = points->poly_points_data[i][VISU_SURFACES_POINTS_OFFSET_NORMAL + 2];
      tool_matrix_productVector(points->poly_points_data[i],
                                trans, old_poly_points);
      tool_matrix_productVector(points->poly_points_data[i] + VISU_SURFACES_POINTS_OFFSET_NORMAL,
                                trans, old_poly_points + 3);
    }
}
/**
 * visu_surfaces_points_remove:
 * @points: a set of points ;
 * @pos: an integer between 0 and points->nsurf.
 *
 * Remove the points belonging to surface number @pos.
 */
void visu_surfaces_points_remove(VisuSurfacesPoints *points, guint pos)
{
  int nPoly, nPoint;
  guint i, j;
  int iPoly, iPoint;
  VisuSurfacesPoints tmpPoints;
  gboolean *usedPoints;
  int *switchArray;

  points->nsurf -= 1;

  if (!points->num_points)
    {
      points->num_polys_surf = g_realloc(points->num_polys_surf,
					 points->nsurf * sizeof(int));
      return;
    }

  DBG_fprintf(stderr, "IsosurfacesPoints: remove surface %d from points %p.\n",
              pos, (gpointer)points);
  g_return_if_fail(pos <= points->nsurf);
  
  /* Special case when there is only one remaining surface. */
  if (points->nsurf == 0)
    {
      visu_surfaces_points_free(points);
      return;
    }

  /* Simple implementation is to create a new VisuSurfacesPoints object arrays, and to
     copy everything, except the polygons belonging to the given pos. */

  /* Count number of poly and points to remove. */
  usedPoints = g_malloc(sizeof(gboolean) * points->num_points);
  memset(usedPoints, 0, sizeof(gboolean) * points->num_points);
  /* We don't use num_polys_surf since it is restricted to visible surfaces. */
  nPoly = 0;
  for (i = 0; i < points->num_polys; i++)
    if ((guint)(ABS(points->poly_surf_index[i]) - 1) != pos)
      {
	nPoly += 1;
	for (j = 0; j < points->poly_num_vertices[i]; j++)
	  usedPoints[points->poly_vertices[i][j]] = TRUE;
      }
  nPoint = 0;
  for (i = 0; i < points->num_points; i++)
    if (usedPoints[i])
      nPoint += 1;
  DBG_fprintf(stderr, " | remove %d polygons and %d points.\n",
	      points->num_polys - nPoly,
	      points->num_points - nPoint);

  visu_surfaces_points_init(&tmpPoints, points->bufferSize);
  visu_surfaces_points_allocate(&tmpPoints, points->nsurf, nPoly, nPoint);

  /* Copy from surf to tmpSurf. */
  switchArray = g_malloc(sizeof(int) * points->num_points);
  iPoint = 0;
  for (i = 0; i < points->num_points; i++)
    if (usedPoints[i])
      {
	memcpy(tmpPoints.poly_points_data[iPoint],
	       points->poly_points_data[i], sizeof(float) *
	       (VISU_SURFACES_POINTS_OFFSET_USER + points->bufferSize));
	switchArray[i] = iPoint;
	iPoint += 1;
	if (iPoint > nPoint)
	  {
	    g_error("Incorrect point checksum.");
	  }
      }
  iPoly = 0;
  for (i = 0; i < points->num_polys; i++)
    {
      if ((guint)(ABS(points->poly_surf_index[i]) - 1) != pos)
	{
	  if (points->poly_surf_index[i] > (int)pos + 1)
	    tmpPoints.poly_surf_index[iPoly] = points->poly_surf_index[i] - 1;
	  else if (points->poly_surf_index[i] < -(int)pos - 1)
	    tmpPoints.poly_surf_index[iPoly] = points->poly_surf_index[i] + 1;
	  else
	    tmpPoints.poly_surf_index[iPoly] = points->poly_surf_index[i];
	  tmpPoints.poly_num_vertices[iPoly] = points->poly_num_vertices[i];
	  tmpPoints.poly_vertices[iPoly]     =
	    g_malloc(sizeof(int) * tmpPoints.poly_num_vertices[iPoly]);
	  for (j = 0; j < tmpPoints.poly_num_vertices[iPoly]; j++)
	    tmpPoints.poly_vertices[iPoly][j] =
	      switchArray[points->poly_vertices[i][j]];
	  iPoly += 1;
	  if (iPoly > nPoly)
	    {
	      g_error("Incorrect polygon checksum.");
	    }
	}
    }
  g_free(usedPoints);
  g_free(switchArray);
  /* Check sum. */
  if (iPoly != nPoly || iPoint != nPoint)
    {
      g_error("Incorrect checksum (%d %d | %d %d).", iPoly, nPoly, iPoint, nPoint);
    }

  /* Move the number of polygons per surface. */
  for (i = pos; i < points->nsurf; i++)
    points->num_polys_surf[i] = points->num_polys_surf[i + 1];
  points->num_polys_surf = g_realloc(points->num_polys_surf,
				     sizeof(int) * points->nsurf);
  g_free(tmpPoints.num_polys_surf);

  /* We replace the arrays between tmpSurf and surf. */
  DBG_fprintf(stderr, " | switch and free arrays.\n");
  g_free(points->poly_surf_index);
  points->poly_surf_index = tmpPoints.poly_surf_index;

  g_free(points->poly_num_vertices);
  points->poly_num_vertices = tmpPoints.poly_num_vertices;

  for (i = 0; i< points->num_polys; i++)
    g_free(points->poly_vertices[i]);
  g_free(points->poly_vertices);
  points->poly_vertices = tmpPoints.poly_vertices;

  g_free(points->poly_points_data[0]);
  g_free(points->poly_points_data);
  points->poly_points_data = tmpPoints.poly_points_data;

  /* additionnal tuning. */
  points->num_polys  = nPoly;
  points->num_points = nPoint;
}
/**
 * visu_surfaces_points_free:
 * @points: a set of points.
 *
 * Free all allocated arrays of the given set of points. The point
 * structure itself is not freed.
 */
void visu_surfaces_points_free(VisuSurfacesPoints *points)
{
  guint i;

  if(points->num_polys == 0)
    return;

  if (points->num_polys_surf)
    g_free(points->num_polys_surf);
  if (points->poly_surf_index)
    g_free(points->poly_surf_index);
  if (points->poly_num_vertices)
    g_free(points->poly_num_vertices);
  if (points->poly_vertices)
    {
      for (i = 0; i < points->num_polys; i++)
	g_free(points->poly_vertices[i]);
      g_free(points->poly_vertices);
    }
  if (points->poly_points_data)
    {
      g_free(points->poly_points_data[0]);
      g_free(points->poly_points_data);
    }

  points->nsurf      = 0;
  points->num_polys  = 0;
  points->num_points = 0;
  points->num_polys_surf = (guint*)0;
  points->poly_surf_index = (int*)0;
  points->poly_num_vertices = (guint*)0;
  points->poly_vertices = (guint**)0;
  points->poly_points_data = (float **)0;
}
/**
 * visu_surfaces_points_init:
 * @points: a pointer on a set of points (not initialised) ;
 * @bufferSize: the number of additional data to coordinates and
 * normals.
 *
 * Initialise a VisuSurfacesPoints structure. It must be done before any use.
 */
void visu_surfaces_points_init(VisuSurfacesPoints *points, int bufferSize)
{
  g_return_if_fail(bufferSize >= 0 && points);
  DBG_fprintf(stderr, "IsosurfacesPoints: initialise point definitions (%p-%d).\n",
	  (gpointer)points, bufferSize);

  points->nsurf      = 0;
  points->num_polys  = 0;
  points->num_points = 0;
  points->bufferSize = bufferSize;

  points->num_polys_surf = (guint*)0;
  points->poly_surf_index = (int*)0;
  points->poly_num_vertices = (guint*)0;
  points->poly_vertices = (guint**)0;
  points->poly_points_data = (float **)0;
}
/**
 * visu_surfaces_points_allocate:
 * @points: a pointer on a set of points (not allocated) ;
 * @nsurf: the number of stored surfaces ;
 * @npolys: the number of stored polygons ;
 * @npoints: the corresponding number of points ;
 *
 * Allocate the arrays to store a set of points.
 */
void visu_surfaces_points_allocate(VisuSurfacesPoints *points, int nsurf,
			       int npolys, int npoints)
{
  int i;

  g_return_if_fail(nsurf > 0 && npolys >= 0 && npoints >= 0);
  DBG_fprintf(stderr, "IsosurfacesPoints: allocate point definitions (%d-%d).\n",
	  npolys, npoints);

  points->nsurf      = nsurf;
  points->num_polys  = npolys;
  points->num_points = npoints;

  points->num_polys_surf = g_malloc(nsurf * sizeof(int));
  memset(points->num_polys_surf, 0, sizeof(int) * nsurf);

  if (npolys == 0 || npoints == 0)
    {
      points->poly_surf_index = (int*)0;
      points->poly_num_vertices = (guint*)0;
      points->poly_vertices = (guint**)0;
      points->poly_points_data = (float **)0;
      return;
    }

  /* for each of the num_polys polygons will contain the surf value
     to which it belongs */
  points->poly_surf_index = g_malloc(npolys * sizeof(int));
  /* for each of the num_polys polygons will contain the number
     of vertices in it */
  points->poly_num_vertices = g_malloc(npolys * sizeof(int));
  /* for each of the num_polys polygons will contain the indices
     of vertices in it */   
  points->poly_vertices = g_malloc(npolys * sizeof(int*));
  memset(points->poly_vertices, 0, npolys * sizeof(int*));

  /* for each of the num_points,
     poly_points_data[][0] = x
     poly_points_data[][1] = y
     poly_points_data[][2] = z
  */ 
  points->poly_points_data = g_malloc(npoints * sizeof(float *));
  points->poly_points_data[0] = g_malloc((VISU_SURFACES_POINTS_OFFSET_USER +
					  points->bufferSize) *
					 npoints * sizeof(float));
  for(i = 0; i < npoints; i++)
    points->poly_points_data[i] = points->poly_points_data[0] +
      i * (VISU_SURFACES_POINTS_OFFSET_USER + points->bufferSize);

  DBG_fprintf(stderr, " | OK.\n");
}
