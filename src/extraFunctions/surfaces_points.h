/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SURFACES_POINTS_H
#define SURFACES_POINTS_H

#include <glib.h>

/**
 * VISU_SURFACES_POINTS_OFFSET_NORMAL:
 * 
 * The offset to read the normal values in poly_points.
 */
#define VISU_SURFACES_POINTS_OFFSET_NORMAL 3
/**
 * VISU_SURFACES_POINTS_OFFSET_TRANSLATION:
 * 
 * The offset to read the translation values in poly_points.
 */
#define VISU_SURFACES_POINTS_OFFSET_TRANSLATION 6
/**
 * VISU_SURFACES_POINTS_OFFSET_USER:
 * 
 * The offset to read the user values in poly_points.
 */
#define VISU_SURFACES_POINTS_OFFSET_USER 9

typedef struct _VisuSurfacesPoints VisuSurfacesPoints;
struct _VisuSurfacesPoints
{
  /* Number of different surfaces. */
  guint nsurf;
  /* This is the size of additional data on each points in addition to
     (x,y,z) coordinates, normal and translation. Then poly_points is
     allocated to (num_points * (9 + bufferSize)). */
  guint bufferSize;

  /* Number of polygons */
  guint num_polys, num_points;

  /* Number of polygons per surface. */
  guint *num_polys_surf;

  /* Give the number of the surface when the number of the
     polygon is given. */
  int *poly_surf_index;

  /* Return the number of vertices when the id of
     the polygon is given. */
  guint *poly_num_vertices;

  /* Return the id in poly_points_data of the vertice j of polygon i. */
  guint **poly_vertices;

  /* Vectors giving points and normal of the vertice i. */
  float **poly_points_data;
};

void visu_surfaces_points_init(VisuSurfacesPoints *points, int bufferSize);
void visu_surfaces_points_allocate(VisuSurfacesPoints *points, int nsurf,
			       int npolys, int npoints);
void visu_surfaces_points_free(VisuSurfacesPoints *points);

void visu_surfaces_points_remove(VisuSurfacesPoints *points, guint pos);

void visu_surfaces_points_translate(VisuSurfacesPoints *points, float xyz[3]);
void visu_surfaces_points_transform(VisuSurfacesPoints *points, float trans[3][3]);

void visu_surfaces_points_check(VisuSurfacesPoints *points);


#endif
