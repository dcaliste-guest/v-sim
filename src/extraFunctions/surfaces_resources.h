/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SURFACES_RESOURCES_H
#define SURFACES_RESOURCES_H

#include <glib.h>
#include <glib-object.h>

#include <coreTools/toolColor.h>

G_BEGIN_DECLS

typedef struct _VisuSurfacesResources VisuSurfacesResources;
struct _VisuSurfacesResources
{
  /* Name used to label the surface. */
  gchar *surfnom;

  /* VisuGlLightMaterial used to draw a specific surface. */
  ToolColor *color;
  float material[5];

  /* Rendered or not */
  gboolean rendered;

  /* Sensitive to masking properties of planes. */
  gboolean sensitiveToPlanes;
};

GType visu_surfaces_resources_get_type(void);
#define VISU_TYPE_SURFACES_RESOURCES (visu_surfaces_resources_get_type())

VisuSurfacesResources* visu_surfaces_resources_getFromName(const gchar *surf_name,
						 gboolean *new_surf);
gboolean visu_surfaces_resources_getRendered(const VisuSurfacesResources *res);

void visu_surfaces_resources_copy(VisuSurfacesResources *res, VisuSurfacesResources *res_old);
void visu_surfaces_resources_free(VisuSurfacesResources *res);

void visu_surfaces_resources_init(void);

G_END_DECLS

#endif
