/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_COLORCOMBOBOXWIDGET_H
#define GTK_COLORCOMBOBOXWIDGET_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

#include <visu_gtk.h>
#include <openGLFunctions/light.h>

G_BEGIN_DECLS
/**
 * VISU_TYPE_UI_COLOR_COMBOBOX:
 *
 * Return the associated #GType to the VisuUiColorCombobox objects.
 *
 * Since: 3.1
 */
#define VISU_TYPE_UI_COLOR_COMBOBOX         (visu_ui_color_combobox_get_type ())
/**
 * VISU_UI_COLOR_COMBOBOX:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiColorCombobox object.
 *
 * Since: 3.1
 */
#define VISU_UI_COLOR_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_UI_COLOR_COMBOBOX, VisuUiColorCombobox))
/**
 * VISU_UI_COLOR_COMBOBOX_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiColorComboboxClass object.
 *
 * Since: 3.1
 */
#define VISU_UI_COLOR_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_UI_COLOR_COMBOBOX, VisuUiColorComboboxClass))
/**
 * VISU_UI_IS_COLOR_COMBOBOX:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiColorCombobox object.
 *
 * Since: 3.1
 */
#define VISU_UI_IS_COLOR_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_UI_COLOR_COMBOBOX))
/**
 * VISU_UI_IS_COLOR_COMBOBOX_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiColorComboboxClass class.
 *
 * Since: 3.1
 */
#define VISU_UI_IS_COLOR_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_UI_COLOR_COMBOBOX))

typedef struct _VisuUiColorCombobox VisuUiColorCombobox;
typedef struct _VisuUiColorComboboxClass VisuUiColorComboboxClass;

GType visu_ui_color_combobox_get_type(void);
GtkWidget* visu_ui_color_combobox_new(gboolean hasAlphaChannel);
GtkWidget* visu_ui_color_combobox_newWithRanges(gboolean hasAlphaChannel);

gboolean visu_ui_color_combobox_setSelection(VisuUiColorCombobox* colorComboBox, ToolColor *color);
void visu_ui_color_combobox_setRangeColor(VisuUiColorCombobox *colorComboBox, float rgba[4],
                                          gboolean raiseSignal);
void visu_ui_color_combobox_setRangeMaterial(VisuUiColorCombobox *colorComboBox,
                                             float material[VISU_GL_LIGHT_MATERIAL_N_VALUES],
                                             gboolean raiseSignal);
ToolColor* visu_ui_color_combobox_getSelection(VisuUiColorCombobox *colorComboBox);
GdkPixbuf* visu_ui_color_combobox_getPixbufFromColor(VisuUiColorCombobox *colorComboBox,
                                                     ToolColor *color);
float* visu_ui_color_combobox_getRangeMaterial(VisuUiColorCombobox *colorComboBox);
float* visu_ui_color_combobox_getRangeColor(VisuUiColorCombobox *colorComboBox);

void visu_ui_color_combobox_setExpanded(VisuUiColorCombobox *colorComboBox, gboolean value);
GtkWidget* visu_ui_color_combobox_getRangeWidgets(VisuUiColorCombobox *colorComboBox);
void visu_ui_color_combobox_setPrintValues(VisuUiColorCombobox *colorComboBox, gboolean value);

G_END_DECLS

#endif
