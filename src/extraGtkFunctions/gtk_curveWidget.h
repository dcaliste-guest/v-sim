#ifndef __CURVEFRAME_H__
#define __CURVEFRAME_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

/**
 * VisuUiCurveFrameStyle:
 * @CURVE_LINEAR: the curve is drawn with lines ;
 * @CURVE_BAR: the curve is drawn with bars ;
 * @CURVE_GAUSS: the curve is convoluted with gaussians (not
 * implemented yet).
 *
 * Possible styles for the curve rendering, see visu_ui_curve_frame_setStyle().
 */
typedef enum
{
  CURVE_LINEAR,
  CURVE_BAR,
  CURVE_GAUSS
} VisuUiCurveFrameStyle;

/**
 * VISU_UI_TYPE_CURVE_FRAME:
 *
 * Return the associated #GType to the #VisuUiCurveFrame objects.
 *
 * Since: 3.6
 */
#define VISU_UI_TYPE_CURVE_FRAME          (visu_ui_curve_frame_get_type())
/**
 * VISU_UI_CURVE_FRAME:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiCurveFrame object.
 *
 * Since: 3.6
 */
#define VISU_UI_CURVE_FRAME(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_UI_TYPE_CURVE_FRAME, VisuUiCurveFrame))
/**
 * VISU_UI_CURVE_FRAME_CLASS:
 * @obj: the class to cast.
 *
 * Cast the given class to a #VisuUiCurveFrame object.
 *
 * Since: 3.6
 */
#define VISU_UI_CURVE_FRAME_CLASS(obj)    (G_TYPE_CHECK_CLASS_CAST((obj), VISU_UI_CURVE_FRAME, VisuUiCurveFrameClass))
/**
 * VISU_UI_IS_CURVE_FRAME:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiCurveFrame object.
 *
 * Since: 3.6
 */
#define VISU_UI_IS_CURVE_FRAME(obj)       (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_UI_TYPE_CURVE_FRAME))
/**
 * VISU_UI_IS_CURVE_FRAME_CLASS:
 * @obj: the class to test.
 *
 * Return if the given class is a valid #VisuUiCurveFrameClass class.
 *
 * Since: 3.6
 */
#define VISU_UI_IS_CURVE_FRAME_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj), VISU_UI_TYPE_CURVE_FRAME))
/**
 * VISU_UI_CURVE_FRAME_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 *
 * Since: 3.6
 */
#define VISU_UI_CURVE_FRAME_GET_CLASS     (G_TYPE_INSTANCE_GET_CLASS((obj), VISU_UI_TYPE_CURVE_FRAME, VisuUiCurveFrameClass))

typedef struct _VisuUiCurveFrame            VisuUiCurveFrame;
typedef struct _VisuUiCurveFrameClass       VisuUiCurveFrameClass;

GType visu_ui_curve_frame_get_type(void);
GtkWidget *visu_ui_curve_frame_new(float distMin, float distMax);
void visu_ui_curve_frame_draw(VisuUiCurveFrame *curve);

gboolean visu_ui_curve_frame_setSpan(VisuUiCurveFrame *curve, float span[2]);
void visu_ui_curve_frame_getSpan(VisuUiCurveFrame *curve, float span[2]);

gboolean visu_ui_curve_frame_hasData(VisuUiCurveFrame *curve);
void visu_ui_curve_frame_setData(VisuUiCurveFrame *curve, float step, float min, float max);
void visu_ui_curve_frame_addData(VisuUiCurveFrame *curve, const gchar *eleName, const gchar *lkName,
			const guint *data, guint nSteps, float init, float step);
gboolean visu_ui_curve_frame_setStyle(VisuUiCurveFrame *curve, VisuUiCurveFrameStyle style);
gboolean visu_ui_curve_frame_setFilter(VisuUiCurveFrame *curve, const gchar* filter);
gboolean visu_ui_curve_frame_setHighlightRange(VisuUiCurveFrame *curve, float range[2]);
gboolean visu_ui_curve_frame_getHighlightRange(VisuUiCurveFrame *curve, float range[2]);
void visu_ui_curve_frame_setNNodes(VisuUiCurveFrame *curve, const gchar *ele, guint n);
float visu_ui_curve_frame_getIntegralInRange(VisuUiCurveFrame *curve, gchar **label);
float visu_ui_curve_frame_getMeanInRange(VisuUiCurveFrame *curve, gchar **label);

G_END_DECLS

#endif
