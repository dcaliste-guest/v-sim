/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GTK_ELEMENTCOMBOBOX_H
#define GTK_ELEMENTCOMBOBOX_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

/**
 * VISU_UI_TYPE_ELEMENT_COMBOBOX:
 *
 * Return the associated #GType to the #VisuUiElementCombobox objects.
 *
 * Since: 3.6
 */
#define VISU_UI_TYPE_ELEMENT_COMBOBOX         (visu_ui_element_combobox_get_type ())
/**
 * VISU_UI_ELEMENT_COMBOBOX:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiElementCombobox object.
 *
 * Since: 3.6
 */
#define VISU_UI_ELEMENT_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_ELEMENT_COMBOBOX, VisuUiElementCombobox))
/**
 * VISU_UI_ELEMENT_COMBOBOX_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiElementCombobox object.
 *
 * Since: 3.6
 */
#define VISU_UI_ELEMENT_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_ELEMENT_COMBOBOX, VisuUiElementComboboxClass))
/**
 * VISU_UI_IS_ELEMENT_COMBOBOX:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiElementCombobox object.
 *
 * Since: 3.6
 */
#define VISU_UI_IS_ELEMENT_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_ELEMENT_COMBOBOX))
/**
 * VISU_UI_IS_ELEMENT_COMBOBOX_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiElementComboboxClass class.
 *
 * Since: 3.6
 */
#define VISU_UI_IS_ELEMENT_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_ELEMENT_COMBOBOX))
/**
 * VISU_UI_ELEMENT_COMBOBOX_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 *
 * Since: 3.6
 */
#define VISU_UI_ELEMENT_COMBOBOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_ELEMENT_COMBOBOX, VisuUiElementComboboxClass))

typedef struct _VisuUiElementCombobox VisuUiElementCombobox;
typedef struct _VisuUiElementComboboxClass VisuUiElementComboboxClass;

GType      visu_ui_element_combobox_get_type(void);
GtkWidget* visu_ui_element_combobox_new(gboolean hasAllSelector,
                                        gboolean hasNoneSelector,
                                        const gchar *format);
GList*     visu_ui_element_combobox_getSelection(VisuUiElementCombobox *wd);
gboolean   visu_ui_element_combobox_setSelection(VisuUiElementCombobox* wd,
                                                 const gchar *name);
void       visu_ui_element_combobox_setUnphysicalStatus(VisuUiElementCombobox* wd,
                                                        gboolean status);

G_END_DECLS

#endif
