/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <gtk/gtk.h>

#include <support.h>
#include "gtk_lineObjectWidget.h"
#include "gtk_stippleComboBoxWidget.h"
#include "gtk_colorComboBoxWidget.h"
#include <visu_tools.h>

/**
 * SECTION:gtk_lineObjectWidget
 * @short_description: Defines a specialised #GtkBox to choose
 * all characteristic of lines.
 * @see_also: #VisuUiColorCombobox and #VisuUiStippleCombobox
 *
 * <para></para>
 *
 * Since: 3.4
 */

enum {
  LINE_USE_CHANGED_SIGNAL,
  LINE_WIDTH_CHANGED_SIGNAL,
  LINE_STIPPLE_CHANGED_SIGNAL,
  LINE_COLOR_CHANGED_SIGNAL,
  LAST_SIGNAL
};

static void visu_ui_line_dispose (GObject *obj);
static void visu_ui_line_finalize(GObject *obj);

static guint visu_ui_line_signals[LAST_SIGNAL] = { 0 };

/**
 * VisuUiLine:
 *
 * Private structure to store informations of a #VisuUiLine object.
 *
 * Since: 3.4
 */
struct _VisuUiLine
{
  GtkVBox box;

  GtkWidget *expand;
  GtkWidget *label;
  GtkWidget *used;
  GtkWidget *width;
  GtkWidget *stipple;

  GtkWidget *rgRGB[3];
  GtkWidget *color;
  GtkWidget *btColor;

  gulong rgSignals[3];
  gulong colorSignal;

  /* Memory gestion. */
  gboolean dispose_has_run;
};
/**
 * VisuUiLineClass
 *
 * Private structure to store informations of a #VisuUiLineClass object.
 *
 * Since: 3.4
 */
struct _VisuUiLineClass
{
  GtkComboBoxClass parent_class;

  void (*lineObject) (VisuUiLine *box);
};

/* Local callbacks. */
static void onUseChanged(GtkCheckButton *check, VisuUiLine *line);
static void onWidthChanged(GtkSpinButton *spin, VisuUiLine *line);
static void onStippleChanged(VisuUiStippleCombobox *wd, guint stipple, VisuUiLine *line);
static void onRGBValueChanged(GtkRange *rg, VisuUiLine *line);
static void onAddClicked(GtkButton *bt, VisuUiLine *line);
static void onColorChanged(VisuUiColorCombobox *combo, ToolColor *color, VisuUiLine *line);

/* Local methods. */
/**
 * visu_ui_line_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiLine objects.
 *
 * Since: 3.4
 */
G_DEFINE_TYPE(VisuUiLine, visu_ui_line, GTK_TYPE_VBOX)

static void visu_ui_line_class_init(VisuUiLineClass *klass)
{
  DBG_fprintf(stderr, "Gtk VisuUiLine: creating the class of the widget.\n");
  DBG_fprintf(stderr, "                     - adding new signals ;\n");
  /**
   * VisuUiLine::use-changed:
   * @line: the #VisuUiLine that emits the signal ;
   * @used: TRUE if the line is used.
   *
   * This signal is emitted when the usage check box is changed.
   *
   * Since: 3.4
   */
  visu_ui_line_signals[LINE_USE_CHANGED_SIGNAL] =
    g_signal_new ("use-changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiLineClass, lineObject),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__BOOLEAN,
		  G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
  /**
   * VisuUiLine::width-changed:
   * @line: the #VisuUiLine that emits the signal ;
   * @width: the new width.
   *
   * This signal is emitted when the width of the line is changed.
   *
   * Since: 3.4
   */
  visu_ui_line_signals[LINE_WIDTH_CHANGED_SIGNAL] =
    g_signal_new ("width-changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiLineClass, lineObject),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__INT,
		  G_TYPE_NONE, 1, G_TYPE_INT);
  /**
   * VisuUiLine::stipple-changed:
   * @line: the #VisuUiLine that emits the signal ;
   * @stipple: the new stipple pattern.
   *
   * This signal is emitted when the stipple pattern of the line is changed.
   *
   * Since: 3.4
   */
  visu_ui_line_signals[LINE_STIPPLE_CHANGED_SIGNAL] =
    g_signal_new ("stipple-changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiLineClass, lineObject),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__UINT,
		  G_TYPE_NONE, 1, G_TYPE_UINT);
  /**
   * VisuUiLine::color-changed:
   * @line: the #VisuUiLine that emits the signal ;
   * @color: the new color values (three RGB values).
   *
   * This signal is emitted when the colour of the line is changed.
   *
   * Since: 3.4
   */
  visu_ui_line_signals[LINE_COLOR_CHANGED_SIGNAL] =
    g_signal_new ("color-changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  G_STRUCT_OFFSET (VisuUiLineClass, lineObject),
		  NULL, 
		  NULL,                
		  g_cclosure_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);
  
  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_line_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_line_finalize;
}

static void visu_ui_line_dispose(GObject *obj)
{
  DBG_fprintf(stderr, "Gtk VisuUiLine: dispose object %p.\n", (gpointer)obj);

  if (VISU_UI_LINE(obj)->dispose_has_run)
    return;

  VISU_UI_LINE(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_line_parent_class)->dispose(obj);
}
static void visu_ui_line_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Gtk VisuUiLine: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_line_parent_class)->finalize(obj);

  DBG_fprintf(stderr, " | freeing ... OK.\n");
}


static void visu_ui_line_init(VisuUiLine *line)
{
  GtkWidget *vbox, *hbox, *label, *align, *table;
  char *rgb[3];
  char *rgbName[3] = {"scroll_r", "scroll_g", "scroll_b"};
  int i;

  DBG_fprintf(stderr, "Gtk VisuUiLine: initializing new object (%p).\n",
	      (gpointer)line);

  gtk_box_set_spacing(GTK_BOX(line), 5);

  /********************/
  /* The header line. */
  /********************/
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(line), hbox, FALSE, FALSE, 0);

  /* The drawn checkbox. */
  line->used = gtk_check_button_new();
  align = gtk_alignment_new(.5, 0., 0, 0);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(align), line->used);
  g_signal_connect(G_OBJECT(line->used), "toggled",
                   G_CALLBACK(onUseChanged), (gpointer)line);

  /* The expander. */
  line->expand = gtk_expander_new("");
  gtk_box_pack_start(GTK_BOX(hbox), line->expand, TRUE, TRUE, 0);

  /* The label. */
  hbox = gtk_hbox_new(FALSE, 30);
  line->label = gtk_label_new("");
  gtk_misc_set_alignment(GTK_MISC(line->label), 0., 0.5);
  gtk_widget_set_name(line->label, "label_head");
  gtk_box_pack_start(GTK_BOX(hbox), line->label, TRUE, FALSE, 0);
  label = gtk_label_new(_("<span size=\"small\"><i>"
			  "expand for options</i></span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_expander_set_label_widget(GTK_EXPANDER(line->expand), hbox);

  /* The insider. */
  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(line->expand), vbox);
  hbox = gtk_hbox_new(FALSE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  /*******************/
  /* The scale line. */
  /*******************/

  /* The label. */
  label = gtk_label_new(_("Line style:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

  /* The scale width spin button. */
  align = gtk_alignment_new(1., 0.5, 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 5, 0);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);
  label = gtk_label_new(_("width:"));
  gtk_container_add(GTK_CONTAINER(align), label);
  line->width = gtk_spin_button_new_with_range(1., 10., 1.);
  gtk_entry_set_width_chars(GTK_ENTRY(line->width), 2);
  gtk_box_pack_start(GTK_BOX(hbox), line->width, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(line->width), "value-changed",
		   G_CALLBACK(onWidthChanged), (gpointer)line);
  /* px for pixel. */
  label = gtk_label_new(_("px"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  /* The stipple pattern. */
  align = gtk_alignment_new(1., 0.5, 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 0, 5, 0);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 3);
  line->stipple = visu_ui_stipple_combobox_new();
  gtk_container_add(GTK_CONTAINER(align), line->stipple);
  g_signal_connect(G_OBJECT(line->stipple), "stipple-selected",
		   G_CALLBACK(onStippleChanged), (gpointer)line);

  /********************/
  /* The colour line. */
  /********************/
  hbox = gtk_hbox_new(FALSE, 10);
  
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  /* The table for the box colour. */
  table = gtk_table_new(3, 2, FALSE);
  gtk_box_pack_end(GTK_BOX(hbox), table, TRUE, TRUE, 0);
  rgb[0] = _("R:");
  rgb[1] = _("G:");
  rgb[2] = _("B:");
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(rgb[i]);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, i, i + 1,
		       GTK_SHRINK, GTK_SHRINK, 0, 0);
      line->rgRGB[i] = gtk_hscale_new_with_range(0., 1., 0.001);
      gtk_scale_set_value_pos(GTK_SCALE(line->rgRGB[i]),
			      GTK_POS_RIGHT);
      gtk_range_set_value(GTK_RANGE(line->rgRGB[i]), 0.741456963);
      gtk_widget_set_name(line->rgRGB[i], rgbName[i]);
      gtk_table_attach(GTK_TABLE(table), line->rgRGB[i],
		       1, 2, i, i + 1,
		       GTK_EXPAND | GTK_FILL, GTK_SHRINK, 3, 0);
      line->rgSignals[i] = g_signal_connect(G_OBJECT(line->rgRGB[i]),
					    "value-changed",
					    G_CALLBACK(onRGBValueChanged),
					    (gpointer)line);
    }
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("color:"));
  line->color = visu_ui_color_combobox_new(FALSE);
  line->colorSignal = g_signal_connect(G_OBJECT(line->color), "color-selected",
				       G_CALLBACK(onColorChanged), (gpointer)line);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  visu_ui_color_combobox_setPrintValues(VISU_UI_COLOR_COMBOBOX(line->color), FALSE);
  gtk_box_pack_start(GTK_BOX(hbox), line->color, FALSE, FALSE, 0);
  align = gtk_alignment_new(0.5, 0.5, 0, 1);
  gtk_box_pack_end(GTK_BOX(hbox), align, FALSE, FALSE, 0);
  line->btColor = gtk_button_new();
  gtk_container_add(GTK_CONTAINER(align), line->btColor);
  gtk_container_add(GTK_CONTAINER(line->btColor),
		    gtk_image_new_from_stock (GTK_STOCK_GO_BACK, GTK_ICON_SIZE_MENU));
  label = gtk_label_new(_("<span size=\"small\"><i>(preset)</i></span>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(line->btColor), "clicked",
		   G_CALLBACK(onAddClicked), (gpointer)line);

  gtk_widget_show_all(GTK_WIDGET(line));
}

/**
 * visu_ui_line_new :
 * @label: the name of the group, output in bold.
 *
 * A #VisuUiLine widget is a widget allowing to choose the
 * properties of a line. These properties are the line stipple
 * pattern, its colour and its width. The colour is available through
 * #GtkRange and with a #VisuUiColorCombobox widget. There is also a
 * checkbox allowing to turn the line on or off.
 *
 * Returns: (transfer full): a newly created #VisuUiLine widget.
 *
 * Since: 3.4
 */
GtkWidget* visu_ui_line_new(const gchar* label)
{
  VisuUiLine *line;
  gchar *markup;

  DBG_fprintf(stderr, "Gtk VisuUiLine: creating new object.\n");

  line = VISU_UI_LINE(g_object_new(visu_ui_line_get_type(), NULL));

  DBG_fprintf(stderr, "Gtk VisuUiLine: build widgets.\n");
  markup = g_markup_printf_escaped("<b>%s</b>", label);
  gtk_label_set_markup(GTK_LABEL(line->label), markup);
  g_free(markup);

  return GTK_WIDGET(line);
}


static void onUseChanged(GtkCheckButton *check, VisuUiLine *line)
{
  g_signal_emit(G_OBJECT(line), visu_ui_line_signals[LINE_USE_CHANGED_SIGNAL],
		0, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check)), NULL);
}
static void onWidthChanged(GtkSpinButton *spin, VisuUiLine *line)
{
  g_signal_emit(G_OBJECT(line), visu_ui_line_signals[LINE_WIDTH_CHANGED_SIGNAL],
		0, (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spin)), NULL);
}
static void onStippleChanged(VisuUiStippleCombobox *wd _U_, guint stipple, VisuUiLine *line)
{
  g_signal_emit(G_OBJECT(line), visu_ui_line_signals[LINE_STIPPLE_CHANGED_SIGNAL],
		0, stipple, NULL);
}
static void onRGBValueChanged(GtkRange *rg _U_, VisuUiLine *line)
{
  float rgba[4];
  int i;

  g_return_if_fail(VISU_UI_IS_LINE(line));

  rgba[0] = gtk_range_get_value(GTK_RANGE(line->rgRGB[0]));
  rgba[1] = gtk_range_get_value(GTK_RANGE(line->rgRGB[1]));
  rgba[2] = gtk_range_get_value(GTK_RANGE(line->rgRGB[2]));
  rgba[3] = 1.f;
  tool_color_getByValues(&i, rgba[0], rgba[1], rgba[2], rgba[3]);

  g_signal_handler_block(G_OBJECT(line->color), line->colorSignal);
  if (i < 0)
    gtk_combo_box_set_active(GTK_COMBO_BOX(line->color), -1);
  else
    gtk_combo_box_set_active(GTK_COMBO_BOX(line->color), i + 1);
  g_signal_handler_unblock(G_OBJECT(line->color), line->colorSignal);

  gtk_widget_set_sensitive(line->btColor, (i < 0));

  g_signal_emit(G_OBJECT(line), visu_ui_line_signals[LINE_COLOR_CHANGED_SIGNAL],
		0, (gpointer)rgba, NULL);
}
static void onAddClicked(GtkButton *bt _U_, VisuUiLine *line)
{
  float rgba[4];
  int selected;

  g_return_if_fail(VISU_UI_IS_LINE(line));

  DBG_fprintf(stderr, "Gtk VisuUiLine: adding a new color from ranges.\n");

  rgba[0] = gtk_range_get_value(GTK_RANGE(line->rgRGB[0]));
  rgba[1] = gtk_range_get_value(GTK_RANGE(line->rgRGB[1]));
  rgba[2] = gtk_range_get_value(GTK_RANGE(line->rgRGB[2]));
  rgba[3] = 1.f;
  tool_color_addFloatRGBA(rgba, &selected);
  g_signal_handler_block(G_OBJECT(line->color), line->colorSignal);
  gtk_combo_box_set_active(GTK_COMBO_BOX(line->color), selected);
  g_signal_handler_unblock(G_OBJECT(line->color), line->colorSignal);
  gtk_widget_set_sensitive(line->btColor, FALSE);
}
static void onColorChanged(VisuUiColorCombobox *combo _U_, ToolColor *color, VisuUiLine *line)
{
  visu_ui_line_setColor(line, color->rgba);
}

/**
 * visu_ui_line_setUsed:
 * @line: the object to modify ;
 * @status: a boolean.
 *
 * The line can be turn on or off, call this routine to change the
 * interface status.
 *
 * Since: 3.4
 */
void visu_ui_line_setUsed(VisuUiLine *line, gboolean status)
{
  g_return_if_fail(VISU_UI_IS_LINE(line));

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(line->used), status);
}
/**
 * visu_ui_line_setWidth:
 * @line: the object to modify ;
 * @width: a value.
 *
 * The line can be drawn with a given width, call this routine to change the
 * interface value.
 *
 * Since: 3.4
 */
void visu_ui_line_setWidth(VisuUiLine *line, gint width)
{
  g_return_if_fail(VISU_UI_IS_LINE(line) && width > 0 && width < 11);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(line->width), (gdouble)width);
}
/**
 * visu_ui_line_setColor:
 * @line: the object to modify ;
 * @rgb: a RGB array.
 *
 * The line can is drawn in a given colour. Change the interface
 * values using this routine. The colour ranges are updated and if it
 * correspond to a registered colour, it is selected in the combobox.
 *
 * Since: 3.4
 */
void visu_ui_line_setColor(VisuUiLine *line, float rgb[3])
{
  gboolean change;

  g_return_if_fail(VISU_UI_IS_LINE(line));

  g_signal_handler_block(G_OBJECT(line->rgRGB[0]), line->rgSignals[0]);
  g_signal_handler_block(G_OBJECT(line->rgRGB[1]), line->rgSignals[1]);
  g_signal_handler_block(G_OBJECT(line->rgRGB[2]), line->rgSignals[2]);

  change = FALSE;
  if ((float)gtk_range_get_value(GTK_RANGE(line->rgRGB[0])) != rgb[0])
    {
      change = TRUE;
      gtk_range_set_value(GTK_RANGE(line->rgRGB[0]), rgb[0]);
    }
  if ((float)gtk_range_get_value(GTK_RANGE(line->rgRGB[1])) != rgb[1])
    {
      change = TRUE;
      gtk_range_set_value(GTK_RANGE(line->rgRGB[1]), rgb[1]);
    }
  if ((float)gtk_range_get_value(GTK_RANGE(line->rgRGB[2])) != rgb[2])
    {
      change = TRUE;
      gtk_range_set_value(GTK_RANGE(line->rgRGB[2]), rgb[2]);
    }

  g_signal_handler_unblock(G_OBJECT(line->rgRGB[0]), line->rgSignals[0]);
  g_signal_handler_unblock(G_OBJECT(line->rgRGB[1]), line->rgSignals[1]);
  g_signal_handler_unblock(G_OBJECT(line->rgRGB[2]), line->rgSignals[2]);

  if (change)
    onRGBValueChanged((GtkRange*)0, line);
}
/**
 * visu_ui_line_setStipple:
 * @line: the object to modify ;
 * @stipple: a value.
 *
 * The line can be drawn with a given stipple pattern, call this
 * routine to change the interface value.
 *
 * Since: 3.4
 */
void visu_ui_line_setStipple(VisuUiLine *line, guint16 stipple)
{
  g_return_if_fail(VISU_UI_IS_LINE(line));

  if (!visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(line->stipple), stipple))
    {
      visu_ui_stipple_combobox_add(VISU_UI_STIPPLE_COMBOBOX(line->stipple), stipple);
      visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(line->stipple), stipple);
    }
}
/**
 * visu_ui_line_getOptionBox:
 * @line:  the object to get the GtkVBox.
 *
 * Give access to the #GtkVBox of the expander.
 *
 * Since: 3.6
 *
 * Returns: (transfer none): a #GtkWidget.
 */
GtkWidget* visu_ui_line_getOptionBox(VisuUiLine *line)
{
  g_return_val_if_fail(VISU_UI_IS_LINE(line), (GtkWidget*)0);

  return gtk_bin_get_child(GTK_BIN(line->expand));
}
