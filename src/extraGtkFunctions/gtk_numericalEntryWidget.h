/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_NUMERICALENTRYWIDGET_H
#define GTK_NUMERICALENTRYWIDGET_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

#include <visu_tools.h>

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_NUMERICAL_ENTRY:
 *
 * Return the associated #GType to the VisuUiNumericalEntry objects.
 */
#define VISU_UI_TYPE_NUMERICAL_ENTRY         (visu_ui_numerical_entry_get_type ())
/**
 * VISU_UI_NUMERICAL_ENTRY:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiNumericalEntry object.
 */
#define VISU_UI_NUMERICAL_ENTRY(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_NUMERICAL_ENTRY, VisuUiNumericalEntry))
/**
 * VISU_UI_NUMERICAL_ENTRY_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiNumericalEntryClass object.
 */
#define VISU_UI_NUMERICAL_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_NUMERICAL_ENTRY, VisuUiNumericalEntryClass))
/**
 * VISU_UI_IS_NUMERICAL_ENTRY:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiNumericalEntry object.
 */
#define VISU_UI_IS_NUMERICAL_ENTRY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_NUMERICAL_ENTRY))
/**
 * VISU_UI_IS_NUMERICAL_ENTRY_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiNumericalEntryClass class.
 */
#define VISU_UI_IS_NUMERICAL_ENTRY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_NUMERICAL_ENTRY))

/**
 * VisuUiNumericalEntry
 *
 * Short form for a #VisuUiNumericalEntry_struct structure.
 */
typedef struct _VisuUiNumericalEntry        VisuUiNumericalEntry;
/* typedef struct VisuUiNumericalEntryPrivate_struct VisuUiNumericalEntryPrivate; */
/**
 * VisuUiNumericalEntryClass
 *
 * Opaque structure.
 */
typedef struct _VisuUiNumericalEntryClass   VisuUiNumericalEntryClass;

/**
 * visu_ui_numerical_entry_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiNumericalEntry objects.
 */
GType visu_ui_numerical_entry_get_type(void);

GtkWidget* visu_ui_numerical_entry_new(double value);
void visu_ui_numerical_entry_setValue(VisuUiNumericalEntry* numericalEntry, double value);
double visu_ui_numerical_entry_getValue(VisuUiNumericalEntry *numericalEntry);


G_END_DECLS

#endif
