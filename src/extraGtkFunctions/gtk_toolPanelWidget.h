/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GTK_TOOLPANELWIDGET_H
#define GTK_TOOLPANELWIDGET_H

#include <glib.h>
#include <gtk/gtk.h>

#include <openGLFunctions/view.h>
#include <visu_data.h>

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_PANEL:
 *
 * Return the associated #GType to the VisuUiPanel objects.
 */
#define VISU_UI_TYPE_PANEL         (visu_ui_panel_get_type ())
/**
 * VISU_UI_PANEL:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiPanel object.
 */
#define VISU_UI_PANEL(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_UI_TYPE_PANEL, VisuUiPanel))
/**
 * VISU_UI_PANEL_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiPanelClass object.
 */
#define VISU_UI_PANEL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_UI_TYPE_PANEL, VisuUiPanelClass))
/**
 * VISU_UI_IS_PANEL:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiPanel object.
 */
#define VISU_UI_IS_PANEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_UI_TYPE_PANEL))
/**
 * VISU_UI_IS_PANEL_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiPanelClass class.
 */
#define VISU_UI_IS_PANEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_UI_TYPE_PANEL))

typedef struct _VisuUiPanel VisuUiPanel;
typedef struct _VisuUiPanelClass VisuUiPanelClass;

GType visu_ui_panel_get_type(void);

typedef struct _VisuUiDockWindow VisuUiDockWindow;

GType visu_ui_dock_window_get_type(void);
/**
 * VISU_UI_TYPE_DOCK_WINDOW:
 *
 * The type of #VisuUiDockWindow objects.
 */
#define VISU_UI_TYPE_DOCK_WINDOW (visu_ui_dock_window_get_type())

GtkWidget* visu_ui_panel_new(gchar *id, gchar* name, gchar *tabName);
GtkWidget* visu_ui_panel_newWithIconFromPath(gchar *id, gchar* name, gchar *tabName,
                                             const gchar* iconPath);
GtkWidget* visu_ui_panel_newWithIconFromStock(gchar *id, gchar* name, gchar *tabName,
                                              const gchar* stock);

GtkWidget*        visu_ui_panel_getHeaderWidget   (VisuUiPanel *visu_ui_panel);
const gchar*      visu_ui_panel_getLabel          (VisuUiPanel *visu_ui_panel);
const gchar*      visu_ui_panel_getId             (VisuUiPanel *visu_ui_panel);
GtkWindow*        visu_ui_panel_getContainerWindow(VisuUiPanel *visu_ui_panel);
VisuUiDockWindow* visu_ui_panel_getContainer      (VisuUiPanel *visu_ui_panel);
const gchar*      visu_ui_panel_getContainerId    (VisuUiPanel *visu_ui_panel);
VisuData*         visu_ui_panel_getData           (VisuUiPanel *visu_ui_panel);
VisuBoxed*        visu_ui_panel_getFocused        (VisuUiPanel *visu_ui_panel);
VisuGlView*       visu_ui_panel_getView           (VisuUiPanel *visu_ui_panel);
gboolean          visu_ui_panel_getVisible        (VisuUiPanel *visu_ui_panel);

void visu_ui_panel_setDockable   (VisuUiPanel *visu_ui_panel, gboolean value);
void visu_ui_panel_setContainer  (VisuUiPanel *visu_ui_panel, VisuUiDockWindow *window);
void visu_ui_panel_setContainerId(VisuUiPanel *visu_ui_panel, const gchar *id);

void visu_ui_panel_attach(VisuUiPanel *visu_ui_panel, VisuUiDockWindow *dock);
void visu_ui_panel_detach(VisuUiPanel *visu_ui_panel);

/* Associated variables and methods. */
GtkWidget* visu_ui_dock_window_getContainer(VisuUiDockWindow *dock);
GtkWidget* visu_ui_dock_window_getNotebook(VisuUiDockWindow *dock);
GtkWidget* visu_ui_dock_window_getWindow(VisuUiDockWindow *dock);
void       visu_ui_dock_window_getCharacteristics(VisuUiDockWindow *dock,
                                                  gchar **id, gboolean *visibility, 
                                                  gint *x, gint *y, 
                                                  gint *width, gint *height);
void       visu_ui_dock_window_setSize(VisuUiDockWindow *dock,
                                             guint width, guint height);
void       visu_ui_dock_window_setPosition(VisuUiDockWindow *dock,
                                                 guint x, guint y);
void       visu_ui_dock_window_setVisibility(VisuUiDockWindow *dock,
                                                   gboolean visible);

/* Class methods. */
void              visu_ui_panel_class_setCurrent(VisuData *dataObj, VisuGlView *view);
void              visu_ui_panel_class_setHeaderVisibility(gboolean status);
VisuUiDockWindow* visu_ui_panel_class_getCommandPanel(void);
VisuUiDockWindow* visu_ui_panel_class_getDockById(const gchar *id);
GList*            visu_ui_panel_class_getAllPanels(void);
GList*            visu_ui_panel_class_getAllWindows(void);
VisuUiPanel*      visu_ui_panel_class_getPanelById(const gchar *id);

G_END_DECLS

#endif
