/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "math.h"

#include "support.h"
#include "interface.h"
#include "gtk_main.h"
#include "visu_object.h"

#include "gtk_move.h"
#include "gtk_pick.h"
#include "gtk_interactive.h"
#include "gtk_renderingWindowWidget.h"
#include "openGLFunctions/interactive.h"
#include "extensions/marks.h"
#include "extraFunctions/plane.h"
#include "extraGtkFunctions/gtk_numericalEntryWidget.h"
#include "extraGtkFunctions/gtk_elementComboBox.h"
#include "coreTools/toolMatrix.h"
#include "extensions/box.h"

/**
 * SECTION: gtk_move
 * @short_description: The move tab in the interactive dialog.
 *
 * <para>This action tab provides widgets to move single or group of
 * atoms. It provides also widgets to remove or add atoms. And finally
 * it provides a way to change the basis-set by picking atoms to form
 * vertices of a new basis-set.</para>
 */

/* Callbacks. */
static void onDataReady(GObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer bool);
static void onDataNotReady(GObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer bool);
static void refreshMoveAxesValues(VisuGlView *view, gpointer data);
static void onMovePositionChanged(VisuUiNumericalEntry *entry,
				  double oldValue, gpointer data);
static void onAxeChoosen(VisuUiNumericalEntry *entry, double oldValue, gpointer data);
static void onMoveToOriginalClicked(GtkButton *button, gpointer data);
static void onRemoveNodeClicked(GtkButton *button, gpointer user_data);
static void onAddNodeClicked(GtkButton *button, gpointer user_data);
static void onMoveMethod(GtkToggleButton *toggle, gpointer data);
static void onPageEnter(GtkNotebook *notebook, GtkWidget *child,
			gint page_num, gpointer user_data);
static void onCheckDuplicate(GtkToggleButton *button, gpointer data);
static void onDuplicate(GtkButton *button, gpointer data);
static void onMoveToggled(GtkToggleButton *toggle, gpointer data);
static void onSpinBasis(GtkSpinButton *spin, gpointer data);
static gboolean removeHighlight(gpointer data);
static void onChooseBasis(GtkToggleButton *toggle, gpointer data);
static void onBasisSelected(VisuInteractive *inter, VisuInteractivePick pick,
			    VisuNode *nodes0, VisuNode *node1, VisuNode *node2,
                            gpointer data);
static void onApplyBasis(GtkButton *button, gpointer data);
static void onPopulationChanged(VisuData *dataObj, gint *nodes, gpointer data);
static void onPositionChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void onStartMove(VisuInteractive *inter, GList* nodeIds, gpointer data);
static void onMove(VisuInteractive *inter, float drag[3], gpointer data);
static void onGetAxisClicked(GtkButton *button, gpointer data);
static void onMoveClickStop(VisuInteractive *inter, gpointer data);
static void onPickClickStop(VisuInteractive *inter, gpointer data);

/* Local methods. */
static void setLabelsOrigin(VisuData *data, GList *nodeIds);
static void drawBasisCell(VisuBox *box, float O[3], float mat[3][3]);

#define GTK_MOVE_INFO				\
  _("left-button\t\t\t\t: drag node(s) in the screen plane\n"	\
    "middle-button (wheel)\t\t: drag node(s) along specific axis\n"	\
    "shift-left-button\t\t\t: drag node(s) along x axis\n"	\
    "control-left-button\t\t\t: drag node(s) along y axis\n"	\
    "control-shift-left-button\t: drag node(s) along z axis\n"	\
    "right-button\t\t\t\t: switch to observe")

/* Pick informations hook */
static VisuInteractive *interPick, *interMove;
static int movedNode = -1;
static float moveNodeOrigin[3];
#define GTK_MOVE_NO_NODE         _("(<i>none</i>)")
static gulong onSpin_id[4];
static VisuInteractiveId currentMode = interactive_move;
static guint currentAxe;
static gulong angles_signal, popInc_signal, popDec_signal, popChg_signal;

/* Widgets */
static GtkWidget *notebookAction;
static GtkWidget *observeWindow;
static GtkWidget *entryMoveXYZ[3];
static GtkWidget *entryAddXYZ[3];
static GtkWidget *entryAxeXYZ[3];
static GtkWidget *removeButton, *cancelButton;
static GtkWidget *comboElements;
static GtkWidget *labelNMoves;
static GtkWidget *radioMovePick, *radioMoveRegion;
enum
  {
    COLUMN_NAME,             /* The label shown */
    COLUMN_POINTER_TO_DATA,  /* Pointer to the VisuElement. */
    N_COLUMNS
  };
static GtkWidget *checkDuplicate, *comboDuplicate, *buttonDupplicate;
static GtkWidget *labelOriginX, *labelOriginY, *labelOriginZ;
static GtkWidget *labelScreenHorizontal, *labelScreenVertical;
static GtkWidget *spinABC[4], *toggleABC[4], *applyBasis;
static gchar *lblSpinABC[4] = {"orig.:", "X:", "Y:", "Z:"};
static gint prevBasis[4] = {0, 0, 0, 0};
static guint timeoutBasis[4];
static VisuGlExtBox *extBasis;

/********************/
/* Public routines. */
/********************/
/**
 * visu_ui_interactive_move_initBuild: (skip)
 * @main: the main interface.
 * @label: a location to store the name of the move tab ;
 * @help: a location to store the help message to be shown at the
 * bottom of the window ;
 * @radio: a location on the radio button that will be toggled when
 * the move action is used.
 * 
 * This routine should be called in conjonction to the
 * visu_ui_interactive_pick_initBuild() one. It completes the creation of widgets
 * (and also initialisation of values) for the move tab.
 */
GtkWidget* visu_ui_interactive_move_initBuild(VisuUiMain *main, gchar **label,
                                              gchar **help, GtkWidget **radio)
{
  GtkWidget *wd, *hbox, *vbox, *bt;
  VisuData *data;
  VisuGlView *view;
  VisuNodeArrayIter dataIter;
  guint i, idmax;
  VisuUiRenderingWindow *window;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  extBasis = visu_gl_ext_box_new("New basis set highlight");
  visu_gl_ext_box_setLineWidth(extBasis, 1.f);
  visu_gl_ext_box_setLineStipple(extBasis, 57568);
  visu_gl_ext_box_setExpandStipple(extBasis, 57568);
  visu_gl_ext_setActive(VISU_GL_EXT(extBasis), FALSE);

  *label = g_strdup("Pick");
  *help  = g_strdup(GTK_MOVE_INFO);
  *radio = lookup_widget(main->interactiveDialog, "radioMove");
  g_signal_connect(G_OBJECT(*radio), "toggled",
		   G_CALLBACK(onMoveToggled), (gpointer)main->interactiveDialog);

  window = visu_ui_main_class_getDefaultRendering();
  view = visu_ui_rendering_window_getGlView(window);
  data = visu_ui_rendering_window_getData(window);
  if (data)
    {
      visu_node_array_iterNew(VISU_NODE_ARRAY(data), &dataIter);
      idmax = dataIter.idMax + 1;
    }
  else
    idmax = 1;

  /* We create here the two interactives. */
  interMove = visu_interactive_new(interactive_move);
  g_object_ref(G_OBJECT(interMove));
  g_signal_connect(G_OBJECT(interMove), "move",
		   G_CALLBACK(onMove), (gpointer)0);
  g_signal_connect(G_OBJECT(interMove), "start-move",
		   G_CALLBACK(onStartMove), (gpointer)window);
  g_signal_connect(G_OBJECT(interMove), "stop",
		   G_CALLBACK(onMoveClickStop), (gpointer)0);
  interPick = visu_interactive_new(interactive_pick);
  g_object_ref(G_OBJECT(interPick));
  g_signal_connect(G_OBJECT(interPick), "node-selection",
		   G_CALLBACK(onBasisSelected), (gpointer)0);
  g_signal_connect(G_OBJECT(interPick), "stop",
		   G_CALLBACK(onPickClickStop), (gpointer)0);

  observeWindow = main->interactiveDialog;
  
  vbox = lookup_widget(main->interactiveDialog, "vbox21");

  notebookAction = lookup_widget(main->interactiveDialog, "notebookAction");
  g_signal_connect(G_OBJECT(notebookAction), "switch-page",
		   G_CALLBACK(onPageEnter), (gpointer)0);

  /* The move node action. */
  removeButton = gtk_button_new();
  gtk_box_pack_end(GTK_BOX(lookup_widget(main->interactiveDialog, "hbox72")),
                   removeButton, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(removeButton,
                              _("Suppress node (either picked one or selected ones)."));
  gtk_container_add(GTK_CONTAINER(removeButton),
                    gtk_image_new_from_stock(GTK_STOCK_REMOVE, GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(removeButton), "clicked",
		   G_CALLBACK(onRemoveNodeClicked), (gpointer)0);
  gtk_widget_show_all(removeButton);

  labelNMoves = lookup_widget(main->interactiveDialog, "labelNMoves");
  gtk_label_set_markup(GTK_LABEL(labelNMoves), GTK_MOVE_NO_NODE);

  radioMovePick = lookup_widget(main->interactiveDialog, "radioMovePick");
  gtk_widget_set_name(radioMovePick, "message_radio");
  radioMoveRegion = lookup_widget(main->interactiveDialog, "radioMoveRegion");
  gtk_widget_set_name(radioMoveRegion, "message_radio");

  labelOriginX = lookup_widget(main->interactiveDialog, "labelOriginalX");
  labelOriginY = lookup_widget(main->interactiveDialog, "labelOriginalY");
  labelOriginZ = lookup_widget(main->interactiveDialog, "labelOriginalZ");

  labelScreenHorizontal = lookup_widget(main->interactiveDialog, "labelHorizontalAxe");
  labelScreenVertical   = lookup_widget(main->interactiveDialog, "labelVerticalAxe");

  wd = lookup_widget(main->interactiveDialog, "tableMovePick");
  entryMoveXYZ[0] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryMoveXYZ[0]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryMoveXYZ[0], 1, 2, 2, 3,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  entryMoveXYZ[1] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryMoveXYZ[1]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryMoveXYZ[1], 3, 4, 2, 3,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  entryMoveXYZ[2] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryMoveXYZ[2]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryMoveXYZ[2], 5, 6, 2, 3,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  entryAxeXYZ[0] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAxeXYZ[0]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryAxeXYZ[0], 1, 2, 1, 2,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  entryAxeXYZ[1] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAxeXYZ[1]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryAxeXYZ[1], 3, 4, 1, 2,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  entryAxeXYZ[2] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAxeXYZ[2]), 8);
  gtk_table_attach(GTK_TABLE(wd), entryAxeXYZ[2], 5, 6, 1, 2,
		   GTK_SHRINK, GTK_SHRINK, 0, 0);
  bt = gtk_button_new();
  gtk_table_attach(GTK_TABLE(wd), bt, 7, 8, 1, 2,
                   GTK_FILL, GTK_SHRINK, 0, 0);
  gtk_widget_set_tooltip_text(bt, _("Capture the perpendicular axis to the current view."));
  gtk_container_add(GTK_CONTAINER(bt),
                    gtk_image_new_from_stock(GTK_STOCK_ZOOM_FIT, GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onGetAxisClicked), (gpointer)window);
  cancelButton = gtk_button_new();
  gtk_table_attach(GTK_TABLE(wd), cancelButton, 7, 8, 2, 3,
                   GTK_FILL, GTK_SHRINK, 0, 0);
  gtk_widget_set_tooltip_text(cancelButton, _("Return coordinates to initial values."));
  gtk_container_add(GTK_CONTAINER(cancelButton),
                    gtk_image_new_from_stock(GTK_STOCK_UNDO, GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(cancelButton), "clicked",
		   G_CALLBACK(onMoveToOriginalClicked), (gpointer)0);
  gtk_widget_show_all(wd);

  /* The add line. */
  wd = lookup_widget(main->interactiveDialog, "hboxAddNode");
  entryAddXYZ[0] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAddXYZ[0]), 6);
  gtk_box_pack_start(GTK_BOX(wd), entryAddXYZ[0], FALSE, FALSE, 0);
  entryAddXYZ[1] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAddXYZ[1]), 6);
  gtk_box_pack_start(GTK_BOX(wd), entryAddXYZ[1], FALSE, FALSE, 0);
  entryAddXYZ[2] = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryAddXYZ[2]), 6);
  gtk_box_pack_start(GTK_BOX(wd), entryAddXYZ[2], FALSE, FALSE, 0);
  /* We create the structure that store the VisuElements */
  comboElements = visu_ui_element_combobox_new(FALSE, FALSE, (const gchar*)0);
  gtk_box_pack_start(GTK_BOX(wd), comboElements, FALSE, FALSE, 0);
  gtk_box_reorder_child(GTK_BOX(wd), comboElements, 0);
  bt = gtk_button_new();
  gtk_box_pack_end(GTK_BOX(wd), bt, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(bt, _("Add a new node."));
  gtk_container_add(GTK_CONTAINER(bt),
                    gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onAddNodeClicked), (gpointer)0);
  gtk_widget_show_all(wd);

  /* The duplicate line. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
  wd = gtk_label_new(_("<b>Duplicate nodes:</b>"));
  gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
  gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), wd, TRUE, TRUE, 0);
  wd = gtk_label_new(_("<span size=\"smaller\">(the nodes listed in the pick tab)</span>"));
  gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  gtk_widget_show_all(hbox);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  wd = gtk_check_button_new_with_mnemonic(_("du_plicate nodes as they are"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wd), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  checkDuplicate = wd;
  wd = gtk_label_new(_(" or as new: "));
  gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  wd = visu_ui_element_combobox_new(FALSE, FALSE, (const gchar*)0);
  gtk_widget_set_sensitive(wd, FALSE);
  comboDuplicate = wd;
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  wd = gtk_button_new_with_mnemonic(_("_duplicate"));
  gtk_box_pack_end(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  buttonDupplicate = wd;
  gtk_widget_show_all(hbox);

  /* The Basis line. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
  wd = gtk_label_new(_("<b>Change the basis set:</b>"));
  gtk_label_set_use_markup(GTK_LABEL(wd), TRUE);
  gtk_misc_set_alignment(GTK_MISC(wd), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  gtk_widget_show_all(hbox);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  for (i = 0; i < 4; i++)
    {
      /* if (i == 1) */
      /*   { */
      /*     gtk_widget_show_all(hbox); */
      /*     hbox = gtk_hbox_new(FALSE, 0); */
      /*     gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0); */
      /*   } */
      wd = gtk_label_new(_(lblSpinABC[i]));
      gtk_misc_set_alignment(GTK_MISC(wd), 1., 0.5);
      gtk_box_pack_start(GTK_BOX(hbox), wd, TRUE, TRUE, 5);
      spinABC[i] = gtk_spin_button_new_with_range(0, idmax, 1);
      onSpin_id[i] = g_signal_connect(G_OBJECT(spinABC[i]), "value-changed",
				      G_CALLBACK(onSpinBasis), GINT_TO_POINTER(i));
      gtk_box_pack_start(GTK_BOX(hbox), spinABC[i], FALSE, FALSE, 0);
      toggleABC[i] = gtk_toggle_button_new();
      gtk_button_set_relief(GTK_BUTTON(toggleABC[i]), GTK_RELIEF_NONE);
      gtk_box_pack_start(GTK_BOX(hbox), toggleABC[i], FALSE, FALSE, 0);
      gtk_container_add(GTK_CONTAINER(toggleABC[i]),
			gtk_image_new_from_stock(GTK_STOCK_FIND,
						 GTK_ICON_SIZE_MENU));
      gtk_widget_set_tooltip_text(toggleABC[i],
                                  _("Select node by picking it on the rendering area."));
      g_signal_connect(G_OBJECT(toggleABC[i]), "toggled",
		       G_CALLBACK(onChooseBasis), GINT_TO_POINTER(i));
    }
  applyBasis = gtk_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), applyBasis, FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(applyBasis),
		    gtk_image_new_from_stock(GTK_STOCK_APPLY, GTK_ICON_SIZE_MENU));
  gtk_widget_set_sensitive(applyBasis, FALSE);
  g_signal_connect(G_OBJECT(applyBasis), "clicked",
		   G_CALLBACK(onApplyBasis), (gpointer)0);
  gtk_widget_show_all(hbox);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReady), (gpointer)0);
  if (data && view)
    {
      visu_interactive_apply(interMove, VISU_NODE_ARRAY(data));
      visu_interactive_apply(interPick, VISU_NODE_ARRAY(data));
      onDataReady((GObject*)0, data, view, (gpointer)0);
    }

  for (i = 0; i < 3; i++)
    {
      g_signal_connect(G_OBJECT(entryMoveXYZ[i]), "value-changed",
		       G_CALLBACK(onMovePositionChanged), GINT_TO_POINTER(i));
      g_signal_connect(G_OBJECT(entryAxeXYZ[i]), "value-changed",
		       G_CALLBACK(onAxeChoosen), (gpointer)0);
    }
  wd = lookup_widget(main->interactiveDialog, "radioMovePick");
  g_signal_connect(G_OBJECT(wd), "toggled",
		   G_CALLBACK(onMoveMethod), (gpointer)0);
  wd = lookup_widget(main->interactiveDialog, "radioMoveRegion");
  g_signal_connect(G_OBJECT(wd), "toggled",
		   G_CALLBACK(onMoveMethod), (gpointer)window);

  g_signal_connect(G_OBJECT(checkDuplicate), "toggled",
		   G_CALLBACK(onCheckDuplicate), comboDuplicate);
  g_signal_connect(G_OBJECT(buttonDupplicate), "clicked",
		   G_CALLBACK(onDuplicate), (gpointer)0);

  return (GtkWidget*)0;
}


static void setLabelsOrigin(VisuData *data, GList *nodeIds)
{
  gchar numero[256];
  VisuNode *node;

  if (nodeIds)
    {
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
	{
	  movedNode = GPOINTER_TO_INT(nodeIds->data);
	  node = visu_node_array_getFromId(VISU_NODE_ARRAY(data),
                                           GPOINTER_TO_INT(nodeIds->data));
	  g_return_if_fail(node);

	  sprintf(numero, _("(node %d)"), GPOINTER_TO_INT(nodeIds->data) + 1);
	  gtk_label_set_markup(GTK_LABEL(labelNMoves), numero);
	  /* Set the origin position values. */
	  moveNodeOrigin[0] = node->xyz[0];
	  moveNodeOrigin[1] = node->xyz[1];
	  moveNodeOrigin[2] = node->xyz[2];
	  sprintf(numero, "<span size=\"small\">/ %5.2f</span>", moveNodeOrigin[0]);
	  gtk_label_set_markup(GTK_LABEL(labelOriginX), numero);
	  sprintf(numero, "<span size=\"small\">/ %5.2f</span>", moveNodeOrigin[1]);
	  gtk_label_set_markup(GTK_LABEL(labelOriginY), numero);
	  sprintf(numero, "<span size=\"small\">/ %5.2f</span>", moveNodeOrigin[2]);
	  gtk_label_set_markup(GTK_LABEL(labelOriginZ), numero);
	  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), node->xyz[0]);
	  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), node->xyz[1]);
	  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), node->xyz[2]);
	}
      else
	{
	  movedNode = -1;
	  sprintf(numero, _("(%d nodes)"), g_list_length(nodeIds));
	  gtk_label_set_markup(GTK_LABEL(labelNMoves), numero);
	  gtk_label_set_markup(GTK_LABEL(labelOriginX), "");
	  gtk_label_set_markup(GTK_LABEL(labelOriginY), "");
	  gtk_label_set_markup(GTK_LABEL(labelOriginZ), "");
	}
    }
  else
    {
      movedNode = -1;
      gtk_label_set_markup(GTK_LABEL(labelNMoves), GTK_MOVE_NO_NODE);
      gtk_label_set_markup(GTK_LABEL(labelOriginX), "");
      gtk_label_set_markup(GTK_LABEL(labelOriginY), "");
      gtk_label_set_markup(GTK_LABEL(labelOriginZ), "");
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), 0.);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), 0.);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), 0.);
    }
  /* Set the sensitivity. */
  gtk_widget_set_sensitive(removeButton, (nodeIds != (GList*)0));
  gtk_widget_set_sensitive(cancelButton, (nodeIds != (GList*)0));
}

static void setMovingNodes()
{
  GList *nodeIds;

  DBG_fprintf(stderr, "Gtk Move: set the moving list.\n");

  nodeIds = (GList*)0;
  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
    nodeIds = visu_ui_interactive_pick_getNodeSelection();
  visu_interactive_setMovingNodes(interMove, nodeIds);
  setLabelsOrigin((VisuData*)0, nodeIds);
  if (nodeIds)
    g_list_free(nodeIds);
}
/**
 * visu_ui_interactive_move_start:
 * @window: a #VisuUiRenderingWindow object.
 *
 * Initialise a moving session.
 */
void visu_ui_interactive_move_start(VisuUiRenderingWindow *window)
{
  DBG_fprintf(stderr, "Gtk Move: start the move panel.\n");
  visu_ui_rendering_window_pushInteractive(window,
				  (currentMode == interactive_move)?
				  interMove:interPick);

  if (currentMode == interactive_pick)
    visu_ui_rendering_window_pushMessage(window, _("Pick a node with the mouse"));

  /* Set the moving list. */
  setMovingNodes();
}
/**
 * visu_ui_interactive_move_stop:
 * @window: a #VisuUiRenderingWindow object.
 *
 * Finalise a moving session.
 */
void visu_ui_interactive_move_stop(VisuUiRenderingWindow *window)
{
  DBG_fprintf(stderr, "Gtk Move: unset the move panel.\n");

  visu_ui_rendering_window_popInteractive(window, (currentMode == interactive_move)?
				 interMove:interPick);

  if (currentMode == interactive_pick)
    visu_ui_rendering_window_popMessage(window);
}
static void onMove(VisuInteractive *inter _U_, float drag[3], gpointer data _U_)
{
/*   GList *nodes; */
  float val;
/*   VisuNode *node; */

  DBG_fprintf(stderr, "Gtk Move: callback on move action.\n");

  /* Modify the entry labels. */
  DBG_fprintf(stderr, "Gtk Move: update label of positions.\n");
  val = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]));
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), val + drag[0]);
  val = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]));
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), val + drag[1]);
  val = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]));
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), val + drag[2]);
  return;
}
static void onStartMove(VisuInteractive *inter _U_, GList* nodeIds, gpointer data)
{
  DBG_fprintf(stderr, "Gtk Move: callback on start-move action.\n");
  setLabelsOrigin(visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(data)), nodeIds);
  return;
}
static void onMoveClickStop(VisuInteractive *inter _U_, gpointer data _U_)
{
  visu_ui_interactive_toggle();
}

/****************/
/* Private part */
/****************/

static void onPageEnter(GtkNotebook *notebook _U_, GtkWidget *child _U_,
			gint page_num, gpointer user_data _U_)
{
  gchar numero[256];
  GList *lst;

  if (page_num != 1)
    return;

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
    {
      lst = visu_ui_interactive_pick_getNodeSelection();
      sprintf(numero, _("(%d nodes)"), g_list_length(lst));
      gtk_label_set_markup(GTK_LABEL(labelNMoves), numero);
      g_list_free(lst);
    }
}

static void onDataReady(GObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view, gpointer data _U_)
{
  int i;
  VisuNodeArrayIter dataIter;

  DBG_fprintf(stderr, "Gtk Move: caught 'dataRendered' signal.\n");
  
  if (dataObj)
    {
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &dataIter);
      for (i = 0; i < 4; i++)
        gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinABC[i]), 0,
                                  dataIter.idMax);
    }
  /* We remove the basisset drawing. */
  visu_gl_ext_setActive(VISU_GL_EXT(extBasis), FALSE);

  if (view && dataObj)
    {
      angles_signal =
        g_signal_connect(G_OBJECT(view), "ThetaPhiOmegaChanged",
                         G_CALLBACK(refreshMoveAxesValues), (gpointer)0);
      popDec_signal =
        g_signal_connect(G_OBJECT(dataObj), "PopulationDecrease",
                         G_CALLBACK(onPopulationChanged), (gpointer)0);
      popInc_signal =
        g_signal_connect(G_OBJECT(dataObj), "PopulationIncrease",
                         G_CALLBACK(onPopulationChanged), (gpointer)0);
      popChg_signal =
        g_signal_connect(G_OBJECT(dataObj), "PositionChanged",
                         G_CALLBACK(onPositionChanged), (gpointer)0);
    }
}
static void onDataNotReady(GObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view, gpointer data _U_)
{
  DBG_fprintf(stderr, "Gtk Move: caught 'dataUnRendered' signal.\n");
  
  g_signal_handler_disconnect(G_OBJECT(view), angles_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), popDec_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), popInc_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), popChg_signal);
}

static void refreshMoveAxesValues(VisuGlView *view, gpointer data)
{
  gint id;
  float x[3], y[3];
  char tmpChr[20];

  id = gtk_notebook_get_current_page(GTK_NOTEBOOK(notebookAction));
  DBG_fprintf(stderr, "Gtk Move: refresh screen basis set.\n");
  DBG_fprintf(stderr, " | %d %d\n", id + 1, VISU_UI_ACTION_MOVE);
  if (id + 1 == VISU_UI_ACTION_MOVE || GPOINTER_TO_INT(data))
    {
      visu_gl_camera_getScreenAxes(view->camera, x, y);
      sprintf(tmpChr, "(%4.2f;%4.2f;%4.2f)", x[0], x[1], x[2]);
      gtk_label_set_text(GTK_LABEL(labelScreenHorizontal), tmpChr);
      sprintf(tmpChr, "(%4.2f;%4.2f;%4.2f)", y[0], y[1], y[2]);
      gtk_label_set_text(GTK_LABEL(labelScreenVertical), tmpChr);
    }
}
static void onMoveMethod(GtkToggleButton *toggle, gpointer data _U_)
{
  if (!gtk_toggle_button_get_active(toggle))
    return;

  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), 0.);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), 0.);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), 0.);

  /* Set the moving list. */
  setMovingNodes();
}
static void onMovePositionChanged(VisuUiNumericalEntry *entry, double oldValue,
				  gpointer data)
{
  float *valOfNode;
  VisuData *dataObj;
  VisuNodeArray *array;
  VisuNode *node;
  VisuElement *ele;
  VisuNodeArrayIter iter;

  g_return_if_fail(GPOINTER_TO_INT(data) >= 0 && GPOINTER_TO_INT(data) < 3);

  /* We are not typing. */
  if (!gtk_widget_is_focus(GTK_WIDGET(entry)))
    return;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  array = VISU_NODE_ARRAY(dataObj);
  ele = (VisuElement*)0;
  DBG_fprintf(stderr, "Gtk Move: move nodes of delta = %g (%d).\n",
              visu_ui_numerical_entry_getValue(entry) - oldValue, GPOINTER_TO_INT(data));
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
    {
      if (movedNode < 0)
	return;

      node = visu_node_array_getFromId(array, movedNode);
      g_return_if_fail(node);

      valOfNode = node->xyz + GPOINTER_TO_INT(data);
  
      if (*valOfNode == (float)visu_ui_numerical_entry_getValue(entry))
	return;

      *valOfNode = (float)visu_ui_numerical_entry_getValue(entry);
      ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);
    }
  else
    {
      visu_node_array_iterNew(array, &iter);
      for (visu_node_array_iterStartList(array, &iter,
                                         visu_ui_interactive_pick_getNodeSelection());
           iter.node; visu_node_array_iterNextList(array, &iter))
        iter.node->xyz[GPOINTER_TO_INT(data)] +=
          visu_ui_numerical_entry_getValue(entry);
   }
  g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", ele, NULL);

  VISU_REDRAW_ADD;
}
static void onMoveToOriginalClicked(GtkButton *button _U_, gpointer data _U_)
{
  VisuData *dataObj;
  VisuNode *node;
  float trans[3];
  VisuNodeArray *array;
  VisuElement *ele;
  VisuNodeArrayIter iter;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  array = VISU_NODE_ARRAY(dataObj);
  ele = (VisuElement*)0;

  /* We are not typing. */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
    {
      if (movedNode < 0)
	return;

      node = visu_node_array_getFromId(array, movedNode);
      g_return_if_fail(node);

      node->xyz[0] = moveNodeOrigin[0];
      node->xyz[1] = moveNodeOrigin[1];
      node->xyz[2] = moveNodeOrigin[2];
      ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);

      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), moveNodeOrigin[0]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), moveNodeOrigin[1]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), moveNodeOrigin[2]);
    }
  else
    {
      trans[0] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]));
      trans[1] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]));
      trans[2] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]));

      visu_node_array_iterNew(array, &iter);
      for (visu_node_array_iterStartList(array, &iter,
                                         visu_ui_interactive_pick_getNodeSelection());
           iter.node; visu_node_array_iterNextList(array, &iter))
        {
          iter.node->xyz[0] -= trans[0];
          iter.node->xyz[1] -= trans[1];
          iter.node->xyz[2] -= trans[2];
        }

      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[0]), 0.);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[1]), 0.);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[2]), 0.);
    }
  g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", ele, NULL);

  VISU_REDRAW_ADD;
}
static void onRemoveNodeClicked(GtkButton *button _U_, gpointer user_data _U_)
{
  int *nodes;
  VisuData *dataObj;
  VisuNode *node;
  int i, n;
  GList *lst, *tmpLst;
  VisuElement *ele;

  DBG_fprintf(stderr, "Gtk Observe/pick: remove the selected node %d.\n", movedNode);
  
  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioMovePick)))
    {
      if (movedNode < 0)
	return;

      node = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), movedNode);
      if (!node)
	return;
      ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);

      nodes = g_malloc(sizeof(int) * 2);
      nodes[0] = movedNode;
      nodes[1] = -1;
      visu_node_array_removeNodes(VISU_NODE_ARRAY(dataObj), nodes);
      g_free(nodes);

      /* Copy the coordinates to the add entries. */
      for (i = 0; i < 3; i++)
	visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryAddXYZ[i]),
				visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryMoveXYZ[i])));
      visu_ui_element_combobox_setSelection(VISU_UI_ELEMENT_COMBOBOX(comboElements), ele->name);

      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", ele, NULL);
    }
  else
    {
      DBG_fprintf(stderr, "Gtk Move: get list of selected nodes.\n");
      tmpLst = lst = visu_ui_interactive_pick_getNodeSelection();
      n = g_list_length(lst);
      if (n > 0)
	{
	  nodes = g_malloc(sizeof(int) * (n + 1));
	  i = 0;
	  while(tmpLst)
	    {
	      nodes[i] = GPOINTER_TO_INT(tmpLst->data);
	      DBG_fprintf(stderr, " | will remove %3d %d\n", i, nodes[i]);
	      i += 1;
	      tmpLst = g_list_next(tmpLst);
	    }
	  nodes[i] = -1;
          visu_node_array_removeNodes(VISU_NODE_ARRAY(dataObj), nodes);
	  g_free(nodes);
	}
      g_list_free(lst);
    }
  /* We remove the node as selected one. */
  setLabelsOrigin((VisuData*)0, (GList*)0);

  VISU_REDRAW_ADD;
}
static void onAddNodeClicked(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuData *dataObj;
  GList *elements;
  VisuElement *element;
  float xyz[3];

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(comboElements));
  g_return_if_fail(elements);
  element = (VisuElement*)elements->data;
  g_list_free(elements);

  xyz[0] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAddXYZ[0]));
  xyz[1] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAddXYZ[1]));
  xyz[2] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAddXYZ[2]));
  visu_data_addNodeFromElement(dataObj, element, xyz, FALSE, TRUE);

  /* We update all drawings. */
  g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", element, NULL);
  VISU_REDRAW_ADD;
}
static void onCheckDuplicate(GtkToggleButton *button, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(data), !gtk_toggle_button_get_active(button));
}
static void onDuplicate(GtkButton *button _U_, gpointer data _U_)
{
  VisuData *dataObj;
  GList *lst;
  gboolean custom;
  VisuElement *element;
  float coord[3], *trans;
  VisuNodeArrayIter iter;

  DBG_fprintf(stderr, "Gtk Move: duplicate selected nodes.\n");

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  element = (VisuElement*)0;
  custom = !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkDuplicate));
  if (custom)
    {
      lst = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(comboDuplicate));
      g_return_if_fail(lst);
      element = (VisuElement*)lst->data;
      DBG_fprintf(stderr, "Gtk Move: change element to '%s'.\n", element->name);
      g_list_free(lst);
    }

  lst = visu_ui_interactive_pick_getNodeSelection();
  if (!lst)
    return;

  trans = visu_data_getXYZtranslation(dataObj);
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for (visu_node_array_iterStartList(VISU_NODE_ARRAY(dataObj), &iter, lst);
       iter.node; visu_node_array_iterNextList(VISU_NODE_ARRAY(dataObj), &iter))
    {
      visu_data_getNodePosition(dataObj, iter.node, coord);
      coord[0] -= trans[0];
      coord[1] -= trans[1];
      coord[2] -= trans[2];
      if (element)
	visu_data_addNodeFromElement(dataObj, element, coord, FALSE, !iter.itLst->next);
      else
	visu_data_addNodeFromIndex(dataObj, iter.iElement, coord, FALSE, !iter.itLst->next);
    }
  g_free(trans);

  /* We update all drawings. */
  /* g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", element, NULL); */
  VISU_REDRAW_ADD;
}
static void onMoveToggled(GtkToggleButton *toggle, gpointer data)
{
  GtkWidget *wd;
  gboolean value;

  value = gtk_toggle_button_get_active(toggle);
  wd = lookup_widget(GTK_WIDGET(data), "alignment41");
  gtk_widget_set_sensitive(wd, value);
}
static gboolean setupBasisMatrix(VisuData *dataObj, float mat[3][3], float O[3])
{
  VisuNode *orig, *nodeA, *nodeB, *nodeC;
  float xyz[3];

  orig  =
    visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), (guint)gtk_spin_button_get_value
                                (GTK_SPIN_BUTTON(spinABC[0])) - 1);
  nodeA =
    visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), (guint)gtk_spin_button_get_value
                                (GTK_SPIN_BUTTON(spinABC[1])) - 1);
  nodeB =
    visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), (guint)gtk_spin_button_get_value
                                (GTK_SPIN_BUTTON(spinABC[2])) - 1);
  nodeC =
    visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), (guint)gtk_spin_button_get_value
                                (GTK_SPIN_BUTTON(spinABC[3])) - 1);
  if (!orig || !nodeA || !nodeB || !nodeC)
    return FALSE;

  visu_data_getNodePosition(dataObj, orig, O);
  visu_data_getNodePosition(dataObj, nodeA, xyz);
  mat[0][0] = xyz[0] - O[0];
  mat[1][0] = xyz[1] - O[1];
  mat[2][0] = xyz[2] - O[2];
  visu_data_getNodePosition(dataObj, nodeB, xyz);
  mat[0][1] = xyz[0] - O[0];
  mat[1][1] = xyz[1] - O[1];
  mat[2][1] = xyz[2] - O[2];
  visu_data_getNodePosition(dataObj, nodeC, xyz);
  mat[0][2] = xyz[0] - O[0];
  mat[1][2] = xyz[1] - O[1];
  mat[2][2] = xyz[2] - O[2];

  return TRUE;
}
static void onSpinBasis(GtkSpinButton *spin, gpointer data)
{
  int i;
  VisuUiRenderingWindow *window;
  VisuGlExtMarks *marks;
  gboolean valid;
  VisuData *dataObj;
  GList lst;
  float mat[3][3], O[3];
  
  i = GPOINTER_TO_INT(data);

  window = visu_ui_main_class_getDefaultRendering();
  dataObj = visu_ui_rendering_window_getData(window);
  marks = visu_ui_rendering_window_getMarks(window);

  lst.next = (GList*)0;
  lst.prev = (GList*)0;
  /* Remove the previous one. */
  if (prevBasis[i] > 0)
    {
      lst.data = GINT_TO_POINTER(prevBasis[i] - 1);
      visu_gl_ext_marks_setHighlightedList(marks, &lst, MARKS_STATUS_UNSET);
    }
  if (timeoutBasis[i] > 0)
    g_source_remove(timeoutBasis[i]);

  prevBasis[i] = (gint)gtk_spin_button_get_value(spin);
  if (prevBasis[i] > 0 && visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), prevBasis[i]))
    {
      lst.data = GINT_TO_POINTER(prevBasis[i] - 1);
      visu_gl_ext_marks_setHighlightedList(marks, &lst, MARKS_STATUS_SET);
      /* Add a new highlight. */
#if GLIB_MINOR_VERSION > 13
      timeoutBasis[i] = g_timeout_add_seconds(1, removeHighlight,
					      GINT_TO_POINTER(prevBasis[i]));
#else
      timeoutBasis[i] = g_timeout_add(1000, removeHighlight,
				      GINT_TO_POINTER(prevBasis[i]));
#endif
    }

  valid = TRUE;
  for(i = 0; i < 4; i++)
    valid = valid && (gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinABC[i])) > 0.);
  gtk_widget_set_sensitive(applyBasis, valid);
  if (valid && setupBasisMatrix(dataObj, mat, O))
    {
      visu_gl_ext_setActive(VISU_GL_EXT(extBasis), TRUE);
      drawBasisCell(visu_boxed_getBox(VISU_BOXED(dataObj)), O, mat);
      if (tool_matrix_determinant(mat) < 0.f)
        visu_ui_interactive_setMessage(_("The new basis set will be indirect."), GTK_MESSAGE_WARNING);
      else
        visu_ui_interactive_unsetMessage();
    }
  else
    visu_gl_ext_setActive(VISU_GL_EXT(extBasis), FALSE);
  
  VISU_REDRAW_FORCE;
}
static gboolean removeHighlight(gpointer data)
{
  int i;
  VisuGlExtMarks *marks;
  GList lst;

  i = GPOINTER_TO_INT(data);
  g_return_val_if_fail(i > 0, FALSE);

  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());

  /* Remove the previous one. */
  lst.next = (GList*)0;
  lst.prev = (GList*)0;
  lst.data = GINT_TO_POINTER(i - 1);
  visu_gl_ext_marks_setHighlightedList(marks, &lst, MARKS_STATUS_UNSET);

  VISU_REDRAW_FORCE;

  return FALSE;
}
static void onChooseBasis(GtkToggleButton *toggle, gpointer data)
{
  guint i;
  VisuUiRenderingWindow *window;
  gboolean valid;

  window = visu_ui_main_class_getDefaultRendering();

  DBG_fprintf(stderr, "Gtk Move: one toggle chooser has been toggled.\n");
  valid = gtk_toggle_button_get_active(toggle);
  if (valid)
    {
      currentMode = interactive_pick;
      currentAxe = GPOINTER_TO_INT(data);
      visu_ui_rendering_window_pushInteractive(window, interPick);
      visu_ui_rendering_window_pushMessage(window, _("Pick a node with the mouse"));
    }
  else
    {
      currentMode = interactive_move;
      visu_ui_rendering_window_popInteractive(window, interPick);
      visu_ui_rendering_window_popMessage(window);
    }

  for(i = 0; i < 4; i++)
    gtk_widget_set_sensitive(toggleABC[i], (!valid || (i == currentAxe)));
  gtk_widget_set_sensitive(lookup_widget(observeWindow, "hbox72"), !valid);
  gtk_widget_set_sensitive(lookup_widget(observeWindow, "alignment44"), !valid);
  gtk_widget_set_sensitive(lookup_widget(observeWindow, "alignment27"), !valid);
  gtk_widget_set_sensitive(lookup_widget(observeWindow, "hboxAddNode"), !valid);
}
static void onBasisSelected(VisuInteractive *inter _U_, VisuInteractivePick pick _U_,
			    VisuNode *node0, VisuNode *node1 _U_, VisuNode *node2 _U_,
                            gpointer data _U_)
{
  g_return_if_fail(node0);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinABC[currentAxe]),
			    node0->number + 1);
  gtk_toggle_button_set_active
    (GTK_TOGGLE_BUTTON(toggleABC[currentAxe]), FALSE);
}
static void onPickClickStop(VisuInteractive *inter _U_, gpointer data _U_)
{
  int i;

  for(i = 0; i < 4; i++)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(toggleABC[i]), FALSE);
}
static void onApplyBasis(GtkButton *button _U_, gpointer data _U_)
{
  VisuData *dataObj;
  float matA[3][3], O[3];

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  if (setupBasisMatrix(dataObj, matA, O))
    {
      if (visu_data_setNewBasis(dataObj, matA, O))
        VISU_REDRAW_FORCE;
      else
        visu_ui_interactive_setMessage
          (_("Cannot change the basis: given matrix is singular."), GTK_MESSAGE_ERROR);
    }

  g_signal_handler_block(G_OBJECT(spinABC[0]), onSpin_id[0]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinABC[0]), 0);
  g_signal_handler_unblock(G_OBJECT(spinABC[0]), onSpin_id[0]);

  g_signal_handler_block(G_OBJECT(spinABC[1]), onSpin_id[1]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinABC[1]), 0);
  g_signal_handler_unblock(G_OBJECT(spinABC[1]), onSpin_id[1]);

  g_signal_handler_block(G_OBJECT(spinABC[2]), onSpin_id[2]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinABC[2]), 0);
  g_signal_handler_unblock(G_OBJECT(spinABC[2]), onSpin_id[2]);

  g_signal_handler_block(G_OBJECT(spinABC[3]), onSpin_id[3]);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinABC[3]), 0);
  g_signal_handler_unblock(G_OBJECT(spinABC[3]), onSpin_id[3]);

  visu_gl_ext_setActive(VISU_GL_EXT(extBasis), FALSE);
}
static void onPopulationChanged(VisuData *dataObj, gint *nodes _U_, gpointer data _U_)
{
  VisuNodeArrayIter iter;

  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  DBG_fprintf(stderr, "Gtk Move: caught 'population changed', update spin up to %d.\n",
              iter.idMax + 1);
  g_signal_handler_block(G_OBJECT(spinABC[0]), onSpin_id[0]);
  gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinABC[0]), 0, iter.idMax + 1);
  g_signal_handler_unblock(G_OBJECT(spinABC[0]), onSpin_id[0]);

  g_signal_handler_block(G_OBJECT(spinABC[1]), onSpin_id[1]);
  gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinABC[1]), 0, iter.idMax + 1);
  g_signal_handler_unblock(G_OBJECT(spinABC[1]), onSpin_id[1]);

  g_signal_handler_block(G_OBJECT(spinABC[2]), onSpin_id[2]);
  gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinABC[2]), 0, iter.idMax + 1);
  g_signal_handler_unblock(G_OBJECT(spinABC[2]), onSpin_id[2]);

  g_signal_handler_block(G_OBJECT(spinABC[3]), onSpin_id[3]);
  gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinABC[3]), 0, iter.idMax + 1);
  g_signal_handler_unblock(G_OBJECT(spinABC[3]), onSpin_id[3]);
}
static void onPositionChanged(VisuData *dataObj, VisuElement *ele _U_, gpointer data _U_)
{
  float mat[3][3], O[3];

  if (setupBasisMatrix(dataObj, mat, O))
    drawBasisCell(visu_boxed_getBox(VISU_BOXED(dataObj)), O, mat);
}
static void onAxeChoosen(VisuUiNumericalEntry *entry _U_, double oldValue _U_,
			 gpointer data _U_)
{
  float axe[3];

  axe[0] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[0]));
  axe[1] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[1]));
  axe[2] = (float)visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[2]));

  visu_interactive_setMovingAxe(interMove, axe);
}
static void onGetAxisClicked(GtkButton *button _U_, gpointer data)
{
  VisuGlView *view;

  view = visu_ui_rendering_window_getGlView(VISU_UI_RENDERING_WINDOW(data));
  g_return_if_fail(view);
  
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[0]),
                                   view->camera->eye[0]);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[1]),
                                   view->camera->eye[1]);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryAxeXYZ[2]),
                                   view->camera->eye[2]);
}

/********************/
/* Drawing methods. */
/********************/
static void drawBasisCell(VisuBox *box _U_, float O[3], float mat[3][3])
{
  /* VisuPlane *dataBox[7]; */
  /* int i, j; */
  /* float ext[3], u[3], n[3], m[3], v[12][2][3], b[8][3]; */
  
  DBG_fprintf(stderr, "Gtk Move: draw the new basis cell.\n");
  visu_gl_ext_box_setBasis(extBasis, O, mat);

  /* /\* We build the planes of the bounding box. *\/ */
  /* visu_box_getExtension(box, ext); */
  /* j = 0; */
  /* for (i = 0; i < 3; i++) */
  /*   { */
  /*     u[0] = 0.f; */
  /*     u[1] = 0.f; */
  /*     u[2] = 0.f; */
  /*     u[i] = 1.f; */
  /*     visu_box_convertBoxCoordinatestoXYZ(box, n, u); */
  /*     dataBox[j] = visu_plane_newUndefined(); */
  /*     visu_plane_setNormalVector(dataBox[j], n); */
  /*     u[i] = 1 + ext[i]; */
  /*     visu_box_convertBoxCoordinatestoXYZ(box, m, u); */
  /*     visu_plane_setDistanceFromOrigin(dataBox[j], sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2])); */
  /*     j += 1; */
  /*     dataBox[j] = visu_plane_newUndefined(); */
  /*     visu_plane_setNormalVector(dataBox[j], n); */
  /*     u[i] = - ext[i]; */
  /*     visu_box_convertBoxCoordinatestoXYZ(box, m, u); */
  /*     visu_plane_setDistanceFromOrigin(dataBox[j], -sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2])); */
  /*     j += 1; */
  /*   } */
  /* dataBox[j] = (VisuPlane*)0; */
  /* /\* We build the vertex array. *\/ */
  /* b[0][0] = O[0]; */
  /* b[0][1] = O[1]; */
  /* b[0][2] = O[2]; */
  /* b[1][0] = O[0] + mat[0][0]; */
  /* b[1][1] = O[1] + mat[1][0]; */
  /* b[1][2] = O[2] + mat[2][0]; */
  /* b[2][0] = O[0] + mat[0][0] + mat[0][1]; */
  /* b[2][1] = O[1] + mat[1][0] + mat[1][1]; */
  /* b[2][2] = O[2] + mat[2][0] + mat[2][1]; */
  /* b[3][0] = O[0] + mat[0][1]; */
  /* b[3][1] = O[1] + mat[1][1]; */
  /* b[3][2] = O[2] + mat[2][1]; */
  /* b[4][0] = O[0] + mat[0][2]; */
  /* b[4][1] = O[1] + mat[1][2]; */
  /* b[4][2] = O[2] + mat[2][2]; */
  /* b[5][0] = O[0] + mat[0][0] + mat[0][2]; */
  /* b[5][1] = O[1] + mat[1][0] + mat[1][2]; */
  /* b[5][2] = O[2] + mat[2][0] + mat[2][2]; */
  /* b[6][0] = O[0] + mat[0][0] + mat[0][1] + mat[0][2]; */
  /* b[6][1] = O[1] + mat[1][0] + mat[1][1] + mat[1][2]; */
  /* b[6][2] = O[2] + mat[2][0] + mat[2][1] + mat[2][2]; */
  /* b[7][0] = O[0] + mat[0][1] + mat[0][2]; */
  /* b[7][1] = O[1] + mat[1][1] + mat[1][2]; */
  /* b[7][2] = O[2] + mat[2][1] + mat[2][2]; */

  /* visu_plane_class_getIntersection(dataBox, b[0], b[1], v[0][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[1], b[0], v[0][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[1], b[2], v[1][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[2], b[1], v[1][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[2], b[3], v[2][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[3], b[2], v[2][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[3], b[0], v[3][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[0], b[3], v[3][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[4], b[5], v[4][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[5], b[4], v[4][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[5], b[6], v[5][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[6], b[5], v[5][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[6], b[7], v[6][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[7], b[6], v[6][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[7], b[4], v[7][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[4], b[7], v[7][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[0], b[4], v[8][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[4], b[0], v[8][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[1], b[5], v[9][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[5], b[1], v[9][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[2], b[6], v[10][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[6], b[2], v[10][1], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[3], b[7], v[11][0], FALSE); */
  /* visu_plane_class_getIntersection(dataBox, b[7], b[3], v[11][1], FALSE); */
  /* DBG_fprintf(stderr, "Gtk Move: new basis axes.\n"); */
  /* for (i = 0; i < 12; i++) */
  /*   DBG_fprintf(stderr, " | %6.2f x %6.2f x %6.2f -> %6.2f x %6.2f x %6.2f\n", */
  /*               v[i][0][0], v[i][0][1], v[i][0][2], v[i][1][0], v[i][1][1], v[i][1][2]); */

  /* for (i = 0; i < 6; i++) */
  /*   g_object_unref(G_OBJECT(dataBox[i])); */

  visu_gl_ext_box_draw(extBasis);

  /* glDeleteLists(visu_gl_ext_getGlList(extBasis), 1); */
  /* glNewList(visu_gl_ext_getGlList(extBasis), GL_COMPILE); */
  /* glDisable(GL_LIGHTING); */
  /* glDisable(GL_DITHER); */
  /* glEnable(GL_BLEND); */

  /* glLineWidth(1); */
  /* glColor3f(1.f, 1.f, 1.f); */
  /* glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR); */
  /* glEnable(GL_LINE_STIPPLE); */
  /* glLineStipple(1, 57568); */
  /* glBegin(GL_LINES); */
  /* glVertex3fv(v[0][0]); */
  /* glVertex3fv(v[0][1]); */
  /* glVertex3fv(v[1][0]); */
  /* glVertex3fv(v[1][1]); */
  /* glVertex3fv(v[2][0]); */
  /* glVertex3fv(v[2][1]); */
  /* glVertex3fv(v[3][0]); */
  /* glVertex3fv(v[3][1]); */
  /* glVertex3fv(v[4][0]); */
  /* glVertex3fv(v[4][1]); */
  /* glVertex3fv(v[5][0]); */
  /* glVertex3fv(v[5][1]); */
  /* glVertex3fv(v[6][0]); */
  /* glVertex3fv(v[6][1]); */
  /* glVertex3fv(v[7][0]); */
  /* glVertex3fv(v[7][1]); */
  /* glVertex3fv(v[8][0]); */
  /* glVertex3fv(v[8][1]); */
  /* glVertex3fv(v[9][0]); */
  /* glVertex3fv(v[9][1]); */
  /* glVertex3fv(v[10][0]); */
  /* glVertex3fv(v[10][1]); */
  /* glVertex3fv(v[11][0]); */
  /* glVertex3fv(v[11][1]); */
  /* glEnd(); */
  /* glDisable(GL_LINE_STIPPLE); */

  /* glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); */
  /* glDisable(GL_CULL_FACE); */
  /* glColor4f(0.f, 0.f, 0.f, 0.2f); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[0]); */
  /* glVertex3fv(b[1]); */
  /* glVertex3fv(b[2]); */
  /* glVertex3fv(b[3]); */
  /* glEnd(); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[0]); */
  /* glVertex3fv(b[1]); */
  /* glVertex3fv(b[5]); */
  /* glVertex3fv(b[4]); */
  /* glEnd(); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[0]); */
  /* glVertex3fv(b[3]); */
  /* glVertex3fv(b[7]); */
  /* glVertex3fv(b[4]); */
  /* glEnd(); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[4]); */
  /* glVertex3fv(b[5]); */
  /* glVertex3fv(b[6]); */
  /* glVertex3fv(b[7]); */
  /* glEnd(); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[6]); */
  /* glVertex3fv(b[5]); */
  /* glVertex3fv(b[1]); */
  /* glVertex3fv(b[2]); */
  /* glEnd(); */
  /* glBegin(GL_POLYGON); */
  /* glVertex3fv(b[6]); */
  /* glVertex3fv(b[7]); */
  /* glVertex3fv(b[3]); */
  /* glVertex3fv(b[2]); */
  /* glEnd(); */
  /* glEnable(GL_CULL_FACE); */
  /* glCullFace(GL_BACK); */

  /* glEnable(GL_LIGHTING); */
  /* glEnable(GL_DITHER); */
  /* glEndList(); */
}
