/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_OPENGLWIDGET_H
#define GTK_OPENGLWIDGET_H

#include "openGLFunctions/view.h"
#include "visu_data.h"

/**
 * VisuUiGlWidget:
 *
 * Short name to address VisuUiGlWidget_struct objects.
 */
typedef struct _VisuUiGlWidget VisuUiGlWidget;
/**
 * VisuUiGlWidgetClass:
 *
 * Short name to address VisuUiGlWidgetClass_struct objects.
 */
typedef struct _VisuUiGlWidgetClass VisuUiGlWidgetClass;

/**
 * VisuUiGlWidgetRedrawMethod:
 * @view: a #VisuGlView object with the camera settings.
 * @lists: (allow-none) (type VisuGlExt*): a #VisuGlExt array or %NULL.
 *
 * Methods of this prototype are called when the redraw of the OpenGL zone
 * should be done.
 */
typedef void (*VisuUiGlWidgetRedrawMethod)(VisuGlView *view, GList *lists);

/**
 * VISU_UI_TYPE_GL_WIDGET:
 * 
 * The type of VisuUiGlWidget objects.
 */
#define VISU_UI_TYPE_GL_WIDGET           (visu_ui_gl_widget_get_type())
/**
 * VISU_UI_GL_WIDGET:
 * @obj: a pointer.
 *
 * Cast @obj to VisuUiGlWidget if possible.
 */
#define VISU_UI_GL_WIDGET(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj),	\
								 VISU_UI_TYPE_GL_WIDGET, \
								 VisuUiGlWidget))
/**
 * VISU_UI_GL_WIDGET_CLASS:
 * @obj: a pointer.
 *
 * Cast @obj to VisuUiGlWidgetClass if possible.
 */
#define VISU_UI_GL_WIDGET_CLASS(obj)     (G_TYPE_CHECK_CLASS_CAST((obj),	\
							      VISU_UI_TYPE_GL_WIDGET, \
							      VisuUiGlWidgetClass))
/**
 * VISU_UI_IS_GL_WIDGET:
 * @obj: a pointer.
 *
 * Return TRUE is @obj is an VisuUiGlWidget object (or inherit from).
 */ 
#define VISU_UI_IS_GL_WIDGET(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj),	\
								 VISU_UI_TYPE_GL_WIDGET))
/**
 * VISU_UI_IS_GL_WIDGET_CLASS:
 * @obj: a pointer.
 *
 * Return TRUE is @obj is an VisuUiGlWidgetClass object (or inherit from).
 */ 
#define VISU_UI_IS_GL_WIDGET_CLASS(obj)  (G_TYPE_CHECK_CLASS_TYPE((obj),	\
							      VISU_UI_TYPE_GL_WIDGET))
/**
 * VISU_UI_GL_WIDGET_GET_CLASS:
 * @obj: a pointer.
 *
 * Return the class of the given VisuUiGlWidget object.
 */
#define VISU_UI_GL_WIDGET_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj),	\
								VISU_UI_TYPE_GL_WIDGET, \
								VisuUiGlWidgetClass))

/**
 * visu_ui_gl_widget_get_type:
 *
 * Retrive the type of VisuUiGlWidget objects.
 *
 * Returns: the id used by OBjects for VisuUiGlWidget objects.
 */
GType visu_ui_gl_widget_get_type(void);

/**
 * visu_ui_gl_widget_new:
 * @contextIsDirect: a boolean.
 *
 * Create a new OpenGL area inside a GTK widget. If @contextIsDirect then
 * it tries to initialise the OpenGL context to a direct one.
 *
 * Returns: a newly created widget.
 */
GtkWidget* visu_ui_gl_widget_new(gboolean contextIsDirect);
/**
 * visu_ui_gl_widget_setCurrent:
 * @render: a #VisuUiGlWidget object ;
 * @force: a boolean.
 *
 * Make this object current. This means that all future OpenGL primitive will be
 * rendered on this surface. If @force is TRUE, the GL routine is
 * actually called whereas in other cases, if @render believe being
 * already current, nothing is done.
 *
 * Returns: TRUE if succeed.
 */
gboolean visu_ui_gl_widget_setCurrent(VisuUiGlWidget *render, gboolean force);
void visu_ui_gl_widget_setRedraw(VisuUiGlWidget *render,
                                 VisuUiGlWidgetRedrawMethod method,
                                 VisuGlView *view);
void visu_ui_gl_widget_redraw(VisuUiGlWidget *render);
/**
 * visu_ui_gl_widget_swapBuffers:
 * @render: a #VisuUiGlWidget object.
 *
 * Swap the buffers of the OpenGL area.
 */
void visu_ui_gl_widget_swapBuffers(VisuUiGlWidget *render);
GArray* visu_ui_gl_widget_getPixmapData(VisuUiGlWidget *render, int *width,
                                        int *height, gboolean offScreen,
                                        gboolean hasAlpha);
VisuUiGlWidget* visu_ui_gl_widget_class_getCurrentContext();

#endif
