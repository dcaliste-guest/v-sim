/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_PAIRS_H
#define GTK_PAIRS_H

#include <gtk/gtk.h>

#include "interface.h"
#include "support.h"
#include "visu_pairs.h"
#include "gtk_main.h"

/**
 * VisuUiPairsInitFunc:
 *
 * Prototype of functions called once on V_Sim start-up.
 */
typedef void (*VisuUiPairsInitFunc)(void);
/**
 * VisuUiPairsBuildWidgetsFunc:
 *
 * Prototype of functions that create a container #GtkWidgets will all
 * element needed to change characteristic of a model.
 *
 * Returns: a newly allocated #GtkWidget.
 */
typedef GtkWidget* (*VisuUiPairsBuildWidgetsFunc)();
/**
 * VisuUiPairsToLabelFunc:
 * @data: information about a pair.
 *
 * Create a string, internationalised and in UTF-8, that describes the
 * given pair @data. This string is used in a column tree view to
 * summarize the pair description.
 *
 * Returns: a newly created string.
 */
typedef gchar* (*VisuUiPairsToLabelFunc)(VisuPairLink *data);
/**
 * VisuUiPairsSetValuesFunc:
 * @data: informations about a pair.
 *
 * Prototype of functions used to update the widgets with given pair @data.
 */
typedef void (*VisuUiPairsSetValuesFunc)(VisuPairLink *data);

/**
 * visu_ui_pairs_init:
 *
 * Initialise the default values for the pair dialog. It does not build
 * the interface, use visu_ui_pairs_initBuild() to do it.
 */
void visu_ui_pairs_init();

/**
 * visu_ui_pairs_initBuild:
 * @main: the command panel the about dialog is associated to.
 *
 * Create the dialog window for pairs.
 */
void visu_ui_pairs_initBuild(VisuUiMain *main);
/**
 * visu_ui_pairs_update:
 * @main: the command panel the about dialog is associated to ;
 * @dataObj: the #VisuData the pairs are related to (can be NULL, if
 * none is loaded) ;
 * @force: build the pairs even if the window is hidden.
 *
 * Update the list of pairs. This routine must be called only after
 * visu_ui_pairs_initBuild() has been called. The job is done only if the
 * window is visible or if the @force argument is used.
 */
void visu_ui_pairs_update(VisuUiMain *main, VisuData *dataObj, gboolean force);
void visu_ui_pairs_show(VisuUiMain *main);

/**
 * visu_ui_pairs_setSpecificLabels:
 * @iter: the #GtkTreeIter to set the label ;
 * @label: the value of the label to be set.
 *
 * Change the specific label shown in the treeview of pairs for the
 * given iter. An iter in this treeview can be retrieve using the
 * #_VisuUiPairsIter objects.
 */
void visu_ui_pairs_setSpecificLabels(GtkTreeIter *iter, const gchar *label);

gboolean visu_ui_pairs_select(const VisuPairLink *data);

typedef struct _VisuUiPairsIter VisuUiPairsIter;
struct _VisuUiPairsIter
{
  VisuElement *ele1;
  VisuElement *ele2;
  VisuPairLink *data;
  GtkTreeIter iter;

  /* Private data. */
  GList *selected;
  GList *current;
};
void visu_ui_pairs_newIter(VisuUiPairsIter *iter);
void visu_ui_pairs_iter_empty(VisuUiPairsIter *iter);
void visu_ui_pairs_iter_startSelected(VisuUiPairsIter *iter);
void visu_ui_pairs_iter_nextSelected(VisuUiPairsIter *iter);

#endif
