/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <math.h>

#include "support.h"
#include "gtk_interactive.h"
#include "visu_gtk.h"
#include "gtk_main.h"
#include "gtk_pick.h"
#include "visu_object.h"
#include "visu_basic.h"
#include "gtk_renderingWindowWidget.h"
#include "extraFunctions/dataNode.h"
#include "extensions/infos.h"
#include "extensions/marks.h"
#include "openGLFunctions/interactive.h"
#include "extraGtkFunctions/gtk_valueIOWidget.h"

/**
 * SECTION: gtk_pick
 * @short_description: The pick and measurement tab in the interactive
 * dialog.
 *
 * <para>This action tab provides widgets to display information about
 * selected atoms, like distances or angles. In addition, measured
 * distances and angles are kept in a list when new files are
 * loaded.</para>
 * <para>With the list of selected nodes, one can modify properties
 * associated to nodes like their coordinates, the value of
 * colourisation if any, the forces on them, if any... One can also
 * decide to display information directly on nodes.</para>
 */

/* The ids of the column used in the data treeview.
   This treeview is made on a list with NB_COLUMN_BASE + n * NB_COLUMN_DATA,
   where n is the number of data associated per node. */
enum
  {
    COLUMN_BASE_NUMBER,
    COLUMN_BASE_ELEMENT,
    COLUMN_BASE_HIGHLIGHT,
    NB_COLUMN_BASE
  };
enum
  {
    COLUMN_DATA_LABEL,
    COLUMN_DATA_EDITABLE,
    COLUMN_DATA_COLOR,
    NB_COLUMN_DATA
  };

/* The ids of the columns used in the combobox that identify
   each data node. */
enum
  {
    COLUMN_COMBO_LABEL,
    COLUMN_COMBO_STOCK,
    COLUMN_COMBO_POINTER,
    NB_COLUMN_COMBO
  };

#define HIDE_HIGHLIGHT     0
#define HIDE_NON_HIGHLIGHT 1

static GtkWidget *comboDraw;

#define GTK_PICK_INFO				\
  _("left-button\t\t\t: standard pick\n"			\
    "control-left-button\t\t: toggle highlihgt node\n"		\
    "middle-button\t\t: measure node neighbouring\n"		\
    "shift-middle-button\t: pick 1st reference\n"		\
    "ctrl-middle-button\t\t: pick 2nd reference\n"		\
    "drag-left-button\t\t: make a rectangular selection\n"	\
    "right-button\t\t\t: switch to observe")

/* Treeview used to print data of nodes. */
static GtkListStore *listDataNode;
static GtkWidget *treeviewDataNode;
#define GTK_PICK_EDITABLE_NODE   "blue"
#define GTK_PICK_UNEDITABLE_NODE "black"
static GtkTreeViewColumn** dataCols;

/* Draw data widgets. */
static GtkWidget *radioDrawNever, *radioDrawSelected, *radioDrawAlways;
static GtkListStore *listComboInfos;
static GtkWidget *lblList;
static GtkWidget *valueIO;
static GtkWidget *hboxMarks, *tglMarks, *lblMarks;
static VisuInteractive *interPick;

/* The pick viewport. */
static GtkWidget *labelPickOut, *labelPickHistory = (GtkWidget*)0, *labelPickError;
struct _PickHistory
{
  VisuData *dataObj;
  gchar *str;
};
static GList *pickHistory = (GList*)0;

/* Signals that need to be suspended */
static gulong radioInfosSignals[3];
static gulong comboInfosSignal;
static gulong hide_signal, popDec_signal;

/* Local callbacks. */
static void onEditedPick(GtkCellRendererText *cellrenderertext,
			 gchar *path, gchar *text, gpointer user_data);
static void onNodePropertyUsed(VisuDataNode* data, VisuData *dataObj, gpointer user_data);
static void onNodePropertyUnused(VisuDataNode* data, VisuData *dataObj,
				 gpointer user_data);
static void onNodePropertyChanged(VisuDataNode* data, VisuData *dataObj,
				  gpointer user_data);
static void onRadioDrawInfos(GtkToggleButton *togglebutton, gpointer user_data);
static void onDrawDistanceChecked(GtkToggleButton* button, gpointer data);
static void onEraseDistanceClicked(GtkButton *button, gpointer user_data);
static void onDataReady(GObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer bool);
static void onDataNotReady(GObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer bool);
static void onNodeRemoved(VisuData *visuData, int *nodeNumbers, gpointer data);
static void onComboInfosChanged(GtkComboBox *combo, gpointer data);
static gboolean onTreeviewInfosKey(GtkWidget *widget, GdkEventKey *event,
				   gpointer user_data);
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw, gpointer data);
static void onHighlightEraseClicked(GtkButton *button, gpointer user_data);
static void onHighlightToggled(GtkCellRendererToggle *cell_renderer,
			       gchar *path, gpointer user_data);
static void onHighlightClicked(GtkButton *button, gpointer user_data);
static void onHighlightSetClicked(GtkButton *button, gpointer user_data);
static void onHighlightHideToggled(GtkButton *button, gpointer user_data);
static void onHighlightHideStatus(GtkButton *button, gpointer user_data);
static void onNodeSelection(VisuInteractive *inter, VisuInteractivePick pick, 
			    VisuNode *node0, VisuNode *node1, VisuNode *node2,
                            gpointer data);
static void onRegionSelection(VisuInteractive *inter, GList *nodes,
			      gpointer data);
static void onSelectionError(VisuInteractive *inter, VisuInteractivePickError error,
			     gpointer data);
static void onClickStop(VisuInteractive *inter, gpointer data);
static void onHighlightList(VisuGlExtMarks *marks, GList *lst, gpointer data);
static void onMeasurementList(VisuGlExtMarks *marks, gpointer data);

/* Local routines. */
static int* getListedNodes();
static void updateLabelMarks(GList *hl);
static void updateLabelList();
static gboolean getIterPick(guint nodeId, GtkTreeIter *iter);
static void drawDataOnNode(VisuData *data, VisuGlView *view, VisuGlExtInfosDrawId item);
static void addNodeAtIter(VisuData *dataObj, VisuNode *node, GtkTreeIter *iter,
			  gboolean highlight);
static void populateComboInfos(VisuData *dataObj);
static gboolean applyHidingScheme(VisuData *data);
static gboolean onLoadXML(const gchar *filename, GError **error);


/********************/
/* Public routines. */
/********************/
/**
 * visu_ui_interactive_pick_init: (skip)
 *
 * Internal routine to setup the pick action of the interactive dialog.
 *
 * Since: 3.6
 */
void visu_ui_interactive_pick_init()
{
  listDataNode = (GtkListStore*)0;
  radioDrawNever = (GtkWidget*)0;
  radioDrawAlways = (GtkWidget*)0;
  radioDrawSelected = (GtkWidget*)0;
  comboDraw = (GtkWidget*)0;
  g_signal_connect
    (G_OBJECT(visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering())),
     "measurementChanged", G_CALLBACK(onMeasurementList), (gpointer)0);
}
/**
 * visu_ui_interactive_pick_initBuild: (skip)
 * @main: the main interface.
 * @label: a location to store the name of the pick tab ;
 * @help: a location to store the help message to be shown at the
 * bottom of the window ;
 * @radio: a location on the radio button that will be toggled when
 * the pick action is used.
 * 
 * This routine should be called in conjonction to the
 * visu_ui_interactive_move_initBuild() one. It completes the creation of widgets
 * (and also initialisation of values) for the pick tab.
 */
GtkWidget* visu_ui_interactive_pick_initBuild(VisuUiMain *main, gchar **label,
                                              gchar **help, GtkWidget **radio)
{
  int i, j, nb, nbData;
  GList *tmpLst;
  GType *dataTypes;
  GtkTreeViewColumn *column;
  GtkCellRenderer *renderer;
  const gchar *title;
  GtkWidget *lbl, *wd, *wd2, *hbox, *vbox, *image;
  gchar *markup;
  VisuData *dataObj;
  VisuUiRenderingWindow *window;
  VisuGlExtMarks *marks;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();
#endif
  
  DBG_fprintf(stderr, "Gtk Pick: setup new action tab.\n");
  window = visu_ui_main_class_getDefaultRendering();
  interPick = visu_interactive_new(interactive_measure);
  g_object_ref(G_OBJECT(interPick));
  marks = visu_ui_rendering_window_getMarks(window);
  g_signal_connect(G_OBJECT(interPick), "node-selection",
		   G_CALLBACK(onNodeSelection), (gpointer)window);
  g_signal_connect(G_OBJECT(interPick), "selection-error",
		   G_CALLBACK(onSelectionError), (gpointer)0);
  g_signal_connect(G_OBJECT(interPick), "region-selection",
		   G_CALLBACK(onRegionSelection), (gpointer)window);
  g_signal_connect(G_OBJECT(interPick), "stop",
		   G_CALLBACK(onClickStop), (gpointer)0);
  g_signal_connect(G_OBJECT(marks), "highlightChanged",
		   G_CALLBACK(onHighlightList), (gpointer)0);

  *label = g_strdup("Pick");
  *help  = g_strdup(GTK_PICK_INFO);
  *radio = lookup_widget(main->interactiveDialog, "radioPick");

  /* Get the current VisuData object. */
  dataObj = visu_ui_rendering_window_getData(window);

  /* Create the liststore used for the VisuDataNode. */
  DBG_fprintf(stderr, "Gtk Pick: Create the liststore used for the VisuDataNode.\n");
  tmpLst = visu_data_node_class_getAll();
  nbData = g_list_length(tmpLst);
  DBG_fprintf(stderr, " | %d columns.\n", nbData);
  dataCols = g_malloc(sizeof(GtkTreeViewColumn*) * nbData);
  nb = nbData * NB_COLUMN_DATA + NB_COLUMN_BASE;
  dataTypes = g_malloc(sizeof(GType) * nb);
  dataTypes[COLUMN_BASE_NUMBER] = G_TYPE_UINT;
  dataTypes[COLUMN_BASE_ELEMENT] = G_TYPE_STRING;
  dataTypes[COLUMN_BASE_HIGHLIGHT] = G_TYPE_BOOLEAN;
  for (i = NB_COLUMN_BASE; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      DBG_fprintf(stderr, " | add dataNode %p '%s'\n", tmpLst->data,
                  visu_data_node_getLabel(VISU_DATA_NODE(tmpLst->data)));
      /* The string to put on screen. */
      dataTypes[i + COLUMN_DATA_LABEL] = G_TYPE_STRING;
      /* Wether this string is editable or not. */
      dataTypes[i + COLUMN_DATA_EDITABLE] = G_TYPE_BOOLEAN;
      /* Give the color used to render the string. */
      dataTypes[i + COLUMN_DATA_COLOR] = G_TYPE_STRING;
      i += NB_COLUMN_DATA;
    }
  listDataNode = gtk_list_store_newv(nb, dataTypes);
  g_free(dataTypes);

  /* Create the treeview and related buttons. */
  DBG_fprintf(stderr, "Gtk Pick: Create the treeview and related buttons.\n");
  hbox = lookup_widget(main->interactiveDialog, "hbox74");

  wd = gtk_scrolled_window_new(NULL, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), wd, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wd),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(wd), GTK_SHADOW_IN);

  treeviewDataNode = gtk_tree_view_new ();
  gtk_container_add(GTK_CONTAINER(wd), treeviewDataNode);
  gtk_widget_set_size_request(treeviewDataNode, -1, 100);
  gtk_tree_view_set_model(GTK_TREE_VIEW(treeviewDataNode),
			  GTK_TREE_MODEL(listDataNode));
  g_signal_connect(G_OBJECT(treeviewDataNode), "key-press-event",
		   G_CALLBACK(onTreeviewInfosKey), (gpointer)0);
  gtk_tree_selection_set_mode
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewDataNode)),
     GTK_SELECTION_MULTIPLE);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 2);

  wd = gtk_button_new();
  gtk_widget_set_tooltip_text(wd, _("Remove all highlight marks for"
				       " the nodes of the list."));
  gtk_box_pack_start(GTK_BOX(vbox), wd, FALSE, FALSE, 1);
  image = create_pixmap((GtkWidget*)0, "stock-unselect-all_20.png");
  gtk_container_add(GTK_CONTAINER(wd), image);
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onHighlightClicked), GINT_TO_POINTER(FALSE));

  wd = gtk_button_new();
  gtk_widget_set_tooltip_text(wd, _("Put a highlight mark on all"
				       " the nodes of the list."));
  gtk_box_pack_end(GTK_BOX(vbox), wd, FALSE, FALSE, 1);
  image = create_pixmap((GtkWidget*)0, "stock-select-all_20.png");
  gtk_container_add(GTK_CONTAINER(wd), image);
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onHighlightClicked), GINT_TO_POINTER(TRUE));

  gtk_widget_show_all(hbox);

  /* Create the list for the combobox of data node. */
  DBG_fprintf(stderr, "Gtk Pick: Create the list store.\n");
  listComboInfos = gtk_list_store_new(NB_COLUMN_COMBO, G_TYPE_STRING,
				      G_TYPE_STRING, G_TYPE_POINTER);
  /* Building headers. */
  DBG_fprintf(stderr, "Gtk Pick: Build the tree view.\n");
  /* Id colum. */
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "scale", 0.75, NULL);
  lbl = gtk_label_new("");
  title = _("Id");
  markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);
  gtk_label_set_markup(GTK_LABEL(lbl), markup);
  gtk_widget_show(lbl);
  g_free(markup);
  column = gtk_tree_view_column_new_with_attributes(_("Node"), renderer,
						    "text", COLUMN_BASE_NUMBER,
						    NULL);
  gtk_tree_view_column_set_sort_column_id(column, COLUMN_BASE_NUMBER);
  gtk_tree_view_column_set_sort_indicator(column, TRUE);
  gtk_tree_view_column_set_widget(column, lbl);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewDataNode), column);
  /* Highlight colum. */
  renderer = gtk_cell_renderer_toggle_new();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onHighlightToggled), (gpointer)0);
  g_object_set(G_OBJECT(renderer), "indicator-size", 10, NULL);
  column = gtk_tree_view_column_new_with_attributes("", renderer,
						    "active", COLUMN_BASE_HIGHLIGHT,
						    NULL);
  gtk_tree_view_column_set_sort_column_id(column, COLUMN_BASE_HIGHLIGHT);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewDataNode), column);
  /* Element column. */
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "scale", 0.75, NULL);
  lbl = gtk_label_new("");
  title = _("Type");
  markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);
  gtk_label_set_markup(GTK_LABEL(lbl), markup);
  gtk_widget_show(lbl);
  g_free(markup);
  column = gtk_tree_view_column_new_with_attributes(_("Node"), renderer,
						    "text", COLUMN_BASE_ELEMENT, NULL);
  gtk_tree_view_column_set_sort_column_id(column, COLUMN_BASE_ELEMENT);
  gtk_tree_view_column_set_widget(column, lbl);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewDataNode), column);

  DBG_fprintf(stderr, "Gtk Pick: Build data node columns.\n");
  i = 0;
  for (tmpLst = visu_data_node_class_getAll(); tmpLst; tmpLst = g_list_next(tmpLst))
    {
      j = i * NB_COLUMN_DATA + NB_COLUMN_BASE;
      renderer = gtk_cell_renderer_text_new();
      g_signal_connect(G_OBJECT(renderer), "edited",
		       G_CALLBACK(onEditedPick), GINT_TO_POINTER(i));
      g_object_set(G_OBJECT(renderer), "scale", 0.75, NULL);
      title = visu_data_node_getLabel(VISU_DATA_NODE(tmpLst->data));
      DBG_fprintf(stderr, " | add column '%s'.\n", title);
      lbl = gtk_label_new("");
      markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);
      gtk_label_set_markup(GTK_LABEL(lbl), markup);
      gtk_widget_show(lbl);
      g_free(markup);
      dataCols[i] = gtk_tree_view_column_new_with_attributes
	("", renderer,
	 "markup", j + COLUMN_DATA_LABEL,
	 "editable", j + COLUMN_DATA_EDITABLE,
	 "foreground", j + COLUMN_DATA_COLOR, NULL);
/*       g_object_set(G_OBJECT(column), "editable-set", TRUE, NULL); */
      gtk_tree_view_column_set_widget(dataCols[i], lbl);
      gtk_tree_view_column_set_visible(dataCols[i], FALSE);
      gtk_tree_view_append_column(GTK_TREE_VIEW(treeviewDataNode), dataCols[i]);

      g_signal_connect_object(G_OBJECT(tmpLst->data), "propertyUsed",
                              G_CALLBACK(onNodePropertyUsed), (gpointer)window, 0);
      g_signal_connect_object(G_OBJECT(tmpLst->data), "propertyUnused",
                              G_CALLBACK(onNodePropertyUnused), (gpointer)window, 0);
      g_signal_connect_object(G_OBJECT(tmpLst->data), "valueChanged",
                              G_CALLBACK(onNodePropertyChanged), (gpointer)window, 0);

      i += 1;
    }

  /* Set the load/save widget. */
  DBG_fprintf(stderr, "Gtk Pick: Add some widgets.\n");
  valueIO = visu_ui_value_io_new(GTK_WINDOW(main->interactiveDialog),
		       _("Import picked nodes from an existing XML file."),
		       _("Export listed picked nodes to the current XML file."),
		       _("Export listed picked nodes to a new XML file."));
  visu_ui_value_io_setSensitiveOpen(VISU_UI_VALUE_IO(valueIO), TRUE);
  visu_ui_value_io_connectOnOpen(VISU_UI_VALUE_IO(valueIO), onLoadXML);
  visu_ui_value_io_connectOnSave(VISU_UI_VALUE_IO(valueIO),
                                 visu_ui_interactive_pick_exportXMLFile);
  gtk_widget_show_all(valueIO);
  wd = lookup_widget(main->interactiveDialog, "hboxPick");
  gtk_box_pack_end(GTK_BOX(wd), valueIO, TRUE, TRUE, 10);

  /* Set the names and load the widgets. */
  comboDraw = lookup_widget(main->interactiveDialog, "comboboxShowInfos");
  gtk_combo_box_set_model(GTK_COMBO_BOX(comboDraw), GTK_TREE_MODEL(listComboInfos));
  gtk_cell_layout_clear(GTK_CELL_LAYOUT(comboDraw));
  renderer = gtk_cell_renderer_pixbuf_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(comboDraw), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(comboDraw), renderer, "stock-id",
				COLUMN_COMBO_STOCK);
  renderer = gtk_cell_renderer_text_new();
  gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(comboDraw), renderer, FALSE);
  gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(comboDraw), renderer, "markup",
				COLUMN_COMBO_LABEL);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboDraw), 0);
  comboInfosSignal = g_signal_connect(G_OBJECT(comboDraw), "changed",
				      G_CALLBACK(onComboInfosChanged), (gpointer)0);
  labelPickOut = lookup_widget(main->interactiveDialog, "pickInfo");
  labelPickHistory = gtk_label_new("");
  gtk_widget_show(labelPickHistory);
  gtk_box_pack_start(GTK_BOX(lookup_widget(main->interactiveDialog, "vbox24")),
                     labelPickHistory, FALSE, FALSE, 0);
  gtk_label_set_use_markup(GTK_LABEL(labelPickHistory), TRUE);
  gtk_label_set_selectable(GTK_LABEL(labelPickHistory), TRUE);
  gtk_misc_set_padding(GTK_MISC(labelPickHistory), 15, 0);
  labelPickError = lookup_widget(main->interactiveDialog, "pickComment");
  gtk_widget_set_name(labelPickError, "label_error");
  wd = lookup_widget(main->interactiveDialog, "viewportPick");
  gtk_widget_set_name(wd, "message_viewport");
  wd = lookup_widget(main->interactiveDialog, "checkDrawDistance");
  gtk_widget_set_name(wd, "message_radio");
  radioDrawNever = lookup_widget(main->interactiveDialog, "radioDrawNever");
  gtk_widget_set_name(radioDrawNever, "message_radio");
  radioInfosSignals[DRAW_NEVER] =
    g_signal_connect(G_OBJECT(radioDrawNever), "toggled",
		     G_CALLBACK(onRadioDrawInfos), GINT_TO_POINTER(DRAW_NEVER));
  radioDrawSelected = lookup_widget(main->interactiveDialog, "radioDrawSelected");
  gtk_widget_set_name(radioDrawSelected, "message_radio");
  radioInfosSignals[DRAW_SELECTED] =
    g_signal_connect(G_OBJECT(radioDrawSelected), "toggled",
		     G_CALLBACK(onRadioDrawInfos), GINT_TO_POINTER(DRAW_SELECTED));
  radioDrawAlways = lookup_widget(main->interactiveDialog, "radioDrawAlways");
  gtk_widget_set_name(radioDrawAlways, "message_radio");
  radioInfosSignals[DRAW_ALWAYS] =
    g_signal_connect(G_OBJECT(radioDrawAlways), "toggled",
		     G_CALLBACK(onRadioDrawInfos), GINT_TO_POINTER(DRAW_ALWAYS));

  wd = lookup_widget(main->interactiveDialog, "checkDrawDistance");
  g_signal_connect(G_OBJECT(wd), "toggled",
		   G_CALLBACK(onDrawDistanceChecked), (gpointer)0);
  wd = lookup_widget(main->interactiveDialog, "buttonEraseDistances");
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onEraseDistanceClicked), (gpointer)0);
  hboxMarks = lookup_widget(main->interactiveDialog, "hboxMarks");
  gtk_widget_set_sensitive(hboxMarks, FALSE);
  wd = lookup_widget(main->interactiveDialog, "buttonEraseMarks");
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onHighlightEraseClicked), (gpointer)0);
  wd = lookup_widget(main->interactiveDialog, "buttonSetMarks");
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onHighlightSetClicked), (gpointer)0);
  tglMarks = gtk_toggle_button_new();
  gtk_widget_set_tooltip_text(tglMarks, _("Hide nodes depending on highlight status."));
  image = create_pixmap((GtkWidget*)0, "stock-masking.png");
  gtk_container_add(GTK_CONTAINER(tglMarks), image);
  gtk_box_pack_start(GTK_BOX(hboxMarks), tglMarks, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(tglMarks), "toggled",
		   G_CALLBACK(onHighlightHideToggled), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxMarks), gtk_label_new("("), FALSE, FALSE, 0);
  wd = gtk_radio_button_new_with_mnemonic((GSList*)0, _("_h."));
  gtk_widget_set_tooltip_text(wd, _("Hide button will hide highlighted nodes."));
  gtk_widget_set_name(wd, "message_radio");
  gtk_box_pack_start(GTK_BOX(hboxMarks), wd, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(wd), "toggled",
		   G_CALLBACK(onHighlightHideStatus), GINT_TO_POINTER(HIDE_HIGHLIGHT));
  wd2 = gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(wd), _("_non-h."));
  gtk_widget_set_tooltip_text(wd2, _("Hide button will hide non-highlighted nodes."));
  gtk_widget_set_name(wd2, "message_radio");
  gtk_box_pack_start(GTK_BOX(hboxMarks), wd2, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(wd2), "toggled",
		   G_CALLBACK(onHighlightHideStatus), GINT_TO_POINTER(HIDE_NON_HIGHLIGHT));
  gtk_box_pack_start(GTK_BOX(hboxMarks), gtk_label_new(")"), FALSE, FALSE, 0);
  lblMarks = lookup_widget(main->interactiveDialog, "labelMarks");
  gtk_widget_show_all(hboxMarks);
  lblList = lookup_widget(main->interactiveDialog, "labelList");
  wd = lookup_widget(main->interactiveDialog, "notebookAction");

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReady), (gpointer)0);

  if (dataObj)
    {
      DBG_fprintf(stderr, "Gtk Pick: Initial setup.\n");
      visu_interactive_apply(interPick, VISU_NODE_ARRAY(dataObj));
      onDataReady((GObject*)0, dataObj, (VisuGlView*)0, (gpointer)0);
      tmpLst = visu_gl_ext_marks_getHighlightedList(marks);
      updateLabelMarks(tmpLst);
      g_list_free(tmpLst);
      onMeasurementList(marks, (gpointer)0);
    }

  return (GtkWidget*)0;
}
/**
 * visu_ui_interactive_pick_start:
 * @window: a #VisuUiRenderingWindow object.
 *
 * Initialise a pick session.
 */
void visu_ui_interactive_pick_start(VisuUiRenderingWindow *window)
{
  VisuInteractive *inter;

  visu_ui_rendering_window_pushInteractive(window, interPick);
  inter = visu_ui_rendering_window_class_getInteractive();
  visu_interactive_setReferences(interPick, inter);
}
/**
 * visu_ui_interactive_pick_stop:
 * @window: a #VisuUiRenderingWindow object.
 *
 * Finalise a pick session.
 */
void visu_ui_interactive_pick_stop(VisuUiRenderingWindow *window)
{
  VisuInteractive *inter;

  visu_ui_rendering_window_popInteractive(window, interPick);
  inter = visu_ui_rendering_window_class_getInteractive();
  visu_interactive_setReferences(inter, interPick);
}
static void onNodeSelection(VisuInteractive *inter _U_, VisuInteractivePick pick, 
			    VisuNode *node0, VisuNode *node1, VisuNode *node2,
                            gpointer data)
{
  gchar *errors;
  GtkTreeIter iter;
  VisuData *dataObj;
  VisuGlExtMarks *marks;
  VisuGlView *view;
  GString *infos;
  float posRef1[3], posRef2[3], posSelect[3];
  double dx, dy, dz, dr, dx1, dy1, dz1, dr1,  dx2, dy2, dz2, dr2;
  double ang;

  dataObj = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(data));
  g_return_if_fail(dataObj);

  DBG_fprintf(stderr, "Gtk Pick: one measurement done (%d).\n", pick);

  gtk_tree_selection_unselect_all(gtk_tree_view_get_selection
				  (GTK_TREE_VIEW(treeviewDataNode)));

  /* Update the texts. */
  infos  = g_string_new("");
  errors = (gchar*)0;
  if (pick == PICK_DISTANCE || pick == PICK_ANGLE ||
      pick == PICK_REFERENCE_1 || pick == PICK_REFERENCE_2)
    g_string_append_printf(infos,
			   _("Reference node\t"
			     " <span font_desc=\"monospace\"><b>#%d</b></span>\n"
			     "<span font_desc=\"monospace\" size=\"small\">"
			     "  ( x  = %7.3f ;  y  = %7.3f ;  z  = %7.3f)\n"
			     "</span>"),
			   node1->number + 1, node1->xyz[0],
			   node1->xyz[1],     node1->xyz[2]);
  if (pick == PICK_ANGLE || pick == PICK_REFERENCE_2)
    {
      visu_data_getNodePosition(dataObj, node1, posRef1);
      visu_data_getNodePosition(dataObj, node2, posRef2);
      dx = posRef2[0] - posRef1[0];
      dy = posRef2[1] - posRef1[1];
      dz = posRef2[2] - posRef1[2];
      dr = sqrt(dx*dx + dy*dy + dz*dz);
      g_string_append_printf(infos,
			     _("2nd Reference node\t"
			       " <span font_desc=\"monospace\"><b>#%d</b>\t"
			       " (<i>i.e.</i> dr = %7.3f)</span>\n"
			       "<span font_desc=\"monospace\" size=\"small\">"
			       "  ( x  = %7.3f ;  y  = %7.3f ;  z  = %7.3f)\n"
			       "  (\316\264x  = %7.3f ; \316\264y  = %7.3f ; \316\264z  = %7.3f)\n"
			       "</span>"),
			     node2->number + 1, dr, node2->xyz[0],
			     node2->xyz[1], node2->xyz[2], dx, dy, dz);
    }
  if (pick == PICK_SELECTED)
    g_string_append_printf(infos,
			   _("Newly picked node\t"
			     " <span font_desc=\"monospace\"><b>#%d</b></span>\n"
			     "<span font_desc=\"monospace\" size=\"small\">"
			     "  ( x  = %7.3f ;  y  = %7.3f ;  z  = %7.3f)\n"
			     "</span>"),
			   node0->number + 1, node0->xyz[0],
			   node0->xyz[1], node0->xyz[2]);
  else if (pick == PICK_DISTANCE)
    {
      visu_data_getNodePosition(dataObj, node0, posSelect);
      visu_data_getNodePosition(dataObj, node1, posRef1);
      dx = posSelect[0] - posRef1[0];
      dy = posSelect[1] - posRef1[1];
      dz = posSelect[2] - posRef1[2];
      dr = sqrt(dx*dx + dy*dy + dz*dz);
      g_string_append_printf(infos,
			     _("Newly picked node\t"
			       " <span font_desc=\"monospace\"><b>#%d</b>\t"
			       " (<i>i.e.</i> dr = %7.3f)</span>\n"
			       "<span font_desc=\"monospace\" size=\"small\">"
			       "  ( x  = %7.3f ;  y  = %7.3f ;  z  = %7.3f)\n"
			       "  (\316\264x  = %7.3f ; \316\264y  = %7.3f ; \316\264z  = %7.3f)\n"
			       "</span>"),
			     node0->number + 1, dr, node0->xyz[0],
			     node0->xyz[1], node0->xyz[2], dx, dy, dz);
    }
  else if (pick == PICK_ANGLE)
    {
      visu_data_getNodePosition(dataObj, node0, posSelect);
      visu_data_getNodePosition(dataObj, node1, posRef1);
      visu_data_getNodePosition(dataObj, node2, posRef2);
      dx1 = posSelect[0] - posRef1[0];
      dy1 = posSelect[1] - posRef1[1];
      dz1 = posSelect[2] - posRef1[2];
      dx2 = posRef2[0] - posRef1[0];
      dy2 = posRef2[1] - posRef1[1];
      dz2 = posRef2[2] - posRef1[2];
      dr1 = sqrt(dx1*dx1 + dy1*dy1 + dz1*dz1);
      g_string_append_printf(infos,
			     _("Newly picked node\t"
			       " <span font_desc=\"monospace\"><b>#%d</b>\t"
			       " (<i>i.e.</i> dr = %7.3f)</span>\n"
			       "<span font_desc=\"monospace\" size=\"small\">"
			       "  ( x  = %7.3f ;  y  = %7.3f ;  z  = %7.3f)\n"
			       "  (\316\264x1 = %7.3f ; \316\264y1 = %7.3f ; \316\264z1 = %7.3f)\n"
			       "  (\316\264x2 = %7.3f ; \316\264y2 = %7.3f ; \316\264z2 = %7.3f)\n"),
			     node0->number + 1, dr1, node0->xyz[0],
			     node0->xyz[1], node0->xyz[2],
			     dx1, dy1, dz1, dx2, dy2, dz2);
      dr2 = sqrt(dx2*dx2 + dy2*dy2 + dz2*dz2);
      ang = acos((dx2*dx1+dy2*dy1+dz2*dz1)/(dr2*dr1))/TOOL_PI180;
      g_string_append_printf(infos,
			     _("  angle (Ref-Ref2, Ref-New) = %5.2f degrees"
			       "</span>"), ang);
    }
  if (pick == PICK_UNREFERENCE_1 || pick == PICK_UNREFERENCE_2)
    errors = g_strdup_printf(_("Unset reference %d."),
			     (pick == PICK_UNREFERENCE_1)?1:2);

  /* Update the remaining of the interface. */
  marks = visu_ui_rendering_window_getMarks(VISU_UI_RENDERING_WINDOW(data));
  view = visu_ui_rendering_window_getGlView(VISU_UI_RENDERING_WINDOW(data));
  switch (pick)
    {
    case PICK_SELECTED:
    case PICK_DISTANCE:
    case PICK_ANGLE:
      /* Add clicked node to list. */
      getIterPick(node0->number, &iter);
      addNodeAtIter(dataObj, node0, &iter,
                    visu_gl_ext_marks_getHighlightStatus(marks, node0->number));

      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
	{
	  drawDataOnNode(dataObj, view, DRAW_SELECTED);
	  VISU_REDRAW_ADD;
	}
    case PICK_UNREFERENCE_1:
    case PICK_UNREFERENCE_2:
    case PICK_REFERENCE_1:
    case PICK_REFERENCE_2:
      if (infos)
	{
	  gtk_label_set_markup(GTK_LABEL(labelPickOut), infos->str);
	  g_string_free(infos, TRUE);
	}
      gtk_label_set_text(GTK_LABEL(labelPickError), errors);
      if (errors)
	g_free(errors);
      return;
    case PICK_HIGHLIGHT:
      getIterPick(node0->number, &iter);
      addNodeAtIter(dataObj, node0, &iter,
                    visu_gl_ext_marks_getHighlightStatus(marks, node0->number));
      return;
    case PICK_INFORMATION:
      break;
    default:
      g_warning("Not a pick event!");
    }
  return;
}
static void onRegionSelection(VisuInteractive *inter _U_, GList *nodes,
			      gpointer data)
{
  VisuData *dataObj;
  GtkTreeIter iter;
  VisuNodeArrayIter it;
  VisuGlExtMarks *marks;
  VisuGlView *view;

  dataObj = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(data));
  g_return_if_fail(dataObj);
  marks = visu_ui_rendering_window_getMarks(VISU_UI_RENDERING_WINDOW(data));
  view = visu_ui_rendering_window_getGlView(VISU_UI_RENDERING_WINDOW(data));

  /* We first empty the list. */
  /* 	  gtk_list_store_clear(listDataNode); */
  /* Update the visibility of valueIO. */
  visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), FALSE);
  /* We add the new elements. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &it);
  for (visu_node_array_iterStartList(VISU_NODE_ARRAY(dataObj), &it, nodes), it.lst = (GList*)0;
       it.node; visu_node_array_iterNextList(VISU_NODE_ARRAY(dataObj), &it))
    {
      getIterPick(it.node->number, &iter);
      addNodeAtIter(dataObj, it.node, &iter,
                    visu_gl_ext_marks_getHighlightStatus(marks, it.node->number));
    }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
    {
      drawDataOnNode(dataObj, view, DRAW_SELECTED);
      VISU_REDRAW_ADD;
    }
  return;
}
static void onSelectionError(VisuInteractive *inter _U_,
			     VisuInteractivePickError error, gpointer data _U_)
{
  switch (error)
    {
    case PICK_ERROR_NO_SELECTION:
      gtk_label_set_text(GTK_LABEL(labelPickError), _("No node has been selected."));
      return;
    case PICK_ERROR_SAME_REF:
      gtk_label_set_text(GTK_LABEL(labelPickError), _("Picked reference and second"
						      " reference are the same."));
      return;
    case PICK_ERROR_REF1:
      gtk_label_set_text(GTK_LABEL(labelPickError), _("Can't pick a second reference"
						      " without any existing first one."));
      return;
    case PICK_ERROR_REF2:
      gtk_label_set_text(GTK_LABEL(labelPickError), _("Can't remove first reference"
						      " before removing the second one."));
      return;
    default:
      return;
    }
}
static void onClickStop(VisuInteractive *inter _U_, gpointer data _U_)
{
  visu_ui_interactive_toggle();
}
/**
 * visu_ui_interactive_pick_getNodeSelection:
 *
 * Compute a list of #VisuNode currently listed.
 *
 * Returns: (transfer container) (element-type guint): a newly created
 * list. Should be freed with g_list_free() after use.
 */
GList* visu_ui_interactive_pick_getNodeSelection()
{
  GList *lst;
  gboolean validIter;
  GtkTreeIter iter;
  guint currentNodeId;

  lst = (GList*)0;
  validIter = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
  while (validIter)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
			 COLUMN_BASE_NUMBER, &currentNodeId, -1);
      lst = g_list_prepend(lst, GINT_TO_POINTER((int)(currentNodeId - 1)));
      validIter = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter);
    }
  return lst;
}


/*********************/
/* Private routines. */
/*********************/
static gboolean getIterPick(guint nodeId, GtkTreeIter *iter)
{
  gboolean validIter, found;
  guint currentNodeId;

  g_return_val_if_fail(iter, FALSE);

  /* Search if @node is already in the tree. */
  found = FALSE;
  validIter = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), iter);
  while (validIter)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), iter,
			 COLUMN_BASE_NUMBER, &currentNodeId, -1);
      if (nodeId + 1 == currentNodeId)
	{
	  found = TRUE;
	  validIter = FALSE;
	}
      else
	validIter = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), iter);
    }
  if (!found)
    {
      gtk_list_store_append(listDataNode, iter);
      updateLabelList();
    }
  return !found;
}
static void populateComboInfos(VisuData *dataObj)
{
  gchar *markup;
  const gchar *title;
  GtkTreeIter iter;
  GList *tmpLst;
  gint i, n;
  gboolean visibility, set;

  DBG_fprintf(stderr, "Gtk Pick: rebuild the data combo list.\n");

  n = gtk_combo_box_get_active(GTK_COMBO_BOX(comboDraw));
  DBG_fprintf(stderr, "Gtk Pick: combo list previously %d.\n", n);

  g_signal_handler_block(G_OBJECT(comboDraw), comboInfosSignal);

  gtk_list_store_clear(listComboInfos);

  title = _("Id");
  markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);
  gtk_list_store_append(listComboInfos, &iter);
  gtk_list_store_set(listComboInfos, &iter,
		     COLUMN_COMBO_LABEL, markup,
		     COLUMN_COMBO_POINTER, GINT_TO_POINTER(1),
		     -1);
  DBG_fprintf(stderr, " | add '%s'\n", title);
  g_free(markup);

  title = _("Type");
  markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);
  gtk_list_store_append(listComboInfos, &iter);
  gtk_list_store_set(listComboInfos, &iter,
		     COLUMN_COMBO_LABEL, markup,
		     COLUMN_COMBO_POINTER, GINT_TO_POINTER(2),
		     -1);
  DBG_fprintf(stderr, " | add '%s'\n", title);
  g_free(markup);

  i = 0;
  for (tmpLst = visu_data_node_class_getAll(); tmpLst; tmpLst = g_list_next(tmpLst))
    {
      visibility = visu_data_node_getUsed(VISU_DATA_NODE(tmpLst->data), dataObj);
      DBG_fprintf(stderr, " | col %d -> %d (%p)\n", i, visibility, (gpointer)dataCols[i]);
      if (visibility)
	{
	  title = visu_data_node_getLabel(VISU_DATA_NODE(tmpLst->data));
	  markup = g_markup_printf_escaped("<span size=\"smaller\">%s</span>", title);

	  gtk_list_store_append(listComboInfos, &iter);
	  gtk_list_store_set(listComboInfos, &iter,
			     COLUMN_COMBO_LABEL, markup,
			     COLUMN_COMBO_POINTER, tmpLst->data,
			     -1);
	  if (visu_data_node_getEditable(VISU_DATA_NODE(tmpLst->data)))
	    gtk_list_store_set(listComboInfos, &iter,
			       COLUMN_COMBO_STOCK, GTK_STOCK_EDIT,
			       -1);
	  DBG_fprintf(stderr, " | add '%s'\n", title);
	  g_free(markup);
	}
      /* Update the column visibility. */
      gtk_tree_view_column_set_visible(dataCols[i], visibility);
      i += 1;
    }
  g_signal_handler_unblock(G_OBJECT(comboDraw), comboInfosSignal);

  set = (n >= 0 && n < gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listComboInfos),
						      (GtkTreeIter*)0));
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboDraw), (set)?n:0);

  DBG_fprintf(stderr, "Gtk Pick: combo list is now %d.\n", (set)?n:0);
}
static void updateLabelMarks(GList *hl)
{
  gboolean st;
  gchar *str;
  guint nbMarks;
  
  DBG_fprintf(stderr, "Gtk Pick: Update marks related widgets.\n");
  nbMarks = g_list_length(hl);

  st = (nbMarks > 0);
  /* Update the visibility of highlight buttons. */
  gtk_widget_set_sensitive(hboxMarks, st);
  if (!st)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tglMarks), FALSE);

  if (st)
    str = g_strdup_printf(_("Highlights <span size=\"small\">(%d)</span>:"), nbMarks);
  else
    str = g_strdup(_("Highlights <span size=\"small\">(none)</span>:"));
  gtk_label_set_markup(GTK_LABEL(lblMarks), str);
  g_free(str);
}
static void updateLabelList()
{
  gchar *str;
  gint n;

  n = gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listDataNode), NULL);
  if (n > 0)
    str = g_strdup_printf(_("<b>List of nodes <span size="
			    "\"small\">(%d)</span>:</b>"), n);
  else
    str = g_strdup(_("<b>List of nodes <span size="
		     "\"small\">(none)</span>:</b>"));
  gtk_label_set_markup(GTK_LABEL(lblList), str);
  g_free(str);
}
static void addNodeAtIter(VisuData *dataObj, VisuNode *node, GtkTreeIter *iter,
			  gboolean highlight)
{
  GList *tmpLst;
  gchar *values, *label, *color;
  int i;
  gboolean editable;
  GtkTreePath *path;
  VisuElement *ele;

  /* Store the base data. */
  ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);
  gtk_list_store_set(listDataNode, iter,
		     COLUMN_BASE_NUMBER, node->number + 1,
		     COLUMN_BASE_ELEMENT, ele->name,
		     COLUMN_BASE_HIGHLIGHT, highlight,
		     -1);

  /* Store the additional data informations. */
  i = NB_COLUMN_BASE;
  for (tmpLst = visu_data_node_class_getAll(); tmpLst; tmpLst = g_list_next(tmpLst))
    {
      editable = visu_data_node_getEditable((VisuDataNode*)tmpLst->data);
      label = visu_data_node_getValueAsString((VisuDataNode*)tmpLst->data,
					dataObj, node);
      if (!label)
	{
	  values = _("<i>None</i>");
	  editable = FALSE;
	}
      else if (label[0] == '\0')
	values = _("<i>None</i>");
      else
	values = label;
      if (editable)
	color = GTK_PICK_EDITABLE_NODE;
      else
	color = GTK_PICK_UNEDITABLE_NODE;
      gtk_list_store_set(listDataNode, iter,
			 i + COLUMN_DATA_LABEL, values,
			 i + COLUMN_DATA_EDITABLE, editable,
			 i + COLUMN_DATA_COLOR, color, -1);
      if (label)
	g_free(label);
      i += NB_COLUMN_DATA;
    }
  /* Update the selection. */
  gtk_tree_selection_select_iter(gtk_tree_view_get_selection
				 (GTK_TREE_VIEW(treeviewDataNode)), iter);
  path = gtk_tree_model_get_path(GTK_TREE_MODEL(listDataNode), iter);
  gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(treeviewDataNode), path,
			       NULL, FALSE, 0., 0.);
  gtk_tree_path_free(path);

  /* Update the visibility of valueIO. */
  visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), TRUE);
}
static int* getListedNodes()
{
  GtkTreeIter iter;
  gboolean valid;
  int *nodes, i;

  DBG_fprintf(stderr, "Gtk Pick: get the list of node ids:\n");
  if (!listDataNode)
    {
      nodes = g_malloc(sizeof(int) * 1);
      nodes[0] = -1;
      return nodes;
    }

  nodes = g_malloc(sizeof(int) * 
		   (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listDataNode),
						   (GtkTreeIter*)0) + 1));
  i = 0;
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
			 COLUMN_BASE_NUMBER, nodes + i,
			 -1);
      nodes[i] -= 1;
      DBG_fprintf(stderr, " | id %d.\n", nodes[i]);
      i += 1;
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter);
    }
  nodes[i] = -1;

  return nodes;
}

static void drawDataOnNode(VisuData *data, VisuGlView *view, VisuGlExtInfosDrawId item)
{
  GtkTreeIter iter;
  gpointer infos;
  gboolean valid;
  int *nodes;
  VisuGlExtInfosDrawMethod method;
  VisuDataNode *dt;

  /* We get what data to draw. */
  valid = gtk_combo_box_get_active_iter(GTK_COMBO_BOX(comboDraw), &iter);
  g_return_if_fail(valid);

  gtk_tree_model_get(GTK_TREE_MODEL(listComboInfos), &iter,
		     COLUMN_COMBO_POINTER, &infos,
		     -1);

  /* We build the list of elements to be drawn in DRAW_SELECTED mode. */
  if (item == DRAW_SELECTED)
    nodes = getListedNodes();
  else
    nodes = (int*)0;
  if (GPOINTER_TO_INT(infos) > 2)
    dt = VISU_DATA_NODE(infos);
  else
    dt = (VisuDataNode*)0;
  if (item == DRAW_NEVER)
    method = EXT_DRAW_METH_NONE;
  else
    {
      switch (GPOINTER_TO_INT(infos))
	{
	case 1:
	  method = EXT_DRAW_METH_ID; break;
	case 2:
	  method = EXT_DRAW_METH_TYPE; break;
	default:
	  method = EXT_DRAW_METH_OTHER; break;
	}
    }
  visu_basic_setExtInfos(data, view, method, nodes, dt);
}

/*************/
/* Callbacks */
/*************/
static void onEditedPick(GtkCellRendererText *cellrenderertext _U_,
			 gchar *path, gchar *text, gpointer user_data)
{
  gboolean valid;
  GtkTreeIter iter;
  gchar *label, *values;
  GList *lst;
  VisuDataNode *data;
  VisuData *dataObj;
  gint number;

  valid = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(listDataNode),
						  &iter, path);
  if (!valid)
    {
      g_warning("Wrong 'path' argument for 'onEditedPick'.");
      return;
    }

  gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
		     COLUMN_BASE_NUMBER, &number, -1);

  DBG_fprintf(stderr, "Gtk Pick: edited value on the fly.\n");
  lst = visu_data_node_class_getAll();
  data = (VisuDataNode*)g_list_nth_data(lst, GPOINTER_TO_INT(user_data));
  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  valid = visu_data_node_setValueAsString
    (data, dataObj,visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), number - 1), text, &label);
  if (!valid)
    visu_ui_raiseWarning(_("Reading values"),
			 _("Wrong format. Impossible to parse the data associated"
			   " to the selected node."), (GtkWindow*)0);
  /* Change the value of the text in the list store. */
  if (label[0] == '\0')
    values = _("<i>None</i>");
  else
    values = label;
  gtk_list_store_set(listDataNode, &iter,
		     GPOINTER_TO_INT(user_data) * NB_COLUMN_DATA + NB_COLUMN_BASE,
		     values, -1);
  g_free(label);

  visu_data_node_emitValueChanged(data, dataObj);
  VISU_REDRAW_ADD;
}
static void highlightChange(GtkTreeIter *iter, VisuGlExtMarksStatus status)
{
  GtkTreeIter iter_;
  gboolean valid, hl, only;
  guint node;
  VisuData *current;
  VisuGlExtMarks *marks;
  GList *lst;

  only = (iter != (GtkTreeIter*)0);
  if (!iter)
    iter = &iter_;
  
  DBG_fprintf(stderr, "Gtk Pick: change highlight status.\n");
  current = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());
  lst = (GList*)0;
  for (valid = (!only)?gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter_):TRUE;
       valid; valid = (!only)?gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter_):FALSE)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), iter,
			 COLUMN_BASE_HIGHLIGHT, &hl,
			 COLUMN_BASE_NUMBER, &node,
			 -1);
      DBG_fprintf(stderr, " | process %d\n", node - 1);
      if (status == MARKS_STATUS_TOGGLE)
	gtk_list_store_set(listDataNode, iter,
			   COLUMN_BASE_HIGHLIGHT, !hl,
			   -1);
      else if (status == MARKS_STATUS_SET)
	gtk_list_store_set(listDataNode, iter,
			   COLUMN_BASE_HIGHLIGHT, TRUE,
			   -1);
      else if (status == MARKS_STATUS_UNSET)
	gtk_list_store_set(listDataNode, iter,
			   COLUMN_BASE_HIGHLIGHT, FALSE,
			   -1);
      lst = g_list_prepend(lst, GINT_TO_POINTER(node - 1));
    }
  visu_gl_ext_marks_setHighlightedList(marks, lst, status);
  g_list_free(lst);
  
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tglMarks)))
    applyHidingScheme(current);
}
static void onMeasurementList(VisuGlExtMarks *marks, gpointer data _U_)
{
  gchar *lbl;
  VisuData *dataObj;
  struct _PickHistory *hist;
  GString *str;
  GList *lst, *rev;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  if (!dataObj)
    return;
  lbl = visu_gl_ext_marks_getMeasurementStrings(marks, dataObj);
  if (!lbl)
    return;

  if (!pickHistory || ((struct _PickHistory*)pickHistory->data)->dataObj != dataObj)
    {
      hist = g_malloc(sizeof(struct _PickHistory));
      hist->dataObj = dataObj;
      pickHistory = g_list_prepend(pickHistory, (gpointer)hist);
    }
  else
    {
      hist = (struct _PickHistory*)pickHistory->data;
      g_free(hist->str);
    }
  hist->str = lbl;

  if (labelPickHistory)
    {
      lbl = visu_gl_ext_marks_getMeasurementLabels(marks);
      str = g_string_new(lbl);
      g_free(lbl);
      rev = g_list_reverse(pickHistory);
      for (lst = rev; lst; lst = g_list_next(lst))
        g_string_append(str, ((struct _PickHistory*)lst->data)->str);
      pickHistory = g_list_reverse(rev);
      lbl = g_markup_printf_escaped
        ("Measurement history, first 6 values (<b>%d entry(ies)</b>):\n"
         "<span font_desc=\"monospace\" size=\"small\">%s</span>",
         g_list_length(pickHistory), str->str);
      g_string_free(str, TRUE);
      gtk_label_set_markup(GTK_LABEL(labelPickHistory), lbl);
      g_free(lbl);
    }
}
static void onHighlightList(VisuGlExtMarks *marks _U_, GList *lst, gpointer data _U_)
{
  DBG_fprintf(stderr, "Gtk Pick: caught highlight list change (%d).\n",
              g_list_length(lst));

  updateLabelMarks(lst);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tglMarks)))
    {
      data = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
      applyHidingScheme(data);
    }
}
static void onHighlightClicked(GtkButton *button _U_, gpointer user_data)
{
  highlightChange((GtkTreeIter*)0,
		  GPOINTER_TO_INT(user_data)?MARKS_STATUS_SET:MARKS_STATUS_UNSET);

  VISU_REDRAW_ADD;
}
static void onHighlightToggled(GtkCellRendererToggle *cell_renderer  _U_,
			       gchar *path, gpointer user_data _U_)
{
  gboolean valid;
  GtkTreeIter iter;

  valid = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(listDataNode),
					      &iter, path);
  g_return_if_fail(valid);

  highlightChange(&iter, MARKS_STATUS_TOGGLE);

  VISU_REDRAW_ADD;
}
static void onHighlightSetClicked(GtkButton *button _U_, gpointer user_data _U_)
{
  GList *lst, *tmpLst;
  VisuUiRenderingWindow *window;
  VisuData *current;
  VisuGlExtMarks *marks;
  VisuGlView *view;
  VisuNode *node;
  GtkTreeIter iter;

  window = visu_ui_main_class_getDefaultRendering();
  current = visu_ui_rendering_window_getData(window);
  marks = visu_ui_rendering_window_getMarks(window);
  view = visu_ui_rendering_window_getGlView(window);

  lst = visu_gl_ext_marks_getHighlightedList(marks);
  for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      node = visu_node_array_getFromId(VISU_NODE_ARRAY(current), GPOINTER_TO_INT(tmpLst->data));
      if (node)
	{
	  getIterPick(node->number, &iter);
	  addNodeAtIter(current, node, &iter, TRUE);
	}
    }
  if (lst &&
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
    {
      drawDataOnNode(current, view, DRAW_SELECTED);
      VISU_REDRAW_ADD;
    }
  g_list_free(lst);
}
static gboolean applyHidingScheme(VisuData *data)
{
  gboolean redraw;

  g_signal_emit_by_name(G_OBJECT(data), "AskForShowHide", &redraw, NULL);
  if (redraw)
    g_signal_emit_by_name(G_OBJECT(data), "VisibilityChanged", NULL);

  return redraw;
}
static void onHighlightHideToggled(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuData *data;

  data = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  if (applyHidingScheme(data))
    VISU_REDRAW_ADD;
}
static void onHighlightHideStatus(GtkButton *button, gpointer user_data)
{
  VisuData *data;

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(button)) ||
      !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tglMarks)))
    return;

  g_object_set_data(G_OBJECT(tglMarks), "hide-status", user_data);
  data = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  if (applyHidingScheme(data))
    VISU_REDRAW_ADD;
}
static void onNodePropertyUsed(VisuDataNode* data _U_, VisuData *dataObj,
			       gpointer user_data)
{
  VisuData *current;

  current = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(user_data));
  DBG_fprintf(stderr, "Gtk Pick: caught 'PropertyUsed' for %p (current %p).\n",
	      (gpointer)dataObj, (gpointer)current);
  if (current == dataObj)
    /* We update the combo for properties. */
    populateComboInfos(dataObj);
}
static void onNodePropertyUnused(VisuDataNode* data _U_, VisuData *dataObj,
				 gpointer user_data)
{
  VisuData *current;

  current = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(user_data));
  DBG_fprintf(stderr, "Gtk Pick: caught 'PropertyUnused' for %p (current %p).\n",
	      (gpointer)dataObj, (gpointer)current);
  if (current == dataObj)
    /* We update the combo for properties. */
    populateComboInfos(dataObj);
}
static void onNodePropertyChanged(VisuDataNode* data, VisuData *dataObj,
				  gpointer user_data)
{
  GtkTreeIter iter;
  gboolean valid;
  int i, nodeId;
  GList *tmpLst;
  gchar *label, *values;
  VisuNode *node;
  VisuData *current;

  current = visu_ui_rendering_window_getData(VISU_UI_RENDERING_WINDOW(user_data));
  DBG_fprintf(stderr, "Gtk Pick: caught 'valueChanged' for %p (current %p).\n",
	      (gpointer)dataObj, (gpointer)current);
  if (current != dataObj)
    return;

  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
			 COLUMN_BASE_NUMBER, &nodeId, -1);
      node = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), nodeId - 1);
      g_return_if_fail(node);

      /* Store the additional data informations. */
      tmpLst = visu_data_node_class_getAll();
      i = NB_COLUMN_BASE + NB_COLUMN_DATA * g_list_index(tmpLst, data);
      label = visu_data_node_getValueAsString(data, dataObj, node);
      if (!label || label[0] == '\0')
	values = _("<i>None</i>");
      else
	values = label;
      gtk_list_store_set(listDataNode, &iter,
			 i + COLUMN_DATA_LABEL, values, -1);
      if (label)
	g_free(label);

      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter);
    }
}
static void onComboInfosChanged(GtkComboBox *combo, gpointer data _U_)
{
  VisuData *dataObj;
  VisuGlView *view;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  g_return_if_fail(dataObj);
  view = visu_ui_rendering_window_getGlView(visu_ui_main_class_getDefaultRendering());

  if (gtk_combo_box_get_active(combo) >= 0)
    {
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawNever)))
	return;
      else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
	drawDataOnNode(dataObj, view, DRAW_SELECTED);
      else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawAlways)))
	drawDataOnNode(dataObj, view, DRAW_ALWAYS);
    }
  else
    drawDataOnNode(dataObj, view, DRAW_NEVER);
  VISU_REDRAW_ADD;
}
static void onRadioDrawInfos(GtkToggleButton *togglebutton, gpointer user_data)
{
  VisuData *data;
  VisuGlView *view;

  if (!gtk_toggle_button_get_active(togglebutton))
    return;

  data = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
  g_return_if_fail(data);
  view = visu_ui_rendering_window_getGlView(visu_ui_main_class_getDefaultRendering());

  drawDataOnNode(data, view, GPOINTER_TO_INT(user_data));
  VISU_REDRAW_ADD;
}
static void onDrawDistanceChecked(GtkToggleButton* button, gpointer data _U_)
{
  VisuGlExtMarks *marks;

  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());
  visu_gl_ext_marks_setDrawValues(marks, gtk_toggle_button_get_active(button));
}
static void onEraseDistanceClicked(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuGlExtMarks *marks;

  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());
  DBG_fprintf(stderr, "Gtk Pick: clicked on 'erase all measures' button.\n");
  if (visu_gl_ext_marks_removeMeasures(marks, -1))
    VISU_REDRAW_FORCE;
}
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw, gpointer data _U_)
{
  VisuUiRenderingWindow *window;
  VisuGlExtMarks *marks;
  VisuData *dataObj;
  GList *lst, *tmpLst;
  guint nHl, *hl, j;
  guint min, max;
  VisuNodeArrayIter iter;
  gboolean hide;
  gint status;

  DBG_fprintf(stderr, "Gtk Pick: caught the 'AskForShowHide' signal for"
	      " VisuData %p.\n", (gpointer)visuData);

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(tglMarks)))
    {
      status = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(tglMarks), "hide-status"));

      window = visu_ui_main_class_getDefaultRendering();
      dataObj = visu_ui_rendering_window_getData(window);
      marks = visu_ui_rendering_window_getMarks(window);
      lst = visu_gl_ext_marks_getHighlightedList(marks);

      if (status == HIDE_NON_HIGHLIGHT)
        {
          /* We switch off all nodes except the highlighted ones. To avoid
             to run the list of highlighted nodes to many times, we create
             a temporary array and store the min and max indexes. */
          nHl = g_list_length(lst);
          if (nHl == 0)
            return;

          visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);

          hl = g_malloc(sizeof(gint) * nHl);
          nHl = 0;
          min = iter.nAllStoredNodes;
          max = 0;
          for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
            {
              hl[nHl] = (guint)GPOINTER_TO_INT(tmpLst->data);
              min = MIN(min, hl[nHl]);
              max = MAX(max, hl[nHl]);
              nHl += 1;
            }

          for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
               visu_node_array_iterNext(VISU_NODE_ARRAY(dataObj), &iter))
            if (iter.node->number >= min && iter.node->number <= max)
              {
                hide = TRUE;
                for (j = 0; hide && j < nHl; j++)
                  hide = (iter.node->number != hl[j]);
                if (hide)
                  *redraw = visu_node_setVisibility(iter.node, FALSE) || *redraw;
              }
            else
              *redraw = visu_node_setVisibility(iter.node, FALSE) || *redraw;

          g_free(hl);
        }
      else
        for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
          {
            *redraw = visu_node_setVisibility
              (visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj),
                                           (guint)GPOINTER_TO_INT(tmpLst->data)),
               FALSE) || *redraw;
          }
    }
}
static void onDataReady(GObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data _U_)
{
  GtkTreeIter iter, removedIter;
  gboolean valid, removed;
  int number;
  VisuNode *node;

  DBG_fprintf(stderr, "Gtk Pick: caught 'dataRendered'"
	      " signal for %p.\n", (gpointer)dataObj);

  if (dataObj)
    {
      popDec_signal = g_signal_connect(G_OBJECT(dataObj), "PopulationDecrease",
                                       G_CALLBACK(onNodeRemoved), (gpointer)0);
      hide_signal = g_signal_connect(G_OBJECT(dataObj), "AskForShowHide",
                                     G_CALLBACK(onAskForHideNodes), (gpointer)0);

      /* Try to match the selected nodes to new ones in the new visuData. */
      valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
      while (valid)
	{
	  removed = FALSE;
	  gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
			     COLUMN_BASE_NUMBER, &number,
			     -1);
	  node = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), number - 1);
	  if (node)
	    addNodeAtIter(dataObj, node, &iter, FALSE);
	  else
	    {
	      removed = TRUE;
	      removedIter = iter;
	    }
          DBG_fprintf(stderr, " | %d -> %p\n", number - 1, (gpointer)node);

	  valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter);
	  if (removed)
	    gtk_list_store_remove(listDataNode, &removedIter);
	}

      /* Rebuild the combo of properties and update drawing. */
      populateComboInfos(dataObj);
    }
  else
    {
      /* Clear the node data. */  
      gtk_list_store_clear(listDataNode);
      /* Update the visibility of valueIO. */
      visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), FALSE);
      /* Clear the combobox of properties. */
      g_signal_handler_block(G_OBJECT(comboDraw), comboInfosSignal);
      gtk_list_store_clear(listComboInfos);
      g_signal_handler_unblock(G_OBJECT(comboDraw), comboInfosSignal);
    }
  updateLabelList();

  DBG_fprintf(stderr, "Gtk Pick: Done\n");

}
static void onDataNotReady(GObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Gtk Pick: caught the 'dataUnRendered' signal.\n");
  g_signal_handler_disconnect(G_OBJECT(dataObj), popDec_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), hide_signal);
}
static void onNodeRemoved(VisuData *visuData _U_, int *nodeNumbers, gpointer data _U_)
{
  GtkTreeIter iter, removeIter;
  gboolean valid;
  int number, i;
  gboolean found;

  DBG_fprintf(stderr, "Gtk Pick: caught the 'PopulationDecrease' signal.\n");

  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listDataNode), &iter,
			 0, &number,
			 -1);
      found = FALSE;
      for (i = 0; !found && nodeNumbers[i] >= 0; i++)
	if (number == (nodeNumbers[i] + 1))
	  {
	    removeIter = iter;
	    found = TRUE;
	  }
			 
      valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter);
      if (found)
	gtk_list_store_remove(listDataNode, &removeIter);
    };
  updateLabelList();
}
static gboolean onTreeviewInfosKey(GtkWidget *widget, GdkEventKey *event,
				   gpointer user_data _U_)
{
  VisuData *data;
  VisuGlView *view;
  GList *selectedPaths, *tmpLst;
  GtkTreeIter iter, *iterCpy;
  GtkTreeModel *model;
  gboolean valid;

  DBG_fprintf(stderr, "Gtk Pick: key pressed on treeview '%d'.\n", event->keyval);
  
  if (event->keyval != GDK_KEY_Delete && event->keyval != GDK_KEY_BackSpace)
    return FALSE;

  selectedPaths = gtk_tree_selection_get_selected_rows
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(widget)), &model);

  /* Transform all paths to iters in the list. */
  DBG_fprintf(stderr, "Gtk Pick: possible removing.\n");
  tmpLst = selectedPaths;
  while (tmpLst)
    {
      DBG_fprintf(stderr, " | remove path '%s'.\n",
		  gtk_tree_path_to_string((GtkTreePath*)tmpLst->data));

      valid = gtk_tree_model_get_iter(model, &iter, (GtkTreePath*)tmpLst->data);
      g_return_val_if_fail(valid, FALSE);

      gtk_tree_path_free((GtkTreePath*)tmpLst->data);
      iterCpy = gtk_tree_iter_copy(&iter);
      /* Replace the path with the tree iter. */
      tmpLst->data = (gpointer)iterCpy;

      tmpLst = g_list_next(tmpLst);
    };
  /* Then remove all iters. */
  DBG_fprintf(stderr, "Gtk Pick: actualy remove the iters.\n");
  tmpLst = selectedPaths;
  while (tmpLst && tmpLst->next)
    {
      gtk_list_store_remove(listDataNode, (GtkTreeIter*)tmpLst->data);
      gtk_tree_iter_free((GtkTreeIter*)tmpLst->data);
      tmpLst = g_list_next(tmpLst);
    };
  if (tmpLst)
    {
      gtk_list_store_remove(listDataNode, (GtkTreeIter*)tmpLst->data);
      gtk_tree_iter_free((GtkTreeIter*)tmpLst->data);
    }
  g_list_free(selectedPaths);
  updateLabelList();
  
  /* If the draw informations is on selected nodes, we need to redraw. */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
    {
      DBG_fprintf(stderr, "Gtk Pick: ask for redraw.\n");
      data = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());
      g_return_val_if_fail(data, FALSE);
      view = visu_ui_rendering_window_getGlView(visu_ui_main_class_getDefaultRendering());

      drawDataOnNode(data, view, DRAW_SELECTED);

      VISU_REDRAW_ADD;
    }
  return TRUE;
}
static void onHighlightEraseClicked(GtkButton *button _U_, gpointer user_data _U_)
{
  VisuGlExtMarks *marks;
  gboolean valid, redraw;
  GtkTreeIter iter;
  GList *lst;

  DBG_fprintf(stderr, "Gtk Pick: remove all highlights.\n");

  /* Remove all the check box in the model. */
  for (valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listDataNode), &iter);
       valid; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listDataNode), &iter))
    gtk_list_store_set(listDataNode, &iter, COLUMN_BASE_HIGHLIGHT, FALSE, -1);

  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());

  lst = visu_gl_ext_marks_getHighlightedList(marks);
  redraw = visu_gl_ext_marks_setHighlightedList(marks, lst, MARKS_STATUS_UNSET);
  g_list_free(lst);

  if (redraw)
    VISU_REDRAW_ADD;
}

static gboolean onLoadXML(const gchar *filename, GError **error)
{
  VisuData *dataObj;

  dataObj = visu_ui_rendering_window_getData(visu_ui_main_class_getDefaultRendering());

  if (visu_ui_interactive_pick_parseXMLFile(filename, dataObj, error))
    {
      /* Update the sensitivity. */
      visu_ui_value_io_setSensitiveSave(VISU_UI_VALUE_IO(valueIO), TRUE);
      /* We ask for redraw. */
      VISU_REDRAW_ADD;
      return TRUE;
    }
  else
    return FALSE;
}
/**
 * visu_ui_interactive_pick_parseXMLFile:
 * @filename: a filename ;
 * @data: a #VisuData object to take the pick information from ;
 * @error: a location to store possible errors.
 *
 * Parse the given V_Sim value file and update the dialog accordingly.
 *
 * Returns: TRUE if no error.
 */
gboolean visu_ui_interactive_pick_parseXMLFile(const gchar* filename, VisuData *data, GError **error)
{
  gboolean set, highlight;
  GList *list, *tmpLst;
  VisuNode *node;
  VisuGlExtInfosDrawId mode;
  guint info;
  VisuGlExtMarks *marks;
  VisuGlView *view;
  GtkTreeIter iter;

  view = visu_ui_rendering_window_getGlView(visu_ui_main_class_getDefaultRendering());
  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());
  set = visu_gl_ext_marks_parseXMLFile(marks, filename, &list, &mode, &info, error);
  if (!set)
    return FALSE;

  /* We stop the callbacks during upgrade. */
  g_signal_handler_block(G_OBJECT(radioDrawNever), radioInfosSignals[DRAW_NEVER]);
  g_signal_handler_block(G_OBJECT(radioDrawSelected),
			 radioInfosSignals[DRAW_SELECTED]);
  g_signal_handler_block(G_OBJECT(radioDrawAlways), radioInfosSignals[DRAW_ALWAYS]);
  g_signal_handler_block(G_OBJECT(comboDraw), comboInfosSignal);

  /* We set the general options. */
  DBG_fprintf(stderr, "Gtk Pick: set the general options (%d %d).\n", mode, info);
  switch (mode)
    {
    case DRAW_NEVER:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioDrawNever), TRUE);
      break;
    case DRAW_SELECTED:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioDrawSelected), TRUE);
      break;
    case DRAW_ALWAYS:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioDrawAlways), TRUE);
      break;
    }
  set = (info < (guint)gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listComboInfos),
						      (GtkTreeIter*)0));
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboDraw), (set)?info:0);

  /* Convert the list to new pick. */
  DBG_fprintf(stderr, "Pick parser: create %d new pick for %p.\n",
	      g_list_length(list), (gpointer)data);
  for (tmpLst = list; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      highlight = FALSE;
      if (GPOINTER_TO_INT(tmpLst->data) == PICK_SELECTED)
	tmpLst = g_list_next(tmpLst);
      else if (GPOINTER_TO_INT(tmpLst->data) == PICK_HIGHLIGHT)
	{
	  tmpLst = g_list_next(tmpLst);
	  highlight = TRUE;
	}
      else if (GPOINTER_TO_INT(tmpLst->data) == PICK_DISTANCE)
	{
	  tmpLst = g_list_next(tmpLst);
	  /* We add the last selection. */
	  tmpLst = g_list_next(tmpLst);
	}
      else if (GPOINTER_TO_INT(tmpLst->data) == PICK_ANGLE)
	{
	  tmpLst = g_list_next(tmpLst);
	  tmpLst = g_list_next(tmpLst);
	  /* We add the last selection. */
	  tmpLst = g_list_next(tmpLst);
	}
      node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), GPOINTER_TO_INT(tmpLst->data) - 1);
      if (node)
	{
	  if (getIterPick(node->number, &iter) || highlight)
	    addNodeAtIter(data, node, &iter, highlight);
	}
    }
  g_list_free(list);

  /* We reset the callbacks during upgrade. */
  g_signal_handler_unblock(G_OBJECT(radioDrawNever), radioInfosSignals[DRAW_NEVER]);
  g_signal_handler_unblock(G_OBJECT(radioDrawSelected),
			   radioInfosSignals[DRAW_SELECTED]);
  g_signal_handler_unblock(G_OBJECT(radioDrawAlways), radioInfosSignals[DRAW_ALWAYS]);
  g_signal_handler_unblock(G_OBJECT(comboDraw), comboInfosSignal);

  /* We update the list. */
  drawDataOnNode(data, view, mode);

  return TRUE;
}

/**
 * visu_ui_interactive_pick_exportXMLFile:
 * @filename: a filename to export to.
 * @error: (allow-none): a location to store an error.
 *
 * Export to @filename the list of currently picked nodes.
 *
 * Since: 3.7
 *
 * Returns: TRUE if everything goes right.
 **/
gboolean visu_ui_interactive_pick_exportXMLFile(const gchar *filename, GError **error)
{
  VisuGlExtMarks *marks;
  int *nodes;
  VisuGlExtInfosDrawId mode;
  int info;
  gboolean valid;

  marks = visu_ui_rendering_window_getMarks(visu_ui_main_class_getDefaultRendering());
  nodes = getListedNodes();

  mode = DRAW_NEVER;
  if (radioDrawNever &&
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawNever)))
    mode = DRAW_NEVER;
  else if (radioDrawSelected &&
           gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawSelected)))
    mode = DRAW_SELECTED;
  else if (radioDrawAlways &&
           gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDrawAlways)))
    mode = DRAW_ALWAYS;
  info = (comboDraw)?gtk_combo_box_get_active(GTK_COMBO_BOX(comboDraw)):-1;

  valid = visu_gl_ext_marks_exportXMLFile(marks, filename, nodes, mode, info, error);

  g_free(nodes);

  return valid;
}
