/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2006)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_RENDERINGWINDOW_H
#define GTK_RENDERINGWINDOW_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "visu_data.h"
#include "visu_dump.h"
#include "extensions/marks.h"
#include "openGLFunctions/interactive.h"
#include "openGLFunctions/view.h"

G_BEGIN_DECLS
/**
 * VISU_UI_TYPE_RENDERING_WINDOW:
 *
 * Return the associated #GType to the Renderingwindow objects.
 */
#define VISU_UI_TYPE_RENDERING_WINDOW         (visu_ui_rendering_window_get_type())
/**
 * VISU_UI_RENDERING_WINDOW:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiRenderingWindow object.
 */
#define VISU_UI_RENDERING_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_UI_TYPE_RENDERING_WINDOW, VisuUiRenderingWindow))
/**
 * VISU_UI_RENDERING_WINDOW_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiRenderingWindowClass object.
 */
#define VISU_UI_RENDERING_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), VISU_UI_TYPE_RENDERING_WINDOW, VisuUiRenderingWindowClass))
/**
 * VISU_UI_IS_RENDERING_WINDOW:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuUiRenderingWindow object.
 */
#define VISU_UI_IS_RENDERING_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_UI_TYPE_RENDERING_WINDOW))
/**
 * VISU_UI_IS_RENDERING_WINDOW_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuUiRenderingWindowClass class.
 */
#define VISU_UI_IS_RENDERING_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), VISU_UI_TYPE_RENDERING_WINDOW))
/**
 * VISU_UI_RENDERING_WINDOW_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_UI_RENDERING_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_UI_TYPE_RENDERING_WINDOW, VisuUiRenderingWindowClass))

/**
 * VisuUiRenderingWindow:
 *
 * Short form for a #VisuUiRenderingWindow_struct structure.
 */
typedef struct _VisuUiRenderingWindow VisuUiRenderingWindow;
/**
 * VisuUiRenderingWindowClass:
 *
 * Opaque structure.
 */
typedef struct _VisuUiRenderingWindowClass VisuUiRenderingWindowClass;

/**
 * visu_ui_rendering_window_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiRenderingWindow objects.
 */
GType          visu_ui_rendering_window_get_type        (void);

GtkWidget* visu_ui_rendering_window_new(int width, int height,
                                        gboolean withFrame, gboolean withToolBar);
void visu_ui_rendering_window_pushMessage(VisuUiRenderingWindow *window,
                                          const gchar *message);
void visu_ui_rendering_window_popMessage(VisuUiRenderingWindow *window);

void visu_ui_rendering_window_setData(VisuUiRenderingWindow *window, VisuData* data);
VisuData* visu_ui_rendering_window_getData(VisuUiRenderingWindow *window);
VisuGlView* visu_ui_rendering_window_getGlView(VisuUiRenderingWindow *window);

void visu_ui_rendering_window_loadFile(VisuUiRenderingWindow *window, VisuData *data, guint iSet);
void visu_ui_rendering_window_open(VisuUiRenderingWindow *window, GtkWindow *parent);

gboolean visu_ui_rendering_window_dump(VisuUiRenderingWindow *window, VisuDump *format,
                                       const char* fileName, gint width, gint height,
                                       ToolVoidDataFunc functionWait, gpointer data,
                                       GError **error);

void visu_ui_rendering_window_reload(VisuUiRenderingWindow *window);
GtkAccelGroup* visu_ui_rendering_window_getAccelGroup(VisuUiRenderingWindow *window);

void visu_ui_rendering_window_setCurrent(VisuUiRenderingWindow *window, gboolean force);

void visu_ui_rendering_window_pushInteractive(VisuUiRenderingWindow *window,
				     VisuInteractive *inter);
void visu_ui_rendering_window_popInteractive(VisuUiRenderingWindow *window,
				    VisuInteractive *inter);
VisuGlExtMarks* visu_ui_rendering_window_getMarks(VisuUiRenderingWindow *window);

VisuInteractive* visu_ui_rendering_window_class_getInteractive();
gboolean visu_ui_rendering_window_class_setDisplayCoordinatesInReduce(gboolean status);
gboolean visu_ui_rendering_window_class_getDisplayCoordinatesInReduce();
gboolean visu_ui_rendering_window_class_setAutoAdjust(gboolean status);
gboolean visu_ui_rendering_window_class_getAutoAdjust();

G_END_DECLS

#endif
