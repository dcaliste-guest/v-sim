/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_boxed.h"

#include "config.h"

/**
 * SECTION:iface_boxed
 * @short_description: Defines a common interface for objects with a #VisuBox.
 * @See_also: #VisuBox, #VisuData, #VisuPlane, #VisuSurfaces, #VisuScalarField
 * and #VisuGlView
 *
 * <para></para>
 */

/**
 * VisuBoxedInterface:
 * @parent: yet, its parent.
 * @get_box: a routine to get a pointer on the #VisuBox.
 * @set_box: a routine to set a #VisuBox to a #VisuBoxed object.
 *
 * The different routines common to objects implementing a #VisuBoxed interface.
 * 
 * Since: 3.7
 */

enum
  {
    SET_BOX_SIGNAL,
    NB_SIGNAL
  };

/* Internal variables. */
static guint _signals[NB_SIGNAL] = { 0 };

/* Boxed interface. */
G_DEFINE_INTERFACE(VisuBoxed, visu_boxed, G_TYPE_OBJECT)

static void visu_boxed_default_init(VisuBoxedInterface *iface)
{
  /**
   * VisuBoxed::setBox:
   * @boxed: the object which received the signal.
   *
   * Gets emitted when the bounding box is changed.
   *
   * Since: 3.7
   */
  _signals[SET_BOX_SIGNAL] =
    g_signal_new("setBox", G_TYPE_FROM_INTERFACE (iface),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
}

/**
 * visu_boxed_getBox:
 * @self: a #VisuBoxed object.
 *
 * Retrieves the #VisuBox of @self.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the #VisuBox of @self.
 **/
VisuBox* visu_boxed_getBox(VisuBoxed *self)
{
  g_return_val_if_fail(VISU_IS_BOXED(self), (VisuBox*)0);

  return VISU_BOXED_GET_INTERFACE(self)->get_box(self);
}
/**
 * visu_boxed_setBox:
 * @self: a #VisuBoxed object.
 * @box: (transfer none): a #VisuBoxed object.
 * @update: a boolean.
 *
 * Attach the #VisuBox of @box to @boxed. If @update is %TRUE, coordinates inside
 * @boxed are updated to fit into the new #VisuBox. A reference is
 * taken on the #VisuBox of @box. This routine emits #VisuBoxed::setBox
 * signal if the @self has changed its #VisuBox.
 *
 * Since: 3.7
 *
 * Returns: FALSE @boxed was already boxed with the #VisuBox of @box.
 **/
gboolean visu_boxed_setBox(VisuBoxed *self, VisuBoxed *box, gboolean update)
{
  VisuBox *boxObj;
  gboolean res;
  
  g_return_val_if_fail(VISU_IS_BOXED(self), FALSE);

  boxObj = visu_boxed_getBox(box);
  res = VISU_BOXED_GET_INTERFACE(self)->set_box(self, boxObj, update);
  if (res)
    g_signal_emit(G_OBJECT(self), _signals[SET_BOX_SIGNAL], 0, NULL);
  return res;
}
