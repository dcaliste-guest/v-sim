/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_BOXED_H
#define IFACE_BOXED_H

#include <glib.h>
#include <glib-object.h>

#include "visu_box.h"

G_BEGIN_DECLS

/* Boxed interface. */
#define VISU_TYPE_BOXED                (visu_boxed_get_type ())
#define VISU_BOXED(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_BOXED, VisuBoxed))
#define VISU_IS_BOXED(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_BOXED))
#define VISU_BOXED_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_BOXED, VisuBoxedInterface))

typedef struct _VisuBoxed VisuBoxed; /* dummy object */
typedef struct _VisuBoxedInterface VisuBoxedInterface;

struct _VisuBoxedInterface
{
  GTypeInterface parent;

  VisuBox* (*get_box) (VisuBoxed *self);
  gboolean (*set_box) (VisuBoxed *self, VisuBox *box, gboolean update);
};

GType visu_boxed_get_type (void);

VisuBox* visu_boxed_getBox(VisuBoxed *self);
gboolean visu_boxed_setBox(VisuBoxed *self, VisuBoxed *box, gboolean update);

G_END_DECLS

#endif
