/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "renderingMode.h"

#include <visu_tools.h>
#include <visu_configFile.h>
#include <coreTools/toolConfigFile.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include <string.h>

/**
 * SECTION:renderingMode
 * @short_description: Controls the way OpenGL renders objects.
 * 
 * <para>
 * This modules creates an interface to access to the way OpenGL
 * renders the objects (glPolygonMode() and glToolShadeModel()
 * functions). There are then three rendering modes available in
 * V_Sim: wireframe, flat and smooth. They are controls by an enum
 * #RenderingModeId. When visu_gl_rendering_applyMode() is called, the
 * current rendering mode is changed for all future drawing calls that
 * uses polygons.
 * </para>
 */

#define FLAG_PARAMETER_OPENGL_RENDERING "opengl_render"
#define DESC_PARAMETER_OPENGL_RENDERING "Rules the way OpenGl draws objects in general ; 4 possible strings : VISU_GL_RENDERING_WIREFRAME, VISU_GL_RENDERING_FLAT, VISU_GL_RENDERING_SMOOTH and VISU_GL_RENDERING_SMOOTH_AND_EDGE"
#define PARAMETER_OPENGL_RENDERING_DEFAULT VISU_GL_RENDERING_SMOOTH

static VisuGlRenderingMode renderingOption;
static gboolean readOpenGLRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				    VisuData *dataObj, VisuGlView *view, GError **error);
static void exportParametersRenderingMode(GString *data,
                                          VisuData *dataObj, VisuGlView *view);

static const char *renderingStrings[VISU_GL_RENDERING_N_MODES + 1] =
  {"Wireframe", "Flat", "Smooth", "SmoothAndEdge", (const char*)0};
static const char *renderingStringsI18n[VISU_GL_RENDERING_N_MODES + 1];


/**
 * visu_gl_rendering_init: (skip)
 *
 * This method is used by opengl.c to initialise this module (declare config file
 * options...). It should not be called elsewhere.
 */
void visu_gl_rendering_init(void)
{
  DBG_fprintf(stderr, "OpenGl RenderingMode : initialization.\n");
  visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
                           FLAG_PARAMETER_OPENGL_RENDERING,
                           DESC_PARAMETER_OPENGL_RENDERING,
                           1, readOpenGLRendering);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParametersRenderingMode);

  renderingOption = PARAMETER_OPENGL_RENDERING_DEFAULT;
  renderingStringsI18n[VISU_GL_RENDERING_WIREFRAME      ] = _("Wireframe");
  renderingStringsI18n[VISU_GL_RENDERING_FLAT           ] = _("Flat");
  renderingStringsI18n[VISU_GL_RENDERING_SMOOTH         ] = _("Smooth");
  renderingStringsI18n[VISU_GL_RENDERING_SMOOTH_AND_EDGE] = _("Smooth & edge");
  renderingStringsI18n[VISU_GL_RENDERING_N_MODES] = (const char*)0;
}

/**
 * visu_gl_rendering_applyMode:
 * @mode: an integer.
 *
 * Change the rendering mode of current OpenGL context.
 */
void visu_gl_rendering_applyMode(VisuGlRenderingMode mode)
{
  switch (mode)
    {
    case VISU_GL_RENDERING_WIREFRAME:
      glShadeModel(GL_FLAT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glLineWidth(1);
      break;
    case VISU_GL_RENDERING_FLAT:
      glShadeModel(GL_FLAT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case VISU_GL_RENDERING_SMOOTH:
    case VISU_GL_RENDERING_SMOOTH_AND_EDGE:
      glShadeModel(GL_SMOOTH);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      g_warning("Wrong value for parameter 'mode' in a call"
		" to 'visu_gl_rendering_applyMode'.");
      return;
    }
  DBG_fprintf(stderr, "Rendering Mode : switch rendering mode to '%s'.\n",
	      renderingStrings[mode]);
}
/**
 * visu_gl_rendering_setGlobalMode:
 * @value: an integer to represent the method of rendering.
 *
 * This function change the value of the parameter renderingOption.
 * It controls how V_Sim renders objects, in wireframe for example.
 *
 * Returns: TRUE if the signal OpenGLAskForReDraw should be emitted.
 */
gboolean visu_gl_rendering_setGlobalMode(VisuGlRenderingMode value)
{
  g_return_val_if_fail(value < VISU_GL_RENDERING_N_MODES, FALSE);
  
  if (value == renderingOption)
    return FALSE;

  renderingOption = value;
  visu_gl_rendering_applyMode(value);

  return 1;
}
/**
 * visu_gl_rendering_getGlobalMode:
 *
 * This function retrieve the value of the parameter renderingOption.
 *
 * Returns: the identifier of the current rendering option.
 */
VisuGlRenderingMode visu_gl_rendering_getGlobalMode(void)
{
  return renderingOption;
}
/**
 * visu_gl_rendering_getAllModeLabels:
 *
 * This function retrieve al the names (translated) of available rendering modes.
 *
 * Returns: (transfer none): an array of string, NULL terminated that
 * is private (not to be freed).
 */
const char** visu_gl_rendering_getAllModeLabels(void)
{
  return renderingStringsI18n;
}
/**
 * visu_gl_rendering_getAllModes:
 *
 * This function retrieve al the names of available rendering modes.
 *
 * Returns: (transfer none): an array of string, NULL terminated that
 * is private (not to be freed).
 */
const char** visu_gl_rendering_getAllModes(void)
{
  return renderingStrings;
}
/**
 * visu_gl_rendering_getModeFromName:
 * @name: a string ;
 * @id: a location to store the resulting id.
 *
 * This function retrieve the rendering mode id associated to the name.
 *
 * Returns: TRUE if the name exists.
 */
gboolean visu_gl_rendering_getModeFromName(const char* name, VisuGlRenderingMode *id)
{
  g_return_val_if_fail(name && id, FALSE);

  *id = 0;
  while (*id < VISU_GL_RENDERING_N_MODES && strcmp(name, renderingStrings[*id]))
    *id += 1;
  return (*id < VISU_GL_RENDERING_N_MODES);  
}

static gboolean readOpenGLRendering(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				    VisuData *dataObj _U_, VisuGlView *view _U_,
                                    GError **error)
{
  VisuGlRenderingMode id;
  gchar **val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readString(lines[0], position, &val, 1, FALSE, error))
    return FALSE;
  if (!visu_gl_rendering_getModeFromName(val[0], &id))
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the shape '%s' is unknown.\n"),
			   position, val[0]);

      g_strfreev(val);
      return FALSE;
    }
  g_strfreev(val);
  visu_gl_rendering_setGlobalMode(id);
  return TRUE;
}
static void exportParametersRenderingMode(GString *data,
                                          VisuData *dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_OPENGL_RENDERING);
  g_string_append_printf(data, "%s: %s\n\n", FLAG_PARAMETER_OPENGL_RENDERING,
	  renderingStrings[renderingOption]);
}
