/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TEXT_H
#define TEXT_H

#include <glib.h>

/**
 * VisuGlTextSize:
 * @VISU_GL_TEXT_NORMAL: normal size (14) ;
 * @VISU_GL_TEXT_SMALL: small text (12).
 *
 * Different text size available in V_Sim.
 */
typedef enum
  {
    VISU_GL_TEXT_NORMAL,
    VISU_GL_TEXT_SMALL
  } VisuGlTextSize;

/**
 * VisuGlTextFunc:
 * @text: the text to render.
 * @size: the size for rendering.
 *
 * A function that render iso8859-1 text at the current raster
 * position.
 *
 * Since: 3.7
 */
typedef void (*VisuGlTextFunc)(const gchar *text, VisuGlTextSize size);

gboolean visu_gl_text_setFunc(VisuGlTextFunc func);

gboolean visu_gl_text_setFontSize(float size);
void visu_gl_text_drawChars(gchar *s, VisuGlTextSize size);
void visu_gl_text_initFontList();
void visu_gl_text_rebuildFontList();
void visu_gl_text_onNewContext();

void visu_gl_text_putTextWithFTGL(const gchar *text, VisuGlTextSize size);

#endif
