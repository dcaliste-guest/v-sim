/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef OPENGL_H
#define OPENGL_H

#include <glib.h>

#include "visu_box.h"
#include "openGLFunctions/view.h"
#include "openGLFunctions/light.h"

/***************/
/* Public part */
/***************/

void visu_gl_setColor(float* material, float* rgba);
void visu_gl_setHighlightColor(float material[5], float rgb[3], float alpha);

void visu_gl_init();
void visu_gl_initGraphics();

/* Objects to play with lights. */
VisuGlLights* visu_gl_getLights();

gboolean visu_gl_setAntialias(gboolean value);
gboolean visu_gl_getAntialias();

gboolean visu_gl_setImmediate(gboolean bool);
gboolean visu_gl_getImmediate();

gboolean visu_gl_setTrueTransparency(gboolean status);
gboolean visu_gl_getTrueTransparency();

gboolean visu_gl_setStereo(gboolean status);
gboolean visu_gl_getStereo();
gboolean visu_gl_setStereoAngle(float angle);
float visu_gl_getStereoAngle();
gboolean visu_gl_getStereoCapability();

/* redraw methods */
void visu_gl_redraw(VisuGlView *view, GList *lists);
void visu_gl_initContext();


#endif
