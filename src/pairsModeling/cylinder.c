/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "cylinder.h"

#include <GL/gl.h>
#include <GL/glu.h> 

#include <math.h>

#include <opengl.h>
#include <visu_tools.h>
#include <visu_data.h>
#include <visu_object.h>
#include <visu_configFile.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolConfigFile.h>

/**
 * SECTION:cylinder
 * @short_description: Gives methods to draw cylinders as OpenGl
 * objects pairing two elements.
 *
 * <para>The cylinders have two characteristics: their radius and
 * their colour. Their radius are in the units of the rendered
 * box. They can be specific to each kind of pairs (e.g. Si-Si) or
 * they have a default value. Only the default value is stored in the
 * resource file with the flag "pairCylinder_radius". Their color is
 * herited by the colour of the pair. The lighting values
 * (i.e. emi., shi., spe. ...) are not movable and are fixed to amb =
 * 0.5, dif = 0.5, shi = 0, spe = 0, emi = 0.</para>
 */

static int listCylinder;
static VisuPairExtension* pointerToPairExtension_cylinder;

#define FLAG_RESOURCES_PAIR_RADIUS "pairCylinder_pairRadius"
#define DESC_RESOURCES_PAIR_RADIUS "This value is the radius for specific pairs drawn as cylinders ; element1 elemen2 0 < real < 10"
#define FLAG_RESOURCES_LINK_RADIUS "pairCylinder_linkRadius"
#define DESC_RESOURCES_LINK_RADIUS "This value is the radius for specific drawn link as cylinders ; [element1] [element2] [min] [max] [0 < real < 10]"
#define FLAG_RESOURCES_CYLINDER_RADIUS "pairCylinder_radius"
#define DESC_RESOURCES_CYLINDER_RADIUS "This value is the default radius of the pairs drawn as cylinders ; 0 < real < 10"
#define RESOURCES_CYLINDER_RADIUS_DEFAULT 0.1
static float cylinderRadius;

#define FLAG_RESOURCES_CYLINDER_COLOR_TYPE "cylinder_colorType"
#define DESC_RESOURCES_CYLINDER_COLOR_TYPE "It chooses the colors of the cylinders according differents criterion ;"
#define RESOURCES_CYLINDER_COLOR_TYPE_DEFAULT VISU_GL_PAIRS_CYLINDER_COLOR_USER
static int cylinderColorType;

static gboolean readCylinderDefaultRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					  VisuData *dataObj, VisuGlView *view,
                                          GError **error);
static gboolean readCylinderRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				   int position, VisuData *dataObj, VisuGlView *view,
                                   GError **error);
static gboolean readLinkRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
			       int position, VisuData *dataObj, VisuGlView *view,
                               GError **error);
static gboolean readCylinderColorType(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				      VisuData *dataObj, VisuGlView *view,
                                      GError **error);
static void exportResourcesCylinder(GString *data, VisuData *dataObj, VisuGlView *view);


/**
 * visu_gl_pairs_cylinder_getStatic:
 *
 * Internal use only to get the cylinder extension.
 */
VisuPairExtension* visu_gl_pairs_cylinder_getStatic()
{
  return pointerToPairExtension_cylinder;
}

/**
 * visu_gl_pairs_cylinder_setGeneralRadius:
 * @val: a float value.
 *
 * This method allows to change the default value of radius for cylinder pairs.
 * When a pair is rendered via a cylinder, it first checks if that pairs has
 * a specific radius value. If not, it uses the default value set by this method.
 * If the default value is indeed changed, all #VisuGlExtPairs objects
 * should redraw themselves.
 *
 * Returns: TRUE if the calling method should take care of
 * #VisuGlExtPairs objects, FALSE if not.
 */
gboolean visu_gl_pairs_cylinder_setGeneralRadius(float val)
{
  DBG_fprintf(stderr, "Pairs Cylinder : set the general cylinder radius to %f.\n", val);
  val = CLAMP(val, VISU_GL_PAIRS_CYLINDER_RADIUS_MIN, VISU_GL_PAIRS_CYLINDER_RADIUS_MAX);

  if (val == cylinderRadius)
    return FALSE;

  cylinderRadius = val;
  return TRUE;
}
/**
 * visu_gl_pairs_cylinder_getGeneralRadius:
 *
 * Get the default value for cylinder radius.
 *
 * Returns: the default value for cylinder radius.
 */
float visu_gl_pairs_cylinder_getGeneralRadius()
{
  return cylinderRadius;
}
/**
 * visu_gl_pairs_cylinder_setRadius:
 * @data: a #VisuPairLink object ;
 * @val: a float value.
 *
 * This method allows to change the radius value of a specific pair.
 * When a pair is rendered via a cylinder, it first checks if that pairs has
 * a specific radius value set by this method. If not, it uses the default value.
 *
 * Returns: TRUE if the value is changed.
 */
gboolean visu_gl_pairs_cylinder_setRadius(VisuPairLink *data, float val)
{
  float *radius;

  if (!data)
    return FALSE;

  DBG_fprintf(stderr, "Pairs Cylinder : set the cylinder radius to %f.\n", val);
  val = CLAMP(val, VISU_GL_PAIRS_CYLINDER_RADIUS_MIN, VISU_GL_PAIRS_CYLINDER_RADIUS_MAX);

  radius = (float*)g_object_get_data(G_OBJECT(data), "radius");
  if (!radius)
    {
      radius = g_malloc(sizeof(float));
      g_object_set_data_full(G_OBJECT(data), "radius", (gpointer)radius, g_free);
      *radius = G_MAXFLOAT;
    }
  if (*radius != val)
    {
      *radius = val;
      if (visu_pair_link_isDrawn(data))
        g_signal_emit_by_name(G_OBJECT(data), "ParameterChanged", NULL);
      return TRUE;
    }

  return FALSE;
}
/**
 * visu_gl_pairs_cylinder_getRadius:
 * @data: a #VisuPairLink object.
 *
 * Get the radius value for the specified pair.
 *
 * Returns: the radius value.
 */
float visu_gl_pairs_cylinder_getRadius(VisuPairLink *data)
{
  float *radius;

  if (!data)
    return -1.;

  radius = (float*)g_object_get_data(G_OBJECT(data), "radius");
  if (radius)
    return *radius;
  else
    return cylinderRadius;
}
/**
 * visu_gl_pairs_cylinder_setColorType:
 * @val: a integer that identify the color scheme.
 *
 * It set the color scheme for cylinder pairs. It can be 0 or 1.
 *
 * Returns: TRUE if the calling method should take care of
 * #VisuGlExtPairs objects, FALSE if not.
 */
gboolean visu_gl_pairs_cylinder_setColorType(VisuGlPairsCylinderColorId val)
{
  DBG_fprintf(stderr, "Pairs Cylinder : set the cylinder color type to %d.\n", val);
  g_return_val_if_fail(val < VISU_GL_PAIRS_CYLINDER_N_COLOR, FALSE);

  cylinderColorType = val;
  return TRUE;
}
/**
 * visu_gl_pairs_cylinder_getColorType:
 *
 * Get the color scheme.
 *
 * Returns: an integer corresponding to the color scheme (0 or 1).
 */
VisuGlPairsCylinderColorId visu_gl_pairs_cylinder_getColorType()
{
  return cylinderColorType;
}

void setColorAndWidthForCylinder(VisuElement *ele1 _U_, VisuElement *ele2 _U_,
				 VisuPairLink *data, VisuGlView *view)
{
  float rgba[4], mm[5] = {0.5, 0.5, 0., 0. , 0.};
  ToolColor *color;
  float radius, *tmpRad;
  guint *nlat;

  switch (cylinderColorType)
    {
    case VISU_GL_PAIRS_CYLINDER_COLOR_USER:
      color = visu_pair_link_getColor(data);
      rgba[0] = color->rgba[0];
      rgba[1] = color->rgba[1];
      rgba[2] = color->rgba[2];
      rgba[3] = 1.;
      visu_gl_setColor(mm, rgba);
      break;
    case VISU_GL_PAIRS_CYLINDER_COLOR_ELEMENT:
    default:
      break;
    }
  tmpRad = (float*)g_object_get_data(G_OBJECT(data), "radius");
  if (tmpRad)
    radius = *tmpRad;
  else
    radius = cylinderRadius;
  nlat = (guint*)g_object_get_data(G_OBJECT(data), "nlat");
  if (!nlat)
    {
      nlat = g_malloc(sizeof(guint));
      g_object_set_data_full(G_OBJECT(data), "nlat", (gpointer)nlat, g_free);
    }
  *nlat = visu_gl_view_getDetailLevel(view, radius);
}

void drawCylinderPairs(VisuElement *ele1, VisuElement *ele2, VisuPairLink *data,
		       VisuGlView *view _U_, double x1, double y1, double z1,
		       double x2, double y2, double z2, float d2, float alphaColour)
{
  float rgba[4], mm[5] = {0.5, 0.5, 0., 0. , 0.};
  ToolColor *color;
  int mat1, mat2;
  guint *nlat;
  double vNorm[3]; /* vecteur normal aux vecteurs (0,0,1) et (x2-x1, y2-y1, z2-z1) */
  double vDest[3]; /* vecteur (x2-x1, y2-y1, z2-z1) */
  double cosAlpha; /* cosinus entre (0,0,1) et Vdest */
  double alpha;
  #define RADTODEG 57.29577951
  float radius, *tmpRad;
  GLUquadricObj *obj;

  tmpRad = (float*)g_object_get_data(G_OBJECT(data), "radius");
  if (tmpRad)
    radius = *tmpRad;
  else
    radius = cylinderRadius;
  nlat = (guint*)g_object_get_data(G_OBJECT(data), "nlat");
  g_return_if_fail(nlat);
  vDest[0] = x2 - x1;
  vDest[1] = y2 - y1;
  vDest[2] = z2 - z1;
  if (vDest[0] != 0 || vDest[1] != 0)
    {
      vNorm[0] = - vDest[1];
      vNorm[1] = vDest[0];
      vNorm[2] = 0.;
      cosAlpha = sqrt((vDest[2] * vDest[2]) / d2);
      if (vDest[2] < 0.)
	cosAlpha = - cosAlpha;
      cosAlpha = CLAMP(cosAlpha, -1., 1.);
      alpha = acos(cosAlpha) * RADTODEG;
    }
  else
    {
      vNorm[0] = 1.;
      vNorm[1] = 0.;
      vNorm[2] = 0.;
      if (vDest[2] < 0.)
	alpha = 180.;
      else
	alpha = 0.;
    }
  obj = gluNewQuadric();
  glPushMatrix();
  switch (cylinderColorType)
    {
    case VISU_GL_PAIRS_CYLINDER_COLOR_USER:
      /* Color is set by the setColorAndWidthForCylinder() method
         before for this family of pairs, we change need to change it
         because of alphaColour. */
      color = visu_pair_link_getColor(data);
      rgba[0] = color->rgba[0];
      rgba[1] = color->rgba[1];
      rgba[2] = color->rgba[2];
      rgba[3] = alphaColour;
      visu_gl_setColor(mm, rgba);

      glTranslated(x1, y1, z1);
      glRotated(alpha, vNorm[0], vNorm[1], vNorm[2]);
      gluCylinder(obj, (GLdouble)radius, (GLdouble)radius,
		  (GLdouble)sqrt(d2), (GLint)*nlat, (GLint)1);
      break;
    case VISU_GL_PAIRS_CYLINDER_COLOR_ELEMENT:
      mat1 = visu_element_getMaterialId(ele1);
      mat2 = visu_element_getMaterialId(ele2);
      if (mat1 <= 0 || mat2 <= 0)
	g_warning("Can't draw cylinders because either ele1"
		  "or ele2 has no identifier for material.\n");
      glTranslated(x1, y1, z1);
      glRotated(alpha, vNorm[0], vNorm[1], vNorm[2]);
      glCallList(mat1);
      gluCylinder(obj, (GLdouble)radius, (GLdouble)radius,
		  (GLdouble)sqrt(d2) * 0.5f, (GLint)*nlat, (GLint)1);
      glPopMatrix();
      glPushMatrix();
      glTranslated(0.5f * (x2 + x1), 0.5f * (y2 + y1), 0.5f * (z2 + z1));
      glRotated(alpha, vNorm[0], vNorm[1], vNorm[2]);
      glCallList(mat2);
      gluCylinder(obj, (GLdouble)radius, (GLdouble)radius,
		  (GLdouble)sqrt(d2) * 0.5f, (GLint)*nlat, (GLint)1);
      break;
    default:
      break;
    }
  glPopMatrix();
  gluDeleteQuadric(obj);
}

/**
 * visu_gl_pairs_cylinder_init:
 *
 * This method is used by V_Sim on startup, don't use it on your own.
 *
 * Returns: the cylinder pair extension.
 */
VisuPairExtension* visu_gl_pairs_cylinder_init()
{
  char *name = _("Cylinder pairs");
  char *desc = _("Pairs are rendered by cylinders."
		 " The color and the width can by chosen.");
  VisuPairExtension *extension;
  VisuConfigFileEntry *resourceEntry, *oldEntry;
  VisuPairDrawFuncs meth;

  meth.start = setColorAndWidthForCylinder;
  meth.stop  = NULL;
  meth.main  = drawCylinderPairs;
  extension = visu_pair_extension_new("Cylinder pairs", name, desc, TRUE, &meth);

  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_CYLINDER_COLOR_TYPE,
					  DESC_RESOURCES_CYLINDER_COLOR_TYPE,
					  1, readCylinderColorType);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_CYLINDER_RADIUS,
					  DESC_RESOURCES_CYLINDER_RADIUS,
					  1, readCylinderDefaultRadius);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     FLAG_RESOURCES_PAIR_RADIUS,
				     DESC_RESOURCES_PAIR_RADIUS,
				     1, readCylinderRadius);
  visu_config_file_entry_setVersion(resourceEntry, 3.1f);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_LINK_RADIUS,
					  DESC_RESOURCES_LINK_RADIUS,
					  1, readLinkRadius);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResourcesCylinder);


  listCylinder = visu_gl_objectlist_new(2);

  cylinderRadius = RESOURCES_CYLINDER_RADIUS_DEFAULT;
  cylinderColorType = VISU_GL_PAIRS_CYLINDER_COLOR_USER;

  pointerToPairExtension_cylinder = extension;
  return extension;
}


/*****************************************/
/* Dealing with parameters and resources */
/*****************************************/

static gboolean readLinkRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			       VisuData *dataObj _U_, VisuGlView *view _U_,
                               GError **error)
{
  gchar **tokens;
  int id;
  float radius;
  VisuPairLink *data;
  
  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit_set(lines[0], " \n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  if (!visu_pair_readLinkFromTokens(tokens, &id, &data, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the associated radius. */
  if (!tool_config_file_readFloatFromTokens(tokens, &id, &radius, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  /* Check. */
  if (radius < VISU_GL_PAIRS_CYLINDER_RADIUS_MIN ||
      radius > VISU_GL_PAIRS_CYLINDER_RADIUS_MAX)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: radius (%g) must be in %g-%g.\n"),
			   position, radius, VISU_GL_PAIRS_CYLINDER_RADIUS_MIN,
			   VISU_GL_PAIRS_CYLINDER_RADIUS_MAX);
      return FALSE;
    }
  /* Set the value. */
  visu_gl_pairs_cylinder_setRadius(data, radius);

  return TRUE;
}
static gboolean readCylinderDefaultRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
					  VisuData *dataObj _U_, VisuGlView *view _U_,
                                          GError **error)
{
  float radius;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readFloat(lines[0], position, &radius, 1, error))
    return FALSE;
  if (radius < VISU_GL_PAIRS_CYLINDER_RADIUS_MIN ||
      radius > VISU_GL_PAIRS_CYLINDER_RADIUS_MAX)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: radius must be in %g-%g.\n"),
			   position, VISU_GL_PAIRS_CYLINDER_RADIUS_MIN,
			   VISU_GL_PAIRS_CYLINDER_RADIUS_MAX);
      return FALSE;
    }
  visu_gl_pairs_cylinder_setGeneralRadius(radius);
  return TRUE;
}
static gboolean readCylinderColorType(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				      VisuData *dataObj _U_, VisuGlView *view _U_,
                                      GError **error)
{
  int val;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readInteger(lines[0], position, &val, 1, error))
    return FALSE;
  if (val < 0 || val >= VISU_GL_PAIRS_CYLINDER_N_COLOR)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: 1 integer value must"
			     " appear after the %s markup.\n"),
			   position, FLAG_RESOURCES_CYLINDER_COLOR_TYPE);
      return FALSE;
    }
  visu_gl_pairs_cylinder_setColorType(val);

  return TRUE;
}
static void exportPairsRadius(VisuElement *ele1, VisuElement *ele2,
			      VisuPairLink *data, gpointer userData)
{
  struct _VisuConfigFileForeachFuncExport *str;
  float *radius;
  gchar *buf;

  buf = g_strdup_printf("%s %s  %4.3f %4.3f", ele1->name, ele2->name,
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MIN),
                        visu_pair_link_getDistance(data, VISU_PAIR_DISTANCE_MAX));

  radius = (float*)g_object_get_data(G_OBJECT(data), "radius");
  if (radius)
    {
      str = ( struct _VisuConfigFileForeachFuncExport*)userData;
      /* We export the resource only if the elements are
	 part of the given VisuData. */
      if (str->dataObj &&
          (!visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele1) >= 0 ||
           !visu_node_array_getElementId(VISU_NODE_ARRAY(str->dataObj), ele2) >= 0))
        return;

      visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_RADIUS, buf,
                                   "%4.3f", *radius);
    }

  g_free(buf);
}
static void exportResourcesCylinder(GString *data, VisuData *dataObj,
                                    VisuGlView *view _U_)
{
  struct _VisuConfigFileForeachFuncExport str;
  gchar *buf;

  buf = g_strdup_printf("%s 0 <= integer < %d", DESC_RESOURCES_CYLINDER_COLOR_TYPE,
                        VISU_GL_PAIRS_CYLINDER_N_COLOR);
  visu_config_file_exportComment(data, buf);
  g_free(buf);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_CYLINDER_COLOR_TYPE, NULL,
                               "%d", cylinderColorType);
  visu_config_file_exportComment(data, DESC_RESOURCES_CYLINDER_RADIUS);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_CYLINDER_RADIUS, NULL,
                               "%f", cylinderRadius);
  str.data           = data;
  str.dataObj        = dataObj;
  visu_config_file_exportComment(data, DESC_RESOURCES_PAIR_RADIUS);
  visu_pair_foreach(exportPairsRadius, &str);
  visu_config_file_exportComment(data, "");
}



/* OBSOLETE function kept for backward compatibility. */
static gboolean readCylinderRadius(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position _U_,
				   VisuData *dataObj _U_, VisuGlView *view _U_,
                                   GError **error)
{
  int token;
  float radius;
  VisuElement* ele[2];
  VisuPairLink *data;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Tokenize the line of values. */
  tokens = g_strsplit_set(g_strchug(lines[0]), " \n", TOOL_MAX_LINE_LENGTH);
  token = 0;

  /* Get the two elements. */
  if (!tool_config_file_readElementFromTokens(tokens, &token, ele, 2, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }

  data = visu_pair_link_getFromId(ele[0], ele[1], 0);
  g_return_val_if_fail(data, FALSE);

  /* Read 1 float. */
  if (!tool_config_file_readFloatFromTokens(tokens, &token, &radius, 1, nbLines, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  radius = CLAMP(radius, VISU_GL_PAIRS_CYLINDER_RADIUS_MIN,
		 VISU_GL_PAIRS_CYLINDER_RADIUS_MAX);
  visu_gl_pairs_cylinder_setRadius(data, radius);

  g_strfreev(tokens);
  return TRUE;
}
