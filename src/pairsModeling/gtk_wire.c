/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "gtk_wire.h"

#include <gtk/gtk.h>

#include <visu_object.h>
#include <visu_tools.h>
#include <gtk_pairs.h>
#include <extensions/pairs.h>
#include <extraGtkFunctions/gtk_stippleComboBoxWidget.h>
#include <extraGtkFunctions/gtk_shadeComboBoxWidget.h>

#include "wire.h"

/**
 * SECTION: gtk_wire
 * @short_description: The additional widgets displayed in the pair
 * dialog to handle wire pairs.
 *
 * <para>Some additional widgets to be displayed in the pairs dialog
 * window. It contains a selector for the stipple of line
 * representation, a selector for the colour and a way to choose a
 * shade in case of colourisation depending on length.</para>
 */

static GtkWidget *spinThickness;
static GtkWidget *comboStipple;
static GtkWidget *comboToolShade;
static GtkWidget *checkNonLinear;
static gulong signalSpinWidthId, signalComboStippleId;
static gulong signalComboShadeId, signalCheckLinearId;

/* Callbacks */
static void onThicknessChanged(GtkSpinButton *spin, gpointer data);
static void onUseToolShade(GtkToggleButton *button, gpointer data);
static void onStippleChanged(VisuUiStippleCombobox *combo, guint value, gpointer data);
static void onComboToolShadeChanged(GtkComboBox *combo, gpointer data);

/**
 * visu_ui_pairs_wire_init: (skip)
 *
 * This routine is used by V_Sim to initialise the Gtk part of
 * wire pair model. This should be called once on start-up.
 */
void visu_ui_pairs_wire_init(void)
{
/*   g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded", */
/* 		   G_CALLBACK(wireValuesChangedOnResources), (gpointer)0); */
}

/**
 * visu_ui_pairs_wire_initBuild: (skip)
 *
 * This routine create a container #GtkWidget that hold all widgets
 * necessary for handling wire pair model.
 *
 * Returns: a newly created #GtkWidget.
 */
GtkWidget* visu_ui_pairs_wire_initBuild()
{
  GtkWidget *hbox, *vbox;
  GtkWidget *label;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;
  tooltips = gtk_tooltips_new ();
#endif

  DBG_fprintf(stderr, "Gtk Wire: building interface for wire pairs.\n");

  vbox = gtk_vbox_new(FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 10);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new(_("Thickness:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  spinThickness = gtk_spin_button_new_with_range(1, 10, 1);
  gtk_box_pack_start(GTK_BOX(hbox), spinThickness, FALSE, FALSE, 0);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinThickness), TRUE);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinThickness), visu_gl_pairs_wire_getGeneralWidth());
  gtk_entry_set_width_chars(GTK_ENTRY(spinThickness), 3);
  label = gtk_label_new(_("Pattern:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  comboStipple = visu_ui_stipple_combobox_new();
  gtk_box_pack_start(GTK_BOX(hbox), comboStipple, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  comboToolShade = visu_ui_shade_combobox_new(TRUE, FALSE);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboToolShade), 0);
  gtk_box_pack_end(GTK_BOX(hbox), comboToolShade, FALSE, FALSE, 0);
  checkNonLinear = gtk_check_button_new_with_mnemonic
   (_("Color _varies with length:"));
  gtk_box_pack_end(GTK_BOX(hbox), checkNonLinear, FALSE, FALSE, 0);

  signalComboShadeId = g_signal_connect(G_OBJECT(comboToolShade), "changed",
                                        G_CALLBACK(onComboToolShadeChanged),
                                        (gpointer)checkNonLinear);
  signalSpinWidthId = g_signal_connect(G_OBJECT(spinThickness), "value-changed",
				       G_CALLBACK(onThicknessChanged), (gpointer)0);
  signalCheckLinearId = g_signal_connect(G_OBJECT(checkNonLinear), "toggled",
                                         G_CALLBACK(onUseToolShade),
                                         (gpointer)comboToolShade);
  signalComboStippleId = g_signal_connect(G_OBJECT(comboStipple), "stipple-selected",
					  G_CALLBACK(onStippleChanged), (gpointer)0);

  return vbox;
}

/**
 * visu_ui_pairs_wire_getValues:
 * @data: information about a pair
 *
 * Create an internationalised UTF-8 label that describes the
 * information about a pair.
 *
 * Returns: (transfer full): a newly allocated string.
 */
gchar* visu_ui_pairs_wire_getValues(VisuPairLink *data)
{
  int width;
  guint16 stipple;
  gchar *str;

  width = visu_gl_pairs_wire_getWidth(data);
  stipple = visu_gl_pairs_wire_getStipple(data);

  /* px is for pixels and pat. for pattern. */
  str = g_strdup_printf("%s %2d%s, %s %d", _("wire:"),
                        width, _("px"), _("pat."), stipple);

  return str;
}
/**
 * visu_ui_pairs_wire_setValues:
 * @data: information about a pair.
 *
 * Change the widget values with the given information. This change trigger
 * no signals.
 */
void visu_ui_pairs_wire_setValues(VisuPairLink *data)
{
  int width;
  guint16 stipple;
  ToolShade *shade;

  /* Update width. */
  width = visu_gl_pairs_wire_getWidth(data);
  g_signal_handler_block(G_OBJECT(spinThickness), signalSpinWidthId);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinThickness), width);
  g_signal_handler_unblock(G_OBJECT(spinThickness), signalSpinWidthId);

  /* Update stipple. */
  stipple = visu_gl_pairs_wire_getStipple(data);
  g_signal_handler_block(G_OBJECT(comboStipple), signalComboStippleId);
  if (!visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(comboStipple), stipple))
    {
      visu_ui_stipple_combobox_add(VISU_UI_STIPPLE_COMBOBOX(comboStipple), stipple);
      visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(comboStipple), stipple);
    }
  g_signal_handler_unblock(G_OBJECT(comboStipple), signalComboStippleId);

  /* Update shade. */
  shade = visu_gl_pairs_wire_getShade(data);
  g_signal_handler_block(G_OBJECT(checkNonLinear), signalCheckLinearId);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkNonLinear),
                               (shade != (ToolShade*)0));
  g_signal_handler_unblock(G_OBJECT(checkNonLinear), signalCheckLinearId);
  gtk_widget_set_sensitive(comboToolShade, (shade != (ToolShade*)0));
  if (shade)
    {
      g_signal_handler_block(G_OBJECT(comboToolShade), signalComboShadeId);
      visu_ui_shade_combobox_setSelectionByShade(VISU_UI_SHADE_COMBOBOX(comboToolShade),
                                                 shade);
      g_signal_handler_unblock(G_OBJECT(comboToolShade), signalComboShadeId);
    }
}

static void onThicknessChanged(GtkSpinButton *spin, gpointer data _U_)
{
  int res;
  VisuUiPairsIter iter;
  gchar* label;

  res = FALSE;
  /* We run on all selected pairs. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      /* We change the value for this pair. */
      res = visu_gl_pairs_wire_setWidth(iter.data, (float)gtk_spin_button_get_value(spin)) || res;
      label = visu_ui_pairs_wire_getValues(iter.data);
      /* We change the drawn label. */
      visu_ui_pairs_setSpecificLabels(&(iter.iter), label);
      g_free(label);
    }

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onStippleChanged(VisuUiStippleCombobox *combo _U_, guint value,
			     gpointer data _U_)
{
  int res;
  VisuUiPairsIter iter;
  gchar* label;

  DBG_fprintf(stderr, "Gtk Wire: caught 'stipple-selected' signal.\n");
  res = FALSE;
  /* We run on all selected pairs. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    {
      /* We change the value for this pair. */
      res = visu_gl_pairs_wire_setStipple(iter.data, (guint16)value) || res;
      label = visu_ui_pairs_wire_getValues(iter.data);
      /* We change the drawn label. */
      visu_ui_pairs_setSpecificLabels(&(iter.iter), label);
      g_free(label);
    }

  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void setToolShade(VisuUiShadeCombobox *combo, GtkToggleButton *button)
{
  gboolean use, res;
  VisuUiPairsIter iter;
  ToolShade *sh;

  use = gtk_toggle_button_get_active(button);
  gtk_widget_set_sensitive(GTK_WIDGET(combo), use);
  sh = (use)?visu_ui_shade_combobox_getSelection(combo):(ToolShade*)0;
  /* We run on all selected pairs. */
  for (visu_ui_pairs_iter_startSelected(&iter); iter.data;
       visu_ui_pairs_iter_nextSelected(&iter))
    res = visu_gl_pairs_wire_setShade(iter.data, sh) || res;
  if (res)
    {
      visu_gl_ext_pairs_draw(visu_gl_ext_pairs_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onComboToolShadeChanged(GtkComboBox *combo, gpointer data)
{
  setToolShade(VISU_UI_SHADE_COMBOBOX(combo), GTK_TOGGLE_BUTTON(data));
}

static void onUseToolShade(GtkToggleButton *button, gpointer data)
{
  setToolShade(VISU_UI_SHADE_COMBOBOX(data), button);
}
