/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef EXTERNALMODULES_H
#define EXTERNALMODULES_H

/**
 * PanelInitFunc:
 *
 * Prototype of the functions used to initialise a new panel.
 *
 * Returns: a newly created #VisuUiPanel.
 */
typedef VisuUiPanel* (*PanelInitFunc) (VisuUiMain *main);

#include "panelGeometry.h"
#include "panelVibration.h"
#include "panelAxes.h"
#include "panelOpenGL.h"
#include "panelFogBgColor.h"
#include "panelBrowser.h"
#include "panelDataFile.h"
#include "panelElements.h"
#include "panelConfig.h"
#include "panelPlanes.h"
#include "panelSurfaces.h"
#include "panelMethod.h"
#include "panelMap.h"


PanelInitFunc panelListAll[] = {
  visu_ui_panel_elements_init,
  visu_ui_panel_axes_init,
  visu_ui_panel_bg_init,
  visu_ui_panel_browser_init,
  visu_ui_panel_geometry_init,
  visu_ui_panel_colorization_init,
  visu_ui_panel_planes_init,
  visu_ui_panel_surfaces_init,
  visu_ui_panel_map_init,
  visu_ui_panel_vibration_init,
  visu_ui_panel_method_init,
  visu_ui_panel_gl_init,
  visu_ui_panel_config_init,
  (PanelInitFunc)0};

#endif
