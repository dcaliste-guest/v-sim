/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelDataFile.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>

#include <gtk_main.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_gtk.h>
#include <visu_configFile.h>
#include <support.h>
#include <coreTools/toolConfigFile.h>
#include <extraFunctions/dataFile.h>
#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_shadeComboBoxWidget.h>
#include <extensions/shade.h>

/**
 * SECTION: panelDataFile
 * @short_description: The tab where to configure the action of an
 * external data file on colourisation of nodes.
 *
 * <para>The widgets are organised in three categories. The first is
 * used to normalised the inputs, see
 * visu_ui_panel_colorization_setManualRange() or
 * visu_ui_panel_colorization_setRangeMode(). The second category is used
 * for colourisation, see
 * visu_ui_panel_colorization_setPresetShade(). Finally the last category
 * is about post-processing.</para>
 */

/* Local variables. */
static GtkWidget *panelDataFile;
static int nbColumns;
static gboolean panelDataFileIsInitiated;
static gboolean flagDataFileIsLoaded;
static gboolean flagDisableCallbacks;
static gchar *fileExt;
static VisuGlExtShade *extLeg;

#define RED_DATA_LABEL   _("R")
#define GREEN_DATA_LABEL _("G")
#define BLUE_DATA_LABEL  _("B")
#define HUE_DATA_LABEL   _("H")
#define SAT_DATA_LABEL   _("S")
#define VAL_DATA_LABEL   _("V")

char* labelRGB[3];
char* labelHSV[3];

#define COL_ONE          "1."
#define COL_COORD_X      _("coord. x")
#define COL_COORD_Y      _("coord. y")
#define COL_COORD_Z      _("coord. z")
#define N_STATIC_COLS    4
#define N_COORDS_COLS    3

#define COLOR_PREVIEW_WIDTH  120
#define COLOR_PREVIEW_HEIGHT 15
#define COLOR_PREVIEW_BITS   8

#define COLOR_TRANSFORMATION_WIDTH  60
#define COLOR_TRANSFORMATION_HEIGHT 40

#define MIN_MAX_NO_DATA_FILE _("<span style=\"italic\">No data file loaded</span>")
#define DATA_FILE_NO_FILE_MESSAGE _("No data file")

#define FLAG_PARAMETER_FILEEXT   "dataFile_fileExtension"
#define DESC_PARAMETER_FILEEXT   "The extension used for data file ; chain e.g. '.dat'"
#define PARAMETER_FILEEXT_DEFAULT    ".dat"

/* Local methods. */
static void createInteriorDataFile();
static void updateNormalisationWidgets(VisuData *data);
static void updateStatusBar(VisuData *data);
static void updateColorWidgets(VisuData *data);
static void updatePostWidgets(VisuData *data);
static void makeColorPreview(VisuData *data);

/* Linkable widgets */
static GtkWidget *vBoxDataFileOption, *vboxNormalize;
static GtkWidget *openDataFileButton;
static GtkWidget *checkbuttonData;
static GtkWidget *checkbuttonAutoLoad;
static GtkWidget *statusbarDataFile;
static guint statusDataFileContextId;
static GtkWidget *expanderNormalize, *expanderPostProcessing, *expanderTransformation;
static GtkWidget *radioNormalized;
static GtkWidget *radioMinMax;
static GtkWidget *entryDataMax;
static GtkWidget *entryDataMin;
static GtkWidget *spinbuttonDataChA[3];
static GtkWidget *spinbuttonDataChB[3];
static GtkWidget *comboboxDataCh[6];
static GtkWidget *radiobuttonDataRGB;
static GtkWidget *radiobuttonDataHSV;
static GtkWidget *labelChannel[3];
static GdkPixbuf *pixbufColorPreview;
static GtkWidget *colorPreview;
static GtkWidget *labelPreview;
static GtkWidget *readMinMaxValues;
static GtkWidget *comboPreSetColorRange;
static GtkWidget *checkHideMinValues;
static GtkWidget *spinHideMinValues;
static GtkWidget *entryHideMinValues;
static GtkWidget *entryFileExtension;
static GtkWidget *tableLinearToolShade;
static GtkWidget *checkScaleRadius;
static GtkWidget *checkRestrictInRange;
static GtkWidget *checkShowLegend;

/* Signals that need to be suspended */
static gulong signalCheckDataFile;
static gulong signalComboColumnId[6];
static gulong signalSpinDataChA[3];
static gulong signalSpinDataChB[3];
static gulong signalComboToolShade;
static gulong signalRadioRGB;
static gulong signalRadioHSV;
static gulong hide_signal;

/* Callbacks */
static void visuFileReset(GObject *obj, VisuData *dataObj, gpointer data);
static void onDataReady(VisuObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(VisuObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void onLoadDataResponse(GtkDialog *dialog, gint response, gpointer data);
static void useDataFileColor(GtkToggleButton *toggle, gpointer data);
static void onScaleTypeChange(GtkToggleButton *toggle, gpointer data);
static void onEntryMinMaxChangeValue(VisuUiNumericalEntry *entry,
				     double oldValue, gpointer data);
static void onSpinChChangeValue(GtkSpinButton *spinbutton, gpointer user_data);
static void onComboColChange(GtkComboBox *combo, gpointer data);
static void onComboPresetColChange(GtkComboBox *combo, gpointer data);
static void onComboManualChange(GtkComboBox *combo, gpointer data);
static void onColorTypeChange(GtkToggleButton *toggle, gpointer data);
static void onColorPreSetChange(VisuUiShadeCombobox *combo, ToolShade *shade, gpointer data);
static void onSpinHideMinValuesChange(GtkSpinButton *spin, gpointer data);
static void onEntryHideMinValuesChange(VisuUiNumericalEntry *entry,
				       double oldValue, gpointer data);
static void onCheckHideMinValuesChange(GtkToggleButton *toggle, gpointer data);
static void onCheckScaleRadiusChange(GtkToggleButton *toggle, gpointer data);
static void onComboScaleChange(GtkComboBox *combo, gpointer data);
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw, gpointer data);
static void onDataFileEnter(VisuUiPanel *visu_ui_panel, gpointer data);
static void onCheckRestrictChange(GtkToggleButton *toggle, gpointer data);
static void onCheckLegendChange(GtkToggleButton *toggle, gpointer data);
static void onEntryExt(GtkEntry *entry, gchar *key, VisuObject *obj);

/* Local methods. */
static GtkWidget* _createLoadDataDialog();
static void applyHideMinValues(VisuData *visuData, gboolean askRedraw);
static gboolean hideBelow(VisuColorization *dt,
                          const VisuColorizationNodeData *values, gpointer data);
static void exportParameters(GString *data, VisuData* dataObj, VisuGlView *view);

struct _HideBelowData
{
  guint column;
  float value;
};
static struct _HideBelowData hidingData;

/**
 * visu_ui_panel_colorization_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the colouring
 * stuff can be done, such as choosing a colour shade, opening a file,
 * setting boundaries...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_colorization_init()
{
  char *cl = _("Colorize with data");
  char *tl = _("Data color");
  VisuConfigFileEntry *resourceEntry;

  labelRGB[0] = RED_DATA_LABEL;
  labelRGB[1] = GREEN_DATA_LABEL;
  labelRGB[2] = BLUE_DATA_LABEL;
  labelHSV[0] = HUE_DATA_LABEL;
  labelHSV[1] = SAT_DATA_LABEL;
  labelHSV[2] = VAL_DATA_LABEL;

  panelDataFile = visu_ui_panel_newWithIconFromPath("Panel_colorise", cl, tl,
						"stock-data_20.png");
  if (!panelDataFile)
    return (VisuUiPanel*)0;
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelDataFile), TRUE);
						 
  /* Local variables */
  checkbuttonData = gtk_check_button_new_with_mnemonic(_("_Use color scheme"));
  entryFileExtension = (GtkWidget*)0;
  tableLinearToolShade   = (GtkWidget*)0;
  panelDataFileIsInitiated = FALSE;
  flagDataFileIsLoaded = FALSE;
  flagDisableCallbacks = FALSE;
  fileExt = g_strdup(PARAMETER_FILEEXT_DEFAULT);
  extLeg = visu_gl_ext_shade_new("Colourisation legend");
  visu_gl_ext_setActive(VISU_GL_EXT(extLeg), FALSE);

  /* Dealing with parameters. */
  resourceEntry = visu_config_file_addStringEntry(VISU_CONFIG_FILE_PARAMETER,
                                                  FLAG_PARAMETER_FILEEXT,
                                                  DESC_PARAMETER_FILEEXT,
                                                  &fileExt);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParameters);

  /* Local callbacks. */
  g_signal_connect(G_OBJECT(panelDataFile), "page-entered",
		   G_CALLBACK(onDataFileEnter), (gpointer)0);

  return VISU_UI_PANEL(panelDataFile);
}

static void createInteriorDataFile()
{
  GtkWidget *containerDataFilePanel;
  GtkWidget *scrolledwindow1;
  GtkWidget *viewport1;
  GtkWidget *vbox;
  GtkWidget *vboxLimits, *vboxTransformation, *vboxPostProcessing;
  GtkWidget *hbox3;
  GtkWidget *alignment2;
  GtkWidget *hbox8;
  GSList *radioNormalized_group = NULL;
  GtkWidget *alignment3;
  GtkWidget *table2;
  GtkWidget *label7;
  GtkWidget *label8;
  GtkWidget *hbox6;
  GSList *radiobuttonDataRGB_group = NULL;
  GtkWidget *label;
  GtkWidget *label14;
  GtkWidget *label15;
  GtkWidget *label16;
  GtkWidget *label20;
  GtkWidget *label21;
  GtkWidget *hrule;
  GtkWidget *hboxPreSetColorRange;
  gint i;
  VisuData *dataObj;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  DBG_fprintf(stderr, "Panel DataFile : creating subpanel.\n");

  containerDataFilePanel = gtk_vbox_new (FALSE, 0);

  gtk_box_pack_start (GTK_BOX (containerDataFilePanel), checkbuttonData, FALSE, FALSE, 0);

  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolledwindow1),
                                      GTK_SHADOW_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (containerDataFilePanel), scrolledwindow1, TRUE, TRUE, 0);

  viewport1 = gtk_viewport_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), viewport1);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (viewport1), vbox);
  vBoxDataFileOption = vbox;
  gtk_widget_set_sensitive(vBoxDataFileOption, FALSE);

  /********************/
  /* The Data subset. */
  /********************/
  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox3, FALSE, FALSE, 0);

  checkbuttonAutoLoad = gtk_check_button_new_with_mnemonic(_("_Auto load data"));
  gtk_box_pack_end(GTK_BOX(hbox3), checkbuttonAutoLoad, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(checkbuttonAutoLoad,
			_("Try to load a data file whenever a new V_Sim file is loaded."
			  " For example, if 'example.ascii' has just been opened, V_Sim"
			  " will look for 'example.dat' and will apply it."));

  entryFileExtension = gtk_entry_new();
  gtk_entry_set_width_chars(GTK_ENTRY(entryFileExtension), 4);
  gtk_entry_set_text(GTK_ENTRY(entryFileExtension), fileExt);
  g_signal_connect_object(VISU_OBJECT_INSTANCE, "entryParsed::" FLAG_PARAMETER_FILEEXT,
                          G_CALLBACK(onEntryExt), (gpointer)entryFileExtension,
                          G_CONNECT_SWAPPED);

#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  openDataFileButton = gtk_file_chooser_button_new_with_dialog(_createLoadDataDialog());
#else
  openDataFileButton = gtk_button_new_from_stock (GTK_STOCK_OPEN);
  g_signal_connect(G_OBJECT(openDataFileButton), "clicked",
		   G_CALLBACK(loadDataFile), (gpointer)0);
#endif
  gtk_widget_set_tooltip_text(openDataFileButton,
		       _("Choose a file to read the colorization data from."));
  gtk_box_pack_start(GTK_BOX(hbox3), openDataFileButton, TRUE, TRUE, 0);
  
  gtk_box_pack_start(GTK_BOX(hbox3), entryFileExtension, FALSE, FALSE, 5);
  gtk_widget_set_tooltip_text(entryFileExtension,
		       _("File extension used for the autoload option, its value is"
			 " saved in the parameter file (see 'dataFile_fileExt')."));

  vboxNormalize = gtk_vbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), vboxNormalize, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxNormalize), hbox3, FALSE, FALSE, 0);

  label = gtk_label_new(_("Normalize input: "));
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
  gtk_box_pack_start (GTK_BOX (hbox3), label, TRUE, TRUE, 0);

  radioNormalized = gtk_radio_button_new_with_mnemonic (NULL, _("auto"));
  gtk_box_pack_start (GTK_BOX (hbox3), radioNormalized, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (radioNormalized), radioNormalized_group);
  radioNormalized_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radioNormalized));

  radioMinMax = gtk_radio_button_new_with_mnemonic (NULL, _("manual"));
  gtk_box_pack_start (GTK_BOX (hbox3), radioMinMax, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (radioMinMax), radioNormalized_group);
  radioNormalized_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radioMinMax));

  expanderNormalize = gtk_expander_new(_("More information"));
  gtk_box_pack_start(GTK_BOX(vboxNormalize), expanderNormalize, FALSE, FALSE, 0);

  alignment3 = gtk_alignment_new(0.5, 0.5, 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(alignment3), 0, 0, 15, 0);
  gtk_container_add(GTK_CONTAINER(expanderNormalize), alignment3);

  vboxLimits = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(alignment3), vboxLimits);

/*   label = gtk_label_new(_("<b>Read min/max values:</b>")); */
/*   gtk_label_set_use_markup(GTK_LABEL(label), TRUE); */
/*   gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5); */
/*   gtk_box_pack_start(GTK_BOX(vboxLimits), label, FALSE, FALSE, 0); */

  readMinMaxValues = gtk_alignment_new (0.5, 0.5, 0.8, 1);
  gtk_box_pack_start (GTK_BOX(vboxLimits), readMinMaxValues, FALSE, FALSE, 0);

  label = gtk_label_new(MIN_MAX_NO_DATA_FILE);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment(GTK_MISC(label), 0.5, 0.5);
  gtk_container_add(GTK_CONTAINER(readMinMaxValues), label);

  label = gtk_label_new(_("<b>Input data bounds:</b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vboxLimits), label, FALSE, FALSE, 0);

  alignment3 = gtk_alignment_new(0.5, 0.5, 0.8, 1);
  gtk_box_pack_start(GTK_BOX(vboxLimits), alignment3, FALSE, FALSE, 0);

  table2 = gtk_table_new (1, 6, FALSE);
  gtk_container_add (GTK_CONTAINER (alignment3), table2);
  gtk_table_set_col_spacings (GTK_TABLE (table2), 3);

  entryDataMax = visu_ui_numerical_entry_new(1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDataMax), 6);
  gtk_table_attach (GTK_TABLE (table2), entryDataMax, 3, 4, 0, 1,
                    GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_widget_set_sensitive(entryDataMax, FALSE);

  entryDataMin = visu_ui_numerical_entry_new(-1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDataMin), 6);
  gtk_table_attach (GTK_TABLE (table2), entryDataMin, 1, 2, 0, 1,
                    GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_widget_set_sensitive(entryDataMin, FALSE);

  comboboxDataCh[4] = gtk_combo_box_text_new();
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[4]), (const gchar*)0, COL_COORD_X);
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[4]), (const gchar*)0, COL_COORD_Y);
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[4]), (const gchar*)0, COL_COORD_Z);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[4]), 0);
  gtk_table_attach(GTK_TABLE(table2), comboboxDataCh[4], 5, 6, 0, 1,
                    GTK_FILL | GTK_EXPAND, (GtkAttachOptions)0, 0, 0);
  gtk_widget_set_sensitive(comboboxDataCh[4], FALSE);

  label7 = gtk_label_new (_("Min:"));
  gtk_table_attach (GTK_TABLE (table2), label7, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label7), 1, 0.5);

  label8 = gtk_label_new (_("Max:"));
  gtk_table_attach (GTK_TABLE (table2), label8, 2, 3, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label8), 1, 0.5);

  label8 = gtk_label_new(_("Col.:"));
  gtk_table_attach (GTK_TABLE(table2), label8, 4, 5, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment(GTK_MISC(label8), 1, 0.5);

  /* Bar */
  alignment2 = gtk_alignment_new(0.5, 0.5, 0.3, 0);
  gtk_box_pack_start(GTK_BOX(vbox), alignment2, FALSE, FALSE, 8);
  hrule = gtk_hseparator_new();
  gtk_container_add(GTK_CONTAINER(alignment2), hrule);

  /***********************/
  /* Transformation part */
  /***********************/
  label = gtk_label_new(_("Define the color scheme:"));
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

  hboxPreSetColorRange = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hboxPreSetColorRange, FALSE, FALSE, 0);

  /* label = gtk_label_new(_("Preset:")); */
  /* gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5); */
  /* gtk_box_pack_start(GTK_BOX(hboxPreSetColorRange), label, TRUE, TRUE, 0); */
  
  comboPreSetColorRange = visu_ui_shade_combobox_new(FALSE, TRUE);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboPreSetColorRange), -1);
  gtk_box_pack_start(GTK_BOX(hboxPreSetColorRange), comboPreSetColorRange, TRUE, TRUE, 0);

  comboboxDataCh[5] = gtk_combo_box_text_new();
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[5]), (const gchar*)0, COL_COORD_X);
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[5]), (const gchar*)0, COL_COORD_Y);
  gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[5]), (const gchar*)0, COL_COORD_Z);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[5]), -1);
  gtk_box_pack_start(GTK_BOX(hboxPreSetColorRange), comboboxDataCh[5], FALSE, FALSE, 0);

  expanderTransformation = gtk_expander_new(_("More options"));
  gtk_expander_set_expanded(GTK_EXPANDER(expanderTransformation), FALSE);
  gtk_box_pack_start(GTK_BOX (vbox), expanderTransformation, FALSE, FALSE, 0);

  alignment3 = gtk_alignment_new(0.5, 0.5, 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(alignment3), 0, 0, 15, 0);
  gtk_container_add(GTK_CONTAINER(expanderTransformation), alignment3);

  vboxTransformation = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(alignment3), vboxTransformation);

  hbox6 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vboxTransformation), hbox6, FALSE, FALSE, 0);

  label = gtk_label_new(_("<b>Color space: </b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
  gtk_box_pack_start (GTK_BOX (hbox6), label, TRUE, TRUE, 0);

  radiobuttonDataRGB = gtk_radio_button_new_with_mnemonic (NULL, _("RGB"));
  gtk_box_pack_start (GTK_BOX (hbox6), radiobuttonDataRGB, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (radiobuttonDataRGB), radiobuttonDataRGB_group);
  radiobuttonDataRGB_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radiobuttonDataRGB));

  radiobuttonDataHSV = gtk_radio_button_new_with_mnemonic (NULL, _("HSV"));
  gtk_box_pack_start (GTK_BOX (hbox6), radiobuttonDataHSV, FALSE, FALSE, 0);
  gtk_radio_button_set_group (GTK_RADIO_BUTTON (radiobuttonDataHSV), radiobuttonDataRGB_group);
  radiobuttonDataRGB_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (radiobuttonDataHSV));

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonDataHSV), TRUE);

  tableLinearToolShade = gtk_table_new (3, 7, FALSE);
  gtk_box_pack_start (GTK_BOX (vboxTransformation), tableLinearToolShade, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (tableLinearToolShade), 3);
  gtk_table_set_row_spacings (GTK_TABLE (tableLinearToolShade), 3);

  labelChannel[0] = gtk_label_new (_("R"));
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), labelChannel[0], 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (labelChannel[0]), 0, 0.5);

  labelChannel[1] = gtk_label_new (_("G"));
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), labelChannel[1], 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (labelChannel[1]), 0, 0.5);

  labelChannel[2] = gtk_label_new (_("B"));
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), labelChannel[2], 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (labelChannel[2]), 0, 0.5);

  label14 = gtk_label_new ("=");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label14, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  label14 = gtk_label_new ("=");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label14, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  label14 = gtk_label_new ("=");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label14, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label15 = gtk_label_new ("+");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label15, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  label15 = gtk_label_new ("+");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label15, 3, 4, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  label15 = gtk_label_new ("+");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label15, 3, 4, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label16 = gtk_label_new ("\303\227");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label16, 5, 6, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label20 = gtk_label_new ("\303\227");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label20, 5, 6, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label21 = gtk_label_new ("\303\227");
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), label21, 5, 6, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  spinbuttonDataChA[0] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[0]), 1.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChA[0], 4, 5, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChA[0]), TRUE);

  spinbuttonDataChA[1] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[1]), 1.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChA[1], 4, 5, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChA[1]), TRUE);

  spinbuttonDataChA[2] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[2]), 1.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChA[2], 4, 5, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChA[2]), TRUE);

  spinbuttonDataChB[0] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[0]), 0.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChB[0], 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChB[0]), TRUE);

  spinbuttonDataChB[1] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[1]), 0.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChB[1], 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChB[1]), TRUE);

  spinbuttonDataChB[2] = gtk_spin_button_new_with_range(-99, 99, 0.01);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[2]), 0.);
  gtk_table_attach (GTK_TABLE (tableLinearToolShade), spinbuttonDataChB[2], 2, 3, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDataChB[2]), TRUE);

  for (i = 0; i < 3; i++)
    {
      comboboxDataCh[i] = gtk_combo_box_text_new();
      gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]), (const gchar*)0, COL_ONE);
      gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]), (const gchar*)0, COL_COORD_X);
      gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]), (const gchar*)0, COL_COORD_Y);
      gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]), (const gchar*)0, COL_COORD_Z);
      gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]), 0);
      gtk_table_attach (GTK_TABLE (tableLinearToolShade), comboboxDataCh[i], 6, 7, i, i + 1,
			(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			(GtkAttachOptions) (GTK_FILL), 0, 0);
    }

  hbox8 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxTransformation), hbox8, FALSE, FALSE, 0);

  label = gtk_label_new(_("Color range preview: "));
  gtk_box_pack_start (GTK_BOX (hbox8), label, FALSE, FALSE, 0);

  pixbufColorPreview = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE,
				      COLOR_PREVIEW_BITS,
				      COLOR_PREVIEW_WIDTH,
				      COLOR_PREVIEW_HEIGHT);
  colorPreview = create_pixmap ((GtkWidget*)0, NULL);
  gtk_widget_set_size_request (colorPreview, COLOR_PREVIEW_WIDTH,
			       COLOR_PREVIEW_HEIGHT);
  gtk_box_pack_start (GTK_BOX (hbox8), colorPreview, TRUE, TRUE, 0);

  labelPreview = gtk_label_new(_("<span style=\"italic\">No preview available</span>"));
  gtk_label_set_use_markup(GTK_LABEL(labelPreview), TRUE);
  gtk_box_pack_start (GTK_BOX (hbox8), labelPreview, TRUE, TRUE, 0);

  /* Bar */
  alignment2 = gtk_alignment_new(0.5, 0.5, 0.3, 0);
  gtk_box_pack_start(GTK_BOX(vbox), alignment2, FALSE, FALSE, 8);
  hrule = gtk_hseparator_new();
  gtk_container_add(GTK_CONTAINER(alignment2), hrule);

  checkShowLegend = gtk_check_button_new_with_mnemonic(_("Display the colour range."));
  gtk_box_pack_start(GTK_BOX(vboxTransformation), checkShowLegend, FALSE, FALSE, 0);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkShowLegend), TRUE);


  /***********************/
  /* Post treatment part */
  /***********************/
  expanderPostProcessing = gtk_expander_new(_("Post processing:"));
  gtk_widget_set_sensitive(expanderPostProcessing, FALSE);
  gtk_widget_set_name(gtk_expander_get_label_widget(GTK_EXPANDER(expanderPostProcessing)),
		      "label_head");
  gtk_expander_set_expanded(GTK_EXPANDER(expanderPostProcessing), TRUE);
  gtk_box_pack_start(GTK_BOX(vbox), expanderPostProcessing, FALSE, FALSE, 0);

  vboxPostProcessing = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(expanderPostProcessing), vboxPostProcessing);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPostProcessing), hbox3, FALSE, FALSE, 0);

  checkHideMinValues = gtk_check_button_new_with_mnemonic(_("_Hide elements"));
  gtk_box_pack_start(GTK_BOX(hbox3), checkHideMinValues, FALSE, FALSE, 0);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkHideMinValues), FALSE);
  
  label = gtk_label_new(_(" whose value from"));
  gtk_box_pack_start(GTK_BOX(hbox3), label, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPostProcessing), hbox3, FALSE, FALSE, 0);

  label = gtk_label_new(_("col. "));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_box_pack_start(GTK_BOX(hbox3), label, TRUE, TRUE, 0);

  /* The 2 here is to avoid a GTK bug in 2.4. */
  spinHideMinValues = gtk_spin_button_new_with_range(1, 2, 1);
  gtk_box_pack_start(GTK_BOX(hbox3), spinHideMinValues, FALSE, FALSE, 0);

  label = gtk_label_new(_(" is lower than "));
  gtk_box_pack_start(GTK_BOX(hbox3), label, FALSE, FALSE, 0);

  entryHideMinValues = visu_ui_numerical_entry_new(1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryHideMinValues), 10);
  gtk_box_pack_start(GTK_BOX(hbox3), entryHideMinValues, FALSE, FALSE, 0);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPostProcessing), hbox3, FALSE, FALSE, 0);

  checkScaleRadius = gtk_check_button_new_with_mnemonic(_("_Scale shape according to "));
  gtk_box_pack_start(GTK_BOX(hbox3), checkScaleRadius, FALSE, FALSE, 0);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkScaleRadius), FALSE);
  
  comboboxDataCh[3] = gtk_combo_box_text_new();
/*   gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxDataCh[i]), COL_ONE); */
/*   gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxDataCh[i]), COL_COORD_X); */
/*   gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxDataCh[i]), COL_COORD_Y); */
/*   gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxDataCh[i]), COL_COORD_Z); */
/*   gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]), 0); */
  gtk_box_pack_start(GTK_BOX(hbox3), comboboxDataCh[3], TRUE, TRUE, 0);

  hbox3 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPostProcessing), hbox3, FALSE, FALSE, 0);

  checkRestrictInRange = gtk_check_button_new_with_mnemonic(_("_Restrict colourisation"
                                                              " to values in range."));
  gtk_box_pack_start(GTK_BOX(hbox3), checkRestrictInRange, FALSE, FALSE, 0);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRestrictInRange), FALSE);

  /**************/
  /* Status bar */
  /**************/
  statusbarDataFile = gtk_statusbar_new();
  gtk_box_pack_end(GTK_BOX(containerDataFilePanel),
		   statusbarDataFile, FALSE, FALSE, 0);
#if GTK_MAJOR_VERSION < 3 && GTK_MINOR_VERSION < 24
  gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (statusbarDataFile), FALSE);
#endif
  statusDataFileContextId =
    gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbarDataFile),
				 _("Description of loaded data file."));
  gtk_statusbar_push(GTK_STATUSBAR(statusbarDataFile), statusDataFileContextId,
		     DATA_FILE_NO_FILE_MESSAGE);

  gtk_widget_show_all(containerDataFilePanel);
  gtk_widget_hide(colorPreview);

  /********************/
  /* Create callbacks */
  /********************/
  signalCheckDataFile =
    g_signal_connect(G_OBJECT(checkbuttonData), "toggled",
		     G_CALLBACK(useDataFileColor), (gpointer)0);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataLoaded",
		   G_CALLBACK(visuFileReset), (gpointer)0);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReady), (gpointer)0);

  g_signal_connect(G_OBJECT(radioNormalized), "toggled",
		   G_CALLBACK(onScaleTypeChange), (gpointer)VISU_COLORIZATION_NORMALIZE);
  g_signal_connect(G_OBJECT(radioMinMax), "toggled",
		   G_CALLBACK(onScaleTypeChange), (gpointer)VISU_COLORIZATION_MINMAX);
  g_signal_connect(G_OBJECT(entryDataMin), "value-changed",
		   G_CALLBACK(onEntryMinMaxChangeValue), (gpointer)0);
  g_signal_connect(G_OBJECT(entryDataMax), "value-changed",
		   G_CALLBACK(onEntryMinMaxChangeValue), (gpointer)1);
  signalComboColumnId[4] =
    g_signal_connect(G_OBJECT(comboboxDataCh[4]), "changed",
		     G_CALLBACK(onComboManualChange), (gpointer)0);
  signalComboColumnId[5] =
    g_signal_connect(G_OBJECT(comboboxDataCh[5]), "changed",
		     G_CALLBACK(onComboPresetColChange), (gpointer)0);
  for (i = 0; i < 3; i++)
    {
      signalSpinDataChA[i] =
	g_signal_connect(G_OBJECT(spinbuttonDataChA[i]), "value-changed",
			 G_CALLBACK(onSpinChChangeValue), GINT_TO_POINTER(i + 3));
      signalSpinDataChB[i] =
	g_signal_connect(G_OBJECT(spinbuttonDataChB[i]), "value-changed",
			 G_CALLBACK(onSpinChChangeValue), GINT_TO_POINTER(i));
      signalComboColumnId[i] =
	g_signal_connect(G_OBJECT(comboboxDataCh[i]), "changed",
			 G_CALLBACK(onComboColChange), GINT_TO_POINTER(i));
    }
  signalRadioRGB = g_signal_connect(G_OBJECT(radiobuttonDataRGB), "toggled",
				    G_CALLBACK(onColorTypeChange),
				    GINT_TO_POINTER(TOOL_SHADE_COLOR_MODE_RGB));
  signalRadioHSV = g_signal_connect(G_OBJECT(radiobuttonDataHSV), "toggled",
				    G_CALLBACK(onColorTypeChange),
				    GINT_TO_POINTER(TOOL_SHADE_COLOR_MODE_HSV));

  signalComboToolShade = g_signal_connect(G_OBJECT(comboPreSetColorRange),
                                          "shade-selected",
                                          G_CALLBACK(onColorPreSetChange), (gpointer)0);

  g_signal_connect(G_OBJECT(checkHideMinValues), "toggled",
		   G_CALLBACK(onCheckHideMinValuesChange), (gpointer)0);
  g_signal_connect(G_OBJECT(entryHideMinValues), "value-changed",
		   G_CALLBACK(onEntryHideMinValuesChange), (gpointer)0);
  g_signal_connect(G_OBJECT(spinHideMinValues), "value-changed",
		   G_CALLBACK(onSpinHideMinValuesChange), (gpointer)0);
  g_signal_connect(G_OBJECT(checkScaleRadius), "toggled",
		   G_CALLBACK(onCheckScaleRadiusChange), comboboxDataCh[3]);
  signalComboColumnId[3] =
    g_signal_connect(G_OBJECT(comboboxDataCh[3]), "changed",
		     G_CALLBACK(onComboScaleChange), checkScaleRadius);
  g_signal_connect(G_OBJECT(checkRestrictInRange), "toggled",
		   G_CALLBACK(onCheckRestrictChange), (gpointer)0);
  g_signal_connect(G_OBJECT(checkShowLegend), "toggled",
		   G_CALLBACK(onCheckLegendChange), (gpointer)0);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  if (dataObj)
    onDataReady((VisuObject*)0, dataObj, (VisuGlView*)0, (gpointer)0);

  DBG_fprintf(stderr, " | Creation OK.\n");

  gtk_container_add(GTK_CONTAINER(panelDataFile), containerDataFilePanel);
}

static GtkWidget* _createLoadDataDialog()
{
  GtkWidget *file_selector;
  char *directory;
  GtkFileFilter *filter;
  GString *label;

  file_selector = gtk_file_chooser_dialog_new(_("Load data file"),
					      visu_ui_panel_getContainerWindow(VISU_UI_PANEL(panelDataFile)),
					      GTK_FILE_CHOOSER_ACTION_OPEN,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OPEN, GTK_RESPONSE_OK,
					      NULL);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_selector), directory);
  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(file_selector),
				       FALSE);
  filter = gtk_file_filter_new();
  label = g_string_new(_("Data files"));
  g_string_append_printf(label, " (*%s)",
			 gtk_entry_get_text(GTK_ENTRY(entryFileExtension)));
  gtk_file_filter_set_name(filter, label->str);
  g_string_printf(label, "*%s", gtk_entry_get_text(GTK_ENTRY(entryFileExtension)));
  gtk_file_filter_add_pattern(filter, label->str);
  g_string_free(label, TRUE);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(file_selector), filter);
  filter = gtk_file_filter_new();
  gtk_file_filter_set_name(filter, _("All files"));
  gtk_file_filter_add_pattern(filter, "*");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(file_selector), filter);

  gtk_widget_set_name(file_selector, "filesel");
  gtk_window_set_position(GTK_WINDOW(file_selector), GTK_WIN_POS_CENTER_ON_PARENT);
  gtk_window_set_modal (GTK_WINDOW (file_selector), TRUE);

  g_signal_connect(G_OBJECT(file_selector), "response",
                   G_CALLBACK(onLoadDataResponse), (gpointer)0);
  
  return file_selector;
}

static void onLoadDataResponse(GtkDialog *dialog, gint response, gpointer data _U_)
{
  char *filename;
  char *directory;
  int res;
  VisuData *dataObj;
  gboolean new;

  gtk_widget_hide(GTK_WIDGET(dialog));
  directory = (char*)gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER(dialog));
  visu_ui_setLastOpenDirectory(directory, VISU_UI_DIR_DATAFILE);
  g_free(directory);
  
  if (response == GTK_RESPONSE_OK)
    {
      dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
      filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
      DBG_fprintf(stderr, "Panel DataFile: load file '%s'.\n", filename);
      res = visu_ui_panel_colorization_load(dataObj, filename, &new);
      DBG_fprintf(stderr, "Panel DataFile: load OK, new %d.\n", new);
      g_free(filename);

      DBG_fprintf(stderr, "Panel DataFile: update the panel.\n");
      visu_ui_panel_colorization_update(dataObj);
      DBG_fprintf(stderr, "Panel DataFile: update OK.\n");

      /* Ask for a redraw */
      if (res)
	{
          g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
	  DBG_fprintf(stderr, "Panel DataFile: may ask for redraw.\n");
	  /* Update rendering. */
	  if (new)
	    gtk_combo_box_set_active(GTK_COMBO_BOX(comboPreSetColorRange), 0);
	  else
	    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)))
              VISU_REDRAW_ADD;
	}
      else
	{
	  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)))
            {
              applyHideMinValues(dataObj, FALSE);
              VISU_REDRAW_ADD;
            }
	}
    }
}

static void updateNormalisationWidgets(VisuData *data)
{
  GtkWidget *childMinMax;
  int nbColumns;
  int i;
  GtkWidget *table, *label;
  GString *labelStr;
  float minMax[2];
  VisuColorization *dt;

  DBG_fprintf(stderr, "Panel DataFile: update widgets for normalisation.\n");

  /* empty the child of the alignment */
  childMinMax = gtk_bin_get_child(GTK_BIN(readMinMaxValues));
  if (childMinMax)
    gtk_widget_destroy(childMinMax);

  dt = visu_colorization_get(data, FALSE, (gboolean*)0);  

  /* Special case without VisuData or without stored data. */
  if (!visu_colorization_getFileSet(dt))
    {
      label = gtk_label_new(MIN_MAX_NO_DATA_FILE);
      gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
      gtk_widget_show(label);
      gtk_misc_set_alignment(GTK_MISC(label), 0.5, 0.5);
      gtk_container_add(GTK_CONTAINER(readMinMaxValues), label);

      return;
    }

  /* Update the radio buttons. */
  switch (visu_colorization_getScaleType(dt))
    {
    case VISU_COLORIZATION_NORMALIZE:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioNormalized), TRUE);
      break;
    case VISU_COLORIZATION_MINMAX:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioMinMax), TRUE);
      break;
    default:
      break;
    }

  /* Update the min max entries. */
  i = gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[4]));
  if (i >= 0)
    {
      i -= N_COORDS_COLS;
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin),
                                       visu_colorization_getMin(dt, i));
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax),
                                       visu_colorization_getMax(dt, i));
    }

  /* Update the table of min/max values. */
  nbColumns = visu_colorization_getNColumns(dt);
  g_return_if_fail(nbColumns > 0);

  table = gtk_table_new(nbColumns + 1, 3, FALSE);
/*   label = gtk_label_new(_("Column number")); */
/*   gtk_widget_set_name(label, "label_head"); */
/*   gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_SHRINK, GTK_SHRINK, 2, 0); */
  label = gtk_label_new(_("Min value"));
  gtk_widget_set_name(label, "label_head");
  gtk_table_attach(GTK_TABLE(table), label, 1, 2, 0, 1, GTK_EXPAND, GTK_SHRINK, 2, 0);
  label = gtk_label_new(_("Max value"));
  gtk_widget_set_name(label, "label_head");
  gtk_table_attach(GTK_TABLE(table), label, 2, 3, 0, 1, GTK_EXPAND, GTK_SHRINK, 2, 0);

  labelStr = g_string_new("");
  for (i = 0; i < nbColumns; i++)
    {
      g_string_printf(labelStr, _("Column %d"), i + 1);
      label = gtk_label_new(labelStr->str);
      gtk_table_attach(GTK_TABLE(table), label, 0, 1, i + 1, i + 2,
		       GTK_SHRINK, GTK_SHRINK, 2, 0);

      if (visu_colorization_getColumnMinMax(dt, minMax, i))
	{
	  g_string_printf(labelStr, "%g", minMax[0]);
	  label = gtk_label_new(labelStr->str);
	  gtk_label_set_selectable(GTK_LABEL(label), TRUE);
	  gtk_table_attach(GTK_TABLE(table), label, 1, 2, i + 1, i + 2,
			   GTK_EXPAND, GTK_SHRINK, 2, 0);

	  g_string_printf(labelStr, "%g", minMax[1]);
	  label = gtk_label_new(labelStr->str);
	  gtk_label_set_selectable(GTK_LABEL(label), TRUE);
	  gtk_table_attach(GTK_TABLE(table), label, 2, 3, i + 1, i + 2,
			   GTK_EXPAND, GTK_SHRINK, 2, 0);
	}
      else
	g_warning("Can't retrieve min/max values for column %d.\n", i);
    }
  gtk_widget_show_all(table);
  g_string_free(labelStr, TRUE);

  gtk_container_add(GTK_CONTAINER(readMinMaxValues), table);
}
static void updateColorWidgets(VisuData *data)
{
  int i, j;
  GString *label;
  const int *selected;
  int scale;
  float *vectA, *vectB;
  ToolShade *shade;
  VisuColorization *dt;

  DBG_fprintf(stderr, "Panel dataFile: rebuilding colour widgets.\n");
  /* empty combobox */
  g_signal_handler_block(G_OBJECT(comboboxDataCh[0]), signalComboColumnId[0]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[1]), signalComboColumnId[1]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[2]), signalComboColumnId[2]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[3]), signalComboColumnId[3]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[4]), signalComboColumnId[4]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);
  for (j = nbColumns + N_COORDS_COLS - 1; j > N_COORDS_COLS - 1; j--)
    {
      gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(comboboxDataCh[4]), j);
      gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(comboboxDataCh[5]), j);
    }
  for (i = 0; i < 3; i++)
    {
      for (j = nbColumns + N_STATIC_COLS - 1; j > N_STATIC_COLS - 1; j--)
	gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]), j);
      gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]), 0);
      DBG_fprintf(stderr, "Panel dataFile: reset channel %d.\n", i);
    }
  for (j = nbColumns - 1; j > - 1; j--)
    gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(comboboxDataCh[3]), j);
  if (data)
    {
      dt = visu_colorization_get(data, FALSE, (gboolean*)0);
      shade = visu_colorization_getShade(dt);
      if (shade)
	{
	  if (tool_shade_getMode(shade) == TOOL_SHADE_MODE_LINEAR)
	    {
	      tool_shade_getLinearCoeff(shade, &vectA, &vectB);
	      /* Update the vectors. */
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[0]),
					vectA[0]);
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[1]),
					vectA[1]);
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[2]),
					vectA[2]);
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[0]),
					vectB[0]);
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[1]),
					vectB[1]);
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[2]),
					vectB[2]);
	    }
	  gtk_widget_set_sensitive(tableLinearToolShade,
				   (tool_shade_getMode(shade) == TOOL_SHADE_MODE_LINEAR));
	}
      /* Rebuild the column comboboxes. */
      nbColumns = visu_colorization_getNColumns(dt);
      if (nbColumns > 0)
	{
	  label = g_string_new("");
	  for (j = 0; j < nbColumns; j++)
	    {
	      g_string_printf(label, _("Col. %d"), j + 1);
	      for (i = 0; i < 6; i++)
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboboxDataCh[i]),
                                          (const gchar*)0, label->str);
	    }
	  g_string_free(label, TRUE);
	}
      selected = visu_colorization_getColUsed(dt);
      if (selected)
	{
	  for (i = 0; i < 3; i++)
	    {
	      DBG_fprintf(stderr, "Panel dataFile: Channel %d has selection %d.\n",
			  i, selected[i]);
	      gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]),
				       selected[i] + N_STATIC_COLS);
	    }
          if (visu_colorization_getSingleColumnId(dt, &i))
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[5]),
                                     i + N_STATIC_COLS - 1);
	}
      scale = visu_colorization_getScalingUsed(dt);
      if (scale != -N_STATIC_COLS)
	gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[3]),
				 scale + N_STATIC_COLS);
      if (shade)
	{
	  /* Change the colour scheme. */
	  switch (tool_shade_getColorMode(shade))
	    {
	    case TOOL_SHADE_COLOR_MODE_RGB:
	      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonDataRGB),
					   TRUE);
	      for (i = 0; i < 3; i++)
		gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelRGB[i]);
	      break;
	    case TOOL_SHADE_COLOR_MODE_HSV:
	      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonDataHSV),
					   TRUE);
	      for (i = 0; i < 3; i++)
		gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelHSV[i]);
	      break;
	    default:
	      break;
	    }
	  /* Update the preset combo. */
	  g_signal_handler_block(G_OBJECT(comboPreSetColorRange), signalComboToolShade);
	  visu_ui_shade_combobox_setSelectionByShade(VISU_UI_SHADE_COMBOBOX(comboPreSetColorRange),
					    shade);
	  g_signal_handler_unblock(G_OBJECT(comboPreSetColorRange), signalComboToolShade);
	}
    }
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[0]), signalComboColumnId[0]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[1]), signalComboColumnId[1]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[2]), signalComboColumnId[2]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[3]), signalComboColumnId[3]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[4]), signalComboColumnId[4]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[4]), 0);
  /* Update the color preview. */
  makeColorPreview(data);
}
static void updatePostWidgets(VisuData *data)
{
  int nb;
  GtkAdjustment *adj;
  VisuColorization *dt;

  if (!data)
    return;

  DBG_fprintf(stderr, "Panel dataFile: update post-processing widgets.\n");

  dt = visu_colorization_get(data, FALSE, (gboolean*)0);
  nb = MAX(visu_colorization_getNColumns(dt), 1);

  adj = gtk_spin_button_get_adjustment(GTK_SPIN_BUTTON(spinHideMinValues));
  gtk_adjustment_set_upper(adj, nb);
  /* gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinHideMinValues), 1); */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRestrictInRange),
                               visu_colorization_getRestrictInRange(dt));
}
static void makeColorPreview(VisuData *data)
{
  const int *valCols;
  int i, j, id;
  int rowstride, x, y;
  guchar *pixels, *p;
  float rgbValues[COLOR_PREVIEW_WIDTH][4];
  float fromVal[3];
  ToolShade *shade;
  VisuColorization *dt;

  if (!data)
    {
      gtk_image_set_from_pixbuf(GTK_IMAGE(colorPreview), NULL);
      gtk_widget_hide(colorPreview);
      gtk_widget_show(labelPreview);
      return;
    }

  dt = visu_colorization_get(data, FALSE, (gboolean*)0);
  valCols = visu_colorization_getColUsed(dt);
  if (!valCols)
    return;

  if (!visu_colorization_getSingleColumnId(dt, &id))
    {
      gtk_image_set_from_pixbuf(GTK_IMAGE(colorPreview), NULL);
      gtk_widget_hide(colorPreview);
      gtk_widget_show(labelPreview);
      visu_gl_ext_setActive(VISU_GL_EXT(extLeg), FALSE);
    }
  else
    {
      DBG_fprintf(stderr, "Panel dataFile: making color preview bitmap.\n");
      rowstride = gdk_pixbuf_get_rowstride(pixbufColorPreview);
      pixels = gdk_pixbuf_get_pixels(pixbufColorPreview);
      shade = visu_colorization_getShade(dt);

      for (i = 0; i < COLOR_PREVIEW_WIDTH; i++)
	{
	  for (j = 0; j < 3; j++)
	    fromVal[j] = (valCols[j] != -N_STATIC_COLS)?(float)i / (float)(COLOR_PREVIEW_WIDTH - 1):1.;
	  tool_shade_channelToRGB(shade, rgbValues[i], fromVal);
/* 	  fprintf(stderr, "% d ----> %f %f %f -> %f %f %f\n",i, fromVal[0], fromVal[1], fromVal [2], rgbValues[i][0], rgbValues[i][1], rgbValues[i][2]); */
	}

      for (y = 0; y < COLOR_PREVIEW_HEIGHT; y++)
	for (x = 0; x < COLOR_PREVIEW_WIDTH; x++)
	  {
	    p = pixels + y * rowstride + x * 3;
	    p[0] = (guchar)(rgbValues[x][0] * 255);
	    p[1] = (guchar)(rgbValues[x][1] * 255);
	    p[2] = (guchar)(rgbValues[x][2] * 255);
	  }
      gtk_image_set_from_pixbuf(GTK_IMAGE(colorPreview), pixbufColorPreview);

      gtk_widget_hide(labelPreview);
      gtk_widget_show(colorPreview);

      visu_gl_ext_setActive(VISU_GL_EXT(extLeg),
                            gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkShowLegend)) &&
                            gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)));
      visu_gl_ext_shade_setMinMax(extLeg, visu_colorization_getMin(dt, id),
                                  visu_colorization_getMax(dt, id));
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(extLeg),
                                  visu_ui_panel_getView(VISU_UI_PANEL(panelDataFile)));
      visu_gl_ext_shade_setShade(extLeg, shade);
      visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLeg));
    }
}

static void updateStatusBar(VisuData *data)
{
  gchar *fileUTF8, *basename;
  gchar *message;
  VisuColorization *dt;
  gchar *currentDataFile;
  gint nbColumns;

  dt = visu_colorization_get(data, FALSE, (gboolean*)0);
  nbColumns = visu_colorization_getNColumns(dt);
  currentDataFile = visu_colorization_getFile(dt);

  /* Create the text of the status bar or empty it. */
  gtk_statusbar_pop(GTK_STATUSBAR(statusbarDataFile),
		    statusDataFileContextId);
  /* Create the text of the status bar or empty it. */
  if (currentDataFile)
    {
      basename = g_path_get_basename(currentDataFile);
      fileUTF8 = g_filename_to_utf8(basename, -1, NULL, NULL, NULL);
      g_free(basename);
      g_return_if_fail(fileUTF8);

      if (nbColumns > 0)
	message = g_strdup_printf(_("%s: %d column(s)"),
                                  fileUTF8, nbColumns);
      else
	message = g_strdup_printf(_("%s: file error"),
                                  fileUTF8);
      g_free(fileUTF8);
      gtk_statusbar_push(GTK_STATUSBAR(statusbarDataFile),
			 statusDataFileContextId,
			 message);
      g_free(message);
      g_free(currentDataFile);
    }
  else if (nbColumns > 0)
    {
      message = g_strdup_printf(_("%d column(s)"), nbColumns);
      gtk_statusbar_push(GTK_STATUSBAR(statusbarDataFile),
			 statusDataFileContextId,
			 message);
      g_free(message);
    }
  else
    gtk_statusbar_push(GTK_STATUSBAR(statusbarDataFile),
                       statusDataFileContextId,
                       DATA_FILE_NO_FILE_MESSAGE);
}
/**
 * visu_ui_panel_colorization_load:
 * @visuData: the #VisuData object to associated the data to ;
 * @file: the file to read ;
 * @new: return TRUE if the loaded data are associated for the
 *       first time to @visuData.
 *
 * Read the given data file and associate the values of each column to
 * the node. It does not update the widgets of the panel. This is done
 * to allow to change the values of the colouration before drawing the
 * widgets. See visu_ui_panel_colorization_update() to do it.
 *
 * Returns: TRUE if VisuNodeArray::RenderingChanged should be emitted.
 */
gboolean visu_ui_panel_colorization_load(VisuData *visuData, const gchar *file, gboolean *new)
{
  GError *error;
  gchar *errStr;
  VisuColorization *dt;

  DBG_fprintf(stderr, "Panel dataFile: loading a new data file '%s'.\n", file);
  g_return_val_if_fail(visuData && file && new, 0);

  /* If panel has still not been loaded, we do it now. */
  if (!panelDataFileIsInitiated)
    {
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }
  
  error = (GError*)0;
  dt = visu_colorization_new_fromFile(visuData, file, new, &error);
  /* Raise the error dialog if necessary. */
  if (error)
    {
      errStr = g_strdup_printf(_("Reading data file '%s' reports:\n\t%s"),
			       file, error->message);
      visu_ui_raiseWarning(_("Loading a data file"), errStr, (GtkWindow*)0);
      g_free(errStr);
      g_error_free(error);
    }

  if (dt)
    {
      /* Set if the colours are used. */
      visu_colorization_setUsed
	(visuData, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)));
      /* Force applying hidding status if needed. */
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)))
	applyHideMinValues(visuData, FALSE);
      flagDataFileIsLoaded = TRUE;
    }
  else
    flagDataFileIsLoaded = FALSE;
  gtk_widget_set_sensitive(expanderPostProcessing, flagDataFileIsLoaded);

  DBG_fprintf(stderr, "Panel dataFile: load OK.\n");
  return flagDataFileIsLoaded;
}
/**
 * visu_ui_panel_colorization_update:
 * @visuData: the #VisuData object to associated the data to.
 *
 * Update the widgets depending on the colouration associated to the given
 * #VisuData object.
 */
void visu_ui_panel_colorization_update(VisuData *dataObj)
{
  VisuColorization *dt;
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gchar *currentDataFile;
#endif

  DBG_fprintf(stderr, "Panel dataFile: update the widgets"
	      " depending on the data %p.\n", (gpointer)dataObj);

  if (!dataObj)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonData), FALSE);

  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);

  /* Update the widgets of this panel if necessary. */
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  currentDataFile = visu_colorization_getFile(dt);
  if (currentDataFile)
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(openDataFileButton), currentDataFile);
  g_free(currentDataFile);
#endif
  updateStatusBar(dataObj);
  updateNormalisationWidgets(dataObj);
  updateColorWidgets(dataObj);
  updatePostWidgets(dataObj);
  /* currentDataFile is updated by updateStatusBar(). */
  gtk_widget_set_sensitive(vBoxDataFileOption, (dataObj != (VisuData*)0));
  gtk_widget_set_sensitive(expanderPostProcessing,
                           (visu_colorization_getNColumns(dt) > 0));
}

/*************/
/* Callbacks */
/*************/
static void visuFileReset(GObject *obj _U_, VisuData *dataObj, gpointer data _U_)
{
  gchar *fileCpy, *fileExtPosition, *dataFile;
  int res, nbColumns, iCol;
  float minVal, maxVal;
  int dataNormalize;
  int colUsed[3];
  const int *oldCols;
  gboolean new;
  ToolShade *shade, *tmpSh;
  VisuData *dataOld;
  VisuColorization *dt;

  DBG_fprintf(stderr, "Panel dataFile: caught 'dataLoaded' signal.\n");
  if (dataObj)
    {
      /* Store old values, may be useful if data file is autoloaded. */
      DBG_fprintf(stderr, "Panel dataFile: Saving current colour values.\n");
      dataOld = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
      dt = visu_colorization_get(dataOld, FALSE, (gboolean*)0);
      tmpSh = (dataOld)?visu_colorization_getShade(dt):
	visu_ui_shade_combobox_getSelection(VISU_UI_SHADE_COMBOBOX(comboPreSetColorRange));
      shade = (tmpSh)?tool_shade_copy(tmpSh):(ToolShade*)0;
      oldCols = (dataOld)?visu_colorization_getColUsed(dt):(int*)0;
      
      if (oldCols)
	{
	  colUsed[0] = oldCols[0];
	  colUsed[1] = oldCols[1];
	  colUsed[2] = oldCols[2];
	}
      else
	{
	  colUsed[0] = 0;
	  colUsed[1] = 0;
	  colUsed[2] = 0;
	}
      DBG_fprintf(stderr, " | column usage %d ; %d ; %d\n",
		  colUsed[0], colUsed[1], colUsed[2]);
      iCol = gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[4]));
      minVal = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin));
      maxVal = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax));
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioNormalized)))
	dataNormalize = VISU_COLORIZATION_NORMALIZE;
      else
	dataNormalize = VISU_COLORIZATION_MINMAX;
	  
      /* Check the autoload flag. */
      res = FALSE;
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonAutoLoad)))
	{
	  /* Compute the name of the data
	     file from the rendered filename. */
	  fileCpy = g_strdup(visu_data_getFile(dataObj, 0, (ToolFileFormat**)0));
	  fileExtPosition = g_strrstr(fileCpy, ".");
	  if (fileExtPosition)
	    *fileExtPosition = '\0';
	  dataFile =
	    g_strdup_printf("%s%s", fileCpy,
			    gtk_entry_get_text(GTK_ENTRY(entryFileExtension)));
	  DBG_fprintf(stderr, "Panel DataFile: try to load a new data file"
		      " ('%s') from the name of the rendered file.\n", dataFile);
	  res = visu_ui_panel_colorization_load(dataObj, dataFile, &new);
	  g_free(dataFile);
	  g_free(fileCpy);
	}
      else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)))
	{
	  /* Try to reload the same data file. */
	  dataFile = (dataOld)?visu_colorization_getFile(dt):(gchar*)0;
	  if (dataFile)
            {
              res = visu_ui_panel_colorization_load(dataObj, dataFile, &new);
              g_free(dataFile);
            }
	  else
	    res = (colUsed[0] > -N_STATIC_COLS &&
		   colUsed[1] > -N_STATIC_COLS &&
		   colUsed[2] > -N_STATIC_COLS);
	}

      if (res)
	{
	  DBG_fprintf(stderr, "Panel dataFile: restore colour"
		      " values from previous.\n");
          dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);
	  /* Reuse previous colUsed values. */
	  if (shade)
	    visu_colorization_setShade(dt, shade);
	  nbColumns = visu_colorization_getNColumns(dt);
	  if (colUsed[0] < nbColumns)
	    visu_colorization_setColUsed(dt, colUsed[0], 0);
	  if (colUsed[1] < nbColumns)
	    visu_colorization_setColUsed(dt, colUsed[1], 1);
	  if (colUsed[2] < nbColumns)
	    visu_colorization_setColUsed(dt, colUsed[2], 2);
          if (iCol >= 0)
            {
              iCol -= N_COORDS_COLS;
              visu_colorization_setMin(dt, minVal, iCol);
              visu_colorization_setMax(dt, maxVal, iCol);
            }
	  visu_colorization_setScaleType(dt, dataNormalize);
	}

      visu_colorization_setUsed
	(dataObj, 
	 gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)));
    }
  if (visu_ui_panel_getVisible(VISU_UI_PANEL(panelDataFile)))
    {
      flagDisableCallbacks = TRUE;
      visu_ui_panel_colorization_update(dataObj);
      flagDisableCallbacks = FALSE;
    }
}
static void onDataReady(VisuObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel DataFile: caught 'dataRendered' signal,"
	      " connecting local signals.\n");
  
  if (dataObj)
    hide_signal =
      g_signal_connect(G_OBJECT(dataObj), "AskForShowHide",
                       G_CALLBACK(onAskForHideNodes), (gpointer)0);
}
static void onDataNotReady(VisuObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel DataFile: caught 'dataUnRendered' signal,"
	      " disconnecting local signals.\n");
  
  g_signal_handler_disconnect(G_OBJECT(dataObj), hide_signal);
}
#if GTK_MAJOR_VERSION < 3 && GTK_MINOR_VERSION < 5
static void loadDataFile(GtkButton *button _U_, gpointer data _U_)
{
  GtkWidget *file_selector;
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  if (!dataObj)
    {
      g_warning("Can't click here since no visuData is available.\n");
      return;
    }

  file_selector = _createLoadDataDialog();
  gtk_dialog_run (GTK_DIALOG (file_selector));
  gtk_widget_destroy(file_selector);
}
#endif
/**
 * visu_ui_panel_colorization_setUsed:
 * @used: a boolean.
 *
 * Set if the panel is used or not.
 */
void visu_ui_panel_colorization_setUsed(gboolean used)
{
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  if (!dataObj)
    return;

  /* If panel has still not been loaded, we do it now. */
  if (!panelDataFileIsInitiated)
    {
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonData), used);
}
/**
 * visu_ui_panel_colorization_setRangeMode:
 * @mode: auto or manual scaling mode.
 *
 * Set how data are clamp into [0;1].
 *
 * Since: 3.7
 */
void visu_ui_panel_colorization_setRangeMode(VisuColorizationInputScaleId mode)
{
  /* If panel has still not been loaded, we do it now. */
  if (!panelDataFileIsInitiated)
    {
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }
  
  if (mode == VISU_COLORIZATION_NORMALIZE)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioNormalized), TRUE);
  else
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radioMinMax), TRUE);
}
/**
 * visu_ui_panel_colorization_setManualRange:
 * @min: a float value.
 * @max: a float value.
 * @column: the column to apply the manual range to.
 *
 * Set the clamping range to [min;max] for @column. Column ids range
 * from 0 to (max number of column - 1). Use -3, -2 and -1 for x
 * coordinates, y and z.
 *
 * Since: 3.7
 */
void visu_ui_panel_colorization_setManualRange(float min, float max, int column)
{
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  if (!dataObj)
    return;

  /* If panel has still not been loaded, we do it now. */
  if (!panelDataFileIsInitiated)
    {
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }
  /* The column combo should be created here. */
  visu_ui_panel_colorization_update(dataObj);
  
  DBG_fprintf(stderr, "Panel DataFile: set manual range of column %d to [%f;%f].\n",
              column, min, max);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[4]), column + 3);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin), (double)min);
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax), (double)max);
}
static void useDataFileColor(GtkToggleButton *toggle, gpointer data _U_)
{
  gboolean isOn;
  int res;
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  if (!dataObj)
    return;

  isOn = gtk_toggle_button_get_active(toggle);
  res = visu_colorization_setUsed(dataObj, (gboolean)isOn);
  makeColorPreview(dataObj);
  /* visu_gl_ext_setActive(VISU_GL_EXT(extLeg), */
  /*                       gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkShowLegend)) && */
  /*                       isOn); */
  /* visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLeg)); */
  /* Force applying hidding status if needed. */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)))
    applyHideMinValues(dataObj, FALSE);
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onDataFileEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel dataFile: check values on enter.\n");

  if (!panelDataFileIsInitiated)
    {
      DBG_fprintf(stderr, "Panel dataFile: initialize interior.\n");
  
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }

  flagDisableCallbacks = TRUE;
  DBG_fprintf(stderr, "Panel dataFile: set values on enter.\n");
  visu_ui_panel_colorization_update(visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile)));
  flagDisableCallbacks = FALSE;
}
static void onScaleTypeChange(GtkToggleButton *toggle, gpointer data)
{
  int scale, res;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on scale radio buttons.\n");

  if (!gtk_toggle_button_get_active(toggle))
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  scale = GPOINTER_TO_INT(data);
  g_return_if_fail(scale >= VISU_COLORIZATION_NORMALIZE && scale <= VISU_COLORIZATION_MINMAX);

  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);
  res = visu_colorization_setScaleType(dt, scale);
  if (scale != VISU_COLORIZATION_MINMAX)
    {
      gtk_widget_set_sensitive(entryDataMax, FALSE);
      gtk_widget_set_sensitive(entryDataMin, FALSE);
      gtk_widget_set_sensitive(comboboxDataCh[4], FALSE);
    }
  else
    {
      gtk_widget_set_sensitive(entryDataMax, TRUE);
      gtk_widget_set_sensitive(entryDataMin, TRUE);
      gtk_widget_set_sensitive(comboboxDataCh[4], TRUE);
      gtk_expander_set_expanded(GTK_EXPANDER(expanderNormalize), TRUE);
    }
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onComboManualChange(GtkComboBox *combo, gpointer data _U_)
{
  int iCol;
  VisuData *dataObj;
  VisuColorization *dt;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);  

  iCol = gtk_combo_box_get_active(combo);
  if (iCol < 0)
    return;
  iCol -= N_COORDS_COLS;

  flagDisableCallbacks = TRUE;
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMin),
                          (double)visu_colorization_getMin(dt, iCol));
  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryDataMax),
                          (double)visu_colorization_getMax(dt, iCol));
  flagDisableCallbacks = FALSE;
}
static void onEntryMinMaxChangeValue(VisuUiNumericalEntry *entry,
				     double oldValue _U_, gpointer data)
{
  int minMax, res, iCol;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on min/max entries.\n");

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  iCol = gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[4]));
  g_return_if_fail(iCol >= 0);
  iCol -= N_COORDS_COLS;

  minMax = GPOINTER_TO_INT(data);
  g_return_if_fail(minMax == 0 || minMax == 1);
  DBG_fprintf(stderr, "Panel Data File: grep value from GtkEntry for"
	      " the minMax value (%d) of column %d.\n", minMax, iCol);

  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);
  switch (minMax)
    {
    case 0:
      res = visu_colorization_setMin(dt, visu_ui_numerical_entry_getValue(entry), iCol);
      break;
    case 1:
      res = visu_colorization_setMax(dt, visu_ui_numerical_entry_getValue(entry), iCol);
      break;
    default:
      res = 0;
      break;
    }
  if (visu_colorization_getSingleColumnId(dt, &iCol) &&
      visu_gl_ext_shade_setMinMax(extLeg, visu_colorization_getMin(dt, iCol),
                                  visu_colorization_getMax(dt, iCol)))
    visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLeg));
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onSpinChChangeValue(GtkSpinButton *spinbutton, gpointer user_data)
{
  int pos;
  int res;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on spin vect[%d].\n",
	      GPOINTER_TO_INT(user_data));
  /* Disable preset range since manual chance. */
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboPreSetColorRange), -1);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  pos = GPOINTER_TO_INT(user_data);
  g_return_if_fail(pos >= 0 && pos < 6);

  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);
  res = tool_shade_setLinearCoeff(visu_colorization_getShade(dt),
                                  gtk_spin_button_get_value(spinbutton),
                                  pos % 3, pos / 3);
  makeColorPreview(dataObj);
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}

static void onComboPresetColChange(GtkComboBox *combo, gpointer data _U_)
{
  int val;
  VisuData *dataObj;
  VisuColorization *dt;

  DBG_fprintf(stderr, "Panel dataFile: change on combo preset column.\n");

  if (flagDisableCallbacks)
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);

  val = gtk_combo_box_get_active(combo) - N_COORDS_COLS;
  visu_colorization_setColUsed(dt, val, 0);
  visu_colorization_setColUsed(dt, val, 1);
  visu_colorization_setColUsed(dt, val, 2);

  g_signal_handler_block(G_OBJECT(comboboxDataCh[0]), signalComboColumnId[0]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[1]), signalComboColumnId[1]);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[2]), signalComboColumnId[2]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[0]), val + N_STATIC_COLS);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[1]), val + N_STATIC_COLS);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[2]), val + N_STATIC_COLS);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[0]), signalComboColumnId[0]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[1]), signalComboColumnId[1]);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[2]), signalComboColumnId[2]);

  if (gtk_combo_box_get_active(GTK_COMBO_BOX(comboPreSetColorRange)) >= 0)
    {
      makeColorPreview(dataObj);

      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onComboColChange(GtkComboBox *combo, gpointer data)
{
  int pos;
  int res;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on combo column.\n");
  /* Disable preset range since manual chance. */
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboPreSetColorRange), -1);
  g_signal_handler_block(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[5]), -1);
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  pos = GPOINTER_TO_INT(data);
  g_return_if_fail(pos >= 0 && pos < 3);

  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);
  res = visu_colorization_setColUsed(dt, gtk_combo_box_get_active(combo) -
                                  N_STATIC_COLS, pos);
  makeColorPreview(dataObj);
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onColorTypeChange(GtkToggleButton *toggle, gpointer data)
{
  int color, res, i;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on colour type radio buttons.\n");
  /* Desable preset range since manual chance. */
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboPreSetColorRange), -1);

  if (!gtk_toggle_button_get_active(toggle))
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  color = GPOINTER_TO_INT(data);
  g_return_if_fail(color == TOOL_SHADE_COLOR_MODE_RGB || color == TOOL_SHADE_COLOR_MODE_HSV);

  dt = visu_colorization_get(dataObj, FALSE, (gboolean*)0);
  res = tool_shade_setColorMode(visu_colorization_getShade(dt), color);
  if (color == TOOL_SHADE_COLOR_MODE_RGB)
    for (i = 0; i < 3; i++)
      gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelRGB[i]);
  else
    for (i = 0; i < 3; i++)
      gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelHSV[i]);
  makeColorPreview(dataObj);
  if (res)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
/**
 * visu_ui_panel_colorization_setPresetShade:
 * @shade: a #ToolShade object.
 *
 * Change the preset @shade used to colourise the data.
 *
 * Returns: TRUE if a #ToolShade is set.
 */
gboolean visu_ui_panel_colorization_setPresetShade(ToolShade *shade)
{
  float *vectA, *vectB;
  int i, j;
  VisuData *dataObj;
  VisuColorization *dt;
  const int *colUsed;

  DBG_fprintf(stderr, "Panel dataFile: change the colour transformation"
	      " according to shade %p.\n", (gpointer)shade);
  
  if (!shade)
    return FALSE;

  /* If panel has still not been loaded, we do it now. */
  if (!panelDataFileIsInitiated)
    {
      panelDataFileIsInitiated = TRUE;
      createInteriorDataFile();
    }

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);  

  /* Suspend all combo signals during changes */
  g_signal_handler_block(G_OBJECT(radiobuttonDataRGB), signalRadioRGB);
  g_signal_handler_block(G_OBJECT(radiobuttonDataHSV), signalRadioHSV);
  for (i = 0; i < 3; i++)
    {
      g_signal_handler_block(G_OBJECT(comboboxDataCh[i]), signalComboColumnId[i]);
      g_signal_handler_block(G_OBJECT(spinbuttonDataChA[i]), signalSpinDataChA[i]);
      g_signal_handler_block(G_OBJECT(spinbuttonDataChB[i]), signalSpinDataChB[i]);
    }
  g_signal_handler_block(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);
  visu_colorization_setShade(dt, shade);
  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
  switch (tool_shade_getColorMode(shade))
    {
    case TOOL_SHADE_COLOR_MODE_RGB:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonDataRGB), TRUE);
      for (i = 0; i < 3; i++)
	gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelRGB[i]);
      break;
    case TOOL_SHADE_COLOR_MODE_HSV:
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonDataHSV), TRUE);
      for (i = 0; i < 3; i++)
	gtk_label_set_text(GTK_LABEL(labelChannel[i]), labelHSV[i]);
      break;
    default:
      g_warning("Incorrect ToolShadeColorMode in call of 'onColorPreSetChange'.");
    }
  /* j is the selected column. */
  j = gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[5]));
  if (j < 0)
    j = (visu_colorization_getFileSet(dt))?N_STATIC_COLS:N_COORDS_COLS;
  else
    j += N_STATIC_COLS - N_COORDS_COLS;
  if (tool_shade_getMode(shade) == TOOL_SHADE_MODE_LINEAR)
    {
      colUsed = visu_colorization_getColUsed(dt);
      tool_shade_getLinearCoeff(shade, &vectA, &vectB);
      for (i = 0; i < 3; i++)
	{
	  if (vectA[i] != 0.f &&
	      gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[i])) < 1)
	    {
	      visu_colorization_setColUsed(dt, j - N_STATIC_COLS, i);
	      gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]), j);
	    }
          else
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[i]), colUsed[i] + N_STATIC_COLS);
	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChA[i]), vectA[i]);
	  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDataChB[i]), vectB[i]);
	}
    }
  else
    {
      visu_colorization_setColUsed(dt, j - N_STATIC_COLS, 0);
      visu_colorization_setColUsed(dt, j - N_STATIC_COLS, 1);
      visu_colorization_setColUsed(dt, j - N_STATIC_COLS, 2);
    }

  gtk_widget_set_sensitive(expanderTransformation,
			   (tool_shade_getMode(shade) == TOOL_SHADE_MODE_LINEAR));

  /* ReActivate all combo signals after changes */
  g_signal_handler_unblock(G_OBJECT(radiobuttonDataRGB), signalRadioRGB);
  g_signal_handler_unblock(G_OBJECT(radiobuttonDataHSV), signalRadioHSV);
  for (i = 0; i < 3; i++)
    {
      g_signal_handler_unblock(G_OBJECT(comboboxDataCh[i]), signalComboColumnId[i]);
      g_signal_handler_unblock(G_OBJECT(spinbuttonDataChA[i]), signalSpinDataChA[i]);
      g_signal_handler_unblock(G_OBJECT(spinbuttonDataChB[i]), signalSpinDataChB[i]);
    }
  g_signal_handler_unblock(G_OBJECT(comboboxDataCh[5]), signalComboColumnId[5]);

  if (gtk_combo_box_get_active(GTK_COMBO_BOX(comboboxDataCh[5])) < 0 &&
      visu_colorization_getSingleColumnId(dt, &i))
    gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxDataCh[5]), i + N_STATIC_COLS - 1);

  makeColorPreview(dataObj);

  return TRUE;
}
static void onColorPreSetChange(VisuUiShadeCombobox *combo _U_, ToolShade *values,
				gpointer data _U_)
{
  if (visu_ui_panel_colorization_setPresetShade(values) &&
      gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)))
    VISU_REDRAW_ADD;
}
static void applyHideMinValues(VisuData *visuData, gboolean askRedraw)
{
  gboolean redraw;
  VisuColorization *dt;

  g_return_if_fail(visuData);

  /* Wants to change the rendered attributes of Nodes, must
     emit the AskForShowHide signal. */
  DBG_fprintf(stderr, "Panel DataFile : emitting 'AskForShowHide' signal.\n");
  g_signal_handler_block(G_OBJECT(visuData), hide_signal);
  g_signal_emit_by_name(G_OBJECT(visuData), "AskForShowHide", &redraw, NULL);
  g_signal_handler_unblock(G_OBJECT(visuData), hide_signal);
  DBG_fprintf(stderr, " | returned redraw value %d.\n", redraw);

  /* Get the parameters for hiding mode. */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)))
    {
      dt = visu_colorization_get(visuData, FALSE, (gboolean*)0);
      hidingData.column = (guint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinHideMinValues)) - 1;
      hidingData.value = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryHideMinValues));

      redraw = visu_colorization_applyHide(dt, visuData) || redraw;
    }

  if (redraw)
    {
      g_signal_emit_by_name(G_OBJECT(visuData), "VisibilityChanged", NULL);
      if (askRedraw)
        VISU_REDRAW_ADD;
    }
}
static gboolean hideBelow(VisuColorization *dt _U_,
                          const VisuColorizationNodeData *values, gpointer data)
{
  struct _HideBelowData *st = (struct _HideBelowData*)data;
  float *arr;

  g_return_val_if_fail(st->column < values->data->len, FALSE);

  arr = (float*)values->data->data;
  return (arr[st->column] < st->value);
}
static void onCheckHideMinValuesChange(GtkToggleButton *toggle,
				       gpointer data _U_)
{
  VisuData *visuData;
  VisuColorization *dt;

  /* Set or unset the hiding mode for the current colorization. */
  visuData = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  dt = visu_colorization_get(visuData, FALSE, (gboolean*)0);
  if (!dt)
    return;
  if (gtk_toggle_button_get_active(toggle))
    visu_colorization_setHidingFunc(dt, hideBelow, &hidingData, (GDestroyNotify)0);
  else
    visu_colorization_setHidingFunc(dt, (VisuColorizationHidingFunc)0,
                                    (gpointer)0, (GDestroyNotify)0);

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)) ||
      flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel DataFile: toggle check"
	      " button to hide elements.\n");
  
  applyHideMinValues(visuData, TRUE);
}
static void onEntryHideMinValuesChange(VisuUiNumericalEntry *entry,
				       double oldValue _U_, gpointer data _U_)
{
  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)) ||
      !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)) ||
      flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel DataFile : change value on hide"
	      " min values entry : %f\n", visu_ui_numerical_entry_getValue(entry));
  applyHideMinValues(visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile)), TRUE);
}
static void onSpinHideMinValuesChange(GtkSpinButton *spinbutton,
				      gpointer user_data _U_)
{
  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData)) ||
      !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)) ||
      flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel DataFile : change value of spin column"
	      " for hide function : %f\n", gtk_spin_button_get_value(spinbutton));
  applyHideMinValues(visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile)), TRUE);
}
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw,
			      gpointer data _U_)
{
  VisuColorization *dt;

  /* No data, useless to stay here. */
  if (!flagDataFileIsLoaded)
    return;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkHideMinValues)))
    {
      DBG_fprintf(stderr, "Panel DataFile: caught the 'AskForShowHide' signal for"
                  " VisuData %p.\n", (gpointer)visuData);

      dt = visu_colorization_get(visuData, FALSE, (gboolean*)0);
      hidingData.column = (guint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinHideMinValues)) - 1;
      hidingData.value  = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryHideMinValues));
      *redraw = visu_colorization_applyHide(dt, visuData) || *redraw;
    }
}
static void onCheckRestrictChange(GtkToggleButton *toggle, gpointer data _U_)
{
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;
  
  DBG_fprintf(stderr, "Panel dataFile: change restrict in range.\n");

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);  
  if (visu_colorization_setRestrictInRange(dt, gtk_toggle_button_get_active(toggle)))
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onCheckLegendChange(GtkToggleButton *toggle, gpointer data _U_)
{
  gboolean active;

  active = gtk_toggle_button_get_active(toggle) &&
    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonData));
  visu_gl_ext_setActive(VISU_GL_EXT(extLeg), active);
  if (active)
    visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(extLeg));
  VISU_REDRAW_ADD;
}
static void onCheckScaleRadiusChange(GtkToggleButton *toggle, gpointer data)
{
  int i;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change scaling radius appliance.\n");

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);

  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);  
  if (gtk_toggle_button_get_active(toggle))
    {
      i = gtk_combo_box_get_active(GTK_COMBO_BOX(data));
      visu_colorization_setScalingUsed(dt, (i < 0)?-N_STATIC_COLS:i);
    }
  else
    visu_colorization_setScalingUsed(dt, -N_STATIC_COLS);

  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
  VISU_REDRAW_ADD;
}
static void onComboScaleChange(GtkComboBox *combo, gpointer data)
{
  int i;
  gboolean val;
  VisuData *dataObj;
  VisuColorization *dt;

  if (flagDisableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel dataFile: change on scaling radius combo.\n");

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelDataFile));
  g_return_if_fail(dataObj);
  dt = visu_colorization_get(dataObj, TRUE, (gboolean*)0);  

  i = gtk_combo_box_get_active(combo);
  val = visu_colorization_setScalingUsed(dt, (i < 0)?-N_STATIC_COLS:i);
  
  if (val && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data)))
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void onEntryExt(GtkEntry *entry, gchar *key _U_, VisuObject *obj _U_)
{
  gtk_entry_set_text(entry, fileExt);
}
static void exportParameters(GString *data, VisuData* dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_FILEEXT);
  g_string_append_printf(data, "%s[gtk]: %s\n\n", FLAG_PARAMETER_FILEEXT,
			 fileExt);
}
