/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelElements.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <support.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_data.h>
#include <visu_elements.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>
#include <extraGtkFunctions/gtk_elementComboBox.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <coreTools/toolColor.h>
#include <openGLFunctions/light.h>

#include "gtkAtomic.h"
#include "gtkSpin.h"

/**
 * SECTION: panelElements
 * @short_description: The tab where #VisuElement characteristics can
 * be tuned.
 *
 * <para>It is possible to get the list of selected elements by
 * calling visu_ui_panel_elements_getSelected().</para>
 */

/**
 * panelElements:
 *
 * A pointer on the #VisuUiPanel that host the elements chooser.
 */
static GtkWidget *panelElements;

/* Functions used to initialise the
 * specific area in the panel of elements.
 */
static ToolInitFunc listInitRendenringGtkPanelFunc[] = {
  visu_ui_panel_elements_atomic_init,
  visu_ui_panel_elements_spin_init,
  (ToolInitFunc)0};

/* sensitive widgets */
GtkWidget *vBoxElements;
GtkWidget *renderingMethodElements;
static GtkWidget *elementsComboBox;
static GtkWidget *checkRendered;
static GtkWidget *checkMasked;
GtkWidget *widgetVisuElementColor;
gint sortVisuElements(gpointer a, gpointer b);

/* Structure to store the hooks that are required for
   the specific zone for elements. */
struct GtkRenderingInterface_struct
{
/*   setSensitiveFunc sensitiveInterface; */
  VisuUiPanelElementsChangeFunc newElementInterface;
  VisuUiNewWidgetFunc createInterface;
};
typedef struct GtkRenderingInterface_struct GtkRenderingInterface;
GHashTable *listOfRenderingInterfaces;
static VisuUiPanelElementsChangeFunc updateRenderingMethodResources;

/* Local methods. */
static void createInteriorElements(GtkWidget *visu_ui_panel);
static void updateRenderingSpecificPart(VisuRendering *method);
void createCallBacksElements();
void setInitialValuesElements();
char *getDefaultLabelOfElement(VisuElement *element);
/* It adds the interface widget in the subPanel. The variable
   renderingMethodElements pints then to this widget. If
   renderingMethodElements has already a target, this target is
   freed via gtk_widget_destroy. */
static void changeRenderingMethodInterface(GtkWidget* wd);
/* It allows the client to set the method use to update
   user defined values. */
static void setElementChangedFunc(VisuUiPanelElementsChangeFunc func);
/* It reads the value of the interface methods. */
static void getRenderingInterface(VisuRendering *method,
				  VisuUiPanelElementsChangeFunc *change,
				  VisuUiNewWidgetFunc *create);

/* Callbacks */
static void onElementSelected(VisuUiElementCombobox *combo, GList *elements, gpointer data);
static gboolean onElementChanged(GSignalInvocationHint *ihint, guint n_param_values,
                                 const GValue *param_values, gpointer data);
static void resourcesChanged(GObject *object, VisuData *dataObj, gpointer data);
static void colorVisuElementChanged(VisuUiColorCombobox *colorWd, ToolColor *color, gpointer userData);
static void checkVisibilityChanged(GtkToggleButton *button, gpointer data);
static void checkMaskedChanged(GtkToggleButton *button, gpointer data);
static void onDataFocused(GObject *obj, VisuData *dataObj, gpointer userData);
static void onRenderingMethodChanged(GObject *obj, VisuRendering *method,
				     gpointer userData);
static void onColorChanged(VisuUiColorCombobox *colorComboBox, guint colorId, gpointer data);
static void onMaterialChanged(VisuUiColorCombobox *colorComboBox, VisuGlLightMaterial mat, gpointer data);

/**
 * visu_ui_panel_elements_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the element
 * stuff can be done, such as choosing a colour, setting the radius of
 * spheres...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_elements_init(VisuUiMain *ui)
{
  gchar *cl = _("Set elements caracteristics");
  gchar *tl = _("Elements");
  int i;

  panelElements = visu_ui_panel_newWithIconFromPath("Panel_elements", cl, tl,
						"stock-elements_20.png");

  /* Create the list of the available rendering method
     interfaces. */
  listOfRenderingInterfaces = g_hash_table_new_full(g_direct_hash,
						    g_direct_equal,
						    NULL, NULL);
  for (i = 0; listInitRendenringGtkPanelFunc[i]; i++)
    listInitRendenringGtkPanelFunc[i]();

  createInteriorElements(panelElements);
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelElements), TRUE);

  if (!panelElements)
    return (VisuUiPanel*)0;

  /* Create the callbacks of all the sensitive widgets. */
  createCallBacksElements();
  g_signal_connect(G_OBJECT(ui), "DataFocused",
		   G_CALLBACK(onDataFocused), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "renderingChanged",
		   G_CALLBACK(onRenderingMethodChanged), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "resourcesLoaded",
		   G_CALLBACK(resourcesChanged), (gpointer)0);
  g_signal_add_emission_hook(g_signal_lookup("ElementVisibilityChanged", VISU_TYPE_ELEMENT),
                             0, onElementChanged, (gpointer)0, (GDestroyNotify)0);
  g_signal_add_emission_hook(g_signal_lookup("ElementMaterialChanged", VISU_TYPE_ELEMENT),
                             0, onElementChanged, (gpointer)0, (GDestroyNotify)0);
  g_signal_add_emission_hook(g_signal_lookup("ElementPlaneChanged", VISU_TYPE_ELEMENT),
                             0, onElementChanged, (gpointer)0, (GDestroyNotify)0);

  return VISU_UI_PANEL(panelElements);
}

static void createInteriorElements(GtkWidget *visu_ui_panel)
{
  GtkWidget *label, *expand, *image;
  GtkWidget *hbox;
  GtkWidget *align;
  GtkWidget *scrollView;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  scrollView = gtk_scrolled_window_new((GtkAdjustment*)0,
				       (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollView),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollView), GTK_SHADOW_NONE);
  vBoxElements = gtk_vbox_new(FALSE, 0);
  gtk_widget_set_sensitive(vBoxElements, FALSE);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollView), vBoxElements);


  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vBoxElements), hbox, FALSE, FALSE, 5);
  label = gtk_label_new(_("<b>Set caracteristics of: </b>"));
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  /* We create the tree widget that show the methods. */
  elementsComboBox = visu_ui_element_combobox_new(TRUE, FALSE, _("Element '%s'"));
  visu_ui_element_combobox_setUnphysicalStatus(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), elementsComboBox, TRUE, TRUE, 2);

  label = gtk_label_new("");
  gtk_label_set_markup(GTK_LABEL(label), _("<b>Standard resources</b>"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_set_name(label, "label_head_2");
  gtk_box_pack_start(GTK_BOX(vBoxElements), label, FALSE, FALSE, 5);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vBoxElements), hbox, FALSE, FALSE, 0);
/*   label = gtk_label_new(_("Color: ")); */
/*   gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2); */
  widgetVisuElementColor = visu_ui_color_combobox_newWithRanges(TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), widgetVisuElementColor, FALSE, FALSE, 2);
  align = gtk_alignment_new(1., 0.5, 0., 0.);
  gtk_box_pack_start(GTK_BOX(hbox), align, TRUE, TRUE, 2);
  checkRendered = gtk_check_button_new_with_label(_("rendered"));
  gtk_container_add(GTK_CONTAINER(align), checkRendered);
  align = gtk_alignment_new(0.5, 0.5, 0., 0.);
  gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 2);
  checkMasked = gtk_check_button_new();
  gtk_container_add(GTK_CONTAINER(align), checkMasked);
  image = create_pixmap((GtkWidget*)0, "stock-masking.png");
  gtk_container_add(GTK_CONTAINER(checkMasked), image);
  gtk_widget_set_tooltip_text(checkMasked,
		              _("Make nodes sensitive to the masking effect of planes."));

  expand = visu_ui_color_combobox_getRangeWidgets(VISU_UI_COLOR_COMBOBOX(widgetVisuElementColor));
  gtk_box_pack_start(GTK_BOX(vBoxElements), expand, FALSE, FALSE, 0);

  label = gtk_label_new(_("<b>Rendering specific resources</b>"));
  gtk_widget_set_name(label, "label_head_2");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(vBoxElements), label, FALSE, FALSE, 5);

  DBG_fprintf(stderr, "Panel Elements: create the specific widgets.\n");
  renderingMethodElements = (GtkWidget*)0;
  updateRenderingSpecificPart(visu_object_getRendering(VISU_OBJECT_INSTANCE));

  gtk_widget_show_all(scrollView);

  gtk_container_add(GTK_CONTAINER(visu_ui_panel), scrollView);
}

void createCallBacksElements()
{
  g_signal_connect(G_OBJECT(elementsComboBox), "element-selected",
                   G_CALLBACK(onElementSelected), (gpointer)0);
  g_signal_connect(G_OBJECT(widgetVisuElementColor), "color-selected",
		   G_CALLBACK(colorVisuElementChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(widgetVisuElementColor), "material-value-changed",
		   G_CALLBACK(onMaterialChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(widgetVisuElementColor), "color-value-changed",
		   G_CALLBACK(onColorChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(checkRendered), "toggled",
		   G_CALLBACK(checkVisibilityChanged), (gpointer)0);
  g_signal_connect(G_OBJECT(checkMasked), "toggled",
		   G_CALLBACK(checkMaskedChanged), (gpointer)0);
}

/*****************/
/* Miscellaneous */
/*****************/
gint sortVisuElements(gpointer a, gpointer b)
{
  return strcmp(((VisuElement*)a)->name, ((VisuElement*)b)->name);
}

/*************/
/* Callbacks */
/*************/
static void onDataFocused(GObject *obj _U_, VisuData *dataObj, gpointer userData _U_)
{
  DBG_fprintf(stderr,"Panel Element: caught 'DataFocused' signal.\n");
  if (dataObj)
    gtk_widget_set_sensitive(vBoxElements, TRUE);
  else
    gtk_widget_set_sensitive(vBoxElements, FALSE);
}
static void updateRenderingSpecificPart(VisuRendering *method)
{
  VisuUiPanelElementsChangeFunc change;
  VisuUiNewWidgetFunc create;

  change = (VisuUiPanelElementsChangeFunc)0;
  create = (VisuUiNewWidgetFunc)0;
  if (method)
    getRenderingInterface(method, &change, &create);

  DBG_fprintf(stderr,"Panel Element: set the gtk interface for"
	      " the method '%s'.\n", visu_rendering_getName(method, FALSE));
  setElementChangedFunc(change);
  if (create)
    changeRenderingMethodInterface(create());
  else
    changeRenderingMethodInterface((GtkWidget*)0);
}
static void onRenderingMethodChanged(GObject *obj _U_, VisuRendering *method,
				     gpointer userData _U_)
{
  updateRenderingSpecificPart(method);
}
static gboolean onElementChanged(GSignalInvocationHint *ihint _U_, guint n_param_values _U_,
                                 const GValue *param_values, gpointer data _U_)
{
  GList *elements;
  VisuElement *ele;

  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  if (!elements)
    return TRUE;

  ele = VISU_ELEMENT(g_value_get_object(param_values));
  DBG_fprintf(stderr, "Panel Elements: element '%s' has been modified.\n", ele->name);
  if (elements->data != (gpointer)ele)
    {
      g_list_free(elements);
      return TRUE;
    }
  g_list_free(elements);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRendered),
                               visu_element_getRendered(ele));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkMasked),
                               visu_element_getSensitiveToPlanes(ele));
  visu_ui_color_combobox_setRangeColor(VISU_UI_COLOR_COMBOBOX(widgetVisuElementColor), ele->rgb, FALSE);
  visu_ui_color_combobox_setRangeMaterial(VISU_UI_COLOR_COMBOBOX(widgetVisuElementColor),
                            ele->material, FALSE);
  return TRUE;
}
static void onElementSelected(VisuUiElementCombobox *combo _U_, GList *elements,
                              gpointer data _U_)
{
  VisuElement *ele;

  DBG_fprintf(stderr, "Panel Elements: set new list of selected elements to %p.\n",
              (gpointer)elements);
  if (elements)
    {
      ele = (VisuElement*)elements->data;
      DBG_fprintf(stderr, "Panel Element: set the selected VisuElement to '%s'.\n",
                  ele->name);
      visu_ui_color_combobox_setRangeColor(VISU_UI_COLOR_COMBOBOX(widgetVisuElementColor), ele->rgb, FALSE);
      visu_ui_color_combobox_setRangeMaterial(VISU_UI_COLOR_COMBOBOX(widgetVisuElementColor),
                                ele->material, FALSE);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkRendered),
				   visu_element_getRendered(ele));
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkMasked),
				   visu_element_getSensitiveToPlanes(ele));
    }
  /* Force refresh on the specific area. */
  DBG_fprintf(stderr, "Panel Element: update the render widgets for %p.\n",
              (gpointer)elements);
  if (updateRenderingMethodResources)
    updateRenderingMethodResources(elements);
}
static void colorVisuElementChanged(VisuUiColorCombobox *colorWd _U_, ToolColor *color,
			     gpointer userData _U_)
{
  int res;
  gboolean refresh;
  VisuData *dataObj;
  GList *elements, *tmp;

  DBG_fprintf(stderr, "Panel Elements: Catch 'color-selected' signal (%p).\n",
              (gpointer) color);

  /* Get the selected elements. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  g_return_if_fail(elements);
    
  /* If currentSelectedElement is NULL we apply the changes
     on all elements. */
  dataObj = VISU_DATA(visu_ui_panel_getFocused(VISU_UI_PANEL(panelElements)));
  g_return_if_fail(!dataObj || VISU_IS_DATA(dataObj));

  refresh = FALSE;
  for (tmp = elements; tmp; tmp = g_list_next(tmp))
    {
      res = visu_element_setAllRGBValues((VisuElement*)tmp->data, color->rgba);
      if (res > 0)
        g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                              (VisuElement*)tmp->data, NULL);
      refresh = refresh || (res != 0);
    }
  g_list_free(elements);
  if (refresh)
    {
      DBG_fprintf(stderr, "Panel Elements: color change, refresh.\n");
      VISU_REDRAW_ADD;
    }
}
static void onColorChanged(VisuUiColorCombobox *colorComboBox, guint colorId _U_,
			   gpointer data _U_)
{
  int res;
  gboolean refresh;
  VisuData *dataObj;
  float *rgbVal;
  GList *elements, *tmp;

  /* Get the selected elements. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  g_return_if_fail(elements);
    
  /* Get the values. */
  rgbVal = visu_ui_color_combobox_getRangeColor(colorComboBox);

  /* If currentSelectedElement is NULL we apply the changes
     on all elements. */
  dataObj = VISU_DATA(visu_ui_panel_getFocused(VISU_UI_PANEL(panelElements)));
  g_return_if_fail(!dataObj || VISU_IS_DATA(dataObj));

  refresh = FALSE;
  for (tmp = elements; tmp; tmp = g_list_next(tmp))
    {
      res = visu_element_setAllRGBValues((VisuElement*)tmp->data, rgbVal);
      if (res > 0)
        g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                              (VisuElement*)tmp->data, NULL);
      refresh = refresh || (res != 0);
    }
  g_list_free(elements);
  if (refresh)
    {
      DBG_fprintf(stderr, "Panel Elements: RGB change, refresh.\n");
      VISU_REDRAW_ADD;
    }
  g_free(rgbVal);
}
static void onMaterialChanged(VisuUiColorCombobox *colorComboBox, VisuGlLightMaterial mat _U_,
			      gpointer data _U_)
{
  int res;
  int refresh;
  VisuData *dataObj;
  float *matVal;
  GList *elements, *tmp;

  /* Get the selected elements. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  g_return_if_fail(elements);
    
  /* Get the values. */
  matVal = visu_ui_color_combobox_getRangeMaterial(colorComboBox);

  /* If currentSelectedElement is NULL we apply the changes
     on all elements. */
  dataObj = VISU_DATA(visu_ui_panel_getFocused(VISU_UI_PANEL(panelElements)));
  g_return_if_fail(!dataObj || VISU_IS_DATA(dataObj));

  refresh = FALSE;
  for (tmp = elements; tmp; tmp = g_list_next(tmp))
    {
      res = visu_element_setAllMaterialValues((VisuElement*)tmp->data, matVal);
      if (res > 0)
        g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                              (VisuElement*)tmp->data, NULL);
      refresh = refresh || (res != 0);
    }
  g_list_free(elements);
  if (refresh)
    {
      DBG_fprintf(stderr, "Panel Elements: material change, refresh.\n");
      VISU_REDRAW_ADD;
    }
  g_free(matVal);
}
static gboolean askForHide(gpointer data _U_)
{
  VisuData *dataObj;
  gboolean refresh;

  dataObj = VISU_DATA(visu_ui_panel_getFocused(VISU_UI_PANEL(panelElements)));
  g_return_val_if_fail(VISU_IS_DATA(dataObj), FALSE);
  refresh = FALSE;
  g_signal_emit_by_name(G_OBJECT(dataObj), "AskForShowHide", &refresh, NULL);
  if (refresh)
    {
      g_signal_emit_by_name(G_OBJECT(dataObj), "VisibilityChanged", NULL);
      VISU_REDRAW_ADD;
    }
  return FALSE;
}
static void checkMaskedChanged(GtkToggleButton *button, gpointer data _U_)
{
  gboolean refresh;
  GList *elements, *tmp;

  /* Get the selected elements. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  g_return_if_fail(elements);

  refresh = FALSE;
  for (tmp = elements; tmp; tmp = g_list_next(tmp))
    refresh =
      visu_element_setSensitiveToPlanes((VisuElement*)tmp->data,
                                        gtk_toggle_button_get_active(button)) ||
      refresh;
  g_list_free(elements);
  if (refresh)
    g_idle_add(askForHide, (gpointer)0);
}
static void checkVisibilityChanged(GtkToggleButton *button, gpointer data _U_)
{
  int res;
  gboolean refresh;
  VisuData *dataObj;
  GList *elements, *tmp;

  /* Get the selected elements. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  g_return_if_fail(elements);

  dataObj = VISU_DATA(visu_ui_panel_getFocused(VISU_UI_PANEL(panelElements)));
  g_return_if_fail(!dataObj || VISU_IS_DATA(dataObj));

  refresh = FALSE;
  for (tmp = elements; tmp; tmp = g_list_next(tmp))
    {
      res = visu_element_setRendered((VisuElement*)tmp->data,
                                     gtk_toggle_button_get_active(button));
      if (res)
	{
	  refresh = TRUE;
          g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                                (VisuElement*)tmp->data, NULL);
	}
    }
  g_list_free(elements);
  if (refresh)
    VISU_REDRAW_ADD;
}
static void resourcesChanged(GObject *object _U_, VisuData *dataObj _U_,
			     gpointer data _U_)
{
  GList *elements;

  /* The resources loading could have changed ToolColor of the element,
     so we check all the list. */
  DBG_fprintf(stderr, "Panel Elements: catch the 'resourcesChanged'"
	      " signal, checking color pointers.\n");

  /* Get the selected iter. */
  elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
  if (!elements)
    return;

  /* Force refresh on the specific area. */
  if (updateRenderingMethodResources)
    updateRenderingMethodResources(elements);
  
  g_list_free(elements);
}
/**
 * visu_ui_panel_elements_getSelected:
 *
 * This method is used to get a list of selected #VisuElement from the
 * element selector of this panel.
 *
 * Since: 3.6
 *
 * Returns: (transfer container) (element-type VisuElement*): a list
 * of #VisuElement, the list should be freed after use by
 * g_list_free().
 */
GList* visu_ui_panel_elements_getSelected()
{
  return visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
}


/* It allows the client to set the method use to update
   user defined values. */
static void setElementChangedFunc(VisuUiPanelElementsChangeFunc func)
{
  updateRenderingMethodResources = func;
}
/* It adds the interface widget in the subPanel. The variable
   renderingMethodElements pints then to this widget. If
   renderingMethodElements has already a target, this target is
   freed via gtk_widget_destroy. If interface is a null pointer
   the previous renderingMethodElements is removed, but
   nothing replaces it. */
static void changeRenderingMethodInterface(GtkWidget* wd)
{
  GList *elements;

  DBG_fprintf(stderr, "Panel Element: caught 'resourcesLoaded' signal.\n");
  if (renderingMethodElements)
    {
      /* note that gtk_destroy automatically remove
	 renderingMethodElements for its container, so
	 a call to gtk_container_remove is not necessary. */
      DBG_fprintf(stderr, "Panel Element: removing old rendering specific widget.\n");
      gtk_widget_destroy(renderingMethodElements);
    }

  renderingMethodElements = wd;

  if (wd)
    {
      gtk_box_pack_start(GTK_BOX(vBoxElements), wd,
			 FALSE, FALSE, 5);
      gtk_box_reorder_child(GTK_BOX(vBoxElements), wd, 6);
      gtk_widget_show(wd);
    }

  /* Force refresh on the specific area. */
  if (updateRenderingMethodResources)
    {
      elements = visu_ui_element_combobox_getSelection(VISU_UI_ELEMENT_COMBOBOX(elementsComboBox));
      updateRenderingMethodResources(elements);
      g_list_free(elements);
    }
}


/* RenderingMethod gtk interface. */

/**
 * visu_ui_panel_elements_setMethod:
 * @method: a pointer on the method this interface is associated to ;
 * @change: (scope call): the method to be called whenever the element
 * selection is cahnged ;
 * @create: (scope call): the method to be called when the interface must be built.
 *
 * This method allows to initiate the methods to deal with the rendering
 * specific interfaces.
 */
void visu_ui_panel_elements_setMethod(VisuRendering *method,
				       VisuUiPanelElementsChangeFunc change,
				       VisuUiNewWidgetFunc create)
{
  GtkRenderingInterface *wd;

  if (!method)
    return;

  /* Search for an already existing GtkRenderingInterface
     for this name. */
  wd = g_hash_table_lookup(listOfRenderingInterfaces,
				  (gpointer)method);
  if (wd)
    {
      wd->newElementInterface = change;
      wd->createInterface = create;
    }
  else
    {
      wd = g_malloc(sizeof(GtkRenderingInterface));
      wd->newElementInterface = change;
      wd->createInterface = create;
      g_hash_table_insert(listOfRenderingInterfaces,
			  (gpointer)method,
			  (gpointer)wd);
    }
}
/* It reads the value of the interface methods. */
static void getRenderingInterface(VisuRendering *method,
				  VisuUiPanelElementsChangeFunc *change,
				  VisuUiNewWidgetFunc *create)
{
  GtkRenderingInterface *wd;

  *change = (VisuUiPanelElementsChangeFunc)0;
  *create = (VisuUiNewWidgetFunc)0;
  if (!method)
    return;

  wd = g_hash_table_lookup(listOfRenderingInterfaces,
				  (gpointer)method);
  if (wd)
    {
      *change = wd->newElementInterface;
      *create = wd->createInterface;
    }
}
/**
 * visu_ui_panel_elements_getStatic:
 *
 * Retrives a pointer on this #VisuUiPanel.
 *
 * Returns: (transfer none): a pointer owned by V_Sim.
 */
GtkWidget* visu_ui_panel_elements_getStatic()
{
  return panelElements;
}
