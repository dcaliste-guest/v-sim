/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelGeometry.h"
#include "panelBrowser.h"

#include <support.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <visu_data.h>
#include <visu_basic.h>
#include <visu_gtk.h>
#include <gtk_main.h>
#include <gtk_renderingWindowWidget.h>
#include <extensions/box.h>
#include <extensions/node_vectors.h>
#include <extensions/paths.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolPhysic.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>
#include <extraGtkFunctions/gtk_stippleComboBoxWidget.h>
#include <extraGtkFunctions/gtk_shadeComboBoxWidget.h>

/**
 * SECTION: panelGeometry
 * @short_description: This tab gathers the geometry operation on a
 * #VisuData, like periodic translation, physical units, ...
 *
 * <para>Nothing tunable here.</para>
 */

/* Local objects. */
static GtkWidget *panelGeometry;
static GtkWidget *stippleExpandBox, *colorExpandBox;
static GtkWidget *vboxPeriodic;
static GtkWidget *checkAllowTranslations;
static GtkWidget *spinTransXYZ[3];
static GtkWidget *checkAllowExpand;
static GtkWidget *spinExpandXYZ[3];
static GtkWidget *imagePeriodic;
static GtkWidget *labelPeriodic;
static gulong     transId;
static gboolean   transStatus = FALSE;
static GtkWidget *comboUnit;
static GtkWidget *checkDiff, *checkAdjust, *checkOrdering;
static GtkWidget *checkVisuPaths, *hboxVisuPaths, *togglePathSave, *labelVisuPaths;
static GtkWidget *hboxIOVisuPaths, *hboxVisuPaths2, *checkColorisePath, *comboToolShadePath;

/* Local variables. */
static gboolean disableCallbacks;
static gboolean widgetsNotBuilt;
static VisuPaths *paths;
static VisuGlExtPaths *extPaths;
static gchar *exportPathFile = NULL;
static gulong extension_signal, translation_signal;

#define NO_PATH _("<span size=\"small\"><i>No stored path</i></span>")
#define PATH _("<span size=\"small\">Path has %d step(s)</span>")

/* Local routines. */
static GtkWidget *createInteriorBox();
static void updateValues(VisuData *dataObj);
static void updatePeriodic(VisuData *dataObj);
static void applyTranslation(gboolean setFree);
static void applyExpansion(float expansion[3]);
static void getCartesianTranslation(float trans[3]);

/* Local callbacks. */
static void onEnter(VisuUiPanel *visu_ui_panel, gpointer data);
static void onVisuDataChanged(GObject *obj, VisuData* visuData, gpointer data);
static void onDataReady(VisuObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(VisuObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void onDataFocused(GObject *obj, VisuData* visuData, gpointer data);
static void onTranslationChanged(GtkSpinButton* button, gpointer data);
static void onTranslationChecked(GtkToggleButton* button, gpointer data);
static void onExpandChanged(GtkSpinButton* button, gpointer data);
static void onExpandChecked(GtkToggleButton* button, gpointer data);
static gboolean onElementRenderChanged(GSignalInvocationHint *ihint,
                                       guint nVals, const GValue *vals, gpointer data);
static void onStippleExpandChanged(VisuUiStippleCombobox *combo, gint stipple, gpointer data);
static void onColorExpandChanged(VisuUiColorCombobox *combo, ToolColor *color, gpointer data);
static void onUnitChanged(GtkComboBox *combo, gpointer data);
static void onDiffChanged(GtkToggleButton *toggle, gpointer data);
static void onAdjustChanged(GtkToggleButton *toggle, gpointer data);
static void onPathToggled(GtkToggleButton *toggle, gpointer data);
static void onPathSaveToggled(GtkToggleButton *toggle, gpointer data);
static void onPathClearClicked(GtkButton *bt, gpointer data);
static void onColorisePathToggled(GtkToggleButton *bt, gpointer data);
static void onToolShadePathSelected(VisuUiShadeCombobox *combo, ToolShade *shade, gpointer data);
static void onSavePathClicked(GtkButton *bt, gpointer data);
static void onLoadPathClicked(GtkButton *bt, gpointer data);
static void onDirBrowsed(VisuObject *obj, VisuUiDirectoryType type, gpointer user);
static void onExtensionChanged(VisuBox *box, gpointer data);
static void onTransChanged(VisuData *dataObj, gpointer data);

/**
 * visu_ui_panel_geometry_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the box
 * stuff can be tuned, such as the bounding box, its colour, and the actions linked
 * to the periodicity (translation, dupplication...).
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_geometry_init(VisuUiMain *ui)
{
  int i;

  panelGeometry = visu_ui_panel_newWithIconFromPath("Panel_geometry",
						_("Geometry operations"),
						_("Geometry"),
						"stock-geometry_20.png");
  if (!panelGeometry)
    return (VisuUiPanel*)0;

  visu_ui_panel_setDockable(VISU_UI_PANEL(panelGeometry), TRUE);

  /* Create the widgets that are needed even without the GTK interface be built. */
  imagePeriodic = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING,
					   GTK_ICON_SIZE_MENU);
  labelPeriodic = gtk_label_new("");
  vboxPeriodic = gtk_vbox_new(FALSE, 0);
  checkAllowTranslations =
    gtk_check_button_new_with_mnemonic(_("_Translations"));
  checkAllowExpand =
    gtk_check_button_new_with_mnemonic(_("_Expand nodes"));
  for (i = 0; i < 3; i++)
    {
      /* For the translation. */
      spinTransXYZ[i] = gtk_spin_button_new_with_range(-1, 1, 0.05);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[i]), 0);
      gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinTransXYZ[i]), TRUE);
      /* For the expansion. */
      spinExpandXYZ[i] = gtk_spin_button_new_with_range(0, 5, 0.05);
      gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinExpandXYZ[i]), TRUE);
    }
  checkDiff = gtk_check_button_new_with_mnemonic(_("Show node _displacements"));
  checkOrdering = gtk_check_button_new_with_mnemonic(_("with re_ordering"));
  comboUnit = gtk_combo_box_text_new();
  checkVisuPaths = gtk_check_button_new_with_mnemonic(_("Use _paths"));
  togglePathSave = gtk_toggle_button_new();
  stippleExpandBox = visu_ui_stipple_combobox_new();
  colorExpandBox = visu_ui_color_combobox_new(TRUE);
  visu_ui_color_combobox_setPrintValues(VISU_UI_COLOR_COMBOBOX(colorExpandBox), FALSE);
  checkAdjust = gtk_check_button_new_with_mnemonic(_("Automatic zoom _adjustment on file loading"));

  /* Create the callbacks of all the sensitive widgets. */
  g_signal_connect(G_OBJECT(panelGeometry), "page-entered",
		   G_CALLBACK(onEnter), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataLoaded",
		   G_CALLBACK(onVisuDataChanged), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
                   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
                   G_CALLBACK(onDataNotReady), (gpointer)0);
  g_signal_connect(G_OBJECT(ui), "DataFocused",
		   G_CALLBACK(onDataFocused), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "DirectoryChanged",
		   G_CALLBACK(onDirBrowsed), (gpointer)0);
  g_signal_add_emission_hook(g_signal_lookup("ElementVisibilityChanged", VISU_TYPE_ELEMENT),
                             0, onElementRenderChanged, (gpointer)0, (GDestroyNotify)0);

  /* Private parameters. */
  disableCallbacks = FALSE;
  widgetsNotBuilt  = TRUE;
  paths            = (VisuPaths*)0;
  extPaths = visu_gl_ext_paths_new(NULL);

  return VISU_UI_PANEL(panelGeometry);
}

static GtkWidget *createInteriorBox()
{
  GtkWidget *vbox, *hbox, *bt, *wd;
  GtkWidget *label, *align;
  int i;
#define X_LABEL _("dx:")
#define Y_LABEL _("dy:")
#define Z_LABEL _("dz:")
  char *xyz[3];
  const gchar **units;
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  vbox = gtk_vbox_new(FALSE, 0);

  /**************************/
  /* The periodicity stuff. */
  /**************************/
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("<b>Periodic operations</b>"));
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 2);

  gtk_box_pack_end(GTK_BOX(hbox), labelPeriodic, FALSE, FALSE, 2);
  gtk_label_set_use_markup(GTK_LABEL(labelPeriodic), TRUE);
  gtk_misc_set_alignment(GTK_MISC(labelPeriodic), 0.5, 0.5);
  gtk_box_pack_end(GTK_BOX(hbox), imagePeriodic, FALSE, FALSE, 0);

  align = gtk_alignment_new(0.5, 0., 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 0, 15, 0, 0);
  gtk_container_add(GTK_CONTAINER(align), vboxPeriodic);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);
  /* The translations. */
  gtk_widget_set_tooltip_text(checkAllowTranslations,
			      _("Translations are given in box coordinates and nodes are "
				"automatically translated back into the bounding box."));
  gtk_box_pack_start(GTK_BOX(vboxPeriodic), checkAllowTranslations, FALSE, FALSE, 0);
  transId = g_signal_connect(G_OBJECT(checkAllowTranslations), "toggled",
			     G_CALLBACK(onTranslationChecked), (gpointer)0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPeriodic), hbox, FALSE, FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), 3);

  xyz[0] = X_LABEL;
  xyz[1] = Y_LABEL;
  xyz[2] = Z_LABEL;
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(xyz[i]);
      gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 3);
      gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);

      g_signal_connect(G_OBJECT(spinTransXYZ[i]), "value-changed",
		       G_CALLBACK(onTranslationChanged), (gpointer)0);
      gtk_box_pack_start(GTK_BOX(hbox), spinTransXYZ[i], FALSE, FALSE, 0);
    }

  /* The replication. */
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPeriodic), hbox, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(checkAllowExpand,
			      _("The size of the expansion is given in box coordinates."
				" Nodes are automatically translated back into the new"
				" defined area. The drawn bounding box is kept to the"
				" original size."));
  gtk_box_pack_start(GTK_BOX(hbox), checkAllowExpand, TRUE, TRUE, 0);
  g_signal_connect(G_OBJECT(checkAllowExpand), "toggled",
		   G_CALLBACK(onExpandChecked), (gpointer)0);
  /* The rendering parameters. */
  label = gtk_label_new(_("param.:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  /* The stipple pattern. */
  gtk_box_pack_start(GTK_BOX(hbox), stippleExpandBox, FALSE, FALSE, 3);
  g_signal_connect(G_OBJECT(stippleExpandBox), "stipple-selected",
        	   G_CALLBACK(onStippleExpandChanged), (gpointer)0);
  /* The color widget. */
  gtk_box_pack_start(GTK_BOX(hbox), colorExpandBox, FALSE, FALSE, 3);
  g_signal_connect(G_OBJECT(colorExpandBox), "color-selected",
        	   G_CALLBACK(onColorExpandChanged), (gpointer)0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vboxPeriodic), hbox, FALSE, FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(hbox), 3);

  xyz[0] = X_LABEL;
  xyz[1] = Y_LABEL;
  xyz[2] = Z_LABEL;
  for (i = 0; i < 3; i++)
    {
      label = gtk_label_new(xyz[i]);
      gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 3);
      gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);

      g_signal_connect(G_OBJECT(spinExpandXYZ[i]), "value-changed",
		       G_CALLBACK(onExpandChanged), (gpointer)0);
      gtk_box_pack_start(GTK_BOX(hbox), spinExpandXYZ[i], FALSE, FALSE, 0);
    }

  /********************/
  /* The units stuff. */
  /********************/
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("<b>Units and lengths</b>"));
  gtk_widget_set_name(label, "label_head");
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 2);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAdjust),
			       (gboolean)visu_ui_rendering_window_class_getAutoAdjust());
  g_signal_connect(G_OBJECT(checkAdjust), "toggled",
		   G_CALLBACK(onAdjustChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(vbox), checkAdjust, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Set the unit of the file:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);

  gtk_widget_set_sensitive(comboUnit, FALSE);
  units = tool_physic_getUnitNames();
  for (i = 0; units[i]; i++)
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(comboUnit), (const gchar*)0, units[i]);
  gtk_combo_box_set_active(GTK_COMBO_BOX(comboUnit), 0);
  g_signal_connect(G_OBJECT(comboUnit), "changed",
		   G_CALLBACK(onUnitChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), comboUnit, FALSE, FALSE, 0);

  /************************/
  /* The multifile stuff. */
  /************************/
  align = gtk_alignment_new(0.5, 0., 1, 1);
  gtk_alignment_set_padding(GTK_ALIGNMENT(align), 15, 0, 1, 0);
  gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, FALSE, 0);

  label = gtk_label_new(_("<b>Multi file actions</b>"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_set_name(label, "label_head");
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_container_add(GTK_CONTAINER(align), label);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(checkOrdering,
			      _("On load of a new file, reorder the nodes to"
                                " minimize displacements."));
  gtk_box_pack_end(GTK_BOX(hbox), checkOrdering, FALSE, FALSE, 0);
  gtk_widget_set_tooltip_text(checkDiff,
			      _("When a new file is loaded, draw arrows on  nodes that"
				" represent their displacements with respect to their"
				" previous positions."));
  g_signal_connect(G_OBJECT(checkDiff), "toggled",
		   G_CALLBACK(onDiffChanged), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hbox), checkDiff, TRUE, TRUE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(checkVisuPaths), "toggled",
		   G_CALLBACK(onPathToggled), (gpointer)0);
  gtk_widget_set_tooltip_text(checkVisuPaths,
			      _("Store differences between files and plot"
				" them as lines."));
  gtk_box_pack_start(GTK_BOX(hbox), checkVisuPaths, TRUE, TRUE, 0);
  hboxVisuPaths = gtk_hbox_new(FALSE, 2);
  gtk_widget_set_sensitive(hboxVisuPaths, FALSE);
  gtk_box_pack_start(GTK_BOX(hbox), hboxVisuPaths, FALSE, FALSE, 3);
  labelVisuPaths = gtk_label_new(NO_PATH);
  gtk_label_set_use_markup(GTK_LABEL(labelVisuPaths), TRUE);
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths), labelVisuPaths, FALSE, FALSE, 3);
  gtk_widget_set_tooltip_text(togglePathSave, _("When toggled, store differences"
						" between files as paths"
						" through nodes.."));
  gtk_container_add(GTK_CONTAINER(togglePathSave),
		    gtk_image_new_from_stock(GTK_STOCK_MEDIA_RECORD,
					     GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(togglePathSave), "toggled",
		   G_CALLBACK(onPathSaveToggled), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths), togglePathSave, FALSE, FALSE, 0);
  bt = gtk_button_new();
  gtk_widget_set_tooltip_text(bt, _("Remove all stored paths."));
  gtk_container_add(GTK_CONTAINER(bt),
		    gtk_image_new_from_stock(GTK_STOCK_CLEAR,
					     GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onPathClearClicked), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths), bt, FALSE, FALSE, 0);

  hboxVisuPaths2 = gtk_hbox_new(FALSE, 0);
  gtk_widget_set_sensitive(hboxVisuPaths2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), hboxVisuPaths2, FALSE, FALSE, 0);

  align = gtk_alignment_new(1., 0.5, 0, 1);
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths2), align, TRUE, TRUE, 0);
  checkColorisePath = bt = gtk_check_button_new_with_mnemonic(_("colourise with: "));
  gtk_widget_set_tooltip_text(bt, _("If energy information was present"
				    " when loading file, colourise the paths"
				    " with shading colours."));
  gtk_container_add(GTK_CONTAINER(align), bt);
  comboToolShadePath = wd = visu_ui_shade_combobox_new(FALSE, FALSE);
  gtk_widget_set_sensitive(wd, FALSE);
  g_signal_connect(G_OBJECT(bt), "toggled",
		   G_CALLBACK(onColorisePathToggled), (gpointer)wd);
  g_signal_connect(G_OBJECT(wd), "shade-selected",
		   G_CALLBACK(onToolShadePathSelected), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths2), wd, FALSE, FALSE, 0);
  hboxIOVisuPaths = gtk_hbox_new(FALSE, 2);
/*   gtk_widget_set_sensitive(hboxIOVisuPaths, FALSE); */
  gtk_box_pack_start(GTK_BOX(hboxVisuPaths2), hboxIOVisuPaths, FALSE, FALSE, 3);
  bt = gtk_button_new();
  gtk_widget_set_tooltip_text(bt, _("Read a set of paths from a file and"
				    " add them to the current set."));
  gtk_container_add(GTK_CONTAINER(bt),
		    gtk_image_new_from_stock(GTK_STOCK_OPEN,
					     GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onLoadPathClicked), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxIOVisuPaths), bt, FALSE, FALSE, 0);
  bt = gtk_button_new();
  gtk_widget_set_tooltip_text(bt, _("Save the current set of paths"
				    " to an XML file."));
  gtk_container_add(GTK_CONTAINER(bt),
		    gtk_image_new_from_stock(GTK_STOCK_SAVE,
					     GTK_ICON_SIZE_MENU));
  g_signal_connect(G_OBJECT(bt), "clicked",
		   G_CALLBACK(onSavePathClicked), (gpointer)0);
  gtk_box_pack_start(GTK_BOX(hboxIOVisuPaths), bt, FALSE, FALSE, 0);

  gtk_widget_show_all(vbox);

  widgetsNotBuilt = FALSE;

  return vbox;
}
static void updateSensitive(VisuData *dataObj)
{
  gboolean per[3], setTrans, setExpand;
  VisuBoxBoundaries bc;

  setExpand = TRUE;
  setTrans  = !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowExpand));
  if (dataObj)
    {
      bc = visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(dataObj)));
      per[0] = (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_YZ) || (bc == VISU_BOX_WIRE_X);
      per[1] = (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_ZX) || (bc == VISU_BOX_WIRE_Y);
      per[2] = (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_XY) || (bc == VISU_BOX_WIRE_Z);
      if (bc == VISU_BOX_FREE)
        {
          setTrans = setExpand = FALSE;
          gtk_widget_show(imagePeriodic);
        }
      else
        gtk_widget_hide(imagePeriodic);
    }
  else
    {
      gtk_widget_hide(imagePeriodic);
      setTrans = setExpand = FALSE;
      per[0] = FALSE;
      per[1] = FALSE;
      per[2] = FALSE;
    }
  gtk_widget_set_sensitive(checkAllowTranslations, setTrans);
  gtk_widget_set_sensitive(spinTransXYZ[0], setTrans && per[0]);
  gtk_widget_set_sensitive(spinTransXYZ[1], setTrans && per[1]);
  gtk_widget_set_sensitive(spinTransXYZ[2], setTrans && per[2]);
  gtk_widget_set_sensitive(checkAllowExpand, setExpand);
  gtk_widget_set_sensitive(spinExpandXYZ[0], setExpand && per[0]);
  gtk_widget_set_sensitive(spinExpandXYZ[1], setExpand && per[1]);
  gtk_widget_set_sensitive(spinExpandXYZ[2], setExpand && per[2]);
}
static void updateValues(VisuData *dataObj)
{
  int i;
  float *transXYZ, transBox[3], expand[3];
  gboolean set;
  guint16 stipple;
  VisuBox *box;
  ToolColor *color;

  disableCallbacks = TRUE;
  DBG_fprintf(stderr, "Panel Geometry: update inside values.\n");

  stipple = visu_gl_ext_box_getExpandStipple(visu_gl_ext_box_getDefault());
  if (!visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(stippleExpandBox), stipple))
    {
      visu_ui_stipple_combobox_add(VISU_UI_STIPPLE_COMBOBOX(stippleExpandBox), stipple);
      visu_ui_stipple_combobox_setSelection(VISU_UI_STIPPLE_COMBOBOX(stippleExpandBox), stipple);
    }

  color = tool_color_addFloatRGBA(visu_gl_ext_box_getSideRGB(visu_gl_ext_box_getDefault()),
                                  (int*)0);
  visu_ui_color_combobox_setSelection(VISU_UI_COLOR_COMBOBOX(colorExpandBox), color);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAdjust),
                               visu_ui_rendering_window_class_getAutoAdjust());
  
  /* Values dependent on a visuData. */
  DBG_fprintf(stderr, "Panel Geometry: update values for object %p.\n",
	      (gpointer)dataObj);
  if (dataObj)
    {
      box = visu_boxed_getBox(VISU_BOXED(dataObj));
      transXYZ = visu_data_getXYZtranslation(dataObj);
      visu_box_convertXYZtoBoxCoordinates(box, transBox, transXYZ);
      set = visu_data_getTranslationStatus(dataObj);
      if (set)
        for (i = 0; i < 3; i++)
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[i]),
                                    (double)transBox[i]);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations),
                                   set);
      g_free(transXYZ);
      if (visu_box_getBoundary(box) != VISU_BOX_FREE)
	{
	  visu_box_getExtension(box, expand);
	  set = (expand[0] != 0. || expand[1] != 0. || expand[2] != 0.);
	  if (set)
	    for (i = 0; i < 3; i++)
	      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinExpandXYZ[i]),
					(double)expand[i]);
          if (set)
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowExpand), TRUE);
	}
      gtk_combo_box_set_active(GTK_COMBO_BOX(comboUnit),
			       visu_box_getUnit(visu_boxed_getBox(VISU_BOXED(dataObj))));
      gtk_widget_set_sensitive(comboUnit, TRUE);
    }
  else
    {
      for (i = 0; i < 3; i++)
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[i]), 0.);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations), FALSE);
      for (i = 0; i < 3; i++)
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinExpandXYZ[i]), 0.);
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowExpand), FALSE);
      gtk_widget_set_sensitive(comboUnit, FALSE);
    }
  disableCallbacks = FALSE;
  
  /* Set the sensitivity of the translation. */
  updateSensitive(dataObj);
}
static void updatePeriodic(VisuData *dataObj)
{
  VisuBoxBoundaries bc;
  gchar *lbl;

  if (dataObj)
    {
      bc = visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(dataObj)));
      switch (bc)
        {
        case VISU_BOX_FREE:
          lbl = g_markup_printf_escaped("<i>%s</i>", _("non periodic data"));
          break;
        case VISU_BOX_WIRE_X:
          lbl = g_strdup(_("(wire X)"));
          break;
        case VISU_BOX_WIRE_Y:
          lbl = g_strdup(_("(wire Y)"));
          break;
        case VISU_BOX_WIRE_Z:
          lbl = g_strdup(_("(wire Z)"));
          break;
        case VISU_BOX_SURFACE_XY:
          lbl = g_strdup(_("(surface XY)"));
          break;
        case VISU_BOX_SURFACE_YZ:
          lbl = g_strdup(_("(surface YZ)"));
          break;
        case VISU_BOX_SURFACE_ZX:
          lbl = g_strdup(_("(surface ZX)"));
          break;
        case VISU_BOX_PERIODIC:
          lbl = g_strdup(_("(periodic)"));
          break;
        default:
          lbl = g_strdup("");
        }
    }
  else
    lbl = g_strdup("");
  gtk_label_set_markup(GTK_LABEL(labelPeriodic), lbl);
  g_free(lbl);
}
static void updateGeometry(VisuData *visuData)
{
  VisuData *dataRef;
  gboolean res, new;
  gchar *text;

  dataRef = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  DBG_fprintf(stderr, "Panel Geometry: updating geometry between %p and %p.\n",
              (gpointer)dataRef, (gpointer)visuData);
  if ((gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkDiff)) ||
       gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(togglePathSave)))&& dataRef)
    {
      res = visu_geodiff_new(dataRef, visuData, gtk_toggle_button_get_active
                             (GTK_TOGGLE_BUTTON(checkOrdering)));
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkDiff)))
        {
          visu_gl_ext_node_vectors_setData(visu_gl_ext_geodiff_getDefault(),
                                           visuData);
          visu_gl_ext_node_vectors_draw(visu_gl_ext_geodiff_getDefault());
        }
      if (res && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(togglePathSave)))
	{
	  g_return_if_fail(paths);

	  new = visu_paths_addFromDiff(paths, visuData);
	  if (new)
	    visu_paths_constrainInBox(paths, visuData);
	  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkVisuPaths)))
            {
              visu_gl_ext_paths_setDirty(extPaths);
              visu_gl_ext_paths_draw(extPaths);
            }
	  text = g_strdup_printf(PATH, visu_paths_getLength(paths));
	  gtk_label_set_markup(GTK_LABEL(labelVisuPaths), text);
	  g_free(text);
	}
    }
}
static void applyTranslation(gboolean setFree)
{
  VisuData *data;
  float cartCoord[3];
  gboolean rebuild;

  data = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  if (!data)
    return;

  DBG_fprintf(stderr, "Visu Ui Geometry: apply translation (setFree = %d).\n", setFree);
  g_signal_handler_block(G_OBJECT(data), translation_signal);
  if (setFree)
    {
      cartCoord[0] = 0.f;
      cartCoord[1] = 0.f;
      cartCoord[2] = 0.f;
      rebuild = (gboolean)visu_data_setXYZtranslation(data, cartCoord);
      rebuild = visu_data_constrainedFree(data) || rebuild;
    }
  else
    {
      getCartesianTranslation(cartCoord);
      rebuild = (gboolean)visu_data_setXYZtranslation(data, cartCoord);
      rebuild = visu_data_constrainedInTheBox(data) || rebuild;
    }
  g_signal_handler_unblock(G_OBJECT(data), translation_signal);

  /* Update the paths if necessary. */
  if (paths)
    {
      visu_paths_setTranslation(paths, cartCoord);
      visu_paths_constrainInBox(paths, data);
      visu_gl_ext_paths_setDirty(extPaths);
      visu_gl_ext_paths_draw(extPaths);
    }

  if (rebuild)
    {
      g_signal_emit_by_name(G_OBJECT(data), "PositionChanged", (VisuElement*)0, NULL);
      VISU_REDRAW_ADD;
    }
}
static void applyExpansion(float expansion[3])
{
  VisuData *data;
  VisuGlView *view;
  gboolean redraw;

  data = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  view = visu_ui_panel_getView(VISU_UI_PANEL(panelGeometry));
  if (!data)
    return;

  redraw = visu_data_replicate(data, expansion);
  if (view && redraw)
    VISU_REDRAW_ADD;
}
static void getCartesianTranslation(float trans[3])
{
  float transBox[3];
  VisuData *data;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowTranslations)))
    {
      transBox[0] = (float)gtk_spin_button_get_value
	(GTK_SPIN_BUTTON(spinTransXYZ[0]));
      transBox[1] = (float)gtk_spin_button_get_value
	(GTK_SPIN_BUTTON(spinTransXYZ[1]));
      transBox[2] = (float)gtk_spin_button_get_value
	(GTK_SPIN_BUTTON(spinTransXYZ[2]));
      data = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
      visu_box_convertBoxCoordinatestoXYZ(visu_boxed_getBox(VISU_BOXED(data)), trans, transBox);
    }
  else
    {
      trans[0] = 0.f;
      trans[1] = 0.f;
      trans[2] = 0.f;
    }
}

/*************/
/* Callbacks */
/*************/
static void onEnter(VisuUiPanel *visu_ui_panel _U_, gpointer data _U_)
{
  VisuData *dataObj;

  if (widgetsNotBuilt)
    {
      DBG_fprintf(stderr, "Panel Geometry: first build on enter.\n");
      gtk_container_add(GTK_CONTAINER(panelGeometry), createInteriorBox());
    }
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  updateValues(dataObj);
  updatePeriodic(dataObj);
  updateSensitive(dataObj);
}
static void onStippleExpandChanged(VisuUiStippleCombobox *combo _U_, gint stipple,
				   gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_box_setExpandStipple(visu_gl_ext_box_getDefault(), stipple))
    {
      visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());
      VISU_REDRAW_ADD;
    }
}
static void onColorExpandChanged(VisuUiColorCombobox *combo _U_, ToolColor *color, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (visu_gl_ext_box_setSideRGB(visu_gl_ext_box_getDefault(), color->rgba,
                                 TOOL_COLOR_MASK_RGBA))
    {
      visu_gl_ext_box_draw(visu_gl_ext_box_getDefault());
      VISU_REDRAW_ADD;
    }  
}
static void onVisuDataChanged(GObject *obj _U_, VisuData* visuData, gpointer data _U_)
{
  float cartCoord[3];
  float transBox[3];
  float expand[3];
  int i, val;
  VisuData *dataRef;
  VisuBox *box;
  
  DBG_fprintf(stderr, "Panel Geometry: Catch 'dataLoaded' signal, update.\n");

  if (!visuData)
    return;

  dataRef = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  box = visu_boxed_getBox(VISU_BOXED(visuData));
  /* Apply the periodic stuffs. */
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowTranslations)) &&
      (visu_box_getBoundary(box) != VISU_BOX_FREE && dataRef &&
       visu_box_getBoundary(visu_boxed_getBox(VISU_BOXED(dataRef))) != VISU_BOX_FREE))
    {
      DBG_fprintf(stderr, "Panel Geometry: Catch 'dataLoaded' signal,"
		  " applying current translations.\n");
      for (i = 0; i < 3; i++)
	transBox[i] =
	  (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinTransXYZ[i]));
      visu_box_convertBoxCoordinatestoXYZ(box, cartCoord, transBox);
      visu_data_setXYZtranslation(visuData, cartCoord);
      visu_data_constrainedInTheBox(visuData);
    }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowExpand)) &&
      visu_box_getBoundary(box) != VISU_BOX_FREE)
    {
      DBG_fprintf(stderr, "Panel Geometry: Catch 'dataLoaded' signal,"
		  " applying current expansion.\n");
      for (i = 0; i < 3; i++)
	expand[i] =
	  (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinExpandXYZ[i]));
      visu_data_replicate(visuData, expand);
    }

  /* Update the units if necessary. */
  if (visu_basic_getPreferedUnit() == TOOL_UNITS_UNDEFINED)
    {
      val = gtk_combo_box_get_active(GTK_COMBO_BOX(comboUnit));
      if (val > 0 && visu_box_getUnit(box) != TOOL_UNITS_UNDEFINED)
	visu_box_setUnit(box, (ToolUnits)val);
    }

  /* Make a geometry diff. */
  updateGeometry(visuData);
}
static void onDataReady(VisuObject *visu _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data _U_)
{
  if (dataObj)
    {
      extension_signal =
        g_signal_connect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), "ExtensionChanged",
                         G_CALLBACK(onExtensionChanged), (gpointer)dataObj);
      translation_signal =
        g_signal_connect(G_OBJECT(dataObj), "TranslationsChanged",
                         G_CALLBACK(onTransChanged), (gpointer)dataObj);
    }
}
static void onDataNotReady(VisuObject *visu _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  g_signal_handler_disconnect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), extension_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), translation_signal);
}
static void onDataFocused(GObject *obj _U_, VisuData* visuData, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel Geometry: Catch 'DataFocused' signal,"
	      " update values.\n");
  if (visuData)
    {
      /* Update the sensitivity of the periodic stuff. */
      updatePeriodic(visuData);
      updateSensitive(visuData);
      updateValues(visuData);
    }
}
static void onTranslationChanged(GtkSpinButton* button _U_, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowTranslations)))
    return;

  applyTranslation(FALSE);
}
static void onTranslationChecked(GtkToggleButton* button, gpointer data _U_)
{
  if (disableCallbacks)
    return;

  applyTranslation(!gtk_toggle_button_get_active(button));
}
static gboolean onElementRenderChanged(GSignalInvocationHint *ihint _U_,
                                       guint nVals _U_, const GValue *vals,
                                       gpointer data _U_)
{
  VisuData *dataObj;
  VisuElement *element;
				   
  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowTranslations)))
    return TRUE;

  element = VISU_ELEMENT(g_value_get_object(vals + 0));
  if (element->rendered)
    {
      DBG_fprintf(stderr, "Panel Geometry: caught 'ElementVisibilityChanged' applying"
		  " translation for element '%s'.\n", element->name);
      dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
      if (visu_data_constrainedElementInTheBox(dataObj, element))
        g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", element, NULL);
    }
  else
    DBG_fprintf(stderr, "Panel Geometry: caught 'ElementVisibilityChanged' but"
		" do not apply translation since element '%s' is masked.\n",
		element->name);

  return TRUE;
}
static void onExpandChanged(GtkSpinButton* button _U_, gpointer data _U_)
{
  float expansion[3];
  int i;

  if (disableCallbacks)
    return;

  if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowExpand)))
    return;

  for (i = 0; i < 3; i++)
    expansion[i] =
      (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinExpandXYZ[i]));
  applyExpansion(expansion);
}
static void onExpandChecked(GtkToggleButton* button, gpointer data _U_)
{
  float expansion[3];
  float transBox[3], cartCoord[3];
  float transZero[3] = { 0.f, 0.f, 0.f};
  VisuData *dataObj;
  gboolean rebuildTrans, redraw;

  if (disableCallbacks)
    return;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  if (!dataObj)
    return;

  disableCallbacks = TRUE;
  redraw = rebuildTrans = FALSE;
  if (gtk_toggle_button_get_active(button))
    {
      /* Before applying expansion we put everything into the box
	 if it is not there yet. */
      if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkAllowTranslations)))
	{
	  gtk_toggle_button_set_active
	    (GTK_TOGGLE_BUTTON(checkAllowTranslations), TRUE);
          transBox[0] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinTransXYZ[0]));
          transBox[1] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinTransXYZ[1]));
          transBox[2] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinTransXYZ[2]));
	  visu_box_convertBoxCoordinatestoXYZ(visu_boxed_getBox(VISU_BOXED(dataObj)), cartCoord, transBox);
	  rebuildTrans = (gboolean)visu_data_setXYZtranslation(dataObj, cartCoord);
	  rebuildTrans = visu_data_constrainedInTheBox(dataObj) || rebuildTrans;
	  transStatus = FALSE;
	}
      else
        transStatus = TRUE;

      expansion[0] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinExpandXYZ[0]));
      expansion[1] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinExpandXYZ[1]));
      expansion[2] = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinExpandXYZ[2]));
      redraw = visu_data_replicate(dataObj, expansion);
    }
  else
    {
      redraw = visu_data_restore(dataObj);

      /* We release the translation if not checked. */
      if (!transStatus)
	{
	  gtk_toggle_button_set_active
	    (GTK_TOGGLE_BUTTON(checkAllowTranslations), FALSE);
	  rebuildTrans = (gboolean)visu_data_setXYZtranslation(dataObj, transZero);
	  rebuildTrans = visu_data_constrainedFree(dataObj) || rebuildTrans;
	}
    }
  disableCallbacks = FALSE;

  if (rebuildTrans)
    g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", (VisuElement*)0, NULL);
  if (rebuildTrans || redraw)
    VISU_REDRAW_ADD;

  /* Set the sensitivity of the translation. */
  updateSensitive(dataObj);
}
static void onExtensionChanged(VisuBox *box _U_, gpointer data)
{
  updateValues(VISU_DATA(data));
}
static void onTransChanged(VisuData *dataObj, gpointer data _U_)
{
  updateValues(dataObj);
}
static void onUnitChanged(GtkComboBox *combo, gpointer data _U_)
{
  gint val;
  ToolUnits unit;
  VisuData *dataObj;
  VisuGlView *view;

  val = gtk_combo_box_get_active(combo);
  g_return_if_fail(val >= 0);

  unit = (ToolUnits)val;
  
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  view = visu_ui_panel_getView(VISU_UI_PANEL(panelGeometry));
  g_return_if_fail(dataObj && view);

  if (visu_box_setUnit(visu_boxed_getBox(VISU_BOXED(dataObj)), unit))
    VISU_REDRAW_ADD;
}
static void onAdjustChanged(GtkToggleButton *toggle, gpointer data _U_)
{
  visu_ui_rendering_window_class_setAutoAdjust(gtk_toggle_button_get_active(toggle));
}
static void onDiffChanged(GtkToggleButton *toggle, gpointer data _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_geodiff_getDefault()),
                           gtk_toggle_button_get_active(toggle));
  visu_gl_ext_node_vectors_draw(visu_gl_ext_geodiff_getDefault());
  VISU_REDRAW_ADD;
}
static void onPathToggled(GtkToggleButton *toggle, gpointer data _U_)
{
  gboolean used;
  float t[3];

  used = gtk_toggle_button_get_active(toggle);
  gtk_widget_set_sensitive(hboxVisuPaths, used);
  gtk_widget_set_sensitive(hboxVisuPaths2, used);

  if (!paths)
    {
      getCartesianTranslation(t);
      paths = visu_paths_new(t);
      visu_gl_ext_paths_set(extPaths, paths);
    }

  visu_gl_ext_setActive(VISU_GL_EXT(extPaths), used);
  VISU_REDRAW_ADD;
}
static void pinPath()
{
  VisuData *dataObj;

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  if (dataObj && paths)
    visu_paths_pinPositions(paths, dataObj);
  visu_ui_panel_browser_setMessage(_("Recording paths"), GTK_MESSAGE_INFO);
}
static void onPathSaveToggled(GtkToggleButton *toggle, gpointer data _U_)
{
  if (gtk_toggle_button_get_active(toggle))
    pinPath();
  else
    visu_ui_panel_browser_setMessage((const gchar*)0, GTK_MESSAGE_INFO);
}
static void onDirBrowsed(VisuObject *obj _U_, VisuUiDirectoryType type, gpointer user _U_)
{
  if (type == VISU_UI_DIR_BROWSER)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(togglePathSave), FALSE);
}
static void onPathClearClicked(GtkButton *bt _U_, gpointer data _U_)
{
  if (paths)
    {
      visu_paths_empty(paths);
      visu_gl_ext_paths_setDirty(extPaths);
      visu_gl_ext_paths_draw(extPaths);
    }
  gtk_label_set_markup(GTK_LABEL(labelVisuPaths), NO_PATH);

  VISU_REDRAW_ADD;
}
static void onColorisePathToggled(GtkToggleButton *bt, gpointer data)
{
  gboolean set;
  ToolShade *shade;

  set = gtk_toggle_button_get_active(bt);

  gtk_widget_set_sensitive(GTK_WIDGET(data), set);
  shade = (set)?visu_ui_shade_combobox_getSelection(VISU_UI_SHADE_COMBOBOX(data)):(ToolShade*)0;
  if (visu_paths_setToolShade(paths, shade))
    {
      visu_gl_ext_paths_setDirty(extPaths);
      visu_gl_ext_paths_draw(extPaths);
      VISU_REDRAW_ADD;
    }
}
static void onToolShadePathSelected(VisuUiShadeCombobox *combo _U_, ToolShade *shade,
				gpointer data _U_)
{
  if (visu_paths_setToolShade(paths, shade))
    {
      visu_gl_ext_paths_setDirty(extPaths);
      visu_gl_ext_paths_draw(extPaths);
      VISU_REDRAW_ADD;
    }
}
static void onSavePathClicked(GtkButton *bt _U_, gpointer data _U_)
{
  GtkWidget *wd;
  gint response;
  GError *error;
  gchar *base;

  if (visu_paths_getLength(paths) < 1)
    return;

  wd = gtk_file_chooser_dialog_new(_("Export current set of paths."), (GtkWindow*)0,
				   GTK_FILE_CHOOSER_ACTION_SAVE,
				   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				   GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
				   NULL);
  if (!exportPathFile)
    exportPathFile = g_build_filename(g_get_current_dir(), _("paths.xml"), NULL);
  base = g_path_get_basename(exportPathFile);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(wd), base);
  g_free(base);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 7
  gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(wd), TRUE);
#endif
  do
    {
      response = gtk_dialog_run(GTK_DIALOG(wd));
      if (exportPathFile)
	g_free(exportPathFile);
      exportPathFile = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(wd));
      switch (response)
	{
	case GTK_RESPONSE_ACCEPT:
	  error = (GError*)0;
	  if (!visu_paths_exportXMLFile(paths, exportPathFile, &error))
	    {
	      visu_ui_raiseWarning(_("Export current set of paths."),
				   error->message, GTK_WINDOW(wd));
	      g_error_free(error);
	      response = GTK_RESPONSE_NONE;
	    }
	  break;
	default:
	  response = GTK_RESPONSE_ACCEPT;
	  break;
	}
    }
  while (response != GTK_RESPONSE_ACCEPT);
  gtk_widget_destroy(wd);
}
static void onLoadPathClicked(GtkButton *bt _U_, gpointer data _U_)
{
  GtkWidget *wd;
  gint response;
  GError *error;
  gchar *text, *directory;

  wd = gtk_file_chooser_dialog_new(_("Load a set of paths."), (GtkWindow*)0,
				   GTK_FILE_CHOOSER_ACTION_OPEN,
				   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				   NULL);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(wd), directory);
  do
    {
      response = gtk_dialog_run(GTK_DIALOG(wd));
      if (exportPathFile)
	g_free(exportPathFile);
      exportPathFile = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(wd));
      switch (response)
	{
	case GTK_RESPONSE_ACCEPT:
	  error = (GError*)0;
	  if (!visu_paths_parseFromXML(exportPathFile, paths, &error))
	    {
	      visu_ui_raiseWarning(_("Load a set of paths."),
				   error->message, GTK_WINDOW(wd));
	      g_error_free(error);
	      response = GTK_RESPONSE_NONE;
	    }
	  break;
	default:
	  response = GTK_RESPONSE_ACCEPT;
	  break;
	}
    }
  while (response != GTK_RESPONSE_ACCEPT);
  gtk_widget_destroy(wd);

  visu_gl_ext_paths_setDirty(extPaths);
  visu_gl_ext_paths_draw(extPaths);
  
  text = g_strdup_printf(PATH, visu_paths_getLength(paths));
  gtk_label_set_markup(GTK_LABEL(labelVisuPaths), text);
  g_free(text);

  VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_geometry_getPaths:
 *
 * This panel can store a #VisuPaths object.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): the stored #VisuPaths object.
 **/
VisuPaths* visu_ui_panel_geometry_getPaths()
{
  return paths;
}


/*******************************/
/* Test script for this panel. */
/*******************************/
static guint n_issues;
#define TEST_COND(C) {if (!(C)) {n_issues += 1; g_message("%s failed", #C);}}
static void _resetTranslation()
{
  guint i;

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations), FALSE);
  for (i = 0; i < 3; i++)
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[i]), 0.);
}
static void _testTranslation(VisuData *data, gboolean status)
{
  float *trans, cartCoord[3];

  TEST_COND(visu_data_getTranslationStatus(data) == status);
  trans = visu_data_getXYZtranslation(data);
  if (status)
    getCartesianTranslation(cartCoord);
  else
    {
      cartCoord[0] = 0.f;
      cartCoord[1] = 0.f;
      cartCoord[2] = 0.f;
    }
  TEST_COND(trans[0] == cartCoord[0]);
  TEST_COND(trans[1] == cartCoord[1]);
  TEST_COND(trans[2] == cartCoord[2]);
  g_free(trans);
}
/**
 * visu_ui_panel_geometry_runTests:
 * @error: the location of a nullified #GError pointer.
 *
 * Internal test routine, used by the test suite.
 *
 * Since: 3.7
 **/
void visu_ui_panel_geometry_runTests(GError **error)
{
  VisuData *data;
  GQuark TEST_ERROR = g_quark_from_static_string("Testing suite error");

  data = visu_ui_panel_getData(VISU_UI_PANEL(panelGeometry));
  n_issues = 0;

  if (widgetsNotBuilt)
    gtk_container_add(GTK_CONTAINER(panelGeometry), createInteriorBox());

  /* Testing translations */
  /* -------------------- */
  _resetTranslation();
  
  /* Check no change. */
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[0]), 0.5);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations), FALSE);
  _testTranslation(data, FALSE);
  _resetTranslation();

  /* Check change before applying. */
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[0]), 0.5);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations), TRUE);
  _testTranslation(data, TRUE);
  _resetTranslation();

  /* Check change after applying. */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkAllowTranslations), TRUE);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinTransXYZ[0]), 0.25);
  _testTranslation(data, TRUE);
  _resetTranslation();

  if (n_issues > 0)
    *error = g_error_new(TEST_ERROR, 0, "There are %d failed tests.", n_issues);
}
