/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef PANELMETHOD_H
#define PANELMETHOD_H

#include <visu_rendering.h>
#include <gtk_main.h>
#include <visu_gtk.h>

/**
 * VisuUiPanelDestroyFunc:
 *
 * Prototype of method called when the rendering method widget is destroyed.
 */
typedef void (*VisuUiPanelDestroyFunc)(void);

/**
 * visu_ui_panel_method_init:
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the
 * stuff about a rendering method can be tuned, such as choosing the
 * current rendering method, its parameters...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_method_init();

/**
 * visu_ui_panel_method_set:
 * @method: a #RenderingMethod ;
 * @create: a method returning a widget ;
 * @destroy: a method to detached, signals, free everything from the
 *           created widget.
 *
 * This method allows to initiate the method to deal with the interface.
 */
void visu_ui_panel_method_set(VisuRendering *method,
				       VisuUiNewWidgetFunc create,
				       VisuUiPanelDestroyFunc destroy);

#endif
