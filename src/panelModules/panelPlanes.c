/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelPlanes.h"

#include <math.h>

#include <visu_gtk.h>
#include <visu_object.h>
#include <support.h>
#include <gtk_main.h>
#include <openGLFunctions/objectList.h>
#include <coreTools/toolMatrix.h>
#include <extensions/planes.h>

#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_colorComboBoxWidget.h>
#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include <extraGtkFunctions/gtk_orientationChooser.h>
#include <extraGtkFunctions/gtk_valueIOWidget.h>

/**
 * SECTION: panelPlanes
 * @short_description: The tab where planes are defined.
 *
 * <para>It is possible to get the list of planes using
 * visu_ui_panel_planes_getAll(). One can also access to the list
 * store hosting the planes by calling visu_ui_panel_planes_getList().</para>
 */

#define PANEL_PLANES_NO_VISU_PLANE_LIST _("<span font_desc=\"smaller\"><i>none</i></span>")

GtkWidget *panelPlanes;
GtkWidget *vBoxVisuPlanes;
GtkWidget *checkUseVisuPlanes;
/* The spin buttons use integer values in the box coordinates. */
GtkWidget *entryNVect[3];
gulong signalsOrientation[3];
/* The true values in the cartesian coordinates are stored there. */
/* float nVect[3]; */
GtkWidget *spinbuttonDistance;
GtkWidget *widgetColorVisuPlane;
GtkWidget *treeviewVisuPlanes;
GtkListStore *listStoredVisuPlanes;
GtkWidget *labelDistance;
GtkWidget *buttonRotate;
GtkWidget *hboxHidingMode;
GtkWidget *vboxDistChange;
GtkWidget *entryDistFrom, *entryDistTo, *entryDistStep, *spinDistDelay;
static GtkWidget *imageDistPlay, *imageDistStop;
static GtkWidget *orientationChooser;


/* Variables for the IO tab. */
GtkWidget *hboxSaveList;
gchar *currentSaveListFile;
GtkWidget *buttonSaveList;
GtkWidget *labelVisuPlaneList;

/* Callbacks for the IO tab. */
static void onVisuPlaneListOpen(GtkButton *button, gpointer data);
static void onVisuPlaneListSave(GtkButton *button, gpointer data);
static void onVisuPlaneListSaveAs(GtkButton *button, gpointer data);

static gulong hide_signal, position_signal, popInc_signal, popDec_signal;
static guint isPlayingDistanceId;
static gdouble directionDist;
static gboolean disableCallbacks;

/* Local callbacks. */
static void onVisuPlaneUsed(GtkToggleButton *button, gpointer data);
void onVisuPlaneAdd(GtkButton *button, gpointer data);
void onVisuPlaneRemove(GtkButton *button, gpointer data);
static void onEntryNVectChange(VisuUiNumericalEntry *entry, double oldValue, gpointer data);
static void onSpinDistanceChange(GtkSpinButton *spin, gpointer data);
static void onVisuPlaneColorChange(VisuUiColorCombobox *combo, ToolColor *selectedColor, gpointer data);
static void onDataFocused(GObject *obj, VisuData *dataObj, gpointer data);
static void onDataReady(VisuObject *obj, VisuData *dataObj,
                        VisuGlView *view, gpointer data);
static void onDataNotReady(VisuObject *obj, VisuData *dataObj,
                           VisuGlView *view, gpointer data);
static void onTreeSelectionChanged(GtkTreeSelection *tree, gpointer data);
static void onPositionChanged(VisuData *dataObj, VisuElement *ele, gpointer data);
static void onPopulationIncreased(VisuData *dataObj,
				      int *newNodes, gpointer data);
void onSetCameraPosition(GtkButton *button, gpointer data);
static void onGtkVisuPlanesDrawnToggled(GtkCellRendererToggle *cell_renderer,
				    gchar *path, gpointer user_data);
static void onGtkVisuPlanesHideToggled(GtkCellRendererToggle *cell_renderer,
				   gchar *path, gpointer user_data);
void onGtkVisuPlanesHidingModeToggled(GtkToggleButton *toggle, gpointer data);
static void onAskForHideNodes(VisuData *visuData, gboolean *redraw, gpointer data);
static void onPlayStopDist(GtkButton *button, gpointer data);
static void onSpinDistDelayChange(GtkSpinButton *spin, gpointer data);
static void onVisuPlanesEnter(VisuUiPanel *planes, gpointer data);
static void onVisuUiOrientationChooser(GtkButton *button, gpointer data);
static void onSizeChanged(VisuBox *box, gfloat extens, gpointer user_data);

/* Local methods. */
static void _getBoxSpan(VisuBox *box, float span[2]);
static void stopPlayStop(gpointer data);
static gboolean playDistances(gpointer data);
/* Call createInteriorVisuPlanes() to create all the widgets,
   and if dataObj is not null, it set callbacks as if
   a new VisuData has been loaded. */
static void createAndInitVisuPlanePanel(VisuData *dataObj);
static void createInteriorVisuPlanes(VisuData *dataObj);

static gboolean isVisuPlanesInitialised;

/* String used to labelled planes, dist. means 'distance' and
   norm. means 'normal' (50 chars max). */
#define LABEL_PLANE _("<b>norm.</b>: (%3d;%3d;%3d)\n<b>distance</b>: %6.2f")

/**
 * visu_ui_panel_planes_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel where the plane
 * stuff can be done, such as creating a plane, masking nodes,
 * changing the orientation or the colour...
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_planes_init(VisuUiMain *ui)
{
  /* Long description */
  char *cl = _("Drawing planes");
  /* Short description */
  char *tl = _("Planes");

  panelPlanes = visu_ui_panel_newWithIconFromPath("Panel_planes", cl, tl,
                                                  "stock-planes_20.png");
  if (!panelPlanes)
    return (VisuUiPanel*)0;
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelPlanes), TRUE);

  vBoxVisuPlanes = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_sensitive(vBoxVisuPlanes, FALSE);

  isVisuPlanesInitialised = FALSE;
  checkUseVisuPlanes      = (GtkWidget*)0;
  spinbuttonDistance  = (GtkWidget*)0;
  orientationChooser  = (GtkWidget*)0;
  isPlayingDistanceId = 0;
  currentSaveListFile = (gchar*)0;
  disableCallbacks    = FALSE;
  /* Create the listModel */
  listStoredVisuPlanes = gtk_list_store_new (VISU_UI_PANEL_PLANES_N_COLUMNS,
					 G_TYPE_BOOLEAN,
					 G_TYPE_STRING,
					 G_TYPE_BOOLEAN,
					 G_TYPE_BOOLEAN,
					 GDK_TYPE_PIXBUF,
					 G_TYPE_OBJECT);

  /* Add the signal for the vBoxVisuPlanes. */
  g_signal_connect(G_OBJECT(ui), "DataFocused",
		   G_CALLBACK(onDataFocused), (gpointer)0);
  g_signal_connect(G_OBJECT(panelPlanes), "page-entered",
		   G_CALLBACK(onVisuPlanesEnter), (gpointer)0);

  return VISU_UI_PANEL(panelPlanes);
}

/**
 * visu_ui_panel_planes_getList:
 * 
 * This method gives read access to the #GtkListStore used to store
 * the planes.
 *
 * Returns: (transfer none): the #GtkListStore used by this panel to
 * store its planes. It should be considered read-only.
 */
GtkListStore* visu_ui_panel_planes_getList()
{
  return listStoredVisuPlanes;
}

static void createAndInitVisuPlanePanel(VisuData *dataObj)
{
  createInteriorVisuPlanes(dataObj);
  isVisuPlanesInitialised = TRUE;
  /* Force to connect signal if a file is already loaded. */
  if (dataObj)
    onDataReady(VISU_OBJECT_INSTANCE, dataObj, (VisuGlView*)0, (gpointer)0);
}

static void onVisuPlanesEnter(VisuUiPanel *planes, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel VisuPlanes: caught the 'page-entered' signal %d.\n",
	      isVisuPlanesInitialised);
  if (!isVisuPlanesInitialised)
    createAndInitVisuPlanePanel(visu_ui_panel_getData(planes));
}

static void createInteriorVisuPlanes(VisuData *dataObj)
{
  GtkWidget *wd;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *scrolledVisuPlanes;
  GtkWidget *hbox2;
  GtkWidget *alignment3;
  GtkWidget *vbox3, *vboxIO;
  GtkWidget *hbox4;
  GtkWidget *label2;
  GtkWidget *hbox5;
  GtkWidget *hbox3;
  GtkWidget *label4;
  GtkWidget *alignment2;
  GtkWidget *vbox2, *vbox;
  GtkWidget *buttonVisuPlaneAdd, *buttonDistPlayStop;
  GtkWidget *image2;
  GtkWidget *buttonVisuPlaneRemove, *button;
  GtkWidget *image4;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkWidget *image;
  GSList *radiobuttonHideUnion_group;
  GtkWidget *radiobuttonHideInter, *radiobuttonHideUnion;
  GtkWidget *notebook;
  int i;
  float span[2];
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
  GtkTooltips *tooltips;

  tooltips = gtk_tooltips_new ();
#endif

  gtk_widget_show(vBoxVisuPlanes);

  checkUseVisuPlanes = gtk_check_button_new_with_mnemonic(_("_Use planes"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkUseVisuPlanes),
			       visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())));
  gtk_widget_show (checkUseVisuPlanes);
  gtk_box_pack_start (GTK_BOX (vBoxVisuPlanes), checkUseVisuPlanes, FALSE, FALSE, 0);

  scrolledVisuPlanes = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (scrolledVisuPlanes);
  gtk_box_pack_start (GTK_BOX (vBoxVisuPlanes), scrolledVisuPlanes, TRUE, TRUE, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledVisuPlanes), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledVisuPlanes), GTK_SHADOW_IN);

  treeviewVisuPlanes = gtk_tree_view_new ();
  gtk_widget_show (treeviewVisuPlanes);
  gtk_container_add (GTK_CONTAINER (scrolledVisuPlanes), treeviewVisuPlanes);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (treeviewVisuPlanes), TRUE);

  notebook = gtk_notebook_new();
  gtk_widget_show(notebook);
  gtk_box_pack_start(GTK_BOX(vBoxVisuPlanes), notebook, FALSE, FALSE, 2);

  /* Page 1  : simple tools*/
  vbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vbox);
  label = gtk_label_new(_("Simple tools"));
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, label);

  /* Hiding Mode */
  hboxHidingMode = gtk_hbox_new (FALSE, 0);
  gtk_widget_show(hboxHidingMode);
  gtk_box_pack_start(GTK_BOX(vbox), hboxHidingMode, FALSE, FALSE, 2);

  label = gtk_label_new(_("Hiding mode: "));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hboxHidingMode), label, FALSE, FALSE, 0);

  radiobuttonHideUnion = gtk_radio_button_new(NULL);
  gtk_widget_show(radiobuttonHideUnion);
  gtk_box_pack_start(GTK_BOX(hboxHidingMode), radiobuttonHideUnion, FALSE, FALSE, 0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radiobuttonHideUnion), (GSList*)0);
  radiobuttonHideUnion_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radiobuttonHideUnion));
  gtk_widget_set_tooltip_text(radiobuttonHideUnion,
		       _("Hide all elements that are hidden by al least one plane."));
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(radiobuttonHideUnion), hbox);
  wd = create_pixmap((GtkWidget*)0, "stock-union.png");
  gtk_widget_show(wd);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  label = gtk_label_new(_("Union"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  radiobuttonHideInter = gtk_radio_button_new(NULL);
  gtk_widget_show(radiobuttonHideInter);
  gtk_box_pack_start(GTK_BOX(hboxHidingMode), radiobuttonHideInter, FALSE, FALSE, 0);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(radiobuttonHideInter), radiobuttonHideUnion_group);
  radiobuttonHideUnion_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radiobuttonHideInter));
  gtk_widget_set_tooltip_text(radiobuttonHideInter,
		       _("Hide elements only if they are hidden by all planes."));
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(radiobuttonHideInter), hbox);
  wd = create_pixmap((GtkWidget*)0, "stock-inter.png");
  gtk_widget_show(wd);
  gtk_box_pack_start(GTK_BOX(hbox), wd, FALSE, FALSE, 0);
  label = gtk_label_new(_("Intersection"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radiobuttonHideUnion), TRUE);
  gtk_widget_set_sensitive(hboxHidingMode, FALSE);

  /* VisuPlanes parameters */
  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, FALSE, 0);

  vbox3 = gtk_vbox_new (FALSE, 3);
  gtk_box_pack_start(GTK_BOX(hbox2), vbox3, FALSE, FALSE, 2);
  buttonRotate = gtk_button_new();
  gtk_widget_set_tooltip_text(buttonRotate, _("Set the camera to look in the direction"
						 " of the normal of the selected plane."));
  gtk_box_pack_start(GTK_BOX(vbox3), buttonRotate, TRUE, FALSE, 0);
  image = create_pixmap((GtkWidget*)0, "stock_rotate_20.png");
  gtk_container_add(GTK_CONTAINER(buttonRotate), image);
  gtk_widget_set_sensitive(buttonRotate, FALSE);
  

  alignment3 = gtk_alignment_new (0.5, 0.5, 0, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), alignment3, TRUE, TRUE, 0);

  vbox3 = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (alignment3), vbox3);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox4, TRUE, TRUE, 0);

  label2 = gtk_label_new (_("Normal: "));
  gtk_box_pack_start (GTK_BOX (hbox4), label2, TRUE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);

  for (i = 0; i < 3; i++)
    {
      entryNVect[i] = visu_ui_numerical_entry_new(1.);
      gtk_entry_set_width_chars(GTK_ENTRY(entryNVect[i]), 5);
      gtk_box_pack_start(GTK_BOX(hbox4), entryNVect[i], FALSE, FALSE, 0);
    }
  wd = gtk_button_new();
  gtk_box_pack_start(GTK_BOX(hbox4), wd, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(wd), "clicked",
		   G_CALLBACK(onVisuUiOrientationChooser), (gpointer)0);
  image = create_pixmap((GtkWidget*)0, "axes-button.png");
  gtk_container_add(GTK_CONTAINER(wd), image);

  hbox5 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox5, TRUE, TRUE, 0);

  labelDistance = gtk_label_new ("");
  gtk_label_set_text(GTK_LABEL(labelDistance),
		     _("Distance from origin: "));
  gtk_box_pack_start (GTK_BOX (hbox5), labelDistance, TRUE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (labelDistance), 1, 0.5);

  if (dataObj)
    _getBoxSpan(visu_boxed_getBox(VISU_BOXED(dataObj)), span);
  else
    {
      span[0] = -998.f;
      span[1] = 1000.f;
    }
  spinbuttonDistance = gtk_spin_button_new_with_range(-span[1], span[1], 0.25);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spinbuttonDistance), 2);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDistance), (span[0] + span[1]) / 2.f);
  gtk_box_pack_start (GTK_BOX (hbox5), spinbuttonDistance, FALSE, FALSE, 0);
  gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinbuttonDistance), TRUE);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox3, TRUE, TRUE, 0);

  label4 = gtk_label_new (_("Color: "));
  gtk_box_pack_start (GTK_BOX (hbox3), label4, TRUE, TRUE, 0);
  gtk_misc_set_alignment (GTK_MISC (label4), 1, 0.5);

  widgetColorVisuPlane = visu_ui_color_combobox_new(TRUE);
  gtk_box_pack_start(GTK_BOX(hbox3), widgetColorVisuPlane, FALSE, FALSE, 0);


  alignment2 = gtk_alignment_new (0.5, 0.5, 1, 0);
  gtk_box_pack_start (GTK_BOX (hbox2), alignment2, FALSE, FALSE, 0);

  vbox2 = gtk_vbox_new (FALSE, 2);
  gtk_container_add (GTK_CONTAINER (alignment2), vbox2);
/*   gtk_container_set_border_width (GTK_CONTAINER (vbox2), 3); */

  buttonVisuPlaneAdd = gtk_button_new ();
  gtk_box_pack_start (GTK_BOX (vbox2), buttonVisuPlaneAdd, FALSE, FALSE, 0);

  image2 = gtk_image_new_from_stock ("gtk-add", GTK_ICON_SIZE_BUTTON);
  gtk_container_add (GTK_CONTAINER (buttonVisuPlaneAdd), image2);

  buttonVisuPlaneRemove = gtk_button_new ();
  gtk_box_pack_start (GTK_BOX (vbox2), buttonVisuPlaneRemove, FALSE, FALSE, 0);

  image4 = gtk_image_new_from_stock ("gtk-remove", GTK_ICON_SIZE_BUTTON);
  gtk_container_add (GTK_CONTAINER (buttonVisuPlaneRemove), image4);

  gtk_widget_show_all(hbox2);

  /* Page   : advanced tools*/
  vboxDistChange = gtk_vbox_new(FALSE, 0);
  gtk_widget_set_sensitive(vboxDistChange, FALSE);
  gtk_widget_show(vboxDistChange);
  label = gtk_label_new(_("Advanced tools"));
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vboxDistChange, label);

  label = gtk_label_new(_("Change selected plane distance"));
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vboxDistChange), label, FALSE, FALSE, 3);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox);

  gtk_box_pack_start(GTK_BOX(vboxDistChange), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("From: "));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  entryDistFrom = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDistFrom), 7);
  gtk_widget_show(entryDistFrom);
  gtk_box_pack_start(GTK_BOX(hbox), entryDistFrom, FALSE, FALSE, 0);
  label = gtk_label_new(_("to: "));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  entryDistTo = visu_ui_numerical_entry_new(0.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDistTo), 7);
  gtk_widget_show(entryDistTo);
  gtk_box_pack_start(GTK_BOX(hbox), entryDistTo, FALSE, FALSE, 0);
  label = gtk_label_new(_("step: "));
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  entryDistStep = visu_ui_numerical_entry_new(1.);
  gtk_entry_set_width_chars(GTK_ENTRY(entryDistStep), 7);
  gtk_widget_show(entryDistStep);
  gtk_box_pack_start(GTK_BOX(hbox), entryDistStep, FALSE, FALSE, 0);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vboxDistChange), hbox, FALSE, FALSE, 0);
  label = gtk_label_new (_("Play at "));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 1., 0.5);
  spinDistDelay = gtk_spin_button_new_with_range(10, 10000, 25);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDistDelay), 500.);
  gtk_widget_show(spinDistDelay);
  gtk_box_pack_start(GTK_BOX(hbox), spinDistDelay, FALSE, TRUE, 0);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinDistDelay), TRUE);
  label = gtk_label_new(_(" ms"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  buttonDistPlayStop = gtk_button_new();
  gtk_widget_set_tooltip_text(buttonDistPlayStop,
		       _("Change the distance parameter of he selected file"
			 " at the given rate."));
  gtk_widget_show(buttonDistPlayStop);
  gtk_box_pack_start(GTK_BOX(hbox), buttonDistPlayStop, FALSE, FALSE, 15);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox);
  gtk_container_add(GTK_CONTAINER(buttonDistPlayStop), hbox);
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 5
  imageDistPlay = create_pixmap((GtkWidget*)0, "stock_media-play.png");
  imageDistStop = create_pixmap((GtkWidget*)0, "stock_media-stop.png");
#else
  imageDistPlay = gtk_image_new_from_stock(GTK_STOCK_MEDIA_PLAY, GTK_ICON_SIZE_BUTTON);
  imageDistStop = gtk_image_new_from_stock(GTK_STOCK_MEDIA_STOP, GTK_ICON_SIZE_BUTTON);
#endif
  gtk_widget_show(imageDistPlay);
  gtk_box_pack_start(GTK_BOX(hbox), imageDistPlay, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), imageDistStop, TRUE, TRUE, 0);

  /* The IO tab. */
  vboxIO = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vboxIO);
  label = gtk_label_new(_("File tools"));
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vboxIO, label);
  
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(vboxIO), hbox, FALSE, FALSE, 0);
  label = gtk_label_new(_("Load a plane list:"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);
  button = gtk_button_new_from_stock(GTK_STOCK_OPEN);
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(button), "clicked",
		   G_CALLBACK(onVisuPlaneListOpen), (gpointer)0);

  label = gtk_label_new(_("Current loaded list:"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vboxIO), label, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0., 0.5);

  labelVisuPlaneList = gtk_label_new("");
  gtk_label_set_use_markup(GTK_LABEL(labelVisuPlaneList), TRUE);
  gtk_label_set_markup(GTK_LABEL(labelVisuPlaneList), PANEL_PLANES_NO_VISU_PLANE_LIST);
  gtk_widget_show(labelVisuPlaneList);
  gtk_box_pack_start(GTK_BOX(vboxIO), labelVisuPlaneList, TRUE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(labelVisuPlaneList), 0., 0.5);
#if GTK_MAJOR_VERSION > 2 || GTK_MINOR_VERSION > 5
  gtk_label_set_ellipsize(GTK_LABEL(labelVisuPlaneList), PANGO_ELLIPSIZE_START);
#endif

  hboxSaveList = gtk_hbox_new(FALSE, 0);
  gtk_widget_set_sensitive(hboxSaveList, FALSE);
  gtk_widget_show(hboxSaveList);
  gtk_box_pack_start(GTK_BOX(vboxIO), hboxSaveList, FALSE, FALSE, 0);
  alignment2 = gtk_alignment_new(1., 0.5, 0, 0);
  gtk_widget_show(alignment2);
  gtk_box_pack_start(GTK_BOX(hboxSaveList), alignment2, TRUE, TRUE, 0);
  buttonSaveList = gtk_button_new_from_stock(GTK_STOCK_SAVE);
  gtk_widget_set_sensitive(buttonSaveList, FALSE);
  gtk_widget_show(buttonSaveList);
  gtk_container_add(GTK_CONTAINER(alignment2), buttonSaveList);
  g_signal_connect(G_OBJECT(buttonSaveList), "clicked",
		   G_CALLBACK(onVisuPlaneListSave), (gpointer)0);
  button = gtk_button_new_from_stock(GTK_STOCK_SAVE_AS);
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hboxSaveList), button, FALSE, FALSE, 0);
  g_signal_connect(G_OBJECT(button), "clicked",
		   G_CALLBACK(onVisuPlaneListSaveAs), (gpointer)0);

  /* Add the callback methods. */
  g_signal_connect(G_OBJECT(checkUseVisuPlanes), "toggled",
		   G_CALLBACK(onVisuPlaneUsed), (gpointer)0);

  g_signal_connect(G_OBJECT(buttonVisuPlaneAdd), "clicked",
		   G_CALLBACK(onVisuPlaneAdd), (gpointer)0);
  g_signal_connect(G_OBJECT(buttonVisuPlaneRemove), "clicked",
		   G_CALLBACK(onVisuPlaneRemove), (gpointer)0);

  for (i = 0; i < 3; i++)
    signalsOrientation[i] = 
      g_signal_connect(G_OBJECT(entryNVect[i]), "value-changed",
		       G_CALLBACK(onEntryNVectChange), GINT_TO_POINTER(i));

  g_signal_connect(G_OBJECT(spinbuttonDistance), "value-changed",
		   G_CALLBACK(onSpinDistanceChange), (gpointer)0);

  g_signal_connect(G_OBJECT(widgetColorVisuPlane), "color-selected",
		   G_CALLBACK(onVisuPlaneColorChange), (gpointer)0);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataRendered",
		   G_CALLBACK(onDataReady), (gpointer)0);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataUnRendered",
		   G_CALLBACK(onDataNotReady), (gpointer)0);

  g_signal_connect(gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)),
		   "changed", G_CALLBACK(onTreeSelectionChanged), (gpointer)0);

  g_signal_connect(G_OBJECT(buttonRotate), "clicked",
		   G_CALLBACK(onSetCameraPosition), (gpointer)0);

  g_signal_connect(G_OBJECT(radiobuttonHideUnion), "toggled",
		   G_CALLBACK(onGtkVisuPlanesHidingModeToggled), GINT_TO_POINTER(VISU_PLANE_HIDE_UNION));
  g_signal_connect(G_OBJECT(radiobuttonHideInter), "toggled",
		   G_CALLBACK(onGtkVisuPlanesHidingModeToggled), GINT_TO_POINTER(VISU_PLANE_HIDE_INTER));

  g_signal_connect(G_OBJECT(buttonDistPlayStop), "clicked",
		   G_CALLBACK(onPlayStopDist), (gpointer)0);
  g_signal_connect(G_OBJECT(spinDistDelay), "value-changed",
		   G_CALLBACK(onSpinDistDelayChange), (gpointer)0);

  /* Render the associated tree */
  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onGtkVisuPlanesDrawnToggled), (gpointer)0);
  column = gtk_tree_view_column_new_with_attributes (_("Drawn"),
						     renderer,
						     "active", VISU_UI_PANEL_PLANES_DRAWN,
						     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewVisuPlanes), column);
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Parameters"),
						     renderer,
						     "markup", VISU_UI_PANEL_PLANES_LABEL,
						     NULL);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_column_set_alignment(column, 0.5);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeviewVisuPlanes), column);
  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onGtkVisuPlanesHideToggled), GINT_TO_POINTER(0));
  column = gtk_tree_view_column_new_with_attributes (_("Mask"),
						     renderer,
						     "active", VISU_UI_PANEL_PLANES_HIDE_IS_ON,
						     NULL);
  image = create_pixmap((GtkWidget*)0, "stock-masking.png");
  gtk_widget_show(image);
  gtk_tree_view_column_set_widget(column, image);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewVisuPlanes), column);
  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect(G_OBJECT(renderer), "toggled",
		   G_CALLBACK(onGtkVisuPlanesHideToggled), GINT_TO_POINTER(1));
  column = gtk_tree_view_column_new_with_attributes (_("Invert"),
						     renderer,
						     "active", VISU_UI_PANEL_PLANES_HIDDEN_SIDE,
						     NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewVisuPlanes), column);
  renderer = gtk_cell_renderer_pixbuf_new ();
  column = gtk_tree_view_column_new_with_attributes (_("Color"),
						     renderer,
						     "pixbuf", VISU_UI_PANEL_PLANES_COLOR_PIXBUF,
						     NULL);
  image = gtk_image_new_from_stock(GTK_STOCK_SELECT_COLOR,
				   GTK_ICON_SIZE_SMALL_TOOLBAR);
  gtk_widget_show(image);
  gtk_tree_view_column_set_widget(column, image);
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeviewVisuPlanes), column);

  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)),
			      GTK_SELECTION_SINGLE);
  gtk_tree_view_set_model(GTK_TREE_VIEW(treeviewVisuPlanes), GTK_TREE_MODEL(listStoredVisuPlanes));

  gtk_container_add(GTK_CONTAINER(panelPlanes), vBoxVisuPlanes);
}

/**
 * visu_ui_panel_planes_getAll:
 * @maskingOnly: a boolean.
 *
 * Return a newly created list (to be freed with g_list_free()) of all planes
 * available in the subpanel. This list can be restricted to masking planes
 * only with the help of argument @maskingOnly.
 *
 * Returns: (array zero-terminated=1) (transfer container): a newly
 * created array NULL terminated that must be freed with g_free().
 */
VisuPlane** visu_ui_panel_planes_getAll(gboolean maskingOnly)
{
  VisuPlane **planeList;
  GtkTreeIter iter;
  VisuPlane *plane;
  int i;
  gboolean skip;
  
  DBG_fprintf(stderr, "Panel VisuPlanes: building plane list...\n");
  /* Build the list of all planes. */
  planeList = g_malloc(sizeof(VisuPlane*) *
		       (gtk_tree_model_iter_n_children
			(GTK_TREE_MODEL(listStoredVisuPlanes), NULL) + 1));
  skip = (maskingOnly &&
	  (!checkUseVisuPlanes ||
	   !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkUseVisuPlanes))));
  i = 0;
  if (!skip && gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter))
    do
      {
	gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes),
			   &iter, VISU_UI_PANEL_PLANES_POINTER, &plane, -1);
	/* Because the get action increase the counter... */
	g_object_unref(G_OBJECT(plane));
	if (!maskingOnly || (visu_plane_getHiddenState(plane) != VISU_PLANE_SIDE_NONE))
	  {
	    planeList[i] = plane;
	    i += 1;
	    DBG_fprintf(stderr, " | found one plane %p.\n", (gpointer)plane);
	  }
      }
    while (gtk_tree_model_iter_next(GTK_TREE_MODEL(listStoredVisuPlanes), &iter));
  planeList[i] = (VisuPlane*)0;

  return planeList;
}

static void _getBoxSpan(VisuBox *box, float span[2])
{
  float diag[3], ext[3], red[3] = {1.f, 1.f, 1.f};

  visu_box_convertBoxCoordinatestoXYZ(box, diag, red);
  visu_box_getExtension(box, ext);
  diag[0] *= diag[0];
  diag[1] *= diag[1];
  diag[2] *= diag[2];
  span[0] = -sqrt(ext[0] * ext[0] * diag[0] +
                  ext[1] * ext[1] * diag[1] +
                  ext[2] * ext[2] * diag[2]);
  span[1] = sqrt((ext[0] + 1.f) *
		 (ext[0] + 1.f) * diag[0] +
		 (ext[1] + 1.f) *
		 (ext[1] + 1.f) * diag[1] +
		 (ext[2] + 1.f) *
		 (ext[2] + 1.f) * diag[2]);
}

static void onAskForHideNodes(VisuData *visuData, gboolean *redraw,
			      gpointer data _U_)
{
  VisuPlane **listOfVisuPlanes;

  DBG_fprintf(stderr, "Panel VisuPlanes: caught the 'AskForShowHide' signal for"
	      " VisuData %p.\n", (gpointer)visuData);

  DBG_fprintf(stderr, "Visu UI Planes: checkbox is %p.\n", (gpointer)checkUseVisuPlanes);
  if (!checkUseVisuPlanes || !gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkUseVisuPlanes)))
    return;

  listOfVisuPlanes = visu_ui_panel_planes_getAll(FALSE);
  if (listOfVisuPlanes)
    {
      *redraw = visu_plane_class_showHideAll(listOfVisuPlanes, visuData) || *redraw;
      g_free(listOfVisuPlanes);
    }
}
/**
 * visu_ui_panel_planes_setUsed:
 * @value: a boolean.
 *
 * When @value is TRUE, the planes are used and drawn. If the panel
 * has not been created yet, a call to this function will do it.
 *
 * Returns: TRUE if the hiding scheme has been applied.
 */
gboolean visu_ui_panel_planes_setUsed(gboolean value)
{
  int val;
  GtkTreeIter iter;
  VisuData *dataObj;

  DBG_fprintf(stderr, "Visu UI Planes: set used (%d).\n", value);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkUseVisuPlanes), value);

  if (!visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault()), value))
    return FALSE;
  visu_gl_ext_planes_draw(visu_gl_ext_planes_getDefault());
  
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  if (!isVisuPlanesInitialised)
    createAndInitVisuPlanePanel(dataObj);

  /* Test if any planes are stored. */
  if (!gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter))
    return FALSE;

  DBG_fprintf(stderr, " | apply hiding scheme.\n");
  val = visu_ui_panel_planes_applyHidingScheme(dataObj);

  return (gboolean)val;
}
static void onVisuPlaneUsed(GtkToggleButton *button, gpointer data _U_)
{
  visu_ui_panel_planes_setUsed(gtk_toggle_button_get_active(button));
  VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_planes_add:
 * @plane: (transfer full): a #VisuPlane object.
 * @hidingStatus: if plane is used for hiding.
 * @hidingSide: the side hiding is done on.
 *
 * Add a plane to the list of planes.
 *
 * Since: 3.7
 *
 * Returns: TRUE if redraw is needed.
 **/
gboolean visu_ui_panel_planes_add(VisuPlane *plane,
                                  gboolean hidingStatus, gboolean hidingSide)
{
  gchar str[256];
  float vect[3];
  GtkTreeIter iter;
  GdkPixbuf *pix;

  /* String used to labelled planes, dist. means 'distance' and
     norm. means 'normal' (50 chars max). */
  visu_plane_getNVectUser(plane, vect);
  sprintf(str, LABEL_PLANE, (int)vect[0], (int)vect[1], (int)vect[2],
          visu_plane_getDistanceFromOrigin(plane));
  pix = tool_color_get_stamp(visu_plane_getColor(plane), TRUE);
  gtk_list_store_append(listStoredVisuPlanes, &iter);
  gtk_list_store_set(listStoredVisuPlanes, &iter,
		     VISU_UI_PANEL_PLANES_DRAWN, visu_plane_getRendered(plane),
		     VISU_UI_PANEL_PLANES_LABEL, str,
		     VISU_UI_PANEL_PLANES_HIDE_IS_ON, hidingStatus,
		     VISU_UI_PANEL_PLANES_HIDDEN_SIDE, hidingSide,
		     VISU_UI_PANEL_PLANES_COLOR_PIXBUF, (gpointer)pix,
		     VISU_UI_PANEL_PLANES_POINTER, (gpointer)plane,
		     -1);
  g_object_unref(pix);

  if (visu_gl_ext_planes_add(visu_gl_ext_planes_getDefault(), plane))
    visu_gl_ext_planes_draw(visu_gl_ext_planes_getDefault());
  gtk_tree_selection_select_iter
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &iter);
  gtk_widget_set_sensitive(hboxHidingMode,
                           (gtk_tree_model_iter_n_children
                            (GTK_TREE_MODEL(listStoredVisuPlanes),
                             (GtkTreeIter*)0) > 1));

  return visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault()));
}
void onVisuPlaneAdd(GtkButton *button _U_, gpointer userData _U_)
{
  VisuPlane* plane;
  float vect[3];
  int i;
  ToolColor *color;
  float dist;
  gboolean redraw;

  /* We create a new plane. */
  color = visu_ui_color_combobox_getSelection
    (VISU_UI_COLOR_COMBOBOX(widgetColorVisuPlane));
  for (i = 0; i < 3; i++)
    vect[i] = (float)visu_ui_numerical_entry_getValue
      (VISU_UI_NUMERICAL_ENTRY(entryNVect[i]));
  dist = (float)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinbuttonDistance));
  plane = visu_plane_new(visu_boxed_getBox(VISU_BOXED(visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes)))), vect, dist, color);
  
  /* We store it. */
  redraw = visu_ui_panel_planes_add(plane, FALSE, FALSE);
  
  /* We count down the counter on plane since it has been added to the list. */
  g_object_unref(G_OBJECT(plane));
  
  if (redraw)
    VISU_REDRAW_ADD;
}
void onVisuPlaneRemove(GtkButton *button _U_, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  VisuData *dataObj;
  VisuPlane *plane;

  DBG_fprintf(stderr, "Panel VisuPlanes: removing plane...\n");
  res = gtk_tree_selection_get_selected
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &tmpList, &iter);
  if (!res)
    return;
  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
                     VISU_UI_PANEL_PLANES_POINTER, &plane);

  /* Remove plane from VisuGlExtPlanes list. */
  if (visu_gl_ext_planes_remove(visu_gl_ext_planes_getDefault(), plane))
    visu_gl_ext_planes_draw(visu_gl_ext_planes_getDefault());
  g_object_unref(plane);

  /* Remove it from interface and update selection. */
  res = TRUE;
  if (!gtk_list_store_remove(GTK_LIST_STORE(listStoredVisuPlanes), &iter))
    res = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter);
  if (res)
    gtk_tree_selection_select_iter
      (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &iter);

  DBG_fprintf(stderr, "Panel VisuPlanes: OK plane found and removed.\n");
  if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listStoredVisuPlanes),
                                     (GtkTreeIter*)0) < 2)
    gtk_widget_set_sensitive(hboxHidingMode, FALSE);

  /* If extension is not used, we leave here. */
  if (!visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())))
    return;

  /* Modify nodes visibility accordingly. */
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  if (visu_ui_panel_planes_applyHidingScheme(dataObj))
    VISU_REDRAW_ADD;
}
static void onEntryNVectChange(VisuUiNumericalEntry *entry, double oldValue _U_, gpointer data)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  VisuPlane *plane;
  char str[256];
  float dist;
  float vect[3];
  gboolean hide, side;
  int i;
  VisuData *dataObj;

  i = GPOINTER_TO_INT(data);
  g_return_if_fail(i >= 0 && i < 3);

  if (disableCallbacks)
    return;

  res = gtk_tree_selection_get_selected
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &tmpList, &iter);
  if (!res)
    return;

  plane = (VisuPlane*)0;
  hide = FALSE;
  side = FALSE;
  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
		     VISU_UI_PANEL_PLANES_POINTER, &plane,
		     VISU_UI_PANEL_PLANES_HIDE_IS_ON, &hide,
		     VISU_UI_PANEL_PLANES_HIDDEN_SIDE, &side,
		     -1);
  g_return_if_fail(plane);
  /* Because the get action increase the counter... */
  g_object_unref(G_OBJECT(plane));

  visu_plane_getNVectUser(plane, vect);
  vect[i] = (float)visu_ui_numerical_entry_getValue(entry);
  if (vect[0] == 0. && vect[1] == 0. && vect[2] == 0.)
    {
      g_warning("Can't assign a null vector.\n");
      visu_plane_getNVectUser(plane, vect);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[0]), (double)vect[0]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[1]), (double)vect[1]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[2]), (double)vect[2]);
      return;
    }
  res = visu_plane_setNormalVector(plane, vect);
  if (!res)
    return;

  dist = visu_plane_getDistanceFromOrigin(plane);
  sprintf(str, LABEL_PLANE,
	  (int)vect[0], (int)vect[1], (int)vect[2], dist);
  gtk_list_store_set(listStoredVisuPlanes, &iter, VISU_UI_PANEL_PLANES_LABEL, str, -1);

  /* Get the VisuData to deal with. */
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  /* Need to modify the nodes visibility only if the current
     plane use the masking function (hiddenState != VISU_PLANE_SIDE_NONE). */
  if (visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())) &&
      visu_plane_getHiddenState(plane) != VISU_PLANE_SIDE_NONE)
    /* Modify nodes visibility accordingly. */
    visu_ui_panel_planes_applyHidingScheme(dataObj);
  VISU_REDRAW_ADD;
}
static void onSpinDistanceChange(GtkSpinButton *spin, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  VisuPlane *plane;
  char str[256];
  float dist;
  float vect[3];
  gboolean hide, side;
  int val;
  VisuData *dataObj;

  if (disableCallbacks)
    return;

  res = gtk_tree_selection_get_selected
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &tmpList, &iter);
  if (!res)
    return;

  plane = (VisuPlane*)0;
  hide = FALSE;
  side = FALSE;
  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
		     VISU_UI_PANEL_PLANES_POINTER, &plane,
		     VISU_UI_PANEL_PLANES_HIDE_IS_ON, &hide,
		     VISU_UI_PANEL_PLANES_HIDDEN_SIDE, &side,
		     -1);
  g_return_if_fail(plane);
  /* Because the get action increase the counter... */
  g_object_unref(G_OBJECT(plane));

  dist = (float)gtk_spin_button_get_value(spin);
  val = visu_plane_setDistanceFromOrigin(plane, dist);
  if (val < 0)
    {
      g_warning("Can't assign the new distance from origin: %f.\n",
		dist);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDistance), 0.);
      return;
    }
  else if (val == 0)
    return;
  visu_plane_getNVectUser(plane, vect);
  sprintf(str, LABEL_PLANE,
	  (int)vect[0], (int)vect[1], (int)vect[2], dist);
  gtk_list_store_set(listStoredVisuPlanes, &iter, VISU_UI_PANEL_PLANES_LABEL, str, -1);

  /* Get the VisuData to deal with. */
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  /* Need to modify the nodes visibility only if the current
     plane use the masking function (hiddenState != VISU_PLANE_SIDE_NONE). */
  if (visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())) &&
      visu_plane_getHiddenState(plane) != VISU_PLANE_SIDE_NONE)
    /* Modify nodes visibility accordingly. */
    visu_ui_panel_planes_applyHidingScheme(dataObj);
  VISU_REDRAW_ADD;
}
static void onVisuPlaneColorChange(VisuUiColorCombobox *combo, ToolColor *color, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  VisuPlane *plane;
  int val;
  GdkPixbuf *pixbufColorAlphaBox;

  if (disableCallbacks)
    return;

  DBG_fprintf(stderr, "Panel VisuPlanes: Catch the 'color-selected' signal.\n");

  pixbufColorAlphaBox = visu_ui_color_combobox_getPixbufFromColor(combo, color);

  /* Get the plane */
  res = gtk_tree_selection_get_selected
    (gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)), &tmpList, &iter);
  if (!res)
    return;
  plane = (VisuPlane*)0;
  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter, VISU_UI_PANEL_PLANES_POINTER, &plane, -1);
  if (!plane)
    return;

  /* Because the get action increase the counter... */
  g_object_unref(G_OBJECT(plane));

  /* Change the color value. */
  val = visu_plane_setColor(plane, color);
  if (val < 0)
    {
      g_warning("Can't assign the new color: %p.\n", (gpointer)color);
      return;
    }
  else if (val == 0)
    return;

  DBG_fprintf(stderr, "Panel VisuPlanes: color combobox changed to %p (%f,%f,%f,%f).\n",
	      (gpointer)color, color->rgba[0], color->rgba[1], color->rgba[2], color->rgba[3]);
  DBG_fprintf(stderr, "Panel VisuPlanes: Changed color (%p) for plane : %p.\n",
	      (gpointer)color, (gpointer)plane);

  gtk_list_store_set(listStoredVisuPlanes, &iter,
                     VISU_UI_PANEL_PLANES_COLOR_PIXBUF, pixbufColorAlphaBox, -1);

  if (visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())))
    VISU_REDRAW_ADD;
}

static void onDataFocused(GObject *obj _U_, VisuData *dataObj, gpointer data _U_)
{
  VisuPlane **list;
  int i;
  gboolean needShowHide;
  float span[2];
  VisuBox *box;

  DBG_fprintf(stderr, "Panel VisuPlanes: caught 'DataFocused' signal\n"
	      " | setting sensitivity.\n");
  if (dataObj)
    gtk_widget_set_sensitive(vBoxVisuPlanes, TRUE);
  else
    {
      gtk_widget_set_sensitive(vBoxVisuPlanes, FALSE);
      return;
    }

  if (spinbuttonDistance)
    {
      DBG_fprintf(stderr, " | set the maximum of the distance spin.\n");
      _getBoxSpan(visu_boxed_getBox(VISU_BOXED(dataObj)), span);
      gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinbuttonDistance),
				-span[1], span[1]);
    }

  DBG_fprintf(stderr, " | recalculating intersections and masking set.\n");
  box = visu_boxed_getBox(VISU_BOXED(dataObj));
  list = visu_ui_panel_planes_getAll(FALSE);
  for (i = 0; list[i]; i++)
    visu_boxed_setBox(VISU_BOXED(list[i]), VISU_BOXED(box), TRUE);

  if (!visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())))
    {
      g_free(list);
      return;
    }

  needShowHide = FALSE;
  for (i = 0; list[i]; i++)
    /* Test if one of the plane at least need to mask some nodes. */
    needShowHide = needShowHide ||
      (visu_plane_getHiddenState(list[i]) != VISU_PLANE_SIDE_NONE);
  g_free(list);
  /* Compute the masking state */
  if (needShowHide)
    visu_ui_panel_planes_applyHidingScheme(dataObj);
}

static void onDataReady(VisuObject *obj _U_, VisuData *dataObj,
                        VisuGlView *view _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel VisuPlanes: caught 'dataRendered' signal,"
	      " connecting local signals.\n");
  
  if (dataObj)
    {
      hide_signal =
        g_signal_connect(G_OBJECT(dataObj), "AskForShowHide",
                         G_CALLBACK(onAskForHideNodes), (gpointer)0);
      position_signal =
        g_signal_connect(G_OBJECT(dataObj), "PositionChanged",
                         G_CALLBACK(onPositionChanged), (gpointer)0);
      popInc_signal =
        g_signal_connect(G_OBJECT(dataObj), "PopulationIncrease",
                         G_CALLBACK(onPopulationIncreased), (gpointer)0);
      popDec_signal =
        g_signal_connect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), "SizeChanged",
                         G_CALLBACK(onSizeChanged), (gpointer)0);
    }
}
static void onDataNotReady(VisuObject *obj _U_, VisuData *dataObj,
                           VisuGlView *view _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel VisuPlanes: caught 'dataUnRendered' signal,"
	      " disconnecting local signals.\n");
  
  g_signal_handler_disconnect(G_OBJECT(dataObj), hide_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), position_signal);
  g_signal_handler_disconnect(G_OBJECT(dataObj), popInc_signal);
  g_signal_handler_disconnect(G_OBJECT(visu_boxed_getBox(VISU_BOXED(dataObj))), popDec_signal);
}

static void onPositionChanged(VisuData *dataObj, VisuElement *ele _U_, gpointer data _U_)
{
  GtkTreeIter iter;

  DBG_fprintf(stderr, "Panel VisuPlanes: catch 'PositionChanged' signal,"
	      " recalculating masking properties.\n");

  if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter))
    visu_ui_panel_planes_applyHidingScheme(dataObj);
}
static void onPopulationIncreased(VisuData *dataObj,
				      int *newNodes _U_, gpointer data _U_)
{
  GtkTreeIter iter;

  DBG_fprintf(stderr, "Panel VisuPlanes: catch 'PopulationIncrease' signal,"
	      " recalculating masking properties for all nodes.\n");

  if (gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter))
    visu_ui_panel_planes_applyHidingScheme(dataObj);
}
static void onSizeChanged(VisuBox *box, gfloat extens _U_, gpointer user_data _U_)
{
  float span[2];

  DBG_fprintf(stderr, "Panel VisuPlanes: catch 'SizeChanged' signal,"
	      " updating interface.\n");
  if (spinbuttonDistance)
    {
      DBG_fprintf(stderr, " | set the maximum of the distance spin.\n");
      _getBoxSpan(box, span);
      gtk_spin_button_set_range(GTK_SPIN_BUTTON(spinbuttonDistance),
				-span[1], span[1]);
    }
}

static void onTreeSelectionChanged(GtkTreeSelection *tree, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  VisuPlane *plane;
  float nVisuPlane[3], dist;
  ToolColor *colorVisuPlane;

  DBG_fprintf(stderr, "Panel VisuPlanes: catch 'changed' signal from "
	      " the gtkTreeSelection.\n");

  res = gtk_tree_selection_get_selected(tree, &tmpList, &iter);
  if (res)
    {
      plane = (VisuPlane*)0;
      gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
			 VISU_UI_PANEL_PLANES_POINTER, &plane,
			 -1);
      /* Because the get action increase the counter... */
      g_object_unref(G_OBJECT(plane));

      DBG_fprintf(stderr, "Panel VisuPlanes: get the values for plane %p.\n",
		  (gpointer)plane);
      visu_plane_getNVectUser(plane, nVisuPlane);
      dist = visu_plane_getDistanceFromOrigin(plane);
      colorVisuPlane = visu_plane_getColor(plane);

      DBG_fprintf(stderr, "Panel VisuPlanes: update the widgets.\n");
      disableCallbacks = TRUE;
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[0]), (double)nVisuPlane[0]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[1]), (double)nVisuPlane[1]);
      visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[2]), (double)nVisuPlane[2]);
      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDistance), dist);
      visu_ui_color_combobox_setSelection(VISU_UI_COLOR_COMBOBOX(widgetColorVisuPlane), colorVisuPlane);
      disableCallbacks = FALSE;
      DBG_fprintf(stderr, " | done.\n");

      gtk_widget_set_sensitive(buttonRotate, TRUE);
      gtk_widget_set_sensitive(vboxDistChange, TRUE);
      gtk_widget_set_sensitive(hboxSaveList, TRUE);
    }
  else
    {
      gtk_widget_set_sensitive(buttonRotate, FALSE);
      gtk_widget_set_sensitive(vboxDistChange, FALSE);
      gtk_widget_set_sensitive(hboxSaveList, FALSE);
    }
}

void onSetCameraPosition(GtkButton *button _U_, gpointer data _U_)
{
  gboolean res;
  GtkTreeModel *tmpList;
  GtkTreeIter iter;
  float spherical[3];
  VisuPlane *plane;
  float nVisuPlane[3];

  res = gtk_tree_selection_get_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(treeviewVisuPlanes)),
					&tmpList, &iter);
  if (res)
    {
      plane = (VisuPlane*)0;
      gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
			 VISU_UI_PANEL_PLANES_POINTER, &plane,
			 -1);
      /* Because the get action increase the counter... */
      g_object_unref(G_OBJECT(plane));

      DBG_fprintf(stderr, "Panel VisuPlanes: Set the camera position to be"
		  " normal to the selected plane (%p).\n", (gpointer)plane);
      visu_plane_getNVectUser(plane, nVisuPlane);
      tool_matrix_cartesianToSpherical(spherical, nVisuPlane);
      visu_gl_view_setThetaPhiOmega(visu_ui_panel_getView(VISU_UI_PANEL(panelPlanes)),
                                    spherical[1], spherical[2], 0.,
                                    VISU_GL_CAMERA_THETA | VISU_GL_CAMERA_PHI);
      VISU_REDRAW_ADD;
    }
}
static void panelSet_rendered(GtkTreeIter *iter, VisuPlane *plane, gboolean status)
{
  visu_plane_setRendered(plane, status);
  gtk_list_store_set(listStoredVisuPlanes, iter,
		     VISU_UI_PANEL_PLANES_DRAWN, status, -1);
  DBG_fprintf(stderr, "Panel planes: set state (%d) for plane %p.\n",
	      status, (gpointer)plane);
}
static void onGtkVisuPlanesDrawnToggled(GtkCellRendererToggle *cell_renderer,
				    gchar *path, gpointer user_data _U_)
{
  gboolean validIter;
  GtkTreeIter iter;
  VisuPlane *plane;

  validIter = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(listStoredVisuPlanes),
						  &iter, path);
  g_return_if_fail(validIter);
  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
		     VISU_UI_PANEL_PLANES_POINTER, &plane, -1);
  g_object_unref(G_OBJECT(plane));
  panelSet_rendered(&iter, plane,
		    !gtk_cell_renderer_toggle_get_active(cell_renderer));
  VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_planes_setRendered:
 * @plane: a #VisuPlane object ;
 * @status: a boolean.
 *
 * Change the visibility of the plane through the Gtk interface, then
 * the widgets are also update and the list of drawn planes is
 * rebuilt. To only directly change the visibility of one plane use
 * visu_plane_setRendered() instead.
 *
 * Returns: TRUE if the redraw sigbnal should be emitted.
 */
gboolean visu_ui_panel_planes_setRendered(VisuPlane *plane, gboolean status)
{
  gboolean valid;
  GtkTreeIter iter;
  VisuPlane *storedVisuPlane;

  storedVisuPlane = (VisuPlane*)0;
  valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(listStoredVisuPlanes), &iter);
  while (valid)
    {
      gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
			 VISU_UI_PANEL_PLANES_POINTER, &storedVisuPlane, -1);
      g_object_unref(G_OBJECT(storedVisuPlane));
      if (storedVisuPlane == plane)
	valid = FALSE;
      else
	valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(listStoredVisuPlanes), &iter);
    }
  g_return_val_if_fail(storedVisuPlane == plane, FALSE);

  panelSet_rendered(&iter, plane, status);
  return TRUE;
}
void onGtkVisuPlanesHideToggled(GtkCellRendererToggle *cell_renderer _U_,
			    gchar *path, gpointer user_data)
{
  gboolean validIter, checked, side;
  GtkTreeIter iter;
  int val;
  VisuPlane *plane;
  VisuData *dataObj;

  DBG_fprintf(stderr, "Panel VisuPlanes: toggle plane hide mode.\n");
  validIter = gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(listStoredVisuPlanes), &iter, path);
  g_return_if_fail(validIter);

  gtk_tree_model_get(GTK_TREE_MODEL(listStoredVisuPlanes), &iter,
		     VISU_UI_PANEL_PLANES_POINTER, &plane,
		     VISU_UI_PANEL_PLANES_HIDE_IS_ON, &checked,
		     VISU_UI_PANEL_PLANES_HIDDEN_SIDE, &side, -1);
  /* Because the get action increase the counter... */
  g_object_unref(G_OBJECT(plane));

  if (GPOINTER_TO_INT(user_data) == 0)
    {
      checked = !checked;
      gtk_list_store_set(listStoredVisuPlanes, &iter,
			 VISU_UI_PANEL_PLANES_HIDE_IS_ON, checked, -1);
    }
  else
    {
      side = !side;
      gtk_list_store_set(listStoredVisuPlanes, &iter,
			 VISU_UI_PANEL_PLANES_HIDDEN_SIDE, side, -1);
    }
  /* Set the hidden status */
  if (!checked)
    val = visu_plane_setHiddenState(plane, VISU_PLANE_SIDE_NONE);
  else
    {
      if (side)
	val = visu_plane_setHiddenState(plane, VISU_PLANE_SIDE_MINUS);
      else
	val = visu_plane_setHiddenState(plane, VISU_PLANE_SIDE_PLUS);
    }
  if (!visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())))
    return;
  if (val)
    {
      /* Get the VisuData to deal with. */
      dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
      /* Modify nodes visibility accordingly. */
      if (visu_ui_panel_planes_applyHidingScheme(dataObj))
        VISU_REDRAW_ADD;
    }
}
void onGtkVisuPlanesHidingModeToggled(GtkToggleButton *toggle, gpointer data)
{
  int res, i;
  VisuPlane **planeList;
  gboolean needShowHide;
  VisuData *dataObj;

  if (!gtk_toggle_button_get_active(toggle))
    return;

  DBG_fprintf(stderr, "Panel planes : set hiding mode to %d.\n", GPOINTER_TO_INT(data));

  res = visu_plane_class_setHiddingMode(GPOINTER_TO_INT(data));
  if (!res || !visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault())))
    return;
  /* Build the list of all planes. */
  planeList = visu_ui_panel_planes_getAll(FALSE);
  if (!planeList[0])
    {
      g_free(planeList);
      return;
    }
  /* Test if one of the plane has a mask attribute. */
  needShowHide = FALSE;
  for (i = 0; planeList[i]; i++)
    /* Test if one of the plane at least need to mask some nodes. */
    needShowHide = needShowHide ||
      (visu_plane_getHiddenState(planeList[i]) != VISU_PLANE_SIDE_NONE);
  res = 0;
  /* Get the VisuData to deal with. */
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  if (needShowHide)
    /* Modify nodes visibility accordingly. */
    res = visu_ui_panel_planes_applyHidingScheme(dataObj);
  g_free(planeList);
  if (res)
    VISU_REDRAW_ADD;
}
/**
 * visu_ui_panel_planes_applyHidingScheme:
 * @data: a #VisuData object to associate the planes to (required
 *        to compute the intersection with the bounding box) ;
 *
 * Use this method to hide nodes according to current list of planes
 * and hiding policy.
 *
 * Returns: TRUE if the redraw signal should be emitted.
 */
gboolean visu_ui_panel_planes_applyHidingScheme(VisuData *data)
{
  gboolean redraw;

  redraw = FALSE;
  g_signal_emit_by_name(G_OBJECT(data), "AskForShowHide", &redraw, NULL);
  if (redraw)
    g_signal_emit_by_name(G_OBJECT(data), "VisibilityChanged", NULL);

  return redraw;
}


static void onPlayStopDist(GtkButton *button _U_, gpointer data _U_)
{
  DBG_fprintf(stderr, "Panel VisuPlanes: push the play/stop button.\n");

  if (!isPlayingDistanceId)
    {
      /* Launch play */
      gtk_widget_hide(imageDistPlay);
      gtk_widget_show(imageDistStop);

      gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDistance),
				visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDistFrom)));
      directionDist = 1.;
      isPlayingDistanceId =
	g_timeout_add_full(G_PRIORITY_DEFAULT + 30,
			   (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDistDelay)),
			   playDistances, (gpointer)0,
			   stopPlayStop);
    }
  else
    {
      /* Stop play */
      g_source_remove(isPlayingDistanceId);
    }
}
static void stopPlayStop(gpointer data _U_)
{
  isPlayingDistanceId = 0;
  gtk_widget_hide(imageDistStop);
  gtk_widget_show(imageDistPlay);
}
static gboolean playDistances(gpointer data _U_)
{
  gdouble val, step;
  gboolean changed;
  
  if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(listStoredVisuPlanes), NULL) == 0)
    return FALSE;

  val = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinbuttonDistance));
  step = visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDistStep));
  changed = FALSE;
  if (directionDist > 0.)
    {
      if (val + step > visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDistTo)))
	directionDist = -1.;
      else
	changed = TRUE;
    }
  else
    changed = TRUE;
  if (directionDist < 0.)
    {
      if (val - step < visu_ui_numerical_entry_getValue(VISU_UI_NUMERICAL_ENTRY(entryDistFrom)))
	directionDist = +1.;
      else
	changed = TRUE;
    }
  /* If the direction has been changed twice in a row,
     then the step is too wide for the range, we stop. */
  if (!changed)
    return FALSE;

  DBG_fprintf(stderr, "Panel VisuPlanes: set new distance to %g.\n", val + directionDist * step);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinbuttonDistance),
			    val + directionDist * step);
  return TRUE;
}
static void onSpinDistDelayChange(GtkSpinButton *spin _U_, gpointer data _U_)
{
  if (isPlayingDistanceId)
    {
      /* Stop play. */
      g_source_remove(isPlayingDistanceId);
      /* Launch play. */
      gtk_widget_hide(imageDistPlay);
      gtk_widget_show(imageDistStop);
      isPlayingDistanceId =
	g_timeout_add_full(G_PRIORITY_DEFAULT + 30,
			   (gint)gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinDistDelay)),
			   playDistances, (gpointer)0,
			   stopPlayStop);
    }
}

/* IO tab. */
static void setCurrentSaveFile()
{
  gchar *filenameUTF8, *markup;

  if (!currentSaveListFile)
    return;

  /* Print the filename to screen. */
  filenameUTF8 = g_filename_to_utf8(currentSaveListFile, -1, NULL, NULL, NULL);
  g_return_if_fail(filenameUTF8);

  markup = g_markup_printf_escaped(_("<span style=\"italic\" size=\"smaller\">%s</span>"),
				   filenameUTF8);
  g_free(filenameUTF8);
  gtk_label_set_markup(GTK_LABEL(labelVisuPlaneList), markup);
  g_free(markup);

  /* Since we've got a file name, save button can be activated. */
  gtk_widget_set_sensitive(buttonSaveList, TRUE);
}
/**
 * visu_ui_panel_planes_load:
 * @dataObj: a #VisuData object to associate the planes to (required
 *           to compute the intersection with the bounding box) ;
 * @filename: the path to the file to load ;
 * @error: a pointer to a GError handler to reccord possible failure.
 *
 * This method is used to parse and load an XML file containing
 * planes informations. The panel must have been initialised before
 * calling this method. The @error argument is required (can't be NULL),
 * since it is set if something went wrong.
 *
 * Returns: TRUE if visu_ui_panel_planes_applyHidingScheme().
 */
gboolean visu_ui_panel_planes_load(VisuData *dataObj, gchar *filename, GError **error)
{
  VisuPlane **list;
  gboolean res;
  int hide, i;

  g_return_val_if_fail(dataObj && filename && error && *error == (GError*)0, FALSE);

  if (!isVisuPlanesInitialised)
    createAndInitVisuPlanePanel(dataObj);

  /* Try to load the file. */
  list = (VisuPlane**)0;
  res = visu_plane_class_parseXMLFile(filename, &list, error);
  if (!res)
    return FALSE;
  DBG_fprintf(stderr, "Panel VisuPlanes: found planes in file '%s'.\n", filename);

  /* Begin to compute intersections of all planes and then
     add then to the list. */
  gtk_list_store_clear(listStoredVisuPlanes);
  for (i = 0; list[i]; i++)
    {
      DBG_fprintf(stderr, " | %d plane %p.\n", i, (gpointer)list[i]);
      hide = visu_plane_getHiddenState(list[i]);
      visu_ui_panel_planes_add(list[i], (hide != VISU_PLANE_SIDE_NONE),
                               (hide == VISU_PLANE_SIDE_MINUS));
      g_object_unref(G_OBJECT(list[i]));
      visu_boxed_setBox(VISU_BOXED(list[i]), VISU_BOXED(dataObj), TRUE);
    }
  g_free(list);

  /* Save the file name. */
  if (currentSaveListFile)
    g_free(currentSaveListFile);
  currentSaveListFile = g_strdup(filename);

  setCurrentSaveFile();

  return visu_gl_ext_getActive(VISU_GL_EXT(visu_gl_ext_planes_getDefault()));
}
static void onVisuPlaneListOpen(GtkButton *button _U_, gpointer data _U_)
{
  GtkWidget *openDialog;
  gchar *filename, *directory;
  VisuData *dataObj;
  gboolean res;
  GError *error;
  GtkWindow *parent;
  GtkFileFilter *filter;
  
  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes));
  g_return_if_fail(dataObj);

  parent = visu_ui_panel_getContainerWindow(VISU_UI_PANEL(panelPlanes));
  openDialog = gtk_file_chooser_dialog_new(_("Choose a file with a list of planes"),
                                           GTK_WINDOW(parent),
					   GTK_FILE_CHOOSER_ACTION_OPEN,
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					   NULL);
  gtk_window_set_modal(GTK_WINDOW(openDialog), TRUE);
  gtk_window_set_transient_for(GTK_WINDOW(openDialog), GTK_WINDOW(parent));
  gtk_window_set_position(GTK_WINDOW(openDialog), GTK_WIN_POS_CENTER_ON_PARENT);
  directory = visu_ui_getLastOpenDirectory();
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(openDialog), directory);
  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(openDialog), FALSE);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("Plane description (*.xml)"));
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(openDialog), filter);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("All files"));
  gtk_file_filter_add_pattern (filter, "*");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(openDialog), filter);

  if (gtk_dialog_run(GTK_DIALOG(openDialog)) != GTK_RESPONSE_ACCEPT)
    {
      gtk_widget_destroy(openDialog);
      return;
    }
  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(openDialog));
  gtk_widget_destroy(openDialog);
  error = (GError*)0;
  res = visu_ui_panel_planes_load(dataObj, filename, &error);
  g_free(filename);
  if (error)
    {
      visu_ui_raiseWarning(_("Loading a file"), error->message, (GtkWindow*)0);
      g_error_free(error);
      return;
    }
  /* If the extension is actually used, recompute the visibility of nodes. */
  if (res)
    {
      visu_ui_panel_planes_applyHidingScheme(dataObj);
      VISU_REDRAW_ADD;
    }
}
static gboolean callbackSave(gchar* filename)
{
  VisuPlane **list;
  GError *error;
  gboolean res;

  g_return_val_if_fail(filename, FALSE);

  /* Get all planes in a list. */
  list = visu_ui_panel_planes_getAll(FALSE);

  /* Give the list to the save method. */
  error = (GError*)0;
  res = visu_plane_class_exportXMLFile(filename, list, &error);
  if (!res)
    {
      visu_ui_raiseWarning(_("Saving a file"), error->message, (GtkWindow*)0);
      g_clear_error(&error);
      return FALSE;
    }

  /* Free the used list. */
  g_free(list);

  return TRUE;
}
static void onVisuPlaneListSave(GtkButton *button _U_, gpointer data _U_)
{
  callbackSave(currentSaveListFile);
}
static void onVisuPlaneListSaveAs(GtkButton *button _U_, gpointer data _U_)
{
  gchar *filename;
  gboolean res;
  
  filename =
    visu_ui_value_io_getFilename(GTK_WINDOW(visu_ui_panel_getContainerWindow
				       (VISU_UI_PANEL(panelPlanes))));
  if (!filename)
    return;

  res = callbackSave(filename);
  if (res)
    {
      currentSaveListFile = filename;
      setCurrentSaveFile();
    }
  else
    g_free(filename);
}
static void onOrientationChanged(VisuUiOrientationChooser *orientationChooser,
				 gpointer data _U_)
{
  int i;
  float values[3];

/*   for (i = 0; i < 2; i++) */
/*     g_signal_handler_block(G_OBJECT(entryNVect[i]), */
/* 			   signalsOrientation[i]); */

  visu_ui_orientation_chooser_getOrthoValues(orientationChooser, values);

  for (i = 0; i < 3; i++)
    visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[i]), (double)values[i]);

/*   for (i = 0; i < 2; i++) */
/*     g_signal_handler_unblock(G_OBJECT(entryNVect[i]), */
/* 			     signalsOrientation[i]); */
}
static void onVisuUiOrientationChooser(GtkButton *button _U_, gpointer data _U_)
{
  float values[3];
  int i;

  if (!orientationChooser)
    {
      orientationChooser = visu_ui_orientation_chooser_new
	(VISU_UI_ORIENTATION_NORMAL, TRUE,
	 visu_ui_panel_getData(VISU_UI_PANEL(panelPlanes)),
	 GTK_WINDOW(visu_ui_dock_window_getWindow
		    (visu_ui_panel_getContainer(VISU_UI_PANEL(panelPlanes)))));
/*       gtk_window_set_modal(GTK_WINDOW(orientationChooser), TRUE); */
      for (i = 0; i < 3; i++)
	values[i] = (float)visu_ui_numerical_entry_getValue
	  (VISU_UI_NUMERICAL_ENTRY(entryNVect[i]));
      visu_ui_orientation_chooser_setOrthoValues(VISU_UI_ORIENTATION_CHOOSER(orientationChooser),
					values);
      g_signal_connect(G_OBJECT(orientationChooser), "values-changed",
		       G_CALLBACK(onOrientationChanged), (gpointer)0);
    }
  else
    gtk_window_present(GTK_WINDOW(orientationChooser));
  
  gtk_widget_show(orientationChooser);
  switch (gtk_dialog_run(GTK_DIALOG(orientationChooser)))
    {
    case GTK_RESPONSE_ACCEPT:
      DBG_fprintf(stderr, "Panel VisuPlanes: accept changings on orientation.\n");
      break;
    default:
      DBG_fprintf(stderr, "Panel VisuPlanes: reset values on orientation.\n");
      for (i = 0; i < 3; i++)
	{
/* 	  g_signal_handler_block(G_OBJECT(entryNVect[i]), */
/* 				 signalsOrientation[i]); */
	  visu_ui_numerical_entry_setValue(VISU_UI_NUMERICAL_ENTRY(entryNVect[i]), (double)values[i]);
/* 	  g_signal_handler_unblock(G_OBJECT(entryNVect[i]), */
/* 				   signalsOrientation[i]); */
	}
    }
  DBG_fprintf(stderr, "Panel VisuPlanes: orientation object destroy.\n");
  gtk_widget_destroy(orientationChooser);
  orientationChooser = (GtkWidget*)0;
}
