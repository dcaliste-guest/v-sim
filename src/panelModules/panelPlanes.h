/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef PANELPLANES_H
#define PANELPLANES_H

#include <glib.h>
#include <gtk/gtk.h>

#include <visu_data.h>
#include <extraFunctions/plane.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>

/**
 * VisuUiPanelPlanesColumnId:
 * @VISU_UI_PANEL_PLANES_DRAWN: a boolean, code if the plane is drawn or not.
 * @VISU_UI_PANEL_PLANES_LABEL: a string, the description of the plane (normal vector
 *                      and distance to origin).
 * @VISU_UI_PANEL_PLANES_HIDE_IS_ON: a boolean, code if the plane hide elements on one side.
 * @VISU_UI_PANEL_PLANES_HIDDEN_SIDE: a boolean, code for the side.
 * @VISU_UI_PANEL_PLANES_COLOR_PIXBUF: a pixbuf, a small colored square.
 * @VISU_UI_PANEL_PLANES_POINTER: the pointer to the #VisuPlane object.
 * @VISU_UI_PANEL_PLANES_N_COLUMNS: the number of columns.
 *
 * Thesse are the description of the columns stored in the #GtkListStore
 * of this panel. See visu_ui_panel_planes_getList() to access this liststore.
 */
typedef enum
  {
    VISU_UI_PANEL_PLANES_DRAWN,
    VISU_UI_PANEL_PLANES_LABEL,
    VISU_UI_PANEL_PLANES_HIDE_IS_ON,
    VISU_UI_PANEL_PLANES_HIDDEN_SIDE,
    VISU_UI_PANEL_PLANES_COLOR_PIXBUF,
    VISU_UI_PANEL_PLANES_POINTER,
    VISU_UI_PANEL_PLANES_N_COLUMNS
  } VisuUiPanelPlanesColumnId;

VisuUiPanel* visu_ui_panel_planes_init();

gboolean visu_ui_panel_planes_load(VisuData *dataObj, gchar *filename, GError **error);
gboolean visu_ui_panel_planes_setUsed(gboolean value);
gboolean visu_ui_panel_planes_setRendered(VisuPlane *plane, gboolean status);
gboolean visu_ui_panel_planes_applyHidingScheme(VisuData *data);
GtkListStore* visu_ui_panel_planes_getList();
VisuPlane** visu_ui_panel_planes_getAll(gboolean maskingOnly);
gboolean visu_ui_panel_planes_add(VisuPlane *plane,
                                  gboolean hidingStatus, gboolean hidingSide);

#endif
