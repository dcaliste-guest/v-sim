/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef PANELSURFACESTOOLS_H
#define PANELSURFACESTOOLS_H

#include <gtk_main.h>

#if GTK_MAJOR_VERSION < 3
/**
 * gtk_tree_model_iter_previous:
 * @tree_model: the #GtkTreeModel to get the previous iteration from ;
 * @iter: a location to a #GtkTreeIter to store the result.
 * 
 * Similar to gtk_tree_model_iter_next(). See 
 * <ulink linkend="http://www.gtk.org">www.gtk.org</ulink>
 * for more info.
 * 
 * Return value: TRUE if @iter has been changed to the next node.
 **/
gboolean gtk_tree_model_iter_previous(GtkTreeModel *tree_model, GtkTreeIter *iter);
#endif

/**
 * visu_ui_panel_surfaces_tools_fileWidget:
 * 
 * Creates a widget containing an interface to merge surf files. 
 * Don't use two of them simultaneously,
 * they would conflict.
 * 
 * Return value: an allocated GtkWidget containing the interface.
 **/
GtkWidget *visu_ui_panel_surfaces_tools_fileWidget();

/**
 * visu_ui_panel_surfaces_tools_convertWidget:
 * 
 * Creates a widget containing the whole pot2surf frontend. Don't use two of them simultaneously,
 * they would conflict.
 * 
 * Return value: an allocated GtkWidget containing the interface.
 **/
GtkWidget *visu_ui_panel_surfaces_tools_convertWidget();

/**
 * visu_ui_panel_surfaces_tools_init:
 * 
 * Opens a new window containing tools to operate on surf and pot files.
 **/
void visu_ui_panel_surfaces_tools_init();

#endif
