/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2012)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2012)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <glib.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "atomic_yaml.h"
#include "renderingAtomic.h"

#include <coreTools/atoms_yaml.h>
#include <coreTools/toolFileFormat.h>
#include <visu_data.h>

#ifdef HAVE_YAML
static gboolean loadYaml(VisuData *data, const gchar* filename,
                         ToolFileFormat *format, int nSet,
                         GCancellable *cancel, GError **error);
#endif

/**
 * SECTION:atomic_yaml
 * @short_description: Method to load YAML position file.
 *
 * <para>YAML format is a plain text format to store atomic
 * positions. This format is defined and used by BigDFT. It has
 * meta-data available to store forces, total energy, units, boundary
 * conditions...</para>
 */

/**
 * visu_rendering_atomic_yaml_init: (skip)
 * @method: the #VisuRendering method to be associated to.
 *
 * Create the structure that gives access to a load method and
 * some description of it (file formats associated, name...). Internal
 * use only
 */
void visu_rendering_atomic_yaml_init(VisuRendering *method)
{
#ifdef HAVE_YAML
  const gchar *typeYAML[] = {"*.yaml", (char*)0};

  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("BigDFT YAML format"),
                                                    typeYAML),
                               110, loadYaml);
#else
  g_warning("No YAML support for method '%s'.", visu_rendering_getName(method, FALSE));
#endif
}

#ifdef HAVE_YAML
static gboolean loadYaml(VisuData *data, const gchar* filename,
                         ToolFileFormat *format _U_, int nSet,
                         GCancellable *cancel _U_, GError **error)
{
  PosinpList *lst, *tmp;
  PosinpAtoms *atoms;
  guint nSets, i;
  GArray *types, *nattyp;
  VisuElement *type;
  double boxGeometry[6];
  float xyz[3], uvw[3], *forces;
  char *message;
  VisuBox *box;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  message = (char*)0;
  lst = posinp_yaml_parse(filename, &message);
  if (!lst)
    {
      if (message)
        free(message);
      return FALSE;
    }
  
  DBG_fprintf(stderr, "Atomic YAML: loading '%s' as a YAML file.\n", filename);
  for (nSets = 0, tmp = lst; tmp && nSets < (guint)nSet; tmp = tmp->next);
  if (tmp && ((tmp->data && tmp->data->nat > 0) ||
              (message && !strstr(message, "<document start>"))))
    atoms = tmp->data;
  else
    {
      if (message)
        free(message);
      posinp_yaml_free_list(lst);
      return FALSE;
    }
  if (message)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           "%s", message);
      free(message);
      posinp_yaml_free_list(lst);
      return TRUE;
    }

  /* Counting the number of sets. */
  nSets = 0;
  for (tmp = lst; tmp; tmp = tmp->next)
    nSets += 1;
  visu_data_setNSubset(data, nSets);

  /* Set comments. */
  for (tmp = lst, i = 0; tmp; tmp = tmp->next, i++)
    visu_data_setFileCommentary(data, tmp->data->comment, i);
  /* Set metadata. */
  if (!isnan(atoms->energy))
    switch (atoms->eunits)
      {
      case (POSINP_ENERG_UNITS_HARTREE):
        g_object_set(G_OBJECT(data), "totalEnergy", atoms->energy * 27.2113834, NULL);
        break;
      case (POSINP_ENERG_UNITS_RYDBERG):
        g_object_set(G_OBJECT(data), "totalEnergy", atoms->energy * 0.5 * 27.2113834, NULL);
        break;
      case (POSINP_ENERG_UNITS_EV):
      default:
        g_object_set(G_OBJECT(data), "totalEnergy", atoms->energy, NULL);
        break;
    }

  /* Setup the population. */
  types  = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), atoms->ntypes);
  for (i = 0; i < atoms->ntypes; i++)
    {
      type = visu_element_retrieveFromName(atoms->atomnames[i], (gboolean*)0);
      g_array_append_val(types, type);
    }
  nattyp = g_array_sized_new(FALSE, TRUE, sizeof(guint), atoms->ntypes);
  g_array_set_size(nattyp, atoms->ntypes);
  for (i = 0; i < atoms->nat;  i++)
    *(&g_array_index(nattyp, guint, atoms->iatype[i])) += 1;
  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  DBG_fprintf(stderr, "Atomic YAML: there are %d types in this file.\n", atoms->ntypes);
  if (DEBUG)
    {
      for (i = 0; i < atoms->ntypes; i++)
	DBG_fprintf(stderr, " | %d atom(s) for type %d.\n",
                    g_array_index(nattyp, guint, i), i);
    }
  g_array_free(nattyp, TRUE);
  g_array_free(types, TRUE);

  if (sin(atoms->angdeg[2]) == 0.f)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Wrong (ab) angle, should be"
                             " different from 0[pi].\n\n"
                             " Quoting '%g'.\n"), atoms->angdeg[2]);
      posinp_yaml_free_list(lst);
      return TRUE;
    }

  /* We remove the infinity. */
  switch (atoms->BC)
    {
    case (POSINP_BC_FREE):
      atoms->acell[0] = 1.;
      atoms->acell[1] = 1.;
      atoms->acell[2] = 1.;
      break;
    case (POSINP_BC_SURFACE_XZ):
      atoms->acell[1] = 1.;
      break;
    default:
      break;
    }

  boxGeometry[0] = atoms->acell[0];
  boxGeometry[1] = atoms->acell[1] * cos(G_PI * atoms->angdeg[2] / 180.);
  boxGeometry[2] = atoms->acell[1] * sin(G_PI * atoms->angdeg[2] / 180.);
  boxGeometry[3] = atoms->acell[2] * cos(G_PI * atoms->angdeg[1] / 180.);
  boxGeometry[4] = atoms->acell[2] * (cos(G_PI * atoms->angdeg[0] / 180.) -
                                      cos(G_PI * atoms->angdeg[1] / 180.) *
                                      cos(G_PI * atoms->angdeg[2] / 180.)) /
    sin(G_PI * atoms->angdeg[2] / 180.);
  boxGeometry[5] = sqrt(atoms->acell[2] * atoms->acell[2] -
                        boxGeometry[3] * boxGeometry[3] -
                        boxGeometry[4] * boxGeometry[4]);
  boxGeometry[5] *= (atoms->angdeg[1] < 0.)?-1.:+1.;

  /* Always set the given box, in case coordinates are reduced. */
  box = (VisuBox*)0;
  switch (atoms->BC)
    {
    case (POSINP_BC_PERIODIC):
      box = visu_box_new(boxGeometry, VISU_BOX_PERIODIC);
      break;
    case (POSINP_BC_FREE):
      box = visu_box_new(boxGeometry, VISU_BOX_FREE);
      break;
    case (POSINP_BC_SURFACE_XZ):
      box = visu_box_new(boxGeometry, VISU_BOX_SURFACE_ZX);
      break;
    default:
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Unsupported boundary conditions.\n"));
      posinp_yaml_free_list(lst);
      return TRUE;
    }
  visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box), FALSE);
  g_object_unref(box);

  for (i = 0; i < atoms->nat; i++)
    {
      xyz[0] = uvw[0] = (float)atoms->rxyz[3 * i + 0];
      xyz[1] = uvw[1] = (float)atoms->rxyz[3 * i + 1];
      xyz[2] = uvw[2] = (float)atoms->rxyz[3 * i + 2];
      if (atoms->units == 3)
	visu_box_convertBoxCoordinatestoXYZ(box, xyz, uvw);
      visu_data_addNodeFromIndex(data, atoms->iatype[i], xyz, FALSE, FALSE);
    }

  /* In free boundary conditions, find the bounding box. */
  if (atoms->BC != POSINP_BC_PERIODIC)
    visu_data_setTightBox(data);

  /* We finish with the box description. */
  DBG_fprintf(stderr, "Atomic YAML: apply the box geometry and set the unit.\n");
  visu_box_setMargin(box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, box), TRUE);
  switch (atoms->Units)
    {
    case (POSINP_CELL_UNITS_ANGSTROEM):
      visu_box_setUnit(box, TOOL_UNITS_ANGSTROEM);
      break;
    case (POSINP_CELL_UNITS_BOHR):
      visu_box_setUnit(box, TOOL_UNITS_BOHR);
      break;
    default:
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
                           _("Unsupported units.\n"));
      posinp_yaml_free_list(lst);
      return TRUE;
    }

  /* Store the forces, if any. */
  if (atoms->fxyz)
    {
      forces = g_malloc(sizeof(float) * 3 * atoms->nat);
      for (i = 0; i < atoms->nat; i++)
        {
          forces[3 * i + 0] = (float)atoms->fxyz[3 * i + 0];
          forces[3 * i + 1] = (float)atoms->fxyz[3 * i + 1];
          forces[3 * i + 2] = (float)atoms->fxyz[3 * i + 2];
        }
      visu_rendering_atomic_setForces(data, forces);
      g_free(forces);
    }

  posinp_yaml_free_list(lst);

  return TRUE;
}
#endif
