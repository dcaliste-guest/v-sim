/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <glib.h>

#include <string.h>

#include "renderingAtomic.h"
#include "renderingAtomic_ascii.h"
#include "renderingAtomic_d3.h"
#include "atomic_xyz.h"
#include "atomic_yaml.h"
#include <visu_basic.h>
#include <visu_elements.h>
#include <visu_configFile.h>
#include <openGLFunctions/view.h>
#include <openGLFunctions/objectList.h>
#include <opengl.h>
#include <coreTools/toolConfigFile.h>
#include <coreTools/toolPhysic.h>

#include <GL/gl.h>
#include <GL/glu.h> 

#include <math.h>

/**
 * SECTION:renderingAtomic
 * @short_description: A rendering method to draw atoms positioned in
 * a box.
 *
 * <para> This the main part of a rendering method made to represent
 * atomic positions. It draws either spheres or cubes depending of the
 * elements properties. The radius (for the sphere) or the length of
 * the sides of the cubes can be tuned.</para>
 */

#define FORCES_MAX "max_" VISU_RENDERING_ATOMIC_FORCES

/* The OpenGL identifier to store the glObjectLists that
   describe the spheres. */
/* int identifierSpheres; */

/* The icosahedron drawing. */
#define X .525731112119133606 
#define Z .850650808352039932
static GLfloat vdata[12][3] = {    
   {X, 0.0, -Z}, {-X, 0.0, -Z}, {X, 0.0, Z}, {-X, 0.0, Z},    
   {0.0, -Z, -X}, {0.0, -Z, X}, {0.0, Z, -X}, {0.0, Z, X},    
   {-Z, -X, 0.0}, {Z, -X, 0.0}, {-Z, X, 0.0}, {Z, X, 0.0} 
};
static GLuint tindices[20][3] = { 
   {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},    
   {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},    
   {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6}, 
   {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11} };

#define RADIUS_DEFAULT 1.
#define SHAPE_DEFAULT VISU_RENDERING_ATOMIC_SPHERE
#define RATIO_DEFAULT 2.
#define PHI_DEFAULT  0.
#define THETA_DEFAULT 90.
/* Dealing with the radius resource. */
#define FLAG_RESOURCE_RADIUS_SHAPE "atomic_radius_shape"
#define DESC_RESOURCE_RADIUS_SHAPE "The radius of the element and its shape, a real > 0. & [Sphere Cube Elipsoid Point]"
static gboolean readAtomicRadiusShape(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
                                      VisuData *dataObj, VisuGlView *view,
                                      GError **error);
/* These functions write all the element list to export there associated resources. */
static void exportAtomicRadiusShape(GString *data, VisuData* dataObj, VisuGlView *view);
#define FLAG_PARAMETER_SHAPE "atomic_sphere_method"
#define DESC_PARAMETER_SHAPE "The sphere drawing method, [GluSphere Icosahedron]"
static gboolean readAtomicShape(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				VisuData *dataObj, VisuGlView *view, GError **error);
static void exportAtomicShape(GString *data, VisuData* dataObj, VisuGlView *view);
enum
  {
    sphere_glu,
    sphere_icosahedron,
    sphere_nb
  };
static guint sphereMethod = sphere_glu;
const char* sphereName[sphere_nb + 1] = {"GluSphere", "Icosahedron", (const char*)0};

const char* shapeName[VISU_RENDERING_ATOMIC_N_SHAPES + 1] = {"Sphere", "Cube", "Elipsoid", "Point", "Torus", (const char*)0};
const char* shapeNameI18n[VISU_RENDERING_ATOMIC_N_SHAPES + 1];

static VisuDataNode *dataNodeForces;

struct atomicResources
{
  /* One of this structure is associated to each element. */

  /* The radius of the element. */
  float radius;
  /* The ratio long axis, short axis when the shape is an elipsoid. */
  float ratio;
  /* The angles positioning of the long axis when the shape is
     an elipsoid. */
  float phi, theta;

  /* The shape used. */
  VisuRenderingAtomicShapeId shape;

  /* An id of the list associated with the form. */
  int openGLIdentifier;
};

/* Local callbacks. */
static void visu_rendering_atomic_dispose(GObject* obj);
static void visu_rendering_atomic_finalize(GObject* obj);
static int createShape(VisuElement* ele, VisuGlView *view);
static void positionShape(VisuData *visuData, VisuNode *node,
                          VisuElement* ele, int glEleId);
static int getShape(VisuElement *element);

enum {
  FORCES_CHANGED,
  LAST_SIGNAL
};

/* Internal variables. */
static guint atomic_signals[LAST_SIGNAL] = { 0 };

/**
 * VisuRenderingAtomic:
 *
 * An opaque structure.
 */
struct _VisuRenderingAtomic
{
  VisuRendering method;

  gboolean dispose_has_run;
};

/**
 * VisuRenderingAtomicClass:
 *
 * An opaque structure.
 */
struct _VisuRenderingAtomicClass
{
  VisuRenderingClass parent_class;
};

static VisuRenderingAtomic *atomic = (VisuRenderingAtomic*)0;

G_DEFINE_TYPE(VisuRenderingAtomic, visu_rendering_atomic, VISU_TYPE_RENDERING)

static void visu_rendering_atomic_class_init(VisuRenderingAtomicClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Visu RenderingAtomic: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_rendering_atomic_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_rendering_atomic_finalize;
  VISU_RENDERING_CLASS(klass)->createElement  = createShape;
  VISU_RENDERING_CLASS(klass)->getElementGlId = getShape;
  VISU_RENDERING_CLASS(klass)->createNode     = positionShape;
  VISU_RENDERING_CLASS(klass)->getNodeExtend  = visu_rendering_atomic_getRadius;

  /**
   * VisuRenderingAtomic::ForcesChanged:
   * @obj: the object emitting the signal.
   * @dataObj: the #VisuData forces are applied on.
   *
   * Emitted each time forces are updated.
   *
   * Since: 3.7
   */
  atomic_signals[FORCES_CHANGED] = 
    g_signal_new("ForcesChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, G_TYPE_OBJECT, NULL);

  /* Dealing with config files. */
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCE_RADIUS_SHAPE,
					  DESC_RESOURCE_RADIUS_SHAPE,
					  1, readAtomicRadiusShape);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportAtomicRadiusShape);
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
					  FLAG_PARAMETER_SHAPE,
					  DESC_PARAMETER_SHAPE,
					  1, readAtomicShape);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportAtomicShape);
					  
  shapeNameI18n[0] = _("Sphere");
  shapeNameI18n[1] = _("Cube");
  shapeNameI18n[2] = _("Elipsoid");
  shapeNameI18n[3] = _("Point");
  shapeNameI18n[4] = _("Torus");
  shapeNameI18n[5] = (const char*)0;

  /* Add possibilities to handle forces. */
  dataNodeForces = VISU_DATA_NODE(visu_data_node_new
                                  (VISU_RENDERING_ATOMIC_FORCES, G_TYPE_FLOAT));
  visu_data_node_setLabel(dataNodeForces, _("Forces"));
  visu_data_node_setEditable(dataNodeForces, FALSE);
}
static void visu_rendering_atomic_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu RenderingAtomic: dispose object %p.\n", (gpointer)obj);

  if (VISU_RENDERING_ATOMIC(obj)->dispose_has_run)
    return;

  VISU_RENDERING_ATOMIC(obj)->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_rendering_atomic_parent_class)->dispose(obj);
}
static void visu_rendering_atomic_finalize(GObject* obj)
{
  DBG_fprintf(stderr, "Visu RenderingAtomic: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_rendering_atomic_parent_class)->finalize(obj);
}

static void visu_rendering_atomic_init(VisuRenderingAtomic *obj)
{
  DBG_fprintf(stderr, "Visu renderingAtomic: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->dispose_has_run = FALSE;

  g_return_if_fail((atomic == (VisuRenderingAtomic*)0));
  atomic = obj;
}

static struct atomicResources *getRadiusAndShape(VisuElement *ele);

/**
 * visu_rendering_atomic_new:
 *
 * Create the structure and initialise its values.
 *
 * Since: 3.6
 *
 * Returns: a newly allocate #VisuRenderingAtomic object.
 */
VisuRenderingAtomic* visu_rendering_atomic_new()
{
  char *descr = _("It draws spheres at specified positions to represent atoms."
		  " The radius of the sphere can vary.");
  gchar *iconPath;
  VisuRenderingAtomic* atom;

  DBG_fprintf(stderr,"Initialising the atomic rendering method...\n");
  atom = VISU_RENDERING_ATOMIC(g_object_new(VISU_TYPE_RENDERING_ATOMIC,
                                            "name", VISU_RENDERING_ATOMIC_NAME,
                                            "label", _(VISU_RENDERING_ATOMIC_NAME),
                                            "description", descr, 
                                            "nFiles", 1, NULL));
  visu_rendering_setFileTypeLabel(VISU_RENDERING(atom), 0, _("Position files"));
  iconPath = g_build_filename(V_SIM_PIXMAPS_DIR, "stock-atomic.png", NULL);
  visu_rendering_setIcon(VISU_RENDERING(atom), iconPath);
  g_free(iconPath);

  initAtomicD3(VISU_RENDERING(atom));
  initAtomicAscii(VISU_RENDERING(atom));
  initAtomicXyz(VISU_RENDERING(atom));
  visu_rendering_atomic_yaml_init(VISU_RENDERING(atom));

  return atom;
}

static void freeForces(gpointer obj, gpointer data _U_)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(float) * 6, obj);
#else
  g_free(obj);
#endif
}
static gpointer newForces(gconstpointer obj, gpointer data _U_)
{
  float *fData;

#if GLIB_MINOR_VERSION > 9
  fData = g_slice_alloc(sizeof(float) * 6);
#else
  fData = g_malloc(sizeof(float) * 6);
#endif
  if (obj)
    memcpy(fData, obj, sizeof(float) * 6);
  else
    memset(fData, 0, sizeof(float) * 6);
    
  return (gpointer)fData;
}
static void initMaxForces(VisuElement *ele _U_, GValue *val)
{
  DBG_fprintf(stderr, " | init max modulus of val %p.\n", (gpointer)val);
  g_value_init(val, G_TYPE_FLOAT);
  g_value_set_float(val, -G_MAXFLOAT);
}
/**
 * visu_rendering_atomic_setForces:
 * @dataObj: a #VisuData object.
 * @forces: (array): an array of forces.
 *
 * Forces can be associated to nodes of @dataObj. The array @forces must
 * have the size of the number of nodes of @dataObj times three. Forces
 * are given in cartesian coordinates.
 *
 * Since: 3.7
 **/
void visu_rendering_atomic_setForces(VisuData *dataObj, float *forces)
{
  float *svgForces, *maxModulus;
  VisuNodeProperty *f;
  VisuNodeArrayIter iter;
  GValue forcesValue = {0, {{0}, {0}}};
  GValueArray *svgMaxModulus;
  GValue *val;

  DBG_fprintf(stderr, "Rendering Atomic: set forces.\n");
  maxModulus = g_malloc(sizeof(float) * 2);
  g_object_set_data(G_OBJECT(dataObj), FORCES_MAX, maxModulus);
  maxModulus[1] = 0.f;
  svgMaxModulus =
    visu_node_array_setElementProperty(VISU_NODE_ARRAY(dataObj),
                                       VISU_RENDERING_ATOMIC_FORCES_ELE_MAX,
                                       initMaxForces);
  f = visu_node_array_property_newPointer(VISU_NODE_ARRAY(dataObj),
                                          VISU_RENDERING_ATOMIC_FORCES,
                                          freeForces, newForces, (gpointer)0);

  g_value_init(&forcesValue, G_TYPE_POINTER);
  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for(visu_node_array_iterStartNumber(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
      visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(dataObj), &iter))
    {
      svgForces = newForces(forces + iter.node->number * 3, (gpointer)0);
      DBG_fprintf(stderr, " | %d -> %fx%fx%f\n", iter.node->number,
                  svgForces[0], svgForces[1], svgForces[2]);
      tool_matrix_cartesianToSpherical(svgForces + 3, svgForces);
      g_value_set_pointer(&forcesValue, svgForces);
      visu_node_property_setValue(f, iter.node, &forcesValue);
      val = g_value_array_get_nth(svgMaxModulus, iter.iElement);
      g_value_set_float(val, MAX(svgForces[3], g_value_get_float(val)));
      maxModulus[1] = MAX(maxModulus[1], svgForces[3]); 
    }
  DBG_fprintf(stderr, " | max forces %g.\n", maxModulus[1]);

  g_signal_emit(G_OBJECT(atomic),
                atomic_signals[FORCES_CHANGED], 0, dataObj, NULL);

  visu_data_node_setUsed(dataNodeForces, dataObj, 3);
}
/**
 * visu_rendering_atomic_getMaxForces:
 * @dataObj: a #VisuData object.
 *
 * If @dataObj has forces associated to (see
 * visu_rendering_atomic_setForces()), this routine provides the
 * modulus of the biggest force.
 *
 * Since: 3.7
 *
 * Returns: 0 if no forces have been associated.
 **/
float visu_rendering_atomic_getMaxForces(VisuData *dataObj)
{
  float *val;

  val = (float*)g_object_get_data(G_OBJECT(dataObj), FORCES_MAX);
  if (val)
    return val[1];
  else
    return 0.f;
}

static struct atomicResources *getRadiusAndShape(VisuElement *ele)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, (struct atomicResources*)0);

  DBG_fprintf(stderr, "Rendering Atomic: get resource structure (%s - %p)...\n",
              ele->name, (gpointer)ele);
  str = (struct atomicResources*)g_object_get_data(G_OBJECT(ele), "radiusAndShape");
  if (!str)
    {
      DBG_fprintf(stderr, "Rendering Atomic: create a new resource structure for '%s'.\n",
                  ele->name);
      str = g_malloc(sizeof(struct atomicResources));
      if (!tool_physic_getZFromSymbol((int*)0, &str->radius, ele->name))
	str->radius = RADIUS_DEFAULT;
      str->shape  = SHAPE_DEFAULT;
      str->ratio  = RATIO_DEFAULT;
      str->phi    = PHI_DEFAULT;
      str->theta  = THETA_DEFAULT;
      str->openGLIdentifier = visu_gl_objectlist_new(1);

      g_object_set_data(G_OBJECT(ele), "radiusAndShape", (gpointer)str);
    }
  return str;
}

/**
 * visu_rendering_atomic_getRadius:
 * @ele: a #VisuElement object.
 *
 * In the rendering atomic method, shapes are characterized by
 * a radius. This method gets it for the specified element. If this
 * element has no radius defined yet, the default value is
 * associated and returned.
 *
 * Returns: the radius of the specified element. A negative value if something
 *          goies wrong.
 */
float visu_rendering_atomic_getRadius(VisuElement *ele)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, RADIUS_DEFAULT);

  DBG_fprintf(stderr, "Rendering Atomic: get radius.\n");
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, -1.f);
  return str->radius;
}
/**
 * visu_rendering_atomic_getRadiusDefault:
 *
 * This method gets the default radius of the rendering atomic method.
 *
 * Returns: the default value for radius resource.
 */
float visu_rendering_atomic_getRadiusDefault()
{
  return RADIUS_DEFAULT;
}
/**
 * visu_rendering_atomic_setRadius:
 * @ele: a #VisuElement object ;
 * @value: a positive floating point value.
 *
 * This change the radius value of element @ele to @value.
 *
 * Returns: 1 if a call to visu_rendering_createElement() is required, 0 if not.
 */
gboolean visu_rendering_atomic_setRadius(VisuElement* ele, float value)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele && value > 0.f, FALSE);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  if (value == str->radius)
    return FALSE;
  else
    str->radius = value;

  DBG_fprintf(stderr, "Rendering Atomic: emit 'elementSizeChanged'.\n");
  if (atomic)
    g_signal_emit_by_name(G_OBJECT(atomic), "elementSizeChanged", value, NULL);
  DBG_fprintf(stderr, "Rendering Atomic: emission done.\n");

  /* Ask for reDraw if needed. */
  return TRUE;
}

/**
 * visu_rendering_atomic_getShape:
 * @ele: a #VisuElement object.
 *
 * In the rendering atomic method, shapes are multiple.
 * This method gets it for the specified element. Shapes are
 * characterized by their id, corresponding to an integer value.
 * Use the enum #VisuRenderingAtomicShapeId to associate an integer value
 * to a specific shape.
 *
 * Returns: the shape id of the element @ele.
 */
VisuRenderingAtomicShapeId visu_rendering_atomic_getShape(VisuElement *ele)
{
  struct atomicResources *str;

  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  return str->shape;
}
/**
 * visu_rendering_atomic_getShapeName:
 * @shape: an integer.
 *
 * This method does the corresponding between a shape id and
 * its name (a string value).
 *
 * Returns: the name associated to a shape.
 */
const char* visu_rendering_atomic_getShapeName(VisuRenderingAtomicShapeId shape)
{
  if (shape >= VISU_RENDERING_ATOMIC_N_SHAPES)
    return (const char*)0;
  else
    return shapeName[shape];
}
/**
 * visu_rendering_atomic_getShapeNameDefault:
 *
 * This method is used to retrieve the default name for shapes.
 *
 * Returns: the name associated to the default shape.
 */
const char* visu_rendering_atomic_getShapeNameDefault()
{
  return shapeName[SHAPE_DEFAULT];
}
/**
 * visu_rendering_atomic_getShapeDefault:
 *
 * This method gets the default shape.
 *
 * Returns: the default shape id.
 */
VisuRenderingAtomicShapeId visu_rendering_atomic_getShapeDefault()
{
  return SHAPE_DEFAULT;
}
/**
 * visu_rendering_atomic_setShape:
 * @ele: a #VisuElement object ;
 * @shape: an integer.
 *
 * This changes the shape of the element @ele to the shape defined by its id.
 *
 * Returns: 1 if a call to visu_rendering_createElement() is required, 0 if not.
 */
int visu_rendering_atomic_setShape(VisuElement *ele, VisuRenderingAtomicShapeId shape)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele && shape < VISU_RENDERING_ATOMIC_N_SHAPES, 0);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  if (shape == str->shape)
    return 0;
  else
    str->shape = shape;

  return 1;
}

/**
 * visu_rendering_atomic_setShapeFromName:
 * @ele: a #VisuElement object ;
 * @shape: a string.
 *
 * This method is equivalent to visu_rendering_atomic_setShape() but the shape is
 * defined by its name.
 *
 * Returns: 1 if a call to visu_rendering_createElement() is required, 0 if not.
 */
int visu_rendering_atomic_setShapeFromName(VisuElement *ele, const char* shape)
{
  int res, i;

  res = -1;
  for (i = 0; shapeName[i] && res < 0; i++)
    {
      if (!strcmp(shapeName[i], shape))
	res = i;
    }
  if (res < 0)
    {
      g_warning("Unknown shape name in the call of visu_rendering_atomic_setShapeFromName.");
      return 0;
    }
  else
    return visu_rendering_atomic_setShape(ele, res);
}
/**
 * visu_rendering_atomic_getAllShapes:
 *
 * This methods retrieve the whole list of shape names used by V_Sim for example
 * in the resources file. These names are not translated. If internationalized
 * names are required, use visu_rendering_atomic_getAllShapesI18n() instead.
 *
 * Returns: (transfer none) (array zero-terminated=1) (element-type filename): a pointer to a list of shape names (should not be modified or freed).
 */
const char** visu_rendering_atomic_getAllShapes()
{
  return shapeName;
}
/**
 * visu_rendering_atomic_getAllShapesI18n:
 *
 * This methods retrieve the whole list of shape names, translated strings.
 *
 * Returns: (transfer none) (array zero-terminated=1) (element-type utf8): a pointer to a list of shape names (should not be modified or freed).
 */
const char** visu_rendering_atomic_getAllShapesI18n()
{
  return shapeNameI18n;
}
/**
 * visu_rendering_atomic_setElipsoidParameters:
 * @ele: a #VisuElement object ;
 * @ratio: a float ;
 * @phi: a float ;
 * @theta: a float.
 *
 * Change the parameters for the elipsoid shape for the given @ele.
 * These parameters include a @ratio which is the ratio of the
 * long axis on the short one. Thus @ratio is always equal or greater than 1.
 * Arguments @theta and @phi are the direction of the long axis.
 *
 * Returns: TRUE if visu_rendering_createElement() should be called.
 */
gboolean visu_rendering_atomic_setElipsoidParameters(VisuElement *ele, float ratio,
					       float phi, float theta)
{
  struct atomicResources *str;
  gboolean refresh;

  g_return_val_if_fail(ele && (ratio >= 1.), 0);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  refresh = FALSE;
  if (ratio != str->ratio)
    {
      str->ratio = ratio;
      refresh = TRUE;
    }
  if (phi != str->phi)
    {
      str->phi = phi;
      refresh = TRUE;
    }
  if (theta != str->theta)
    {
      str->theta = theta;
      refresh = TRUE;
    }

  return refresh && (str->shape == VISU_RENDERING_ATOMIC_ELLIPSOID || str->shape == VISU_RENDERING_ATOMIC_TORUS);
}
/**
 * visu_rendering_atomic_setElipsoidRatio:
 * @ele: a #VisuElement object.
 * @ratio: a float ;
 *
 * Set the ratio parameter of the elipsoid shape for the element @ele.
 *
 * Returns: TRUE if visu_rendering_createElement() should be called.
 */
gboolean visu_rendering_atomic_setElipsoidRatio(VisuElement *ele, float ratio)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele && (ratio >= 1.), FALSE);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  if (ratio != str->ratio)
    {
      str->ratio = ratio;
      return (str->shape == VISU_RENDERING_ATOMIC_ELLIPSOID || str->shape == VISU_RENDERING_ATOMIC_TORUS);
    }
  return FALSE;
}
/**
 * visu_rendering_atomic_setElipsoidPhi:
 * @ele: a #VisuElement object.
 * @phi: a float ;
 *
 * Set the phi angle parameter of the elipsoid shape for the element @ele.
 *
 * Returns: TRUE if visu_rendering_createElement() should be called.
 */
gboolean visu_rendering_atomic_setElipsoidPhi(VisuElement *ele, float phi)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, FALSE);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  if (phi != str->phi)
    {
      str->phi = phi;
      return (str->shape == VISU_RENDERING_ATOMIC_ELLIPSOID || str->shape == VISU_RENDERING_ATOMIC_TORUS);
    }
  return FALSE;
}
/**
 * visu_rendering_atomic_setElipsoidTheta:
 * @ele: a #VisuElement object.
 * @theta: a float.
 *
 * Set the theta angle parameter of the elipsoid shape for the element @ele.
 *
 * Returns: TRUE if visu_rendering_createElement() should be called.
 */
gboolean visu_rendering_atomic_setElipsoidTheta(VisuElement *ele, float theta)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, FALSE);
  
  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, FALSE);

  if (theta != str->theta)
    {
      str->theta = theta;
      return (str->shape == VISU_RENDERING_ATOMIC_ELLIPSOID || str->shape == VISU_RENDERING_ATOMIC_TORUS);
    }
  return FALSE;
}
/**
 * visu_rendering_atomic_getElipsoidRatio:
 * @ele: a #VisuElement object.
 *
 * Retrieve the ratio parameter of the elipsoid shape for the element @ele.
 *
 * Returns: the ratio of the elipsoid.
 */
float visu_rendering_atomic_getElipsoidRatio(VisuElement *ele)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, RATIO_DEFAULT);

  str = (struct atomicResources*)g_object_get_data(G_OBJECT(ele), "radiusAndShape");
  if (str)
    return str->ratio;
  else
    return RATIO_DEFAULT;
}
/**
 * visu_rendering_atomic_getElipsoidPhi:
 * @ele: a #VisuElement object.
 *
 * Retrieve the phi angle parameter of the elipsoid shape for the element @ele.
 *
 * Returns: the phi angle of the elipsoid.
 */
float visu_rendering_atomic_getElipsoidPhi(VisuElement *ele)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, PHI_DEFAULT);

  str = (struct atomicResources*)g_object_get_data(G_OBJECT(ele), "radiusAndShape");
  if (str)
    return str->phi;
  else
    return PHI_DEFAULT;
}
/**
 * visu_rendering_atomic_getElipsoidTheta:
 * @ele: a #VisuElement object.
 *
 * Retrieve the theta angle parameter of the elipsoid shape for the element @ele.
 *
 * Returns: the theta angle of the elipsoid.
 */
float visu_rendering_atomic_getElipsoidTheta(VisuElement *ele)
{
  struct atomicResources *str;

  g_return_val_if_fail(ele, THETA_DEFAULT);

  str = (struct atomicResources*)g_object_get_data(G_OBJECT(ele), "radiusAndShape");
  if (str)
    return str->theta;
  else
    return THETA_DEFAULT;
}

static gboolean readAtomicRadiusShape(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				      VisuData *dataObj _U_, VisuGlView *view _U_,
                                      GError **error)
{
  VisuElement* ele;
  int shapeNum, i, token;
  float radius;
  gchar **shape;
  gchar **tokens;

  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Tokenize the line of values. */
  tokens = g_strsplit_set(g_strchug(lines[0]), " \n", TOOL_MAX_LINE_LENGTH);
  token = 0;

  DBG_fprintf(stderr, "Rendering Atomic: parse line.\n");
  /* Get the two elements. */
  if (!tool_config_file_readElementFromTokens(tokens, &token, &ele, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }

  /* Read 1 int. */
  if (!tool_config_file_readFloatFromTokens(tokens, &token, &radius, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  radius = CLAMP(radius, 0., G_MAXFLOAT);

  /* Read 1 string. */
  if (!tool_config_file_readStringFromTokens(tokens, &token, &shape, 1, position, error))
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  shapeNum = -1;
  for (i = 0; shapeName[i] && shapeNum < 0; i++)
    {
      if (!strcmp(shapeName[i], shape[0]))
	shapeNum = i;
    }
  if (shapeNum < 0)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the shape '%s' is unknown.\n"),
			   position, shape[0]);
      return FALSE;
    }
  g_strfreev(shape);
  DBG_fprintf(stderr, "Rendering Atomic: store values.\n");
  visu_rendering_atomic_setRadius(ele, radius);
  visu_rendering_atomic_setShape(ele, shapeNum);

  return TRUE;
}

/* These functions write all the element list to export there associated resources. */
static void exportAtomicRadiusShape(GString *data, VisuData *dataObj,
                                    VisuGlView *view _U_)
{
  GList *eleList, *pos;
  struct atomicResources *str;
  VisuNodeArrayIter iter;

  visu_config_file_exportComment(data, DESC_RESOURCE_RADIUS_SHAPE);
  /* We create a list of elements, or get the whole list. */
  if (dataObj)
    {
      eleList = (GList*)0;
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
      for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.element;
           visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter))
        eleList = g_list_prepend(eleList, (gpointer)iter.element);
    }
  else
    eleList = g_list_copy((GList*)visu_element_getAllElements());
  for (pos = eleList; pos; pos = g_list_next(pos))
    {
      str = (struct atomicResources*)g_object_get_data(G_OBJECT(pos->data), "radiusAndShape");
      if (str)
        visu_config_file_exportEntry(data, FLAG_RESOURCE_RADIUS_SHAPE,
                                     visu_element_getName(VISU_ELEMENT(pos->data)),
                                     "%10.3f %s", str->radius, shapeName[str->shape]);
    }
  visu_config_file_exportComment(data, "");
  g_list_free(eleList);
}

static gboolean readAtomicShape(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
				VisuData *dataObj _U_, VisuGlView *view _U_,
                                GError **error)
{
  gchar **vals;
  guint i;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readString(lines[0], position, &vals, 1, FALSE, error))
    return FALSE;

  for (i = 0; i < sphere_nb; i++)
    if (!strcmp(vals[0], sphereName[i]))
      {
	sphereMethod = i;
	break;
      }
  if (i == sphere_nb)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the sphere method '%s' is unknown.\n"),
			   position, vals[0]);
      g_strfreev(vals);
      return FALSE;
    }
  g_strfreev(vals);
  return TRUE;
}
static void exportAtomicShape(GString *data, VisuData* dataObj _U_, VisuGlView *view _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_SHAPE);
  g_string_append_printf(data, "%s: %s\n\n", FLAG_PARAMETER_SHAPE,
			 sphereName[sphereMethod]);
}


/***************/
/* OpenGL part */
/***************/

static void positionShape(VisuData *visuData, VisuNode *node,
                          VisuElement* ele, int eleGlId)
{
  float rgba[4];
  float xyz[3];
  float scale;

  visu_data_getNodePosition(visuData, node, xyz);
  scale = visu_data_getNodeScalingFactor(visuData, node);

  glPushMatrix();
  glTranslated(xyz[0], xyz[1], xyz[2]);
  if (visu_data_getUserColor(visuData, ele, node, rgba))
    visu_gl_setColor(ele->material, rgba);
  else if (visu_data_hasUserColorFunc(visuData))
    visu_gl_setColor(ele->material, ele->rgb);
  glScalef(scale, scale, scale);
  glCallList(eleGlId);
  glPopMatrix();
}
static void drawtriangle(float *v1, float *v2, float *v3) 
{ 
   glBegin(GL_TRIANGLES); 
      glNormal3fv(v1); glVertex3fv(v1);    
      glNormal3fv(v2); glVertex3fv(v2);    
      glNormal3fv(v3); glVertex3fv(v3);    
   glEnd(); 
}
static void normalize(float v[3]) {    
   GLfloat d = 1.f / sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]); 

   g_return_if_fail(d > 0.);
   v[0] *= d; v[1] *= d; v[2] *= d; 
}
static void subdivide(float *v1, float *v2, float *v3, int depth) 
{ 
  GLfloat v12[3], v23[3], v31[3];    
  GLint i;

  if (depth == 0)
    {
      drawtriangle(v1, v2, v3);
      return;
    }
  for (i = 0; i < 3; i++)
    { 
      v12[i] = v1[i]+v2[i]; 
      v23[i] = v2[i]+v3[i];     
      v31[i] = v3[i]+v1[i];    
    } 
  normalize(v12);    
  normalize(v23); 
  normalize(v31); 
  subdivide(v1, v12, v31, depth - 1);    
  subdivide(v2, v23, v12, depth - 1);    
  subdivide(v3, v31, v23, depth - 1);    
  subdivide(v12, v23, v31, depth - 1); 
}
static int getShape(VisuElement *element)
{
  struct atomicResources *str;

  str = getRadiusAndShape(element);
  g_return_val_if_fail(str, 0);

  return str->openGLIdentifier;
}
static int createShape(VisuElement* ele, VisuGlView *view)
{
  struct atomicResources *str;
  int nlat, i, nfac;
  GLUquadricObj *obj;

  g_return_val_if_fail(ele, -1);

  str = getRadiusAndShape(ele);
  g_return_val_if_fail(str, -1);

  nlat = visu_gl_view_getDetailLevel(view, str->radius);

  DBG_fprintf(stderr, "Rendering Atomic: creating '%s' for %s (%d - OpenGL id"
	      " %d - fac %d)\n", shapeName[str->shape],
	      ele->name, ele->typeNumber, str->openGLIdentifier,
	      nlat);
  if (nlat < 0)
    return -1;
   
  obj = gluNewQuadric();
  glNewList(str->openGLIdentifier, GL_COMPILE);
  switch (str->shape)
    {
    case VISU_RENDERING_ATOMIC_SPHERE:
      DBG_fprintf(stderr, " | use sphere method %d\n", sphereMethod);
      if (sphereMethod == sphere_glu)
	gluSphere(obj, (double)str->radius, nlat, nlat);
      else if (sphereMethod == sphere_icosahedron)
	{
	  nfac = (int)(log((float)(nlat + 2) / 4.f) / log(2.f));
	  DBG_fprintf(stderr, " | glusphere vs. icosahedron %dx%d\n",
		      nlat * nlat, 20 * (int)pow(4, nfac));
	  glPushMatrix();
	  glScalef(str->radius, str->radius, str->radius);
	  glBegin(GL_TRIANGLES);    
	  for (i = 0; i < 20; i++)
	    subdivide(&vdata[tindices[i][0]][0],       
		      &vdata[tindices[i][1]][0],       
		      &vdata[tindices[i][2]][0], nfac);
	  glEnd();
	  glPopMatrix();
	}
      else
	g_warning("Wrong sphere method.");
      break;
    case VISU_RENDERING_ATOMIC_ELLIPSOID:
      glPushMatrix();
      glRotatef(str->phi, 0., 0., 1.);
      glRotatef(str->theta, 0., 1., 0.);
      glScalef(1.0, 1.0, str->ratio);
      gluSphere(obj, (double)str->radius, nlat, nlat);
      glPopMatrix();
      break;
    case VISU_RENDERING_ATOMIC_POINT:
      glPointSize(MAX(1, (int)(str->radius * view->camera->gross * 5.)));
      glBegin(GL_POINTS);
      glVertex3f(0., 0., 0.);
      glEnd();
      break;
    case VISU_RENDERING_ATOMIC_CUBE:
      glBegin(GL_QUADS);

      glNormal3f(0., 0., 1.);
      glVertex3f(str->radius / 2., str->radius / 2., str->radius / 2.);
      glVertex3f(-str->radius / 2., str->radius / 2., str->radius / 2.);
      glVertex3f(-str->radius / 2., -str->radius / 2., str->radius / 2.);
      glVertex3f(str->radius / 2., -str->radius / 2., str->radius / 2.);

      glNormal3f(0., 0., -1.);
      glVertex3f(str->radius / 2.,str->radius / 2.,-str->radius / 2.);
      glVertex3f(str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(-str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(-str->radius / 2.,str->radius / 2.,-str->radius / 2.);

      glNormal3f(1., 0., 0.);
      glVertex3f(str->radius / 2.,str->radius / 2.,str->radius / 2.);
      glVertex3f(str->radius / 2.,-str->radius / 2.,str->radius / 2.);
      glVertex3f(str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(str->radius / 2.,str->radius / 2.,-str->radius / 2.);

      glNormal3f(-1., 0., 0.);
      glVertex3f(-str->radius / 2.,str->radius / 2.,str->radius / 2.);
      glVertex3f(-str->radius / 2.,str->radius / 2.,-str->radius / 2.);
      glVertex3f(-str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(-str->radius / 2.,-str->radius / 2.,str->radius / 2.);

      glNormal3f(0., 1., 0.);
      glVertex3f(-str->radius / 2.,str->radius / 2.,-str->radius / 2.);
      glVertex3f(-str->radius / 2.,str->radius / 2.,str->radius / 2.);
      glVertex3f(str->radius / 2.,str->radius / 2.,str->radius / 2.);
      glVertex3f(str->radius / 2.,str->radius / 2.,-str->radius / 2.);

      glNormal3f(0., -1., 0.);
      glVertex3f(-str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(str->radius / 2.,-str->radius / 2.,-str->radius / 2.);
      glVertex3f(str->radius / 2.,-str->radius / 2.,str->radius / 2.);
      glVertex3f(-str->radius / 2.,-str->radius / 2.,str->radius / 2.);

      glEnd();
      break;
    case VISU_RENDERING_ATOMIC_TORUS:
      
      glPushMatrix();
      glRotatef(str->phi, 0., 0., 1.);
      glRotatef(str->theta, 0., 1., 0.);
      visu_gl_drawTorus(obj, 0, str->radius, str->ratio, nlat, nlat, FALSE);
      glPopMatrix();
      
      break;
    default:
      g_warning("Unsupported shape id.");
    }
  glEndList();
  gluDeleteQuadric(obj);
   
  DBG_fprintf(stderr, " | shape created %d\n", str->openGLIdentifier);

  return (str->openGLIdentifier);
}
