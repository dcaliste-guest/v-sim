/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef RENDERINGATOMIC_H
#define RENDERINGATOMIC_H

#include <visu_rendering.h>
#include <visu_data.h>
#include <visu_object.h>
#include <visu_tools.h>
#include <coreTools/toolFileFormat.h>

G_BEGIN_DECLS

/* This method draws spheres to represent atoms.
   It introduces a new resource : the radius of
   the sphere. It can read data file either in d3
   format or in ascii format. */

/**
 * VisuRenderingAtomicShapeId:
 * @VISU_RENDERING_ATOMIC_SPHERE: draw sphere ;
 * @VISU_RENDERING_ATOMIC_CUBE: draw cube ;
 * @VISU_RENDERING_ATOMIC_ELLIPSOID: draw elipsoid ;
 * @VISU_RENDERING_ATOMIC_POINT: draw square dot ;
 * @VISU_RENDERING_ATOMIC_TORUS: draw torus ;
 * @VISU_RENDERING_ATOMIC_N_SHAPES: number of shapes.
 *
 * This enum is used as identifier for shapes managed by the
 * attomic rendering method.
 */
typedef enum 
{
  VISU_RENDERING_ATOMIC_SPHERE,
  VISU_RENDERING_ATOMIC_CUBE,
  VISU_RENDERING_ATOMIC_ELLIPSOID,
  VISU_RENDERING_ATOMIC_POINT,
  VISU_RENDERING_ATOMIC_TORUS,
  VISU_RENDERING_ATOMIC_N_SHAPES
} VisuRenderingAtomicShapeId;

/**
 * VISU_TYPE_RENDERING_ATOMIC:
 *
 * return the type of #VisuRenderingAtomic.
 */
#define VISU_TYPE_RENDERING_ATOMIC	     (visu_rendering_atomic_get_type ())
/**
 * VISU_RENDERING_ATOMIC:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuRenderingAtomic type.
 */
#define VISU_RENDERING_ATOMIC(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_RENDERING_ATOMIC, VisuRenderingAtomic))
/**
 * VISU_RENDERING_ATOMIC_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuRenderingAtomicClass.
 */
#define VISU_RENDERING_ATOMIC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_RENDERING_ATOMIC, VisuRenderingAtomicClass))
/**
 * VISU_IS_RENDERING_ATOMIC_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuRenderingAtomic object.
 */
#define VISU_IS_RENDERING_ATOMIC_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_RENDERING_ATOMIC))
/**
 * VISU_IS_RENDERING_ATOMIC_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuRenderingAtomicClass class.
 */
#define VISU_IS_RENDERING_ATOMIC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_RENDERING_ATOMIC))
/**
 * VISU_RENDERING_ATOMIC_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_RENDERING_ATOMIC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_RENDERING_ATOMIC, VisuRenderingAtomicClass))

/**
 * visu_rendering_atomic_get_type:
 *
 * This method returns the type of #VisuRenderingAtomic, use
 * VISU_TYPE_RENDERING_ATOMIC instead.
 *
 * Returns: the type of #VisuRenderingAtomic.
 */
GType visu_rendering_atomic_get_type(void);

/**
 * VISU_RENDERING_ATOMIC_NAME:
 *
 * Public name of the atomic rendering mode.
 */
#define VISU_RENDERING_ATOMIC_NAME "Atom visualisation"

/**
 * VISU_RENDERING_ATOMIC_FORCES:
 *
 * Public name of the #VisuNodeProperty used to store forces on atoms.
 */
#define VISU_RENDERING_ATOMIC_FORCES "forces_id"
/**
 * VISU_RENDERING_ATOMIC_FORCES_ELE_MAX:
 *
 * Public name of the #VisuNodeProperty used to store the max of forces per element.
 */
#define VISU_RENDERING_ATOMIC_FORCES_ELE_MAX "max_ele_forces_id"

typedef struct _VisuRenderingAtomicClass VisuRenderingAtomicClass;
typedef struct _VisuRenderingAtomic VisuRenderingAtomic;

VisuRenderingAtomic* visu_rendering_atomic_new();

/***********************/
/* Different resources */
/***********************/
float visu_rendering_atomic_getRadius(VisuElement *ele);
float visu_rendering_atomic_getRadiusDefault();
int visu_rendering_atomic_setRadius(VisuElement* ele, float value);

VisuRenderingAtomicShapeId visu_rendering_atomic_getShape(VisuElement *ele);
VisuRenderingAtomicShapeId visu_rendering_atomic_getShapeDefault();
const char* visu_rendering_atomic_getShapeName(VisuRenderingAtomicShapeId shape);
const char* visu_rendering_atomic_getShapeNameDefault();
int visu_rendering_atomic_setShape(VisuElement *ele, VisuRenderingAtomicShapeId shape);
int visu_rendering_atomic_setShapeFromName(VisuElement *ele, const char* shape);
const char** visu_rendering_atomic_getAllShapes();
const char** visu_rendering_atomic_getAllShapesI18n();

gboolean visu_rendering_atomic_setElipsoidParameters(VisuElement *ele, float ratio,
						    float phi, float theta);
float visu_rendering_atomic_getElipsoidRatio(VisuElement *ele);
float visu_rendering_atomic_getElipsoidPhi(VisuElement *ele);
float visu_rendering_atomic_getElipsoidTheta(VisuElement *ele);
gboolean visu_rendering_atomic_setElipsoidRatio(VisuElement *ele, float ratio);
gboolean visu_rendering_atomic_setElipsoidPhi(VisuElement *ele, float phi);
gboolean visu_rendering_atomic_setElipsoidTheta(VisuElement *ele, float theta);

void visu_rendering_atomic_setForces(VisuData *dataObj, float *forces);
float visu_rendering_atomic_getMaxForces(VisuData *dataObj);

G_END_DECLS


#endif
