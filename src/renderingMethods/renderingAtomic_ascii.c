/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "renderingAtomic_ascii.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "renderingAtomic.h"
#include <visu_rendering.h>
#include <visu_data.h>
#include <coreTools/toolPhysic.h>
#include <extraFunctions/extraNode.h>
#include <extraFunctions/vibration.h>

/**
 * SECTION:renderingAtomic_ascii
 * @short_description: Method to load ascii position file.
 *
 * <para>Ascii format is a plain text format to store elements and
 * their position. The first line is a commentary and is not
 * read. Then, the next two lines must contains each three numbers,
 * describing the box containing the data. The first number is the
 * length on x direction. The next two numbers are on one hand the
 * component on the same x direction and on the other hand the other
 * component on a direction orthogonal to the x direction, setting
 * thus the y direction. The three other numbers are the component of
 * the z direction on first, the x direction, then the y direction and
 * finally in a direction orthogonal both to x and y directions. After
 * these three first lines, all other lines can be blank, beginning
 * with a '#' character or containing three numbers and a label name
 * for the element.</para>
 *
 * <para>Some public methods here have been put for other loader which
 * wants to share the code of this loader (see the XYZ loader for
 * instance).</para>
 */

typedef struct AsciiKeywordData_
{
  gboolean reduced;
  gboolean angdeg;
  ToolUnits unit;
  VisuBoxBoundaries bc;
} AsciiKeywordData;
typedef struct AsciiMetaData_
{
  guint nqpt;
  gdouble totalEnergy;
  gboolean forces;
} AsciiMetaData;

static double readTotalEnergy(const gchar *str);
static gboolean loadAscii(VisuData *data, const gchar* filename,
			  ToolFileFormat *format, int nSet,
                          GCancellable *cancel, GError **error);
static int read_ascii_file(VisuData *data, GIOChannel *flux, GError **error);
static gboolean readFile_is_comment(char *str, AsciiKeywordData *kw,
				    AsciiMetaData *md);

/**
 * initAtomicAscii: (skip)
 * @method: the #VisuRendering method to be associated to.
 *
 * Create the structure that gives access to a load method and
 * some description of it (file formats associated, name...). Internal
 * use only
 */
void initAtomicAscii(VisuRendering *method)
{
  const gchar *typeASCII[] = {"*.ascii", (char*)0};

  visu_rendering_addFileFormat(method, 0,
                               tool_file_format_new(_("'x y z Element' format"),
                                                    typeASCII),
                               50, loadAscii);
}

/******************************************************************************/

static gboolean readFile_is_comment(char *str, AsciiKeywordData *kw,
				    AsciiMetaData *md)
{
  gchar **tokens;
  int i;

  if (kw && !strncmp(str + 1, "keyword", 7))
    {
      tokens = g_strsplit_set(str + 9, " ,\n\t\r", -1);
      for (i = 0; tokens[i]; i++)
	if (tokens[i][0] != '\0')
	  {
/* 	    tokens[i] = g_strstrip(tokens[i]); */
	    DBG_fprintf(stderr, "Atomic ASCII: get keyword '%s'.\n", tokens[i]);
	    if (!g_ascii_strcasecmp(tokens[i], "reduced"))
	      kw->reduced = TRUE;
	    else if (!g_ascii_strcasecmp(tokens[i], "angdeg"))
	      kw->angdeg = TRUE;
	    else if (!g_ascii_strcasecmp(tokens[i], "atomic") ||
		     !g_ascii_strcasecmp(tokens[i], "atomicd0") ||
		     !g_ascii_strcasecmp(tokens[i], "bohr") ||
		     !g_ascii_strcasecmp(tokens[i], "bohrd0"))
	      kw->unit = TOOL_UNITS_BOHR;
	    else if (!g_ascii_strcasecmp(tokens[i], "angstroem") ||
		     !g_ascii_strcasecmp(tokens[i], "angstroemd0"))
	      kw->unit = TOOL_UNITS_ANGSTROEM;
	    else if (!g_ascii_strcasecmp(tokens[i], "periodic"))
	      kw->bc = VISU_BOX_PERIODIC;
	    else if (!g_ascii_strcasecmp(tokens[i], "surface"))
	      kw->bc = VISU_BOX_SURFACE_ZX;
	    else if (!g_ascii_strcasecmp(tokens[i], "surfaceXY"))
	      kw->bc = VISU_BOX_SURFACE_XY;
	    else if (!g_ascii_strcasecmp(tokens[i], "freeBC"))
	      kw->bc = VISU_BOX_FREE;
	  }
      g_strfreev(tokens);
    }
  if (md && !g_ascii_strncasecmp(str + 1, "metaData", 8))
    {
      tokens = g_strsplit_set(str + 10, " ,\n\t\r", -1);
      for (i = 0; tokens[i]; i++)
	if (tokens[i][0] != '\0')
	  {
/* 	    tokens[i] = g_strstrip(tokens[i]); */
	    DBG_fprintf(stderr, "Atomic ASCII: get meta data '%s'.\n", tokens[i]);
	    if (!g_ascii_strncasecmp(tokens[i], "qpt", 3))
	      md->nqpt += 1;
	    else if (!g_ascii_strncasecmp(tokens[i], "totalEnergy", 11))
	      md->totalEnergy = readTotalEnergy(str + 10);
	    else if (!g_ascii_strncasecmp(tokens[i], "forces", 6))
	      md->forces = TRUE;
	  }
      g_strfreev(tokens);
    }
  if(str[0] == '#' || str[0] == '!')
    return TRUE;
  str = g_strstrip(str);
  return (str[0] == '\0');
}

static double readTotalEnergy(const gchar *str)
{
  gchar *chr, *tmpStr;
  int res, pos;
  double val;

  chr = strstr(str, "totalEnergy");
  chr = strchr(chr, '=');
  chr += 1;

  val = 999.;
  res = (chr)?sscanf(chr, "%lf%n", &val, &pos):0;
  if (res != 1)
    g_warning("syntax error for meta data 'totalEnergy' in '%s'.",
	      chr);
  else if (chr[pos] != '\0')
    {
      tmpStr = g_strdup(chr + pos);
      tmpStr = g_strstrip(tmpStr);
      if (!g_ascii_strncasecmp(tmpStr, "Ht", 2))
	val *= 27.21138386;
      else if (!g_ascii_strncasecmp(tmpStr, "Ry", 2))
	val *= .5 * 27.21138386;
      g_free(tmpStr);
    }
  DBG_fprintf(stderr, "Atomic ASCII: found 'totalEnergy' at %geV.\n", val);
  return val;
}

static gboolean readQpt(gchar *str, float qpt[4], float *disp, guint natom)
{
  gchar **values;
  gchar *ptr;
  guint j, k;

  ptr = strstr(str, "qpt");
  if (!ptr)
    return FALSE;
  ptr = strstr(ptr, "=");
  if (!ptr)
    {
      g_warning("syntax error for meta data 'qpt' in '%s'.", str);
      return FALSE;
    }

  DBG_fprintf(stderr, "Atomic ASCII: read qpt from '%s'\n", ptr);
  memset(disp, 0, sizeof(float) * 6 * natom);
  values = g_strsplit_set(ptr + 1, " [];\n\r\\\t", -1);
  k = 0;
  for (j = 0; values[j] && k < 4; j++)
    if (values[j][0] != '\0')
      {
	if (sscanf(values[j], "%f", qpt + k) != 1)
	  {
	    g_warning("Can't read a float value from qpt"
		      " keyword in '%s'.", values[j]);
	    qpt[k] = 0.f;
	  }
	DBG_fprintf(stderr, " | qpt[%d] = %g\n", k, qpt[k]);
	k += 1;
      }
  if (k != 4)
    g_warning("Can't read 4 float values from qpt"
	      " keyword in '%s'.", ptr);
  for (k = 0; values[j] && k < 6 * natom; j++)
    if (values[j][0] != '\0')
      {
	if (sscanf(values[j], "%f", disp + k) == 1)
	  k += 1;
	if (DEBUG && k % 6 == 5)
	  DBG_fprintf(stderr, " | u[%d] = (%g,%g,%g) + i(%g,%g,%g)\n", k / 6,
		      disp[(k / 6) * 6 + 0], disp[(k / 6) * 6 + 1], disp[(k / 6) * 6 + 2],
		      disp[(k / 6) * 6 + 3], disp[(k / 6) * 6 + 4], disp[(k / 6) * 6 + 5]);
      }
  if (k != natom * 6)
    g_warning("Can't read enough displacement values,"
	      " %d read while %d were awaited.", k, natom * 6);
  g_strfreev(values);

  return TRUE;
}

static gboolean readForces(gchar *str, float *forces, guint natom)
{
  gchar **values;
  gchar *ptr;
  guint j, k;

  ptr = strstr(str, "forces");
  if (!ptr)
    return FALSE;
  ptr = strstr(ptr, "=");
  if (!ptr)
    {
      g_warning("syntax error for meta data 'forces' in '%s'.", str);
      return FALSE;
    }

  DBG_fprintf(stderr, "Atomic ASCII: read forces from '%s'\n", ptr);
  memset(forces, 0, sizeof(float) * 6 * natom);
  values = g_strsplit_set(ptr + 1, " [];\n\r\\\t", -1);
  for (k = 0, j = 0; values[j] && k < 6 * natom; j++)
    if (values[j][0] != '\0')
      {
	if (sscanf(values[j], "%f", forces + k) == 1)
	  k += 1;
	if (DEBUG && k % 3 == 2)
	  DBG_fprintf(stderr, " | f[%d] = (%g,%g,%g)\n", k / 3,
		      forces[(k / 3) * 3 + 0],
                      forces[(k / 3) * 3 + 1],
                      forces[(k / 3) * 3 + 2]);
      }
  if (k != natom * 3)
    g_warning("Can't read enough force values,"
	      " %d read while %d were awaited.", k, natom * 3);
  g_strfreev(values);

  return TRUE;
}

gboolean continuousLine(GString *line)
{
  gsize len;

  len = line->len - 1;
  while (line->str[len] == '\n' || line->str[len] == '\r' ||
	 line->str[len] == '\t' || line->str[len] == ' ')
    len -= 1;

  return (line->str[len] == '\\');
}

/******************************************************************************/

/**
 * visu_meth_asciiFree:
 * @data: a pointer to #stuct VisuMethAscii instances.
 *
 * This method is used to free an allocated #stuct VisuMethAscii, and can
 * be used as a destructor when creating an hashtable with such datas.
 */
void visu_meth_asciiFree(gpointer data)
{
  g_free((struct VisuMethAscii*)data);
}
/**
 * visu_meth_asciiValToType:
 * @key: a #VisuElement pointer ;
 * @value: a #struct VisuMethAscii pointer ;
 * @data: a pointer on array of pointers of #VisuElement.
 *
 * This method is used in a g_hash_table_foreach() method that work on @data.
 * @data is an array of pointers to #VisuElement and this method puts its
 * nth value to point to @key. The value nth is read from the @pos attribute of
 * the structure object pointed by @value.
 */
void visu_meth_asciiValToType(gpointer key _U_, gpointer value, gpointer data)
{
  struct VisuMethAscii *infos;
  
  infos = (struct VisuMethAscii *)value;
  DBG_fprintf(stderr, " | %d -> '%s' (%p).\n", infos->pos,
              infos->ele->name, (gpointer)infos->ele);
  g_array_index((GArray*)data, VisuElement*, infos->pos) = infos->ele;
}
/**
 * visu_meth_asciiValToNb:
 * @key: a #VisuElement pointer ;
 * @value: a #struct VisuMethAscii pointer ;
 * @data: a pointer on array of int.
 *
 * This method is used in a g_hash_table_foreach() method that work on @data.
 * @data is an array of int and this method puts its
 * nth value to the value read from the @nbNodes attribute of
 * the structure object pointed by @value.
 */
void visu_meth_asciiValToNb(gpointer key _U_, gpointer value, gpointer data)
{
  struct VisuMethAscii *infos;
  
  infos = (struct VisuMethAscii *)value;
  DBG_fprintf(stderr, " | %d -> %d.\n", infos->pos, infos->nbNodes);
  g_array_index((GArray*)data, guint, infos->pos) = infos->nbNodes;
}
/******************************************************************************/

static gboolean loadAscii(VisuData *data, const gchar* filename,
			  ToolFileFormat *format _U_, int nSet _U_,
                          GCancellable *cancel _U_, GError **error)
{
  int res;
  GIOChannel *readFrom;
  GIOStatus status;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(data && filename, FALSE);

  readFrom = g_io_channel_new_file(filename, "r", error);
  if (!readFrom)
    return FALSE;

  /* Do the job of parsing. */
  res = read_ascii_file(data, readFrom, error);
  DBG_fprintf(stderr, "Atomic ASCII: parse return %d.\n", res);
  status = g_io_channel_shutdown(readFrom, FALSE, (GError**)0);
  g_io_channel_unref(readFrom);
  if (status != G_IO_STATUS_NORMAL)
    {
      DBG_fprintf(stderr, "Atomic ASCII: can't close file.\n");
      return FALSE;
    }

  if (res < 0)
    {
      /* The file is not an ascii file. */
      return FALSE;
    }
  else if (res > 0)
    {
      /* The file is an ascii file but some errors occured. */
      return TRUE;
    }
  /* Everything is OK. */
  *error = (GError*)0;
  return TRUE;
}


static int read_ascii_file(VisuData *data, GIOChannel *flux, GError **error)
{
  gchar *info;
  GString *line, *bufLine;
  gsize term;
  double xu, yu, zu;
  guint i, iph;
  int pos;
  char nomloc[TOOL_MAX_LINE_LENGTH];
  VisuElement *type;
  GArray *types;
  GArray *nattyp;
  guint ntype, nbLine, natom;
  GHashTable *elements;
  struct VisuMethAscii *infos;
  double boxGeometry[6], abc[3], ang[3];
  float  xyz[3], uvw[3];
  gchar *infoUTF8, **nodeComments;
  gboolean hasNodeComments;
  GIOStatus status;
  float qpt[4], *dcoord, *forces;
  AsciiKeywordData kw;
  AsciiMetaData md;
  VisuBox *box;
    
  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  line = g_string_new("");

  /* The first line is a commentry. */
  if (g_io_channel_read_line_string(flux, line, &term, error) != G_IO_STATUS_NORMAL)
    {
      g_string_free(line, TRUE);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -1;
    }
  info = g_strdup(line->str);
  /* The second line contains xx, xy and yy */
  if (g_io_channel_read_line_string(flux, line, &term, error) != G_IO_STATUS_NORMAL)
    {
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -2;
    }
  if(sscanf(line->str, "%lf %lf %lf",
	    &(boxGeometry[0]), &(boxGeometry[1]), &(boxGeometry[2])) != 3)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("Cannot read dxx dyx dyy"
			     " on 2nd line of ASCII file.\n"));
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -3;
    }
  /* The third line contains xz, yz and zz */
  if (g_io_channel_read_line_string(flux, line, &term, error) != G_IO_STATUS_NORMAL)
    {
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -4;
    }
  if(sscanf(line->str, "%lf %lf %lf",
	    &boxGeometry[3], &boxGeometry[4], &boxGeometry[5]) != 3)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("Cannot read dzx dzy dzz"
			     " on 3rd line of ASCII file.\n"));
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -5;
    }
   
  /* Ok, from this point we assert that the file is in ascii format.
     All following errors are treated as syntax errors and the corresponding
     flag is return. */
  DBG_fprintf(stderr, "Atomic ASCII: read box is %f x %f x %f\n"
	      "                          %f x %f x %f\n",
	      boxGeometry[0], boxGeometry[1], boxGeometry[2],
	      boxGeometry[3], boxGeometry[4], boxGeometry[5]);

  /* Set the commentary. */
  g_strstrip(info);
  if (info[0] == '#')
    infoUTF8 = g_locale_to_utf8(info + 1, -1, NULL, NULL, NULL);
  else
    infoUTF8 = g_locale_to_utf8(info, -1, NULL, NULL, NULL);
  if (infoUTF8)
    {
      visu_data_setFileCommentary(data, infoUTF8, 0);
      g_free(infoUTF8);
    }
  else
    g_warning("Can't convert '%s' to UTF8.\n", info);

  /* 1st pass to count ntype */
  kw.reduced     = FALSE;
  kw.angdeg      = FALSE;
  kw.unit        = TOOL_UNITS_UNDEFINED;
  kw.bc          = VISU_BOX_PERIODIC;
  md.nqpt        = 0;
  md.totalEnergy = 999.;
  md.forces      = FALSE;
  dcoord  = (float*)0;
  forces  = (float*)0;
  nbLine  = 4;
  ntype   = 0;
  elements = g_hash_table_new_full(g_direct_hash, g_direct_equal,
				   NULL, visu_meth_asciiFree);
  g_return_val_if_fail(elements, -1);

  for(status = g_io_channel_read_line_string(flux, line, &term, error);
      status == G_IO_STATUS_NORMAL;
      status = g_io_channel_read_line_string(flux, line, &term, error))
    {
      nbLine += 1;
      if(readFile_is_comment(line->str, &kw, &md)) continue;
      if(sscanf(line->str, "%lf %lf %lf %s", 
		&xu, &yu, &zu, nomloc) != 4)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Cannot read x, y, z,"
				 " name in ASCII file at line %d.\n\n"
				 "Quoting '%s'.\n"), nbLine, line->str);
	  g_string_free(line, TRUE);
	  return 1;
	}
      nomloc[8] = '\0';
      /* adding nomloc to the hashtable */
      type = visu_element_retrieveFromName(nomloc, (gboolean*)0);
      infos = (struct VisuMethAscii*)g_hash_table_lookup(elements, (gconstpointer)type);
      if (!infos)
	{
	  infos = g_malloc(sizeof(struct VisuMethAscii));
	  infos->ele = type;
	  infos->pos = ntype;
	  infos->nbNodes = 1;
	  g_hash_table_insert(elements, (gpointer)type, (gpointer)infos);
	  ntype++;
	}
      else
	infos->nbNodes += 1;
    }
  if (status != G_IO_STATUS_EOF)
    {
      g_string_free(line, TRUE);
      return 1;
    }

  if (ntype <= 0)
    {
      g_string_free(line, TRUE);
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("The file contains no atom coordinates.\n"));
      return 1;
    }
  DBG_fprintf(stderr, "Atomic ASCII: transfer to VisuData.\n");
  /* Allocate the space for the nodes. */
  types  = g_array_sized_new(FALSE, FALSE, sizeof(VisuElement*), ntype);
  nattyp = g_array_sized_new(FALSE, FALSE, sizeof(guint), ntype);
  g_array_set_size(types, ntype);
  g_array_set_size(nattyp, ntype);
  g_hash_table_foreach(elements, (GHFunc)visu_meth_asciiValToType, (gpointer)types);
  g_hash_table_foreach(elements, (GHFunc)visu_meth_asciiValToNb, (gpointer)nattyp);
  g_hash_table_destroy(elements);
  natom = 0;
  for (i = 0; i < ntype; i++) natom += g_array_index(nattyp, guint, i);

  visu_node_array_allocate(VISU_NODE_ARRAY(data), types, nattyp);
  g_array_free(nattyp, TRUE);
  g_array_free(types, TRUE);
         
  /* We build boxGeometry according to angdeg flag. */
  DBG_fprintf(stderr, "Atomic ASCII: build the box if any.\n");
  if (kw.angdeg)
    {
      abc[0] = boxGeometry[0];
      abc[1] = boxGeometry[1];
      abc[2] = boxGeometry[2];
      ang[0] = boxGeometry[3];
      ang[1] = boxGeometry[4];
      ang[2] = boxGeometry[5];

      if (sin(ang[2]) == 0.f)
	{
	  *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			       _("Wrong (ab) angle, should be"
				 " different from 0[pi].\n\n"
				 " Quoting '%g'.\n"), ang[2]);
	  g_string_free(line, TRUE);
	  return 1;
	}

      boxGeometry[0] = abc[0];
      boxGeometry[1] = abc[1] * cos(G_PI * ang[2] / 180.);
      boxGeometry[2] = abc[1] * sin(G_PI * ang[2] / 180.);
      boxGeometry[3] = abc[2] * cos(G_PI * ang[1] / 180.);
      boxGeometry[4] = abc[2] * (cos(G_PI * ang[0] / 180.) -
				 cos(G_PI * ang[1] / 180.) *
				 cos(G_PI * ang[2] / 180.)) /
	sin(G_PI * ang[2] / 180.);
      boxGeometry[5] = sqrt(abc[2] * abc[2] - boxGeometry[3] * boxGeometry[3] -
			    boxGeometry[4] * boxGeometry[4]);
      boxGeometry[5] *= (ang[1] < 0.)?-1.:+1.;
    }
  /* Always set the given box, in case coordinates are reduced. */
  box = visu_box_new(boxGeometry, kw.bc);
  visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box), FALSE);
  g_object_unref(box);
  /* Test phonons. */
  if (md.nqpt > 0)
    {
      DBG_fprintf(stderr, "Atomic ASCII: initialise phonon storage for %d data.\n",
		  natom * 6);
      visu_vibration_init(data, md.nqpt, natom);
      dcoord = g_malloc(sizeof(float) * (natom * 6));
    }
  if (md.forces)
    {
      DBG_fprintf(stderr, "Atomic ASCII: initialise forces storage for %d data.\n",
		  natom * 6);
      forces = g_malloc(sizeof(float) * (natom * 6));
    }

  DBG_fprintf(stderr, "Atomic ASCII: reread file for node positions.\n");
  /* Allocate the storage of the node comments. */
  nodeComments = g_malloc(sizeof(gchar*) * natom);
  hasNodeComments = FALSE;

  g_io_channel_seek_position(flux, 0, G_SEEK_SET, error);
  /* reread the file to store the coordinates */
  g_io_channel_read_line_string(flux, line, &term, error);
  g_io_channel_read_line_string(flux, line, &term, error);
  g_io_channel_read_line_string(flux, line, &term, error);
  i = 0;
  iph = 0;
  bufLine = g_string_new((gchar*)0);
  for(status = g_io_channel_read_line_string(flux, line, &term, error);
      status == G_IO_STATUS_NORMAL;
      status = g_io_channel_read_line_string(flux, line, &term, error))
    {
      if(readFile_is_comment(line->str, (AsciiKeywordData*)0, (AsciiMetaData*)0))
	{
	  while (continuousLine(line) && status == G_IO_STATUS_NORMAL)
	    {
	      status = g_io_channel_read_line_string(flux, bufLine, &term, error);
	      g_string_append(line, bufLine->str);
	    }
	  
	  if (md.nqpt > 0 && readQpt(line->str, qpt, dcoord, natom))
	    {
	      visu_vibration_setCharacteristic(data, iph, qpt, qpt[3], 1.f);
	      visu_vibration_setDisplacements(data, iph, dcoord, TRUE);
	      iph += 1;
	    }

          if (md.forces && readForces(line->str, forces, natom))
            visu_rendering_atomic_setForces(data, forces);
	  
	  continue;
	}
     
      pos = 0;
      sscanf(line->str, "%lf %lf %lf %s %n", &xu, &yu, &zu, nomloc, &pos);
      nomloc[8] = '\0';
      type = visu_element_lookup(nomloc);
      if (!type)
	{
	  g_warning("The input file must"
		    " have been modified when loading since"
		    " at the second reading the element '%s'"
		    " seems to be new.\n", nomloc);
	  g_string_free(line, TRUE);
	  g_string_free(bufLine, TRUE);
	  return 1;
	}
      xyz[0] = uvw[0] = (float)xu;
      xyz[1] = uvw[1] = (float)yu;
      xyz[2] = uvw[2] = (float)zu;
      if (kw.reduced)
	visu_box_convertBoxCoordinatestoXYZ(box, xyz, uvw);
      visu_data_addNodeFromElement(data, type, xyz, FALSE, FALSE);

      /* Store a possible comment. */
      if (line->str[pos] != '\0')
	{
	  nodeComments[i] = (line->str[pos] == '#')?g_strdup(line->str + pos + 1):
	    g_strdup(line->str + pos);
	  g_strstrip(nodeComments[i]);
	  if (nodeComments[i][0] == '\0')
	    {
	      g_free(nodeComments[i]);
	      nodeComments[i] = (gchar*)0;
	    }
	  else
	    hasNodeComments = TRUE;
	}
      else
	nodeComments[i] = (gchar*)0;
      i++;
    }
  g_string_free(line, TRUE);
  g_string_free(bufLine, TRUE);

  /* In free boundary conditions, find the bounding box. */
  if (kw.bc != VISU_BOX_PERIODIC)
    visu_data_setTightBox(data);

  /* We apply the comments, if any. */
  visu_extra_node_addLabel(data);
  if (hasNodeComments)
    {
      for (i = 0; i < natom; i++)
	if (nodeComments[i])
	  {
	    visu_extra_node_setLabel(data, i, nodeComments[i]);
	    g_free(nodeComments[i]);
	  }
    }
  g_free(nodeComments);

  /* Add some other meta data. */
  if (md.totalEnergy != 999.)
    g_object_set(G_OBJECT(data), "totalEnergy", md.totalEnergy, NULL);
  
  /* We finish with the box description. */
  DBG_fprintf(stderr, "Atomic ASCII: apply the box geometry and set the unit.\n");
  visu_box_setMargin(box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, box), TRUE);
  visu_box_setUnit(box, kw.unit);

  if (md.nqpt > 0)
    g_free(dcoord);
  if (md.forces)
    g_free(forces);

  DBG_fprintf(stderr, "Atomic ASCII: parse done.\n");

  return 0;
}

