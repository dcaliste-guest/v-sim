/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <glib.h>

#include <GL/gl.h>
#include <GL/glu.h> 
#include <string.h>

#include "renderingSpin.h"
#include "renderingAtomic.h"
#include <visu_basic.h>
#include <visu_commandLine.h>
#include <visu_configFile.h>
#include <math.h>
#include <assert.h>
#include <coreTools/toolFileFormat.h>
#include <coreTools/toolMatrix.h>
#include <coreTools/toolColor.h>
#include <coreTools/toolConfigFile.h>
#include <coreTools/toolFortran.h>
#include <openGLFunctions/objectList.h>
#include <opengl.h>
#include <extraFunctions/dataNode.h>

/**
 * SECTION:renderingSpin
 * @short_description: A module able to represent atoms by their
 * position and spin.
 *
 * <para>This method draws arrows to represent atoms. Each arrow has a
 * given orientation which is set acording to parameters. This is one
 * way to represent the spin of an atom. These arrows direction are
 * determined by the spin of each atom. It is designed to read two
 * separate files : one containing the position of the atoms, the
 * other containing the spin of each atom. Of course these two files
 * need to have the exact same number of atoms and also need to sort
 * atoms in the same order.</para>
 */

#define FLAG_RESOURCES_SPIN "spin_resources"
#define DESC_RESOURCES_SPIN "Global or element resource for rendering spin module"

/* Parameters for each VisuElement. */
struct spinResources_struct
{
  /* One of this structure is associated to each element. */

  /* Params for the arrow shapes. */
  /* The radius and the length of the hat. */
  float length, height;
  /* The radius and the length of the tail. */
  float u_length, u_height;
  /* The coloring pattern. */
  gboolean use_element_color, use_element_color_hat;

  /* Params for the elipsoid shapes. */
  /* The long and the short axis. */
  float aAxis, bAxis;
  /* The coloring pattern. */
  gboolean elipsoidColor;

  /* The shape used. */
  VisuRenderingSpinShapeId shape;

  /* An id of the list associated with the form. */
  int openGLIdentifier;
  /* The associated id for the atomic shape (when used). */
  int openGLIdentifierAtomic;
};
static GType spinElementResourcesTypes[VISU_RENDERING_SPIN_N_RESOURCES] =
  {G_TYPE_FLOAT, G_TYPE_FLOAT, G_TYPE_FLOAT, G_TYPE_FLOAT,
   G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_FLOAT, G_TYPE_FLOAT,
   G_TYPE_BOOLEAN, G_TYPE_UINT};
/* Default values. */
#define SPIN_ELEMENT_HAT_RADIUS_DEFAULT  0.8
#define SPIN_ELEMENT_HAT_LENGTH_DEFAULT  2.0
#define SPIN_ELEMENT_TAIL_RADIUS_DEFAULT 0.33
#define SPIN_ELEMENT_TAIL_LENGTH_DEFAULT 0.8
#define SPIN_ELEMENT_HAT_COLOR_DEFAULT   FALSE
#define SPIN_ELEMENT_TAIL_COLOR_DEFAULT  FALSE
#define SPIN_ELEMENT_AAXIS_DEFAULT       1.5
#define SPIN_ELEMENT_BAXIS_DEFAULT       0.6
#define SPIN_ELEMENT_ELIP_COLOR_DEFAULT  FALSE
#define SPIN_ELEMENT_SHAPE_DEFAULT       VISU_RENDERING_SPIN_ARROW_SMOOTH

enum
  {
    spin_prop0,
    spin_globalConeTheta,
    spin_globalConePhi,
    spin_globalColorWheel,
    spin_globalHidingMode,
    spin_globalAtomic,
    spin_globalModulus
  };
struct _VisuRenderingSpinPrivate
{
  float coneOrientation[2];
  float colorWheel;
  VisuRenderingSpinDrawingPolicy spinPolicy;
  gboolean spinAndAtomicRendering;
  VisuRenderingSpinModulusPolicy spinModulusUsage;

  gboolean dispose_has_run;
};

/* Internal variables. */
static GValue spinValue = {0, {{0}, {0}}};
static VisuDataNode *dataNode;

/* Names as they appear in the gtk combo box when selecting an element shape. */
/* tab[i] element matches the above enum i-th element. */
const char* shapeNameSpin[VISU_RENDERING_SPIN_N_SHAPES + 1] = {"Rounded", 
						 "Edged", 
						 "Elipsoid",
						 "Torus",
						 (const char*)0};
const char** shapeNameSpin_UTF8;

const char* policyNameSpin[VISU_RENDERING_SPIN_N_MODES + 1] = {"always", 
						 "never", 
						 "atomic",
						 (const char*)0};
const char** policyNameSpin_UTF8;

enum {
  COLOR_CHANGED,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL];

/* Local methods. */
static gboolean readSpinResources(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				  int position, VisuData *dataObj, VisuGlView *view,
                                  GError **error);
static void exportResourcesRenderingSpin(GString *data,
                                         VisuData *dataObj, VisuGlView *view);
static void onRenderingChanged(GObject *visu, VisuRendering *method,
			       gpointer data);
static void onRenderingUpdated(VisuRendering *method, gpointer data);
static struct spinResources_struct* getSpinResources(VisuElement *ele);
static void onSpinParametersChanged(VisuData *dataObj, VisuNode *node, gpointer data);
static void freeSpin(gpointer obj, gpointer data);
static gpointer newOrCopySpin(gconstpointer obj, gpointer data);
static float getSize(VisuElement *ele);
static int createShape(VisuElement* ele, VisuGlView *view);
static void positionShape(VisuData *visuData, VisuNode *node,
                          VisuElement* ele, int eleGlId);
static int getShape(VisuElement *element);

/* Local loading methods. */
static const char* visu_rendering_spin_shape_number_to_name(VisuRenderingSpinShapeId n);
static VisuRenderingSpinShapeId visu_rendering_spin_shape_name_to_number(const char *name);
static void spinAsciiInit(VisuRendering *method);
static void spinBinaryInit(VisuRendering *method);
static gboolean read_spin_file(VisuData *data, const char* fileName,
			       ToolFileFormat *format, int nSet,
                               GCancellable *cancel, GError **error);
static gboolean read_binary_file(VisuData *data, const char* fileName,
				 ToolFileFormat *format, int nSet,
                                 GCancellable *cancel, GError **error);

/* Local callbacks. */
static void visu_rendering_spin_dispose(GObject* obj);
static void visu_rendering_spin_finalize(GObject* obj);
static void visu_rendering_spin_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec);
static void visu_rendering_spin_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec);

/**
 * VisuRenderingSpinClass:
 *
 * An opaque structure.
 */
struct _VisuRenderingSpinClass
{
  VisuRenderingClass parent_class;
};

static VisuRenderingSpin *spinMethod = (VisuRenderingSpin*)0;

G_DEFINE_TYPE(VisuRenderingSpin, visu_rendering_spin, VISU_TYPE_RENDERING)

static void visu_rendering_spin_class_init(VisuRenderingSpinClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  DBG_fprintf(stderr, "Visu RenderingSpin: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_rendering_spin_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_rendering_spin_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_rendering_spin_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_rendering_spin_get_property;
  VISU_RENDERING_CLASS(klass)->createElement  = createShape;
  VISU_RENDERING_CLASS(klass)->getElementGlId = getShape;
  VISU_RENDERING_CLASS(klass)->createNode     = positionShape;
  VISU_RENDERING_CLASS(klass)->getNodeExtend  = getSize;

  /**
   * VisuRenderingSpin::colorisationChange:
   * @render: the object emitting the signal.
   *
   * This signal is emitted whenever a change occur in the
   * colourisation scheme of the spins.
   *
   * Since: 3.6
   */
  signals[COLOR_CHANGED] = 
    g_signal_newv ("colorisationChange",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL , NULL, NULL,
		   g_cclosure_marshal_VOID__VOID,
		   G_TYPE_NONE /* return_type */, 0, NULL);

  /**
   * VisuRenderingSpin::cone-theta:
   *
   * The theta angle to orientate the colourisation cone.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalConeTheta,
     g_param_spec_float("cone-theta", _("Theta angle"),
                        _("The theta angle to orientate the colourisation cone."), 0, 180, 0,
                        G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRenderingSpin::cone-phi:
   *
   * The phi angle to orientate the colourisation cone.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalConePhi,
     g_param_spec_float("cone-phi", _("Phi angle"),
                        _("The phi angle to orientate the colourisation cone."), 0, 360, 0,
                        G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRenderingSpin::cone-omega:
   *
   * The omega angle to orientate the colourisation cone.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalColorWheel,
     g_param_spec_float("cone-omega", _("Omega angle"),
                        _("The omega angle to orientate the colourisation cone."), 0, 360, 0,
                        G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRenderingSpin::hiding-mode:
   *
   * The hiding policy for spin with a null modulus.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalHidingMode,
     g_param_spec_uint("hiding-mode", _("Hiding policy for null modulus"),
                       _("The hiding policy for spin with a null modulus."),
                       0, VISU_RENDERING_SPIN_N_MODES, VISU_RENDERING_SPIN_ALWAYS,
                       G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRenderingSpin::modulus-scaling:
   *
   * The scaling policy based on modulus value.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalModulus,
     g_param_spec_uint("modulus-scaling", _("Scaling of spin depending on modulus value"),
                       _("The scaling policy based on modulus value."),
                       0, VISU_RENDERING_SPIN_N_MODULUS_MODES, VISU_RENDERING_SPIN_CONSTANT,
                       G_PARAM_WRITABLE | G_PARAM_READABLE));
  /**
   * VisuRenderingSpin::use-atomic:
   *
   * If atomic rendering is used in addition to spin rendering.
   *
   * Since: 3.6
   */
  g_object_class_install_property
    (G_OBJECT_CLASS(klass), spin_globalAtomic,
     g_param_spec_boolean("use-atomic", _("Use atomic rendering"),
                          _("If atomic rendering is used in addition to spin rendering."),
                          FALSE, G_PARAM_WRITABLE | G_PARAM_READABLE));

  /* Dealing with config files. */
  resourceEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
					  FLAG_RESOURCES_SPIN,
					  DESC_RESOURCES_SPIN,
					  1, readSpinResources);
  visu_config_file_entry_setVersion(resourceEntry, 3.1f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResourcesRenderingSpin);
  

  shapeNameSpin_UTF8 = g_malloc(sizeof(gchar*) * VISU_RENDERING_SPIN_N_SHAPES);
  shapeNameSpin_UTF8[0] = _("Rounded arrow");
  shapeNameSpin_UTF8[1] = _("Edged arrow");
  shapeNameSpin_UTF8[2] = _("Elipsoid");
  shapeNameSpin_UTF8[3] = _("Torus");

  /* Register a new NodeData. */
  dataNode = VISU_DATA_NODE(visu_data_node_new(VISU_RENDERING_SPIN_VALUES_ID, G_TYPE_FLOAT));
  visu_data_node_setLabel(dataNode, _("Spin (\316\270, \317\206, mod.)"));
  visu_data_node_setCallback(dataNode, onSpinParametersChanged, (gpointer)0);

  /* We initialise the container GValue for the spin property. */
  g_value_init(&spinValue, G_TYPE_POINTER);

  g_type_class_add_private(klass, sizeof(VisuRenderingSpinPrivate));
}
static void visu_rendering_spin_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu RenderingSpin: dispose object %p.\n", (gpointer)obj);

  if (VISU_RENDERING_SPIN(obj)->priv->dispose_has_run)
    return;

  VISU_RENDERING_SPIN(obj)->priv->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_rendering_spin_parent_class)->dispose(obj);
}
static void visu_rendering_spin_finalize(GObject* obj)
{
  DBG_fprintf(stderr, "Visu RenderingSpin: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_rendering_spin_parent_class)->finalize(obj);
}
static void visu_rendering_spin_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec)
{
  VisuRenderingSpinPrivate *self = VISU_RENDERING_SPIN(obj)->priv;

  DBG_fprintf(stderr, "Visu RenderingSpin: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case spin_globalConeTheta:
      g_value_set_float(value, self->coneOrientation[0]);
      DBG_fprintf(stderr, "%f.\n", self->coneOrientation[0]); break;
    case spin_globalConePhi:
      g_value_set_float(value, self->coneOrientation[1]);
      DBG_fprintf(stderr, "%f.\n", self->coneOrientation[1]); break;
    case spin_globalColorWheel:
      g_value_set_float(value, self->colorWheel);
      DBG_fprintf(stderr, "%f.\n", self->colorWheel); break;
    case spin_globalHidingMode:
      g_value_set_uint(value, self->spinPolicy);
      DBG_fprintf(stderr, "%d.\n", self->spinPolicy); break;
    case spin_globalAtomic:
      g_value_set_boolean(value, self->spinAndAtomicRendering);
      DBG_fprintf(stderr, "%d.\n", self->spinAndAtomicRendering); break;
    case spin_globalModulus:
      g_value_set_uint(value, self->spinModulusUsage);
      DBG_fprintf(stderr, "%d.\n", self->spinModulusUsage); break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_rendering_spin_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec)
{
  VisuRenderingSpinPrivate *self = VISU_RENDERING_SPIN(obj)->priv;

  DBG_fprintf(stderr, "Visu RenderingSpin: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case spin_globalConeTheta:
      self->coneOrientation[0] = g_value_get_float(value);
      g_signal_emit(G_OBJECT(obj), signals[COLOR_CHANGED], 0, NULL);
      DBG_fprintf(stderr, "%f.\n", self->coneOrientation[0]); break;
    case spin_globalConePhi:
      self->coneOrientation[1] = g_value_get_float(value);
      g_signal_emit(G_OBJECT(obj), signals[COLOR_CHANGED], 0, NULL);
      DBG_fprintf(stderr, "%f.\n", self->coneOrientation[1]); break;
    case spin_globalColorWheel:
      self->colorWheel = g_value_get_float(value);
      g_signal_emit(G_OBJECT(obj), signals[COLOR_CHANGED], 0, NULL);
      DBG_fprintf(stderr, "%f.\n", self->colorWheel); break;
    case spin_globalHidingMode:
      self->spinPolicy = g_value_get_uint(value);
      DBG_fprintf(stderr, "%d.\n", self->spinPolicy); break;
    case spin_globalAtomic:
      self->spinAndAtomicRendering = g_value_get_boolean(value);
      DBG_fprintf(stderr, "%d.\n", self->spinAndAtomicRendering); break;
    case spin_globalModulus:
      self->spinModulusUsage = g_value_get_uint(value);
      DBG_fprintf(stderr, "%d.\n", self->spinModulusUsage); break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static void visu_rendering_spin_init(VisuRenderingSpin *obj)
{
  DBG_fprintf(stderr, "Visu renderingSpin: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_RENDERING_SPIN,
                                          VisuRenderingSpinPrivate);
  obj->priv->spinPolicy             = commandLineGet_spinHidingMode();
  obj->priv->spinAndAtomicRendering = commandLineGet_spinAndAtomic();
  obj->priv->spinModulusUsage       = VISU_RENDERING_SPIN_CONSTANT;
  obj->priv->coneOrientation[0]     = 0.;
  obj->priv->coneOrientation[1]     = 0.;
  obj->priv->colorWheel             = 0.;

  obj->priv->dispose_has_run = FALSE;

  g_return_if_fail((spinMethod == (VisuRenderingSpin*)0));
  spinMethod = obj;
}

/**
 * visu_rendering_spin_new:
 *
 * Create the structure and initialise its values.
 *
 * Since: 3.6
 *
 * Returns: a newly allocate #VisuRenderingSpin object.
 */
VisuRenderingSpin* visu_rendering_spin_new()
{
  char *descr = _("It draws arrows at given positions to represent an atom "
                  "and its spin.");
  gchar *iconPath;
  VisuRenderingSpin* spin;
  VisuRendering *atomic;

  DBG_fprintf(stderr,"Initialising the spin rendering method...\n");

  /* We check that the atomic rendering has been already defined, to use its own
     loader and file formats. */
  atomic = visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME);
  g_return_val_if_fail(atomic, (VisuRenderingSpin*)0);

  spin = VISU_RENDERING_SPIN(g_object_new(VISU_TYPE_RENDERING_SPIN,
                                          "name", VISU_RENDERING_SPIN_NAME,
                                          "label", _(VISU_RENDERING_SPIN_NAME),
                                          "description", descr, 
                                          "nFiles", 2, NULL));
  visu_rendering_setFileTypeLabel(VISU_RENDERING(spin),
                                  FILE_KIND_POSITION, _("Position files"));
  visu_rendering_setFileTypeLabel(VISU_RENDERING(spin),
                                  FILE_KIND_SPIN,     _("Spin files"));
  iconPath = g_build_filename(V_SIM_PIXMAPS_DIR, "stock-spin.png", NULL);
  visu_rendering_setIcon(VISU_RENDERING(spin), iconPath);
  g_free(iconPath);

  spinAsciiInit(VISU_RENDERING(spin));
  spinBinaryInit(VISU_RENDERING(spin));
  visu_rendering_setFileFormat(VISU_RENDERING(spin), FILE_KIND_POSITION, atomic);

  /* Connect to the signal method changed to set or unset some variables
     when spin is current or not. */
  g_signal_connect(VISU_OBJECT_INSTANCE, "renderingChanged",
		   G_CALLBACK(onRenderingChanged), (gpointer)spin);
  g_signal_connect(G_OBJECT(atomic), "fileTypeChanged",
		   G_CALLBACK(onRenderingUpdated), (gpointer)spin);

  return spin;
}





static VisuRenderingSpinShapeId visu_rendering_spin_shape_name_to_number(const char *name)
{
  int i=0;

  g_return_val_if_fail(name, -1);

  for(i=0; i < VISU_RENDERING_SPIN_N_SHAPES; i++)
    if(strcmp(name, shapeNameSpin[i]) == 0)
      return i;

  return -1;
}

static const char* visu_rendering_spin_shape_number_to_name(VisuRenderingSpinShapeId n)
{
  g_return_val_if_fail(n < VISU_RENDERING_SPIN_N_SHAPES, (const gchar*)0);
  
  return shapeNameSpin[n];
}
/**
 * visu_rendering_spin_getShapeNameI18n:
 * @n: an id for spin shape.
 *
 * This routine returnes the translated name in UTF-8 corresponding to
 * the given shape id.
 *
 * Returns: (type utf8): a string owned by V_Sim.
 */
const char* visu_rendering_spin_getShapeNameI18n(VisuRenderingSpinShapeId n)
{
  g_return_val_if_fail(n < VISU_RENDERING_SPIN_N_SHAPES, (const gchar*)0);
  
  return shapeNameSpin_UTF8[n];
}


/**
 * visu_rendering_spin_getHidingPolicyFromName:
 * @name: (type filename): a string.
 *
 * In the config file, the hiding policy resource is stored with its name (untranslated).
 * This method is used to retrieve the id from the name.
 *
 * Returns: the maximum value if the name is invalid.
 */
VisuRenderingSpinDrawingPolicy visu_rendering_spin_getHidingPolicyFromName(const char *name)
{
  int i=0;

  if(name == NULL) 
    return -1;

  for(i=0; i<VISU_RENDERING_SPIN_N_MODES; i++)
    if(strcmp(name, policyNameSpin[i]) == 0)
      return i;

  return VISU_RENDERING_SPIN_N_MODES;
}

/**
 * visu_rendering_spin_getHidingPolicyName:
 * @n: an id for hiding policy.
 *
 * Transform ids to untranslated names.
 *
 * Returns: (type filename): the name associated to the id.
 */
const char* visu_rendering_spin_getHidingPolicyName(VisuRenderingSpinDrawingPolicy n)
{
  if(n>=VISU_RENDERING_SPIN_N_MODES)
    return NULL;
  
  return policyNameSpin[n];
}
/**
 * visu_rendering_spin_getHidingPolicyNameI18n:
 * @n: an id for hiding policy.
 *
 * Transform ids to translated names.
 *
 * Returns: (type utf8): the name associated to the id in UTF-8.
 */
const char* visu_rendering_spin_getHidingPolicyNameI18n(VisuRenderingSpinDrawingPolicy n)
{
  if(n>=VISU_RENDERING_SPIN_N_MODES)
    return NULL;
  
  return policyNameSpin_UTF8[n];
}

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
static void spinAsciiInit(VisuRendering *method)
{
  const gchar *typeSpin[] = {"*.spin", "*.sp", NULL};
  
  visu_rendering_addFileFormat(method, FILE_KIND_SPIN,
                               tool_file_format_new(_("Ascii spin files"),
                                                    typeSpin),
                               100, read_spin_file);
}

static void spinBinaryInit(VisuRendering *method)
{
  const gchar *typeSpin[] = {"*.bspin", "*.bsp", NULL};

  visu_rendering_addFileFormat(method, FILE_KIND_SPIN,
                               tool_file_format_new(_("Binary spin files"),
                                                    typeSpin),
                               10, read_binary_file);
}

static void onRenderingChanged(GObject *visu _U_, VisuRendering *method,
			       gpointer data)
{
  if (method == (VisuRendering*)data)
    visu_element_setUpdateNodesOnMaterialChange();
  else
    visu_element_unsetUpdateNodesOnMaterialChange();
}
static void onRenderingUpdated(VisuRendering *atomic, gpointer data)
{
  DBG_fprintf(stderr, "Rendering Spin: update the atomic methods.\n");

  visu_rendering_setFileFormat(VISU_RENDERING(data), FILE_KIND_POSITION, atomic);
}


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
/* The following are the methods responsible of dealing with the reading of files. */
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/

static void freeSpin(gpointer obj, gpointer data _U_)
{
#if GLIB_MINOR_VERSION > 9
  g_slice_free1(sizeof(float) * 3, obj);
#else
  g_free(obj);
#endif
}
static gpointer newOrCopySpin(gconstpointer obj, gpointer data _U_)
{
  float *spinData;

#if GLIB_MINOR_VERSION > 9
  spinData = g_slice_alloc(sizeof(float) * 3);
#else
  spinData = g_malloc(sizeof(float) * 3);
#endif
  if (obj)
    memcpy(spinData, obj, sizeof(float) * 3);
  else
    memset(spinData, 0, sizeof(float) * 3);
    
  return (gpointer)spinData;
}

static void initMaxModulus(VisuElement *ele _U_, GValue *val)
{
  DBG_fprintf(stderr, " | init max modulus of val %p.\n", (gpointer)val);
  g_value_init(val, G_TYPE_FLOAT);
  g_value_set_float(val, -G_MAXFLOAT);
}

/* This is the method associated to the reading of supported spin files. */
static gboolean read_spin_file(VisuData *data, const char* fileName,
			       ToolFileFormat *format _U_, int nSet _U_,
                               GCancellable *cancel _U_, GError **error)
{
  char line[TOOL_MAX_LINE_LENGTH] = "\0";
  float vals[3];
  float *svgSpinValues;
  GValue *val;
  GValueArray *svgMaxSpinModulus;
  int itrash, iLine;
  VisuNodeArrayIter iter;
  FILE *readFrom;
  gboolean readContinue;
  VisuNodeProperty *spin;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  readFrom = fopen(fileName, "r");
  if (!readFrom)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("impossible to open this spin file.\n"));
      return FALSE;
    }
  iLine = 1;

  /* The first line is a commentry. */
  if(!fgets(line, TOOL_MAX_LINE_LENGTH, readFrom) || feof(readFrom))
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("spin file should have one line at least.\n"));
      fclose(readFrom);
      return FALSE;
    }
  iLine += 1;
   
  /* Create a storage for max values of spin modulus for each element. */
  svgMaxSpinModulus =
    visu_node_array_setElementProperty(VISU_NODE_ARRAY(data), VISU_RENDERING_SPIN_MAX_MODULUS_ID,
                                       initMaxModulus);
  spin = visu_node_array_property_newPointer(VISU_NODE_ARRAY(data), VISU_RENDERING_SPIN_VALUES_ID,
                                       freeSpin, newOrCopySpin, (gpointer)0);
  
  readContinue = TRUE;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for(visu_node_array_iterStartNumber(VISU_NODE_ARRAY(data), &iter); iter.node;
      visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(data), &iter))
    {
      if (readContinue)
	{
	  if(!fgets(line, TOOL_MAX_LINE_LENGTH, readFrom) || feof(readFrom))
	    readContinue = FALSE;
	  else
	    {
	      if(sscanf(line, "%d %f %f %f", &itrash, vals + TOOL_MATRIX_SPHERICAL_MODULUS,
			vals + TOOL_MATRIX_SPHERICAL_THETA, vals + TOOL_MATRIX_SPHERICAL_PHI) != 4)
		{
		  g_warning("line number #%d is invalid."
			    " Setting node parameters to default ones...", iLine);
		  vals[TOOL_MATRIX_SPHERICAL_THETA]   = 0.f;
		  vals[TOOL_MATRIX_SPHERICAL_PHI]     = 0.f;
		  vals[TOOL_MATRIX_SPHERICAL_MODULUS] = 0.f;
		}
	    }
	  iLine += 1;
	}
      else
	{
	  vals[TOOL_MATRIX_SPHERICAL_THETA]   = 0.f;
	  vals[TOOL_MATRIX_SPHERICAL_PHI]     = 0.f;
	  vals[TOOL_MATRIX_SPHERICAL_MODULUS] = 0.f;
	}
      svgSpinValues = newOrCopySpin(vals, (gpointer)0);
      g_value_set_pointer(&spinValue, svgSpinValues);
      visu_node_property_setValue(spin, iter.node, &spinValue);
      val = g_value_array_get_nth(svgMaxSpinModulus, iter.iElement);
      g_value_set_float(val, MAX(vals[TOOL_MATRIX_SPHERICAL_MODULUS],
                                 g_value_get_float(val)));
    }
  fclose(readFrom);
  visu_data_node_setUsed(dataNode, data, 3);
  return TRUE;
}

/* This is the method associated to the reading of binary spin files. */
static gboolean read_binary_file(VisuData *data, const char* fileName,
				 ToolFileFormat *format _U_, int nSet _U_,
                                 GCancellable *cancel _U_, GError **error)
{
  FILE *readFrom;
  gboolean valid;
  ToolFortranEndianId endian;
  guint nspins;
  double *spins;
  int i;
  float *svgSpinValues, vals[3];
  GValueArray *svgMaxSpinModulus;
  VisuNodeArrayIter iter;
  VisuNodeProperty *spin;
  GValue *val;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  readFrom = fopen(fileName, "r");
  if (!readFrom)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FILE,
			   _("impossible to open this spin file.\n"));
      return FALSE;
    }

  /* Try to find the endianness. */
  valid = tool_fortran_testEndianness(4, readFrom, error, &endian);
  if (!valid)
    {
      fclose(readFrom);
      return FALSE;
    }

  /* Try to the number of spins. */
  valid = tool_fortran_readInteger(&nspins, 1, readFrom, error, endian, TRUE, TRUE);
  if (!valid)
    {
      fclose(readFrom);
      return FALSE;
    }

  /* From now on, we consider to a have valid spin file. */
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  if (nspins != iter.nAllStoredNodes)
    {
      *error = g_error_new(VISU_ERROR_RENDERING, RENDERING_ERROR_FORMAT,
			   _("number of spin differs from number of nodes.\n"));
      fclose(readFrom);
      return TRUE;
    }
  
  spins = g_malloc(sizeof(double) * 3 * nspins);
  /* Read module. */
  valid = tool_fortran_readDouble(spins, nspins, readFrom, error, endian, TRUE, TRUE);
  if (!valid)
    {
      g_free(spins);
      fclose(readFrom);
      return TRUE;
    }
  /* Read theta. */
  valid = tool_fortran_readDouble(spins + nspins, nspins, readFrom, error, endian, TRUE, TRUE);
  if (!valid)
    {
      g_free(spins);
      fclose(readFrom);
      return TRUE;
    }
  /* Read phi. */
  valid = tool_fortran_readDouble(spins + 2 * nspins, nspins, readFrom, error, endian, TRUE, TRUE);
  if (!valid)
    {
      g_free(spins);
      fclose(readFrom);
      return TRUE;
    }
  /* Close the file. */
  fclose(readFrom);

  /* Create a storage for max values of spin modulus for each element. */
  svgMaxSpinModulus =
    visu_node_array_setElementProperty(VISU_NODE_ARRAY(data), VISU_RENDERING_SPIN_MAX_MODULUS_ID,
                                       initMaxModulus);
  spin = visu_node_array_property_newPointer(VISU_NODE_ARRAY(data), VISU_RENDERING_SPIN_VALUES_ID,
                                       freeSpin, newOrCopySpin, (gpointer)0);

  for(visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter), i = 0; iter.node;
      visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter), i++)
    {
      vals[TOOL_MATRIX_SPHERICAL_MODULUS] = (float)spins[i];
      vals[TOOL_MATRIX_SPHERICAL_THETA]   = (float)spins[nspins + i];
      vals[TOOL_MATRIX_SPHERICAL_PHI]     = (float)spins[2 * nspins + i];
      svgSpinValues = newOrCopySpin(vals, (gpointer)0);
      g_value_set_pointer(&spinValue, svgSpinValues);
      visu_node_property_setValue(spin, iter.node, &spinValue);
      val = g_value_array_get_nth(svgMaxSpinModulus, iter.iElement);
      g_value_set_float(val, MAX((float)spins[i], g_value_get_float(val)));
    }

  g_free(spins);
  visu_data_node_setUsed(dataNode, data, 3);
  return TRUE;
}

/********************************/
/* Access to element resources. */
/********************************/
static float getSize(VisuElement *ele)
{
  struct spinResources_struct *str;
  
  g_return_val_if_fail(ele, 1.f);

  str = getSpinResources(ele);
  if (str->shape == VISU_RENDERING_SPIN_ARROW_SMOOTH || str->shape == VISU_RENDERING_SPIN_ARROW_SHARP)
    return str->height + str->u_height;
  else
    return MAX(str->aAxis, str->bAxis);
}
static struct spinResources_struct* getSpinResources(VisuElement *ele)
{
  struct spinResources_struct *str;
  
  g_return_val_if_fail(ele, (struct spinResources_struct*)0);

  str = (struct spinResources_struct *)g_object_get_data(G_OBJECT(ele), "spinElementResources");
  if(!str)
    {
      str = g_malloc(sizeof(struct spinResources_struct));

      str->shape                  = SPIN_ELEMENT_SHAPE_DEFAULT;
      str->length                 = SPIN_ELEMENT_HAT_RADIUS_DEFAULT;
      str->u_length               = SPIN_ELEMENT_TAIL_RADIUS_DEFAULT;
      str->height                 = SPIN_ELEMENT_HAT_LENGTH_DEFAULT;
      str->u_height               = SPIN_ELEMENT_TAIL_LENGTH_DEFAULT;
      str->use_element_color      = SPIN_ELEMENT_TAIL_COLOR_DEFAULT;
      str->use_element_color_hat  = SPIN_ELEMENT_HAT_COLOR_DEFAULT;
      str->aAxis                  = SPIN_ELEMENT_AAXIS_DEFAULT;
      str->bAxis                  = SPIN_ELEMENT_BAXIS_DEFAULT;
      str->elipsoidColor          = SPIN_ELEMENT_ELIP_COLOR_DEFAULT;
      str->openGLIdentifier       = visu_gl_objectlist_new(1);
      str->openGLIdentifierAtomic = -1;

      g_object_set_data(G_OBJECT(ele), "spinElementResources", (gpointer)str);
    }
  return str;
}
/**
 * visu_rendering_spin_getResource:
 * @ele: a pointer to a #VisuElement object ;
 * @property: the id of the resource (see
 * #VisuRenderingSpinResources);
 * @val: (out caller-allocates): a location to store the value.
 *
 * This is a generic method to access resources per element. Use
 * visu_rendering_spin_setResourceBoolean() is favored if the type of
 * the value is known (boolean in this exemple).
 *
 * Since: 3.7
 */
void visu_rendering_spin_getResource(VisuElement *ele,
                                     VisuRenderingSpinResources property,
                                     GValue *val)
{
  struct spinResources_struct *str;

  g_return_if_fail(property < VISU_RENDERING_SPIN_N_RESOURCES);
  g_return_if_fail(val);
  
  str = getSpinResources(ele);
  g_return_if_fail(str);

  memset(val, 0, sizeof(GValue));
  g_value_init(val, spinElementResourcesTypes[property]);
  switch (property)
    {
    case VISU_RENDERING_SPIN_HAT_LENGTH:
      g_value_set_float(val, str->length);
      return;
    case spin_VISU_RENDERING_SPIN_TAIL_LENGTH:
      g_value_set_float(val, str->u_length);
      return;
    case VISU_RENDERING_SPIN_HAT_RADIUS:
      g_value_set_float(val, str->height);
      return;
    case VISU_RENDERING_SPIN_TAIL_RADIUS:
      g_value_set_float(val, str->u_height);
      return;
    case VISU_RENDERING_SPIN_HAT_COLOR:
      g_value_set_boolean(val, str->use_element_color_hat);
      return;
    case VISU_RENDERING_SPIN_TAIL_COLOR:
      g_value_set_boolean(val, str->use_element_color);
      return;
    case VISU_RENDERING_SPIN_A_AXIS:
      g_value_set_float(val, str->aAxis);
      return;
    case VISU_RENDERING_SPIN_B_AXIS:
      g_value_set_float(val, str->bAxis);
      return;
    case VISU_RENDERING_SPIN_ELIPSOID_COLOR:
      g_value_set_boolean(val, str->elipsoidColor);
      return;
    case VISU_RENDERING_SPIN_SHAPE:
      g_value_set_uint(val, str->shape);
      return;
    default:
      g_error("Wrong implementation, property value should be handled by the switch.");
    }
  return;
}
/**
 * visu_rendering_spin_setResource:
 * @ele: a #VisuElement object.
 * @property: an id for element property.
 * @val: the value.
 *
 * The spin rendering method has properties stored for each
 * element. Use this method to change the value of @property for @ele.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is indeed changed.
 **/
gboolean visu_rendering_spin_setResource(VisuElement *ele,
                                         VisuRenderingSpinResources property,
                                         GValue *val)
{
  struct spinResources_struct *str;
  gboolean ret;

  g_return_val_if_fail(property < VISU_RENDERING_SPIN_N_RESOURCES, FALSE);
  g_return_val_if_fail(val && (G_VALUE_TYPE(val) ==
                               spinElementResourcesTypes[property]), FALSE);
  
  str = getSpinResources(ele);

  ret = FALSE;
  switch (property)
    {
    case VISU_RENDERING_SPIN_HAT_LENGTH:
      ret = (str->length != g_value_get_float(val));
      if (ret)
        {
          str->length = g_value_get_float(val);
          if (spinMethod)
            g_signal_emit_by_name(G_OBJECT(spinMethod), "elementSizeChanged",
                                  str->length + str->u_length, NULL);
        }
      return ret;
    case spin_VISU_RENDERING_SPIN_TAIL_LENGTH:
      ret = (str->u_length != g_value_get_float(val));
      if (ret)
        {
          str->u_length = g_value_get_float(val);
          if (spinMethod)
            g_signal_emit_by_name(G_OBJECT(spinMethod), "elementSizeChanged",
                                  str->length + str->u_length, NULL);
        }
      return ret;
    case VISU_RENDERING_SPIN_HAT_RADIUS:
      ret = (str->height != g_value_get_float(val));
      if (ret)
        str->height = g_value_get_float(val);
      return ret;
    case VISU_RENDERING_SPIN_TAIL_RADIUS:
      ret = (str->u_height != g_value_get_float(val));
      if (ret)
        str->u_height = g_value_get_float(val);
      return ret;
    case VISU_RENDERING_SPIN_HAT_COLOR:
      ret = (str->use_element_color_hat != g_value_get_boolean(val));
      if (ret)
        str->use_element_color_hat = g_value_get_boolean(val);
      return ret;
    case VISU_RENDERING_SPIN_TAIL_COLOR:
      ret = (str->use_element_color != g_value_get_boolean(val));
      if (ret)
        str->use_element_color = g_value_get_boolean(val);
      return ret;
    case VISU_RENDERING_SPIN_A_AXIS:
      ret = (str->aAxis != g_value_get_float(val));
      if (ret)
        {
          str->aAxis = g_value_get_float(val);
          if (spinMethod)
            g_signal_emit_by_name(G_OBJECT(spinMethod), "elementSizeChanged",
                                  MAX(str->aAxis, str->bAxis), NULL);
        }
      return ret;
    case VISU_RENDERING_SPIN_B_AXIS:
      ret = (str->bAxis != g_value_get_float(val));
      if (ret)
        {
          str->bAxis = g_value_get_float(val);
          if (spinMethod)
            g_signal_emit_by_name(G_OBJECT(spinMethod), "elementSizeChanged",
                                  MAX(str->aAxis, str->bAxis), NULL);
        }
      return ret;
    case VISU_RENDERING_SPIN_ELIPSOID_COLOR:
      ret = (str->elipsoidColor != g_value_get_boolean(val));
      if (ret)
        str->elipsoidColor = g_value_get_boolean(val);
      return ret;
    case VISU_RENDERING_SPIN_SHAPE:
      ret = (str->shape != g_value_get_uint(val));
      if (ret)
        str->shape = g_value_get_uint(val);
      return ret;
    default:
      g_error("Wrong implementation, property value should be handled by the switch.");
    }
  return FALSE;
}
gboolean visu_rendering_spin_setResourceBoolean(VisuElement *ele,
                                                VisuRenderingSpinResources property,
                                                gboolean value)
{
  GValue data;

  memset(&data, 0, sizeof(GValue));
  g_value_init(&data, G_TYPE_BOOLEAN);
  g_value_set_boolean(&data, value);
  return visu_rendering_spin_setResource(ele, property, &data);
}
gboolean visu_rendering_spin_setResourceUint(VisuElement *ele,
                                             VisuRenderingSpinResources property,
                                             guint value)
{
  GValue data;

  memset(&data, 0, sizeof(GValue));
  g_value_init(&data, G_TYPE_UINT);
  g_value_set_uint(&data, value);
  return visu_rendering_spin_setResource(ele, property, &data);
}
gboolean visu_rendering_spin_setResourceFloat(VisuElement *ele,
                                              VisuRenderingSpinResources property,
                                              gfloat value)
{
  GValue data;

  memset(&data, 0, sizeof(GValue));
  g_value_init(&data, G_TYPE_FLOAT);
  g_value_set_float(&data, value);
  return visu_rendering_spin_setResource(ele, property, &data);
}
gboolean visu_rendering_spin_getResourceBoolean(VisuElement *ele,
                                                VisuRenderingSpinResources property)
{
  GValue data;

  visu_rendering_spin_getResource(ele, property, &data);
  return g_value_get_boolean(&data);
}
guint visu_rendering_spin_getResourceUint(VisuElement *ele, VisuRenderingSpinResources property)
{
  GValue data;

  visu_rendering_spin_getResource(ele, property, &data);
  return g_value_get_uint(&data);
}
gfloat visu_rendering_spin_getResourceFloat(VisuElement *ele, VisuRenderingSpinResources property)
{
  GValue data;

  visu_rendering_spin_getResource(ele, property, &data);
  return g_value_get_float(&data);
}


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
/* Ok now we introduce the functions used to load/save resources of our module. */
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/

#define FLAG_ELEMENT_ARROW "spin_element_arrow_params"
static gboolean readElementArrow(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				 int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float size[4];
  gboolean color[2];
  gboolean res;
  VisuElement *ele;
  struct spinResources_struct *str;
  gchar **tokens;
  int id;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit_set(lines[0], " \n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  /* Read the element. */
  res = tool_config_file_readElementFromTokens(tokens, &id, &ele, 1, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the size params : 4 floats. */
  res = tool_config_file_readFloatFromTokens(tokens, &id, size, 4, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the color params : 2 booleans. */
  res = tool_config_file_readBooleanFromTokens(tokens, &id, color, 2, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  /* All is OK, we store the values. */
  str = getSpinResources(ele);
  str->height                = size[0];
  str->u_height              = size[1];
  str->length                = size[2];
  str->u_length              = size[3];
  str->use_element_color     = color[0];
  str->use_element_color_hat = color[1];

  return TRUE;
}
#define FLAG_ELEMENT_ELIPSOID "spin_element_elipsoid_params"
static gboolean readElementElipsoid(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				    int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  float size[2];
  gboolean color;
  gboolean res;
  VisuElement *ele;
  struct spinResources_struct *str;
  gchar **tokens;
  int id;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  tokens = g_strsplit_set(lines[0], " \n", TOOL_MAX_LINE_LENGTH);
  id = 0;
  /* Read the element. */
  res = tool_config_file_readElementFromTokens(tokens, &id, &ele, 1, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the size params : 2 floats. */
  res = tool_config_file_readFloatFromTokens(tokens, &id, size, 2, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  /* Read the color params : 1 boolean. */
  res = tool_config_file_readBooleanFromTokens(tokens, &id, &color, 1, position, error);
  if (!res)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  /* All is OK, we store the values. */
  str = getSpinResources(ele);
  str->aAxis         = size[0];
  str->bAxis         = size[1];
  str->elipsoidColor = color;

  return TRUE;
}
#define FLAG_ELEMENT_SHAPE "spin_element_shape"
static gboolean readElementShape(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				 int position, VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gchar **shapes;
  int shapeId;
  gboolean res;
  VisuElement *ele;
  struct spinResources_struct *str;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read a string.*/
  res = tool_config_file_readStringWithElement(lines[0], position, &shapes, 1, &ele, error);
  if (*error)
    return FALSE;
  if (res)
    {
      /* All is OK, we store the values. */
      shapeId = visu_rendering_spin_shape_name_to_number(shapes[0]);
      if (shapeId == -1)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_READ,
			       _("Parse error at line %d, the shape '%s' is unknown.\n"), 
			       position, shapes[0]);
	  g_strfreev(shapes);
	  return FALSE;
	}
      g_strfreev(shapes);
      str = getSpinResources(ele);
      str->shape = shapeId;
    }
  return res;
}

#define FLAG_SPIN_CONE_ANGLE "spin_global_color_cone"
static gboolean readSpinColorCone(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				  int position, VisuData *dataObj _U_,
                                  VisuGlView *view _U_, GError **error)
{
  float angles[2];
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read one floating point values.*/
  res = tool_config_file_readFloat(lines[0], position, angles, 2, error);
  if (*error)
    return FALSE;
  if (res)
    {
      /* All is OK, we store the values. */
      spinMethod->priv->coneOrientation[0] = angles[0];
      spinMethod->priv->coneOrientation[1] = angles[1];
    }
  return res;
}
#define FLAG_SPIN_WHEEL_ANGLE "spin_global_color_wheel"
static gboolean readSpinColorWheel(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				   int position, VisuData *dataObj _U_,
				   VisuGlView *view _U_, GError **error)
{
  float angle;
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read one floating point values.*/
  res = tool_config_file_readFloat(lines[0], position, &angle, 1, error);
  if (*error)
    return FALSE;
  if (res)
    /* All is OK, we store the values. */
    spinMethod->priv->colorWheel = angle;
  return res;
}
#define FLAG_SPIN_HIDING_MODE "spin_global_hiding_mode"
static gboolean readSpinHidingMode(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				   int position, VisuData *dataObj _U_,
				   VisuGlView *view _U_, GError **error)
{
  gchar **mode;
  VisuRenderingSpinDrawingPolicy modeId;
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read a string.*/
  res = tool_config_file_readString(lines[0], position, &mode, 1, FALSE, error);
  if (*error)
    return FALSE;
  if (res)
    {
      /* All is OK, we store the values. */
      modeId = visu_rendering_spin_getHidingPolicyFromName(g_strstrip(mode[0]));
      if (modeId == VISU_RENDERING_SPIN_N_MODES)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_READ,
			       _("Parse error at line %d," 
				 " the hiding mode '%s' is unknown.\n"), 
			       position, mode[0]);
	  g_strfreev(mode);
	  return FALSE;
	}
      g_strfreev(mode);
      spinMethod->priv->spinPolicy = modeId;
    }
  return res;
}
#define FLAG_SPIN_AND_ATOMIC "spin_global_atomic"
static gboolean readSpinAndAtomic(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				  int position, VisuData *dataObj _U_,
				  VisuGlView *view _U_, GError **error)
{
  gboolean use;
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read one floating point values.*/
  res = tool_config_file_readBoolean(lines[0], position, &use, 1, error);
  if (*error)
    return FALSE;
  if (res)
    /* All is OK, we store the values. */
    spinMethod->priv->spinAndAtomicRendering = use;
  return res;
}
#define FLAG_TOOL_MATRIX_SPHERICAL_MODULUS "spin_global_modulus"
static gboolean readSpinModulus(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines,
				int position, VisuData *dataObj _U_,
				VisuGlView *view _U_, GError **error)
{
  int modeId;
  gboolean res;

  g_return_val_if_fail(error && (*error == (GError*)0), FALSE);
  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read a string.*/
  res = tool_config_file_readInteger(lines[0], position, &modeId, 1, error);
  if (*error)
    return FALSE;
  if (res)
    {
      if (modeId < 0 || modeId >= VISU_RENDERING_SPIN_N_MODULUS_MODES)
	{
	  *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_READ,
			       _("Parse error at line %d," 
				 " the modulus mode '%d' is unknown.\n"), 
			       position, modeId);
	  return FALSE;
	}
      spinMethod->priv->spinModulusUsage = (VisuRenderingSpinModulusPolicy)modeId;
    }
  return res;
}

static gboolean readSpinResources(VisuConfigFileEntry *entry, gchar **lines, int nbLines,
				  int position, VisuData *dataObj,
                                  VisuGlView *view, GError **error)
{
  gchar **tokens;
  int i, token;
  gboolean ok;
  VisuElement* ele;
  char shape[TOOL_MAX_LINE_LENGTH];
  float length, u_length, height, u_height;
  int use_color_element, use_element_color_hat;
  int shape_number;
  struct spinResources_struct *str;
  #define NB_SPIN_READ_METHODS 8
  VisuConfigFileReadFunc readFuncs[NB_SPIN_READ_METHODS] =
    {readElementArrow, readElementElipsoid, readElementShape,
     readSpinColorCone, readSpinColorWheel,
     readSpinHidingMode, readSpinAndAtomic, readSpinModulus};
  gchar *readFlags[NB_SPIN_READ_METHODS] =
    {FLAG_ELEMENT_ARROW, FLAG_ELEMENT_ELIPSOID, FLAG_ELEMENT_SHAPE,
     FLAG_SPIN_CONE_ANGLE, FLAG_SPIN_WHEEL_ANGLE,
     FLAG_SPIN_HIDING_MODE, FLAG_SPIN_AND_ATOMIC, FLAG_TOOL_MATRIX_SPHERICAL_MODULUS};

  g_return_val_if_fail(nbLines == 1, FALSE);

  /* Read the first keyword. */
  tokens = g_strsplit_set(g_strchug(lines[0]), " \n", 2);

  /* Try to find an element/global resource flag. */
  for (i = 0; i < NB_SPIN_READ_METHODS; i++)
    {
      if (!strcmp(g_strstrip(tokens[0]), readFlags[i]))
	{
	  ok = readFuncs[i](entry, tokens + 1, 1, position, dataObj, view, error);
	  g_strfreev(tokens);
	  return ok;
	}
    }
  if (!strcmp(g_strstrip(tokens[0]), "cone_phi_angle") ||
      !strcmp(g_strstrip(tokens[0]), "cone_theta_angle") ||
      !strcmp(g_strstrip(tokens[0]), "color_wheel_angle"))
    {
      g_warning("Deprecated flag at line %d, value ignored.", position);
      return TRUE;
    }

  /* Finaly fallback the old format "ele + values". */
  token = 0;
  ok = tool_config_file_readElementFromTokens(tokens, &token, &ele, 1, nbLines, error);
  if (!ok)
    {
      g_strfreev(tokens);
      return FALSE;
    }
  
  /* Retrieving element's resources */
  if(sscanf(tokens[1], "%s %f %f %f %f %d %d", shape, 
	     &height, &u_height, &length, &u_length, 
	     &use_color_element, &use_element_color_hat) != 7
     || length <= 0. || u_length <= 0. 
     || height <= 0. || u_height <= 0.)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_MISSING,
			   _("Parse error at line %d: a shape with 4 floating points "
			     "and 2 booleans must appear after the %s markup.\n"),
			   position, FLAG_RESOURCES_SPIN);
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);
  if((shape_number = visu_rendering_spin_shape_name_to_number(shape)) == -1)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: the shape '%s' is unknown.\n"),
			   position, shape);
      return FALSE;
    }
  str = getSpinResources(ele);
  str->height                = height;
  str->u_height              = u_height;
  str->length                = length;
  str->u_length              = u_length;
  str->use_element_color     = use_color_element;
  str->use_element_color_hat = use_element_color_hat;
  str->shape                 = shape_number;
  DBG_fprintf(stderr, "resources set successfully for element '%s'\n", ele->name);

  return TRUE;
}

static void exportResourcesRenderingSpin(GString *data,
                                         VisuData* dataObj, VisuGlView *view _U_)
{
  GList *pos, *eleList;
  struct spinResources_struct *str;
  VisuElement *ele;
  VisuNodeArrayIter iter;

  /* If dataObj is given and the rendering method is not spin,
     we return. */
  if (dataObj && (visu_object_getRendering(VISU_OBJECT_INSTANCE) ==
                  visu_rendering_getByName(VISU_RENDERING_SPIN_NAME)))
    return;

  DBG_fprintf(stderr, "Rendering Spin: exporting element resources...\n");

  visu_config_file_exportComment(data, DESC_RESOURCES_SPIN);

  visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_SPIN_CONE_ANGLE,
                               "%f %f", spinMethod->priv->coneOrientation[0],
                               spinMethod->priv->coneOrientation[1]);

  visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_SPIN_WHEEL_ANGLE,
                               "%f", spinMethod->priv->colorWheel);

  visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_SPIN_HIDING_MODE,
                               "%s", policyNameSpin[spinMethod->priv->spinPolicy]);

  visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_SPIN_AND_ATOMIC,
                               "%d", spinMethod->priv->spinAndAtomicRendering);

  visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_TOOL_MATRIX_SPHERICAL_MODULUS,
                               "%d", spinMethod->priv->spinModulusUsage);

  /* We create a list of elements, or get the whole list. */
  if (dataObj)
    {
      eleList = (GList*)0;
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
      for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.element;
           visu_node_array_iterNextElement(VISU_NODE_ARRAY(dataObj), &iter))
        eleList = g_list_prepend(eleList, (gpointer)iter.element);
    }
  else
    eleList = g_list_copy((GList*)visu_element_getAllElements());
  for (pos = eleList; pos; pos = g_list_next(pos))
    {
      ele = (VisuElement*)pos->data;
      str = getSpinResources(ele);
      if (str->shape != SPIN_ELEMENT_SHAPE_DEFAULT)
        visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_ELEMENT_SHAPE,
                                     "%s %s", ele->name,
                                     visu_rendering_spin_shape_number_to_name(str->shape));
      if (ABS(str->height - SPIN_ELEMENT_HAT_LENGTH_DEFAULT) > 1e-6 ||
	  ABS(str->u_height - SPIN_ELEMENT_TAIL_LENGTH_DEFAULT) > 1e-6 ||
	  ABS(str->length - SPIN_ELEMENT_HAT_RADIUS_DEFAULT) > 1e-6 ||
	  ABS(str->u_length - SPIN_ELEMENT_TAIL_RADIUS_DEFAULT) > 1e-6 ||
	  str->use_element_color != SPIN_ELEMENT_TAIL_COLOR_DEFAULT ||
	  str->use_element_color_hat != SPIN_ELEMENT_HAT_COLOR_DEFAULT)
        visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_ELEMENT_ARROW,
                                     "%s %f %f %f %f %d %d", ele->name, 
                                     str->height, str->u_height, str->length, str->u_length,
                                     str->use_element_color, str->use_element_color_hat);
      if (ABS(str->aAxis - SPIN_ELEMENT_AAXIS_DEFAULT) > 1e-6 ||
	  ABS(str->bAxis - SPIN_ELEMENT_BAXIS_DEFAULT) > 1e-6 ||
	  str->elipsoidColor != SPIN_ELEMENT_ELIP_COLOR_DEFAULT)
        visu_config_file_exportEntry(data, FLAG_RESOURCES_SPIN, FLAG_ELEMENT_ELIPSOID,
                                     "%s %f %f %d", ele->name, 
                                     str->aAxis, str->bAxis, str->elipsoidColor);
    }

  visu_config_file_exportComment(data, "");
  g_list_free(eleList);

  DBG_fprintf(stderr, "Rendering Spin: element resources succesfully exported\n");
}

static void onSpinParametersChanged(VisuData *dataObj, VisuNode *node,
				    gpointer data _U_)
{
  g_return_if_fail(dataObj && node);

  DBG_fprintf(stderr, "Rendering Spin : callback when a spin parameter has been changed.\n");

  g_signal_emit_by_name(G_OBJECT(dataObj), "RenderingChanged",
                        visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node), NULL);
}

/***************/
/* OpenGL part */
/***************/
static int getShape(VisuElement *element)
{
  struct spinResources_struct *str;

  str = getSpinResources(element);
  g_return_val_if_fail(str, 0);

  return str->openGLIdentifier;
}
static int createShape(VisuElement* ele, VisuGlView *view)
{
  int nlatl=0, nlatul=0, nlatoh=0;
  float hatLength, hatRadius, tailLength, tailRadius;
  struct spinResources_struct *str;
  GLUquadricObj *obj;

  g_return_val_if_fail(ele, -1);
 
  str = getSpinResources(ele);
  hatRadius  = str->length;
  tailRadius = str->u_length;
  hatLength  = str->height;
  tailLength = str->u_height;

  nlatul = visu_gl_view_getDetailLevel(view, tailRadius) ;
  nlatl = visu_gl_view_getDetailLevel(view, hatRadius);
  nlatoh = visu_gl_view_getDetailLevel(view, hatLength);
  
  DBG_fprintf(stderr, "Rendering Spin : creating arrow for %s,  nlatl = %d,"
	      " nlatul = %d, nlatoh = %d\n", 
	      ele->name, nlatl, nlatul, nlatoh);

  /* We always build atomic shapes in case we need them. */
  str->openGLIdentifierAtomic =
    visu_rendering_createElement(visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME),
                                 ele, view);

  obj = gluNewQuadric();
  glNewList(str->openGLIdentifier, GL_COMPILE);
  switch (str->shape)
    {
    case VISU_RENDERING_SPIN_ARROW_SMOOTH:
      visu_gl_drawSmoothArrow(obj, visu_element_getMaterialId(ele),
				       VISU_GL_ARROW_CENTERED,
				       tailLength, tailRadius, nlatul, str->use_element_color,
				       hatLength, hatRadius, nlatl, str->use_element_color_hat);
      break;
    case VISU_RENDERING_SPIN_ARROW_SHARP:
      visu_gl_drawEdgeArrow(visu_element_getMaterialId(ele),
				     VISU_GL_ARROW_CENTERED,
				     tailLength, tailRadius, str->use_element_color,
				     hatLength, hatRadius, str->use_element_color_hat);
      break;
    case VISU_RENDERING_SPIN_ELLIPSOID:
      nlatl = visu_gl_view_getDetailLevel(view, str->bAxis);
      visu_gl_drawEllipsoid(obj, visu_element_getMaterialId(ele),
				     str->aAxis, str->bAxis, nlatl,
				     str->elipsoidColor);
      break;
    case VISU_RENDERING_SPIN_TORUS:
      nlatul = visu_gl_view_getDetailLevel(view, str->aAxis);
      nlatl = visu_gl_view_getDetailLevel(view, str->bAxis);
      visu_gl_drawTorus(obj, visu_element_getMaterialId(ele),
				 str->aAxis - str->bAxis, str->aAxis / str->bAxis,
				 nlatul, nlatl, str->elipsoidColor);
      break;
    default:
      g_warning("Unknown shape.");
      break;
    }
  glEndList();
  gluDeleteQuadric(obj);
  
  return str->openGLIdentifier;
}

/* This is a method called before the drawing of each node. It's needed to set the
   right positions of each atom but you can apply other treatments if you need.
   Here we also rotate our atom according to the spin theta and phi parameters. */
#ifndef DEG2RAD
#define DEG2RAD(x) (0.01745329251994329509 * x)    /* pi / 180 * x */
#endif
static void positionShape(VisuData *visuData, VisuNode *node,
                          VisuElement* ele, int eleGlId)
{
  /* This is the part responsible for the colorisation of
     the atoms according to their spin. */
  float mm[4]; 
  float hsl[3];
  float rgba[4];
  float xyz[3];

  float *spinValues, scale;
  
  float theta_prime;
  float phi_prime;
  float spherical[3];
  float matrix_rot_theta[3][3];
  float matrix_rot_phi[3][3]; 
  float cartesian[3]; 
  float cartesian_prime[3];
  float cartesian_second[3];
  float cosCone, sinCone;
  float ratio, globalMax;
  GValueArray *maxModulus;
  guint iele;

  struct spinResources_struct *str;

  /* Test the modulus. */
  visu_node_array_getPropertyValue(VISU_NODE_ARRAY(visuData), node,
                                   VISU_RENDERING_SPIN_VALUES_ID, &spinValue);
  spinValues = (float*)g_value_get_pointer(&spinValue);
  scale = visu_data_getNodeScalingFactor(visuData, node);
  if (spinValues && (spinValues[TOOL_MATRIX_SPHERICAL_MODULUS] != 0. ||
		     spinMethod->priv->spinPolicy == VISU_RENDERING_SPIN_ALWAYS))
    {
      /* Get the scaling factor for this element. */
      globalMax = 0.f;
      if (spinMethod->priv->spinModulusUsage == VISU_RENDERING_SPIN_PER_TYPE)
        {
          maxModulus = visu_node_array_getElementProperty(VISU_NODE_ARRAY(visuData),
                                                          VISU_RENDERING_SPIN_MAX_MODULUS_ID);
          g_return_if_fail(maxModulus);
          ratio = 1.f / g_value_get_float(g_value_array_get_nth(maxModulus,
                                                                node->posElement));
        }
      else if (spinMethod->priv->spinModulusUsage == VISU_RENDERING_SPIN_GLOBAL)
        {
          maxModulus = visu_node_array_getElementProperty(VISU_NODE_ARRAY(visuData),
                                                          VISU_RENDERING_SPIN_MAX_MODULUS_ID);
          g_return_if_fail(maxModulus);
          globalMax = -G_MAXFLOAT;
          for (iele = 0; iele < maxModulus->n_values; iele++)
            globalMax = MAX(g_value_get_float(g_value_array_get_nth(maxModulus,iele)),
                            globalMax);
          ratio = 1.f / globalMax;
        }
      else
        ratio = 1.f;

      /* We draw a spin shape. */
      spherical[0] = 1;
      spherical[1] = spinValues[TOOL_MATRIX_SPHERICAL_THETA];
      spherical[2] = spinValues[TOOL_MATRIX_SPHERICAL_PHI];

      cosCone = cos(DEG2RAD(spinMethod->priv->coneOrientation[0]));
      sinCone = sin(DEG2RAD(spinMethod->priv->coneOrientation[0]));
      matrix_rot_theta[0][0] = cosCone;
      matrix_rot_theta[0][1] = 0;
      matrix_rot_theta[0][2] = -sinCone;
      matrix_rot_theta[1][0] = 0;
      matrix_rot_theta[1][1] = 1;
      matrix_rot_theta[1][2] = 0;
      matrix_rot_theta[2][0] = sinCone;
      matrix_rot_theta[2][1] = 0;
      matrix_rot_theta[2][2] = cosCone;

      cosCone = cos(DEG2RAD(-spinMethod->priv->coneOrientation[1]));
      sinCone = sin(DEG2RAD(-spinMethod->priv->coneOrientation[1]));
      matrix_rot_phi[0][0] = cosCone;
      matrix_rot_phi[0][1] = -sinCone;
      matrix_rot_phi[0][2] = 0;
      matrix_rot_phi[1][0] = sinCone;
      matrix_rot_phi[1][1] = cosCone;
      matrix_rot_phi[1][2] = 0;
      matrix_rot_phi[2][0] = 0;
      matrix_rot_phi[2][1] = 0;
      matrix_rot_phi[2][2] = 1; 

      cartesian[0] = sin(DEG2RAD(spinValues[TOOL_MATRIX_SPHERICAL_THETA])) *
	cos(DEG2RAD(spinValues[TOOL_MATRIX_SPHERICAL_PHI]));
      cartesian[1] = sin(DEG2RAD(spinValues[TOOL_MATRIX_SPHERICAL_THETA])) *
	sin(DEG2RAD(spinValues[TOOL_MATRIX_SPHERICAL_PHI]));
      cartesian[2] = cos(DEG2RAD(spinValues[TOOL_MATRIX_SPHERICAL_THETA]));

      /*   fprintf(stderr, "%f %f %f --> ", spherical[0], spherical[1], spherical[2]); */

      tool_matrix_productVector(cartesian_prime, matrix_rot_phi, cartesian);
      tool_matrix_productVector(cartesian_second, matrix_rot_theta, cartesian_prime);
      /*   square_matrix_product(matrix_rot_phi, cartesian_second, cartesian_ter, 3); */

      tool_matrix_cartesianToSpherical(spherical, cartesian_second);

      /*   fprintf(stderr, "%f %f %f \n", spherical[0], spherical[1], spherical[2]); */

      /*   theta_prime = spherical[1] - coneOrientation[0]; */
      /*   phi_prime = spherical[2]; */

      /*   /\* fin modifs *\/ */
      /*   theta_prime = tool_modulo_float(theta_prime, 360); */

      /*   if(theta_prime > 180) */
      /*     { */
      /*       theta_prime = 360 - theta_prime; */
      /*       phi_prime += 180; */
      /*       phi_prime = tool_modulo_float(phi_prime, 360); */
      /*     } */

      /*   phi_prime -= coneOrientation[1]; */
      /*   phi_prime = tool_modulo_float(phi_prime, 360);  /\* Probably useless *\/ */

      theta_prime = spherical[1];
      phi_prime = spherical[2];

      hsl[2] = 1 - theta_prime/180.;

      hsl[0] = tool_modulo_float(phi_prime - spinMethod->priv->colorWheel, 360) / 360.;

      /* FIN ALGO COLORISATION */

      hsl[1] = 1;
  
      tool_color_convertHSLtoRGB(mm, hsl);

      mm[3] = ele->rgb[3];
  
      /* The following is the part responsible for the rotation 
	 of the atoms according to their spins. */

      visu_data_getNodePosition(visuData, node, xyz);

      glPushMatrix();

      /* Translate to the rendering position. */
      glTranslated(xyz[0], xyz[1], xyz[2]);


      /* If we need to draw also an atom shape. */
      if (spinMethod->priv->spinAndAtomicRendering)
	{
	  glCallList(visu_element_getMaterialId(ele));
	  str = (struct spinResources_struct*)getSpinResources(ele);
	  glCallList(str->openGLIdentifierAtomic);
	}

      /* We rotate the spin shape into the right direction. */
      glRotated(spinValues[TOOL_MATRIX_SPHERICAL_PHI], 0, 0, 1);  
      glRotated(spinValues[TOOL_MATRIX_SPHERICAL_THETA], 0, 1, 0); 

      /* We scale the shape if required. */
      if (spinMethod->priv->spinModulusUsage != VISU_RENDERING_SPIN_CONSTANT)
	{
	  glScalef(spinValues[TOOL_MATRIX_SPHERICAL_MODULUS],
		   spinValues[TOOL_MATRIX_SPHERICAL_MODULUS],
		   spinValues[TOOL_MATRIX_SPHERICAL_MODULUS]);
/* 	  glScalef(1.0, 1.0, str->modulusProp * spinValues[TOOL_MATRIX_SPHERICAL_MODULUS]); */
	}

      /* We change its color if required. */
      if (visu_data_getUserColor(visuData, ele, node, rgba))
	visu_gl_setColor(ele->material, rgba);
      else
	visu_gl_setColor(ele->material, mm);

      /* We finaly put the spin shape. */
      glScalef(scale * ratio, scale * ratio, scale * ratio);
      glCallList(eleGlId);

      glPopMatrix();
    }
  else
    {
      /* Case modulus is null. */
      if (spinMethod->priv->spinPolicy == VISU_RENDERING_SPIN_ATOMIC_NULL ||
          spinMethod->priv->spinAndAtomicRendering)
	{
	  visu_data_getNodePosition(visuData, node, xyz);

	  glPushMatrix();
  
	  glTranslated(xyz[0], xyz[1], xyz[2]);

	  glScalef(scale, scale, scale);
	  glCallList(visu_element_getMaterialId(ele));
	  str = (struct spinResources_struct*)getSpinResources(ele);
	  glCallList(str->openGLIdentifierAtomic);
	  glPopMatrix();
	}
    }
}
