/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "visu_basic.h"
#include "visu_object.h"
#include "visu_rendering.h"
#include "visu_extension.h"
#include "visu_data.h"
#include "visu_elements.h"
#include "visu_commandLine.h"
#include "opengl.h"
#include "visu_dump.h"
#include "visu_pairs.h"
#include "visu_tools.h"
#include "visu_configFile.h"
#include "extraFunctions/dataFile.h"
#include "extraFunctions/plane.h"
#include "extraFunctions/pot2surf.h"
#include "extraFunctions/extraNode.h"
#include "extraFunctions/map.h"
#include "openGLFunctions/objectList.h"
#include "openGLFunctions/text.h"
#include "openGLFunctions/interactive.h"
#include "OSOpenGL/visu_openGL.h"
#include "extensions/fogAndBGColor.h"
#include "extensions/scale.h"
#include "extensions/marks.h"
#include "extensions/axes.h"
#include "extensions/box.h"
#include "extensions/legend.h"
#include "extensions/pairs.h"
#include "extensions/shade.h"
#include "extensions/node_vectors.h"
#include "extensions/planes.h"
#include "extensions/surfs.h"
#include "extensions/maps.h"
#include "extensions/forces.h"
#include "coreTools/toolConfigFile.h"
#include "extraFunctions/geometry.h"
#include "renderingMethods/renderingAtomic.h"
#include "renderingMethods/renderingSpin.h"

#include <stdio.h>
#include <locale.h>
#include <unistd.h> /* For the access markers R_OK, W_OK ... */
#include <string.h>
#include <stdlib.h>

/* For the GdkPixbuf, to be removed later. */
#include <gtk/gtk.h>

/**
 * SECTION:visu_basic
 * @short_description: Main functions of V_Sim (except graphical
 * ones).
 *
 * <para>There are here the main functions of V_Sim (except for
 * graphical methods) such as open file.</para>
 */

/* Valeurs par d�faut pour les chemins utiles dans V_Sim. */
#if SYSTEM_X11 == 1
#define V_SIM_DATA_DIR_DEFAULT    "/usr/local/share/v_sim"
#define V_SIM_LEGAL_DIR_DEFAULT   "/usr/local/share/v_sim"
#define V_SIM_PIXMAPS_DIR_DEFAULT "/usr/local/share/v_sim/pixmaps"
#define V_SIM_ICONS_DIR_DEFAULT   "/usr/local/share/icons"
#define V_SIM_PLUGINS_DIR_DEFAULT "/usr/local/lib/v_sim/plug-ins"
#define V_SIM_LOCALE_DIR_DEFAULT  "/usr/local/share/locale"
#endif
#if SYSTEM_WIN32 == 1
#define V_SIM_DATA_DIR_DEFAULT    "C:\\PROGRAM FILES\\V_Sim\\share\\v_sim"
#define V_SIM_LEGAL_DIR_DEFAULT   "C:\\PROGRAM FILES\\V_Sim\\share\\doc\\v_sim"
#define V_SIM_PIXMAPS_DIR_DEFAULT "C:\\PROGRAM FILES\\V_Sim\\share\\v_sim\\pixmaps"
#define V_SIM_ICONS_DIR_DEFAULT   "C:\\PROGRAM FILES\\V_Sim\\share\\icons"
#define V_SIM_PLUGINS_DIR_DEFAULT "C:\\PROGRAM FILES\\V_Sim\\lib\\v_sim\\plug-ins"
#define V_SIM_LOCALE_DIR_DEFAULT  "C:\\PROGRAM FILES\\V_Sim\\share\\locale"
#endif
static gchar *exeLocation = NULL;
static gchar *v_sim_data_dir = NULL;
static gchar *v_sim_legal_dir = NULL;
static gchar *v_sim_pixmaps_dir = NULL;
static gchar *v_sim_icons_dir = NULL;
static gchar *v_sim_local_conf_dir = NULL;
static gchar *v_sim_old_local_conf_dir = NULL;
static gchar *v_sim_plugins_dir = NULL;
static gchar *v_sim_locale_dir = NULL;
static ToolUnits preferedUnit;

/* Local methods. */
static gboolean dumpData(gpointer data);
static void freeExtension(gpointer data);
static void onDataLoaded(VisuObject* obj, VisuData *dataObj, gpointer data);
static void setToolFileFormatOption(gpointer key, gpointer value, gpointer data);

#define FLAG_PARAMETER_UNIT   "main_unit"
#define DESC_PARAMETER_UNIT   "Define the prefered unit to display files ; string"
static gboolean readUnit(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			 VisuData *dataObj, VisuGlView *view, GError **error);
static void exportParameters(GString *data, VisuData *dataObj, VisuGlView *view);

/**
 * visu_basic_init:
 *
 * A call to this method is done at startup after having probe the locale of the file
 * system and having initialized the rendering window. It makes the following actions :
 * create the visu object to store the signals, initialize the module part (parameters,
 * and resources), initialize the dump part, the OpenGL part and its extensions, the
 * storage of elements and the 'colorize with data' part.
 */
void visu_basic_init(void)
{
  int res;
  VisuConfigFileEntry *entry;

  DBG_fprintf(stderr,"--- Initialising variables ---\n");
  /* We want to read . as floating point separator : in french it is , */
  setlocale(LC_NUMERIC, "C");
  tool_matrix_init();
  tool_shade_get_type();
  preferedUnit = TOOL_UNITS_UNDEFINED;

  /* Force the creation of the main object class. */
  g_type_class_ref(VISU_TYPE_OBJECT);
  g_signal_connect(VISU_OBJECT_INSTANCE, "dataLoaded",
		   G_CALLBACK(onDataLoaded), (gpointer)0);

  /* initialise OpenGL. */
  DBG_fprintf(stderr,"--- Initialising OpenGL ---\n");
  visu_gl_init();

  DBG_fprintf(stderr,"--- Initialising dataFile modules ---\n");
  res = visu_colorization_init();
  if (!res)
    exit(1);

  /* Initialise extra functions. */
  DBG_fprintf(stderr,"--- Initialising extra functions ---\n");
  g_type_class_ref(VISU_TYPE_SURFACES);
  g_type_class_ref(VISU_TYPE_SCALAR_FIELD);
  visu_geometry_init();
  visu_map_init();

  /* Force the creation of some internal classes. */
  DBG_fprintf(stderr,"--- Initialising internal classes ---\n");
  g_type_class_ref(VISU_TYPE_PAIR_LINK);
  g_type_class_ref(VISU_TYPE_RENDERING);
  g_type_class_ref(VISU_TYPE_GL_EXT);
  visu_gl_ext_axes_getDefault();
  visu_gl_ext_box_getDefault();
  visu_gl_ext_box_legend_getDefault();
  visu_gl_ext_legend_getDefault();
  visu_gl_ext_pairs_getDefault();
  visu_gl_ext_planes_getDefault();
  visu_gl_ext_surfaces_getDefault();
  visu_gl_ext_bg_getDefault();
  visu_gl_ext_fog_init();
  g_type_class_ref(VISU_TYPE_GL_EXT_MARKS);
  g_type_class_ref(VISU_TYPE_GL_EXT_NODE_VECTORS);
  g_type_class_ref(VISU_TYPE_GL_EXT_INFOS);
  g_type_class_ref(VISU_TYPE_GL_EXT_SCALE);
  g_type_class_ref(VISU_TYPE_GL_EXT_FORCES);
  g_type_class_ref(VISU_TYPE_DATA);

  entry = visu_config_file_addEntry(VISU_CONFIG_FILE_PARAMETER,
				  FLAG_PARAMETER_UNIT,
				  DESC_PARAMETER_UNIT,
				  1, readUnit);
  visu_config_file_entry_setVersion(entry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
				   exportParameters);
}

/**
 * visu_basic_parseConfigFiles:
 * @view: (allow-none): current #VisuGlView if any.
 * 
 * Parse the parameter and the resource file. Used once at startup.
 *
 * Returns: a newly allocated #GString if some error occured.
 */
gchar* visu_basic_parseConfigFiles(VisuGlView *view)
{
  GString *message;
  char* path;
  GError *error;
  gboolean res;
  char *resFile;

  message = (GString*)0;
  DBG_fprintf(stderr,"--- Initialising the parameters ---\n");
  /* Look for parameters file */
  path = visu_config_file_getValidPath(VISU_CONFIG_FILE_PARAMETER, R_OK, 0);
  if (path)
    {
      error = (GError*)0;
      res = visu_config_file_load(VISU_CONFIG_FILE_PARAMETER, path,
                                  (VisuData*)0, (VisuGlView*)0, &error);
      if (!res && error)
	{
	  message = g_string_new("");
	  g_string_append_printf(message,
				 _("While parsing parameter file '%s':\n\n%s"),
				 path, error->message);
	  g_error_free(error);
	}
      g_free(path);
    }
  else
    message = g_string_new("Unable to find a valid parameters file."
			   " Starting with defaults.\n");

  DBG_fprintf(stderr,"--- Initialising resources ---\n");
  /* Look for resources file */
  resFile = commandLineGet_resourcesFile();
  if (!resFile)
    path = visu_config_file_getValidPath(VISU_CONFIG_FILE_RESOURCE, R_OK, 0);
  else
    path = g_strdup(resFile);
  if (path)
    {
      error = (GError*)0;
      res = visu_config_file_load(VISU_CONFIG_FILE_RESOURCE, path,
                                  (VisuData*)0, view, &error);
      if (!res && error)
	{
	  if (message)
	    g_string_append(message, "\n\n");
	  else
	    message = g_string_new("");
	  g_string_append_printf(message,
				 _("While parsing resource file '%s':\n\n%s"),
				 path, error->message);
	  g_error_free(error);
	}
      g_free(path);
    }

  if (message)
    return g_string_free(message, FALSE);
  else
    return (gchar*)0;
}

/**
 * visu_basic_setDataFromCommandLine:
 * 
 * Read the command line option and set the filenames for a new
 * #VisuData. The object is not loaded (files are not parsed), just prepared.
 *
 * Returns: (transfer full): a newly allocated #VisuData if required.
 */
VisuData* visu_basic_setDataFromCommandLine(void)
{
  char* filename, *spin_filename;
  VisuData *newData;

  newData = (VisuData*)0;

  DBG_fprintf(stderr, "Visu Basic: create VisuData from command line arguments.\n");
  filename = commandLineGet_ArgFilename();
  spin_filename = commandLineGet_ArgSpinFileName();
  if (filename && !spin_filename)
    {
      newData = visu_data_new();
      visu_object_setRendering(VISU_OBJECT_INSTANCE,
                               visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME));

      visu_data_addFile(newData, filename, FILE_KIND_POSITION, (ToolFileFormat*)0);
    }
  else if(filename && spin_filename)
    {
      newData = visu_data_new();
      visu_object_setRendering(VISU_OBJECT_INSTANCE, visu_rendering_getByName(VISU_RENDERING_SPIN_NAME));

      visu_data_addFile(newData, filename, FILE_KIND_POSITION, (ToolFileFormat*)0);
      visu_data_addFile(newData, spin_filename, FILE_KIND_SPIN, (ToolFileFormat*)0);
    }
  return newData;
}

static void onDataLoaded(VisuObject* obj _U_, VisuData *dataObj, gpointer data _U_)
{
  visu_extra_node_addLabel(VISU_DATA(dataObj));
}

struct _dump
{
  VisuData *data;
  VisuGlView *view;
  VisuDump *format;
  gchar *exportFileName;
  int width, height;

  /* Bitmap export only. */
  VisuPixmapContext *dumpData;
  GMainLoop *loop;

  /* Return. */
  int status;
};

/**
 * visu_basic_mainExport:
 *
 * This method is called when V_Sim is in export mode from the command line. 
 *
 * Returns: 0 if everything is normal, 1 if an error occured.
 */
int visu_basic_mainExport(void)
{
  gchar *message;
  VisuData *newData;
  GError *error;
  gboolean res;
  struct _dump dt;
  GList *pnt;
  GHashTable *opts;
  VisuBasicCLISet *set;
  ToolOption *id;
  VisuGlExtNodes *extNodes;

  dt.exportFileName = commandLineGet_ExportFileName();
  if (!dt.exportFileName)
    {
      g_error("This method should be called with"
	      " an argument that is the file name to export to.\n");
    }
  
  id = (ToolOption*)0;
  opts = commandLineGet_options();
  if (opts)
    id = (ToolOption*)g_hash_table_lookup(opts, "fileFormatId");
  pnt = visu_dump_getAllModules();
  if (!id)
    while (pnt && !tool_file_format_match(TOOL_FILE_FORMAT(pnt->data), dt.exportFileName))
      pnt = g_list_next(pnt);
  else
    pnt = g_list_nth(pnt, g_value_get_int(tool_option_getValue(id)) - 1);
  if (!pnt)
    {
      g_warning(_("The format can't be found from the"
                  " filename '%s' entered.\n"), dt.exportFileName);
      g_print(_("Use -o fileFormatId=id to specify a file format"
                " when the autodetection fails. Get a list of ids"
                " with option -o list:\n\n"));
      visu_basic_showOptionHelp(TRUE);
      return 1;
    }
  dt.format = VISU_DUMP(pnt->data);

  /* We transfer some options to file format. */
  if (opts)
    g_hash_table_foreach(opts, setToolFileFormatOption,
                         (gpointer)dt.format);

  /* Start the elements related to the main loop. */
  dt.loop = g_main_loop_new(NULL, FALSE);
  g_type_init();

  visu_basic_init();

  commandLineGet_XWindowGeometry((int*)&(dt.width), (int*)&(dt.height));
  if (visu_dump_getGlStatus(dt.format))
    {
      visu_gl_initGraphics();

      /* We need to initialise the OpenGL surface as soon as possible to
	 avoid crash in some OpenGL implementations when OpenGL routines
	 may be called while parsing files. */
      dt.dumpData = visu_pixmap_context_new((guint)dt.width, (guint)dt.height);
      if (!dt.dumpData)
	{
	  g_error("can't create off-screen rendering. Command line"
		  " exportation is not available.");
	}
      /* We set the glViewport of this new context. */
      glViewport(0, 0, dt.width, dt.height);
      /* We call the given draw method. */
      visu_gl_initContext();
    }

  newData = visu_basic_setDataFromCommandLine();
  if (!newData)
    {
      if (visu_dump_getGlStatus(dt.format))
	visu_pixmap_context_free(dt.dumpData);
      g_error(_("a file to render is mandatory with the '--export' option."));
    }
  dt.view = visu_gl_view_new();
  dt.data = newData;
  
  error = (GError*)0;
  res = visu_object_load(VISU_OBJECT_INSTANCE, newData, 0, (GCancellable*)0, &error);
  if (!res)
    {
      if (visu_dump_getGlStatus(dt.format))
	visu_pixmap_context_free(dt.dumpData);
      g_object_unref(newData);
      g_error("%s", (error)?error->message:"No error message!");
    }

  error = (GError*)0;
  dt.status = 0;
  if (visu_dump_getGlStatus(dt.format))
    {
      message = visu_basic_parseConfigFiles(dt.view);
      if (message)
	{
	  g_warning("%s", message);
	  g_free(message);
	}

      /* Load extensions by hand. */
      extNodes = visu_gl_ext_nodes_new();
      visu_gl_ext_nodes_setData(extNodes, dt.view, dt.data);
      visu_gl_ext_pairs_setData(visu_gl_ext_pairs_getDefault(), dt.view, dt.data);
      visu_gl_ext_axes_setGlView(visu_gl_ext_axes_getDefault(), dt.view);
      visu_gl_ext_box_setBox(visu_gl_ext_box_getDefault(),
                             visu_boxed_getBox(VISU_BOXED(dt.data)));
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(visu_gl_ext_box_legend_getDefault()),
                                  dt.view);
      visu_gl_ext_box_legend_setBox(visu_gl_ext_box_legend_getDefault(),
                                    visu_boxed_getBox(VISU_BOXED(dt.data)));
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(visu_gl_ext_legend_getDefault()),
                                  dt.view);
      visu_gl_ext_legend_setNodes(visu_gl_ext_legend_getDefault(),
                                  VISU_NODE_ARRAY(dt.data));

      /* Run a main loop to load everything. */
      DBG_fprintf(stderr, "Visu Basic: emitting the "
		  "'dataRendered' signal.\n");
      g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "dataRendered",
                            newData, dt.view, NULL);
      DBG_fprintf(stderr, "Visu Basic: emitting done.\n");
      
      DBG_fprintf(stderr, "Visu Basic: adding the load call back in the queue.\n");
      g_idle_add(dumpData, (gpointer)&dt);
  
      g_main_loop_run(dt.loop);
    }
  else
    {
      /* Direct export. */
      set = g_malloc0(sizeof(VisuBasicCLISet));
      if (!visu_basic_applyCommandLine(dt.data, dt.view, set, &error))
	{
	  g_warning("%s", error->message);
	  g_error_free(error);
	  dt.status = 1;
	}
      else if (!visu_dump_write(dt.format, dt.exportFileName,
                                dt.width, dt.height, dt.data, (GArray*)0,
                                (ToolVoidDataFunc)0, (gpointer)0, &error) && error)
	{
	  g_warning("%s", error->message);
	  g_error_free(error);
	  dt.status = 1;
	}
      freeExtension(set);
    }

  g_object_unref(G_OBJECT(dt.data));
  g_object_unref(G_OBJECT(dt.view));

  return dt.status;
}

/**
 * visu_basic_setExtInfos:
 * @dataObj: a #VisuData object ;
 * @view: a #VisuGlView object.
 * @method: where to draw information ;
 * @nodes: a possible list of selected nodes, or NULL ;
 * @data: the information to draw.
 *
 * Create and draw possible information on nodes. This method
 * is mainly used when V_Sim run without the command panel.
 */
void visu_basic_setExtInfos(VisuData *dataObj, VisuGlView *view,
                            VisuGlExtInfosDrawMethod method,
                            int *nodes, VisuDataNode *data)
{
  gboolean redraw;

  g_return_if_fail(VISU_IS_DATA(dataObj));
  g_return_if_fail((method == EXT_DRAW_METH_OTHER && VISU_IS_DATA_NODE_TYPE(data)) ||
		   method != EXT_DRAW_METH_OTHER);

  DBG_fprintf(stderr, "VisuBasic: set the extension infos with method %d.\n", method);
  visu_gl_ext_setActive(VISU_GL_EXT(visu_gl_ext_infos_getDefault()),
                           (method != EXT_DRAW_METH_NONE));

  redraw = FALSE;
  switch (method)
    {
    case EXT_DRAW_METH_NONE:
      break;
    case EXT_DRAW_METH_ID:
      redraw = visu_gl_ext_infos_drawIds(visu_gl_ext_infos_getDefault(), nodes);
      break;
    case EXT_DRAW_METH_TYPE:
      redraw = visu_gl_ext_infos_drawElements(visu_gl_ext_infos_getDefault(), nodes);
      break;
    default:
      redraw = visu_gl_ext_infos_drawData(visu_gl_ext_infos_getDefault(), data, nodes);
    }
  redraw = visu_gl_ext_infos_setGlView(visu_gl_ext_infos_getDefault(), view) || redraw;
  redraw = visu_gl_ext_infos_setData(visu_gl_ext_infos_getDefault(), dataObj) || redraw;
  if (redraw)
    visu_gl_ext_infos_draw(visu_gl_ext_infos_getDefault());
}

static void setBgImage(const gchar *filename)
{
  GError *error;
  GdkPixbuf *pixbuf;
  gchar *title;
  gboolean fit;

  DBG_fprintf(stderr, "Visu Basic: set the background image to '%s'.\n",
	      filename);
  error = (GError*)0;
  pixbuf = gdk_pixbuf_new_from_file(filename, &error);
  if (!pixbuf)
    {
      g_warning("%s", error->message);
      g_error_free(error);
      return;
    }

  fit = TRUE;
  title = g_path_get_basename(filename);
  if (!strcmp(title, "logo_grey.png"))
    {
      fit = FALSE;
      g_free(title);
      title = (gchar*)0;
    }
  visu_gl_ext_bg_setImage(visu_gl_ext_bg_getDefault(),
                          gdk_pixbuf_get_pixels(pixbuf),
                          gdk_pixbuf_get_width(pixbuf),
                          gdk_pixbuf_get_height(pixbuf),
                          gdk_pixbuf_get_has_alpha(pixbuf),
                          title, fit);
  g_object_unref(pixbuf);
  g_free(title);
}

static void computeMap(VisuBasicCLISet *set, VisuGlExtMaps *maps, VisuGlExtShade *shade)
{
  int i;
  float *mM, *marks;
  float drawnMinMax[2], minmax[2];

  g_return_if_fail(set);

  drawnMinMax[0] = + G_MAXFLOAT;
  drawnMinMax[1] = - G_MAXFLOAT;
  /* We create the maps. */
  for (i = 0; set->mapsList[i]; i++)
    {
      visu_gl_ext_maps_add(maps, set->mapsList[i], (float)commandLineGet_mapPrecision(),
                           set->shade, set->isoLinesColor, FALSE);
      visu_map_compute(set->mapsList[i]);
      mM = visu_map_getScaledMinMax(set->mapsList[i]);
      drawnMinMax[0] = MIN(drawnMinMax[0], mM[0]);
      drawnMinMax[1] = MAX(drawnMinMax[1], mM[1]);
    }
  mM = commandLineGet_mapMinMax();
  for (i = 0; set->mapsList[i]; i++)
    visu_map_setLines(set->mapsList[i], set->nIsolines, (mM)?mM:drawnMinMax);

  DBG_fprintf(stderr, "Visu Basic: set two marks [%g; %g].\n", drawnMinMax[0], drawnMinMax[1]);
  if (mM)
    {
      minmax[0] = 0.;
      minmax[1] = 1.;
    }
  else
    {
      minmax[0] = drawnMinMax[0];
      minmax[1] = drawnMinMax[1];
    }
  marks = g_malloc(sizeof(float) * (2 + set->nIsolines));
  for (i = 0; i < (gint)set->nIsolines; i++)
    marks[1 + i] = (minmax[1] - minmax[0]) * (float)(i + 1) /
      (float)(set->nIsolines + 1) + minmax[0];
  marks[0]                  = drawnMinMax[0];
  marks[1 + set->nIsolines] = drawnMinMax[1];
  visu_gl_ext_shade_setMarks(shade, marks, 2 + set->nIsolines);
  g_free(marks);
}

/**
 * visu_basic_createExtensions:
 * @data: a #VisuData object ;
 * @view: a #VisuGlView object.
 * @set: a table of options ;
 * @rebuild: a boolean.
 *
 * Create all extensions (planes, surfaces...) for the given data. Use
 * static values read from visu_basic_applyCommandLine(). This method
 * is mainly used when V_Sim run without the command panel.
 */
void visu_basic_createExtensions(VisuData *data, VisuGlView *view,
                                 VisuBasicCLISet *set, gboolean rebuild)
{
  VisuGlExtShade *legExt, *dtExt;
  VisuGlExtPlanes *planeExt;
  VisuGlExtSurfaces *surfExt;
  VisuGlExtMaps *mapExt;
  VisuColorization *dt;
  double minMax[2];
  gint id;

  /* We create the colourisation extensions, if necessary. */
  dt = visu_colorization_get(data, FALSE, (gboolean*)0);
  if (dt && visu_colorization_getSingleColumnId(dt, &id))
    {
      dtExt = visu_gl_ext_shade_new("Colourisation legend");
      visu_gl_ext_shade_setMinMax(dtExt, visu_colorization_getMin(dt, id),
                                  visu_colorization_getMax(dt, id));
      visu_gl_ext_shade_setShade(dtExt, set->shade);
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(dtExt), view);
      if (rebuild)
        visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(dtExt));
    }

  /* We create the plane extension if necessary. */
  if (set->planesList)
    {
      DBG_fprintf(stderr, "Visu Basic: create plane extension.\n");
      planeExt = visu_gl_ext_planes_new(NULL);
      visu_gl_ext_setActive(VISU_GL_EXT(planeExt), TRUE);
      for (id = 0; set->planesList[id]; id++)
        visu_gl_ext_planes_add(planeExt, set->planesList[id]);
      if (rebuild)
        visu_gl_ext_planes_draw(planeExt);
    }

  /* We create the surface extension. */
  if (set->surfsList)
    {
      DBG_fprintf(stderr, "Visu Basic: create surface extension.\n");
      surfExt = visu_gl_ext_surfaces_new(NULL);
      visu_gl_ext_setActive(VISU_GL_EXT(surfExt), TRUE);
      for (id = 0; set->surfsList[id]; id++)
        visu_gl_ext_surfaces_add(surfExt, set->surfsList[id]);
      if (rebuild)
        visu_gl_ext_surfaces_draw(surfExt);
    }

  /* We create the map extension. */
  if (set->mapPlaneId)
    {
      visu_scalar_field_getMinMax((VisuScalarField*)set->fieldsList->data, minMax);

      DBG_fprintf(stderr, "Visu Basic: create map extension.\n");
      legExt = visu_gl_ext_shade_new("MapLegend");
      visu_gl_ext_frame_setScale(VISU_GL_EXT_FRAME(legExt), visu_map_getLegendScale());
      visu_gl_ext_frame_setPosition(VISU_GL_EXT_FRAME(legExt),
                                    visu_map_getLegendPosition(TOOL_XYZ_X),
                                    visu_map_getLegendPosition(TOOL_XYZ_Y));
      visu_gl_ext_frame_setGlView(VISU_GL_EXT_FRAME(legExt), view);
      visu_gl_ext_shade_setShade(legExt, set->shade);
      visu_gl_ext_shade_setMinMax(legExt, minMax[0], minMax[1]);
      visu_gl_ext_shade_setScaling(legExt, set->logScale);

      mapExt = visu_gl_ext_maps_new(NULL);
      visu_gl_ext_maps_setGlView(mapExt, view);
      computeMap(set, mapExt, legExt);
      if (rebuild)
        {
          visu_gl_ext_maps_draw(mapExt);
          visu_gl_ext_frame_draw(VISU_GL_EXT_FRAME(legExt));
        }
    }

  /* We set the background image. */
  if (set->bgImage)
    setBgImage(set->bgImage);
}

static gboolean dumpData(gpointer data)
{
  GMainContext *context;
  GError *error;
  VisuBasicCLISet *set;
  struct _dump *dt;
  GArray* image;
  double *zoomLevel;

  dt = (struct _dump*)data;

  /* Here the dump is bitmap only. */
  g_return_val_if_fail(visu_dump_getGlStatus(dt->format), FALSE);

  DBG_fprintf(stderr, "Visu Basic: begin dump exportation.\n");
  set = g_malloc0(sizeof(VisuBasicCLISet));
  g_object_set_data_full(G_OBJECT(dt->data), "optionSet", set, freeExtension);
  error = (GError*)0;
  if (!visu_basic_applyCommandLine(dt->data, dt->view, set, &error))
    {
      g_warning("%s", error->message);
      g_error_free(error);
    }
  else
    {
      visu_gl_view_setViewport(dt->view, (guint)dt->width, (guint)dt->height);
      visu_boxed_setBox(VISU_BOXED(dt->view), VISU_BOXED(dt->data), TRUE);
      if (visu_dump_getGlStatus(dt->format) && !visu_dump_getBitmapStatus(dt->format))
        {
          /* We unzoom to allow GL to render out-of-the-box points. */
          zoomLevel = g_malloc(sizeof(double));
          *zoomLevel = MAX(dt->view->camera->gross, 1.f);
          g_object_set_data_full(G_OBJECT(dt->format), "zoomLevel", zoomLevel, g_free);
          visu_gl_view_setGross(dt->view, MIN(dt->view->camera->gross, 1.f));
        }

      visu_basic_createExtensions(dt->data, dt->view, set, FALSE);

      context = g_main_loop_get_context(dt->loop);
      while(g_main_context_pending(context))
	g_main_context_iteration(context, FALSE);

      visu_gl_ext_rebuildAll();

      if (visu_dump_getBitmapStatus(dt->format))
        {
          visu_gl_redraw(dt->view, (GList*)0);
          /* We copy the pixmap into generic data. */
          image = visu_pixmap_getData((guint)dt->width, (guint)dt->height,
                                           visu_dump_getAlphaStatus(dt->format));
        }
      else
        image = (GArray*)0;

      DBG_fprintf(stderr, "Visu Basic: calling exportation routine.\n");
      error = (GError*)0;
      if (!visu_dump_write(dt->format, dt->exportFileName,
                           dt->width, dt->height, dt->data, image,
                           (ToolVoidDataFunc)0, (gpointer)0, &error) && error)
	{
	  g_warning("%s", error->message);
	  g_error_free(error);
	  dt->status = 1;
	}
      else
	dt->status = 0;

      if (image)
        g_array_free(image, TRUE);
    }

  /* We free the pixmap context. */
  DBG_fprintf(stderr, "Visu Basic: going to free the context.\n");
  visu_pixmap_context_free(dt->dumpData);

  DBG_fprintf(stderr, "Visu Basic: stopping the main loop.\n");
  g_main_loop_quit(dt->loop);

  return FALSE;
}

/**
 * visu_basic_applyCommandLine:
 * @data: a #VisuData object to apply the options on ;
 * @view: a #VisuGlView object.
 * @set: a table of options ;
 * @error: a location for error report.
 *
 * Call all the get methods on the command line options to tune the
 * given @data.
 *
 * Returns: TRUE if complete without error.
 */
gboolean visu_basic_applyCommandLine(VisuData *visuData, VisuGlView *view,
                                     VisuBasicCLISet *set, GError **error)
{
  gchar *planesFile, *surfFile, *fieldFile, *valueFile;
  int i, j, presetToolShade, nb, surfNb;
  gboolean somethingIsLoaded, res, new;
  int *colUsed;
  float *translations, *extension;
  float *values;
  gchar **names;
  GList *tmplst, *list;
  GHashTable *table;
  int *nodes;
  VisuGlExtInfosDrawId mode;
  guint info;
  VisuDataNode *dt;
  VisuGlExtInfosDrawMethod method;
  GArray *minMax;
  VisuColorization *dataFile;

  g_return_val_if_fail(visuData && set, FALSE);
  g_return_val_if_fail(error && !*error, FALSE);

  set->colorFile      = (gchar*)0;
  set->planesList     = (VisuPlane**)0;
  set->surfsList      = (VisuSurfaces**)0;
  set->mapsList       = (VisuMap**)0;
  set->shade          = (ToolShade*)0;
  set->mapPlaneId     = (gint*)0;
  set->logScale       = TOOL_MATRIX_SCALING_LINEAR;
  set->nIsolines      = 0;
  set->isoLinesColor  = (ToolColor*)0;
  set->fieldsList     = (GList*)0;
  set->bgImage        = (gchar*)0;

  surfNb = 0;

/*   commandLineGet_XWindowGeometry((int*)&width, (int*)&height); */
/*   visu_gl_view_setViewport(visuData, width, height); */

  /* The value File, maybe used later. */
  valueFile = commandLineGet_valueFile();
  /* translate argument */
  translations = commandLineGet_translation();
  if (translations)
    {
      visu_data_setXYZtranslation(visuData, translations);
      visu_data_constrainedInTheBox(visuData);
    }
  /* expand argument */
  extension = commandLineGet_extension();
  if (extension)
    {
      if (!translations)
	visu_data_constrainedInTheBox(visuData);
      visu_data_replicate(visuData, extension);
    }

  /* The shade option. */
  presetToolShade = commandLineGet_presetColor();
  list            = tool_shade_getList();
  if (presetToolShade >= 0)
    {
      if (presetToolShade >= (int)g_list_length(list))
	{
	  g_warning(_("unknown shade id (max is %d)."), g_list_length(list) - 1);
	  set->shade = (ToolShade*)g_list_nth_data(list, 0);
	}
      else
	set->shade = (ToolShade*)g_list_nth_data(list, presetToolShade);
      DBG_fprintf(stderr, "Visu Basic: load a shade from command line %p.\n",
		  (gpointer)set->shade);
    }
  else
    set->shade = (ToolShade*)g_list_nth_data(list, 0);
  /* colorize argument */
  set->colorFile = commandLineGet_colorizeFileName();
  colUsed = commandLineGet_colorizeColUsed();
  if (set->colorFile || colUsed)
    {
      DBG_fprintf(stderr, "Visu basic : loading a new data file '%s'.\n",
		  set->colorFile);
      somethingIsLoaded = FALSE;
      if (set->colorFile)
        somethingIsLoaded = (visu_colorization_new_fromFile(visuData, set->colorFile,
                                                         &new, error) !=
                             (VisuColorization*)0);
      else
        for (i = 0; i < 3; i++)
          {
            somethingIsLoaded = somethingIsLoaded || (colUsed[i] <= 0);
            if (colUsed[i] > 0)
              g_warning(_("Assign a column data without specifying a data file."
                          " Use -c option or change the value %d."), colUsed[i]);
          }

      /* Print errors if necessary. */
      if (*error)
	{
	  g_warning("%s", (*error)->message);
	  g_error_free(*error);
	  *error = (GError*)0;
	}

      if (somethingIsLoaded)
	{
          dataFile = visu_colorization_get(visuData, FALSE, (gboolean*)0);
          minMax = commandLineGet_colorMinMax();
          if (minMax->len > 0)
            {
              visu_colorization_setScaleType(dataFile, VISU_COLORIZATION_MINMAX);
              for (i = 0; i < (int)minMax->len / 3; i++)
                {
                  j = g_array_index(minMax, int, 3 * i) - 1;
                  visu_colorization_setMin(dataFile, g_array_index(minMax, float, 3 * i + 1), j);
                  visu_colorization_setMax(dataFile, g_array_index(minMax, float, 3 * i + 2), j);
                }
            }
          for (i = 0; i < 3; i++)
            visu_colorization_setColUsed(dataFile, (colUsed)?colUsed[i] - 1:0, i);
          visu_colorization_setShade(dataFile, set->shade);
          if (commandLineGet_scalingColumn() >= 0)
            visu_colorization_setScalingUsed(dataFile, commandLineGet_scalingColumn());
	  visu_colorization_setUsed(visuData, TRUE);
	}
    }
  /* VisuPlanes argument. */
  planesFile = (valueFile)?valueFile:commandLineGet_planesFileName();
  if (planesFile)
    {
      somethingIsLoaded = visu_plane_class_parseXMLFile(planesFile, &set->planesList, error);
      if (!somethingIsLoaded)
	return FALSE;
      else
	{
	  for (i = 0; set->planesList[i]; i++)
	    visu_boxed_setBox(VISU_BOXED(set->planesList[i]), VISU_BOXED(visuData), TRUE);
	  /* Hide nodes if required. */
	  res = visu_plane_class_showHideAll(set->planesList, visuData);
	  if (res)
            g_signal_emit_by_name(G_OBJECT(visuData), "VisibilityChanged", NULL);
	}
    }
  /* Iso-surfaces arguments. */
  surfFile = commandLineGet_isoVisuSurfacesFileName();
  if (surfFile)
    {
      surfNb = 1;
      set->surfsList = g_malloc(sizeof(VisuSurfaces*) * (surfNb + 1));
      somethingIsLoaded = visu_surfaces_loadFile(surfFile, set->surfsList, error);
      if (!somethingIsLoaded)
	{
	  surfNb = 0;
	  return FALSE;
	}
      else if (commandLineGet_fitToBox())
	visu_boxed_setBox(VISU_BOXED(set->surfsList[0]), VISU_BOXED(visuData), TRUE);
      visu_surfaces_setShowAll(set->surfsList[0], TRUE);
      set->surfsList[surfNb] = (VisuSurfaces*)0;
    }
  /* Scalar-field arguments. */
  fieldFile = commandLineGet_scalarFieldFileName();
  if (fieldFile)
    {
      /* Create an option table for possible spin or complex
	 modifiers. */
      table = commandLineGet_options();
      /* Call the load method. */
      somethingIsLoaded = visu_scalar_field_new_fromFile(fieldFile, &set->fieldsList,
						   table, error);
      if (!somethingIsLoaded)
	{
	  surfNb = 0;
	  return FALSE;
	}
      else
	{
	  if (commandLineGet_fitToBox())
            for (tmplst = set->fieldsList; tmplst; tmplst = g_list_next(tmplst))
              visu_boxed_setBox(VISU_BOXED(tmplst->data), VISU_BOXED(visuData), FALSE);
	  nb             = g_list_length(set->fieldsList);
	  set->surfsList = g_malloc0(sizeof(VisuSurfaces*) * (nb + 1));
	  surfNb         = 0;
	  values         = commandLineGet_isoValues(&nb);
	  names          = commandLineGet_isoNames(&nb);
	  if (values)
	    {
	      tmplst = set->fieldsList;
	      while(tmplst)
		{
		  for (i = 0; i < nb; i++)
		    res = visu_surfaces_createFromScalarField(set->surfsList, (VisuScalarField*)tmplst->data,
					 values[i], i, names[i]);
		  visu_surfaces_setShowAll(set->surfsList[surfNb++], TRUE);
		  tmplst = g_list_next(tmplst);
		}
	    }
	  
	  /* We may add the iso-values from a file. */
	  if (valueFile)
	    {
	      somethingIsLoaded =
		visu_surfaces_parseXMLFile(valueFile, set->surfsList,
				      (VisuScalarField*)set->fieldsList->data, error);
	      if (!somethingIsLoaded)
		return FALSE;
	    }
	}
    }
  /* We apply the masking effect of planes, if necessary. */
  if (set->surfsList && set->planesList)
    for (i = 0; set->surfsList[i]; i++)
      visu_surfaces_hide(set->surfsList[i], set->planesList);
  /* The coloured map argument. */
  set->mapPlaneId = (gint*)commandLineGet_coloredMap();
  if (set->mapPlaneId)
    {
      if (!set->planesList)
	g_warning(_("option '--build-map' has been given but"
		    " no plane is available (use '--planes')."));
      if (!set->fieldsList)
	g_warning(_("option '--build-map' has been given but"
		    " no scalar field is available (use '--scalar-field')."));
      if (!set->shade)
	g_warning(_("option '--build-map' has been given but"
		    " no shade is available (use '--color-preset')."));
      set->logScale = commandLineGet_logScale();
      set->nIsolines = commandLineGet_nIsoLines();
      set->isoLinesColor = tool_color_new(commandLineGet_isoLinesColor());
      if (set->planesList && set->fieldsList && set->shade)
        {
          set->mapsList = g_malloc(sizeof(VisuMap*) * (set->mapPlaneId[0] + 1));
          for (i = 1; i <= set->mapPlaneId[0]; i++)
            {
              /* We create the maps. */
              set->mapsList[i - 1] =
                visu_map_newFromPlane(set->planesList[set->mapPlaneId[i]]);
              if (set->mapsList[i - 1])
                {
                  visu_map_setField(set->mapsList[i - 1],
                                    (VisuScalarField*)set->fieldsList->data,
                                    set->logScale, commandLineGet_mapMinMax());

                  /* We change the visibility of the selected planes. */
                  visu_plane_setRendered(set->planesList[set->mapPlaneId[i]], FALSE);
                }
            }
          set->mapsList[i - 1] = (VisuMap*)0;
        }
    }
  /* The bg image. */
  set->bgImage = commandLineGet_bgImage();
  /* The pick information. */
  if (valueFile)
    {
      if (!visu_gl_ext_marks_parseXMLFile((VisuGlExtMarks*)0, valueFile, &list, &mode, &info, error))
	return FALSE;
      nb = g_list_length(list) + 1;
      if (mode == DRAW_SELECTED)
	{
	  nodes = g_malloc(sizeof(int) * nb);
	  nodes[nb] = -1;
	  i = 0;
	  for (tmplst = list; tmplst; tmplst = g_list_next(tmplst))
	    {
	      if (GPOINTER_TO_INT(tmplst->data) == PICK_SELECTED)
		{
		  tmplst = g_list_next(tmplst);
		  nodes[i++] = GPOINTER_TO_INT(tmplst->data) - 1;
		}
	      else if (GPOINTER_TO_INT(tmplst->data) == PICK_REFERENCE_1)
		{
		  tmplst = g_list_next(tmplst);
		  /* We add the last selection. */
		  tmplst = g_list_next(tmplst);
		  nodes[i++] = GPOINTER_TO_INT(tmplst->data) - 1;
		}
	      else if (GPOINTER_TO_INT(tmplst->data) == PICK_HIGHLIGHT)
		tmplst = g_list_next(tmplst);
	    }
	  g_list_free(list);
	  nodes[i] = -1;
	}
      else
	nodes = (int*)0;
      if (mode != DRAW_NEVER)
	{
	  dt = (VisuDataNode*)0;
	  switch (info)
	    {
	    case 0:
	      method = EXT_DRAW_METH_ID; break;
	    case 1:
	      method = EXT_DRAW_METH_TYPE; break;
	    default:
	      method = EXT_DRAW_METH_OTHER;
	      i = 2;
	      for (tmplst = visu_data_node_class_getAll(); tmplst; tmplst = g_list_next(tmplst))
		{
		  if (visu_data_node_getUsed(VISU_DATA_NODE(tmplst->data), visuData))
		    {
		      if (i == (int)info)
			break;
		      i += 1;
		    }
		}
	      if (tmplst)
		dt = VISU_DATA_NODE(tmplst->data);
	      break;
	    }
	  visu_basic_setExtInfos(visuData, view, method, nodes, dt);
	}
    }

  return TRUE;
}
static void freeExtension(gpointer data)
{
  int i;
  GList *tmplst;
  VisuBasicCLISet *set;

  set = (VisuBasicCLISet*)data;
  /* Free allocated memory. */
  if (set->fieldsList)
    {
      for (tmplst = set->fieldsList; tmplst; tmplst = g_list_next(tmplst))
        g_object_unref(G_OBJECT(tmplst->data));
      g_list_free(set->fieldsList);
    }
  if (set->planesList)
    {
      for (i = 0; set->planesList[i]; i++)
	g_object_unref(G_OBJECT(set->planesList[i]));
      g_free(set->planesList);
    }
  if (set->surfsList)
    {
      for (i = 0; set->surfsList[i]; i++)
	g_object_unref(set->surfsList[i]);
      g_free(set->surfsList);
    }
  if (set->mapsList)
    {
      for (i = 0; set->mapsList[i]; i++)
	visu_map_free(set->mapsList[i]);
      g_free(set->mapsList);
    }
  g_free(set);
}

static void setToolFileFormatOption(gpointer key, gpointer value, gpointer data)
{
  ToolOption *prop;

  DBG_fprintf(stderr, "Visu Basic: transfer option '%s' to file format.\n",
              (gchar*)key);
  prop = tool_file_format_getPropertyByName(TOOL_FILE_FORMAT(data), (const gchar*)key);
  if (!prop)
    return;

  fprintf(stderr, "%p %p\n", (gpointer)tool_option_getValue((ToolOption*)value), (gpointer)tool_option_getValue(prop));
  g_value_copy(tool_option_getValue((ToolOption*)value), tool_option_getValue(prop));
}
/**
 * visu_basic_showOptionHelp:
 * @force: a boolean.
 *
 * Display a small help for some options. The output is different from
 * the -h command line options, here some details about running time
 * options is displayed like the available file format for
 * exportation... If @force is TRUE, all possible values are output,
 * otherwise only those relevant to the user provided command line
 * options.
 *
 * Since: 3.6
 *
 * Returns: TRUE if something is displayed.
 */
gboolean visu_basic_showOptionHelp(gboolean force)
{
  ToolFileFormatIter iter;
  GList *pnt;
  GHashTable *opts;
  ToolFileFormat *format;
  guint i;

  if (!force)
    {
      opts = commandLineGet_options();
      if (!opts || !g_hash_table_lookup(opts, "list"))
        return FALSE;
    }

  i = 1;
  for (pnt = visu_dump_getAllModules(); pnt; pnt = g_list_next(pnt))
    {
      format = TOOL_FILE_FORMAT(pnt->data);
      fprintf(stdout, _("\n#%2d - file format '%s':\n"),
              i++, tool_file_format_getName(format));
      iter.lst = (GList*)0;
      for (tool_file_format_iterNextProperty(format, &iter); iter.lst;
           tool_file_format_iterNextProperty(format, &iter))
	{
	  fprintf(stdout, " - '%25s'", iter.name);
	  switch (G_VALUE_TYPE(iter.val))
	    {
	    case G_TYPE_INT:
	      fprintf(stdout, " %10s (%5d): ", _("integer"), g_value_get_int(iter.val));
	      break;
	    case G_TYPE_BOOLEAN:
	      fprintf(stdout, " %10s (%5d): ", _("boolean"), g_value_get_boolean(iter.val));
	      break;
	    case G_TYPE_STRING:
	      fprintf(stdout, " %10s: ", _("string"));
	      break;
	    default:
              g_warning("Unknown type for file format property.");
	      break;
	    }
	  fprintf(stdout, "%s.\n", iter.label);
	}
      tool_file_format_iterNextProperty(format, &iter);
      if (!iter.lst)
	fprintf(stdout, _("No option for this file format.\n"));
    }

  return TRUE;
}

static gchar* setDir(const gchar* const *sysDirs, const gchar *prefix,
		     const gchar* subDir, const gchar* defaultDir)
{
  gchar *dir;
  int i;

  dir = g_build_filename(prefix, subDir, NULL);
  if (!g_file_test(dir, G_FILE_TEST_IS_DIR))
    {
      g_free(dir);
      dir = (gchar*)0;
      /* We try the XDG stuff. */
      for (i = 0; sysDirs[i]; i++)
	{
	  dir = g_build_filename(sysDirs[i], subDir, NULL);
	  if (g_file_test(dir, G_FILE_TEST_IS_DIR))
	    break;
	  else
	    g_free(dir);
	  dir = (gchar*)0;
	}
    }
  if (!dir)
    dir = g_strdup(defaultDir);
  return dir;
}

/**
 * visu_basic_setExePath:
 * @exePath: a path where the V_Sim executable is running in.
 *
 * This method is used to tell V_Sim where V_Sim is running (usually
 * reading from argv[0]. This makes it possible to relocate everything
 * on the fly. @exePath is copied.
 *
 * Since: 3.6
 */
void visu_basic_setExePath(const gchar *exePath)
{
  if (exeLocation)
    g_free(exeLocation);

  exeLocation = g_strdup(exePath);
}
/**
 * setVisuPaths:
 * 
 * This method sets the paths. On Unix systems, this method sets the paths
 * from macros defined by configure. On Win32 systems, it reads paths in
 * a v_sim.ini file found in the current directory or in the C:\windows.
 */
static void setVisuPaths(void)
{
#if SYSTEM_WIN32 == 1
  #define V_SIM_INI_FILE PACKAGE".ini"
  GIOChannel *iniFile;
  gchar *iniPath, *tmp, *me, *prefix;
  gchar *buffer, *line;
  GIOStatus res;
  GError *err;
  gchar **tokens;
  gsize length;

  if (!exeLocation)
    exeLocation = g_strdup(PACKAGE_TARNAME);

  v_sim_data_dir    = g_strdup(V_SIM_DATA_DIR_DEFAULT);
  v_sim_legal_dir   = g_strdup(V_SIM_LEGAL_DIR_DEFAULT);
  v_sim_pixmaps_dir = g_strdup(V_SIM_PIXMAPS_DIR_DEFAULT);
  v_sim_icons_dir   = g_strdup(V_SIM_ICONS_DIR_DEFAULT);
  v_sim_plugins_dir = g_strdup(V_SIM_PLUGINS_DIR_DEFAULT);
  v_sim_locale_dir  = g_strdup(V_SIM_LOCALE_DIR_DEFAULT);
  iniPath = g_strdup(V_SIM_INI_FILE);
  /* Try to find the INI file from cwd. */
  prefix = (gchar*)0;
  iniFile = g_io_channel_new_file(iniPath, "r", (GError**)0);
  if (iniFile)
    prefix = g_get_current_dir();
  /* Try to find the INI file from the name of the executable. */
  if (!iniFile)
    {
      g_free(iniPath);

      if (g_file_test(exeLocation, G_FILE_TEST_IS_SYMLINK))
	tmp = g_file_read_link(exeLocation, (GError**)0);
      else
	tmp = g_strdup(exeLocation);
      me = tool_path_normalize(tmp);
      g_free(tmp);
    
      DBG_fprintf(stderr, "Visu Basic: running program is '%s'.\n", me);
      /* From the location of the executable, we take the base dir. */
      prefix = g_path_get_dirname(me);
      g_free(me);

      iniPath = g_build_filename(prefix, V_SIM_INI_FILE, NULL);
      iniFile = g_io_channel_new_file(iniPath, "r", (GError**)0);
    }  
  /* Try to find the INI file in the Windows directory. */
  if (!iniFile)
    {
      g_free(iniPath);
      prefix = g_strdup("C:\\WINDOWS");
      iniPath = g_build_filename(prefix, V_SIM_INI_FILE, NULL);
      iniFile = g_io_channel_new_file(iniPath, "r", (GError**)0);
    }
  if (iniFile)
    {
      buffer = (gchar*)0;
      err = (GError*)0;
      do
	{
	  res = g_io_channel_read_line(iniFile, &line, &length, NULL, &err);
	  if (line && res == G_IO_STATUS_NORMAL)
	    {
	      tokens = g_strsplit(line, "=", 2);
	      if (!strcmp(g_strstrip(tokens[0]), "data_dir") && tokens[1])
		{
		  g_free(v_sim_data_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_data_dir = g_strdup(tmp);
		  else
		    v_sim_data_dir = g_build_filename(prefix, tmp, NULL);
		}
	      if (!strcmp(g_strstrip(tokens[0]), "legal_dir") && tokens[1])
		{
		  g_free(v_sim_legal_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_legal_dir = g_strdup(tmp);
		  else
		    v_sim_legal_dir = g_build_filename(prefix, tmp, NULL);
		}
	      if (!strcmp(g_strstrip(tokens[0]), "pixmaps_dir") && tokens[1])
		{
		  g_free(v_sim_pixmaps_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_pixmaps_dir = g_strdup(tmp);
		  else
		    v_sim_pixmaps_dir = g_build_filename(prefix, tmp, NULL);
		}
	      if (!strcmp(g_strstrip(tokens[0]), "icons_dir") && tokens[1])
		{
		  g_free(v_sim_icons_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_icons_dir = g_strdup(tmp);
		  else
		    v_sim_icons_dir = g_build_filename(prefix, tmp, NULL);
		}
	      if (!strcmp(g_strstrip(tokens[0]), "plugins_dir") && tokens[1])
		{
		  g_free(v_sim_plugins_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_plugins_dir = g_strdup(tmp);
		  else
		    v_sim_plugins_dir = g_build_filename(prefix, tmp, NULL);
		}
	      if (!strcmp(g_strstrip(tokens[0]), "locale_dir") && tokens[1])
		{
		  g_free(v_sim_locale_dir);
		  tmp = g_strstrip(tokens[1]);
		  if (g_path_is_absolute(tmp))
		    v_sim_locale_dir = g_strdup(tmp);
		  else
		    v_sim_locale_dir = g_build_filename(prefix, tmp, NULL);
		}
	      g_strfreev(tokens);
	      g_free(line);
	    }
	}
      while (res != G_IO_STATUS_EOF);
      g_io_channel_shutdown (iniFile, FALSE, (GError**)0);
      g_io_channel_unref (iniFile);
    }
  g_free(iniPath);
#endif

#if SYSTEM_X11 == 1
  const gchar* const *sysDirs;
  gchar *me, *prefix, *tmp;
#if DEBUG == 1
  int i;
#endif

  /* We try to get the dirs from XDG specs, otherwise we fall back to
     hard coded values at compilation time. */
#if GLIB_MINOR_VERSION > 5
  sysDirs = g_get_system_data_dirs();
#if DEBUG == 1
  fprintf(stderr, "Visu Basic: available data dirs:\n");
  for (i = 0; sysDirs[i]; i++)
    fprintf(stderr, " | '%s'.\n", sysDirs[i]);
#endif
#else
  sysDirs = g_malloc(sizeof(const gchar*));
  sysDirs[0] = (const gchar*)0;
#endif

  if (!exeLocation)
    exeLocation = g_strdup(PACKAGE_TARNAME);
  if (g_file_test(exeLocation, G_FILE_TEST_IS_SYMLINK))
    tmp = g_file_read_link(exeLocation, (GError**)0);
  else
    tmp = g_strdup(exeLocation);
  me = tool_path_normalize(tmp);
  g_free(tmp);
    
  DBG_fprintf(stderr, "Visu Basic: running program is '%s'.\n", me);
  /* From the location of the executable, we take the base dir
     or its parents if the basedir is bin. */
  prefix = g_path_get_dirname(me);
  g_free(me);
  me = g_path_get_basename(prefix);
  if (!strcmp(me, "bin"))
    {
      g_free(me);
      me = prefix;
      prefix = g_path_get_dirname(me);
    }
  g_free(me);
  DBG_fprintf(stderr, " | prefix is '%s'.\n", prefix);

  v_sim_data_dir = setDir(sysDirs, prefix, "share/" PACKAGE, DATA_DIR);
  v_sim_legal_dir = setDir(sysDirs, prefix, "share/doc/" PACKAGE, LEGAL_DIR);
  v_sim_pixmaps_dir = setDir(sysDirs, prefix, "share/" PACKAGE "/pixmaps", PIXMAPS_DIR);
  v_sim_icons_dir = setDir(sysDirs, prefix, "share/icons", ICONS_DIR);
  v_sim_plugins_dir = setDir(sysDirs, prefix, "lib/" PACKAGE "/plug-ins", PLUGINS_DIR);
  v_sim_locale_dir = setDir(sysDirs, prefix, "share/locale", LOCALE_DIR);

  g_free(prefix);
#endif

  /* Create the local dirs. */
#if GLIB_MINOR_VERSION > 5
  v_sim_local_conf_dir = g_build_filename(g_get_user_config_dir(), "v_sim", NULL);
#else
  v_sim_local_conf_dir = g_build_filename(g_get_home_dir(), ".config/v_sim", NULL);
#endif
  if (!v_sim_local_conf_dir)
    g_warning("WARNING! Impossible to get the default"
	      " path $XDG_CONFIG_HOME/v_sim.\n");
  v_sim_old_local_conf_dir = g_build_filename(g_get_home_dir(), ".v_sim", NULL);

  DBG_fprintf(stderr, "Visu Basic: data directory      : '%s'.\n", v_sim_data_dir);
  DBG_fprintf(stderr, "Visu Basic: local conf directory: '%s'.\n", v_sim_local_conf_dir);
  DBG_fprintf(stderr, "Visu Basic: legal directory     : '%s'.\n", v_sim_legal_dir);
  DBG_fprintf(stderr, "Visu Basic: pixmaps directory   : '%s'.\n", v_sim_pixmaps_dir);
  DBG_fprintf(stderr, "Visu Basic: icons directory     : '%s'.\n", v_sim_icons_dir);
  DBG_fprintf(stderr, "Visu Basic: plug-ins directory  : '%s'.\n", v_sim_plugins_dir);
  DBG_fprintf(stderr, "Visu Basic: locale directory    : '%s'.\n", v_sim_locale_dir);
}
/**
 * visu_basic_getDataDir:
 *
 * Get the static string where V_Sim looks for its data files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getDataDir(void)
{
  if (!v_sim_data_dir)
    setVisuPaths();

  return v_sim_data_dir;
}
/**
 * visu_basic_getLegalDir:
 *
 * Get the static string where V_Sim looks for its legal files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getLegalDir(void)
{
  if (!v_sim_legal_dir)
    setVisuPaths();

  return v_sim_legal_dir;
}
/**
 * visu_basic_getPixmapsDir:
 *
 * Get the static string where V_Sim looks for its pixmap files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getPixmapsDir(void)
{
  if (!v_sim_pixmaps_dir)
    setVisuPaths();

  return v_sim_pixmaps_dir;
}
/**
 * visu_basic_getIconsDir:
 *
 * Get the static string where V_Sim looks for its icon files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getIconsDir(void)
{
  if (!v_sim_icons_dir)
    setVisuPaths();

  return v_sim_icons_dir;
}
/**
 * visu_basic_getLocalDir:
 *
 * Get the static string where V_Sim looks for its user configuration files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getLocalDir(void)
{
  if (!v_sim_local_conf_dir)
    setVisuPaths();

  return v_sim_local_conf_dir;
}
/**
 * visu_basic_getOldLocalDir:
 *
 * Get the static string where V_Sim looks for its user configuration
 * files (old location).
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getOldLocalDir(void)
{
  if (!v_sim_old_local_conf_dir)
    setVisuPaths();

  return v_sim_old_local_conf_dir;
}
/**
 * visu_basic_getPluginsDir:
 *
 * Get the static string where V_Sim looks for its plug-in files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getPluginsDir(void)
{
  if (!v_sim_plugins_dir)
    setVisuPaths();

  return v_sim_plugins_dir;
}
/**
 * visu_basic_getLocaleDir:
 *
 * Get the static string where V_Sim looks for its localisation files.
 *
 * Since: 3.4
 *
 * Returns: (transfer none): a string owned by V_Sim.
 */
const gchar* visu_basic_getLocaleDir(void)
{
  if (!v_sim_locale_dir)
    setVisuPaths();

  return v_sim_locale_dir;
}
/**
 * visu_basic_freeAll:
 *
 * This routine is called by V_Sim when quiting and it frees the memory
 * used by visu_basic.
 *
 * Since: 3.5
 */
void visu_basic_freeAll(void)
{
  DBG_fprintf(stderr, "Visu Basic: free all.\n - the paths\n");
  g_free(v_sim_data_dir);
  g_free(v_sim_legal_dir);
  g_free(v_sim_pixmaps_dir);
  g_free(v_sim_plugins_dir);
  g_free(v_sim_locale_dir);
  g_free(v_sim_local_conf_dir);
  g_free(v_sim_old_local_conf_dir);

  DBG_fprintf(stderr, " - the color storage\n");
  tool_color_freeAll();

  g_object_unref(visu_gl_ext_axes_getDefault());
  g_object_unref(visu_gl_ext_box_getDefault());
  g_object_unref(visu_gl_ext_box_legend_getDefault());
  g_object_unref(visu_gl_ext_legend_getDefault());
  g_object_unref(visu_gl_ext_pairs_getDefault());
  g_object_unref(visu_gl_ext_planes_getDefault());
  g_object_unref(visu_gl_ext_surfaces_getDefault());
}
/**
 * visu_basic_getMainContext:
 *
 * Even without GUI, V_Sim requires to run a main loop. This method is
 * to get the main loop.
 *
 * Since: 3.6
 *
 * Returns: (transfer none): the main loop, as defined in GLib.
 */
GMainContext* visu_basic_getMainContext(void)
{
  return g_main_context_default();
}


/* Resources. */
/**
 * visu_basic_getPreferedUnit:
 *
 * By setting the prefered unit, when a file is load, V_Sim tries to
 * render it in this prefered unit.
 *
 * Since: 3.5
 *
 * Returns: the prefered unit set by the user (default is
 * #TOOL_UNITS_UNDEFINED).
 */
ToolUnits visu_basic_getPreferedUnit(void)
{
  return preferedUnit;
}
/**
 * visu_basic_setPreferedUnit:
 * @unit: a #ToolUnits value.
 *
 * By setting the prefered unit, when a file is load, V_Sim tries to
 * render it in this prefered unit.
 *
 * Since: 3.5
 *
 * Returns: TRUE if the prefered unit is actually changed.
 */
gboolean visu_basic_setPreferedUnit(ToolUnits unit)
{
  if (unit == preferedUnit)
    return FALSE;

  preferedUnit = unit;
  return TRUE;
}
static gboolean readUnit(VisuConfigFileEntry *entry _U_, gchar **lines, int nbLines, int position,
			 VisuData *dataObj _U_, VisuGlView *view _U_, GError **error)
{
  gchar **tokens;
  ToolUnits unit;

  g_return_val_if_fail(nbLines == 1, FALSE);

  if (!tool_config_file_readString(lines[0], position, &tokens, 1, FALSE, error))
    return FALSE;

  unit = tool_physic_getUnitFromName(tokens[0]);
  if (unit == TOOL_UNITS_UNDEFINED)
    {
      *error = g_error_new(TOOL_CONFIG_FILE_ERROR, TOOL_CONFIG_FILE_ERROR_VALUE,
			   _("Parse error at line %d: unit '%s' is unknown.\n"),
			   position, tokens[0]);
      g_strfreev(tokens);
      return FALSE;
    }
  g_strfreev(tokens);

  preferedUnit = unit;
  return TRUE;
}
static void exportParameters(GString *data, VisuData *dataObj _U_, VisuGlView *view _U_)
{
  const gchar **units;

  if (preferedUnit != TOOL_UNITS_UNDEFINED)
    {
      units = tool_physic_getUnitNames();

      g_string_append_printf(data, "# %s\n", DESC_PARAMETER_UNIT);
      g_string_append_printf(data, "%s: %s\n\n", FLAG_PARAMETER_UNIT,
			     units[preferedUnit]);
    }
}
