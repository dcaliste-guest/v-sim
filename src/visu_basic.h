/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_BASIC_H
#define VISU_BASIC_H

#include <glib.h>
#include "visu_data.h"
#include "visu_tools.h"
#include "coreTools/toolFileFormat.h"
#include "coreTools/toolOptions.h"
#include "coreTools/toolShade.h"
#include "coreTools/toolPhysic.h"
#include "extraFunctions/plane.h"
#include "extraFunctions/surfaces.h"
#include "extraFunctions/scalarFields.h"
#include "extraFunctions/map.h"
#include "extensions/infos.h"

G_BEGIN_DECLS

/**
 * VisuBasicCLISet:
 * @colorFile: TODO
 * @planesList: TODO
 * @surfsList: TODO
 * @fieldsList: TODO
 * @mapsList: TODO
 * @shade: TODO
 * @mapPlaneId: TODO
 * @logScale: TODO
 * @nIsolines: TODO
 * @isoLinesColor: TODO
 * @bgImage: TODO
 *
 * Internal object to store values read from command line.
 */
typedef struct _VisuBasicCLISet VisuBasicCLISet;

struct _VisuBasicCLISet
{
  gchar *colorFile;

  VisuPlane **planesList;

  VisuSurfaces **surfsList;
  GList *fieldsList;

  VisuMap **mapsList;

  ToolShade *shade;

  gint *mapPlaneId;
  ToolMatrixScalingFlag logScale;
  guint nIsolines;
  ToolColor* isoLinesColor;

  gchar *bgImage;
};

void visu_basic_init(void);

/* Common parse routines. */
gchar* visu_basic_parseConfigFiles(VisuGlView *view);
gboolean visu_basic_applyCommandLine(VisuData *data, VisuGlView *view,
                                     VisuBasicCLISet *set, GError **error);
gboolean visu_basic_showOptionHelp(gboolean force);

/* Export routines. */
void visu_basic_createExtensions(VisuData *data, VisuGlView *view,
                                 VisuBasicCLISet *set, gboolean rebuild);
VisuData* visu_basic_setDataFromCommandLine(void);
void visu_basic_setExtInfos(VisuData *dataObj, VisuGlView *view,
                            VisuGlExtInfosDrawMethod method,
                            int *nodes, VisuDataNode *data);
int visu_basic_mainExport(void);

/* Some resources. */
ToolUnits visu_basic_getPreferedUnit(void);
gboolean visu_basic_setPreferedUnit(ToolUnits unit);

/**
 * VISU_VERSION:
 *
 * The value of current compiled version.
 */
#define VISU_VERSION       VERSION
/**
 * VISU_WEB_SITE:
 *
 * URL where to find info on V_Sim.
 */
#define VISU_WEB_SITE      "http://inac.cea.fr/L_Sim/V_Sim"

/* Main paths. */
void visu_basic_setExePath(const gchar *exePath);
/**
 * V_SIM_DATA_DIR:
 *
 * The directory where data files are stored.
 */
#define V_SIM_DATA_DIR           visu_basic_getDataDir()
/**
 * V_SIM_LEGAL_DIR:
 *
 * The directory where copyright and author files are stored.
 */
#define V_SIM_LEGAL_DIR          visu_basic_getLegalDir()
/**
 * V_SIM_PIXMAPS_DIR:
 *
 * The directory where pixmap files are stored.
 */
#define V_SIM_PIXMAPS_DIR        visu_basic_getPixmapsDir()
/**
 * V_SIM_ICONS_DIR:
 *
 * The directory where icon files are stored.
 */
#define V_SIM_ICONS_DIR          visu_basic_getIconsDir()
/**
 * V_SIM_LOCAL_CONF_DIR:
 *
 * The directory where user configuration files are stored.
 */
#define V_SIM_LOCAL_CONF_DIR     visu_basic_getLocalDir()
/**
 * V_SIM_OLD_LOCAL_CONF_DIR:
 *
 * Old value of #V_SIM_LOCAL_CONF_DIR.
 */
#define V_SIM_OLD_LOCAL_CONF_DIR visu_basic_getOldLocalDir()
/**
 * V_SIM_PLUGINS_DIR:
 *
 * The directory where plugin files are stored.
 */
#define V_SIM_PLUGINS_DIR        visu_basic_getPluginsDir()
/**
 * V_SIM_LOCALE_DIR:
 *
 * The directory where locale files are stored.
 */
#define V_SIM_LOCALE_DIR         visu_basic_getLocaleDir()
const gchar* visu_basic_getDataDir(void);
const gchar* visu_basic_getLegalDir(void);
const gchar* visu_basic_getPixmapsDir(void);
const gchar* visu_basic_getIconsDir(void);
const gchar* visu_basic_getLocalDir(void);
const gchar* visu_basic_getOldLocalDir(void);
const gchar* visu_basic_getPluginsDir(void);
const gchar* visu_basic_getLocaleDir(void);

/* Miscellaneous. */
GMainContext* visu_basic_getMainContext(void);
void visu_basic_freeAll(void);

G_END_DECLS

#endif
