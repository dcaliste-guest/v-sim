/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_BOX_H
#define VISU_BOX_H

#include <glib.h>
#include <glib-object.h>

#include "coreTools/toolPhysic.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_BOX:
 *
 * return the type of #VisuBox.
 */
#define VISU_TYPE_BOX	     (visu_box_get_type ())
/**
 * VISU_BOX:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuBox type.
 */
#define VISU_BOX(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_BOX, VisuBox))
/**
 * VISU_BOX_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuBoxClass.
 */
#define VISU_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_BOX, VisuBoxClass))
/**
 * VISU_IS_BOX:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuBox object.
 */
#define VISU_IS_BOX(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_BOX))
/**
 * VISU_IS_BOX_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuBoxClass class.
 */
#define VISU_IS_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_BOX))
/**
 * VISU_BOX_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_BOX, VisuBoxClass))

typedef struct _VisuBoxClass VisuBoxClass;
typedef struct _VisuBox VisuBox;
typedef struct _VisuBoxPrivate VisuBoxPrivate;

/**
 * visu_box_get_type:
 *
 * This method returns the type of #VisuBox, use VISU_TYPE_BOX instead.
 *
 * Returns: the type of #VisuBox.
 */
GType visu_box_get_type(void);

/**
 * _VisuBox:
 * @parent: parent.
 * @priv: private data.
 *
 * This structure describes a box.
 */

struct _VisuBox
{
  GObject parent;

  VisuBoxPrivate *priv;
};

struct _VisuBoxClass
{
  GObjectClass parent;
};

/**
 * VisuBoxVector:
 * @VISU_BOX_DXX: x box vector along X;
 * @VISU_BOX_DYX: y box vector along X;
 * @VISU_BOX_DYY: y box vector along Y;
 * @VISU_BOX_DZX: z box vector along X;
 * @VISU_BOX_DZY: z box vector along Y;
 * @VISU_BOX_DZZ: z box vector along Z;
 * @VISU_BOX_N_VECTORS: number of elements.
 *
 * Identifier of every projection of the box super-cell on an
 * orthogonal basis-set.
 *
 * Since: 3.7
 **/
typedef enum
  {
    VISU_BOX_DXX,
    VISU_BOX_DYX,
    VISU_BOX_DYY,
    VISU_BOX_DZX,
    VISU_BOX_DZY,
    VISU_BOX_DZZ,
    VISU_BOX_N_VECTORS
  } VisuBoxVector;

/**
 * VisuBoxBoundaries:
 * @VISU_BOX_PERIODIC: the full 3D periodicity ;
 * @VISU_BOX_SURFACE_XY: the Z axis is a free axis ;
 * @VISU_BOX_SURFACE_YZ: the X axis is a free axis ;
 * @VISU_BOX_SURFACE_ZX: the Y axis is a free axis ;
 * @VISU_BOX_WIRE_X: the periodicity is along X axis only ;
 * @VISU_BOX_WIRE_Y: the periodicity is along Y axis only ;
 * @VISU_BOX_WIRE_Z: the periodicity is along Z axis only ;
 * @VISU_BOX_FREE: the system is isolated.
 *
 * This describes the periodicity of the bounding box in the three directions.
 */
typedef enum
  {
    VISU_BOX_PERIODIC,
    VISU_BOX_SURFACE_XY,
    VISU_BOX_SURFACE_YZ,
    VISU_BOX_SURFACE_ZX,
    VISU_BOX_WIRE_X,
    VISU_BOX_WIRE_Y,
    VISU_BOX_WIRE_Z,
    VISU_BOX_FREE
  } VisuBoxBoundaries;

/* These structures are used for bindings. */
typedef struct _VisuBoxVertices VisuBoxVertices;
struct _VisuBoxVertices
{
  float vertices[8][3];
};
typedef struct _VisuBoxCell VisuBoxCell;
struct _VisuBoxCell
{
  double box[VISU_BOX_N_VECTORS];
};

VisuBox* visu_box_new(double geometry[VISU_BOX_N_VECTORS],
                      VisuBoxBoundaries bc);
VisuBox* visu_box_new_full(double full[3][3], VisuBoxBoundaries bc);

void visu_box_convertFullToCell(VisuBox *box, float cell[3], float full[3]);
void visu_box_convertXYZtoBoxCoordinates(VisuBox *box, float boxCoord[3], float xyz[3]);
void visu_box_convertXYZToReduced(VisuBox *box, GArray *xyz,
                                  float *u, float *v, float *w);
void visu_box_convertBoxCoordinatestoXYZ(VisuBox *box, float xyz[3], float boxCoord[3]);
void visu_box_convertReducedToXYZ(VisuBox *box, GArray *red,
                                  float *x, float *y, float *z);

gboolean visu_box_constrainInside(VisuBox *box, float translat[3], float xyz[3],
                                  gboolean withExt);

void visu_box_getInvMatrix(VisuBox *box, double matrix[3][3]);
void visu_box_getCellMatrix(VisuBox *box, double matrix[3][3]);
void visu_box_getCellMatrixv(VisuBox *box, double *m11, double *m12, double *m13,
                             double *m21, double *m22, double *m23,
                             double *m31, double *m32, double *m33);
void visu_box_getVertices(VisuBox *box, float v[8][3],
                          gboolean withExtension);
void visu_box_getExtension(const VisuBox *boxObj, float extension[3]);
VisuBoxBoundaries visu_box_getBoundary(VisuBox *box);
void visu_box_getPeriodicity(VisuBox *box, gboolean per[3]);
double visu_box_getGeometry(VisuBox *box, VisuBoxVector vector);
void visu_box_getCentre(VisuBox *box, float centre[3]);
float visu_box_getGlobalSize(VisuBox *box, gboolean withExt);
ToolUnits visu_box_getUnit(VisuBox *box);
void visu_box_getPeriodicVector(VisuBox *box, float vect[3]);
void visu_box_getPeriodicVectorv(VisuBox *box, float *x, float *y, float *z,
                                  float vect[3]);
void visu_box_getPeriodicArray(VisuBox *box, float *array, guint nEle);

gboolean visu_box_setBoundary(VisuBox *box, VisuBoxBoundaries bc);
gboolean visu_box_setGeometry(VisuBox *box, double geometry[VISU_BOX_N_VECTORS]);
gboolean visu_box_setGeometryFull(VisuBox *box, double full[3][3]);
gboolean visu_box_setMargin(VisuBox *box, gfloat margin, gboolean emit);
gboolean visu_box_setExtension(VisuBox *boxObj, float extension[3]);
gboolean visu_box_setUnit(VisuBox *box, ToolUnits unit);

G_END_DECLS

#endif
