/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VISU_CONFIG_FILE_H
#define VISU_CONFIG_FILE_H

#include <glib.h>
#include <glib-object.h>
#include <stdarg.h>
#include "visu_data.h"
#include "openGLFunctions/view.h"

typedef struct _VisuConfigFileEntry VisuConfigFileEntry;

/**
 * VISU_CONFIG_FILE_PARAMETER:
 *
 * This defines a parameter entry in the config files.
 */
#define VISU_CONFIG_FILE_PARAMETER 0
/**
 * VISU_CONFIG_FILE_RESOURCE:
 *
 * This defines a resource entry in the config files.
 */
#define VISU_CONFIG_FILE_RESOURCE 1

/**
 * VisuConfigFileReadFunc:
 * @entry: the #VisuConfigFileEntry that raises this callback.
 * @lines: an array of strings ;
 * @nbLines: an integer ;
 * @position: an integer ;
 * @dataObj: (allow-none): a #VisuData object ;
 * @view: (allow-none): a #VisuGlView object.
 * @error: a pointer to a GError pointer.
 *
 * This prototype corresponds to methods called when an entry is
 * found. The @lines argument is an array of lines read from the files.
 * These strings are copies and can be modified but not freed. There are
 * @nbLines and this value correspond to the number of lines defined
 * for the entry. The @error argument is used to store some text
 * messages and error ids. They should be in UTF8. The @error argument
 * must be initialised with (GError*)0. The @position argument give the number
 * of the first line given in @lines argument. If the @dataObj argument is not null,
 * some updates should be done with read values.
 *
 * Returns: TRUE if everything goes right, FALSE otherwise.
 */
typedef gboolean (*VisuConfigFileReadFunc)(VisuConfigFileEntry *entry,
                                           gchar **lines, int nbLines,
					   int position, VisuData *dataObj,
					   VisuGlView *view, GError **error);
/**
 * VisuConfigFileExportFunc:
 * @data: an empty GString to store the export ;
 * @dataObj: (allow-none): a #VisuData object ;
 * @view: (allow-none): a #VisuGlView object.
 *
 * This prototype defines a method that is used to export some resources
 * or parameters. The @data argument is an empty GString where the export has
 * to be written. If the argument @dataObj is not null, only resources related
 * to the #VisuData object should be exported (for parameters files, @dataObj is
 * always NULL).
 */
typedef void (*VisuConfigFileExportFunc)(GString *data,
                                         VisuData* dataObj, VisuGlView *view);
/**
 * VisuConfigFileForeachFuncExport:
 * @data: the string where the values are exported to ;
 * @dataObj: the current #VisuData object, values are related to.
 *
 * This structure can be used to encapsulate the arguments of an export method
 * when used in a foreach glib loop.
 */
struct _VisuConfigFileForeachFuncExport
{
  GString *data;
  VisuData *dataObj;
};

GType visu_config_file_entry_get_type(void);
#define VISU_TYPE_CONFIG_FILE_ENTRY (visu_config_file_entry_get_type())

VisuConfigFileEntry* visu_config_file_addEntry(int kind, const gchar *key,
                                               const gchar* description, int nbLines,
                                               VisuConfigFileReadFunc readFunc);
VisuConfigFileEntry* visu_config_file_addBooleanEntry(int kind, const gchar *key,
                                                      const gchar* description,
                                                      gboolean *location);
VisuConfigFileEntry* visu_config_file_addFloatArrayEntry(int kind, const gchar *key,
                                                         const gchar* description,
                                                         guint nValues, float *location,
                                                         float clamp[2]);
VisuConfigFileEntry* visu_config_file_addStringEntry(int kind, const gchar *key,
                                                     const gchar* description,
                                                     gchar **location);
void visu_config_file_addExportFunction(int kind, VisuConfigFileExportFunc writeFunc);

void visu_config_file_entry_setTag(VisuConfigFileEntry *entry, const gchar *tag);
void visu_config_file_entry_setVersion(VisuConfigFileEntry *entry, float version);
void visu_config_file_entry_setReplace(VisuConfigFileEntry *newEntry,
			       VisuConfigFileEntry *oldEntry);

gboolean visu_config_file_load(int kind, const char* filename,
                               VisuData *dataObj, VisuGlView *view, GError **error);
gboolean visu_config_file_save(int kind, const char* fileName, int *lines,
                               VisuData *dataObj, VisuGlView *view, GError **error);
gboolean visu_config_file_saveResourcesToXML(const char* filename, int *lines,
                                             VisuData *dataObj, VisuGlView *view,
                                             GError **error);

void visu_config_file_addKnownTag(gchar* tag);

void visu_config_file_exportComment(GString *buffer, const gchar *comment);
void visu_config_file_exportEntry(GString *buffer, const gchar *name,
                                  const gchar *id_value, const gchar *format_, ...);

gchar* visu_config_file_getValidPath(int kind, int mode, int utf8);
gchar* visu_config_file_getNextValidPath(int kind, int accessMode, GList **list, int utf8);
const gchar* visu_config_file_getDefaultFilename(int kind);
GList* visu_config_file_getPathList(int kind);


GList* visu_config_file_getEntries(int kind);
gboolean visu_config_file_exportToXML(const gchar *filename, int kind, GError **error);

const gchar* visu_config_file_getPathToResources();

#endif
