/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_data.h"


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "visu_object.h"
#include "extraFunctions/dataNode.h"
#include "extraFunctions/geometry.h"
#include "coreTools/toolMatrix.h"

/**
 * SECTION:visu_data
 * @short_description: Give methods to store and manage data from
 * input file(s).
 * 
 * <para>The main goal of V_Sim is to draw lists of elements. For
 * example, when used to render atoms, a box that contains 24 silicon
 * atoms and 46 germanium atoms is a box with two elements (silicon
 * and germanium) where the silicon element has 24 nodes and the
 * germanium element has 46 nodes. This module gives then methods to
 * create nodes (see #VisuElement to create and managed
 * elements).</para>
 *
 * <para>All nodes are stored in a structure called #VisuNodes and
 * #VisuNodes is encapsulated in a #VisuData for all not-node related
 * information. V_Sim uses one #VisuData per input file(s). This
 * structure contains a list of pointers on all the #VisuElement used
 * in this file.</para>
 *
 * <para>To iterate on nodes, one should use the provided iterators
 * (see #VisuNodeArrayIter) methods, like visu_node_array_iter_next().</para>
 */

struct FileDescription_struct
{
  /* Identity. */
  int kind;

  /* The name of a file. */
  gchar *name;

  /* The format of the file.
     This format can be null, if the file has not been
     parsed yet and the format is unknown.
     If the file file is not parsed but the format is
     set, then it is just an indication and may not
     be the right format. */
  ToolFileFormat *format;
};

enum {
  FILES_CHANGED_SIGNAL,
  TRANSLATIONS_CHANGED_SIGNAL,
  VISU_DATA_FREED_SIGNAL,
  LAST_SIGNAL
};

enum
  {
    PROP_0,
    TOTAL_ENERGY_PROP
  };

/* Define a key for a VisuDataNode value. */
#define COORDINATES_ID     "visu_data_coordinates"

/* Local variables. */
static VisuDataNode *dataNodeCoord;

/**
 * VisuData:
 *
 * Opaque structure for #VisuData objects.
 */

/**
 * VisuDataPrivate:
 *
 * Private fields for #VisuData objects.
 */
struct _VisuDataPrivate
{
  gboolean dispose_has_run;

  /*******************/
  /* Drawing Methods */
  /*******************/
  /* This function is used to scale the nodes before drawing them. */
  VisuDataScalingFunc scaling;
  /* Define a method to set the color of each node.
     If this method is NULL, the color of the element is used. */
  VisuDataColorFunc setColor;

  /********************/
  /* Files attributes */
  /********************/
  /* Files that correspond to that VisuData.
     They are identified by a kind which is an integer.
     The list is pointers to FileDescription_struct. */
  GList *files;
  /* Commentary associated to the rendered data, this commentary can
     be different for each set of nodes. */
  gchar** commentary;
  /* The number of set of nodes in one file, and the currently loaded one. */
  int nSets, iSet;

  /******************/
  /* Box attributes */
  /******************/
  VisuBox *box;
  gulong unit_signal;
  /* Translation applied to all nodes when rendered. */
  gboolean translationApply;
  float translation[3];

  /********************/
  /* Misc. attributes */
  /********************/
  /* This list contains pointers on source id, to be removed when the object
     is finalized. */
  GList *timeoutList;
  /* The total energy of the system in eV. */
  gdouble totalEnergy;
};


static void visu_data_dispose     (GObject* obj);
static void visu_data_finalize    (GObject* obj);
static void visu_data_get_property(GObject* obj, guint property_id,
				   GValue *value, GParamSpec *pspec);
static void visu_data_set_property(GObject* obj, guint property_id,
				   const GValue *value, GParamSpec *pspec);
static void visu_boxed_interface_init(VisuBoxedInterface *iface);

/* Local callbacks. */
static void onBoxUnitChanged(VisuBox *box, gfloat fact, gpointer user_data);

/* Local routines. */
static VisuBox* visu_data_getBox(VisuBoxed *self);
static gboolean visu_data_setBox(VisuBoxed *self, VisuBox *box, gboolean update);
static gboolean setCoordFromString(VisuDataNode *dataNode, VisuData *dataObj,
				   VisuNode *node, gchar* labelIn,
				   gchar **labelOut, gboolean *modify);
static gchar* getCoordFromString(VisuDataNode *dataNode, VisuData *dataObj,
				 VisuNode *node);
static int* shrinkNodeList(VisuData *data, int coord, float valueTo);
static int* extendNodeList(VisuData *data, int coord, float valueFrom, float valueTo);
static float defaultScaling(VisuData *data, VisuNode *node);

static guint visu_data_signals[LAST_SIGNAL] = { 0 };
static GList* allObjects;

G_DEFINE_TYPE_WITH_CODE(VisuData, visu_data, VISU_TYPE_NODE_ARRAY,
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_BOXED,
                                              visu_boxed_interface_init))

static void visu_data_class_init(VisuDataClass *klass)
{
  DBG_fprintf(stderr, "Visu Data: creating the class of the object.\n");
  DBG_fprintf(stderr, "                - adding new signals ;\n");
  /**
   * VisuData::objectFreed:
   * @dataObj: the object which received the signal ;
   *
   * Gets emitted when the object is been destroyed. All external
   * objects having a reference on this #VisuData should clean it.
   *
   * Since: 3.3
   */
  visu_data_signals[VISU_DATA_FREED_SIGNAL] =
    g_signal_newv ("objectFreed", G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL , NULL, NULL, g_cclosure_marshal_VOID__VOID,
		   G_TYPE_NONE, 0, NULL);

  /**
   * VisuData::FilesChanged:
   * @dataObj: the object which received the signal ;
   *
   * Gets emitted when one file of @dataObj is set.
   *
   * Since: 3.7
   */
  visu_data_signals[FILES_CHANGED_SIGNAL] =
    g_signal_new("FilesChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		 0, NULL, NULL, g_cclosure_marshal_VOID__UINT,
                 G_TYPE_NONE, 1, G_TYPE_UINT, NULL);

  /**
   * VisuData::TranslationsChanged:
   * @dataObj: the object which received the signal ;
   *
   * Gets emitted when translations are changed. At that point, node
   * coordinates are all different, but to update on node positions,
   * it's better to listen to VisuNodeArray::PositionChanged.
   *
   * Since: 3.7
   */
  visu_data_signals[TRANSLATIONS_CHANGED_SIGNAL] =
    g_signal_new("TranslationsChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0, NULL);

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose      = visu_data_dispose;
  G_OBJECT_CLASS(klass)->finalize     = visu_data_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_data_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_data_get_property;

  /**
   * VisuData::totalEnergy:
   *
   * Store the total energy of the system in eV.
   *
   * Since: 3.6
   */
  g_object_class_install_property(G_OBJECT_CLASS(klass), TOTAL_ENERGY_PROP,
				  g_param_spec_double("totalEnergy", "Total energy",
						      "Total energy of"
						      " the system (eV)",
						      -G_MAXFLOAT, G_MAXFLOAT,
						      G_MAXFLOAT,
						      G_PARAM_CONSTRUCT |
						      G_PARAM_READWRITE));

  /* Initialise internal variables. */
  allObjects = (GList*)0;

  /* Register a new NodeData. */
  dataNodeCoord = VISU_DATA_NODE(visu_data_node_newWithCallbacks
                                 (COORDINATES_ID,
                                  setCoordFromString, getCoordFromString));
  visu_data_node_setLabel(dataNodeCoord, _("Coord. (x, y, z)"));
  visu_data_node_setEditable(dataNodeCoord, TRUE);
}
static void visu_boxed_interface_init(VisuBoxedInterface *iface)
{
  iface->get_box = visu_data_getBox;
  iface->set_box = visu_data_setBox;
}

static void visu_data_init(VisuData *obj)
{
  int i;

  DBG_fprintf(stderr, "Visu Data: initializing a new object (%p).\n",
	      (gpointer)obj);
  
  obj->priv = g_malloc(sizeof(VisuDataPrivate));
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->files            = (GList*)0;
  obj->priv->commentary       = g_malloc(sizeof(gchar*) * 2);
  obj->priv->commentary[0]    = g_strdup("");
  obj->priv->commentary[1]    = (gchar*)0;
  obj->priv->box              = (VisuBox*)0;
  obj->priv->unit_signal      = 0;
  obj->priv->translationApply = FALSE;
  for (i = 0; i < 3; i++)
    obj->priv->translation[i] = 0.;
  obj->priv->nSets            = 1;
  obj->priv->iSet             = -1;
  obj->priv->timeoutList      = (GList*)0;  
  obj->priv->scaling          = defaultScaling;
  obj->priv->setColor         = (VisuDataColorFunc)0;

  visu_data_node_setUsed(dataNodeCoord, obj, 3);

  /* Add object from allObjects list. */
  allObjects = g_list_append(allObjects, (gpointer)obj);

  g_signal_emit_by_name(VISU_OBJECT_INSTANCE, "dataNew", obj, NULL);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_data_dispose(GObject* obj)
{
  VisuData *data;

  DBG_fprintf(stderr, "Visu Data: dispose object %p.\n", (gpointer)obj);

  data = VISU_DATA(obj);
  if (data->priv->dispose_has_run)
    return;
  data->priv->dispose_has_run = TRUE;

#if DEBUG == 1
  g_mem_profile();
#endif

  DBG_fprintf(stderr, "Visu Data: emit a 'objectFreed' signal.\n");
  g_signal_emit(obj, visu_data_signals[VISU_DATA_FREED_SIGNAL],
		0, NULL);

  visu_data_setBox(VISU_BOXED(data), (VisuBox*)0, FALSE);

  visu_data_node_setUsed(dataNodeCoord, data, 0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_data_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_data_finalize(GObject* obj)
{
  VisuData *data;
  GList *lst;

  g_return_if_fail(obj);

  DBG_fprintf(stderr, "Visu Data: finalize object %p.\n", (gpointer)obj);

  /* Remove object from allObjects list. */
  allObjects = g_list_remove(allObjects, (gpointer)obj);

  data = VISU_DATA(obj);

  /* Free privs elements. */
  if (data->priv)
    {
      DBG_fprintf(stderr, "Visu data: free private data.\n");
      if (data->priv->files)
	{
	  lst = data->priv->files;
	  while (lst)
	    {
	      g_free(((struct FileDescription_struct*)lst->data)->name);
	      g_free(lst->data);
	      lst = g_list_next(lst);
	    }
	  g_list_free(data->priv->files);
	}
      if (data->priv->commentary)
	g_strfreev(data->priv->commentary);
      if (data->priv->timeoutList)
	{
	  lst = data->priv->timeoutList;
	  while (lst)
	    {
	      g_source_remove(*(guint*)lst->data);
	      g_free(lst->data);
	      lst = g_list_next(lst);
	    }
	  g_list_free(data->priv->timeoutList);
	}
      g_free(data->priv);
    }
  /* The free is called by g_type_free_instance... */
  /*   g_free(data); */

  /* Chain up to the parent class */
  DBG_fprintf(stderr, "Visu data: chain to parent.\n");
  G_OBJECT_CLASS(visu_data_parent_class)->finalize(obj);
  DBG_fprintf(stderr, "Visu data: freeing ... OK.\n");

#if DEBUG == 1
  g_mem_profile();
#endif
}
static void visu_data_get_property(GObject* obj, guint property_id,
				   GValue *value, GParamSpec *pspec)
{
  VisuData *self = VISU_DATA(obj);

  DBG_fprintf(stderr, "Visu Data: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case TOTAL_ENERGY_PROP:
      g_value_set_double(value, self->priv->totalEnergy);
      DBG_fprintf(stderr, "%geV.\n", self->priv->totalEnergy);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_data_set_property(GObject* obj, guint property_id,
				   const GValue *value, GParamSpec *pspec)
{
  VisuData *self = VISU_DATA(obj);

  DBG_fprintf(stderr, "Visu Data: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case TOTAL_ENERGY_PROP:
      self->priv->totalEnergy = g_value_get_double(value);
      DBG_fprintf(stderr, "%geV.\n", self->priv->totalEnergy);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}


/**
 * visu_data_new:
 *
 * This creates an empty #VisuData object.
 *
 * Returns: a newly created #VisuData object (its ref count is set to 1).
 */
VisuData* visu_data_new(void)
{
  VisuData *data;

#if DEBUG == 1
  g_mem_profile();
#endif

  DBG_fprintf(stderr, "Visu Data: create a new VisuData object of type %d.\n",
              (int)VISU_TYPE_DATA);
  data = VISU_DATA(g_object_new(VISU_TYPE_DATA, NULL));

  if (!data)
    return (VisuData*)0;

#if DEBUG == 1
  g_mem_profile();
#endif

  return data;
}

/**
 * visu_data_new_withFiles:
 * @files: (element-type filename) (array zero-terminated=1): a list
 * of names.
 *
 * This creates an empty #VisuData object with filename set to be
 * ready to be loaded.
 *
 * Since: 3.7
 *
 * Returns: a newly created #VisuData object (its ref count is set to 1).
 */
VisuData* visu_data_new_withFiles(const gchar **files)
{
  VisuData *data;
  guint i;

#if DEBUG == 1
  g_mem_profile();
#endif

  DBG_fprintf(stderr, "Visu Data: create a new VisuData object of type %d.\n",
              (int)VISU_TYPE_DATA);
  data = VISU_DATA(g_object_new(VISU_TYPE_DATA, NULL));
  for (i = 0; files[i]; i++)
    visu_data_addFile(data, files[i], i, (ToolFileFormat*)0);

#if DEBUG == 1
  g_mem_profile();
#endif

  return data;
}

static gboolean setCoordFromString(VisuDataNode *dataNode, VisuData *dataObj,
				   VisuNode *node, gchar* labelIn,
				   gchar **labelOut, gboolean *modify)
{
  int res, ln, i;
  gchar **datas;
  gboolean error;
  float valueFloat;
  VisuElement *ele;

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(dataNode) &&
		       VISU_IS_DATA(dataObj) && node, FALSE);
  g_return_val_if_fail(labelIn && labelOut && modify, FALSE);

  /* Parse the given labelIn.
     remove first and last parenthesis. */
  if (labelIn[0] == '(')
    labelIn += 1;
  ln = strlen(labelIn);
  if (labelIn[ln - 1] == ')')
    labelIn[ln - 1] = '\0';
  datas = g_strsplit(labelIn, ";", 3);
  *modify = FALSE;
  for (i = 0; datas[i]; i++)
    {
      error = FALSE;
      res = sscanf(datas[i], "%f", &valueFloat);
      if (res != 1)
	error = TRUE;
      else
	if (node->xyz[i] != valueFloat)
	  {
	    node->xyz[i] = valueFloat;
	    *modify = TRUE;
	  }
      if (error)
	{
	  *labelOut = getCoordFromString(dataNode, dataObj, node);
	  g_strfreev(datas);
	  return FALSE;
	}
    }
  if (i != 3)
    error = TRUE;
  else
    error = FALSE;

  *labelOut = getCoordFromString(dataNode, dataObj, node);
  g_strfreev(datas);

  if (*modify)
    {
      ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);
      g_signal_emit_by_name(G_OBJECT(dataObj), "PositionChanged", ele, NULL);
    }

  return !error;
}
static gchar* getCoordFromString(VisuDataNode *dataNode, VisuData *dataObj,
				 VisuNode *node)
{
  GString *str;
  gchar *value;

  g_return_val_if_fail(VISU_IS_DATA_NODE_TYPE(dataNode) &&
		       VISU_IS_DATA(dataObj) && node, (gchar*)0);

  /* Check if the given property has an association with the given VisuData. */
  DBG_fprintf(stderr, "Visu Data: get label for node coordinates.\n");

  /* Set the format attribute. */
  str = g_string_new("");
  g_string_append_printf(str, "( %g ; %g ; %g )",
			 node->xyz[0], node->xyz[1], node->xyz[2]);
  value = str->str;
  g_string_free(str, FALSE);

  DBG_fprintf(stderr, "Visu Data: get values '%s'.\n", value);
  return value;
}

/**
 * visu_data_freePopulation:
 * @data: a VisuData to be freed.
 *
 * This method frees only the allocated memory that deals with
 * the nodes (i.e. everything except the data of the files,
 * the properties and the setColor method.
 */
void visu_data_freePopulation(VisuData *data)
{
  float zeros[3] = {0.f, 0.f, 0.f};

  if (!data)
    return;

  DBG_fprintf(stderr, "Visu Data: freeing the population of VisuData %p ...\n",
              (gpointer)data);
  visu_node_array_freeNodes(VISU_NODE_ARRAY(data));

  data->priv->iSet = -1;
  visu_box_setExtension(data->priv->box, zeros);
  visu_data_setXYZtranslation(data, zeros);

  DBG_fprintf(stderr, "Visu Data: freeing ... OK.\n");
}

/**
 * visu_data_addFile:
 * @data: a #VisuData object ;
 * @file: a string that points to a file ;
 * @kind: an integer to qualify the file to add ;
 * @format: (allow-none): a file format.
 *
 * This method is used to add files
 * of type @kind to the @data. The @file
 * attribute is copied. The @format argument can be null.
 */
void visu_data_addFile(VisuData *data, const gchar* file,
                       int kind, ToolFileFormat *format)
{
  struct FileDescription_struct *dt;
  GList *lst;
  gchar *oldName;

  g_return_if_fail(data && file);

  DBG_fprintf(stderr, "Visu Data: adding '%s' filename (key: %d) to the VisuData %p.\n",
	      file, kind, (gpointer)data);

  oldName = (gchar*)0;
  dt = (struct FileDescription_struct*)0;
  for (lst = data->priv->files; lst; lst = g_list_next(lst))
    if (((struct FileDescription_struct*)lst->data)->kind == kind)
      dt = (struct FileDescription_struct*)lst->data;
  if (!dt)
    {
      dt = g_malloc(sizeof(struct FileDescription_struct));
      dt->kind = kind;
      data->priv->files =
        g_list_prepend(data->priv->files, (gpointer)dt);
    }
  else
    oldName = dt->name;
  dt->name = tool_path_normalize(file);
  dt->format = format;

  if (oldName)
    {
      DBG_fprintf(stderr, "Visu Data: freeing name %p.\n", (gpointer)dt->name);
      g_free(oldName);
    }

  g_signal_emit(G_OBJECT(data), visu_data_signals[FILES_CHANGED_SIGNAL],
		0, kind, NULL);
}

/**
 * visu_data_removeAllFiles:
 * @data: a #VisuData object.
 *
 * This method is used to empty the list of
 * known file from the given @data.
 */

void visu_data_removeAllFiles(VisuData *data)
{
  GList *lst;

  g_return_if_fail(VISU_IS_DATA(data));

  lst = data->priv->files;
  while (lst)
    {
      g_free(((struct FileDescription_struct*)lst->data)->name);
      g_free(lst->data);
      lst = g_list_next(lst);
    }
  g_list_free(data->priv->files);
  data->priv->files = (GList*)0;
  visu_data_setNSubset(data, 1);
}

/**
 * visu_data_getFile:
 * @data: a #VisuData object.
 * @kind: an integer to qualify the required file ;
 * @format: (out caller-allocates) (allow-none): a location for a file format (can be NULL).
 *
 * This prototype is used to retrieve stored
 * files identify by their @kind.
 *
 * Returns: the name of a file (it should not be deleted).
 */
const gchar* visu_data_getFile(VisuData *data, int kind,
                               ToolFileFormat **format)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_DATA(data), (gchar*)0);

  DBG_fprintf(stderr, "Visu Data: retrieve filename with key %d"
	      " from the VisuData %p.\n", kind, (gpointer)data);
  if (format)
    *format = (ToolFileFormat*)0;
  for (lst = data->priv->files; lst; lst = g_list_next(lst))
    if (((struct FileDescription_struct*)lst->data)->kind == kind)
      {
	if (format)
	  *format = ((struct FileDescription_struct*)lst->data)->format;
        DBG_fprintf(stderr, " | '%s'\n", ((struct FileDescription_struct*)lst->data)->name);
	return ((struct FileDescription_struct*)lst->data)->name;
      }
  return (gchar*)0;
}

/**
 * visu_data_getFilesAsLabel:
 * @data: a #VisuData object.
 *
 * Creates a label using the list of files used to defined this @data
 * separated by dashes.
 *
 * Since: 3.6
 *
 * Returns: a newly created string with the filenames. If no filename
 * were used to defined @data, the function returns NULL.
 */
gchar* visu_data_getFilesAsLabel(const VisuData *data)
{
  GList *lst;
  GString *str;
  gchar *s;

  g_return_val_if_fail(VISU_IS_DATA(data), (gchar*)0);

  if (!data->priv->files)
    return (gchar*)0;

  DBG_fprintf(stderr, "Visu Data: retrieve all filenames"
	      " from the VisuData %p.\n", (gpointer)data);
  str = g_string_new("");
  for (lst = data->priv->files->next; lst; lst = g_list_next(lst))
    {
      s = g_path_get_basename(((struct FileDescription_struct*)lst->data)->name);
      g_string_append_printf(str, "\302\240\342\200\224 %s", s);
      g_free(s);
    }
  s = g_path_get_basename(((struct FileDescription_struct*)data->priv->files->data)->name);
  g_string_prepend(str, s);
  g_free(s);
  DBG_fprintf(stderr, " | '%s'\n", str->str);
  return g_string_free(str, FALSE);
}

/**
 * visu_data_setFileCommentary:
 * @data: a #VisuData object ;
 * @commentary: the message to be stored (null terminated) ;
 * @iSet: an integer.
 *
 * This method is used to store a description of the given @data. This
 * string is copied and @commentary can be freed. Before using this
 * method, the number of possible node sets must have been defined
 * using visu_data_setNSubset(), if not, only iSet == 0 is allowed.
 */
void visu_data_setFileCommentary(VisuData *data, gchar* commentary, gint iSet)
{
  g_return_if_fail(VISU_IS_DATA(data) &&
		   iSet >= 0 && iSet < data->priv->nSets);
  
  g_free(data->priv->commentary[iSet]);
  data->priv->commentary[iSet] = g_strdup(commentary);
}

/**
 * visu_data_getFileCommentary:
 * @data: a #VisuData object ;
 * @iSet: an integer (>= 0).
 *
 * Get the commentary associated to the given @data, for the given
 * node set.
 *
 * Returns: a string description (possibly empty). This string is own by V_Sim
 *          and should not be freed.
 */
gchar* visu_data_getFileCommentary(VisuData *data, gint iSet)
{
  g_return_val_if_fail(VISU_IS_DATA(data) &&
		       iSet >= 0 && iSet < data->priv->nSets, (gchar*)0);
  
  return data->priv->commentary[iSet];
}

/**
 * visu_data_setNSubset:
 * @data: a #VisuData object ;
 * @nSet: an integer.
 *
 * Change the number of available sets of nodes for this
 * #VisuData. This has a side effect to delete all previously saved
 * file commentaries (see visu_data_setFileCommentary()).
 */
void visu_data_setNSubset(VisuData *data, int nSet)
{
  int i;

  g_return_if_fail(VISU_IS_DATA(data) && nSet > 0);

  data->priv->nSets = nSet;
  if (data->priv->commentary)
    g_strfreev(data->priv->commentary);
  data->priv->commentary = g_malloc(sizeof(gchar*) * (nSet + 1));
  for (i = 0; i < nSet; i++)
    data->priv->commentary[i] = g_strdup("");
  data->priv->commentary[nSet] = (gchar*)0;
}

/**
 * visu_data_getNSubset:
 * @data: a #VisuData object.
 *
 * Retrieve the number of available sets of nodes for this #VisuData,
 * see visu_data_setNSubset().
 *
 * Returns: the number of set of nodes (1 is default).
 */

int visu_data_getNSubset(VisuData *data)
{
  g_return_val_if_fail(VISU_IS_DATA(data), 1);

  return data->priv->nSets;
}

/**
 * visu_data_setISubset:
 * @data: a #VisuData object ;
 * @iSet: an integer.
 *
 * Change the current id of the set of data (ordered as in C,
 * beginning at 0).
 */
void visu_data_setISubset(VisuData *data, int iSet)
{
  g_return_if_fail(VISU_IS_DATA(data));
  DBG_fprintf(stderr, "Visu Data: set id to %d (%d).\n",
	      iSet, data->priv->nSets);

  g_return_if_fail(iSet >= 0 && iSet < data->priv->nSets);

  data->priv->iSet = iSet;
}

/**
 * visu_data_getISubset:
 * @data: a #VisuData object.
 *
 * Retrieve the id of the current set of data (ordered as in C,
 * beginning at 0).
 *
 * Returns: the id of the set of nodes currently loaded, -1 if none.
 */
int visu_data_getISubset(VisuData *data)
{
  g_return_val_if_fail(VISU_IS_DATA(data), -1);

  DBG_fprintf(stderr, "Visu Data: get current set id %d.\n", data->priv->iSet);
  return data->priv->iSet;
}

/**
 * visu_data_setChangeElementFlag:
 * @data: a #VisuData object ;
 * @changeElement: a boolean.
 *
 * This method is mainly used by internal gears to set a flag. This flag control
 * if the @data object has the same #VisuElement objects than the previously rendered one.
 */
void visu_data_setChangeElementFlag(VisuData *data, gboolean changeElement)
{
  gboolean *val;

  g_return_if_fail(data);

  /* Test if already exists */
  val = (gboolean*)g_object_get_data(G_OBJECT(data), "changeElementListFlag");
  if (!val)
    {
      val = g_malloc(sizeof(gboolean));
      g_object_set_data_full(G_OBJECT(data), "changeElementListFlag",
			     val, g_free);
    }
  *val = changeElement;
}

/**
 * visu_data_getChangeElementFlag:
 * @data: a #VisuData object.
 *
 * V_Sim can use a flag set on @data object to know if @data has exactly the same
 * #VisuElement list than the previously rendered one.
 *
 * Returns: TRUE if the previously rendered #VisuData object has had the same
 *          #VisuElement list than the given one, FALSE otherwise.
 */
gboolean visu_data_getChangeElementFlag(VisuData *data)
{
  gboolean *val;

  g_return_val_if_fail(data, FALSE);

  val = (gboolean*)g_object_get_data(G_OBJECT(data), "changeElementListFlag");
  if (val)
    return *val;
  else
    return FALSE;
}


/*************************/
/* The geometry routines */
/*************************/
static VisuBox* visu_data_getBox(VisuBoxed *self)
{
  g_return_val_if_fail(VISU_IS_DATA(self), (VisuBox*)0);

  if (!VISU_DATA(self)->priv->box)
    {
      g_error("No box.");
    }
  return VISU_DATA(self)->priv->box;
}
static gboolean visu_data_setBox(VisuBoxed *self, VisuBox *box, gboolean update _U_)
{
  VisuData *data;

  g_return_val_if_fail(VISU_IS_DATA(self), FALSE);
  data = VISU_DATA(self);

  if (data->priv->box == box)
    return FALSE;

  if (data->priv->box)
    {
      if (data->priv->unit_signal)
        g_signal_handler_disconnect(G_OBJECT(data->priv->box),
                                    data->priv->unit_signal);
      g_object_unref(data->priv->box);
    }
  data->priv->box = box;
  if (box)
    {
      g_object_ref(box);
      data->priv->unit_signal = 
        g_signal_connect(G_OBJECT(data->priv->box), "UnitChanged",
                         G_CALLBACK(onBoxUnitChanged), (gpointer)data);
    }
  return TRUE;
}

/**
 * visu_data_getXYZtranslation:
 * @data: a #VisuData object.
 *
 * The nodes are rendered at thier coordinates plus a translation. This method
 * allows to retrieve that translation.
 *
 * Returns: (array fixed-size=3) (transfer full): a newly allocated
 * array of 3 floats. It should be freed with a call to free() after
 * use.
 */
float* visu_data_getXYZtranslation(VisuData* data)
{
  float *trans;

  g_return_val_if_fail(VISU_IS_DATA(data), (float*)0);

  trans = g_malloc(sizeof(float) * 3);
  trans[0] = data->priv->translation[0];
  trans[1] = data->priv->translation[1];
  trans[2] = data->priv->translation[2];

  return trans;
}

/**
 * visu_data_forceXYZtranslation:
 * @data: a #VisuData object.
 * @xyz: (in) (array fixed-size=3): three floats in cartesian representation.
 *
 * Apply the given box translation without paying attention to the box
 * boundary conditions. See visu_data_setXYZtranslation() for a method
 * respecting the periodicity.
 *
 * Since: 3.7
 *
 * Returns: if returns TRUE, the VisuNodeArray::PositionChanged signal should be emitted.
 */
gboolean visu_data_forceXYZtranslation(VisuData* data, float xyz[3])
{
  gboolean res;

  g_return_val_if_fail(VISU_IS_DATA(data) && xyz, 0);

  res = FALSE;
  if (data->priv->translation[0] != xyz[0])
    {
      data->priv->translation[0] = xyz[0];
      res = TRUE;
    }
  if (data->priv->translation[1] != xyz[1])
    {
      data->priv->translation[1] = xyz[1];
      res = TRUE;
    }
  if (data->priv->translation[2] != xyz[2])
    {
      data->priv->translation[2] = xyz[2];
      res = TRUE;
    }
  DBG_fprintf(stderr, "Visu Data: force translation to: %f %f %f\n",
	      data->priv->translation[0], data->priv->translation[1],
	      data->priv->translation[2]);
  if (res)
    g_signal_emit(data, visu_data_signals[TRANSLATIONS_CHANGED_SIGNAL], 0, NULL);

  return res;
}
/**
 * visu_data_setXYZtranslation:
 * @data: a #VisuData object ;
 * @xyz: (in) (array fixed-size=3): an array of floating point values.
 *
 * This set the translations of the specified #VisuData whatever previous values.
 * The translation is done in the orthonormal referential, not the referential of
 * the box.
 *
 * Returns: if returns TRUE, the VisuNodeArray::PositionChanged signal should be emitted.
 */
gboolean visu_data_setXYZtranslation(VisuData* data, float xyz[3])
{
  VisuBoxBoundaries bc;
  float xyz_[3];

  g_return_val_if_fail(VISU_IS_DATA(data) && xyz, 0);

  xyz_[0] = data->priv->translation[0];
  xyz_[1] = data->priv->translation[1];
  xyz_[2] = data->priv->translation[2];
  bc = visu_box_getBoundary(data->priv->box);
  if (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_YZ)
    xyz_[0] = xyz[0];
  if (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_ZX)
    xyz_[1] = xyz[1];
  if (bc != VISU_BOX_FREE && bc != VISU_BOX_SURFACE_XY)
    xyz_[2] = xyz[2];

  return visu_data_forceXYZtranslation(data, xyz_);
}

/**
 * visu_data_getNodeBoxFromNumber:
 * @data: a #VisuData object.
 * @nodeId: the index of the node considered.
 * @nodeBox: (in) (array fixed-size=3): the array to store the box of the node.
 *
 * This method retrieves the value of the box associated to a node (with respect to the unit cell).
 * 
 * Returns: TRUE if everything went well, FALSE otherwise. The box is stored in the nodeBox array.
 */
gboolean visu_data_getNodeBoxFromNumber(VisuData *data, guint nodeId, int nodeBox[3])
{
  float xcart[3];

  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);

  visu_data_getNodePosition(data, visu_node_array_getFromId(VISU_NODE_ARRAY(data),nodeId), xcart);
  visu_data_getNodeBoxFromCoord(data, xcart, nodeBox);
  return TRUE;
}

/**
 * visu_data_getNodeBoxFromCoord:
 * @data: a #VisuData object.
 * @xcart: (in) (array fixed-size=3): the coordinates of a node.
 * @nodeBox: (in) (array fixed-size=3): the array to store the box of the node.
 *
 * This method retrieves the value of the box associated to the coordinates of the node (with respect to the unit cell).
 * 
 * Returns: TRUE if everything went well, FALSE otherwise. The box is stored in the nodeBox array.
 */
gboolean visu_data_getNodeBoxFromCoord(VisuData *data, float xcart[3], int nodeBox[3])
{
  float xred[3];

  visu_box_convertXYZtoBoxCoordinates(data->priv->box, xred, xcart);
  nodeBox[0] = floor(xred[0]);
  nodeBox[1] = floor(xred[1]);
  nodeBox[2] = floor(xred[2]);
  DBG_fprintf(stderr, "Visu Data: nodeBox found for atom at %f %f %f : %d %d %d.\n",
		xcart[0], xcart[1], xcart[2], nodeBox[0], nodeBox[1], nodeBox[2]);
  return TRUE;
}

static void onBoxUnitChanged(VisuBox *box, gfloat fact, gpointer user_data)
{
  VisuData *data = VISU_DATA(user_data);
  VisuNodeArrayIter iter;

  DBG_fprintf(stderr, "Visu Data: caught 'UnitChanged' signal with factor %g.\n", fact);

  /* We do an homothety on the nodes. */
  data->priv->translation[0] *= fact;
  data->priv->translation[1] *= fact;
  data->priv->translation[2] *= fact;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for( visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      iter.node->xyz[0] *= fact;
      iter.node->xyz[1] *= fact;
      iter.node->xyz[2] *= fact;
      iter.node->translation[0] *= fact;
      iter.node->translation[1] *= fact;
      iter.node->translation[2] *= fact;
    }
  /* We raise the signals. */
  g_signal_emit_by_name(G_OBJECT(data), "PositionChanged", (VisuElement*)0, NULL);

  /* Update the margin with the new AllNodeExtens. */
  visu_box_setMargin(box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, (VisuBox*)0), FALSE);
  DBG_fprintf(stderr, "Visu Data: done 'UnitChanged'.\n");
}

/**
 * visu_data_constrainedElementInTheBox:
 * @data: a #VisuData object ;
 * @element: a #VisuElement object.
 *
 * Check all the nodes of the specified @element and change their coordinates if they are out
 * of the bounding box. The position of each node is the result of the
 * sum of their own position and of the box translation.
 *
 * Returns: if returns TRUE, the VisuNodeArray::PositionChanged signal should be emitted.
 */
gboolean visu_data_constrainedElementInTheBox(VisuData *data, VisuElement *element)
{
  gboolean changed;
  float cartCoord[3], t[3];
  VisuNodeArrayIter iter;

  g_return_val_if_fail(VISU_IS_DATA(data) && element, FALSE);

  if (!element->rendered)
    return FALSE;

  DBG_fprintf(stderr, "Visu Data: Checking for nodes of element '%s'"
	      " to be in the box.\n", element->name);
  changed = FALSE;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  iter.element = element;
  for(visu_node_array_iterRestartNode(VISU_NODE_ARRAY(data), &iter); iter.node;
      visu_node_array_iterNextNode(VISU_NODE_ARRAY(data), &iter))
    {
      visu_data_getNodePosition(data, iter.node, cartCoord);
      if (visu_box_constrainInside(data->priv->box, t, cartCoord, TRUE))
	{
	  changed = TRUE;
	  iter.node->translation[0] += t[0];
	  iter.node->translation[1] += t[1];
	  iter.node->translation[2] += t[2];
	}
    }
  data->priv->translationApply = TRUE;
  return changed;
}

/**
 * visu_data_constrainedInTheBox:
 * @data: a #VisuData object.
 *
 * It does the same things that visu_data_constrainedElementInTheBox() but for all
 * the #VisuElement of the given @data. I.e. it checks all the nodes and changes
 * their coordinates if they are out of the bounding box.
 * The position of each node is the result of the
 * sum of their own position and of the box translation.
 *
 * Returns: if returns TRUE, the VisuNodeArray::PositionChanged signal should be emitted.
 */
gboolean visu_data_constrainedInTheBox(VisuData *data)
{
  VisuNodeArrayIter iter;
  gboolean changed;

  g_return_val_if_fail(VISU_IS_DATA(data), 0);

  changed = FALSE;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.element;
       visu_node_array_iterNextElement(VISU_NODE_ARRAY(data), &iter))
    changed = visu_data_constrainedElementInTheBox
      (data, iter.element) || changed;
  data->priv->translationApply = TRUE;
  return changed;
}

/**
 * visu_data_constrainedFree:
 * @data: a #VisuData object.
 *
 * Return all the nodes to their original position, except for the
 * global translation.
 *
 * Returns: if returns TRUE, the VisuNodeArray::PositionChanged signal should be emitted.
 */
gboolean visu_data_constrainedFree(VisuData *data)
{
  VisuNodeArrayIter iter;

  g_return_val_if_fail(VISU_IS_DATA(data), 0);

  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      iter.node->translation[0] = 0.;
      iter.node->translation[1] = 0.;
      iter.node->translation[2] = 0.;
    }
  data->priv->translationApply = FALSE;
  return TRUE;
}

/**
 * visu_data_getTranslationStatus:
 * @data: a #VisuData object.
 *
 * When a translation is applied (even with a [0,0,0] vector), the
 * nodes are shifted to be in the box. This routine returns the
 * translation status of all nodes. If one of them is translated, then
 * return value is TRUE.
 *
 * Returns: if one of the nodes is shifted.
 */
gboolean visu_data_getTranslationStatus(VisuData *data)
{
  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);
  
  DBG_fprintf(stderr, "Visu Data: retrieve translation status (%d).\n",
	      data->priv->translationApply);
  return data->priv->translationApply;
}
/**
 * visu_data_setTightBox:
 * @data: a #VisuData object.
 *
 * Calculate the box geometry to have a tight box in directions that
 * are not periodic. If some directions are still periodic, the box
 * size in these directions should be setup first with
 * visu_box_setGeometry().
 *
 * Returns: (transfer none): a new #VisuBox if @data had not one
 * before, or the modified box of @data.
 */
VisuBox* visu_data_setTightBox(VisuData *data)
{
  double xMin, yMin, zMin, xMax, yMax, zMax, xFree, yFree, zFree;
  double boxGeometry[6], boxGeometry_[6];
  float xyz[3];
  VisuNodeArrayIter iter;
  VisuBoxBoundaries bc;
  guint i;
  VisuBox *box;
  
  g_return_val_if_fail(VISU_IS_DATA(data), (VisuBox*)0);

  if (!data->priv->box)
    {
      for (i = 0; i < VISU_BOX_N_VECTORS; i++)
        boxGeometry_[i] = 0.;
      box = visu_box_new(boxGeometry_, VISU_BOX_FREE);
      visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box), FALSE);
      g_object_unref(box);
    }
  bc = visu_box_getBoundary(data->priv->box);
  if (bc == VISU_BOX_PERIODIC)
    return data->priv->box;

  /* Store the coordinates */
  xMin = 1e5;
  yMin = 1e5;
  zMin = 1e5;
  xMax = -1e5;
  yMax = -1e5;
  zMax = -1e5;
  xFree = (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_YZ)?1.:0.;
  yFree = (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_ZX)?1.:0.;
  zFree = (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_XY)?1.:0.;
  
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      xMin = MIN(xMin, iter.node->xyz[0]);
      yMin = MIN(yMin, iter.node->xyz[1]);
      zMin = MIN(zMin, iter.node->xyz[2]);
      xMax = MAX(xMax, iter.node->xyz[0]);
      yMax = MAX(yMax, iter.node->xyz[1]);
      zMax = MAX(zMax, iter.node->xyz[2]);
    }

  DBG_fprintf(stderr, "Visu Data: the elements are in [%f, %f]x[%f, %f]x[%f, %f].\n",
	      xMin, xMax, yMin, yMax, zMin, zMax);
  for (i = 0; i < VISU_BOX_N_VECTORS; i++)
    boxGeometry_[i] = visu_box_getGeometry(data->priv->box, i);
  boxGeometry[0] = (xMax - xMin + 1e-5) * xFree + (1. - xFree) * boxGeometry_[0];
  boxGeometry[1] = 0.                           + (1. - yFree) * boxGeometry_[1];
  boxGeometry[2] = (yMax - yMin + 1e-5) * yFree + (1. - yFree) * boxGeometry_[2];
  boxGeometry[3] = 0.                           + (1. - zFree) * boxGeometry_[3];
  boxGeometry[4] = 0.                           + (1. - zFree) * boxGeometry_[4];
  boxGeometry[5] = (zMax - zMin + 1e-5) * zFree + (1. - zFree) * boxGeometry_[5];
  visu_box_setGeometry(data->priv->box, boxGeometry);

  xyz[0] = -xMin * xFree;
  xyz[1] = -yMin * yFree;
  xyz[2] = -zMin * zFree;
  data->priv->translationApply = TRUE;
  visu_data_forceXYZtranslation(data, xyz);

  return data->priv->box;
}

/**
 * visu_data_replicate:
 * @data: a #VisuData object ;
 * @extension: (in) (array fixed-size=3): three floating point values ;
 *
 * This routine will create (or remove) nodes to expand the initial box to
 * the required size. An extension of 0 means no extension, i.e. the initial
 * box. The extension is done symmetrically in each direction toward negative
 * and positive direction.
 *
 * To remove added nodes, see visu_data_restore().
 *
 * returns: TRUE if the redraw should be done.
 */
gboolean visu_data_replicate(VisuData *data, float extension[3])
{
  int i;
  gboolean changed;
  /* Will store the index of removed or added nodes, always finish by -1.
     The two first values are specific and used to store:
     - the current number of index ;
     - the allocated size of the array (not including these two
       specific values and the -1 at the end. */
  int *index;
  float extension0[3];

  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);
  g_return_val_if_fail(extension[0] >= 0. &&
		       extension[1] >= 0. &&
		       extension[2] >= 0., FALSE);

  /* Keep only three digits for the extension to avoid rounding
     troubles. */
  extension[0] = (float)((int)(extension[0] * 1000)) / 1000;
  extension[1] = (float)((int)(extension[1] * 1000)) / 1000;
  extension[2] = (float)((int)(extension[2] * 1000)) / 1000;
  DBG_fprintf(stderr, "Visu Data: modify extension (%g, %g, %g).\n",
	      extension[0], extension[1], extension[2]);

  visu_box_getExtension(data->priv->box, extension0);
  for (i = 0; i < 3; i++)
    {
      if (extension0[i] > extension[i])
	{
	  index = shrinkNodeList(data, i, extension[i]);
	  if (index)
	    {
	      /* We finish the list with -1. */
	      index[index[0] + 2] = -1;
	      visu_node_array_removeNodes(VISU_NODE_ARRAY(data), index + 2);
	      g_free(index);
	    }
	}
      else if (extension0[i] < extension[i])
	{
	  index = extendNodeList(data, i, extension0[i], extension[i]);
	  if (index)
	    {
	      /* We finish the list with -1. */
	      index[index[0] + 2] = -1;
	      DBG_fprintf(stderr, "Visu Data: emit a "
			  "'PopulationIncrease' signal.\n");
	      g_signal_emit_by_name(G_OBJECT(data), "PopulationIncrease",
                                    (gpointer)index, NULL);
	      g_free(index);
	    }
	}
    }
  changed = visu_box_setExtension(data->priv->box, extension);
  if (DEBUG)
    visu_node_array_traceProperty(VISU_NODE_ARRAY(data), "originalId");
  return changed;
}

/**
 * visu_data_restore:
 * @data: a #VisuData object.
 *
 * Remove all nodes that have been added by a visu_data_replicate()
 * call.
 *
 * Returns: TRUE if some nodes has been indeed removed.
 */
gboolean visu_data_restore(VisuData *data)
{
  float zeros[3] = {0.f, 0.f, 0.f};

  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);

  visu_box_setExtension(data->priv->box, zeros);
  return visu_node_array_removeAllDuplicateNodes(VISU_NODE_ARRAY(data));
}
static int* reallocIndexList(int *index, int size)
{
  int currentSize;
  
  if (index)
    currentSize = index[1];
  else
    currentSize = 0;

  index = g_realloc(index, sizeof(int) * (currentSize + size + 3));
  index[0] = currentSize;
  index[1] = currentSize + size;

  DBG_fprintf(stderr, "Visu Data: realloc internal index list (%d %d).\n",
	      index[0], index[1]);
  return index;
}
static int* addIndexList(int *index, int value, int size)
{
  if (!index || index[0] == index[1])
    index = reallocIndexList(index, size);
  index[index[0] + 2]  = value;
  index[0]            += 1;
  return index;
}
static int* shrinkNodeList(VisuData *data, int coord, float valueTo)
{
  float cartCoord[3], boxCoord[3];
  int *index;
  VisuNodeArrayIter iter;

  g_return_val_if_fail(coord == 0 || coord == 1 || coord == 2, FALSE);
  g_return_val_if_fail(valueTo >= 0.f, FALSE);

  DBG_fprintf(stderr, "Visu Data: shrink to %g (%d).\n", valueTo, coord);
  index = (int*)0;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      cartCoord[0] = iter.node->xyz[0] + data->priv->translation[0] +
	iter.node->translation[0];
      cartCoord[1] = iter.node->xyz[1] + data->priv->translation[1] +
	iter.node->translation[1];
      cartCoord[2] = iter.node->xyz[2] + data->priv->translation[2] +
	iter.node->translation[2];
      visu_box_convertXYZtoBoxCoordinates(data->priv->box, boxCoord, cartCoord);
      if ((boxCoord[coord] < - valueTo - 1e-6 ||  /* We are out on the
						     low coord. */
	   boxCoord[coord] >= 1.f + valueTo -1e-6) && /* We are out on
							 the high
							 coord. */
	  visu_node_array_getOriginal(VISU_NODE_ARRAY(data), iter.node->number) >= 0)
	/* We remove the element. */
	index = addIndexList(index, iter.node->number,
			     visu_node_array_getNNodes(VISU_NODE_ARRAY(data)));
      DBG_fprintf(stderr, "Visu Data: test shrink for %d: %d %15.12fx%15.12fx%15.12f.\n",
		  iter.node->number, (index)?index[0]:0,
		  boxCoord[0], boxCoord[1], boxCoord[2]);
    }
  return index;
}
static int* extendNodeList(VisuData *data, int coord, float valueFrom, float valueTo)
{
  int k, id;
  unsigned nb, nbInit;
  VisuNode *newNode;
  float cartCoord[3], boxCoord[3], ratio;
  int *index;
  VisuNodeArrayIter iter;

  g_return_val_if_fail(coord == 0 || coord == 1 || coord == 2, FALSE);
  g_return_val_if_fail(valueTo > valueFrom, FALSE);

  DBG_fprintf(stderr, "Visu Data: expand in %d direction to %g.\n",
	      coord, valueTo);
  DBG_fprintf(stderr, " | k runs in [%d %d[ ]%d %d].\n", (int)floor(-valueTo),
	      -(int)valueFrom, (int)valueFrom, (int)ceil(valueTo));
  DBG_fprintf(stderr, " | keeps new ele in [%g %g] [%g %g].\n", -valueTo,
	      -valueFrom, valueFrom + 1.f, valueTo + 1.f);
  index = (int*)0;

  /* We estimate the number of data to be added and we call a realloc of
     this amount now to avoid to much small reallocations.
     The slab of box to be extend is 2*(valueTo-extension[coord]).
     So the volume in box coordinates is the same value and since
     the volume in box coordinates is product(1+2*extension),
     the ratio of new space is the fraction.
     So we realloc all elements on this ratio. */
  ratio = (2.f * (valueTo - valueFrom)) / (1.f + 2.f * valueFrom);
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.element;
       visu_node_array_iterNextElement(VISU_NODE_ARRAY(data), &iter))
    {
      nb = (int)ceil((float)iter.nStoredNodes * ratio);
      visu_node_array_allocateNodesForElement(VISU_NODE_ARRAY(data), iter.iElement,
                                              iter.nStoredNodes + nb);
    }
  
  /* All node with an id higher than nbInit are considered as new
     nodes. */
  nbInit = G_MAXUINT;
  for (visu_node_array_iterStartNumber(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNextNodeNumber(VISU_NODE_ARRAY(data), &iter))
    {
      /* Do not duplicate the new nodes. */
      if (iter.node->number > nbInit)
	continue;

      cartCoord[0] = iter.node->xyz[0] + data->priv->translation[0] +
	iter.node->translation[0];
      cartCoord[1] = iter.node->xyz[1] + data->priv->translation[1] +
	iter.node->translation[1];
      cartCoord[2] = iter.node->xyz[2] + data->priv->translation[2] +
	iter.node->translation[2];
      visu_box_convertXYZtoBoxCoordinates(data->priv->box, boxCoord, cartCoord);
      for (k = (int)floor(-valueTo); k < (int)ceil(valueTo) + 1; k++)
	{
	  if (k >= -(int)valueFrom && k <   (int)valueFrom + 1)
	    continue;
	  boxCoord[coord] += (float)k;
	  if ((boxCoord[coord] >= -valueTo && boxCoord[coord] < -valueFrom ) ||
	      (boxCoord[coord] < valueTo + 1.f  && boxCoord[coord] >= valueFrom + 1.f))
	    {
	      DBG_fprintf(stderr, "Visu Data: replicating node %d, (%d)"
			  " (%15.12fx%15.12fx%15.12f).\n", iter.node->number, coord,
			  boxCoord[0], boxCoord[1], boxCoord[2]);
	      /* We save the current node id, because the pointer may be
		 relocated by the visu_node_array_getCopyNode() call. */
	      id = iter.node->number;
	      /* We create and add a new element. */
	      newNode = visu_node_array_getCopyNode(VISU_NODE_ARRAY(data), iter.node);
	      index = addIndexList(index, newNode->number,
				   visu_node_array_getNNodes(VISU_NODE_ARRAY(data)));
              if (nbInit == G_MAXUINT)
                nbInit = newNode->number - 1;
	      visu_box_convertBoxCoordinatestoXYZ(data->priv->box,
                                                  newNode->xyz, boxCoord);
	      newNode->xyz[0] -= data->priv->translation[0] +
		newNode->translation[0];
	      newNode->xyz[1] -= data->priv->translation[1] +
		newNode->translation[1];
	      newNode->xyz[2] -= data->priv->translation[2] +
		newNode->translation[2];
	      /* We reset the iter.node pointer. */
	      iter.node = visu_node_array_getFromId(VISU_NODE_ARRAY(data), id);
	    }
	  boxCoord[coord] -= (float)k;
	}
    }
  return index;
}

/**
 * visu_data_getAllNodeExtens:
 * @dataObj: a #VisuData object.
 * @box: (allow-none): a #VisuBox object.
 *
 * Calculate the longest distance between the surface of @box (without
 * extension) and all the nodes. If @box is NULL, then the internal
 * box of @dataObj is used.
 *
 * Since: 3.7
 *
 * Returns: the longest distance between the surface of @box (without
 * extension) and all the nodes.
 **/
gfloat visu_data_getAllNodeExtens(VisuData *dataObj, VisuBox *box)
{
  VisuNodeArrayIter iter;
  float xyz[2][3], t[3], lg[2], coord[3];

  g_return_val_if_fail(VISU_IS_DATA(dataObj), 0.f);

  if (!box)
    box = dataObj->priv->box;

  t[0] = (float)(visu_box_getGeometry(box, VISU_BOX_DXX) +
		 visu_box_getGeometry(box, VISU_BOX_DYX) +
                 visu_box_getGeometry(box, VISU_BOX_DZX));
  t[1] = (float)(visu_box_getGeometry(box, VISU_BOX_DYY) +
		 visu_box_getGeometry(box, VISU_BOX_DZY));
  t[2] = (float)(visu_box_getGeometry(box, VISU_BOX_DZZ));
  xyz[0][0] = xyz[0][1] = xyz[0][2] = 0.f;
  xyz[1][0] = xyz[1][1] = xyz[1][2] = 0.f;

  visu_node_array_iterNew(VISU_NODE_ARRAY(dataObj), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataObj), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(dataObj), &iter))
    {
      visu_data_getNodePosition(dataObj, iter.node, coord);
      xyz[0][0] = MIN(xyz[0][0], coord[0]);
      xyz[0][1] = MIN(xyz[0][1], coord[1]);
      xyz[0][2] = MIN(xyz[0][2], coord[2]);

      xyz[1][0] = MAX(xyz[1][0], coord[0]);
      xyz[1][1] = MAX(xyz[1][1], coord[1]);
      xyz[1][2] = MAX(xyz[1][2], coord[2]);
    }
  xyz[1][0] -= t[0];
  xyz[1][1] -= t[1];
  xyz[1][2] -= t[2];
  /* Compute the longest vector out of the box. */
  lg[0] = sqrt(xyz[0][0] * xyz[0][0] + 
	       xyz[0][1] * xyz[0][1] + 
	       xyz[0][2] * xyz[0][2]);
  lg[1] = sqrt(xyz[1][0] * xyz[1][0] + 
	       xyz[1][1] * xyz[1][1] + 
	       xyz[1][2] * xyz[1][2]);
  DBG_fprintf(stderr, "VisuData: vectors outside of the box %g %g.\n", lg[0], lg[1]);
  return MAX(lg[0], lg[1]);
}

/**
 * visu_data_getDistanceList:
 * @data: a #VisuData object ;
 * @nodeId: a node id.
 * @minVal: a location for a float.
 *
 * This routine creates an array of #VisuNodeInfo, storing for each
 * node its node id and its distance to @nodeId. The periodicity is
 * NOT taken into account. The array is not distance sorted, but if
 * @minVal is provided, it will contain the minimal distance between
 * @nodeId and the other nodes.
 *
 * Since: 3.5
 *
 * Returns: an array of #VisuNodeInfo of size the number of nodes. It
 * is terminated by @nodeId value itself.
 */
VisuNodeInfo* visu_data_getDistanceList(VisuData *data, guint nodeId, float *minVal)
{
  VisuNodeInfo *infos;
  int nNodes;
  VisuNodeArrayIter iter;
  VisuNode *nodeRef;
  float xyz[3], xyzRef[3], min;

  g_return_val_if_fail(VISU_IS_DATA(data), (VisuNodeInfo*)0);

  DBG_fprintf(stderr, "Visu Data: get distance list for node %d.\n", nodeId);
  nodeRef = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_val_if_fail(nodeRef, (VisuNodeInfo*)0);

  nNodes = visu_node_array_getNNodes(VISU_NODE_ARRAY(data));
  infos = g_malloc(sizeof(VisuNodeInfo) * nNodes);
  
  visu_data_getNodePosition(data, nodeRef, xyzRef);

  min = G_MAXFLOAT;
  nNodes = 0;
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNextVisible(VISU_NODE_ARRAY(data), &iter))
    {
      infos[nNodes].id = iter.node->number;
      visu_data_getNodePosition(data, iter.node, xyz);
      infos[nNodes].dist =
	(xyz[0] - xyzRef[0]) * (xyz[0] - xyzRef[0]) +
	(xyz[1] - xyzRef[1]) * (xyz[1] - xyzRef[1]) +
	(xyz[2] - xyzRef[2]) * (xyz[2] - xyzRef[2]);
      if (infos[nNodes].dist > 0.0001f)
	{
	  min = MIN(min, infos[nNodes].dist);
	  nNodes += 1;
	}
    }
  infos[nNodes].id = nodeId;

  DBG_fprintf(stderr, " | min value = %g.\n", min);
  if (minVal)
    *minVal = min;

  return infos;
}
/**
 * visu_data_setNewBasisFromNodes:
 * @data: a #VisuData object.
 * @nO: the index of node as origin.
 * @nA: the index of node on X axis.
 * @nB: the index of node as Y axis.
 * @nC: the index of node as Z axis.
 *
 * Change the basis set by providing the new basis set from a list of
 * nodes. See also visu_data_setNewBasis(). Nodes outside the new box
 * are killed.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the new basis set is valid.
 */
gboolean visu_data_setNewBasisFromNodes(VisuData *data, guint nO, guint nA, guint nB, guint nC)
{
  VisuNode *orig, *nodeA, *nodeB, *nodeC;
  float matA[3][3], O[3], xyz[3];

  orig = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nO);
  DBG_fprintf(stderr, " orig = %p\n", (gpointer)orig);
  nodeA = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nA);
  DBG_fprintf(stderr, " nodeA = %p\n", (gpointer)nodeA);
  nodeB = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nB);
  DBG_fprintf(stderr, " nodeB = %p\n", (gpointer)nodeB);
  nodeC = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nC);
  DBG_fprintf(stderr, " nodeC = %p\n", (gpointer)nodeC);
  g_return_val_if_fail(orig && nodeA && nodeB && nodeC, FALSE);

  visu_data_getNodePosition(data, orig, O);
  visu_data_getNodePosition(data, nodeA, xyz);
  matA[0][0] = xyz[0] - O[0];
  matA[1][0] = xyz[1] - O[1];
  matA[2][0] = xyz[2] - O[2];
  visu_data_getNodePosition(data, nodeB, xyz);
  matA[0][1] = xyz[0] - O[0];
  matA[1][1] = xyz[1] - O[1];
  matA[2][1] = xyz[2] - O[2];
  visu_data_getNodePosition(data, nodeC, xyz);
  matA[0][2] = xyz[0] - O[0];
  matA[1][2] = xyz[1] - O[1];
  matA[2][2] = xyz[2] - O[2];

  return visu_data_setNewBasis(data, matA, O);
}
/**
 * visu_data_setNewBasis:
 * @data: a #VisuData object.
 * @matA: a basis set definition.
 * @O: the origin cartesian coordinates.
 *
 * Change the basis set of @data according to the new definition given
 * by @matA and @O. Nodes outside the new box are killed. See also
 * visu_data_setNewBasisFromNodes() for a convenient function using
 * nodes as basis set definition.
 * 
 * Since: 3.6
 *
 * Returns: TRUE if the new basis set is valid.
 */
gboolean visu_data_setNewBasis(VisuData *data, float matA[3][3], float O[3])
{
  double mat_[3][3];
  float inv[3][3], vect[3], xred[3];
  double box[6];
  float vectEps[3], deltaEps[3];
  VisuNodeArrayIter iter;
  int *rmNodes, i;
  float zeros[3] = {0.f, 0.f, 0.f};
#define EPS 1.e-5

  DBG_fprintf(stderr, "Visu Data: basis matrice:\n");
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      matA[0][0], matA[0][1], matA[0][2]);
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      matA[1][0], matA[1][1], matA[1][2]);
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      matA[2][0], matA[2][1], matA[2][2]);
  if (!tool_matrix_invert(inv, matA))
    return FALSE;
  DBG_fprintf(stderr, "Visu Data: transformation matrice:\n");
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      inv[0][0], inv[0][1], inv[0][2]);
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      inv[1][0], inv[1][1], inv[1][2]);
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      inv[2][0], inv[2][1], inv[2][2]);

  mat_[0][0] = (double)matA[0][0];
  mat_[1][0] = (double)matA[0][1];
  mat_[2][0] = (double)matA[0][2];
  mat_[0][1] = (double)matA[1][0];
  mat_[1][1] = (double)matA[1][1];
  mat_[2][1] = (double)matA[1][2];
  mat_[0][2] = (double)matA[2][0];
  mat_[1][2] = (double)matA[2][1];
  mat_[2][2] = (double)matA[2][2];
  if (!tool_matrix_reducePrimitiveVectors(box, mat_))
    return FALSE;
  DBG_fprintf(stderr, "Visu Data: new box:\n");
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      box[0], box[1], box[2]);
  DBG_fprintf(stderr, "  (%10.5f  %10.5f  %10.5f)\n",
	      box[3], box[4], box[5]);

  visu_box_setBoundary(data->priv->box, VISU_BOX_PERIODIC);
  /* Trick to avoid the emission of SizeChanged signal. */
  visu_box_setMargin(data->priv->box, G_MAXFLOAT, FALSE);
  visu_box_setGeometry(data->priv->box, box);
  /* Remove possible extension. */
  visu_box_setExtension(data->priv->box, zeros);

  /* We need to move all the atoms of (eps, eps, eps) in the new box
     to avoid rounding problems. */
  xred[0] = 1.f;
  xred[1] = 1.f;
  xred[2] = 1.f;
  tool_matrix_productVector(vect, matA, xred);
  vectEps[0] = (vect[0] >= 0.f)?EPS:-EPS;
  vectEps[1] = (vect[1] >= 0.f)?EPS:-EPS;
  vectEps[2] = (vect[2] >= 0.f)?EPS:-EPS;
  tool_matrix_productVector(xred, inv, vectEps);
  visu_box_convertBoxCoordinatestoXYZ(data->priv->box, deltaEps, xred);
  DBG_fprintf(stderr, "Visu Data: applied epsilon (%10.5f  %10.5f  %10.5f)\n",
	      vectEps[0], vectEps[1], vectEps[2]);

  /* Transform each atomic coordinates using this matrice. */
  DBG_fprintf(stderr, "Visu Data: reset the coordinates for all nodes.\n");
  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  rmNodes = g_malloc(sizeof(int) * iter.nAllStoredNodes);
  i = 0;
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      visu_data_getNodePosition(data, iter.node, vect);
      vect[0] += - O[0] + vectEps[0];
      vect[1] += - O[1] + vectEps[1];
      vect[2] += - O[2] + vectEps[2];
      tool_matrix_productVector(xred, inv, vect);
      if (xred[0] < 0.f || xred[0] >= 1.f ||
	  xred[1] < 0.f || xred[1] >= 1.f ||
	  xred[2] < 0.f || xred[2] >= 1.f)
	{
	  rmNodes[i] = (int)iter.node->number;
	  DBG_fprintf(stderr, " | %d  (%6.1f %6.1f %6.1f)"
		      " %10.5f %10.5f %10.5f -> removed\n",
		      iter.node->number, vect[0], vect[1], vect[2],
		      xred[0], xred[1], xred[2]);
	  i+= 1;
	}
      else
	{
	  visu_box_convertBoxCoordinatestoXYZ(data->priv->box, iter.node->xyz, xred);
	  iter.node->xyz[0] -= deltaEps[0];
	  iter.node->xyz[1] -= deltaEps[1];
	  iter.node->xyz[2] -= deltaEps[2];
	  iter.node->translation[0] = 0.f;
	  iter.node->translation[1] = 0.f;
	  iter.node->translation[2] = 0.f;
	  visu_node_array_setOriginal(VISU_NODE_ARRAY(data), iter.node->number);
	  DBG_fprintf(stderr, " | %d  (%6.1f %6.1f %6.1f)"
		      " %10.5f %10.5f %10.5f -> %10.5f %10.5f %10.5f\n",
		      iter.node->number, vect[0], vect[1], vect[2],
		      xred[0], xred[1], xred[2], iter.node->xyz[0],
		      iter.node->xyz[1], iter.node->xyz[2]);
	}
    }
  rmNodes[i] = -1;

  visu_node_array_removeNodes(VISU_NODE_ARRAY(data), rmNodes);

  g_free(rmNodes);

  /* Raise the SizeChanged signal at last. */
  visu_box_setMargin(data->priv->box, visu_node_array_getMaxElementSize(VISU_NODE_ARRAY(data)) +
                     visu_data_getAllNodeExtens(data, data->priv->box), TRUE);

  /* Remove possible translation. */
  data->priv->translationApply = FALSE;
  visu_data_forceXYZtranslation(data, zeros);
  g_signal_emit_by_name(G_OBJECT(data), "PositionChanged", (VisuElement*)0, NULL);

  return TRUE;
}
/**
 * visu_data_reorder:
 * @data: a #VisuData object, to reorder.
 * @dataRef: a #VisuData object, to take the order from.
 *
 * This routine modifies the node ordering of @data using the order in
 * @dataRef. The association is done by nearest neigbours conditions.
 *
 * Since: 3.6
 *
 * Returns: TRUE is the reordering is successfull (i.e. all nodes of
 * @data correspond to one of @dataRef).
 */
gboolean visu_data_reorder(VisuData *data, VisuData *dataRef)
{
  VisuNodeArrayIter iter, iterRef;
  float d, diff[3], dMin;
  guint id;

  g_return_val_if_fail(VISU_IS_DATA(dataRef), FALSE);
  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);

  DBG_fprintf(stderr, "Geometry: reorder between %p and %p.\n",
	      (gpointer)dataRef, (gpointer)data);

  DBG_fprintf(stderr, " | %d - %d.\n", visu_node_array_getNNodes(VISU_NODE_ARRAY(data)),
              visu_node_array_getNNodes(VISU_NODE_ARRAY(dataRef)));
  if (visu_node_array_getNNodes(VISU_NODE_ARRAY(data)) !=
      visu_node_array_getNNodes(VISU_NODE_ARRAY(dataRef)))
    return FALSE;

  visu_node_array_iterNew(VISU_NODE_ARRAY(data), &iter);
  for (visu_node_array_iterStart(VISU_NODE_ARRAY(data), &iter); iter.node;
       visu_node_array_iterNext(VISU_NODE_ARRAY(data), &iter))
    {
      id = 0;
      dMin = G_MAXFLOAT;
      visu_node_array_iterNew(VISU_NODE_ARRAY(dataRef), &iterRef);
      for (visu_node_array_iterStart(VISU_NODE_ARRAY(dataRef), &iterRef); iterRef.node;
           visu_node_array_iterNext(VISU_NODE_ARRAY(dataRef), &iterRef))
        {
          visu_geodiff_getPeriodicDistance(diff, data, iter.node, iterRef.node);
          d = diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2];
          if (d < dMin)
            {
              id = iterRef.node->number;
              dMin = d;
            }
        }
      visu_node_array_switchNumber(VISU_NODE_ARRAY(data), iter.node->number, id);
    }
  return TRUE;
}


/*****************************/
/* The node related routines */
/*****************************/

/**
 * visu_data_addNodeFromIndex:
 * @data: the #VisuData where to add the new #VisuNode ;
 * @position: a integer corresponding to the position of
 *            a #VisuElement in the array **nodes in the structure;
 * @xyz: (in) (array fixed-size=3): its coordinates ;
 * @reduced: coordinates are in reduced coordinates ;
 * @emitSignal: a boolean.
 *
 * This method adds a new #VisuNode to the specified #VisuData. Position must be
 * chosen between 0 and (ntype - 1) and corresponds to the position of the array
 * of #VisuNodes of a #VisuElement. If @emitSignal is TRUE, then
 * PopulationIncrease signal is triggered.
 *
 * Returns: (transfer none): a pointer to the newly created node.
 */
VisuNode* visu_data_addNodeFromIndex(VisuData *data, guint position,
                                     float xyz[3], gboolean reduced, gboolean emitSignal)
{
  VisuNode *node;
  int index[4];
  float coord[3];

  g_return_val_if_fail(VISU_IS_DATA(data), (VisuNode*)0);

  node = visu_node_array_getNewNode(VISU_NODE_ARRAY(data), position);
  g_return_val_if_fail(node, (VisuNode*)0);

  /* If coordinates are reduced, we expand them. */
  DBG_fprintf(stderr, "Visu Data: set node coordinates from (%g;%g;%g).\n",
              xyz[0], xyz[1], xyz[2]);
  if (reduced)
    visu_box_convertBoxCoordinatestoXYZ(data->priv->box, coord, xyz);
  else
    {
      coord[0] = xyz[0];
      coord[1] = xyz[1];
      coord[2] = xyz[2];
    }

  visu_node_newValues(node, coord);

  if (emitSignal)
    {
      index[0] = 1;
      index[1] = 2;
      index[2] = node->number;
      index[3] = -1;
      DBG_fprintf(stderr, "Visu Data: emit a "
		  "'PopulationIncrease' signal.\n");
      g_signal_emit_by_name(G_OBJECT(data), "PopulationIncrease",
                            (gpointer)(&index), NULL);
    }
  return node;
}

/**
 * visu_data_addNodeFromElement:
 * @data: the #VisuData where to add the new #VisuNode ;
 * @ele: the #VisuElement kind of the new #VisuNode ;
 * @xyz: (in) (array fixed-size=3): its coordinates ;
 * @reduced: coordinates are in reduced coordinates ;
 * @emitSignal: a boolean.
 *
 * This method adds a new #VisuNode to the specified #VisuData. If
 * @emitSignal is TRUE, then PopulationIncrease signal is
 * triggered.
 *
 * Returns: (transfer none): a pointer to the newly created node.
 */
VisuNode* visu_data_addNodeFromElement(VisuData *data, VisuElement *ele,
                                       float xyz[3], gboolean reduced,
                                       gboolean emitSignal)
{
  int pos;

  g_return_val_if_fail(VISU_IS_DATA(data) && ele, (VisuNode*)0);

  pos = visu_node_array_getElementId(VISU_NODE_ARRAY(data), ele);
  g_return_val_if_fail(pos >= 0, (VisuNode*)0);

  return visu_data_addNodeFromIndex(data, (guint)pos, xyz, reduced, emitSignal);
}
/**
 * visu_data_addNodeFromElementName:
 * @data: the #VisuData where to add the new #VisuNode ;
 * @name: the name of the element ;
 * @xyz: (in) (array fixed-size=3): its coordinates ;
 * @reduced: coordinates are in reduced coordinates ;
 * @emitSignal: a boolean.
 *
 * This method adds a new #VisuNode to the specified #VisuData. If
 * @emitSignal is TRUE, then PopulationIncrease signal is
 * triggered.
 *
 * Returns: (transfer none): a pointer to the newly created node.
 *
 * Since: 3.6
 */
VisuNode* visu_data_addNodeFromElementName(VisuData *data, const gchar *name,
                                           float xyz[3], gboolean reduced,
                                           gboolean emitSignal)
{
  VisuElement *ele;

  g_return_val_if_fail(VISU_IS_DATA(data) && name, (VisuNode*)0);

  ele = visu_element_lookup(name);
  g_return_val_if_fail(ele, (VisuNode*)0);

  return visu_data_addNodeFromElement(data, ele, xyz, reduced, emitSignal);
}

/**
 * visu_data_getNodeCoordinates:
 * @data: a #VisuData object ;
 * @node: a #VisuNode object ;
 * @x: (out caller-allocates): the x coordinate.
 * @y: (out caller-allocates): the y coordinate.
 * @z: (out caller-allocates): the z coordinate.
 *
 * Wrapper for the function visu_data_getNodePosition() in case of call
 * from python.
 *
 * Since: 3.6
 */
void visu_data_getNodeCoordinates(VisuData *data, VisuNode *node,
				  float *x, float *y, float *z)
{
  float xyz[3];

  g_return_if_fail(x && y && z);

  visu_data_getNodePosition(data, node, xyz);
  *x = xyz[0];
  *y = xyz[1];
  *z = xyz[2];
}
/**
 * visu_data_getNodePosition: (skip)
 * @data: a #VisuData object ;
 * @node: a #VisuNode object ;
 * @coord: (array fixed-size=3) (out caller-allocates): an array of 3
 * floating point values to store the position.
 *
 * Position of nodes are subject to various translations and different transformations.
 * Their coordinates should not be access directly through node.[xyz]. This method
 * is used to retrieve the given node position.
 *
 */
void visu_data_getNodePosition(VisuData *data, VisuNode *node, float coord[3])
{
  g_return_if_fail(VISU_IS_DATA(data) && node && coord);

  coord[0] = node->xyz[0] + node->translation[0] + data->priv->translation[0];
  coord[1] = node->xyz[1] + node->translation[1] + data->priv->translation[1];
  coord[2] = node->xyz[2] + node->translation[2] + data->priv->translation[2];
}
/**
 * visu_data_getNodeUserPosition: (skip)
 * @data: a #VisuData object ;
 * @node: a #VisuNode object ;
 * @coord: (array fixed-size=3) (out caller-allocates): an array of 3
 * floating point values to store the position.
 *
 * This routine is equivalent to visu_data_getNodePosition() except
 * that it's not applying internal box translation for non periodic
 * directions.
 *
 * Since: 3.7
 */
void visu_data_getNodeUserPosition(VisuData *data, VisuNode *node, float coord[3])
{
  VisuBoxBoundaries bc;

  g_return_if_fail(VISU_IS_DATA(data) && node && coord);

  visu_data_getNodePosition(data, node, coord);
  bc = visu_box_getBoundary(data->priv->box);
  if (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_YZ)
    coord[0] -= data->priv->translation[0];
  if (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_ZX)
    coord[1] -= data->priv->translation[1];
  if (bc == VISU_BOX_FREE || bc == VISU_BOX_SURFACE_XY)
    coord[2] -= data->priv->translation[2];
}


/****************/
/* The timeouts */
/****************/
/**
 * visu_data_addTimeout:
 * @data: a valid #VisuData object ;
 * @time: the period of call in milliseconds ;
 * @func: (scope call): the callback function to be called ;
 * @user_data: (closure): a pointer to some user defined informations.
 *
 * This method is used to add the @func method to be called regularly at the period
 * @time. This methos calls in fact g_timeout_add() with the given arguments. But
 * the source id is stored internaly and the timeout function is removed automatically
 * when the object @data is destroyed. It is convienient to add a method working
 * on the #VisuData object that is called periodically during the life of te object.
 *
 * Returns: the source id if the calling method need to work with it. To remove
 *          the callback, don't use g_source_remove() but visu_data_removeTimeout()
 *          to inform the #VisuData object that this source has been removed and
 *          not to remove it when the object will be destroyed.
 */
guint visu_data_addTimeout(VisuData *data, guint time,
			   GSourceFunc func, gpointer user_data)
{
  guint *id;

  g_return_val_if_fail(VISU_IS_DATA(data) && func, (guint)0);

  id = g_malloc(sizeof(guint));
  *id = g_timeout_add(time, func, user_data);
  data->priv->timeoutList = g_list_prepend(data->priv->timeoutList, (gpointer)id);
  DBG_fprintf(stderr, "Visu Data: create a new timeout callback : %d.\n", *id);
  return *id;
}

/**
 * visu_data_removeTimeout:
 * @data: a valid #VisuData object ;
 * @timeoutId: a source id.
 *
 * This method is used to remove a timeout that has been associated to the given
 * @data (see visu_data_addTimeout()).
 *
 * Returns: TRUE if the source has been found and removed.
 */
gboolean visu_data_removeTimeout(VisuData *data, guint timeoutId)
{
  GList *lst;
  gboolean found;

  g_return_val_if_fail(VISU_IS_DATA(data), FALSE);

  DBG_fprintf(stderr, "Visu Data: trying to remove a timeout callback (%d) ... ", timeoutId);
  found = FALSE;
  lst = data->priv->timeoutList;
  while(lst)
    {
      if (*(guint*)lst->data == timeoutId)
	{
	  found = g_source_remove(timeoutId);
	  data->priv->timeoutList =
	    g_list_delete_link(data->priv->timeoutList, lst);
	  DBG_fprintf(stderr, "OK (%d).\n", found);
	  return found;
	}
      lst = g_list_next(lst);
    }
  DBG_fprintf(stderr, "not found.\n");
  return found;
}


/*****************/
/* Miscellaneous */
/*****************/

/**
 * visu_data_class_getAllObjects:
 *
 * This methods is used to retrieve all #VisuObject currently allocated
 * in V_Sim. It is usefull to apply some changes on all objects (resources
 * for example).
 *
 * Returns: (transfer none) (element-type VisuData*): a list of V_Sim
 * own #VisuData objects.
 */
GList* visu_data_class_getAllObjects(void)
{
  DBG_fprintf(stderr, "Visu Data: get the allObjects list.\n");
  return allObjects;
}

/**
 * visu_data_setColorFunc:
 * @data: a #VisuData object ;
 * @func: (scope call): a method that colorize the nodes.
 *
 * This is a little trick to colorized the nodes. It should not be used since it
 * will probably be different in future release.
 */
void visu_data_setColorFunc(VisuData *data, VisuDataColorFunc func)
{
  g_return_if_fail(data);

  DBG_fprintf(stderr, "Visu Data: set the color method to %d.\n",
	      GPOINTER_TO_INT(func));
  data->priv->setColor = func;
}
/**
 * visu_data_getUserColor:
 * @data: a #VisuData object ;
 * @ele: a #VisuElement object ;
 * @node: a #VisuNode object ;
 * @rgba: (array fixed-size=4): a location to store the color.
 *
 * If a user defined color has been set (see
 * visu_data_setColorFunc()), then call this method to obtain a color
 * for the given node.
 *
 * Returns: TRUE if a user color has been defined.
 *
 * Since: 3.6
 */
gboolean visu_data_getUserColor(VisuData *data, VisuElement *ele,
				VisuNode *node, float rgba[4])
{
  if (!data->priv->setColor)
    return FALSE;

  return data->priv->setColor(data, rgba, ele, node);
}
/**
 * visu_data_hasUserColorFunc:
 * @data: a #VisuData object.
 * 
 * Test the existence of a user defined colourisation function.
 *
 * Returns: TRUE if a user color function has been defined with
 * visu_data_setColorFunc().
 *
 * Since: 3.6
 */
gboolean visu_data_hasUserColorFunc(VisuData *data)
{
  return (data->priv->setColor)?TRUE:FALSE;
}

/**
 * visu_data_setNodeScalingFunc:
 * @data: a #VisuData object ;
 * @scaling: (scope call): a scaling routine.
 *
 * Change the scaling routine when nodes are drawn.
 *
 * Since: 3.5
 */
void visu_data_setNodeScalingFunc(VisuData *data, VisuDataScalingFunc scaling)
{
  g_return_if_fail(VISU_IS_DATA(data));

  if (scaling)
    data->priv->scaling = scaling;
  else
    data->priv->scaling = defaultScaling;
}
/**
 * visu_data_getNodeScalingFactor:
 * @data: a #VisuData object.
 * @node: a #VisuNode object.
 *
 * One can modify the size of a given node using a routine set by
 * visu_data_setNodeScalingFunc(). By default the scaling is 1.
 *
 * Since: 3.5
 *
 * Returns: the scaling factor to be applied to node @node.
 */
float visu_data_getNodeScalingFactor(VisuData *data, VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_DATA(data), 0.f);

  return data->priv->scaling(data, node);
}
static float defaultScaling(VisuData *data _U_, VisuNode *node _U_)
{
  return 1.f;
}
