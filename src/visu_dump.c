/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_dump.h"

#include "visu_tools.h"
#include "dumpModules/externalDumpModules.h"

/**
 * SECTION:visu_dump
 * @short_description: Some resources to add the ability to export the
 * rendered data to an other format (usually image format).
 *
 * <para>V_Sim can export loaded data to othe formats. This module
 * descibes the methods and structure to create a dumping
 * extension. Basically, a dumping extension is just a #FileFormat and
 * a method that is called when exporting is required. No method
 * exists to create a dumping extension, just allocate and initialize
 * the #DumpType structure.</para>
 *
 * <para>The #writeDumpFunc should suspend its process to allow the
 * calling program to refresh itself if the dump process is
 * slow. Ideally, the argument @waitFunction should be called exactly
 * 100 times.</para>
 */

static GQuark quark;
static GList *allDumpModuleList = (GList*)0;
static int nbDumpModules = 0;
static gboolean internalInit = FALSE;
static VisuDumpClass *my_class = (VisuDumpClass*)0;

/**
 * VisuDump:
 * @bitmap: (in): TRUE if the format requires to export the view
 * into a bitmap ;
 * @glRequired: (in): TRUE if the method requires to run OpenGL.
 * @fileType: a #ToolFileFormat ;
 * @hasAlpha: TRUE if the format support alpha channel ;
 * @writeFunc: (scope call): a pointer to a write func.
 *
 * This structure is used to store a dumping extension. Such an
 * extension is characterized by its #ToolFileFormat and a method that can
 * write a file from the current rendered data.
 */
struct _VisuDump
{
  ToolFileFormat parent;

  gboolean bitmap, glRequired;
  gboolean hasAlpha;
  VisuDumpWriteFunc writeFunc;

  gboolean dispose_has_run;
};
struct _VisuDumpClass
{
  ToolFileFormatClass parent;
};

static void visu_dump_dispose(GObject* obj);
static void visu_dump_finalize(GObject* obj);
static void initDumpList();

G_DEFINE_TYPE(VisuDump, visu_dump, TOOL_TYPE_FILE_FORMAT)

static void visu_dump_class_init(VisuDumpClass *klass)
{
  DBG_fprintf(stderr, "Visu Dump: creating the class of the object.\n");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_dump_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_dump_finalize;

  my_class = klass;
  quark = g_quark_from_static_string("visu_dump");
}
static void visu_dump_dispose(GObject* obj)
{
  DBG_fprintf(stderr, "Visu Dump: dispose object %p.\n", (gpointer)obj);

  if (VISU_DUMP(obj)->dispose_has_run)
    return;

  VISU_DUMP(obj)->dispose_has_run = TRUE;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_dump_parent_class)->dispose(obj);
}
static void visu_dump_finalize(GObject* obj)
{
  DBG_fprintf(stderr, "Visu Dump: finalize object %p.\n", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_dump_parent_class)->finalize(obj);
}

static void visu_dump_init(VisuDump *obj)
{
  DBG_fprintf(stderr, "Visu Dump: initializing a new object (%p).\n",
	      (gpointer)obj);

  obj->dispose_has_run = FALSE;
  obj->writeFunc = (VisuDumpWriteFunc)0;
  obj->hasAlpha = TRUE;
  obj->bitmap = TRUE;
  obj->glRequired = TRUE;

  nbDumpModules += 1;
  allDumpModuleList = g_list_append(allDumpModuleList, obj);
}
/**
 * visu_dump_new:
 * @descr: an UTF8 translated string ;
 * @patterns: (array zero-terminated=1): a NULL terminated list of
 * pattern for this dump format ;
 * @method: (scope call): a method used to dump the current #VisuData ;
 * @bitmap: a flag for bitmap export.
 *
 * Create a new dump format.
 *
 * Since: 3.7
 *
 * Returns: a newly created dump format.
 */
VisuDump* visu_dump_new(const gchar* descr, const gchar** patterns,
                        VisuDumpWriteFunc method, gboolean bitmap)
{
  VisuDump *meth;

  g_return_val_if_fail(descr && method && patterns, (VisuDump*)0);

  meth = VISU_DUMP(g_object_new(VISU_TYPE_DUMP,
                                "name", descr, "ignore-type", FALSE, NULL));
  tool_file_format_addPatterns(TOOL_FILE_FORMAT(meth), patterns);
  meth->writeFunc  = method;
  meth->bitmap     = bitmap;
  meth->hasAlpha   = bitmap;
  meth->glRequired = bitmap;

  return meth;
}
/**
 * visu_dump_setHasAlpha:
 * @dump: a #VisuDump method.
 * @hasAlpha: a boolean.
 *
 * Set if @dump has an alpha channel or not.
 **/
void visu_dump_setHasAlpha(VisuDump *dump, gboolean hasAlpha)
{
  g_return_if_fail(VISU_IS_DUMP(dump));

  dump->hasAlpha = hasAlpha;
}
/**
 * visu_dump_setGl:
 * @dump: a #VisuDump method.
 * @needGl: a boolean
 *
 * Set if @dump requires OpenGL or not.
 **/
void visu_dump_setGl(VisuDump *dump, gboolean needGl)
{
  g_return_if_fail(VISU_IS_DUMP(dump));

  dump->glRequired = needGl;
}
/**
 * visu_dump_getBitmapStatus:
 * @dump: a #VisuDump method.
 *
 * Retrieve if @dump is exporting a bitmap or not (like to a new input
 * file format...).
 *
 * Returns: TRUE if the output is a bitmap indeed.
 **/
gboolean visu_dump_getBitmapStatus(VisuDump *dump)
{
  g_return_val_if_fail(VISU_IS_DUMP(dump), FALSE);

  return dump->bitmap;
}
/**
 * visu_dump_getGlStatus:
 * @dump: a #VisuDump method.
 *
 * Retrieve if @dump requires OpenGL to export or not.
 *
 * Returns: TRUE if OpenGL is required.
 **/
gboolean visu_dump_getGlStatus(VisuDump *dump)
{
  g_return_val_if_fail(VISU_IS_DUMP(dump), FALSE);

  return dump->glRequired;
}
/**
 * visu_dump_getAlphaStatus:
 * @dump: a #VisuDump method.
 *
 * Retrieve if @dump use alpha channel or not.
 *
 * Returns: TRUE if @dump has an alpha channel.
 **/
gboolean visu_dump_getAlphaStatus(VisuDump *dump)
{
  g_return_val_if_fail(VISU_IS_DUMP(dump), FALSE);

  return dump->hasAlpha;
}

/**
 * visu_dump_getAllModules:
 *
 * All dumping extensions are stored in an opaque way in V_Sim. But
 * they can be listed by a call to this method.
 *
 * Returns: (transfer none) (element-type VisuDump*): a list of all
 * the known dumping extensions. This list is own by V_Sim and should
 * be considered read-only.
 */
GList* visu_dump_getAllModules()
{
  if (!internalInit)
    initDumpList();
  DBG_fprintf(stderr, "Visu Dump: give list of dumps (%p).\n",
	      (gpointer)allDumpModuleList);
  return allDumpModuleList;
}
/**
 * visu_dump_getNModules:
 * 
 * A convenient way to know how many dumping extensions are registered.
 *
 * Returns: the number of known dumping extensions.
 */
gint visu_dump_getNModules()
{
  if (!internalInit)
    initDumpList();
  return nbDumpModules;
}

/**
 * visu_dump_write:
 * @dump: a #VisuDump object ;
 * @fileName: (type filename): a string that defined the file to write to ;
 * @width: an integer ;
 * @height: an integer ;
 * @dataObj: the #VisuData to be exported ;
 * @image: (allow-none) (element-type gint8): the data to be written ;
 * @error: a location to store some error (not NULL) ;
 * @functionWait: (allow-none) (closure data) (scope call): a method
 * to call periodically during the dump ;
 * @data: (closure): some pointer on object to be passed to the wait
 * function.
 *
 * Use the write function of @dump to export the current @dataObj to
 * file @fileName.
 *
 * Since: 3.6
 *
 * Returns: TRUE if dump succeed.
 */
gboolean visu_dump_write(VisuDump *dump, const char* fileName,
                         int width, int height, VisuData *dataObj,
                         GArray* image, ToolVoidDataFunc functionWait,
                         gpointer data, GError **error)
{
  g_return_val_if_fail(dump && dump->writeFunc, FALSE);

  if (dump->bitmap)
    {
      g_return_val_if_fail(image, FALSE);
      return dump->writeFunc(TOOL_FILE_FORMAT(dump), fileName, width, height,
                             dataObj, (guchar*)image->data, error, functionWait, data);
    }
  else
    return dump->writeFunc(TOOL_FILE_FORMAT(dump), fileName, width, height,
                           dataObj, (guchar*)0, error, functionWait, data);
}


static void initDumpList()
{
  int i;
  const VisuDump *dump;

  DBG_fprintf(stderr, "Visu Dump: Init export list.\n");
  for (i = 0; listInitDumpModuleFunc[i]; i++)
    {
      dump = listInitDumpModuleFunc[i]();
      if (dump)
        DBG_fprintf(stderr, " | %s\n", tool_file_format_getLabel(TOOL_FILE_FORMAT(dump)));
    }
  internalInit = TRUE;
  DBG_fprintf(stderr, "Visu Dump: %d valid dump module(s) found.\n", nbDumpModules);
}
/**
 * visu_dump_getQuark: (skip)
 * 
 * Internal routine to get the #GQuark to handle error related to dump
 * actions.
 */
GQuark visu_dump_getQuark()
{
  return quark;
}

void visu_dump_abort(GObject *obj _U_, gpointer data)
{
  /* TODO : mettre un stop dans le fichier en cours. */
  DBG_fprintf(stderr, "Visu dump : abortion requested.\n");
  *((int*)data) = 1;
}
