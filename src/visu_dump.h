/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DUMP_H
#define VISU_DUMP_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"
#include "coreTools/toolFileFormat.h"
#include "visu_data.h"

G_BEGIN_DECLS

/**
 * VisuDumpWriteFunc:
 * @format: a #ToolFileFormat object, corresponding to the write method ;
 * @fileName: a string that defined the file to write to ;
 * @width: an integer ;
 * @height: an integer.
 * @dataObj: the #VisuData to be exported ;
 * @image: the data to be written ;
 * @error: (allow-none): a location to store some error (not NULL) ;
 * @functionWait: (allow-none) (scope call): a method to call
 * periodically during the dump ;
 * @data: (closure): some pointer on object to be passed to the wait function.
 *
 * This is a prototype of a method implemented by a dumping extension that is called
 * when the current rendering must be dumped to a file.
 *
 * Returns: TRUE if everything went right.
 */
typedef gboolean (*VisuDumpWriteFunc) (ToolFileFormat *format, const char* fileName,
				       int width, int height, VisuData *dataObj,
				       guchar* image, GError **error,
				       ToolVoidDataFunc functionWait, gpointer data);

/**
 * VISU_TYPE_DUMP:
 *
 * Return the associated #GType to the VisuDump objects.
 */
#define VISU_TYPE_DUMP         (visu_dump_get_type ())
/**
 * VISU_DUMP:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuDump object.
 */
#define VISU_DUMP(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_DUMP, VisuDump))
/**
 * VISU_DUMP_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuDumpClass object.
 */
#define VISU_DUMP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_DUMP, VisuDumpClass))
/**
 * VISU_IS_DUMP:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuDump object.
 */
#define VISU_IS_DUMP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_DUMP))
/**
 * VISU_IS_DUMP_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuDumpClass class.
 */
#define VISU_IS_DUMP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_DUMP))
/**
 * VISU_DUMP_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_DUMP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DUMP, VisuDumpClass))

/**
 * VisuDump:
 *
 * An opaque structure.
 */
typedef struct _VisuDump VisuDump;
/**
 * VisuDumpClass:
 *
 * An opaque structure.
 */
typedef struct _VisuDumpClass VisuDumpClass;

/**
 * visu_dump_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuDump objects.
 */
GType visu_dump_get_type(void);

/**
 * VISU_ERROR_DUMP: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_ERROR_DUMP visu_dump_getQuark()
GQuark visu_dump_getQuark();
/**
 * VisuDumpErrorFlag:
 * @DUMP_ERROR_OPENGL: Error with OpenGL dumping.
 * @DUMP_ERROR_FILE: Error when opening.
 * @DUMP_ERROR_ENCODE: Wrongness when computing the encoding format.
 *
 * These are flags used when dumping to a file.
 */
typedef enum
  {
    DUMP_ERROR_OPENGL,
    DUMP_ERROR_FILE,
    DUMP_ERROR_ENCODE
  } VisuDumpErrorFlag;


/**
 * VisuDumpInitFunc: (skip)
 *
 * This protoype defines initializing function for dumping extension.
 * Create such a function and add its name in the list #listInitDumpModuleFunc
 * defined in dumpModules/externalDumpModules.h thus the new dumping extension
 * will be initialized on startup.
 *
 * Returns: a newly allocated #VisuDump.
 */
typedef const VisuDump* (*VisuDumpInitFunc) ();

VisuDump* visu_dump_new(const gchar* descr, const gchar** patterns,
                        VisuDumpWriteFunc method, gboolean bitmap);
void visu_dump_setHasAlpha(VisuDump *dump, gboolean hasAlpha);
void visu_dump_setGl(VisuDump *dump, gboolean needGl);
gboolean visu_dump_getBitmapStatus(VisuDump *dump);
gboolean visu_dump_getGlStatus(VisuDump *dump);
gboolean visu_dump_getAlphaStatus(VisuDump *dump);

gboolean visu_dump_write(VisuDump *dump, const char* fileName,
                         int width, int height, VisuData *dataObj,
                         GArray* image, ToolVoidDataFunc functionWait,
                         gpointer data, GError **error);


GList* visu_dump_getAllModules();
gint visu_dump_getNModules();

/**
 * visu_dump_abort:
 * @obj: an object ;
 * @data: some data.
 *
 * Does nothing for the moment.
 */
void visu_dump_abort(GObject *obj, gpointer data);

G_END_DECLS

#endif
