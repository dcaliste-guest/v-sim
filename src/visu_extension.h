/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_GL_EXT_H
#define VISU_GL_EXT_H

#include <glib.h>
#include <glib-object.h>

#include "openGLFunctions/renderingMode.h"

/***************/
/* Public part */
/***************/

/**
 * VISU_GL_EXT_PRIORITY_BACKGROUND
 *
 * An extension with this priority is drawn first.
 */
#define VISU_GL_EXT_PRIORITY_BACKGROUND 0
/**
 * VISU_GL_EXT_PRIORITY_NODES
 *
 * An extension with this priority is drawn alsmost first with the nodes.
 */
#define VISU_GL_EXT_PRIORITY_NODES 2
/**
 * VISU_GL_EXT_PRIORITY_NODE_DECORATIONS
 *
 * An extension with this priority is drawn just after the nodes.
 */
#define VISU_GL_EXT_PRIORITY_NODE_DECORATIONS 5
/**
 * VISU_GL_EXT_PRIORITY_HIGH
 *
 * An extension with this priority is drawn after the higher priorities.
 */
#define VISU_GL_EXT_PRIORITY_HIGH 20
/**
 * VISU_GL_EXT_PRIORITY_NORMAL
 *
 * An extension with this priority is drawn after the higher priorities.
 */
#define VISU_GL_EXT_PRIORITY_NORMAL 50
/**
 * VISU_GL_EXT_PRIORITY_LOW
 *
 * An extension with this priority is drawn among last extensions.
 */
#define VISU_GL_EXT_PRIORITY_LOW 80
/**
 * VISU_GL_EXT_PRIORITY_LAST
 *
 * An extension with this priority is drawn last.
 */
#define VISU_GL_EXT_PRIORITY_LAST 100

/**
 * VISU_TYPE_GL_EXT:
 *
 * return the type of #VisuGlExt.
 */
#define VISU_TYPE_GL_EXT	     (visu_gl_ext_get_type ())
/**
 * VISU_GL_EXT:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExt type.
 */
#define VISU_GL_EXT(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT, VisuGlExt))
/**
 * VISU_GL_EXT_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtClass.
 */
#define VISU_GL_EXT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT, VisuGlExtClass))
/**
 * VISU_IS_GL_EXT_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExt object.
 */
#define VISU_IS_GL_EXT_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT))
/**
 * VISU_IS_GL_EXT_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtClass class.
 */
#define VISU_IS_GL_EXT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT))
/**
 * VISU_GL_EXT_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_GL_EXT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT, VisuGlExtClass))


/**
 * VisuGlExtPrivate:
 * 
 * Private data for #VisuGlExt objects.
 */
typedef struct _VisuGlExtPrivate VisuGlExtPrivate;
/**
 * VisuGlExtClassPrivate :
 * 
 * Private data for #VisuGlExtClass classes.
 */
typedef struct _VisuGlExtClassPrivate VisuGlExtClassPrivate;

/**
 * VisuGlExt:
 * 
 * Common name to refer to a #_VisuGlExt.
 */
typedef struct _VisuGlExt VisuGlExt;
struct _VisuGlExt
{
  GObject parent;

  VisuGlExtPrivate *priv;
};

/**
 * VisuGlExtClass:
 * @parent: private.
 * @rebuild: a rebuilding function for this extension.
 * @priv: private.
 * 
 * Common name to refer to a #_VisuGlExtClass.
 */
typedef struct _VisuGlExtClass VisuGlExtClass;
struct _VisuGlExtClass
{
  GObjectClass parent;

  void (*rebuild)(VisuGlExt *self);

  VisuGlExtClassPrivate *priv;
};

/**
 * visu_gl_ext_get_type:
 *
 * This method returns the type of #VisuGlExt, use
 * VISU_TYPE_GL_EXT instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExt.
 */
GType visu_gl_ext_get_type(void);

void visu_gl_ext_setPriority(VisuGlExt* extension, guint priority);
void visu_gl_ext_setSaveState(VisuGlExt *extension, gboolean saveState);
void visu_gl_ext_setSensitiveToRenderingMode(VisuGlExt* extension, gboolean status);
gboolean visu_gl_ext_getSensitiveToRenderingMode(VisuGlExt* extension);

gboolean visu_gl_ext_getActive(VisuGlExt* extension);
gboolean visu_gl_ext_setActive(VisuGlExt* extension, gboolean value);

gboolean visu_gl_ext_setPreferedRenderingMode(VisuGlExt* extension,
						  VisuGlRenderingMode value);
VisuGlRenderingMode visu_gl_ext_getPreferedRenderingMode(VisuGlExt* extension);

guint visu_gl_ext_getGlList(VisuGlExt *extension);

void visu_gl_ext_call(VisuGlExt *extension, gboolean lastOnly);

void visu_gl_ext_rebuild(VisuGlExt *self);


/* Class methods. */
VisuGlExt* visu_gl_ext_getFromName(const char *name);
GList* visu_gl_ext_getAll(void);

void visu_gl_ext_rebuildAll(void);

#endif
