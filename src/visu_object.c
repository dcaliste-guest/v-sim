/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "visu_object.h"

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include "renderingMethods/renderingAtomic.h"
#include "visu_basic.h"

/**
 * SECTION:visu_object
 * @short_description: A general object to store the signals.
 * 
 * <para>At the present time, the signals are global to all V_Sim,
 * owned by a variable include in each parts called visu. This is
 * wherre these signals are defined.</para>
 */

/**
 * VisuObject:
 *
 * This structure describes a #VisuObject object.
 */
struct _VisuObjectPrivate
{
  VisuRendering *render;

  gboolean loading;
  GFunc    loadMessageFunc;
  gpointer loadMessageData;
};

/**
 * VisuObjectClass:
 * @parent: an object to inherit from.
 *
 * This structure describes the class #VisuObjectClass.
 */

/* All signal emit by visu are handled by this ogbject. */
static VisuObject *visu = (VisuObject*)0;

enum
  {
    COLORNEWAVAILABLE_SIGNAL,
    SHADENEWAVAILABLE_SIGNAL,
    DATANEW_SIGNAL,
    DATALOADED_SIGNAL,
    DATARENDERED_SIGNAL,
    DATAUNRENDERED_SIGNAL,
    VIEWNEW_SIGNAL,
    RENDERINGCHANGED_SIGNAL,
    RESOURCESLOADED_SIGNAL,
    ENTRYPARSED_SIGNAL,
    OPENGLASKFORREDRAW_SIGNAL,
    OPENGLFORCEREDRAW_SIGNAL,
    DIR_SIGNAL,
    VISU_NB_SIGNAL
  };

static guint signals[VISU_NB_SIGNAL];

G_DEFINE_TYPE(VisuObject, visu_object, G_TYPE_OBJECT)

/* Private marshals. */
static void g_cclosure_marshal_VOID__OBJECT_OBJECT(GClosure *closure,
                                                   GValue *return_value _U_,
                                                   guint n_param_values,
                                                   const GValue *param_values,
                                                   gpointer invocation_hint _U_,
                                                   gpointer marshal_data)
{
  typedef void (*callbackFunc)(gpointer data1, gpointer arg_1,
                               gpointer arg_2, gpointer data2);
  register callbackFunc callback;
  register GCClosure *cc = (GCClosure*)closure;
  register gpointer data1, data2;

  g_return_if_fail(n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA(closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer(param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer(param_values + 0);
      data2 = closure->data;
    }
  callback = (callbackFunc)(size_t)(marshal_data ? marshal_data : cc->callback);

  callback(data1, g_value_get_object(param_values + 1),
           g_value_get_object(param_values + 2), data2);
}

static void visu_object_class_init(VisuObjectClass *klass)
{
  GType paramPointer[1] = {G_TYPE_POINTER};
  GType paramObject[1] = {G_TYPE_OBJECT};
  GType paramGuint[1] = {G_TYPE_UINT};

  DBG_fprintf(stderr, "Visu Object: installing signals.\n");
  /**
   * VisuObject::colorNewAvailable:
   * @visuObj: the object emitting the signal.
   * @color: the newly created #ToolColor.
   *
   * A new #ToolColor is available.
   *
   * Since: 3.2
   */
  signals[COLORNEWAVAILABLE_SIGNAL] = 
    g_signal_newv ("colorNewAvailable",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__POINTER,
		   G_TYPE_NONE /* return_type */,
		   1     /* n_params */,
		   paramPointer /* pointer to the added color */);
  /**
   * VisuObject::shadeNewAvailable:
   * @visuObj: the object emitting the signal.
   * @shade: the newly created #ToolShade.
   *
   * A new #ToolShade is available.
   *
   * Since: 3.7
   */
  signals[SHADENEWAVAILABLE_SIGNAL] = 
    g_signal_new("shadeNewAvailable", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL /* accumulator */, NULL /* accu_data */,
                 g_cclosure_marshal_VOID__POINTER,
                 G_TYPE_NONE /* return_type */, 1, G_TYPE_POINTER);
  /**
   * VisuObject::dataNew:
   * @visuObj: the object emitting the signal.
   * @dataObj: the newly created #VisuData.
   *
   * A new #VisuData is available.
   *
   * Since: 3.2
   */
  signals[DATANEW_SIGNAL] = 
    g_signal_newv("dataNew",
		  G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL,
		  NULL, 
		  NULL,              
		  g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, paramObject);
  /**
   * VisuObject::dataLoaded:
   * @visuObj: the object emitting the signal.
   * @dataObj: the newly created #VisuData.
   *
   * The given @dataObj is fully populated and ready for usage.
   *
   * Since: 3.1
   */
  signals[DATALOADED_SIGNAL] = 
    g_signal_newv("dataLoaded",
		  G_TYPE_FROM_CLASS(klass),
		  G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		  NULL,
		  NULL, 
		  NULL,              
		  g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1, paramObject);
  /**
   * VisuObject::dataRendered:
   * @visuObj: the object emitting the signal.
   * @dataObj: the #VisuData object to be rendered on @view.
   * @view: the view to render to.
   *
   * The given @dataObj is fully set up and ready for rendering (no
   * further internal modifications will occur).
   */
  signals[DATARENDERED_SIGNAL] = 
    g_signal_new("dataRendered", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0 , NULL, NULL, g_cclosure_marshal_VOID__OBJECT_OBJECT,
                 G_TYPE_NONE, 2, G_TYPE_OBJECT, G_TYPE_OBJECT);
  /**
   * VisuObject::dataUnRendered:
   * @visuObj: the object emitting the signal.
   * @dataObj: the #VisuData object that was rendered on @view.
   * @view: the view @dataObj was rendered to.
   *
   * The given @dataObj is not rendered anymore on @view.
   *
   * Since: 3.7
   */
  signals[DATAUNRENDERED_SIGNAL] = 
    g_signal_new("dataUnRendered", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0 , NULL, NULL, g_cclosure_marshal_VOID__OBJECT_OBJECT,
                 G_TYPE_NONE, 2, G_TYPE_OBJECT, G_TYPE_OBJECT);
  /**
   * VisuObject::renderingChanged:
   * @visuObj: the object emitting the signal.
   * @meth: the newly chosen #VisuRendering method.
   *
   * The rendering method has been changed.
   */
  signals[RENDERINGCHANGED_SIGNAL] = 
    g_signal_newv ("renderingChanged",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__POINTER,
		   G_TYPE_NONE /* return_type */,
		   1     /* n_params */,
		   paramPointer  /* param_types */);
  /**
   * VisuObject::entryParsed:
   * @visuObj: the object emitting the signal.
   * @key: the key that has been parsed.
   *
   * The entry @key of a configuration file has just been successfully parsed.
   *
   * Since: 3.7
   */
  signals[ENTRYPARSED_SIGNAL] = 
    g_signal_new("entryParsed", G_TYPE_FROM_CLASS (klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS | G_SIGNAL_DETAILED,
                 0 , NULL, NULL, g_cclosure_marshal_VOID__STRING,
                 G_TYPE_NONE, 1, G_TYPE_STRING);
  /**
   * VisuObject::resourcesLoaded:
   * @visuObj: the object emitting the signal.
   * @dataObj: the associated #VisuData.
   *
   * The resource file has been read.
   */
  signals[RESOURCESLOADED_SIGNAL] = 
    g_signal_newv ("resourcesLoaded",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__OBJECT,
		   G_TYPE_NONE /* return_type */,
		   1     /* n_params */,
		   paramObject  /* param_types */);
  /**
   * VisuObject::OpenGLAskForReDraw:
   * @visuObj: the object emitting the signal.
   *
   * Internal signal, use VISU_REDRAW_ADD() instead.
   */
  signals[OPENGLASKFORREDRAW_SIGNAL] = 
    g_signal_newv ("OpenGLAskForReDraw",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__VOID,
		   G_TYPE_NONE /* return_type */,
		   0     /* n_params */,
		   NULL  /* param_types */);
  /**
   * VisuObject::OpenGLForceReDraw:
   * @visuObj: the object emitting the signal.
   *
   * Internal signal, use VISU_REDRAW_FORCE() instead.
   */
  signals[OPENGLFORCEREDRAW_SIGNAL] = 
    g_signal_newv ("OpenGLForceReDraw",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL /* class closure */,
		   NULL /* accumulator */,
		   NULL /* accu_data */,
		   g_cclosure_marshal_VOID__VOID,
		   G_TYPE_NONE /* return_type */,
		   0     /* n_params */,
		   NULL  /* param_types */);
  /**
   * VisuObject::DirectoryChanged:
   * @visuObj: the object emitting the signal.
   * @kind: a flag.
   *
   * The current directory has been changed. The kind of directory is
   * defined by @kind (see #).
   *
   * Since: 3.6
   */
  signals[DIR_SIGNAL] = 
    g_signal_newv ("DirectoryChanged",
		   G_TYPE_FROM_CLASS (klass),
		   G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
		   NULL , NULL, NULL,
		   g_cclosure_marshal_VOID__UINT,
		   G_TYPE_NONE /* return_type */,
		   1     /* n_params */,
		   paramGuint  /* param_types */);
  /**
   * VisuObject::viewNew:
   * @visuObj: the object emitting the signal.
   * @view: the newly created #VisuGlView.
   *
   * A new #VisuGlView is available.
   *
   * Since: 3.7
   */
  signals[DATANEW_SIGNAL] = 
    g_signal_new("viewNew", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, G_TYPE_OBJECT);

  g_type_class_add_private(klass, sizeof(VisuObjectPrivate));
}

static void visu_object_init(VisuObject *obj)
{
  DBG_fprintf(stderr, "Visu Object: creating a new visu (%p).\n", (gpointer)obj);

  /* Putting the default. */
  obj->priv = G_TYPE_INSTANCE_GET_PRIVATE(obj, VISU_TYPE_OBJECT, VisuObjectPrivate);

  obj->priv->render          = visu_rendering_getByName(VISU_RENDERING_ATOMIC_NAME);
  obj->priv->loading         = FALSE;
  obj->priv->loadMessageFunc = (GFunc)0;
  obj->priv->loadMessageData = (gpointer)0;
}

/**
 * visu_object_redraw:
 * @data: (allow-none) (type utf8): a string.
 *
 * Call the signal OpenGLAskForReDraw. The API is adapted to the routine to be added
 * in the gloop. Use VISU_REDRAW_ADD instead of this routine.
 *
 * Returns: FALSE to stop the emission as soon as done.
 */
gboolean visu_object_redraw(gpointer data)
{
  if (data)
    DBG_fprintf(stderr, "Visu Object: '%s' call for redraw.\n", (gchar*)data);
  g_signal_emit (visu, signals[OPENGLASKFORREDRAW_SIGNAL],
		 0 /* details */, NULL);
  return FALSE;
}
/**
 * visu_object_redrawForce:
 * @data: (allow-none) (type utf8): a string.
 *
 * Call the signal OpenGLForceReDraw. The API is adapted to the routine to be added
 * in the gloop. Use VISU_REDRAW_FORCE instead of this routine.
 *
 * Returns: FALSE to stop the emission as soon as done.
 */
gboolean visu_object_redrawForce(gpointer data)
{
  if (data)
    DBG_fprintf(stderr, "Visu Object: '%s' call for force redraw.\n", (gchar*)data);
  g_signal_emit(visu, signals[OPENGLFORCEREDRAW_SIGNAL],
		0 , NULL);
  return FALSE;
}

/**
 * visu_object_class_getStatic:
 *
 * Internal routine to access the #VisuObject object instanciated by
 * default. Use #VISU_OBJECT_INSTANCE instead.
 *
 * Returns: (transfer none): the default #VisuObject used by V_Sim.
 */
VisuObject* visu_object_class_getStatic()
{
  if (!visu)
    /* Creating the visu object to handle the signals. */
    visu = VISU_OBJECT(g_object_new(VISU_TYPE_OBJECT, NULL));

  return visu;
}

/**
 * visu_object_setRendering:
 * @obj: a #VisuObject object.
 * @method: (transfer full): a #VisuRendering method.
 *
 * Choose the method used to render the data.
 *
 * Returns: TRUE if the rendering method of @obj is actually changed.
 */
gboolean visu_object_setRendering(VisuObject *obj, VisuRendering* method)
{
  g_return_val_if_fail(VISU_IS_OBJECT_TYPE(obj), FALSE);
  g_return_val_if_fail(VISU_IS_RENDERING_TYPE(method), FALSE);

  DBG_fprintf(stderr, "Visu Object: set the rendering method to '%s' (%p).\n",
              visu_rendering_getName(method, TRUE), (gpointer)method);
  if (obj->priv->render == method)
    return FALSE;

  if (obj->priv->render)
    g_object_unref(obj->priv->render);
  obj->priv->render = method;
  g_object_ref(method);

  DBG_fprintf(stderr, "Visu Object: emit method changed (%p).\n", (gpointer)obj);
  g_signal_emit(obj, signals[RENDERINGCHANGED_SIGNAL],
		0, (gpointer)method, NULL);
  return TRUE;
}
/**
 * visu_object_getRendering:
 * @obj: a #VisuObject object.
 *
 * Get the current method used to render the data.
 *
 * Returns: (transfer none): the rendering method attached to @obj.
 */
VisuRendering* visu_object_getRendering(VisuObject *obj)
{
  g_return_val_if_fail(VISU_IS_OBJECT_TYPE(obj), (VisuRendering*)0);

  return obj->priv->render;
}

/**
 * visu_object_setLoadMessageFunc:
 * @obj: a #VisuObject object.
 * @func: (scope call): a function to print a message.
 * @data: user data.
 *
 * When a load process is running, on can defined a message function
 * that may be called to output message to the user using
 * visu_object_setLoadMessage().
 *
 * Since: 3.6
 */
void visu_object_setLoadMessageFunc(VisuObject *obj, GFunc func, gpointer data)
{
  obj->priv->loadMessageFunc = func;
  obj->priv->loadMessageData = data;
}
/**
 * visu_object_setLoadMessage:
 * @obj: a #VisuObject object.
 * @mess: a string.
 *
 * If a message function on load action has been set by
 * visu_object_setLoadMessageFunc(), then the given @mess is given as
 * argument to this function.
 *
 * Since: 3.6
 */
void visu_object_setLoadMessage(VisuObject *obj, const gchar *mess)
{
  if (obj->priv->loadMessageFunc)
    obj->priv->loadMessageFunc((gpointer)mess, obj->priv->loadMessageData);
}

/**
 * visu_object_load:
 * @obj: a #VisuObject object.
 * @data: a #VisuData object ;
 * @nSet: an integer ;
 * @cancel: (allow-none): a #GCancellable object.
 * @error: a pointer to store a possible error, location must be
 * initialized to (GError*)0.
 *
 * This calls the load method of the current rendering
 * method. Some informations may be store in @error if the returned
 * value is FALSE.
 * The file(s) which is(are) opened is(are) stored in the
 * #VisuData. The @nSet argument is used to load a specific set of
 * nodes if the input format supports it. If @nSet is 0, then the
 * default set of nodes is loaded.
 *
 * Returns: TRUE if everithing is OK, if FALSE, the @error is set and
 * should be freed with g_error_free().
 */
gboolean visu_object_load(VisuObject *obj, VisuData *data, int nSet,
                          GCancellable *cancel, GError **error)
{
  gboolean res;

  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(VISU_IS_OBJECT_TYPE(obj), FALSE);
  g_return_val_if_fail(!obj->priv->loading, FALSE);
  g_return_val_if_fail(obj->priv->render, FALSE);

  DBG_fprintf(stderr, "##### Load process #####\n");
  obj->priv->loading = TRUE;
  if (data)
    DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                (gpointer)data, G_OBJECT(data)->ref_count);

#if DEBUG == 1
  g_mem_profile();
#endif
  DBG_fprintf(stderr, "Visu Object: load dataset %d.\n", nSet);
  res = visu_rendering_load(obj->priv->render, data, nSet, cancel, error);
  DBG_fprintf(stderr, "Visu Object: load OK from rendering method,"
	      " continue with basic parts.\n");
  if (data)
    DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                (gpointer)data, G_OBJECT(data)->ref_count);
#if DEBUG == 1
  g_mem_profile();
#endif

  /* Set flags... */
  if (res)
    {
      DBG_fprintf(stderr, "##### Post-load signal emission #####\n");
      if (data)
        DBG_fprintf(stderr, " | %p has %d ref counts.\n",
                    (gpointer)data, G_OBJECT(data)->ref_count);
      DBG_fprintf(stderr, "Visu Object: emitting 'dataLoaded' signal for %p (%d).\n",
                  (gpointer)data, res);
      g_signal_emit(G_OBJECT(obj), signals[DATALOADED_SIGNAL],
                    0, data, NULL);
      DBG_fprintf(stderr, "Visu Object: emittion done.\n");
    }

  obj->priv->loading = FALSE;
  return res;
}
