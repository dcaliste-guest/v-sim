/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_OBJECT_H
#define VISU_OBJECT_H

/* This .c and .h is here to create a GObject
   that can emit signals when necessary. */

#include <glib-object.h>

#include "visu_tools.h"
#include "visu_rendering.h"
#include "visu_data.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_OBJECT:
 *
 * return the type of #VisuObject.
 */
#define VISU_TYPE_OBJECT		  (visu_object_get_type ())
/**
 * VISU_OBJECT:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuObject type.
 */
#define VISU_OBJECT(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_OBJECT, VisuObject))
/**
 * VISU_OBJECT_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuObjectClass.
 */
#define VISU_OBJECT_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_OBJECT, VisuObjectClass))
/**
 * VISU_IS_OBJECT_TYPE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuObject object.
 */
#define VISU_IS_OBJECT_TYPE(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_OBJECT))
/**
 * VISU_IS_OBJECT_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuObjectClass class.
 */
#define VISU_IS_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_OBJECT))
/**
 * VISU_OBJECT_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_OBJECT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), VISU_TYPE_OBJECT, VisuObjectClass))

typedef struct _VisuObjectClass VisuObjectClass;
typedef struct _VisuObjectPrivate VisuObjectPrivate;
typedef struct _VisuObject VisuObject;

/**
 * visu_object_get_type:
 *
 * This method returns the type of #VisuObject, use #VISU_TYPE_OBJECT instead.
 *
 * Returns: the type of #VisuObject.
 */
GType visu_object_get_type (void);

struct _VisuObject
{
  GObject parent;

  VisuObjectPrivate *priv;
};

struct _VisuObjectClass
{
  GObjectClass parent;
};

gboolean       visu_object_setRendering(VisuObject *obj, VisuRendering* method);
VisuRendering* visu_object_getRendering(VisuObject *obj);

/* Load related methods. */
void     visu_object_setLoadMessageFunc(VisuObject *obj, GFunc func, gpointer data);
void     visu_object_setLoadMessage    (VisuObject *obj, const gchar *mess);
gboolean visu_object_load              (VisuObject *obj, VisuData *data, int nSet,
                                        GCancellable *cancel, GError **error);


/**
 * VISU_OBJECT_INSTANCE:
 * 
 * This routine is used to get the global #VisuObject object to listen
 * to its signals.
 */
#define VISU_OBJECT_INSTANCE visu_object_class_getStatic()
VisuObject* visu_object_class_getStatic();

gboolean visu_object_redraw(gpointer data);
gboolean visu_object_redrawForce(gpointer data);

/**
 * VISU_REDRAW_ADD:
 *
 * A macro to ask V_Sim to redraw the rendering area at next idle
 * time, except if the deferred redraw option is set.
 */
#define VISU_REDRAW_ADD   g_idle_add(visu_object_redraw, (gpointer)__func__)
/**
 * VISU_REDRAW_FORCE:
 *
 * Force V_Sim to redraw at the next idle time, whatever value for the
 * deferred redraw option.
 */
#define VISU_REDRAW_FORCE g_idle_add(visu_object_redrawForce, (gpointer)__func__)


G_END_DECLS

#endif
