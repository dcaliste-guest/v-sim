/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse m�l :
	BILLARD, non joignable par m�l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant � visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accept� les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PAIRS
#define VISU_PAIRS

#include "openGLFunctions/view.h"
#include "visu_data.h"
#include "coreTools/toolColor.h"

G_BEGIN_DECLS

/**
 * VISU_PAIR_DISTANCE_MIN
 *
 * Flag used to define the minimum length to draw pair. This is useful with
 * the visu_pair_link_getDistance() and the visu_pair_link_setDistance() methods.
 */
#define VISU_PAIR_DISTANCE_MIN 0
/**
 * VISU_PAIR_DISTANCE_MAX
 *
 * Flag used to define the maximum length to draw pair. This is useful with
 * the visu_pair_link_getDistance() and the visu_pair_link_setDistance() methods.
 */
#define VISU_PAIR_DISTANCE_MAX 1

/**
 * VisuPair:
 * 
 * An opaque structure to define links (i.e. several #VisuPairLink)
 * between elements.
 */
typedef struct _VisuPair VisuPair;

/**
 * VISU_TYPE_PAIR_LINK:
 *
 * return the type of #VisuPairLink.
 *
 * Since: 3.7
 */
#define VISU_TYPE_PAIR_LINK	     (visu_pair_link_get_type ())
/**
 * VISU_PAIR_LINK:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPairLink type.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PAIR_LINK, VisuPairLink))
/**
 * VISU_PAIR_LINK_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPairLinkClass.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR_LINK, VisuPairLinkClass))
/**
 * VISU_IS_PAIR_LINK:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPairLink object.
 *
 * Since: 3.7
 */
#define VISU_IS_PAIR_LINK(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PAIR_LINK))
/**
 * VISU_IS_PAIR_LINK_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPairLinkClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_PAIR_LINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR_LINK))
/**
 * VISU_PAIR_LINK_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR_LINK, VisuPairLinkClass))

typedef struct _VisuPairLink        VisuPairLink;
typedef struct _VisuPairLinkPrivate VisuPairLinkPrivate;
typedef struct _VisuPairLinkClass   VisuPairLinkClass;

struct _VisuPairLink
{
  GObject parent;

  VisuPairLinkPrivate *priv;
};

struct _VisuPairLinkClass
{
  GObjectClass parent;
};

/**
 * visu_pair_link_get_type:
 *
 * This method returns the type of #VisuPairLink, use
 * VISU_TYPE_PAIR_LINK instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuPairLink.
 */
GType visu_pair_link_get_type(void);

GList*     visu_pair_link_getAll        (VisuElement *ele1, VisuElement *ele2);
VisuPairLink* visu_pair_link_new        (VisuElement *ele1, VisuElement *ele2,
                                         float minMax[2]);
VisuPairLink* visu_pair_link_getFromId  (VisuElement *ele1, VisuElement *ele2, guint pos);

gboolean   visu_pair_link_setDrawn      (VisuPairLink *data, gboolean drawn);
gboolean   visu_pair_link_getDrawn      (const VisuPairLink *data);
gboolean   visu_pair_link_setColor      (VisuPairLink *data, ToolColor* destColor);
ToolColor* visu_pair_link_getColor      (const VisuPairLink *data);
gboolean   visu_pair_link_setPrintLength(VisuPairLink *data, gboolean status);
gboolean   visu_pair_link_getPrintLength(const VisuPairLink *data);
gboolean   visu_pair_link_setDistance   (VisuPairLink *data, float val, int minOrMax);
float      visu_pair_link_getDistance   (const VisuPairLink *data, int minOrMax);
VisuPair*  visu_pair_link_getPair       (const VisuPairLink *data);

gboolean   visu_pair_link_isDrawn       (const VisuPairLink *data);

/**
 * VisuPairDistribution:
 * @ele1: one #VisuElement.
 * @ele2: one #VisuElement.
 * @histo: an array containing the distribution ;
 * @nValues: the size of the array ;
 * @initValue: the initial distance value (usualy 0) ;
 * @stepValue: the step increase in distance at each value ;
 * @nNodesEle1: the number of nodes used during the computation ;
 * @nNodesEle2: idem for #VisuElement 2.
 *
 * This structure stores for a given pair, the distance distribution
 * on a given range [@initValue;@nValues * @stepValue[.
 */
typedef struct _VisuPairDistribution VisuPairDistribution;
struct _VisuPairDistribution
{
  VisuElement *ele1, *ele2;
  guint *histo;
  guint nValues;
  float initValue, stepValue;
  guint nNodesEle1, nNodesEle2;
};

VisuPair* visu_pair_getPair(VisuElement *ele1, VisuElement *ele2);
void visu_pair_setProperty(VisuPair *pair, const gchar* key,
                           gpointer value, GDestroyNotify freeFunc);
gpointer visu_pair_getProperty(VisuPair *pair, const gchar* key);
void visu_pair_getElements(const VisuPair *pair, VisuElement **ele1, VisuElement **ele2);

gboolean visu_pair_removePairLink(VisuElement *ele1, VisuElement *ele2, VisuPairLink *data);

VisuPairDistribution* visu_pair_getDistanceDistribution(VisuPair *pair,
                                                        VisuData *dataObj,
                                                        float step,
                                                        float min, float max);
gboolean visu_pair_distribution_getNextPick(VisuPairDistribution *dd,
                                            guint startStopId[2], guint *integral,
                                            guint *max, guint *posMax);

/**
 * VisuPairForeachFunc:
 * @ele1: a #VisuElement object ;
 * @ele2: a #VisuElement object ;
 * @data: a #VisuPairLink object ;
 * @user_data: some user defined data.
 *
 * Prototype of functions called with the foreach method apply to each
 * pairs.
 */
typedef void (*VisuPairForeachFunc)(VisuElement *ele1, VisuElement *ele2,
                                    VisuPairLink *data, gpointer user_data);
void visu_pair_foreach(VisuPairForeachFunc whatToDo, gpointer user_data);
/**
 * visu_pair_readLinkFromTokens:
 * @tokens: array of tokens resulting from a call to g_strsplit() with " " as separator ;
 * @index: IN, the position of the beginning in @tokens ; OUT, one token
 *            after the last read ;
 * @data: a pointer to return an allocated link object ;
 * @position: the number of the line of the config
 *            file which the @line argument is taken from ;
 * @error: a location to store a possible reading error.
 *
 * This routine is used to read the resource file. Given @tokens, it
 * associate a link object by reading the two elements and the two
 * distances that characterised this link.
 *
 * Returns: TRUE if succeed.
 */
gboolean visu_pair_readLinkFromTokens(gchar **tokens, int *index, VisuPairLink **data,
				     int position, GError **error);

G_END_DECLS

#endif
